# Sistema para luminarias de CFE

## Requisitos del sistema

1. Tener instalado Docker
2. Tener instalado docker-compose

## Instrucciones para correr el sistema

Correr el siguiente comando

``` bash
docker-compose up -d
```

Y checar el puerto 8082 (lo configuramos por defecto para correr ahi, puede cambiarse en el archivo **docker-compose.yml**)

## Limpiar todos los archivos del sistema

Correr el siguiente comando

``` bash
docker-compose down
```