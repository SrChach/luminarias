# If you don't set "_usuario" and "_pass" for DB, this script won't work

# constants setted
_usuario="cfe_luminarias"
_pass="Cf3LuM1n@R1@S"
#_limit="LIMIT 150"
_no_clasificadas="no_clasificadas.txt" # No modificar

# Clean from past executions
rm -f $_no_clasificadas

# getting url from images from mysql
mysql --user="$_usuario" --password="$_pass" -e \
    "SELECT URLIMAGEN FROM CFELUMINARIAS.IMAGEN WHERE EXISTE_OS=0 $_limit" \
    | tail -n +2 > $_no_clasificadas

while IFS= read -r line
do
  if [ -f "../storage/images/$line" ]; then
    mysql --user="$_usuario" --password="$_pass" -e "UPDATE CFELUMINARIAS.IMAGEN SET EXISTE_OS=1 WHERE URLIMAGEN='$line'" && \
    echo -n "."
  else
    echo "! $line"
  fi
done < $_no_clasificadas

# Clean temporary files
rm -f $_no_clasificadas
