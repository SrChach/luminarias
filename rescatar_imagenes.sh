# If you don't set "_usuario" and "_pass" for DB, this script won't work

# constants setted
_usuario="cfe_luminarias"
_pass=""
_input="rescatar_imagenes.entrada" # No cambiar.
_search_in="../Evidencia7" # Cambiar, para buscar en otra carpeta
_carpeta_puente="dir_rescatar_imagenes" # No cambiar
_carpeta_salida="../storage/images"
_limit=""
_listado_rescatadas="rescatar_imagenes.rescatadas" # No cambiar

# Clean from past executions
rm -rf $_carpeta_puente
rm -f $_input $_listado_rescatadas
mkdir $_carpeta_puente

# getting url from images from mysql
mysql --user="$_usuario" --password="$_pass" -e \
    "SELECT URLIMAGEN FROM CFELUMINARIAS.IMAGEN WHERE EXISTE_OS=0 $_limit" \
    | tail -n +2 > $_input

echo "Buscando imagenes en '$_search_in'. Copiando a carpeta puente."
# For each image found in mysql not founded here, this
while IFS= read -r line
do
#  (find "$_search_in" -name "$line" -print &) | (head -n 1) >> "$_carpeta_puente/encontradas.txt"
  (find "$_search_in" -name "$line" -print &) |
  (head -n 1) | xargs -I '{}' cp '{}' $_carpeta_puente
done < $_input

ls $_carpeta_puente/ > $_listado_rescatadas

echo "Moviendo imagenes a destino. Guardando encontradas en la BD"
while IFS= read -r line
do
  mv $_carpeta_puente/$line $_carpeta_salida && 
  mysql --user="$_usuario" --password="$_pass" -e \
     "UPDATE CFELUMINARIAS.IMAGEN SET EXISTE_OS=2 WHERE URLIMAGEN='$line'" && echo "$line"
done < $_listado_rescatadas

# clean residual files
rm -rf $_carpeta_puente
rm -f $_input $_listado_rescatadas
