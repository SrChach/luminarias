<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');


//include_once './controlador.php';
//include_once './utilities.php';
include_once 'ErrorController.php';



class BodyMail  extends controlador
{

	protected $result;

	function optionBody($opcion,$nombreCompleto,$rutaDescarga,$values=''){
		$parametros= new parametros;
		$title="";
		if ($opcion=='generaReportePadron') {
			$title="LIGA PARA DESCARGA DE REPORTE";
		}else if($opcion=='credenciales'){
			$title="IMÁGENES CREDENCIALES ".$values['lote'];
		}else if($opcion=='acciones'){
			$title=$values['accion']." Padrón ".$values['programa'];
		}else if($opcion=='consulta_cuis'){
			$title="DESCARGA REPORTE CONSULTA CUIS";
		}else if($opcion=='generaRepoPanelEjec'){
			$title="DESCARGA REPORTE CUIS PANEL EJECUTIVO";
		}




		$body="<div style='  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); max-width: 600px; margin: auto;
		font-family: arial; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px;'>
		<img src='".$parametros -> urlHome()."public/portal/assets/img/logo.png' alt='' style='width:50%; height:50%; padding-left:150px'/><br>
		<h2 align='center' style='color:#28A7D0; margin-top: 20px; font-size: 25px'>".$title."</h2>
		<h3 align='left' style='color: grey; font-size: 22px;margin-top: 50px; '>".$nombreCompleto."</h3> 
		";



		if ($opcion =='generaReportePadron' || $opcion =='consulta_cuis' || $opcion =='generaRepoPanelEjec'){
			$body .="
			<p align='justify' style='margin-top: 50px; '>Por este medio se le notífica que su reporte a sido generado y podrá descargarlo a través de la siguiente liga:
			</p>
			<br>
			<p><strong>Descarga: </strong><a href='".$rutaDescarga."'>".$rutaDescarga."</a></p>
			";

		} else if ($opcion =='credenciales'){
			$body .="
			<p align='justify' style='margin-top: 50px; font-size:16px'>Por este medio se le notifica que la extracción de imágenes del lote <strong>".$values['lote']."</strong>
			se realizó con éxito. Consulte el portal para descargar las imágenes.
			</p>
			";

		} else if ($opcion =='acciones'){
			$body .="
			<p align='justify' style='margin-top: 50px; font-size:16px'>
			El proceso de ".$values['accion']." del padron ".$values['programa']." se realizo con exito. El resultado fue:
			</p>
			<p><strong>ACCIONES REALIZADAS: </strong>".$values['finalizadas']."</p>
			<p><strong>ERRORES: </strong>".$values['errores']."</p>
			<p>Descarga tu log en:<a href='".$rutaDescarga."'>".$rutaDescarga."</a></p>
			<p></p>
			";
		}





		$body .="<p style='margin-top: 50px; '>Sin mas por el momento, se le extiende un cordial saludo.</p>
		</div>";

		return $body;
	}

}


?>

