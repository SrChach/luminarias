<?php 

/**
* CREATED BY: 	JOSÉ JUAN DEL PRADO LUGO.
*/
class Get
{
	protected $conexion;
	protected $result;
	protected $sql;
	function __construct()
	{
		$this -> conexion = new conexion;
		$this -> result = null;
		$this -> sql = null;
	}

	function maxID($index,$tb){
		$this -> sql = "SELECT MAX($index) MAXID FROM $tb";
		$this -> result = $this -> conexion -> getResult($this -> sql);
		return $this -> result['MAXID'];
	}
}

?>