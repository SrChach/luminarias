<?php
//header('Content-type: application/json');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include 'controlador.php';
include 'conexion.php';
//include 'conexion_c.php';
include 'cuis.php';
include 'programas.php';
include 'utilities.php';
//include 'info.php';
include 'niveles_info.php';
include 'cedula.php';
include 'padrones.php';
include 'eventos.php';
include 'evidencia.php';
include 'exportaRepoGeneral.php';





class controlador_pruebas extends controlador{
	// function cuis(){ //LUIS
	// 	$cuis = new cuis;	
	// 	$datosGeneralesCuis = $cuis -> traeInfoCuis('50022220');
	// 	$infoIntegrantes = $cuis -> traeInfoIntegrantes('50022220');

	// 	$regresa['datosGeneralesCuis'] = $datosGeneralesCuis;
	// 	$regresa['infoIntegrantes'] = $infoIntegrantes;
	// 	return $regresa;
	// }

	function generalesPrograma($idPrograma){ // JJ
		$programas = new programas;

		$generales = $programas->traeProgramas($idPrograma);
		$tiposEventoPrograma = $programas->tiposEventoPrograma($idPrograma);
		$metasPrograma = $programas->metasPrograma($idPrograma);
		$fases = "";
		$avances = "";

		$regresa['generales'] =  $generales;
		$regresa['metas'] = $metasPrograma;
		$regresa['tiposEventoPrograma'] = $tiposEventoPrograma;
		$regresa['fases'] = $fases;
		$regresa['avances'] = "";

		return $regresa;
	}

	// function inicioBusquedaCUIS(){
	// 	$niveles_info = new niveles_info;
	// 	$utilities = new utilities;

	// 	$delegaciones = $niveles_info->delegaciones_cuis();
	// 	$subdelegaciones = $utilities->objetivisa("ID_DEL",$niveles_info->subdelegaciones_cuis());

	// 	$estados = $niveles_info -> estados_cuis();
	// 	$municipios = $utilities->objetivisa("CVE_ENTIDAD_FEDERATIVA",$niveles_info -> municipios_cuis());
	// 	$localidades = $utilities->objetivisa("CVE_MUNICIPIO",$niveles_info -> localidades_cuis());


	// 	$regresa['delegaciones'] = $delegaciones;
	// 	$regresa['subdelegaciones'] = $subdelegaciones;
	// 	$regresa['estados'] = $estados;
	// 	$regresa['municipios'] = $municipios;
	// 	$regresa['localidades'] = $localidades;
	// 	return $regresa;
	// }

	// function buscarCUIS($del = "",$subdel = "",$edo = "",$mun = "",$loc = "",$folio = "",$nombre = "",$idUsuario,$tipo=""){
	// 	$cuis = new cuis;
	// 	$csv = new Reports2;
	// 	$regresa ="";
	// 	if ($tipo=='complete') {

	// 		$query = $cuis -> buscaCuis($del,$subdel,$edo,$mun,$loc,$folio,$nombre,$tipo);
	// 		$fecha=strftime( "%Y%m%d%H%M%S", time() );
	// 		$nombreReporte='ReporteCuis_'.$fecha.'.csv';
	// 		$carpetaReporte='../../../reportes/'.$nombreReporte;
	// 		$opcion='consulta_cuis';
	// 		$respuestaGenera = $csv -> csv($query,$carpetaReporte,$nombreReporte,$idUsuario,$opcion);
	// 		$regresa='OK';
	// 	}else{
	// 		$busca = $cuis -> buscaCuis($del,$subdel,$edo,$mun,$loc,$folio,$nombre);
	// 		$regresa['generales'] = $busca['generales'];
	// 		$regresa['detalle'] = $busca['detalle'];
			
	// 	}
	// 	return $regresa;
	// }



	function cedula($c_persona){
		$cedula = new cedula;
		$generales = $cedula->generales($c_persona);
		$traeEventos = $cedula->traeEventos( $generales['IDLISTADO'] );

		$regresa['generales'] = $generales;
		$regresa['traeEventos'] = $eventos;

		return $regresa;
	}

	function generales_cuis($del,$subdel,$mun,$loc){
		$cuis = new cuis;

		if($del != ""){
			return $cuis->cuis_subdelegacion($del);
		}else if($subdel != ""){
			return $cuis->cuis_municipio($subdel);
		}elseif($mun != ""){
			return $cuis->cuis_localidad($mun);
		}else{
			return $cuis->cuis_delegacion();
		}

	}	

	function generales_padron($idPrograma,$idTipoEvento,$del,$subdel,$mun,$loc,$fechaInicio,$fechaTermino){
		$programas = new programas;
		$padrones = new padrones;


		if($del != ""){
			$datos = $programas->avanceProgramaSubdelegacion($idPrograma,$del,$idTipoEvento,$fechaInicio,$fechaTermino);
		}else if($subdel != ""){
			$datos = $programas->avanceProgramaMunicipio($idPrograma,$subdel,$idTipoEvento,$fechaInicio,$fechaTermino);
		}elseif($mun != ""){
			$datos = $programas->avanceProgramaLocalidad($idPrograma,$mun,$idTipoEvento,$fechaInicio,$fechaTermino);
		}else{
			$datos = $programas->avanceProgramaDelegacion($idPrograma,$idTipoEvento,$fechaInicio,$fechaTermino);
		}

		$fechas = $programas->fechasPrograma($idPrograma,$idTipoEvento);
		$entregas = $programas->entregas($idPrograma,$idTipoEvento);

		$regresa['generales'] = $padrones->generalesPadrones($idPrograma); 
		$regresa['datos'] = $datos; 
		$regresa['fechas'] = $fechas; 
		$regresa['entregas'] = $entregas; 
		
		return $regresa;
	}

	function cedula_2($identificador,$tipo = "LISTADO",$idProgramas=""){
		$cedula = new cedula;
		$programas = new programas;
		$padrones = new padrones;
		$utilities = new utilities;
		$eventos = new eventos;
		$evidencia = new evidencia;
		//echo "idprogramas = " . $idProgramas;
		// ECHO "A";
		$generales = $cedula->generales_idListado($identificador,$idProgramas);
		// ECHO "B";
		$programas = $padrones -> programas_idListado($identificador,$idProgramas);
		// ECHO "C";
		$eventos = $utilities->objetivisa('IDPROGRAMA',$eventos -> eventos_idListado($identificador,$idProgramas));
		// ECHO "D";
		$imagenes_eventos = $utilities->objetivisa('IDEVENTO',$evidencia->evidencia_eventos_idListado($identificador,$idProgramas));
		// ECHO "E";
		$expediente = $cedula->expediente($identificador);
		// ECHO "F";
		//$infoProgramas = $programas -> infoProgramas();

		$regresa['generales'] = $generales;
		$regresa['programas'] = $programas;
		$regresa['eventos']  = $eventos;
		$regresa['imagenes_eventos'] = $imagenes_eventos;
		$regresa['expediente'] = $expediente;


		return $regresa;
	}

	function respuestas_cuis($folio_cuis){
		$cuis = new cuis;
		$regresa['respuestas'] =$cuis->respuestas_cuis($folio_cuis);
		return $regresa;
	}
	

}

$controlador_pruebas = new controlador_pruebas;
$controlador_pruebas->VerificaSolicitud();
$error = $controlador_pruebas->error;

if($error != null){
	echo $error;
	return false;
}

$opcion = $controlador_pruebas->opcion;

if($opcion == "cuis"){
 	echo json_encode( $controlador_pruebas->cuis() ) ;
}elseif($opcion == "generalesPrograma"){
	echo json_encode( $controlador_pruebas->generalesPrograma(1) ) ;	
}


// elseif($opcion == "inicioBusquedaCUIS"){
// 	echo json_encode( $controlador_pruebas->inicioBusquedaCUIS() ) ;		
// }elseif($opcion == "buscarCUIS"){
// 	$del = (isset($_POST['delegacion']))?$_POST['delegacion']:"";
// 	$subdel = (isset($_POST['subdelegacion']))?$_POST['subdelegacion']:"";
// 	$edo = (isset($_POST['estado']))?$_POST['estado']:"";
// 	$mun = (isset($_POST['municipio']))?$_POST['municipio']:"";
// 	$loc = (isset($_POST['localidad']))?$_POST['localidad']:"";
// 	$folio = (isset($_POST['folio_cuis']))?$_POST['folio_cuis']:"";
// 	$nombre = (isset($_POST['nombre']))?$_POST['nombre']:"";
// 	$tipo = (isset($_POST['tipo']))?$_POST['tipo']:"";
// 	$idUsuario = (isset($_POST['idUsuario']))?$_POST['idUsuario']:"";
	
// 	// echo $del.' '.$subdel.' '.$edo.' '.$mun.' '.$loc.' '.$folio.' '.$nombre;
// 	echo json_encode( $controlador_pruebas->buscarCUIS($del,$subdel,$edo,$mun,$loc,$folio,$nombre,$idUsuario,$tipo) ) ;		

// }

elseif($opcion == "cedula"){
	echo json_encode( $controlador_pruebas->cedula( 691127 ) ) ;	
}elseif($opcion == "generales_cuis"){
	$del = (isset($_POST['delegacion']))?$_POST['delegacion']:"";
	$subdel = (isset($_POST['subdelegacion']))?$_POST['subdelegacion']:"";
	$mun = (isset($_POST['municipio']))?$_POST['municipio']:"";
	$loc = (isset($_POST['localidad']))?$_POST['localidad']:"";

	echo json_encode( $controlador_pruebas->generales_cuis($del,$subdel,$mun,$loc) );

}elseif($opcion == "generales_programa"){
	$idPrograma = $_POST['idPrograma'];
	//$idPrograma = 3; 
	$del = (isset($_POST['delegacion']))?$_POST['delegacion']:"";
	//$del = 9;
	$subdel = (isset($_POST['subdelegacion']))?$_POST['subdelegacion']:"";
	$mun = (isset($_POST['municipio']))?$_POST['municipio']:"";
	//$mun = "30004";
	$loc = (isset($_POST['localidad']))?$_POST['localidad']:"";


	$fechaInicio = (isset($_POST['fechaInicio']))?$_POST['fechaInicio']:"";
	$fechaTermino = (isset($_POST['fechaTermino']))?$_POST['fechaTermino']:"";
	$idTipoEvento = (isset($_POST['evento']))?$_POST['evento']:"";

	echo json_encode( $controlador_pruebas->generales_padron($idPrograma,$idTipoEvento,$del,$subdel,$mun,$loc,$fechaInicio,$fechaTermino) );

}elseif($opcion == "cedula_2"){
	$idPrograma = (isset($_POST['idPrograma']))?$_POST['idPrograma']:"";
	echo json_encode( $controlador_pruebas->cedula_2($_POST['folio'],"LISTADO",$idPrograma) ) ;
}elseif($opcion == "respuestas_cuis"){
	echo json_encode( $controlador_pruebas->respuestas_cuis($_POST['folio_cuis']) ) ;
	
}elseif($opcion == "listaPreguntaDoc"){
	$conexion = new conexion;
	$utilities = new utilities;

	$regresa['LISTA1'] = $utilities->objetivisa('IDPROGRAMA' , $conexion->getResults("SELECT  IDPROGRAMA,NOMBREPROGRAMA  FROM INTEGRADOR.PROGRAMA") );
	//$regresa['LISTA2'] = $utilities->objetivisa('IDTIPOEVENTO' , $conexion->getResults("SELECT IDTIPOEVENTO,TIPOEVENTO FROM INTEGRADOR.TIPOEVENTO") ) ;
	$regresa['LISTA2'] = $utilities->objetivisa('IDUSUARIO' , $conexion->getResults("SELECT IDUSUARIO,NOMBRE || ' ' || APELLIDOPATERNO NOMBRE FROM INTEGRADOR.USUARIO") ) ;
	//$regresa['LISTA1ID'] = 'IDPROGRAMA';
	//$regresa['LISTA2ID'] = 'IDTIPOEVENTO';
	$regresa['TABLAHIJA'] = 'USUARIOPROGRAMA';

	echo json_encode($regresa);
}

?>