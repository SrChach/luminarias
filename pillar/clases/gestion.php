<?php

class gestion{


	function traeTipoImagen($categorias = ""){
		$conexion = new conexion;
		$query = "SELECT TIPOIMAGEN NOMBRE,IDTIPOIMAGEN VALUE,CATEGORIA SEPARADOR 
					FROM INTEGRADOR.TIPOIMAGEN TI
					JOIN INTEGRADOR.CATEGORIA C ON TI.IDCATEGORIA = C.IDCATEGORIA
					ORDER BY CATEGORIA";
		return $conexion->getResults($query);
	}

	function traeTipoDocumentoEspecifico($categorias = ""){
		$conexion = new conexion;
		$utilities = new utilities;
		$query = "SELECT TIPODOCUMENTO NOMBRE,IDTIPODOCUMENTO VALUE,  IDTIPOIMAGEN 
		              FROM INTEGRADOR.TIPODOCUMENTO 
		              ORDER BY IDTIPOIMAGEN";
		return $utilities->objetivisa('IDTIPOIMAGEN',$conexion->getResults($query));
	}

	function preguntas(){
		$conexion = new conexion;
		$utilities = new utilities;	
		$query = "SELECT  PD.IDPREGUNTADOC IDPREGUNTA_TIPODOC, PD.IDTIPODOCUMENTO IDTIPODOCUMENTOESPECIFICO, PD.IDPREGUNTA, PD.ESTATUS, PD.ORDERN ORDEN, 
		                  PD.FECHAINSERCION, PD.FECHAMODIFICACION, P.IDPREGUNTA, P.TIPOPREGUNTA, P.PREGUNTA, 
		                  P.OPCIONES, P.MENSAJEERROR, P.VALORESCORRECTOS, P.ESTATUS STATUS, P.NOMBRECORTO NOMBRE_CORTO, 
		                  P.FECHAINSERCION, P.FECHAMODIFICACION
                        FROM INTEGRADOR.PREGUNTADOC PD 
						JOIN INTEGRADOR.PREGUNTA P ON P.IDPREGUNTA = PD.IDPREGUNTA
						WHERE PD.ESTATUS = 1 
						ORDER BY PD.IDTIPODOCUMENTO,PD.ORDERN";
		return $utilities->objetivisa('IDTIPODOCUMENTOESPECIFICO',$conexion->getResults($query));
	}

	function respuestas($idsImagen){
		$conexion = new conexion;
		$utilities = new utilities;	
		$query = "SELECT * FROM ".$conexion->esquema."RESPUESTA pd where idImagen in ($idsImagen) order by idImagen";
    	//echo $query;
		$final = null;
		$capa1 = $utilities->objetivisa('IDIMAGEN',$conexion->getResults($query));
		if($capa1 != null){
			foreach ($capa1 as $key => $value) {
				$final[$key] = $utilities->objetivisa('IDPREGUNTA',$value);
			}
		}
		return $final; 
	}

	/**********************/
	function respuestas_folio_cuis($folio_cuis=""){
		$conexion = new conexion;
		$utilities = new utilities;	
		$query = "select r.idrespuesta, r.idexpediente, r.idimagen, r.idpregunta, r.idtipopregunta, r.idtipopregunta_id,r.respuesta 
		from respuestas r
		left join imagen i on r.idimagen= i.idimagen
		where i.folio_cuis=$folio_cuis";
	    	//echo $query;
		$final = null;
		$capa1 = $utilities->objetivisa('IDIMAGEN',$conexion->getResults($query));
		if($capa1 != null){
			foreach ($capa1 as $key => $value) {
				$final[$key] = $utilities->objetivisa('IDPREGUNTA',$value);
			}
		}
		return $final;
	}

	function respuestas_c_persona($c_persona=""){
		$conexion = new conexion;
		$utilities = new utilities;	
		$query = "select r.idrespuesta, r.idexpediente, r.idimagen, r.idpregunta, r.idtipopregunta, r.idtipopregunta_id,r.respuesta 
		from respuestas r
		left join imagen i on r.idimagen= i.idimagen
		right join  (select idimagen, respuesta, idpregunta from respuestas r where r.respuesta='$c_persona' and r.idpregunta=35) icp 
		on r.idimagen=icp.idimagen";
	    	//echo $query;
		$final = null;
		$capa1 = $utilities->objetivisa('IDIMAGEN',$conexion->getResults($query));
		if($capa1 != null){
			foreach ($capa1 as $key => $value) {
				$final[$key] = $utilities->objetivisa('IDPREGUNTA',$value);
			}
		}
		return $final;
	}

	function respuestas_imagen($idImagen){
		$conexion = new conexion;
		$utilities = new utilities;	
		$query = "select * from ".$conexion->esquema."RESPUESTA pd where idImagen = $idImagen";
    	//echo $query;
		$final = null;
		$final = $utilities->objetivisa('IDPREGUNTA',$conexion->getResults($query));	
		return $final;
	}

	function guardaValidacion($POST){
		$conexion = new conexion;
		//$calificador = new calificador;
		$utilities = new utilities;
		$rutinasValidacion = new rutinasValidacion;

		if($_SESSION['perfil'] != "2" AND $_SESSION['perfil'] != "10" and $_SESSION['perfil'] != "4" and $_SESSION['perfil'] != "6"){
			ECHO "SU USUARIO NO TIENE LOS PRIVILEGIOS PARA HACER CAMBIOS EN ESTA SECCION, POR FAVOR, VUELVA A INICIAR SESSION, SI EL PROBLEMA PERSISTE CONTACTE AL ADMINSTRADOR DEL SISTEMA";
			return;
		}

		
		$todas_preguntas = $this->preguntas();
		//$todas_respuestas = $this->respuestas($idExpediente);
		$idUsuario_val = $_SESSION['IDUSUARIO'];


		//var_dump($todas_respuestas[518969]);
		//var_dump($todas_respuestas[$idImagen]);


		$idUsuario = $_SESSION['IDUSUARIO'];
		//var_dump($_SESSION);
		$ids = [];
		$i = 0;
		foreach ($POST as $key => $value) {  // Recorre todo el arreglo recibido
			$pos = strpos($key, "cambio");
			if($pos !== false){  // Filtra de inicio solo los input de cambio
				$idImagen = explode("_", $key)[1];
				if($value == 1){  // Filtra todos los input de cambio que fueron afectados
					$idTipoImagen = $POST['tipoImagen_'.$idImagen];
					$tipoDocEsp = $POST['tipoDocumentoEspecifico_'.$idImagen];
					$query = "SELECT * FROM ".$conexion->esquema."RESPUESTA WHERE IDIMAGEN = $idImagen";

					//$todas_respuestas = $utilities->objetivisa( 'IDIMAGEN', $conexion->getResults($query) );
					//echo $query;
					//var_dump($todas_respuestas);

					//$query = "SELECT * FROM ".$conexion->esquema."RESPUESTA pd where idImagen in ($idsImagen) order by idImagen";
    	//echo $query;
		$final = null;
		$capa1 = $utilities->objetivisa('IDIMAGEN',$conexion->getResults($query));
		if($capa1 != null){
			foreach ($capa1 as $key => $value) {
				$final[$key] = $utilities->objetivisa('IDPREGUNTA',$value);
			}
		}
		$todas_respuestas = $final;

					$query = "UPDATE ".$conexion->esquema."IMAGEN SET IDTIPOIMAGEN = $idTipoImagen, IDTIPODOCUMENTO = $tipoDocEsp WHERE IDIMAGEN = $idImagen";
					//echo $query;
					$conexion->insertUpdate($query,0);
					
					$preguntas = $todas_preguntas[$tipoDocEsp];
					
					if(count($preguntas)>0){
						foreach ($preguntas as $p) {
							//var_dump($p);
							if($p['TIPOPREGUNTA'] == 1 or $p['TIPOPREGUNTA'] == 3 or $p['TIPOPREGUNTA'] == 9){
								$respuesta =  $POST['val_' . $idImagen . "_" . $p['IDPREGUNTA']];

								if($respuesta != ""){
									if( !isset($todas_respuestas[$idImagen][$p['IDPREGUNTA'] ] ) ){

										$query = "insert into RESPUESTA ( IDIMAGEN, IDPREGUNTA, IDTIPOPREGUNTA, IDTIPOPREGUNTA_ID, RESPUESTA) 
													values ($idImagen,".$p['IDPREGUNTA'].",".$p['TIPOPREGUNTA'].",0,'$respuesta')";
									}else{
										$query = "UPDATE ".$conexion->esquema."RESPUESTA SET  RESPUESTA  = '$respuesta' WHERE  idImagen = $idImagen 
																				AND idPregunta = ".$p['IDPREGUNTA']."";
									}
									//echo $query."<br><br>";
									$conexion->insertUpdate($query,0);//****
								}
								
							}elseif($p['TIPOPREGUNTA'] == 4){

							}elseif($p['TIPOPREGUNTA'] == 5){
								$respuesta = $_POST['fechaanio_'.$idImagen.'_'.$p['IDPREGUNTA']] . "_". $_POST['fechames_'.$idImagen.'_'.$p['IDPREGUNTA']] . "_" . $_POST['fechadia_'.$idImagen.'_'.$p['IDPREGUNTA']];

								if( !isset($todas_respuestas[$idImagen][$p['IDPREGUNTA'] ] ) ){
									if($respuesta != "" and $respuesta != null and $respuesta != "_undefined_undefined"){
										$query = "INSERT INTO ".$conexion->esquema."RESPUESTA ( IDIMAGEN, IDPREGUNTA, IDTIPOPREGUNTA, IDTIPOPREGUNTA_ID, RESPUESTA) 
												values ($idImagen,".$p['IDPREGUNTA'].",".$p['TIPOPREGUNTA'].",0,'$respuesta')";
									}
									
								}else{
									
									$query = "UPDATE ".$conexion->esquema."RESPUESTA SET  RESPUESTA  = '$respuesta' WHERE idImagen = $idImagen AND idPregunta = ".$p['IDPREGUNTA']."";
								}
								
								//echo $query;
								$conexion->insertUpdate($query,0);// ****
								
							}
						}
					}
					

					$observacion = $_POST['observacion_'.$idImagen];
					if(isset($_POST['docpadre_'.$idImagen])){
						$docpadre = $_POST['docpadre_'.$idImagen];
					}else{
						$docpadre = 0;
					}
					if(isset($_POST['version_'.$idImagen])){
						$version = $_POST['version_'.$idImagen];
					}else{
						$version = 0;
					}
					
					$error = $rutinasValidacion->valida($idImagen);//****



					$query = "update ".$conexion->esquema."imagen set v_imagen = ".$error['estatus'].",MENSAJE = '".$error['mensaje']."', IDTIPOIMAGEN = $idTipoImagen, IDTIPODOCUMENTO = $tipoDocEsp,observacion = '$observacion',V_USUARIO = $idUsuario,/*DOCPADRE = $docpadre,dia=$version,*/fechavalidacion = SYSDATE where idImagen = $idImagen";
					echo "<br>".$query."<br>";
					$conexion->insertUpdate($query);//****
					
					$query = "commit";
					$conexion->insertUpdate($query); //****
					
					
					
				}

				

			}
		}
		
	

		//$avance_gestion = $_POST['avance_gestion'];
		//$reparacion = $_POST['reparacion'];
		//$estado_cons = $_POST['estado_cons'];
		//$nombre_val = $_POST['validador'];
		//$validacion_fisica = $_POST['validacion_avance'];
		//$obs_gen = $_POST['obs_gen'];

		//$query = "update expedientes set FECHA_VAL = SYSDATE,OBSERVACION = '$obs_gen' where idExpediente = $idExpediente";

		//echo $query . "<br><br>";
		//$conexion->insertUpdate($query);
		
		//echo "<script>window.location='../valida.php?folio=".$_POST['idObra_folio_cuis']."'</script>";
	}


	function imagenXimagen($tipo_img,$cve_edo,$cve_mun,$cve_loc,$tipoObra,$region,$avance,$lote,$tipoReporte,$subdel="",$nombre=""){
		$conexion = new conexion; 

		$idReporte = $conexion -> getResult("select max(idReporte) IDREPORTE FROM reportes")['IDREPORTE'] + 1;
		$query = "insert into reportes values($idReporte,".$_SESSION['idUsuario'].",'".$nombre.".csv',0,0,SYSDATE)";
		
		$conexion->insertUpdate($query); 
		
		



		$query = "select * from pregunta  where status = 1";
		$preguntas = $conexion->getResults($query);

		$complementos = new complementos;
		$complemento = $complementos -> complemento_imagen_x_imagen($tipo_img,'B',$cve_edo,$cve_mun ,$cve_loc,"11111" ,$region,$avance,$lote,$subdel); 



		$query = "select img.idExpediente from imagen img join expedientes exp on img.idExpediente = exp.idExpediente 
		$complemento
		group by img.idExpediente";

		

		$folios = $conexion->getResults($query); 


		$fp = fopen("../../../../../../fonden/portal/reportes/$nombre.csv", 'w');



		
		$ig = 0;
		$titulos = 0;
		foreach ($folios as $f) {

			$csv_export = "";
			if($complemento == ""){
				$compDif = " where i.idExpediente = " . $f['IDEXPEDIENTE'];
			}else{
				$compDif = " where i.idExpediente = " . $f['IDEXPEDIENTE'];
			}

			$cadena = "";
			foreach ($preguntas as $p) { 
				
				$cadena .= "LISTAGG(case when r.idPregunta = ".$p['IDPREGUNTA']." THEN  RESPUESTA ELSE '' END) WITHIN GROUP (ORDER BY P.IDPREGUNTA) " . $p['NOMBRE_CORTO'] .",";

			}
			


			
			$query = "select  exp.idtipoObra prioridad,folio_cuis_i,exp.region delegacion,exp.accion subdeletacion,
			exp.nombre_mun,exp.nombre_loc,exp.idExpediente idLote,
			exp.folio lote,img.folio_cuis_i folio_cuis,'DATOS ' DATOS,img.url,
			img.IDIMAGEN, img.TIPOIMAGEN, img.TIPODOCESP_DOC,RESPUESTAS, DOCUMENTO_LEGIBLE, VIGENCIA, NOMBRE_S, 
			img.AP_PATERNO, img.AP_MATERNO, img.NOMBRE_2, AP_PATERNO_2, AP_MATERNO_2,CP, NUMERO_EXT, COLONIA, NUM_EXTERIOR, CLAVE_LOC, 
			NUMERO_MEDIDOR, NUMERO_MEDIDOR_L, CLAVE_EDO, CALLE, EMISION, CURP, 
			NUMERO_CONTRATO_CUENTA,  CLAVE_ELECTOR, FECHA_NACIMIENTO,TIENE_HUELLA, FOLIO_INCLUSION, VIGENCIA_ANIO, SECCION, 
			DISTRITO, img.FOLIO_CUIS, CLAVE_MUN, NUMERO_INTERIOR, FIRMA, 
			FOLIO_CUESTIONARIO, FECHA_LEVANTAMIENDO, TIENE_FIRMA, DIRECCION, FIRMA_SELLO_AUTORIDAD_LOCAL,
			FECHA_VENCIMIENTO, NUMERO_CONTRATO, NUMERO_SERVICIO, OCR, img.OBSERVACION, img.fechavalidacion,  img.v_usuario, img.hora_val, img.email,IMG.PARENTESCO_1,prioridad, img.FOJA, img.NUMERO_ACTA, img.NUMERO_LIBRO, img.TOMO, img.ANIO_REGISTRO, img.GENERO, img.ESTADO_NACIMIENTO
			from  (
				select i.idExpediente,i.folio_cuis folio_cuis_i,i.idTipoImagen,i.idImagen,
				ti.nombre tipoImagen,tde.nombredoc tipoDocEsp_doc,i.url,count(*) respuestas,
				$cadena       
				i.OBSERVACION, i.fechavalidacion,   i.v_usuario,TO_CHAR(i.FECHAVALIDACION,'HH24:MI:SS') hora_val, usu.email,i.idEmpresa prioridad
				from imagen i 
				left join respuestas r on r.idImagen = i.idImagen 
				left join pregunta p on r.idPRegunta = p.idPregunta 
				join tipoImagen ti on i.idTipoImagen = ti.idTipoImagen 
				join TIPODOCUMENTOESPECIFICO tde 
				on i.idTIPODOCUMENTOESPECIFICO = tde.idTIPODOCUMENTOESPECIFICO 
				join usuario usu on i.v_usuario = usu.idusuario
				$compDif
				group by i.idExpediente,i.folio_cuis,i.url,i.idTipoImagen,
				i.idImagen,ti.nombre,tde.nombredoc,i.url ,i.OBSERVACION,i.fechavalidacion,i.v_usuario,usu.email,i.idEmpresa 
			) img 
			join expedientes exp on exp.idExpediente = img.idExpediente	
			$complemento 
			order by exp.region,exp.accion,exp.nombre_mun,exp.nombre_loc,exp.idExpediente,
			exp.folio,to_number(replace( SUBSTR(url,32),'.TIF',''))";

//echo $query;
/*
return false;*/

$datos = $conexion->getResults($query);

if($titulos == 0){
	foreach ($datos[0] as $key => $value) {
		if(!is_numeric($key)){
			if($csv_export==""){
				$csv_export = $key ;
			}else{
				$csv_export = $csv_export . "," . $key ;
			}
		}
	}
	$csv_export.= '
	';  
	fwrite($fp, $csv_export);
	$titulos = 1;
}

foreach ($datos as $c) {
	$csv_export  = "";
	foreach ($c as $d){
		$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r","/\t+/","\t","		    ","		    ",chr(9));

		$reemplazar=array('','','','','','','','','');

		$csv_export .= trim(str_ireplace($buscar,$reemplazar, str_replace('\n','', utf8_decode ( str_replace('\n','', str_replace(',','' ,trim ( $d ) ) ) )))). ',';
	} 
	$csv_export.= '
	';  
					//var_dump($csv_export);
	fwrite($fp, $csv_export);
}


}
fclose($fp);

$conexion->insertUpdate("update reportes set terminado = 1 where idReporte = $idReporte");
		//var_dump($array);
header ("Location: http://sedatu-fonden.com.mx/reportes/$nombre.csv"); 
return ;

}


}

?>