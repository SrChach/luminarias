<?php

/* * ******************************************************
 * Copyright G214                                         *
 *                                                        *
 * Author:  Angel Alam Gonzalez Gonzalez                  *
 *                                                        *
 * ****************************************************** */


date_default_timezone_set('America/Mexico_City');


class conexion {
    protected $DB;
    protected $DB_USER;
    protected $DB_PASS;
    protected $DB_CHAR;
    protected $conn;
    public $esquema;

    public function __construct() {
        /*$this -> DB = '//192.168.214.6/ORAG214';
        $this -> DB_USER = 'INTEGRADOR';
        $this -> DB_PASS = 'INTEGRADOR';
        $this -> DB_CHAR = 'AL32UTF8';
        $this -> esquema = 'INTEGRADOR.';
        $this -> conn = oci_connect($this->DB_USER, $this->DB_PASS, $this->DB, $this->DB_CHAR);
        if (!$this->conn) {
            $m = oci_error();
            trigger_error(htmlentities($m['message']), E_USER_ERROR);
        }*/
        $this -> DB = '//169.57.16.75:1521/uatportdb';
        $this -> DB_USER = 'AGONZALEZ';
        $this -> DB_PASS = 'p7eU1AWbT';
        $this -> DB_CHAR = 'AL32UTF8';
        $this -> esquema = 'INTEGRADOR.';
        $this -> conn = oci_connect($this->DB_USER, $this->DB_PASS, $this->DB, $this->DB_CHAR);
        if (!$this->conn) {
            $m = oci_error();
            trigger_error(htmlentities($m['message']), E_USER_ERROR);
        }
    }

    public function getResults($query,$maxRows = -1,$skipRow = 0) {

        $stid = oci_parse($this->conn, $query);
        $aa = oci_execute($stid);
        $nrows = oci_fetch_all($stid, $res, $skipRow, $maxRows, OCI_FETCHSTATEMENT_BY_ROW);
        // return $nrows;
        return $res;
    }

    public function getResult($query) {
        $stid = oci_parse($this->conn, $query);
        $aa = oci_execute($stid);
        $var = oci_fetch_array($stid,OCI_RETURN_NULLS + OCI_ASSOC);
        return $var;
    }

    public function insertUpdate($query,$commit = 1) {
        if($commit == 1){
            //echo "entre";
            $comm = OCI_COMMIT_ON_SUCCESS   ;
        }else{
            //echo "entre2";
            $comm = OCI_NO_AUTO_COMMIT;
        }
        $stid = oci_parse($this->conn, $query);
        $ok=oci_execute($stid);
        return $ok;
    }
}
