<?php 

set_time_limit(60000);
ini_set('memory_limit', -1);
ignore_user_abort(1);
ini_set('max_execution_time', 30000);


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 




class Reports 
{
	protected $conexion;
	protected $utilities;
	protected $result;
	protected $sql;
	function __construct(){
		$this -> conexion = new conexion;
		$this -> utilities = new utilities;
		$this -> sql = null;
		$this -> result = null;
	}
	public function ReportPogramaEvento($params)
	{

		$this-> sql = "SELECT * FROM (
							SELECT 
      							P.IDPADRON,P.IDPROGRAMA, P.IDLISTADO, P.FECHAINSERCION, P.FECHAMODIFICACION, 
      							P.DISTRIBUCION_OPERATIVA, P.ESTATUS, P.IDTIPOEVENTO,PR.NOMBREPROGRAMA,TE.TIPOEVENTO,
      							L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,(L.NOMBRE||' '||L.APATERNO||' '||L.AMATERNO)BENEFICIARIO,
                    			L.ORIGEN,L.DELEGACION ID_DEL,SD.DELEGACION,SD.SUBDELEGACION ID_SUB,L.SUBDELEGACION,
      							L.CVEEDO,EML.NOMBREENT,EML.CVEMUN,EML.NOMBREMUN,EML.CVELOC,EML.NOMBRELOC
							FROM INTEGRADOR.PADRON P
								INNER JOIN INTEGRADOR.PROGRAMA PR ON (PR.IDPROGRAMA=P.IDPROGRAMA)
								INNER JOIN INTEGRADOR.TIPOEVENTO TE ON (TE.IDTIPOEVENTO=P.IDTIPOEVENTO)
								INNER JOIN INTEGRADOR.LISTADO L ON (L.IDLISTADO=P.IDLISTADO)
								INNER JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON (L.CVELOC=EML.CVELOC)
                				INNER JOIN INTEGRADOR.DELEGACIONSUBDELEGACION SD ON (SD.ID_SUB=L.SUBDELEGACION)
            				GROUP BY P.IDPADRON,P.IDPROGRAMA, P.IDLISTADO, P.FECHAINSERCION, P.FECHAMODIFICACION, 
      							P.DISTRIBUCION_OPERATIVA, P.ESTATUS, P.IDTIPOEVENTO,PR.NOMBREPROGRAMA,TE.TIPOEVENTO,
      							L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
                    			L.ORIGEN,L.DELEGACION,SD.DELEGACION,SD.SUBDELEGACION,L.SUBDELEGACION,
      							L.CVEEDO,EML.NOMBREENT,EML.CVEMUN,EML.NOMBREMUN,EML.CVELOC,EML.NOMBRELOC
						)
						WHERE IDPROGRAMA = ".$params['idP']." AND IDTIPOEVENTO = ".$params['idE'];
		$this -> result = $this -> conexion -> getResults($this -> sql);
		return $this -> result = array('DATOS' => $this -> result , "NAME" => 'Infra_estuctura');
	}

	public function ReportPogramaEvento23($params)
	{
		$this -> sql = "SELECT L.DELEGACION ID_DEL,DS.DELEGACION,COUNT(*) ENTREGAS FROM INTEGRADOR.EVENTO E 
				    JOIN INTEGRADOR.LISTADO L ON E.IDLISTADO = L.IDLISTADO
				    JOIN INTEGRADOR.DELEGACIONSUBDELEGACION DS ON L.CVEMUN = DS.ID_MUN
				    WHERE IDPROGRAMA = $idPrograma $comp
				    GROUP BY DS.DELEGACION,L.DELEGACION";
	}

	public function Avances($params)
	{

        $nivel=$params['nivel'];

        if ($nivel=="subdelegacion") {
        	$tipo=" L.DELEGACION =";
        }else if($nivel=="municipio"){
        	$tipo=" L.SUBDELEGACION =";
        }else if($nivel=="localidad"){
        	$tipo=" L.CVEMUN =";
        }else if($nivel==""){
        	$tipo=" L.CVELOC =";
        }
   		
   		

		$sql = "SELECT 
							 E.IDEVENTO, L.DELEGACION ID_DEL, DS.DELEGACION , L.SUBDELEGACION ID_SUB, DS.SUBDELEGACION,
							 L.CVEEDO, ENT.NOMBREENT, L.CVEMUN, MUN.NOMBREMUN, L.CVELOC, LOC.NOMBRELOC, E.LATITUD, E.LONGITUD,
							 E.IDPADRON, E.IDPROGRAMA ID_PROGRAMA, PR.NOMBREPROGRAMA PROGRAMA, E.IDTIPOEVENTO ID_EVENTO, 
							 TE.TIPOEVENTO EVENTO, E.IDLISTADO, L.FOLIOCUIS, L.C_PERSONA, E.FOLIOPDA, E.FECHAVISITA,
							 E.FECHA_INI, E.FECHA_TERMINO, E.IDUSUARIOSINCRONIZACION, E.IMEI, E.OBSERVACION, 
							 E.VALIDACION, E.IDUSUARIOVALIDACION, E.OBSERVACIONVALIDACION, E.CODIGO_RESULTADO ID_CODIGO_RESULTADO,  CR.CODIGO_RESULTADO,
							 E.AVANCE, P.DISTRIBUCION_OPERATIVA, P.ESTATUS, L.NOMBRE, L.APATERNO, L.AMATERNO, L.GENERO, L.PARENTESCO, L.C_INTEGRANTE
					FROM INTEGRADOR.EVENTO E 	
						JOIN INTEGRADOR.LISTADO L ON E.IDLISTADO = L.IDLISTADO
						JOIN INTEGRADOR.DELEGACIONSUBDELEGACION DS ON L.CVEMUN = DS.ID_MUN
						JOIN INTEGRADOR.PADRON P ON P.IDPADRON=E.IDPADRON
						JOIN INTEGRADOR.PROGRAMA PR ON P.IDPROGRAMA=PR.IDPROGRAMA
						JOIN INTEGRADOR.TIPOEVENTO TE ON P.IDTIPOEVENTO=TE.IDTIPOEVENTO
						JOIN INTEGRADOR.CODIGORESULTADO CR ON E.CODIGO_RESULTADO=CR.IDCODIGO_RESULTADO
						JOIN (select CVEENT, NOMBREENT from integrador.estadomunicipiolocalidad GROUP BY CVEENT, NOMBREENT ORDER BY CVEENT ASC) ENT ON ENT.CVEENT=L.CVEEDO
						JOIN (select CVEMUN, NOMBREMUN from integrador.estadomunicipiolocalidad GROUP BY CVEMUN, NOMBREMUN ORDER BY CVEMUN ASC) MUN ON MUN.CVEMUN=L.CVEMUN
						JOIN (select CVELOC, NOMBRELOC from integrador.estadomunicipiolocalidad GROUP BY CVELOC, NOMBRELOC ORDER BY CVELOC ASC) LOC ON L.CVELOC=LOC.CVELOC
				    WHERE E.IDPROGRAMA =".$params['idP']." AND " .$tipo.$params['valor']." ORDER BY DS.DELEGACION ASC, DS.SUBDELEGACION ASC";


			return $sql;
		// $this -> result = $this -> conexion -> getResults($this -> sql);
		// return $this -> result = array('DATOS' => $this -> result , "NAME" => 'Avances');

		
	}


	public function Credenciales($params)
	{
		

        $nivel=$params['nivel'];

        if ($nivel=="subdelegacion") {
        	$tipo=" L.DELEGACION =";
        }else if($nivel=="municipio"){
        	$tipo=" L.SUBDELEGACION =";
        }else if($nivel=="localidad"){
        	$tipo=" L.CVEMUN =";
        }else if($nivel==""){
        	$tipo=" L.CVELOC =";
        }
   		
   		

		$this-> sql = " SELECT L.DELEGACION ID_DEL,DS.DELEGACION, L.SUBDELEGACION ID_SUBDEL, DS.SUBDELEGACION, L.CVEEDO, ENT.NOMBREENT, L.CVEMUN, MUN.NOMBREMUN, L.CVELOC, LOC.NOMBRELOC,
					        P.IDPADRON ,L.IDLISTADO,C.IDCREDENCIAL,L.C_PERSONA CLAVE_PER,P.IDPROGRAMA , 
					        C.FOLIOCUIS,C.C_PERSONA,C.IDTIPOEVENTO,
				        CASE WHEN C.IDCREDENCIAL IS NULL THEN 0 ELSE 1 END TIENE
						FROM INTEGRADOR.PADRON P--760150,
						JOIN INTEGRADOR.LISTADO L ON P.IDLISTADO=L.IDLISTADO--760150,
						JOIN INTEGRADOR.DELEGACIONSUBDELEGACION DS ON L.CVEMUN = DS.ID_MUN
						JOIN (select CVEENT, NOMBREENT from integrador.estadomunicipiolocalidad GROUP BY CVEENT, NOMBREENT ORDER BY CVEENT ASC) ENT ON ENT.CVEENT=L.CVEEDO
						JOIN (select CVEMUN, NOMBREMUN from integrador.estadomunicipiolocalidad GROUP BY CVEMUN, NOMBREMUN ORDER BY CVEMUN ASC) MUN ON MUN.CVEMUN=L.CVEMUN
						JOIN (select CVELOC, NOMBRELOC from integrador.estadomunicipiolocalidad GROUP BY CVELOC, NOMBRELOC ORDER BY CVELOC ASC) LOC ON L.CVELOC=LOC.CVELOC
						 LEFT JOIN INTEGRADOR.CREDENCIAL C ON C.C_PERSONA = L.C_PERSONA AND P.IDPROGRAMA=".$params['idP']. "
						  WHERE P.IDPROGRAMA =".$params['idP']." AND " .$tipo.$params['valor']." ORDER BY TIENE DESC" ;
							 	

		$this -> result = $this -> conexion -> getResults($this -> sql);
		return $this -> result = array('DATOS' => $this -> result , "NAME" => 'Avances');

		
	}
}
?>