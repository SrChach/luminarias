<?php
    class gestionImg_model{

        function getCategories(){            
            $conexion = new conexion;
            $query = "SELECT * FROM INTEGRADOR.CATEGORIA";            
            $results = $conexion->getResults($query);            
            return $results;
        }
        
        function getTypeImages($category){
            $conexion = new conexion;
            $query = "SELECT IDTIPOIMAGEN CLAVEIMAGEN, TIPOIMAGEN NOMBRE, NOMBRE_CORTO FROM INTEGRADOR.TIPOIMAGEN
                        INNER JOIN INTEGRADOR.CATEGORIA CAT ON INTEGRADOR.TIPOIMAGEN.IDCATEGORIA = CAT.IDCATEGORIA
                        WHERE CAT.CATEGORIA = '$category'";
            $results = $conexion->getResults($query);
            return $results;
        }
        
        function getTypeDocs($typeImage){
            $conexion = new conexion;
            $query = "SELECT TPDOC.IDTIPODOCUMENTO ClAVEDOCUMENTO, TPDOC.TIPODOCUMENTO, TPDOC.NOMBRE_CORTO FROM INTEGRADOR.TIPODOCUMENTO TPDOC
                        INNER JOIN INTEGRADOR.TIPOIMAGEN TIMG ON TPDOC.IDTIPOIMAGEN = TIMG.IDTIPOIMAGEN
                        WHERE TIMG.IDTIPOIMAGEN = '$typeImage'";
            $results = $conexion->getResults($query);
            return $results;
        }
        
        function createCategory($objectData){            
            $conexion = new conexion;
            $query = "INSERT INTO INTEGRADOR.CATEGORIA(IDCATEGORIA, CATEGORIA) VALUES( (SELECT MAX(IDCATEGORIA) + 1 FROM INTEGRADOR.CATEGORIA), '".$objectData['nameCategory']."')";
            
            $result = $conexion->insertUpdate($query);
            return $result;
        }

        function createTypeImageLastCat($objectData){
            $conexion = new conexion;
            $query = "INSERT INTO INTEGRADOR.TIPOIMAGEN(IDTIPOIMAGEN, TIPOIMAGEN, NOMBRE_CORTO, IDCATEGORIA) VALUES( 
                            (SELECT MAX(IDTIPOIMAGEN) + 1 FROM INTEGRADOR.TIPOIMAGEN), 
                            '".$objectData['namTypImgCat']."', 
                            '".$objectData['namTypImgCatShort']."',
                            (SELECT IDCATEGORIA FROM INTEGRADOR.CATEGORIA WHERE FECHAINSERCION IN(SELECT MAX(FECHAINSERCION) FROM INTEGRADOR.CATEGORIA)))";
            
            $result = $conexion->insertUpdate($query);
            return $result;
        }

        function createTypeDocumentLastCat($objectData){
            $conexion = new conexion;
            $query = "INSERT INTO INTEGRADOR.TIPODOCUMENTO(IDTIPODOCUMENTO, TIPODOCUMENTO, NOMBRE_CORTO, IDTIPOIMAGEN) VALUES(
                            (SELECT MAX(IDTIPODOCUMENTO) + 1 FROM INTEGRADOR.TIPODOCUMENTO),
                            '".$objectData['namTypDoc']."',                            
                            '".$objectData['namTypDocShort']."',
                            (SELECT IDTIPOIMAGEN FROM INTEGRADOR.TIPOIMAGEN WHERE FECHAINSERCION IN(SELECT MAX(FECHAINSERCION) FROM INTEGRADOR.TIPOIMAGEN)))";
            $result = $conexion->insertUpdate($query);
            return $result;
        }

        function createTypeImage($objectData){
            $conexion = new conexion;
            $query = "INSERT INTO INTEGRADOR.TIPOIMAGEN(IDTIPOIMAGEN, TIPOIMAGEN, NOMBRE_CORTO, IDCATEGORIA) VALUES( 
                            (SELECT MAX(IDTIPOIMAGEN) + 1 FROM INTEGRADOR.TIPOIMAGEN), 
                            '".$objectData['nameTypeImg']."', 
                            '".$objectData['nameTypeImgShort']."', 
                            (SELECT IDCATEGORIA FROM INTEGRADOR.CATEGORIA WHERE CATEGORIA = '".$objectData['nameCategory']."') )";
            
            $result = $conexion->insertUpdate($query);
            return $result;
        }
        
        function createTypeDocument($objectData){
            $conexion = new conexion;
            $query = "INSERT INTO INTEGRADOR.TIPODOCUMENTO(IDTIPODOCUMENTO, TIPODOCUMENTO, IDTIPOIMAGEN, NOMBRE_CORTO) VALUES(
                            (SELECT MAX(IDTIPODOCUMENTO) + 1 FROM INTEGRADOR.TIPODOCUMENTO),
                            '".$objectData['nameTypeDoc']."',
                            '".$objectData['keyTypeImg']."',
                            '".$objectData['nameTypeDocShort']."' )";
            $result = $conexion->insertUpdate($query);
            return $result;
        }

        function updateTypeImage($objectData){
            $conexion = new conexion;
            $query = "UPDATE INTEGRADOR.TIPOIMAGEN SET TIPOIMAGEN = '".$objectData['nameTypeImg']."', NOMBRE_CORTO = '".$objectData['nameTypeImgShort']."' WHERE IDTIPOIMAGEN = '".$objectData['keyImg']."' ";
            $result = $conexion->insertUpdate($query);
            return $result;
        }

        function updateTypeDocument($objectData){
            $conexion = new conexion;
            $query = "UPDATE INTEGRADOR.TIPODOCUMENTO SET TIPODOCUMENTO = '".$objectData['nameTypeDoc']."', NOMBRE_CORTO = '".$objectData['nameTypeDocShort']."' WHERE IDTIPODOCUMENTO = '".$objectData['keyDoc']."' ";
            $result = $conexion->insertUpdate($query);
            return $result;
        }
    }
?>