<?php
/**
*
*/
include_once 'conexion_mysql.php';
include_once 'getS.php';


class User
{
	protected $sql;
	protected $result;
	protected $conexion;
	protected $mensaje;
	protected $getS;


	function __construct()
	{
		$this -> conexion = new conexion;
		$this -> uniqid = "_VER#".substr(explode(".", uniqid('',true))[1], 2,7);
		$this -> mensaje = ['SIN DATOS',
		'EXITO! SE HA CANCELADO ESTA CUENTA.',
		'EXITO! SE HA ACTIVO ESTA CUENTA.',
		'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO',
		'EXITO! SE ELIMINO CUENTA DE USUARIO.'] ;
		$this -> getS = new Get;

	}
	public function getUser($id)
	{
		$complete="";
		if ($id!='') {
			$complete = " AND U.IDUSUARIO=$id";
		}
		$this -> sql = "SELECT U.IDUSUARIO,U.NOMBRE||' '||U.APELLIDOPATERNO||' '||U.APELLIDOMATERNO USUARIO,
		U.PERFIL,TU.TIPOUSUARIO, U.STATUS, U.IMG_PERFIL, EMAIL FROM INTEGRADOR.USUARIO U
		INNER JOIN INTEGRADOR.TIPOUSUARIO TU ON (U.PERFIL=TU.IDTIPOUSUARIO $complete)
		ORDER BY U.NOMBRE ASC, U.APELLIDOPATERNO";
		try {
			$this -> result = $this -> conexion -> getResults($this -> sql);
			if ($this -> result != null) {
				$this -> result = array ('CODIGO'=> true, 'DATOS' => $this -> result);
			}else{$this -> result = array ('CODIGO'=> false, 'DATOS' => $this -> mensaje[0]);}
		} catch (Exception $e) {
			$this -> result = array ('CODIGO'=> false, 'DATOS' => $e);
		}
		return $this -> result;
	}


	public function getUserProfile($id)
	{
		$this -> sql = "SELECT IDUSUARIO,NOMBRE,APELLIDOPATERNO,APELLIDOMATERNO,TELEFONO_C,TELEFONO_T,IMG_PERFIL, PERFIL,EMAIL, PASS, ROL, VALIDACION
		FROM INTEGRADOR.USUARIO WHERE IDUSUARIO=$id";
		$this -> sql2 = "SELECT IDPROGRAMA FROM INTEGRADOR.USUARIOPROGRAMA WHERE IDUSUARIO=$id AND STATUS_PERFIL=1";
		$this -> sql3 = "SELECT IDMODULO FROM INTEGRADOR.USUARIOMODULO WHERE IDUSUARIO=$id AND STATUS_PERFIL=1";

		try {
			$DATOS['GENERALES'] = $this -> conexion -> getResult($this -> sql);
			$DATOS['PROGRAMAS'] = $this -> conexion -> getResults($this -> sql2);
			$DATOS['MODULOS'] = $this -> conexion -> getResults($this -> sql3);

			$this -> result=$DATOS;
			if ($this -> result != null) {
				$this -> result = array ('CODIGO'=> true, 'DATOS' => $this -> result);
			}else{$this -> result = array ('CODIGO'=> false, 'DATOS' => $this -> mensaje[0]);}
		} catch (Exception $e) {
			$this -> result = array ('CODIGO'=> false, 'DATOS' => $e);
		}
		return $this -> result;
	}






	public function getListado($id='')
	{
		$complete="";
		if ($id!='') {
			$complete = " WHERE IDUSUARIO=$id";
		}
		$this -> result = array ('CODIGO' => true, 'DATOS' => 'SE MOSTRARÁ LA CUIS');
		return $this -> result;
	}

	public function cancelAccount($id, $values='')
	{
		$this -> sql = "UPDATE INTEGRADOR.USUARIO SET STATUS = 0 WHERE IDUSUARIO = $id";
		try {
			$this -> result = $this -> conexion -> insertUpdate ($this -> sql);
			if ($this -> result) {
				$this -> result = array ('CODIGO' => $this -> result, "DATOS" => $this -> mensaje[1]);
			}else{
				$this -> result = array ('CODIGO' => false, "DATOS" => $this -> mensaje[3]);
			}
		} catch (Exception $e) {
			$this -> result = array ('CODIGO'=> false, 'DATOS' => $e);
		}
		return $this -> result;
	}


	public function upAccount($id,$values='')
	{
		$this -> sql = "UPDATE INTEGRADOR.USUARIO SET STATUS = 1 WHERE IDUSUARIO = $id";
		try {
			$this -> result = $this -> conexion -> insertUpdate ($this -> sql);
			if ($this -> result) {
				$this -> result = array ('CODIGO' => $this -> result, "DATOS" => $this -> mensaje[2]);
			}else{
				$this -> result = array ('CODIGO' => false, "DATOS" => $this -> mensaje[3]);
			}
		} catch (Exception $e) {
			$this -> result = array ('CODIGO'=> false, 'DATOS' => $e);
		}
		return $this -> result;
	}
	public function seeAccount($id,$values='')
	{
		$this -> result = $this -> getUser($id);
		return $this -> result;
	}






	public function putUser($id,$values)
	{
		//var_dump($values);
		$img_perfil= $values['imagen'];
		$tipo=$values['tipo'];
		$generales=$values['generales'];
		$amaterno=$generales[2]['value'];
		if ($amaterno==""|| $amaterno==null) {
			$amaterno='';
		}

		$tel_c=$generales[3]['value'];
		if ($tel_c==""|| $tel_c==null) {
			$tel_c='';
		}



		if($tipo=='registrar'){
			$idusuario = $this -> getS -> maxID('IDUSUARIO','INTEGRADOR.USUARIO')+1;
			$this -> sql = " INSERT INTO INTEGRADOR.USUARIO(IDUSUARIO,NOMBRE,APELLIDOPATERNO,APELLIDOMATERNO,TELEFONO_C,TELEFONO_T,EMAIL,PASS,PERFIL,ROL,STATUS,STATUS_R,UNIQUEID,FECHAINSERCION,IMG_PERFIL,VALIDACION)
										VALUES
										($idusuario,
										'".$generales[0]['value']."',
										'".$generales[1]['value']."',
										'".$amaterno."',
										'".$tel_c."',
										'".$generales[4]['value']."',
										'".$generales[5]['value']."',
										'".$generales[6]['value']."',
										".$generales[7]['value'].",
										".$generales[8]['value'].",
										1,1,
										'".$this -> uniqid."',DEFAULT,'".$img_perfil."', 0 )";
			$insertGeneral = $this -> conexion -> insertUpdate($this -> sql);

			$insertprogramas="";
			$insertaplicativos="";
			if ($insertGeneral) {
				if (isset($values['programas']) ||  isset($values['aplicativos'])) {
					if (isset($values['programas'])) {
						$programas=$values['programas'];
						for ($i=0; $i < count($programas); $i++) {
							$idUsuarioPrograma = $this -> getS -> maxID('IDUSUARIOPROGRAMA','INTEGRADOR.USUARIOPROGRAMA')+1;
							$this -> sql = " INSERT INTO INTEGRADOR.USUARIOPROGRAMA VALUES ($idUsuarioPrograma,$idusuario,".$programas[$i]['value'].",DEFAULT,DEFAULT)";
							$insertprogramas = $this -> conexion -> insertUpdate($this -> sql);
						}
					}
					if (isset($values['aplicativos'])) {
						$aplicativos=$values['aplicativos'];
						for ($i=0; $i < count($aplicativos); $i++) {
							$idUsuarioModulo = $this -> getS -> maxID('IDUSUARIOMODULO','INTEGRADOR.USUARIOMODULO')+1;
							$this -> sql = " INSERT INTO INTEGRADOR.USUARIOMODULO VALUES ($idUsuarioModulo,$idusuario,".$aplicativos[$i]['value'].",DEFAULT,DEFAULT)";
							$insertaplicativos = $this -> conexion -> insertUpdate($this -> sql);
						}
					}
					$result="OK";
				}else{
					$result="OK";
				}

			}else{
				$result= "NO";
			}
		} else if($tipo=='actualizar'){

			$idusuario =$values['idusuario'];
			$this -> sql =" UPDATE integrador.usuario SET NOMBRE='".$generales[0]['value']."',
									APELLIDOPATERNO='".$generales[1]['value']."',
									APELLIDOMATERNO='".$generales[2]['value']."',
									TELEFONO_C='".$generales[3]['value']."',
									TELEFONO_T='".$generales[4]['value']."',
									EMAIL='".$generales[5]['value']."',
									PASS='".$generales[6]['value']."',
									PERFIL='".$generales[7]['value']."',
									ROL='".$generales[8]['value']."',
									VALIDACION=1,
									IMG_PERFIL='".$img_perfil."' WHERE IDUSUARIO=".$idusuario;
									$updateGeneral = $this -> conexion -> insertUpdate($this -> sql);
									$updateprogramas="";
									$updateaplicativos="";


			if ($updateGeneral) {
				$this -> sql = "UPDATE INTEGRADOR.USUARIOPROGRAMA SET STATUS_PERFIL=0  where idusuario=".$idusuario;
				$updateprogramas = $this -> conexion -> insertUpdate($this -> sql);
				$this -> sql = "UPDATE INTEGRADOR.USUARIOMODULO SET STATUS_PERFIL=0  where idusuario=".$idusuario;
				$updateaplicativos = $this -> conexion -> insertUpdate($this -> sql);
				if (isset($values['programas']) ||  isset($values['aplicativos'])) {
					if (isset($values['programas'])) {
						$programas=$values['programas'];
						for ($i=0; $i < count($programas); $i++) {
							$this -> sql = "select * from integrador.usuarioprograma where idusuario=".$idusuario." and idprograma=".$programas[$i]['value'];
							$verifica = $this -> conexion -> getResults($this -> sql);
							if ($verifica) {
								$this -> sql = "UPDATE INTEGRADOR.USUARIOPROGRAMA SET STATUS_PERFIL=1  where idusuario=".$idusuario." AND IDPROGRAMA=".$programas[$i]['value'];
								$updateprogramas = $this -> conexion -> insertUpdate($this -> sql);
							}else{
								$idUsuarioPrograma = $this -> getS -> maxID('IDUSUARIOPROGRAMA','INTEGRADOR.USUARIOPROGRAMA')+1;
								$this -> sql = " INSERT INTO INTEGRADOR.USUARIOPROGRAMA VALUES ($idUsuarioPrograma,$idusuario,".$programas[$i]['value'].",1,DEFAULT)";
								$updateprogramas2 = $this -> conexion -> insertUpdate($this -> sql);
							}
						}
					}
					if (isset($values['aplicativos'])) {
						$aplicativos=$values['aplicativos'];
						for ($i=0; $i < count($aplicativos); $i++) {
							$this -> sql = "select * from integrador.usuariomodulo where idusuario=".$idusuario." and idmodulo=".$aplicativos[$i]['value'];
							$verifica = $this -> conexion -> getResults($this -> sql);

							if ($verifica) {
								$this -> sql = "UPDATE INTEGRADOR.USUARIOMODULO SET STATUS_PERFIL=1  where idusuario=".$idusuario." AND IDMODULO=".$aplicativos[$i]['value'];
								$updateaplicativos2 = $this -> conexion -> insertUpdate($this -> sql);
							}else{
								$idUsuarioModulo = $this -> getS -> maxID('IDUSUARIOMODULO','INTEGRADOR.USUARIOMODULO')+1;
								$this -> sql = " INSERT INTO INTEGRADOR.USUARIOMODULO VALUES ($idUsuarioModulo,$idusuario,".$aplicativos[$i]['value'].",DEFAULT,DEFAULT)";
								$updateaplicativos2 = $this -> conexion -> insertUpdate($this -> sql);
							}
						}
					}
					$result="OK";
				}else{
					$result="OK";
				}
			}else{
				$result= "NO";
			}
		}
		return $result;
	}







	public function deleteAccount($id,$values='')
	{
		$this -> sql = "DELETE FROM INTEGRADOR.USUARIO WHERE IDUSUARIO=$id";
		try {
			$this -> result = $this -> conexion -> insertUpdate($this -> sql);
			if ($this -> result) {
				$this -> result = array ('CODIGO' => $this -> result, 'DATOS' => $this -> mensaje[4]);
			}else{
				$this -> result = array ('CODIGO' => $this -> false, 'DATOS' => $this -> mensaje[3]);
			}
		} catch (Exception $e) {
			$this -> result = array ('CODIGO'=> false, 'DATOS' => $e);
		}
		return $this -> result;
	}

}
?>
