<?php

    class unirTablas_model{

        // public function getInfoTable($tableName){
        //     $conexion = new conexion;
        //     $query = "SELECT * FROM $tableName ";
        //     $results = $conexion->getResults($query, 50);
        //     return $results;
        // }

        // public function getInfoList(){
        //     $conexion = new conexion;
        //     $query = "SELECT $fields FROM $tableList1 WHERE $fieldName = '$fieldValue' ";
        //     $results = $conexion->getResults($query, 10);
        //     return $results;
        // }

        public function getInfoTable($tableName, $complement = ''){
            $conexion = new conexion;
            $query = "SELECT * FROM $conexion->esquema$tableName " .$complement;
            // print_r($query);
            $results = $conexion->getResults($query, 250);
            return $results;
        }

        public function addNewFieldToTab($firstField, $firstVal, $secondField, $secondVal, $tabName){
            $conexion = new conexion;
            $query = "INSERT INTO $conexion->esquema$tabName($firstField, $secondField, ESTATUS) VALUES($firstVal, $secondVal, 1)";
            // print_r($query);
            $result = $conexion->insertUpdate($query);
            return $result;
        }

        public function updateTableState($complement, $tabName, $newState){
            $conexion = new conexion;
            $query = "UPDATE $conexion->esquema$tabName SET ESTATUS = $newState WHERE " . $complement;
            $result = $conexion->insertUpdate($query);
            return $result;
        }

    }

?>