<?php

include './consult_ExpFonden.php';

    class pdf_ToImage{

        function convertImages($estado){
            $createImages = new Pdf();            
            $results = $createImages->saveAllPagesAsImages($estado);

            if(!empty($results)){
                $directory = 'http://75.126.28.84:9013/';
                foreach($results as $item){                                                         
                    print_r($item);
                    if( empty($item['CONSECUTIVO']) || empty($item['FOLIO']) || empty($item['TIPOEXP']) || empty($item['ESTADO']) )                        
                        return;                        
                    else{
                        $directory .= $item['ESTADO'] . '/' . $item['TIPOEXP'] . '/' . '00' . $item['CONSECUTIVO'];
                        if(!empty($item['TOMO'])) $directory .= '/' . $item['TOMO'];
                        // $directory .= '/' . $item['FOLIO'] . '.pdf';                    
                                                          
                        try{                                                            
                            $filePdf = $item['FOLIO'] . '.pdf';
        
                            $fileNameExt = $item['FOLIO'];
                            // if($fileNameExt[1] == 'pdf' || $fileNameExt[1] == 'PDF'){
                                // $fileStruct = './'. $fileNameExt[0] .'/';
                                $img = new imagick($directory . '/' . $filePdf);            
                                $noOfPagesInPDF = $img->getNumberImages(); 
                                $img->clear(); 
                                $img->destroy();
                                
                                if($noOfPagesInPDF){ 
                                    $image = new Imagick();                            
                                    // $image->setResolution(350,350);                         
                                    $image->readImage($directory . '/' . $filePdf);                            
                                    $image->setImageFormat('jpeg');                            
                                    $tempLocation = $fileNameExt . '.jpg';                            
                                    $image->writeImages($tempLocation, false);                                                                                                 
                                    $image->clear(); 
                                    $image->destroy();
        
                                    /* Directorio en donde se guardaran las imagenes de los pdf's*/
                                    $fileStruct = '/home/integrador/desarrollo_des/pillar/clases/img_tmp/EXPFONDEN/' . $item['ESTADO'] . '/' . $item['TIPOEXP'] . '/' . '00' . $item['CONSECUTIVO'] . '/';
                                    if(!empty($item['TOMO'])) $fileStruct .= $item['TOMO'] . '/';

                                    if(!mkdir($fileStruct . $fileNameExt, 0777, true)) {
                                        die('Failed to create folders...');
                                    }else{
                                        for ($i = 0; $i < $noOfPagesInPDF; $i++) {
                                            if(!copy($fileNameExt.'-'.$i.'.jpg', $fileStruct . $fileNameExt . '/' . $fileNameExt.'-'.($i+1).'.jpg')){
                                                echo 'No se ha copiado el archivo';
                                            }
                                            else{
                                                unlink($fileNameExt.'-'.$i.'.jpg');
                                            }                                                                           
                                        }
                                        if(!chmod($fileStruct . $fileNameExt, 0777)){
                                            echo 'Cambio de permisos fallido';
                                        } 
                                    }
                                    
                                    echo "All pages of PDF is converted to images";
                            
                                }
                                else{
                                    die('Ha ocurrido un problema con el pdf');
                                }    
                            // }
                        }
                        catch(ImagickException $e){
                            var_dump($e);
                        }
                    }
                }

            }
        }

        function convertDirPdfToImg($directory, $filePdf, $subDirGroup = ''){            
            // $ficheros = scandir($directory, $filePdf);
            
            // foreach($ficheros as $filePdf){
            //     if($filePdf != '.' && $filePdf != '..'){
                    $fileNameExt = explode('.', $filePdf);

                    if($fileNameExt[1] == 'pdf' || $fileNameExt[1] == 'PDF'){
                        // $fileStruct = './'. $fileNameExt[0] .'/';
                        $img = new imagick($directory . '/' . $filePdf);            
                        $noOfPagesInPDF = $img->getNumberImages(); 
                        $img->clear(); 
                        $img->destroy();
                        
                        if($noOfPagesInPDF){ 
                            $image = new Imagick();                            
                            $image->setResolution(350,350);                         
                            $image->readImage($directory . '/' . $filePdf);                            
                            $image->setImageFormat('jpeg');                            
                            $tempLocation = $fileNameExt[0] . '.jpg';                            
                            $image->writeImages($tempLocation, false);                                                                                                 
                            $image->clear(); 
                            $image->destroy();

                            /* Directorio en donde se guardaran las imagenes de los pdf's*/
                            $fileStruct = '/home/integrador/desarrollo_des/pillar/clases/img_tmp/' . $subDirGroup;
                            if(!mkdir($fileStruct . $fileNameExt[0], 0777, true)) {
                                die('Failed to create folders...');
                            }else{
                                for ($i = 0; $i < $noOfPagesInPDF; $i++) {
                                    if(!copy($fileNameExt[0].'-'.$i.'.jpg', $fileStruct . $fileNameExt[0] . '/' . $fileNameExt[0].'-'.($i+1).'.jpg')){
                                        echo 'No se ha copiado el archivo';
                                    }
                                    else{
                                        unlink($fileNameExt[0].'-'.$i.'.jpg');
                                    }                                                                           
                                }
                                if(!chmod($fileStruct . $fileNameExt[0], 0777)){
                                    echo 'Cambio de permisos fallido';
                                } 
                            }
                            
                            echo "All pages of PDF is converted to images";
                    
                        }
                        else{
                            die('Ha ocurrido un problema con el pdf');
                        }    
                    }
            //     }
            // }               
        }
    }
    
    $option = $_GET['option'];

    $pdf_ToImage = new pdf_ToImage;    
    
    if($option == 'convertDir'){
        if(!empty($_GET['dirPdfs']) && !empty($_GET['filePdf']))
            return json_encode($pdf_ToImage->convertDirPdfToImg(urldecode($_GET['dirPdfs']), urldecode($_GET['filePdf'])));
    }
    else if($option == 'consultFiles'){
        if(!empty($_GET['estado']))
            return json_encode($pdf_ToImage->convertImages(urldecode($_GET['estado'])));
    }

?>