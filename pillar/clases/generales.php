<?php
/**
*
*/
include_once 'conexion.php';
include_once 'utilities.php';
class Generales
{
	protected $sql;
	protected $result;
	protected $conexion;
	protected $mensaje;
	protected $utilities;
	function __construct()
	{
		$this -> result = null;
		$this -> conexion = new conexion;
		$this -> utilities = new utilities;
		$this -> mensaje = ['SIN DATOS',
							'EXITO! SE HA CANCELADO ESTA CUENTA.','EXITO! SE HA ACTIVO ESTA CUENTA.',
							'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO'] ;
	}

	public function getProfiles($id='',$values='')
	{
		$this -> sql = "SELECT * FROM INTEGRADOR.TIPOUSUARIO where idtipoUsuario not in (10,4) ORDER BY TIPOUSUARIO";
		$this -> result = $this -> conexion -> getResults($this -> sql);
		return $this -> result = array ('CODIGO' => true, 'DATOS' => $this -> result);
	}


	public function getProfilesAll($values,$idPrograma)
	{

		$this -> sql = "SELECT * FROM INTEGRADOR.TIPOUSUARIO where idtipoUsuario  in (8,3,11) ORDER BY TIPOUSUARIO";
		$this -> result = $this -> conexion -> getResults($this -> sql);
		return $this -> result = array ('CODIGO' => true, 'DATOS' => $this -> result);
	}


	public function comboMain($idPrograma='')
	{
		$this -> sql = "SELECT CVEEDO,NOMBREENT FROM (
							".$this->QueryPadron()."
						)
						WHERE IDPROGRAMA=$idPrograma AND NOMBREENT IS NOT NULL
						GROUP BY CVEEDO,NOMBREENT
						ORDER BY NOMBREENT";
		$estados = $this -> conexion -> getResults($this -> sql);

		/*$this -> sql ="SELECT CVEEDO,CVEMUN,NOMBREMUN FROM (
							".$this->QueryPadron()."
						)
						WHERE IDPROGRAMA=$idPrograma
						GROUP BY CVEEDO,CVEMUN,NOMBREMUN
						ORDER BY NOMBREMUN";

		$municipios = $this -> conexion -> getResults($this -> sql);
		$mun = $this -> utilities -> objetivisa('CVEEDO',$municipios);

		$this -> sql= "SELECT CVEMUN,CVELOC,NOMBRELOC FROM (
							".$this->QueryPadron()."
						)
						WHERE IDPROGRAMA=$idPrograma
						GROUP BY CVEMUN,CVELOC,NOMBRELOC
						ORDER BY NOMBRELOC";
		$localidades = $this -> conexion -> getResults($this -> sql);
		$loc = $this -> utilities -> objetivisa('CVEMUN',$localidades);*/

		$this -> sql="SELECT ID_DEL,DELEGACION FROM (
							".$this->QueryPadron()."
						)
						WHERE IDPROGRAMA=$idPrograma AND ID_DEL <> 0
						GROUP BY ID_DEL,DELEGACION
						ORDER BY DELEGACION";
		$delegaciones = $this -> conexion -> getResults($this -> sql);

		/*$this -> sql="SELECT ID_DEL,ID_SUB,SUBDELEGACION FROM (
							".$this->QueryPadron()."
						)
						WHERE IDPROGRAMA=$idPrograma
						GROUP BY ID_DEL,ID_SUB,SUBDELEGACION
						ORDER BY SUBDELEGACION";
		$subdel = $this -> conexion -> getResults($this -> sql);
		$subd = $this -> utilities -> objetivisa('ID_DEL',$subdel);*/

		return $this -> result = array ('CODIGO' => true, 'DATOS' => array('estados' => $estados,
																			'municipios' => '',//$mun,
																			'localidades' => '',//$loc,
																			'delegaciones' =>  $delegaciones,
																			'subdelegaciones' => '')//$subd)
										);
	}
	public function comboMainSubitem($item,$key,$idPrograma)
	{
		if ($item=='delegacion') {
			$this -> sql = "SELECT L.DELEGACION ID_DEL,L.SUBDELEGACION ID_SUB,DS.SUBDELEGACION
							FROM INTEGRADOR.PADRON P
								JOIN INTEGRADOR.LISTADO L ON L.IDLISTADO=P.IDLISTADO AND L.DELEGACION=$key
								JOIN INTEGRADOR.DELEGACIONSUBDELEGACION DS ON DS.ID_SUB=L.SUBDELEGACION
							WHERE P.IDPROGRAMA=$idPrograma
							GROUP BY L.DELEGACION,L.SUBDELEGACION,DS.SUBDELEGACION
							ORDER BY L.SUBDELEGACION";
		}elseif ($item=='estado') {
			$this -> sql = "SELECT L.CVEEDO, L.CVEMUN,EML.NOMBREMUN
							FROM INTEGRADOR.LISTADO L
								JOIN INTEGRADOR.PADRON P ON P.IDLISTADO= L.IDLISTADO
								JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON EML.CVEMUN=L.CVEMUN AND L.CVEEDO=$key
							WHERE P.IDPROGRAMA=$idPrograma
							GROUP BY L.CVEEDO, L.CVEMUN,EML.NOMBREMUN
							ORDER BY EML.NOMBREMUN";
		}elseif ($item=='municipio') {
			$this -> sql = "SELECT L.CVEMUN,L.CVELOC,EML.NOMBRELOC
							FROM INTEGRADOR.LISTADO L
								JOIN INTEGRADOR.PADRON P ON P.IDLISTADO= L.IDLISTADO
								JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON EML.CVELOC=L.CVELOC AND L.CVEMUN=$key
							WHERE P.IDPROGRAMA=$idPrograma
							GROUP BY L.CVEMUN,L.CVELOC,EML.NOMBRELOC
							ORDER BY NOMBRELOC";
		}

		try {
			$sub_items = $this-> conexion -> getResults($this -> sql);
			if ($sub_items!=null) {
				$this -> result = array ('CODIGO' => true, 'DATOS' => $sub_items);
			}else{
				$this -> result = array ('CODIGO' => false, 'DATOS' => 'SIN DATOS');
			}
		} catch (Exception $e) {
			$this -> result = array ('CODIGO' => false, 'DATOS' => $e);
		}
		return $this -> result;
	}


	public function getNamePrograma($idPrograma='')
	{
		$this -> sql = "SELECT * FROM INTEGRADOR.PROGRAMA
						WHERE IDPROGRAMA=$idPrograma";
		$this -> result = $this -> conexion -> getResults($this -> sql);
		if ($this -> result != null) {
			$this -> result = array('CODIGO' => true,'DATOS' => $this -> result);
		}else{
			$this -> result = array('CODIGO' => false,'DATOS' => $this -> mensaje[0]);
		}
		return $this -> result;
	}
	public function QueryPadron()
	{
		$this -> sql= "SELECT
      							P.IDPADRON,P.IDPROGRAMA,P.ID_ANT,P.IDLISTADO,P.FECHAINSERCION,P.FECHAMODIFICACION,
                    			P.DISTRIBUCION_OPERATIVA,P.ESTATUS, P.IDTIPOEVENTO,PR.NOMBREPROGRAMA,TE.TIPOEVENTO,
      							L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,(L.NOMBRE||' '||L.APATERNO||' '||L.AMATERNO)BENEFICIARIO,
                    			L.ORIGEN,L.DELEGACION ID_DEL,SD.DELEGACION,L.SUBDELEGACION  ID_SUB,SD.SUBDELEGACION,
      							L.CVEEDO,EML.NOMBREENT,EML.CVEMUN,EML.NOMBREMUN,EML.CVELOC,EML.NOMBRELOC
							FROM INTEGRADOR.PADRON P
                				JOIN INTEGRADOR.PROGRAMA PR ON (PR.IDPROGRAMA=P.IDPROGRAMA)
								JOIN INTEGRADOR.TIPOEVENTO TE ON (TE.IDTIPOEVENTO=P.IDTIPOEVENTO)
								JOIN INTEGRADOR.LISTADO L ON (L.IDLISTADO=P.IDLISTADO)
                				LEFT JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON (L.CVELOC=EML.CVELOC)
                				JOIN INTEGRADOR.DELEGACIONSUBDELEGACION SD ON (SD.ID_MUN=L.CVEMUN)";
      	return $this -> sql;
	}

	public function tipoEvento($x='')
	{
		$this -> sql = "SELECT * FROM INTEGRADOR.TIPOEVENTO ORDER BY TIPOEVENTO";
		$eventos = $this -> conexion -> getResults($this -> sql);

		$eventosObj= $this -> utilities -> objetivisa('IDTIPOEVENTO',$eventos);

		$this -> result = array ('eventos' => $eventos, 'tipo'=> $eventosObj);

		if ($this -> result != null) {
			$this -> result = array('CODIGO' => true,'DATOS' => $this -> result);
		}else{
			$this -> result = array('CODIGO' => false,'DATOS' => $this -> mensaje[0]);
		}
		return $this -> result;
	}

	public function tipoFase($x='')
	{
		$this -> sql = "SELECT * FROM INTEGRADOR.FASE ORDER BY FASE";
		$fases = $this -> conexion -> getResults($this -> sql);
		$fasesObj = $this -> utilities -> objetivisa('IDFASE', $fases);
		$this -> result = array('fases'=> $fases, 'tipo'=> $fasesObj);
		if ($this -> result != null) {
			$this -> result = array('CODIGO' => true,'DATOS' => $this -> result);
		}else{
			$this -> result = array('CODIGO' => false,'DATOS' => $this -> mensaje[0]);
		}
		return $this -> result;
	}
	public function tipoEtiqueta($x='')
	{
		$this -> sql = "SELECT * FROM INTEGRADOR.ETIQUETA ORDER BY ETIQUETA";
		$etiquetas = $this -> utilities -> objetivisa('IDETIQUETA',$this-> conexion -> getResults($this -> sql));

		$etiquetaGrupo= $this -> utilities -> objetivisa( 'GRUPO', $this -> conexion -> getResults($this -> sql) );


		$this -> sql = "SELECT GRUPO FROM INTEGRADOR.ETIQUETA GROUP BY GRUPO ORDER BY GRUPO";
		$grupo = $this -> conexion -> getResults($this -> sql);

		$this -> result = array ('grupo' => $grupo,'etiquetas'=> $etiquetaGrupo,'tipo'=> $etiquetas);

		if ($this -> result != null) {
			$this -> result = array('CODIGO' => true,'DATOS' => $this -> result);
		}else{
			$this -> result = array('CODIGO' => false,'DATOS' => $this -> mensaje[0]);
		}
		return $this -> result;
	}




/////////////////////////////////
	public function infoPDF($idPrograma='')
	{

	   $comquery=" FROM integrador.evento E
		            JOIN INTEGRADOR.LISTADO L ON (E.IDLISTADO=L.IDLISTADO)
		            JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON (L.CVELOC=EML.CVELOC)
		            WHERE IDPROGRAMA=3
                    ORDER BY 2
		            ";


	 	$this -> sql ="SELECT DISTINCT(L.CVEEDO) CVEEDO, UPPER(EML.NOMBREMUN) NOMBREMUN, L.CVEMUN CVEMUN $comquery";
	 	$mun = $this -> conexion -> getResults($this -> sql);
	    $municipios = $this -> utilities -> objetivisa('CVEEDO',$mun);




	 	return $this -> result = array ('CODIGO' => true, 'DATOS' => array(
	 																		'municipios' => $municipios
	 															     		)

								     	);
	 }



	public function infoAnosVisitas($idPrograma='', $values='')
		{
			$extra=$values['vs_cveMun'];
		 	$this -> sql ="SELECT L.CVEMUN, to_char(fechavisita, 'YYYY') NOMBRE, to_char(fechavisita, 'YYYY') VALOR
								FROM integrador.evento E
								JOIN INTEGRADOR.LISTADO L ON (E.IDLISTADO=L.IDLISTADO) and cvemun=$extra
								JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON (L.CVELOC=EML.CVELOC)
								WHERE IDPROGRAMA=$idPrograma
								GROUP BY  L.CVEMUN, to_char(fechavisita, 'YYYY'), to_char(fechavisita, 'YYYY')
								ORDER BY 2";
	 		$anos = $this -> conexion -> getResults($this -> sql);
	        $anosVisita = $this -> utilities -> objetivisa('CVEMUN',$anos);

		 	return $this -> result = array ('CODIGO' => true, 'DATOS' => array(	'anosVisita' => $anosVisita));
		}




	public function infoMesesVisitas($idPrograma='', $values='')
		{
				$cveMun=$values['vs_cveMun'];
				$ano=$values['vs_ano'];

			 	$this -> sql ="SELECT to_char(fechavisita, 'YYYY') ANO, to_char(fechavisita, 'MM') VALOR,
									CASE to_char(fechavisita, 'MM')
									WHEN '01' THEN 'ENERO'
									WHEN '02' THEN 'FEBRERO'
									WHEN '03' THEN 'MARZO'
									WHEN '04' THEN 'ABRIL'
									WHEN '05' THEN 'MAYO'
									WHEN '06' THEN 'JUNIO'
									WHEN '07' THEN 'JULIO'
									WHEN '08' THEN 'AGOSTO'
									WHEN '09' THEN 'SEPTIEMBRE'
									WHEN '10' THEN 'OCTUBRE'
									WHEN '11' THEN 'NOVIEMBRE'
									WHEN '12' THEN 'DICIEMBRE'  END NOMBRE
									FROM integrador.evento E
									JOIN INTEGRADOR.LISTADO L ON (E.IDLISTADO=L.IDLISTADO) and cvemun=$cveMun
									JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON (L.CVELOC=EML.CVELOC)
									WHERE IDPROGRAMA=$idPrograma AND to_char(fechavisita, 'YYYY')=$ano
									GROUP BY   to_char(fechavisita, 'YYYY'), to_char(fechavisita, 'MM')
									ORDER BY 2";
		 		$mes = $this -> conexion -> getResults($this -> sql);
		        $mesVisita = $this -> utilities -> objetivisa('ANO',$mes);

			 	return $this -> result = array ('CODIGO' => true, 'DATOS' => array(	'mesVisita' => $mesVisita));
		 }


	public function infoEntregasVisitas($idPrograma='')
		{
			$entrega[0]['NOMBRE'] = "01"; $entrega[0]['VALOR'] = "01";
			$entrega[1]['NOMBRE'] = "02"; $entrega[1]['VALOR'] = "02";
			$entrega[2]['NOMBRE'] = "03"; $entrega[2]['VALOR'] = "03";
			$entrega[3]['NOMBRE'] = "04"; $entrega[3]['VALOR'] = "04";


		 	return $this -> result = array ('CODIGO' => true, 'DATOS' => array('entrega'=> $entrega));
		 }






	 function getPestañas($idPrograma){
		$query = "SELECT MODALIDAD FROM ".$this -> conexion -> esquema."PROGRAMA WHERE IDPROGRAMA = $idPrograma";
		$mod = $this -> conexion -> getResult($query)['MODALIDAD'];

		if ($mod=="1") {
			$query = "SELECT IDPROGRAMA,PTE.IDTIPOEVENTO,TIPOEVENTO FROM INTEGRADOR.PROGRAMATIPOEVENTO PTE
						JOIN INTEGRADOR.TIPOEVENTO TE ON PTE.IDTIPOEVENTO=TE.IDTIPOEVENTO
					WHERE IDPROGRAMA=$idPrograma";
		}
		if ($mod=="2") {
			$query= "SELECT FP.IDFASE IDTIPOEVENTO,F.FASE TIPOEVENTO,PTE.IDPROGRAMA FROM INTEGRADOR.FASEPROGRAMA FP
						JOIN INTEGRADOR.FASE F ON FP.IDFASE=F.IDFASE
						JOIN INTEGRADOR.PROGRAMATIPOEVENTO PTE ON FP.IDPROGRAMATIPOEVENTO= PTE.IDPROGRAMATIPOEVENTO AND PTE.IDPROGRAMA=$idPrograma
					GROUP BY FP.IDFASE,F.FASE,PTE.IDPROGRAMA";
		}
		return array('CODIGO' => true , 'DATOS' =>  $this -> conexion -> getResults($query) );
	}








}



?>
