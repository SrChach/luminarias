<?php 
/**
* 
*/
include_once 'conexion.php';
include_once 'utilities.php';
class GeneralesIntegra
{
	protected $sql;
	protected $result;
	protected $conexion;
	protected $mensaje;
	protected $utilities;
	function __construct()
	{
		$this -> result = null;
		$this -> conexion = new conexion;
		$this -> utilities = new utilities;
		$this -> mensaje = ['SIN DATOS',
							'EXITO! SE HA CANCELADO ESTA CUENTA.','EXITO! SE HA ACTIVO ESTA CUENTA.',
							'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO'] ;
	}


	public function traeDelegacion($idPrograma='')
	{
		$this -> sql = "SELECT L.DELEGACION CVEDEL, DS.DELEGACION NOMBREDEL
			                    FROM INTEGRADOR.LISTADO L
			                    JOIN INTEGRADOR.DELEGACIONSUBDELEGACION  DS ON (L.DELEGACION=DS.ID_DEL)
			                    GROUP BY L.DELEGACION, DS.DELEGACION
			                    ORDER BY DS.DELEGACION";
		$delegacion = $this -> conexion -> getResults($this -> sql);
		return $this -> result = array ('CODIGO' => true, 'DATOS' => array('delegacion' => $delegacion));
	}



	public function traeSubdelegacion($idPrograma='',$values='')
	{
		$cveDel=$values['cveDel'];
		$this -> sql = "SELECT  L.SUBDELEGACION CVESUBDEL, DS.SUBDELEGACION
			                    FROM INTEGRADOR.LISTADO L
			                    JOIN INTEGRADOR.DELEGACIONSUBDELEGACION  DS ON (L.SUBDELEGACION=DS.ID_SUB)
                                WHERE L.DELEGACION=$cveDel
			                    GROUP BY L.SUBDELEGACION, DS.SUBDELEGACION
			                    ORDER BY  DS.SUBDELEGACION";
		$subdelegacion = $this -> conexion -> getResults($this -> sql);


		return $this -> result = array ('CODIGO' => true, 'DATOS' => array('subdelegacion' => $subdelegacion
																			)
										);
	}









}




?>