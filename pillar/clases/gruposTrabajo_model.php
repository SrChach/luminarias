<?php
    class gruposTrabajo_model{

        function getWorkGroups(){
            $conexion = new conexion;
            $query = "SELECT * FROM INTEGRADOR.GRUPOTRABAJO WHERE ESTATUS = 1";
            $result = $conexion->getResults($query);
            return $result;
        }

        function getTypeImg(){
            $conexion = new conexion;
            $query = "SELECT DISTINCT(TIPOIMAGEN), IDTIPOIMAGEN FROM INTEGRADOR.TIPOIMAGEN";
            $results = $conexion->getResults($query);
            return $results;
        }

        function getUsersOperative(){
            $conexion = new conexion;
            $query = "SELECT USR.NOMBRE, USR.IDUSUARIO, GPT.GRUPO FROM INTEGRADOR.USUARIO USR LEFT JOIN INTEGRADOR.GRUPOTRABAJO GPT ON USR.IDGRUPOTRABAJO = GPT.IDGRUPO WHERE USR.PERFIL = 11";
            $results = $conexion->getResults($query);
            return $results;
        }

        function insertWorkGroup($nameGroup, $origin, $typeImg){
            $conexion = new conexion;
            $query = "INSERT INTO INTEGRADOR.GRUPOTRABAJO(IDGRUPO, ESTATUS, GRUPO, ORIGEN, TIPOSIMAGEN)
                VALUES(
                    (SELECT NVL(MAX(IDGRUPO)+1, 1) IDGRUPO FROM INTEGRADOR.GRUPOTRABAJO),
                    1,
                    '$nameGroup',
                    '$origin',
                    '$typeImg'
                )";
            $result = $conexion->insertUpdate($query);
            return $result;
        }

        function deleteWorkGroup($keyGroup){
            $conexion = new conexion;
            $query = "UPDATE INTEGRADOR.GRUPOTRABAJO SET ESTATUS = 0 WHERE IDGRUPO = '$keyGroup'";
            $result = $conexion->insertUpdate($query);
            return $result;
        }

        function asignGroupToUsers($keyWorkGroup, $objectData){
            $conexion = new conexion;
            $failUpdate = 0;
            foreach($objectData as $item){
                $query = "UPDATE INTEGRADOR.USUARIO SET IDGRUPOTRABAJO = '$keyWorkGroup' WHERE IDUSUARIO = '$item' ";
                $result = $conexion->insertUpdate($query,0);
                if($result == false)
                    $failUpdate++;
            }

            if($failUpdate == 0){
                return true;
            }else{
                return false;
            }

        }
    }
?>