<?php

ini_set('memory_limit', -1);
ignore_user_abort(1); 
ini_set('max_execution_time', 0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 






include_once './controlador.php';
include_once './utilities.php';
include_once './QueryController.php';
include_once './mailReportes.php';
include_once './exporta_reportesMail.php';
include_once '../../../parametros.php'; 
include_once 'ErrorController.php';
include_once '../../public/portal/libraries/phpmailer-master/class.phpmailer.php';


class ReportsController extends controlador
{
	protected $result;
	protected $model;
  protected $modelCsv;
  protected $enviaMail;


  public function Reportes($function,$params)
  {	

    $this -> model = new QueryReports;
    $query = $this -> model -> $function($params);
    $idUsuarioLogin=$params['i'];
    $fecha=strftime( "%Y%m%d%H%M%S", time() );

    /*Reporte Avance-Canastas*/
    $parametros= new parametros;
    if ($function=='Avances') {
      $nombreReporte='Avance_'.$params['nombreRep']."_".$fecha;
      $nombre=$nombreReporte.'.csv';
      $rutaGuardaReporte='../../../reportes/'.$nombreReporte;
      $rutaDescarga=$parametros -> urlReportes().$nombreReporte.'.csv';
      //$titulos=" IDEVENTO, ID_OFF, OFICINA_REGIONAL, ID_OF_SUB, OFICINA_SUBREGIONAL, CVEEDO, NOMBREENT, CVEMUN, NOMBREMUN, CVELOC, NOMBRELOC, LATITUD, LONGITUD, IDPADRON, ID_PROGRAMA, PROGRAMA, ID_EVENTO, EVENTO, IDLISTADO, FOLIOCUIS, C_PERSONA, FOLIOPDA, FECHAVISITA, FECHA_INI, FECHA_TERMINO, IDUSUARIOSINCRONIZACION, IMEI, OBSERVACION, VALIDACION, IDUSUARIOVALIDACION, OBSERVACIONVALIDACION, ID_CODIGO_RESULTADO, CODIGO_RESULTADO, AVANCE, DISTRIBUCION_OPERATIVA, ESTATUS, NOMBRE, APATERNO, AMATERNO, GENERO, PARENTESCO, C_INTEGRANTE";

    }



    if ($function=='ReportPogramaEvento') {
      $evento=$params['eventoG'];
      if ($evento==1) {
        $nombreEvento='';
      }


      switch ($evento) {
        case 1:
        $nombreEvento='PISO';
        break;
        case 2:
        $nombreEvento='TECHO';
        break;
        case 3:
        $nombreEvento='CUARTO';
        break;
        case 4:
        $nombreEvento='BAÑO';
        break;
        default:
        $nombreEvento='NO ESPECIFICADO';
      }

      $nombreReporte='ReportePrograma_'.$nombreEvento."_".$fecha;
      $nombre=$nombreReporte.'.csv';
      $rutaGuardaReporte='../../../reportes/'.$nombreReporte;
      $rutaDescarga=$parametros -> urlReportes().$nombreReporte.'.csv';
      //$titulos=" IDPADRON, IDPROGRAMA, IDLISTADO, FECHAINSERCION, FECHAMODIFICACION, DISTRIBUCION_OPERATIVA, ESTATUS, IDTIPOEVENTO, NOMBREPROGRAMA, TIPOEVENTO, FOLIOCUIS, NOMBRE, APATERNO, AMATERNO, BENEFICIARIO, ORIGEN, DELEGACION, SUBDELEGACION, CVEEDO, NOMBREENT, CVEMUN, NOMBREMUN, CVELOC, NOMBRELOC";

    }



    $this -> modelCsv = new Reports;
    $respuestaGenera = $this -> modelCsv -> csv($query,$rutaGuardaReporte,$nombre,$idUsuarioLogin);







    return ;
  }




  public function traeReportes($opcion,$values){
    $this -> model = new QueryReports;
    $function="query_".$opcion;
    $query = $this -> model ->$function($values);
    return $query;
  }

}
















if( isset( $_POST['opcion'] ) ){
  $opcion = $_POST['opcion'];  
  if (isset( $_POST['values'] )) {
    $values = $_POST['values'];  
  }  
}



$reportes = new ReportsController;

if($opcion == "generaReporte" || $opcion == "ReportPogramaEvento"  || $opcion == "repoPanelEjecutivo"){
  $params = ($_POST['params'] != "") ? $_POST['params']:"0";
  $function = ($_POST['function'] != "") ? $_POST['function']:"0";
  echo json_encode($reportes-> Reportes($function,$params) ); 
}

else if ($opcion == "traeReportes") {
  echo json_encode($reportes-> traeReportes($opcion,$values) ); 
}








?>