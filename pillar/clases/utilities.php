<?php

class utilities{
	
	function fecha($fecha,$anio = "NO"){
       //Recibe una fecha con formato YYYY-MM-DD y la devuelve como cadena tipo 12 de Marzo de YYYY
        $comps = explode("-", $fecha);
        $mes = "";
        switch ($comps[1]) {
            case 1:$mes = "Enero";break;
            case 2:$mes = "Febrero";break;
            case 3:$mes = "Marzo";break;
            case 4:$mes = "Abril";break;
            case 5:$mes = "Mayo";break;
            case 6:$mes = "Junio";break;
            case 7:$mes = "Julio";break;
            case 8:$mes = "Agosto";break;
            case 9:$mes = "Septiembre";break;
            case 10:$mes = "Octubre";break;
            case 11:$mes = "Noviembre";break;
            case 12:$mes = "Diciembre";break;
        }
        if($anio == "NO"){
            $fechaTexto = $comps[2] . " de " . $mes;    
        }elseif($anio == "SI"){
            $fechaTexto = $comps[2] . " de " . $mes . " del " . $comps[0];  
        }
        return $fechaTexto;
    }


    function quitaSaltosDeLinea(){
        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r","?");
        $reemplazar=array("", "", "", "","");
        $query = "select * from CUIS_ENCUESTA where FOLIO_CUIS = " .str_ireplace($buscar,$reemplazar, trim( utf8_decode ( $folio_cuis ) ) ) ;
    }

    public function trim($string){
        $order=array(" ", " ",'"');
        $replace = '';
        $newString=str_replace($order, $replace, $string);
        return $newString;
    }

    function objetivisa($key,$datos){
        $regresa = [];
        $i = 0;
        $keyact = "";
        if($datos != null){
            foreach ($datos as $d) {
                if($keyact == $d[$key]){
                    $regresa[$d[$key]][$i] = $d;
                    $i++;
                }
                else{
                    $i = 0;
                    $regresa[$d[$key]][$i] = $d;
                    $i++;
                    $keyact = $d[$key];
                }
            }

        }
        
        return $regresa;
    }

    function serializa($key,$datos){
        $serial = "";
        foreach ($datos as $d) {
            if($serial != ""){
                $serial = $serial.",".$d[$key];
            }else{
                $serial = $d[$key];
            }
        }
        return $serial;
    }

    function hash($data){
        $hash1 = str_pad($data, 50, "0", STR_PAD_LEFT);
        echo  "<br><br><br>" . $hash1 . "<br>";
        $hash2 = dechex("2018".$hash1);
        echo $hash2 . "<br>";
        $hash3 = base64_encode($hash2);
        echo $hash3 . "<br>";
    }
}








?>