<?php   
    class lotesImpresion_model{
        
        function getAllLotsImpresion(){
            $conexion = new conexion;
            $query = "SELECT LIMP.*, NVL(TOTAL, 0)TOTAL, NVL(IMPRESIONES, 0) IMPRESIONES, NVL(REIMPRESIONES, 0) REIMPRESIONES 
                        FROM INTEGRADOR.LOTEIMPRESION LIMP
                        LEFT JOIN (
                            SELECT IDLOTEIMPRESION, COUNT(*) TOTAL,
                                SUM(CASE WHEN TIPOIMPRESION = 1 THEN 1 ELSE 0 END) IMPRESIONES,
                                SUM(CASE WHEN TIPOIMPRESION = 2 THEN 1 ELSE 0 END) REIMPRESIONES
                            FROM INTEGRADOR.DETALLELOTEIMPRESION GROUP BY IDLOTEIMPRESION) RESUMENLOTEIMPRESION
                            ON LIMP.IDLOTEIMPRESION = RESUMENLOTEIMPRESION.IDLOTEIMPRESION ORDER BY LIMP.IDLOTEIMPRESION DESC";

            $results = $conexion->getResults($query);
            return $results;
        }

        function getLoteActual(){
            $conexion = new conexion;
            $query = "SELECT LIMP.*, NVL(TOTAL, 0) TOTAL, NVL(IMPRESIONES, 0) IMPRESIONES, NVL(REIMPRESIONES, 0) REIMPRESIONES
                        FROM INTEGRADOR.LOTEIMPRESION LIMP
                        LEFT JOIN(
                            SELECT IDLOTEIMPRESION, COUNT(*) TOTAL,
                                SUM(CASE WHEN TIPOIMPRESION = 1 THEN 1 ELSE 0 END) IMPRESIONES,
                                SUM(CASE WHEN TIPOIMPRESION = 2 THEN 1 ELSE 0 END) REIMPRESIONES
                            FROM INTEGRADOR.DETALLELOTEIMPRESION GROUP BY IDLOTEIMPRESION
                        ) RESUMENLOTE ON LIMP.IDLOTEIMPRESION = RESUMENLOTE.IDLOTEIMPRESION WHERE LIMP.FECHACIERRE IS NULL ORDER BY LIMP.IDLOTEIMPRESION DESC";
            $result = $conexion->getResult($query);
            return $result;
        }

        function closeLotActive($cveLoteAct){
            $conexion = new conexion;
            $query = "UPDATE INTEGRADOR.LOTEIMPRESION SET FECHACIERRE = SYSDATE WHERE IDLOTEIMPRESION = '$cveLoteAct' ";
            $result = $conexion->insertUpdate($query);

            if($result){
                $insertQuery = "INSERT INTO INTEGRADOR.LOTEIMPRESION(IDLOTEIMPRESION,FECHAAPERTURA)
                    VALUES(
                        (SELECT MAX(IDLOTEIMPRESION)+1 FROM INTEGRADOR.LOTEIMPRESION),
                        SYSDATE
                    )";
                $result = $conexion->insertUpdate($insertQuery);
            }
            return $result;
        }

        function printLote($cveLoteImp){
            // $conexion = new conexion;
            $query = "SELECT
                            e.C_ENCUESTA ,
                            /*inf.ORDEN,*/
                            /*v.CONSECUTIVO,*/
                            e.FOLIO_CUIS,
                        L.DELEGACION||LPAD(L.SUBDELEGACION,2,'0') CLAVE_DEL_SUB,
                        L.DELEGACION CLAVE_DEL,
                        L.SUBDELEGACION CLAVE_SUB,
                        
                        
                        DS.DELEGACION,
                        DS.SUBDELEGACION,
                        EML.NOMBREENT  ESTADO,EML.NOMBREMUN NOM_MUNICIPIO,
                        L.CVELOC CVE_LOCALIDAD,
                        SUBSTR (L.CVELOC,1,2) cve_estado,
                        SUBSTR (L.CVELOC,3,3) cve_mun,
                        SUBSTR (L.CVELOC,6,4) cve_loc,
                        EML.NOMBRELOC NOM_LOCALIDAD,
                            CASE
                                WHEN DECODE(pConyuge.NB_NOMBRE,NULL,0,1) = 0 AND DECODE(phijo.NB_NOMBRE,NULL,0,1) = 0  THEN 0
                                WHEN DECODE(pConyuge.NB_NOMBRE,NULL,0,1)  = 1  THEN 1
                                WHEN DECODE(pConyuge.NB_NOMBRE,NULL,0,1)  = 0 AND DECODE(phijo.NB_NOMBRE,NULL,0,1) = 1   THEN 2
                            END CODIFICACION,
                        p.NB_NOMBRE nombreJefe,
                        p.NB_PRIMER_AP primerApJefe,
                        p.NB_SEGUNDO_AP segundoApJefe,
                        pConyuge.NB_NOMBRE nombreConyuge,
                        pConyuge.NB_PRIMER_AP primerConyuge,
                        pConyuge.NB_SEGUNDO_AP segundoConyuge,
                        phijo.NB_NOMBRE nombreHijo1,
                        phijo.NB_PRIMER_AP primerApHijo1,
                        phijo.NB_SEGUNDO_AP segundoApHijo1,
                        phijo2.NB_NOMBRE nombreHijo2,
                        phijo2.NB_PRIMER_AP primerApHijo2,
                        phijo2.NB_SEGUNDO_AP segundoApHijo2,
                        fotos.FIRMA,
                        fotos.HUELLA,
                        fotos.JEFE FOTO,
                        /*DECODE(inf.SP,1,'SP',0,'',NULL,'','') SP,*/
                        /*DECODE(STATUS,1,'CORRECTO',2,'PENDIENTE',null,'VALIDACION','OTRO')ESTATUS,*/
                        /*DECODE(CRITERIO,0,'',1,'Fotos ilegibles',2,'Sin fotos',3,'Foto Cruzada con el nombre',4,'Duplicado',5,'Correcto',6,'Correcto Obscuro') motivo,*/
                            OBSERVACION,
                            DLI.FECHASOLICUTUD,email USUARIO_VALIDACION
                            /*,inf.dia*/
                        FROM INTEGRADOR.DETALLELOTEIMPRESION DLI
                        JOIN INTEGRADOR.CREDENCIAL C ON DLI.FOLIOCUIS = C.FOLIOCUIS
                        JOIN INTEGRADOR.LISTADO L ON DLI.FOLIOCUIS = L.FOLIOCUIS AND L.PARENTESCO = 1
                        JOIN INTEGRADOR.DELEGACIONSUBDELEGACION DS ON L.CVEMUN = DS.ID_MUN
                        JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON L.CVELOC = EML.CVELOC
                        JOIN SEDEVER.CUIS_ENCUESTA e ON  DLI.FOLIOCUIS = e.FOLIO_CUIS AND e.ENVIADO = 0
                        /*JOIN INFO inf on inf.FOLIO_CUIS = e.FOLIO_CUIS*/
                        JOIN integrador.USUARIO u ON C.V_USUARIO = u.IDUSUARIO
                        JOIN SEDEVER.CUIS_VIVIENDA  vi ON vi.C_ENCUESTA = E.C_ENCUESTA
                        /*JOIN CUIS_DOMICILIO d ON vi.C_VIVIENDA = d.C_VIVIENDA*/
                        JOIN SEDEVER.CUIS_INTEGRANTE i ON vi.C_VIVIENDA = i.C_VIVIENDA AND i.C_CD_PARENTESCO = 1
                        JOIN SEDEVER.CUIS_PERSONA p ON i.C_PERSONA = p.C_PERSONA
                        LEFT JOIN SEDEVER.CUIS_INTEGRANTE conyuge
                        ON vi.C_VIVIENDA = conyuge.C_VIVIENDA AND conyuge.C_CD_PARENTESCO = 2
                        LEFT JOIN CUIS_PERSONA pConyuge
                        ON conyuge.C_PERSONA = pConyuge.C_PERSONA
                        LEFT JOIN (SELECT * FROM (
                            SELECT ROW_NUMBER() OVER(PARTITION BY v.C_VIVIENDA
                                                                    ORDER BY i.C_INTEGRANTE) AS tupla,i.* FROM SEDEVER.CUIS_VIVIENDA v
                                JOIN SEDEVER.CUIS_INTEGRANTE i ON v.C_VIVIENDA = i.C_VIVIENDA AND i.C_CD_PARENTESCO = 3 AND i.EDAD >= 18)
                                WHERE tupla = 1) hijo ON vi.C_VIVIENDA = hijo.C_VIVIENDA AND hijo.C_CD_PARENTESCO = 3 AND hijo.EDAD >= 18
                        LEFT JOIN SEDEVER.CUIS_PERSONA pHijo ON hijo.C_PERSONA = pHijo.C_PERSONA
                
                
                        LEFT JOIN (select * from (
                            select ROW_NUMBER() OVER(PARTITION BY v.C_VIVIENDA
                                                                    ORDER BY i.C_INTEGRANTE) AS tupla,i.* FROM SEDEVER.CUIS_VIVIENDA v
                                JOIN SEDEVER.CUIS_INTEGRANTE i ON v.C_VIVIENDA = i.C_VIVIENDA AND i.C_CD_PARENTESCO = 3 AND i.EDAD >= 18)
                                WHERE tupla = 2) hijo2 ON vi.C_VIVIENDA = hijo2.C_VIVIENDA AND hijo2.C_CD_PARENTESCO = 3 AND hijo2.EDAD >= 18
                        LEFT JOIN SEDEVER.CUIS_PERSONA pHijo2 ON hijo2.C_PERSONA = pHijo2.C_PERSONA
                
                        LEFT JOIN SEDEVER.CUIS_IMAGENES_VIVIENDA FOTOS  ON VI.C_VIVIENDA = FOTOS.C_VIVIENDA 
                        WHERE IDLOTEIMPRESION IN ('$cveLoteImp')";
            return $query;
            // $results = $conexion->getResults($query);

            // return $results;
        }
    }

?>