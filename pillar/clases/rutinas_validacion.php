<?php
//include '../support/conexion.php';
//include 'gestion.php';
//include '../support/utilities.php';

class rutinasValidacion{

	function valida($idImagen){	
		$conexion = new conexion;
		$gestion = new gestion;

		$preguntas = $gestion->preguntas();

		$query = "select * from ".$conexion->esquema."imagen where idImagen = $idImagen";
		$datosImagen = $conexion -> getResult($query);


		//Validaciones Genericas de combos
		$respuestas = $gestion->respuestas_imagen($idImagen);
		$error['mensaje'] = "";
		$error['estatus'] = 1;

		$error = $this->validacionesEspecificas($datosImagen,$preguntas,$respuestas,$error);
		if($preguntas[$datosImagen['IDTIPOIMAGEN']]!= null){
			$error = $this->comprueba($preguntas[$datosImagen['IDTIPOIMAGEN']],$respuestas,$error);	
		}
		

		
		//Inician validaciones Especificas en caso de ser necesario
		if($error['mensaje'] == ""){
			$error['mensaje'] = "APROBADO";
		}
		
		
		
		return $error;
		//echo $query . "<br><br>";
		//$conexion->insertUpdate($query);
	}



	function comprueba($preguntas,$respuestas,$error){
		if($preguntas != null){
			foreach ($preguntas as $p) {
				if($p['TIPOPREGUNTA'] == 1){
					if($p['RUTINAVALIDACION'] == "SI_NO_1"){
						//echo "Voy a validar la pregunta ".$p['PREGUNTA']." con un si como positivo: la respuesta es " . $respuestas[$p['IDPREGUNTA']][0]['RESPUESTA'];
						if($respuestas[$p['IDPREGUNTA']][0]['RESPUESTA'] != 'SI'){
							$error['mensaje'] .= $p['MENSAJEERROR']." //&&// ";
							$error['estatus'] = 2;
						}
					}
				}else{
					//echo "no";
				}
			}
		}
		return $error;
	}


	function validacionesEspecificas($datosImagen,$preguntas,$respuestas,$error){
		
		$conexion = new conexion;

		if($datosImagen['IDTIPOIMAGEN'] == 8){ // Foto de las tarjetas
			$query = "select * from expedientes where idExpediente = ".$datosImagen['IDEXPEDIENTE'];
			$datos_exp = $conexion->getResult($query);


			//ECHO "COMPARTAR " . $respuestas[13][0]['RESPUESTA'] ." CON ". $datos_exp['VIVIENDA'] ." Y " . $respuestas[13][0]['RESPUESTA'] ." CON  ".  $datos_exp['MONETARIA'];
			if( $respuestas[13][0]['RESPUESTA'] == $datos_exp['VIVIENDA'] and $respuestas[14][0]['RESPUESTA'] == $datos_exp['MONETARIA'] ){
				
			}else{
				$error['mensaje'] .= "Los numeros de las tarjetas no corresponden a los registrados //&&//";
				$error['estatus'] = 2;
			}

		}
		return $error;
	}


	function valida_todas_imagenes(){
		$conexion = new conexion;
		$query = "select * from imagen where idExpediente = 116558 and idTipoImagen not in (0,10)";
		$imagenes = $conexion->getResults($query);
		foreach ($imagenes as $i) {
			$this->valida($i['IDIMAGEN']);
		}
	}
}


//$rutinasValidacion = new rutinasValidacion;
//$rutinasValidacion->valida_todas_imagenes();


?>