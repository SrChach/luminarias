<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 


include_once 'conexion.php';
include_once 'getS.php';

// $directorio = explode('/', getcwd() );
// $directoriPetition = array_pop($directorio);
// if($directoriPetition == 'controller'){
// 	//include_once '../../../public/portal/libraries/phpmailer-master/class.phpmailer.php';
// 	//include_once '../../../../parametros.php';	
// }else{
// 	//include_once '../../public/portal/libraries/phpmailer-master/class.phpmailer.php';
// 	//include_once '../../../parametros.php';
// }



class MailControllerReportes{
	protected $sql;
	protected $result;
	protected $conexion;
	protected $mensaje;
	protected $getS;

	function __construct()
	{
		$this -> conexion = new conexion;
		$this -> uniqid = "_VER#".substr(explode(".", uniqid('',true))[1], 2,7);
		$this -> mensaje = ['SIN DATOS',
		'EXITO! SE HA CANCELADO ESTA CUENTA.',
		'EXITO! SE HA ACTIVO ESTA CUENTA.',
		'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO',
		'EXITO! SE ELIMINO CUENTA DE USUARIO.'] ;
		$this -> getS = new Get;

	}


	public function verificaMail($usuarioId,$rutaDescarga)
	{

		$this -> sql = "SELECT IDUSUARIO,EMAIL, NOMBRE, APELLIDOPATERNO, APELLIDOMATERNO FROM INTEGRADOR.USUARIO WHERE IDUSUARIO=$usuarioId";
		$consulta['GENERALES'] = $this -> conexion -> getResult($this -> sql);
		$email=$consulta['GENERALES']['EMAIL'];
		$nombre=$consulta['GENERALES']['NOMBRE'];
		$paterno=$consulta['GENERALES']['APELLIDOPATERNO'];
		$materno=$consulta['GENERALES']['APELLIDOMATERNO'];

		$nombreCompleto= $nombre . " ". $paterno . " " . $materno;
		// $nombreUser=$consulta['GENERALES']['NOMBRE']." ".$consulta['GENERALES']['APELLIDOPATERNO']." ".$consulta['GENERALES']['APELLIDOMATERNO']
		$enviar=$this -> enviaMail($email,$nombreCompleto,$rutaDescarga);

		return $enviar;	
	}



	public function enviaMail($mail_user,$nombre,$rutaDescarga)
	{
		$nombre=$nombre;
		$mailto=$mail_user;
		$rutaDescarga=$rutaDescarga;

		$parametros= new parametros;
		$mail = new PHPMailer();

		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Host = "smtp.gmail.com"; 
		$mail->Port = 587; 
		$mail->SMTPSecure = 'tls';

		$mail->Username = "prueba2142018@gmail.com"; 
		$mail->Password = "Prueba211589";
		$mail->From = "prueba2142018@gmail.com"; 
		$mail->FromName = "PLATAFORMA VERACRUZ COMIENZA CONTIGO";

		$mail->AddAddress($mailto);
		$mail->IsHTML(true); 


		$mail->Subject = "LIGA PARA DESCARGAR DE REPORTE";


		$body ="<div style='  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
		max-width: 600px; 
		margin: auto;
		font-family: arial;
		padding-top: 20px;
		padding-right: 20px;
		padding-bottom: 20px;
		padding-left: 20px;
		'>";
		$body .= "<img src='".$parametros -> urlImageMail()."' alt='' style='width:50% ;height:50%;padding-left:150px'/><br>";
		$body .= "<h2 align='center' style='color:#28A7D0; margin-top: 20px; font-size: 25px'>LIGA PARA DESCARGA DE REPORTE</h2>";
		$body .= "<h3 align='left' style='color: grey; font-size: 22px;margin-top: 50px; '>".$nombre."</h3>"; 
		$body .= "<p align='justify' style='margin-top: 50px; '>Por este medio se le notífica que su reporte a sido generado y podrá descargarlo a través de la siguiente liga:
		</p><br>
		<p><strong>Descarga: </strong><a href='".$rutaDescarga."'>".$rutaDescarga."</a></p>
		<p style='margin-top: 50px; '>SIN MAS POR EL MOMENTO, SE LE EXTIENDE UN CORDIAL SALUDO.</p>
		</div>
		"; 


		$mail->Body = $body; 
		$mail->CharSet = 'UTF-8'; 
		$exito = $mail->Send(); 



		if($exito){ 
			$res='OK'; 
		}else{
			$res='NO'; 
		} 

		return $res;

	}

}



$controller = new MailControllerReportes; 


$opcion="";
if( isset( $_POST['opcion'] ) ){
	$opcion = $_POST['opcion'];    
}


if ($opcion=='enviaMail') {	
	$usuarioId = ($_POST['usuarioId'] != "") ? $_POST['usuarioId']:"0";
	$rutaDescarga = ($_POST['rutaDescarga'] != "") ? $_POST['rutaDescarga']:"0";
	echo json_encode($controller -> verificaMail($usuarioId,$rutaDescarga));
}

?>




