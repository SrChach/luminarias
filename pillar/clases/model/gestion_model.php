<?php
session_start();

/**/

class Gestion{
	protected $conexion;
	protected $utilities;
	protected $result;
	protected $Error;
	protected $sql;
	protected $mensaje;

	function __construct(){
		$this->conexion = new conexion;
		//$this -> utilities = new utilities;
		$this -> Error = new ErrorController;
		$this -> result = null;
		$this -> mensaje = ['SIN DATOS',
		'EXITO! SE HA CANCELADO ESTA CUENTA.',
		'EXITO! SE ACTIVO ESTA CUENTA.',
		'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO',
		'EXITO! SE ELIMINO CUENTA DE USUARIO.'] ;
	}

	public function getCatalogosLuminarias(){
		// Catalogo por campo - Vertical
		
		$generales = new Generales;
		$catalogos = [];

		$catalogos[] = [
			'titulo' => 'Tipo Ubicacion',
			'name' => 'tipo_ubicacion',
			'disabled' => 'true',
			'options' => [
				[ "value" => "1", "text" => "Vía Primaria" ],
				[ "value" => "2", "text" => "Via Secundaria" ]
			]
		];

		$catalogos[] = [
			'titulo' => 'Ubicacion', // ubicacionfisica
			'name' => 'ubicacion_fisica',
			'disabled' => 'true',
			'options' => $generales->getSelectOptions($table = 'CATALOGO_UBICACIONFISICA', $value = 'IDUBICACIONFISICA', $text = 'NOMBRE')
		];

		$catalogos[] = [
			'titulo' => 'Tipo Poste', // tipoposte
			'name' => 'tipo_poste',
			'options' => $generales->getSelectOptions('TIPOPOSTE', 'IDTIPOPOSTE', 'TIPOPOSTE')
		];

		// "MODELO", en la vista. El campo complementrio si se usa para filtrar, ver lógica en el front
		$catalogos[] = [
			'titulo' => 'Modelo', // extraubicacion
			'name' => 'modelo',
			'options' => $generales->getSelectOptions('EXTRAUBICACIONFISICA', 'IDEXTRAUBICACIONFISICA', 'NOMBRE', ['CATEGORIAUBICACION'])
		];

		$catalogos[] = [
			'titulo' => 'Tipo Lampara', // Tecnología
			'name' => 'caracteristica_tecnica',
			'options' => $generales->getSelectOptions('CATALOGO_CARACTERISTICATECNICA', 'IDCARATERISTICATECNICA', 'NOMBRE')
		];
		
		$catalogos[] = [
			'titulo' => 'Tipo Carcaza', // carcaza
			'name' => 'tipo_carcaza',
			'options' => $generales->getSelectOptions('TIPOCARCAZA', 'IDTIPOCARCAZA', 'TIPOCARCAZA')
		];

		$catalogos[] = [
			'titulo' => 'Wattaje', // watts
			'name' => 'wattaje',
			'options' => $generales->getSelectOptions('CATALOGO_WATTS', 'IDWATTS', 'VALOR')
		];

		$catalogos[] = [
			'titulo' => 'Tipo Balastro', // balastro
			'name' => 'tipo_balastro',
			'options' => $generales->getSelectOptions('TIPOBALASTRO', 'IDTIPOBALASTRO', 'TIPOBALASTRO')
		];

		// $catalogos[] = [
		// 	'titulo' => 'tipolampara',
		// 	'options' => $generales->getSelectOptions('CATALOGO_TIPOLAMPARA', 'IDTIPOLAMPARA', 'NOMBRETIPOLAMAPARA')
		// ];
		
		return $catalogos;
	}

	public function getCatalogo_CD_telmex(){
		// Catalogo horizontal
		$sql = "SELECT 
				CD_T.ID, CD_T.TEXTO, CD_T.FABRICANTE, CD_T.MARCA, CD_T.MODELO, CD_T.CAPACIDAD_WATTS, 
				CD_T.ENCENDIDO_HR, CD_T.USO, E.NOMBREEMPRESA AS EMPRESA, C.COMPANIA
			FROM
				CATALOGO_CD_TELMEX CD_T
			JOIN 
				CATALOGO_EMPRESA E ON CD_T.ID_EMPRESA = E.IDEMPRESA
			JOIN
				COMPANIA C ON C.IDCOMPANIA = CD_T.ID_COMPANIA";

		return [
			'tipo' => 'CD Telmex',
			'selects' => $this->conexion->getResults($sql),
			'select_by' => [
				'value_field' => 'ID',
				'text_field' => 'TEXTO',
				'text_field_as' => 'SELECCIONAR'
			] 
		];
	}

	public function getAsignacion_CD_telmex(){
		if( !isset($_SESSION['IDUSUARIO']) )
			return ['error' => 'Inicia sesión para continuar'];

		$id_user = $_SESSION['IDUSUARIO'];

		// Obtiene el mismo registro si tenía uno pendiente, o obtiene uno nuevo si no tenía
		$sql = "SELECT RL.IDREGISTRO, RL.LATITUD, RL.LONGITUD, RL.OBSERVACIONLEVANTAMIENTO, RL.FOLIO
			FROM 
				CUESTIONARIOLUMINARIA CL 
			JOIN
				REGISTROLUMINARIA RL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
			WHERE 
				CL.EMPRESA = 1 AND RL.ESTATUS = 1 AND RL.TIPIFICADO = 0
				AND (RL.IDUSUARIOASIGNADO IS NULL OR RL.IDUSUARIOASIGNADO = '$id_user')
				AND (
					RL.IDUSUARIOASIGNADO='$id_user'
					OR NOT EXISTS(
						SELECT RL.IDUSUARIOASIGNADO FROM 
								REGISTROLUMINARIA RL 
							JOIN
								CUESTIONARIOLUMINARIA CL
							ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
							WHERE RL.IDUSUARIOASIGNADO='$id_user' AND CL.EMPRESA = 1 AND RL.ESTATUS = 1 AND RL.TIPIFICADO = 0
					)
				)
			LIMIT 1";

		$tipificando = $this->conexion->getResult($sql);
		if($tipificando == null)
			return ['error' => 'no hay más registros para tipificar'];

		// Asigna el registro al usuario. Des-asigna todos los registros con más de 30 minutos y que no estén tipificados
		$updateselect = "UPDATE REGISTROLUMINARIA SET FECHA_ASIGNACION=NOW(), IDUSUARIOASIGNADO='$id_user' WHERE IDREGISTRO='{$tipificando['IDREGISTRO']}';
		UPDATE REGISTROLUMINARIA SET IDUSUARIOASIGNADO=NULL, FECHA_ASIGNACION = NULL WHERE FECHA_ASIGNACION <= NOW() - INTERVAL 30 MINUTE AND TIPIFICADO = 0";
		$comprobar = $this->conexion->insertUpdate($updateselect);

		$sql_images = "SELECT TIPOIMAGEN, URLIMAGEN FROM IMAGEN WHERE IDREGISTROLUMINARIA='{$tipificando['IDREGISTRO']}'";
		$images = $this->conexion->getResults($sql_images);

		foreach ($images as $imagen) {
			if($imagen['TIPOIMAGEN'] == 'EVIDENCIA'){
				if( !array_key_exists("evidencias", $tipificando) )
					$tipificando['evidencias'] = [$imagen['URLIMAGEN']];
				else
					$tipificando['evidencias'][] = $imagen['URLIMAGEN'];
			} else {
				if( !array_key_exists("referencias", $tipificando) )
					$tipificando['referencias'] = [$imagen['URLIMAGEN']];
				else
					$tipificando['referencias'][] = $imagen['URLIMAGEN'];
			}
		}

		// Limpiamos las cosas extra
		foreach ([0, 1, 2, 3, 4] as $key)
			unset($tipificando[$key]);

		return ['status'=> $comprobar, 'actual' => $tipificando];
	}

	public function update_CD_telmex($data){
		if( !isset($data['selected']) || !isset($data['folio']) || !isset($data['tba']) || !isset($data['no_economico']) )
			return ['error' => 'Error. Faltaron parámetros. Recargue la página e intente de nuevo'];
		
		$aditional = "";
		$aditional .= ($data['no_economico'] == NULL || $data['no_economico'] == 'null') ? '' : "NO_ECONOMICO = '{$data['no_economico']}',"; 
		$aditional .= ($data['tba'] == NULL || $data['tba'] == 'null') ? '' : "TBA = '{$data['tba']}',"; 

		$sql = "UPDATE REGISTROLUMINARIA
			SET
				ID_TIPIFICADO_CD_TELMEX={$data['selected']},
				$aditional
				CARGA_WATTS=(SELECT CAPACIDAD_WATTS FROM CATALOGO_CD_TELMEX WHERE ID = {$data['selected']}),
				TIPIFICADO=1
			WHERE FOLIO = '{$data['folio']}'";

		$sql2 = "UPDATE IMAGEN SET FECHATIPIFICACION=DATE(NOW()) WHERE FOLIOREGISTRO = '{$data['folio']}' AND TIPOIMAGEN='EVIDENCIA'";

		$status_tipificado_rl = $this->conexion->insertUpdate($sql);
		$status_fecha_tip_imagen = $this->conexion->insertUpdate($sql2);

		if( $status_tipificado_rl != true )
			return ['error' => 'no se pudo guardar correctamente. Intentelo de nuevo'];

		return [
			'data' => [
				'status_tipificado_rl' => $status_tipificado_rl,
				'status_fecha_tip_imagen' => $status_fecha_tip_imagen
			]
		];
	}

	// Menos llamadas a BD, tarda menos que la original
	public function getAsignacionLuminarias(){
		$iduser = $_SESSION['IDUSUARIO'];
		
		// Saca todas las luminarias asignadas
		$this->sql = "SELECT 
				ASIGNADAS.IDREGISTRO, CL.UBICACIONFISICA ubicacion_fisica, CL.EXTRAUBICACIONFISICA modelo,
				CL.CARACTERITICATECTNICA caracteristica_tecnica, CL.TIPOLUMINARIA tipo_ubicacion -- 2 = (VÍA SECUNDARIA), otro = VÍA PRIMARIA
			FROM 
				(
					SELECT IDREGISTRO, IDTIPOLUMINARIA FROM REGISTROLUMINARIA WHERE IDUSUARIOASIGNADO=$iduser AND TIPIFICADO=0 AND ESTATUS=1 LIMIT 3
				) ASIGNADAS
			LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = ASIGNADAS.IDTIPOLUMINARIA
			LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = ASIGNADAS.IDREGISTRO";
		$registros = $this->conexion -> getResults($this->sql);

		// Obtener todas las imágenes de los registros
		$ids_registro = '';
		foreach ($registros as $key => $value) {
			if($key != 0)
				$ids_registro .= ',';
			$ids_registro .= $value['IDREGISTRO'];
		}
		$sql_imagenes = "SELECT IDREGISTROLUMINARIA, URLIMAGEN, TIPOIMAGEN FROM IMAGEN WHERE TIPOIMAGEN IS NOT NULL AND IDREGISTROLUMINARIA IN ($ids_registro)";
		$imagenes = $this->conexion->getResults($sql_imagenes);
		
		// Guardar imagenes en el catálogo original
		foreach($imagenes as &$value){
			$ret = array_search($value['IDREGISTROLUMINARIA'], array_column($registros, 'IDREGISTRO'));
			if($value['TIPOIMAGEN'] == 'EVIDENCIA'){
			
				if (!array_key_exists("evidencias",$registros[$ret]))
					$registros[$ret]['evidencias'] = [$value];
				else
					$registros[$ret]['evidencias'][] = $value;
				
			} else {
				if (!array_key_exists("referencias",$registros[$ret]))
					$registros[$ret]['referencias'] = [$value];
				else
					$registros[$ret]['referencias'][] = $value;
			}
			$value = null;
		}

		return $registros;

	}

	public function getAsignacion()
	{
		$iduser=$_SESSION['IDUSUARIO'];
		$this->sql="UPDATE REGISTROLUMINARIA SET IDUSUARIOASIGNADO = '$iduser' WHERE IDREGISTRO IN (
						SELECT IDREGISTRO FROM (
							SELECT IDREGISTRO FROM REGISTROLUMINARIA WHERE IDUSUARIOASIGNADO IS NULL AND ESTATUS=1 AND IDTIPOLUMINARIA=1 LIMIT 15
						) ASIGNACION
					)";
		$asignacion = $this->conexion-> insertUpdate($this->sql);

		$respuesta = $this->getRegistros($asignacion);

		$generales = new Generales;
		$utilities = new utilities;

		$catalogos['ubicacionfisica'] = $generales->getCatalogo("CATALOGO_UBICACIONFISICA");
		$catalogos['extraubicacion'] = $utilities->objetivisa('CATEGORIAUBICACION',$generales->getCatalogo("EXTRAUBICACIONFISICA"));
		$catalogos['tipolampara'] = $utilities->objetivisa('IDTIPOLAMPARA',$generales->getCatalogo("CATALOGO_TIPOLAMPARA"));
		$catalogos['watts'] = $generales->getCatalogo("CATALOGO_WATTS");
		$catalogos['tipoposte'] = $generales->getCatalogo("TIPOPOSTE");
		$catalogos['tecnologia'] = $generales->getCatalogo("CATALOGO_CARACTERISTICATECNICA");
		$catalogos['carcaza'] = $generales->getCatalogo("TIPOCARCAZA");
		$catalogos['balastro'] = $generales->getCatalogo("TIPOBALASTRO");



		$respuesta['catalogos'] = $catalogos;

		return $respuesta;
	}


	public function checkAsignacion($value='')
	{
		if( !isset($_SESSION['IDUSUARIO']) ) return ['code' => false, 'error' => 'Recarga la página e inicia sesión para continuar'];
		
		$perfil=$_SESSION['IDUSUARIO'];
		$sql = "SELECT * FROM REGISTROLUMINARIA WHERE IDUSUARIOASIGNADO=$perfil AND TIPIFICADO=0";
		$registros = $this->conexion -> getResults($sql);

		$respuesta['code'] = false;
		$respuesta['data'] = [];
		if (count($registros)>0) {
			$respuesta = $this->getRegistros(true);
		}
		$generales = new Generales;
		$utilities = new utilities;

		$catalogos['ubicacionfisica'] = $generales->getCatalogo("CATALOGO_UBICACIONFISICA");
		$catalogos['extraubicacion'] = $utilities->objetivisa('CATEGORIAUBICACION',$generales->getCatalogo("EXTRAUBICACIONFISICA"));
		$catalogos['tipolampara'] = $utilities->objetivisa('IDTIPOLAMPARA',$generales->getCatalogo("CATALOGO_TIPOLAMPARA"));
		$catalogos['watts'] = $generales->getCatalogo("CATALOGO_WATTS");
		$catalogos['tipoposte'] = $generales->getCatalogo("TIPOPOSTE");
		$catalogos['tecnologia'] = $generales->getCatalogo("CATALOGO_CARACTERISTICATECNICA",'NOMBRE');
		$catalogos['carcaza'] = $generales->getCatalogo("TIPOCARCAZA");
		$catalogos['balastro'] = $generales->getCatalogo("TIPOBALASTRO");


		$respuesta['catalogos'] = $catalogos;

		return $respuesta;
	}

	public function getRegistros($asignacion)
	{
		$perfil=$_SESSION['IDUSUARIO'];
		$registros=[];
		$data=[];
		$respuesta['code'] = false;
		$respuesta['data'] = [];
		if ($asignacion) {
			$sql = "SELECT * FROM REGISTROLUMINARIA WHERE IDUSUARIOASIGNADO=$perfil AND TIPIFICADO=0";
			$registros = $this->conexion -> getResults($sql);
			foreach ($registros  as $key => $resgitro) {
				$type = $resgitro['IDTIPOLUMINARIA'];
				$key = $resgitro['IDREGISTRO'];
				if ($type==1) { // luminarias
					$this -> sql = "SELECT RL.*,TL.NOMBRE TIPO,
										CL.TIPOLUMINARIA KEYTIPOLUMINARIA,
										CASE WHEN CL.TIPOLUMINARIA=2 THEN 'ALUMBRADO PUBLICO (VÍA SECUNDARIA)' ELSE 'ALUMBRADO PUBLICO (VÍA PRIMARIA)' END TIPOLUMINARIA,
										CUF.IDUBICACIONFISICA,
										CUF.NOMBRE UBICACIONFISICA,CL.EXTRAUBICACIONFISICA,CCT.NOMBRE CARACTERISTICATECNICA
									FROM REGISTROLUMINARIA RL
										LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
										LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
										LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
										LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA
									WHERE RL.IDREGISTRO=$key";
				}
				if ($type==2) { // semaforos
					$this -> sql = "SELECT
										RL.*,TL.NOMBRE TIPO,TS.NOMBRE TIPOLUMINARIA,
										CUF.NOMBRE UBICACIONFISICA,CUF.IDUBICACIONFISICA,CL.EXTRAUBICACIONFISICA,CCT.NOMBRE CARACTERISTICATECNICA
									FROM REGISTROLUMINARIA RL
										LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
										LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
		    							LEFT JOIN TIPOSEMAFORO TS ON TS.IDTIPOSEMAFORO = CL.TIPOLUMINARIA
										LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
										LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA
									WHERE RL.IDREGISTRO=$key";
				}

				if ($type==3) { //cargas directas
					$this -> sql = "SELECT
										RL.*,TL.NOMBRE TIPO,TCD.NOMBRE TIPOLUMINARIA,
										CUF.NOMBRE UBICACIONFISICA,CUF.IDUBICACIONFISICA,CL.EXTRAUBICACIONFISICA,CCT.NOMBRE CARACTERISTICATECNICA
									FROM REGISTROLUMINARIA RL
										LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
										LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
		    							LEFT JOIN TIPOCARGADIRECTA TCD ON TCD.IDTIPOCARGADIRECTA = CL.TIPOLUMINARIA
										LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
										LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA
									WHERE RL.IDREGISTRO=$key";
				}
				$this->result = $this->conexion -> getResults($this -> sql);

				$sql = "SELECT * FROM IMAGEN WHERE IDREGISTROLUMINARIA = $key";
				$images = $this -> conexion -> getResults($sql);
				$response['images'] = $images;
				$response['type'] = $type;
				if (count($this -> result)>0) {
					//$data = $this -> result[0];
					$response['generales'] = $this -> result[0];
					//$response['code'] = true;
					array_push($data, $response);
				}
			}
			$respuesta['code'] = true;
			$respuesta['data'] = $data;
		}
		return $respuesta;
	}


	public function saveData($values)
	{
		$this->sql = "UPDATE IMAGEN SET ";
		foreach ($values as $key => $value) {
			if ($key!="csrf_token" and $key!="keyimg" and $key!="opcion") {
				$this->sql.= " $key = '$value', ";
			}
		}
		$date = date('Y-m-d H:i:s');
		$this->sql.= "FECHATIPIFICACION = '$date' WHERE IDIMAGEN=".$values['keyimg'];

		try {
			$this->result = $this->conexion->insertUpdate($this->sql);
			if ($this->result===true) {
				$this->result = array('codigo'=>true);

				$sql = "SELECT * FROM IMAGEN WHERE IDIMAGEN=".$values['keyimg'];
				$imagen = $this->conexion->getResult($sql);
				$sql = "UPDATE REGISTROLUMINARIA SET TIPIFICADO=1 WHERE IDREGISTRO=".$imagen['IDREGISTROLUMINARIA'];
				$this->conexion->insertUpdate($sql);
			}else{
				$this->result = array('codigo'=>false,'message'=>$this->result);
			}

		} catch (\Exception $e) {
			$this->result = array('codigo'=>false,'message'=>$e->getMessage());
		}
		return $this->result;
	}

	public function setConciliacion($POST)
	{
		$this->sql = "UPDATE IMAGEN SET CONCILIACIONCFE=".$POST['value'].", CONCILIACIONTIA = ".$POST['value'].", CONCILIACIONALCALDIA=".$POST['value']."  WHERE IDIMAGEN=".$POST['keyimg'];
		try {
			$this->result = $this->conexion->insertUpdate($this->sql);
			if ($this->result===true) {
				$this->result = array('codigo'=>true);
			}else{
				$this->result = array('codigo'=>false,'message'=>$this->result);
			}
		} catch (\Exception $e) {
			$this->result = array('codigo'=>false,'message'=>$e->getMessage());
		}
		return $this->result;
	}
	public function setConciliacionCFE($POST)
	{
		$this->sql = "UPDATE IMAGEN SET CONCILIACIONCFE=".$POST['value']." WHERE IDIMAGEN=".$POST['keyimg'];
		try {
			$this->result = $this->conexion->insertUpdate($this->sql);
			if ($this->result===true) {
				$this->result = array('codigo'=>true);
			}else{
				$this->result = array('codigo'=>false,'message'=>$this->result);
			}
		} catch (\Exception $e) {
			$this->result = array('codigo'=>false,'message'=>$e->getMessage());
		}
		return $this->result;
	}
	public function setConciliacionTIA($POST)
	{
		$this->sql = "UPDATE IMAGEN SET CONCILIACIONTIA=".$POST['value']." WHERE IDIMAGEN=".$POST['keyimg'];
		try {
			$this->result = $this->conexion->insertUpdate($this->sql);
			if ($this->result===true) {
				$this->result = array('codigo'=>true);
			}else{
				$this->result = array('codigo'=>false,'message'=>$this->result);
			}
		} catch (\Exception $e) {
			$this->result = array('codigo'=>false,'message'=>$e->getMessage());
		}
		return $this->result;
	}
	public function setConciliacionAlcandia($POST)
	{
		$this->sql = "UPDATE IMAGEN SET CONCILIACIONALCALDIA=".$POST['value']." WHERE IDIMAGEN=".$POST['keyimg'];
		try {
			$this->result = $this->conexion->insertUpdate($this->sql);
			if ($this->result===true) {
				$this->result = array('codigo'=>true);
			}else{
				$this->result = array('codigo'=>false,'message'=>$this->result);
			}
		} catch (\Exception $e) {
			$this->result = array('codigo'=>false,'message'=>$e->getMessage());
		}
		return $this->result;
	}

}



?>
