<?php
session_start();

/**/

class Device{
	protected $conexion;
	protected $result;
	protected $Error;
	protected $sql;
	protected $mensaje;

	function __construct(){
		$this->conexion = new conexion;
		$this -> Error = new ErrorController;
		$this -> result = null;
		$this->mensaje = ['SIN DATOS',
		'EXITO! SE HA CANCELADO ESTA CUENTA.',
		'EXITO! SE ACTIVO ESTA CUENTA.',
		'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO',
		'EXITO! SE ELIMINO CUENTA DE USUARIO.'] ;
	}

	public function getDevicesByZone()
	{
		$cve_zona = isset($_POST['zona']) ? $_POST['zona'] : null;

		$detalle_zona = [];
		if ($cve_zona!=null) {
			$sql= "SELECT D.*,Z.NOMBRE NOMBREZONA,U.NOMBRE,U.APATERNO,U.AMATERNO,U.EMAIL USUARIO, U.PASS CONTRASEÑA FROM DISPOSITIVOS D
					LEFT JOIN USUARIO U ON U.IDUSUARIO = D.USUARIO
					JOIN ZONA Z ON Z.IDZONA = D.ZONA
					WHERE ZONA = '$cve_zona' AND D.ESTATUS=1";
			$detalle_zona = $this->conexion->getResults($sql);
		}
		$this->sql = "SELECT Z.IDZONA, Z.NOMBRE ZONA, IF(TOTAL IS NULL , 0, TOTAL) TOTAL FROM ZONA Z
						LEFT JOIN (SELECT ZONA,COUNT(*) TOTAL FROM DISPOSITIVOS WHERE ESTATUS=1 GROUP BY ZONA)D ON D.ZONA =Z.IDZONA";


		try {
			$this->result = array('codigo'=>true,'data'=>$this->conexion->getResults($this->sql),'detalle'=>$detalle_zona);
		} catch (\Exception $e) {
			$this->result = array('codigo'=>false,'message'=>$e->getMessage());
		}
		return $this->result;
	}

	public function addDevicesToZone($pathFile)
	{
		$imeis = [];
		$ZONA = $_POST['zona'];
		$name_zona = $_POST['nombrezona'];
		if (file_exists($pathFile)) {
			$objPHPExcel = new PHPExcel();

			$inputFileType = PHPExcel_IOFactory::identify($pathFile);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);

			$objPHPExcel = $objReader->load($pathFile);
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$increment = 0;
			for ($row = 6; $row <= $highestRow; $row++){
				//array_push($imeis,$sheet->getCell("C".$row)->getFormattedValue());
				$pass = $sheet->getCell("M".$row)->getFormattedValue();
				$sql = "SELECT * FROM USUARIO WHERE PASS='$pass'";
				$user = $this->conexion->getResults($sql);
				$usuario = $user[0]['IDUSUARIO'];

				$IMEI = $sheet->getCell("C".$row)->getFormattedValue();
				$IME2 = $sheet->getCell("D".$row)->getFormattedValue();
				$NUM_SERIE_SIM = $sheet->getCell("E".$row)->getFormattedValue();
				$CELULAR = $sheet->getCell("F".$row)->getFormattedValue();
				$date = new DateTime($sheet->getCell("H".$row)->getFormattedValue());
				$FECHAENTREGA = $date->format('Y-m-d H:i:s');

				$sql = "INSERT INTO DISPOSITIVOS(ZONA,IMEI,IMEI2,NUM_SERIE_SIM,CELULAR,FECHAENTREGA,USUARIO)
						VALUES('$ZONA','$IMEI','$IME2','$NUM_SERIE_SIM','$CELULAR','$FECHAENTREGA','$usuario')";
				$result = $this->conexion->insertUpdate($sql);
				if ($result!=true) {
					$this->result = array('codigo'=>false,'message'=>$this->mensaje[3]);
				}
				$increment++;
			}
			$this->result = array('codigo'=>true,'message'=>'Se asignarón '.$increment.' dispositivos a la zona de '.$name_zona.'.');

		}else{
			$this->result = array('codigo'=>false,'message'=>'No existe la ruta del archivo!');
		}
		return $this->result;
	}

	public function downDevice()
	{
		$key = $_POST['key'];
		$this->sql = "UPDATE DISPOSITIVOS SET ESTATUS=0 WHERE IDDISPOSITIVO = $key";
		try {
			$this -> result = $this -> conexion -> insertUpdate ($this -> sql);
			if ($this -> result===true) {
				$this -> result = array ('codigo' => $this -> result, "message" => 'Se eliminodispositivo de esta zona!');
			}else{
				$this -> result = array ('codigo' => false, "message" => $this -> result );
			}
		} catch (Exception $e) {
			$this -> result = array ('codigo'=> false, 'message' => $e);
		}
		return $this -> result;
	}
}



?>
