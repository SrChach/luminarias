<?php
session_start();

/**/

class Main{
	protected $conexion;
	protected $utilities;
	protected $result;
	protected $Error;
	protected $sql;

	function __construct(){
		$this -> conexion = new conexion;
		$this -> Error = new ErrorController;
		$this -> result = null;
		
		// Validación para zonas de CFE
		if($_SESSION['PERFIL'] == '11' && $_SESSION['ZONA'] != '0')
			$this->zona = $_SESSION['ZONA'];
		else
			$this->zona = null;

		$this->isDelegacion = ($_SESSION['PERFIL'] == '12') ? true : false;
		$this->delegacion = ($_SESSION['PERFIL'] == '12' && $_SESSION['DELEGACION'] != null) ? $_SESSION['DELEGACION'] : null;
		if($this->delegacion) {
			$sql_delegacion = "SELECT D.NOMBRE FROM USUARIO U JOIN DELEGACION D ON U.IDDELEGACION = D.IDDELEGACION WHERE U.IDUSUARIO = {$_SESSION['IDUSUARIO']}";
			$this->del_nombre = $this->conexion->getResults($sql_delegacion)[0]['NOMBRE'];
		}
	}

	public function getDatasInit()
	{
		header('Content-type: Application/json');
		$where_zona = ($this->zona) ? "AND RL.ZONA='{$this->zona}'" : '';
		$where_delegacion = ($this->delegacion) ? "AND RL.DELLOCMUN = '{$this->delegacion}'" : '';
		$this->sql = "SELECT
				TL.NOMBRE TIPO, COALESCE(SUM(CARGA_WATTS), 0) AS CARGA, COUNT(*) TOTAL
			FROM
				REGISTROLUMINARIA RL
			JOIN
				TIPOLUMINARIA TL
			ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
			WHERE RL.ESTATUS=1 $where_zona $where_delegacion
			GROUP BY RL.IDTIPOLUMINARIA";

		$sql_por_zona = "SELECT
				ZONA.NOMBRE, TL.NOMBRE TIPO, COUNT(*) AS TOTAL
			FROM
				REGISTROLUMINARIA RL
			JOIN
				TIPOLUMINARIA TL
			JOIN
				ZONA
			ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
				AND ZONA.IDZONA = RL.ZONA
			WHERE RL.ESTATUS=1
				AND RL.ZONA != 'null'
			GROUP BY RL.ZONA, RL.IDTIPOLUMINARIA
			ORDER BY RL.ZONA, RL.IDTIPOLUMINARIA";

		$del_nombre = isset($this->del_nombre) ? $this->del_nombre : null;

		if($this->isDelegacion && !($this->delegacion))
			return ['codigo'=>false,'message'=>'Aun no tienes delegacion asignada. Contacta a soporte'];
		
		try {
			$datos_generales = $this->conexion->getResults($this->sql);
			$datos_por_zona = ($this->zona || $this->delegacion) ? null : $this->conexion->getResults($sql_por_zona);
			$this->result = ['codigo'=>true, 'data'=>$datos_generales, 'data_by_zone'=>$datos_por_zona, 'delegacion' => $del_nombre];
		} catch (\Exception $e) {
			$this->result = ['codigo'=>false,'message'=>$e->getMessage()];
		}
		return $this->result;
	}

	public function getDatas($request)
	{
		$where_zona = ($this->zona) ? "AND RL.ZONA='{$this->zona}'" : "";
		$where_delegacion = ($this->delegacion) ? "AND RL.DELLOCMUN = '{$this->delegacion}'" : '';

		if ($request['filter']=='luminarias') {		
			$this->sql = "SELECT UF.NOMBRE AS UBICACION_LUMINARIA, COALESCE(SUM(RL.CARGA_WATTS), 0) AS CARGA, COUNT(*) AS TOTAL
				FROM
						REGISTROLUMINARIA RL
					JOIN
						CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
					JOIN 
						CATALOGO_UBICACIONFISICA UF ON CL.UBICACIONFISICA = UF.IDUBICACIONFISICA
				WHERE RL.ESTATUS=1 AND IDTIPOLUMINARIA=1 $where_zona $where_delegacion
				GROUP BY UF.NOMBRE";
		} elseif ($request['filter']=='semaforos') {
			$this->sql = "SELECT TS.NOMBRE TIPO_SEMAFORO, COALESCE(SUM(RL.CARGA_WATTS), 0) AS CARGA, COUNT(*) TOTAL
				FROM
						REGISTROLUMINARIA RL
					JOIN
						CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
					JOIN
						TIPOSEMAFORO TS ON TS.IDTIPOSEMAFORO = CL.TIPOLUMINARIA
				WHERE RL.ESTATUS=1 AND RL.IDTIPOLUMINARIA=2 $where_zona $where_delegacion
				GROUP BY NOMBRE";

		} elseif ($request['filter']=='cargas') {

			$this->sql = "SELECT TCD.NOMBRE 'TIPO_C_DIRECTA', COALESCE(SUM(RL.CARGA_WATTS), 0) AS CARGA, COUNT(*) TOTAL
				FROM
						REGISTROLUMINARIA RL
					JOIN
						CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
					LEFT JOIN
						TIPOCARGADIRECTA TCD ON TCD.IDTIPOCARGADIRECTA = CL.TIPOLUMINARIA
				WHERE RL.ESTATUS=1 AND RL.IDTIPOLUMINARIA=3 $where_zona $where_delegacion
				GROUP BY NOMBRE";
		
		} else {
			return ['codigo' => false, 'message' => 'operacion no permitida'];
		}

		if($this->isDelegacion && !($this->delegacion))
			return ['codigo'=>false,'message'=>'Aun no tienes delegacion asignada. Contacta a soporte'];
		
		try {
			$this->result = array('codigo'=>true,'data'=>$this->conexion->getResults($this->sql));
		} catch (\Exception $e) {
			$this->result = array('codigo'=>false,'message'=>$e->getMessage());
		}
		return $this->result;
	}
}














?>
