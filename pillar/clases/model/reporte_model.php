<?php
session_start();

/**/

class Reporte{
	protected $conexion;
	protected $utilities;
	protected $result;
	protected $Error;
	protected $sql;
	protected $mensaje;

	function __construct(){
		$this->conexion = new conexion;
		//$this -> utilities = new utilities;
		$this -> Error = new ErrorController;
		$this -> result = null;
		$this -> mensaje = ['SIN DATOS',
		'EXITO! SE HA CANCELADO ESTA CUENTA.',
		'EXITO! SE ACTIVO ESTA CUENTA.',
		'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO',
		'EXITO! SE ELIMINO CUENTA DE USUARIO.'] ;
	}
	public function eficiencia_usuario($supervisor=null)
	{
		$this->sql = "SELECT DISTINCT DATE_FORMAT(FECHALEVANTAMIENTO,'%d-%m-%Y') FECHA FROM REGISTROLUMINARIA WHERE ESTATUS=1 -- ORDER BY FECHALEVANTAMIENTO";
		$fechas = $this->conexion->getResults($this->sql);

		$complete = "";

		if ($supervisor!=null) {
			$complete = "AND SUPERVISOR = $supervisor";
		}

		$this->sql = "SELECT
						U.IDUSUARIO,
						CONCAT(U.NOMBRE,' ', U.APATERNO,' ', U.AMATERNO) USUARIO,";
		foreach ($fechas as $key => $fecha) {
			$date = $fecha['FECHA'];
			$column = str_replace("-","_",$date);
			$this->sql.= "IF($column IS NULL, 0, $column) $column,";
		}
		$this->sql.="IF(TOTAL IS NULL, 0, TOTAL) TOTAL
					FROM(SELECT * FROM USUARIO WHERE IDPERFIL=9 AND IDUSUARIO NOT IN (2,4,5,6,7,8,112) $complete) U
					LEFT JOIN (
						SELECT
							R.USUARIOLEVANTAMIENTO,";
		foreach ($fechas as $key => $fecha) {
			$date = $fecha['FECHA'];
			$column = str_replace("-","_",$date);
			$this->sql.= "SUM(CASE WHEN DATE_FORMAT(FECHALEVANTAMIENTO,'%d-%m-%Y') = '$date' THEN 1 ELSE 0 END ) $column,";
		}
		$this->sql .= "	COUNT(USUARIOLEVANTAMIENTO) TOTAL
						FROM REGISTROLUMINARIA R
						WHERE R.ESTATUS=1
						GROUP BY R.USUARIOLEVANTAMIENTO
					) R ON R.USUARIOLEVANTAMIENTO = U.IDUSUARIO";
		//echo $this->sql;

		/*$this->sql = "SELECT
						R.USUARIOLEVANTAMIENTO, CONCAT(U.NOMBRE,' ', U.APATERNO,' ', U.AMATERNO) USUARIO,";
		foreach ($fechas as $key => $fecha) {
			$date = $fecha['FECHA'];
			$column = str_replace("-","_",$date);
			$this->sql.= "SUM(CASE WHEN DATE_FORMAT(FECHALEVANTAMIENTO,'%d-%m-%Y') = '$date' THEN 1 ELSE 0 END ) $column,";
		}
		$this->sql .= "COUNT(USUARIOLEVANTAMIENTO) TOTAL
					FROM REGISTROLUMINARIA R
					RIGHT JOIN (SELECT * FROM USUARIO WHERE IDPERFIL=9 AND IDUSUARIO NOT IN (2,4,5,6,7,8)) U ON U.IDUSUARIO = R.USUARIOLEVANTAMIENTO $complete
					WHERE R.ESTATUS=1
					GROUP BY R.USUARIOLEVANTAMIENTO";*/
		try {
			$this->result = $this->conexion->getResults($this->sql);
			$this->result = array('code' => true, 'data' => $this->result);
		} catch (\Exception $e) {
			$this->result = array('code' => false, 'data' => $e->getMessage());
		}
		return $this->result;
	}

	public function eficiencia_usuario_dia($user, $dateFilter)
	{
		$userFind = "SELECT CONCAT(NOMBRE, ' ', APATERNO, ' ', AMATERNO) USUARIO FROM USUARIO WHERE IDUSUARIO = $user";

		$this->sql = 
		"SELECT
				date_format(FECHALEVANTAMIENTO, '%H') hora,
				count(*) cantidad
			FROM REGISTROLUMINARIA
			WHERE 
				ESTATUS = 1
				AND USUARIOLEVANTAMIENTO = $user
				AND date(FECHALEVANTAMIENTO) = '$dateFilter'
			GROUP BY hora
			ORDER BY hora
		";

		try {
			$userName = $this->conexion->getResults($userFind);
			$this->result = $this->conexion->getResults($this->sql);
			$this->result = array('code' => true, 'usuario'=>$userName[0], 'data' => $this->result);
		} catch (\Exception $e) {
			$this->result = array('code' => false, 'data' => $e->getMessage());
		}
		return $this->result;
	}

	public function check_images(){
		$sql = "SELECT 
				IDIMAGEN, URLIMAGEN, EXISTE_OS
			FROM IMAGEN 
			WHERE EXISTE_OS IS NULL
			ORDER BY IDIMAGEN
			LIMIT 150";
	
		$routecheck = dirname(__FILE__) . "/../../../../storage/images/";

		try {
			$to_check = ['code' => true, 'data' => $this->conexion->getResults($sql) ];	
		} catch (\Exception $e) {
			$to_check = ['code' => false, 'data' => $e->getMessage()];
		}

		if( $to_check['code'] == false || !isset($to_check['data']) ){
			$to_check['code'] == false;
			return $to_check;
		}

		foreach($to_check['data'] as &$elemento){
			$img_actual = $routecheck . $elemento['URLIMAGEN'];

			$elemento['EXISTE_OS'] = file_exists($img_actual);
			if($elemento['EXISTE_OS']){
				$elemento['SIZE_KB'] = round(filesize($img_actual)/1024, 2);
				$update_sql = "UPDATE IMAGEN SET EXISTE_OS=1, SIZE_KB={$elemento['SIZE_KB']} WHERE IDIMAGEN={$elemento['IDIMAGEN']}";
			} else {
				$update_sql = "UPDATE IMAGEN SET EXISTE_OS=0 WHERE IDIMAGEN={$elemento['IDIMAGEN']}";
			}

			$this->conexion->insertUpdate($update_sql);
		}

		return $to_check;	
	}

	public function conciliacion()
	{
		$inicio = date("Y-m-d",strtotime($_POST['init']));
		$fin = date("Y-m-d",strtotime($_POST['end'].' + 1 days'));

		$profile = $_SESSION['PERFIL'];
		$where = '';
		if ($_SESSION['PERFIL'] == 11 && $_SESSION['ZONA'] != '0') { // COLOCAR ESTA VALIDACION EN LAS CONSULTAS
			$where = " AND RL.ZONA = '{$_SESSION['ZONA']}'";
		}
		$this->sql = "SELECT
						RL.FOLIO, T.NOMBRE TIPO, RL.LATITUD, RL.LONGITUD,
					    IMG.IDIMAGEN, IMG.URLIMAGEN, IMG.TIPOLAMPARA,IMG.WATTS,
					    CASE WHEN RL.IDTIPOLUMINARIA=1 THEN '12' ELSE '24' END HRS,
					    CL.UBICACIONFISICA,CUF.NOMBRE UBICACION,
					    CTL.NOMBRETIPOLAMAPARA TECNOLOGIA,
					    IMG.MODELO IDMODELO,EUF.NOMBRE MODELO
					FROM REGISTROLUMINARIA RL
					JOIN TIPOLUMINARIA T ON T.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA AND RL.FECHALEVANTAMIENTO BETWEEN '$inicio' AND '$fin'
					LEFT JOIN IMAGEN IMG ON IMG.IDREGISTROLUMINARIA = RL.IDREGISTRO AND IMG.TIPOIMAGEN='EVIDENCIA'
					JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
					LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
					LEFT JOIN CATALOGO_TIPOLAMPARA CTL ON CTL.IDTIPOLAMPARA = IMG.TIPOLAMPARA
					LEFT JOIN EXTRAUBICACIONFISICA EUF ON EUF.IDEXTRAUBICACIONFISICA = IMG.MODELO
					WHERE RL.ESTATUS=1 $where";
		try {
			$this->result = array('codigo'=>true, 'data'=> $this->conexion->getResults($this->sql));
		} catch (\Exception $e) {
			$this->result = array('code' => false, 'data' => $e->getMessage());
		}
		return $this->result;
	}

	public function teams()
	{
		$utilities = new utilities;

		$perfil = $_SESSION['PERFIL'];
		$complete = "";
		$c = "";
		$eficiencia = [];

		$idusuario = isset($_POST['keysupervisor']) ? $_POST['keysupervisor']:$_SESSION['IDUSUARIO'];

		if ($perfil==4) {
			$complete = "AND IDUSUARIO=".$idusuario;
			$c = "AND SUPERVISOR = ".$idusuario;
			$eficiencia = Self::eficiencia_usuario($idusuario);
		}
		if (isset($_POST['keysupervisor'])) {
			$complete = "AND IDUSUARIO=".$idusuario;
			$c = "AND SUPERVISOR = ".$idusuario;
			$eficiencia = Self::eficiencia_usuario($idusuario);
		}

		$supervisores = $this->conexion->getResults("SELECT IDUSUARIO, CONCAT(NOMBRE, ' ', APATERNO, ' ', AMATERNO) SUPERVISOR FROM USUARIO WHERE IDPERFIL = 4 AND STATUS = 1 $complete");
		$supervisores = $utilities->objetivisa('IDUSUARIO', $supervisores);

		$current_date = date('d-m-Y');

		$sql = "SELECT U.*, IF(T.TOTAL IS NULL, 0,T.TOTAL) TOTAL FROM (
					SELECT SUPERVISOR,IDUSUARIO,CONCAT(NOMBRE,' ',APATERNO,' ',AMATERNO)USUARIO FROM USUARIO WHERE IDPERFIL=9 AND STATUS=1 AND IDUSUARIO NOT IN(2,3,4,5,6,7,8,9,112) $c /*AND SUPERVISOR IS NOT NULL*/)U
				LEFT JOIN (
					SELECT USUARIOLEVANTAMIENTO, COUNT(*) TOTAL FROM REGISTROLUMINARIA WHERE DATE_FORMAT(FECHALEVANTAMIENTO,'%d-%m-%Y') = '$current_date' AND ESTATUS=1 GROUP BY USUARIOLEVANTAMIENTO
				) T ON T.USUARIOLEVANTAMIENTO = U.IDUSUARIO ORDER BY SUPERVISOR,USUARIO";

		$usuarios_supervisor = $utilities->objetivisa('SUPERVISOR',$this->conexion->getResults($sql));

		$this->result['supervisores'] = $supervisores;
		$this->result['usuarios'] = $usuarios_supervisor;
		$this->result['eficiencia'] = $eficiencia;
		return $this->result;
	}

}



?>
