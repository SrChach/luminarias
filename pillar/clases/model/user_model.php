<?php
session_start();
/**/

class User{
	protected $conexion;
	protected $utilities;
	protected $result;
	protected $Error;
	protected $sql;
	protected $mensaje;

	function __construct(){
		$this -> conexion = new conexion;
		//$this -> utilities = new utilities;
		$this -> Error = new ErrorController;
		$this -> result = null;
		$this -> mensaje = ['SIN DATOS',
		'EXITO! SE HA CANCELADO ESTA CUENTA.',
		'EXITO! SE ACTIVO ESTA CUENTA.',
		'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO',
		'EXITO! SE ELIMINO CUENTA DE USUARIO.',
		'EXITO! SE ACTULIZARÓN DATOS DE LA CUENTA',
		'LO SIENTO! NO SE ACTUALIZARÓN DATOS DE LA CUENTA'] ;
	}

	public function getAll()
	{
		$complete = "";
		if ($_SESSION['PERFIL']==5) {
			$complete= "WHERE U.IDPERFIL = 9";
		}
		$this -> sql = "SELECT
							U.*, P.PERFIL
						FROM USUARIO U
						JOIN perfil P ON P.IDPERFIL = U.IDPERFIL
						$complete";

		try {
			$this -> result = $this -> conexion -> getResults($this -> sql);
			if ($this -> result != null) {
				$this -> result = array('codigo' => true, 'data' => $this -> result);
			}else{
				$this -> result = $this -> Error -> ErrorMessages('Null');
			}
		} catch (Exception $e) {
			$this -> result = array('codigo' => false, 'data' => $this -> $e);
		}

		//$this -> result = $this -> conexion -> getResults($this -> sql);

		return $this -> result;
	}

	public function downUp_Account($id, $values='',$status)
	{
		$this -> sql = "UPDATE USUARIO SET STATUS = $status WHERE IDUSUARIO = $id";
		try {
			$this -> result = $this -> conexion -> insertUpdate ($this -> sql);
			if ($this -> result) {
				if ($status==0) {
					$this -> result = array ('codigo' => $this -> result, "message" => $this -> mensaje[1]);
				}else{
					$this -> result = array ('codigo' => $this -> result, "message" => $this -> mensaje[2]);
				}
			}else{
				$this -> result = array ('codigo' => false, "message" => $this -> mensaje[3]);
			}
		} catch (Exception $e) {
			$this -> result = array ('codigo'=> false, 'message' => $e);
		}
		return $this -> result;
	}

	public function get($id, $values='',$status)
	{
		$this->sql = "SELECT IDUSUARIO,NOMBRE,APATERNO,AMATERNO,EMAIL,CELULAR,IDPERFIL,EMAILREAL,PASS,IMEI,STATUS,SUPERVISOR FROM USUARIO WHERE IDUSUARIO=$id";
		try {
			$this -> result = $this -> conexion -> getResult($this -> sql);
			if ($this -> result != null) {
				$data['user'] = $this -> result;

				$this->sql = "SELECT * FROM perfil";
				$this -> result = $this -> conexion -> getResults($this -> sql);
				$data['perfiles'] = $this -> result;
				$data['supervisores'] = $this -> conexion -> getResults("SELECT IDUSUARIO, CONCAT(NOMBRE, ' ', APATERNO, ' ', AMATERNO) SUPERVISOR FROM USUARIO WHERE IDPERFIL = 4 AND STATUS = 1");


				$this -> result = array('codigo' => true, 'data' => $data);
			}else{
				$this -> result = array('codigo' => false, 'message' => 'EL USUARIO SOLICITADO NO EXISTE');
			}
		} catch (Exception $e) {
			$this -> result = array('codigo' => false, 'data' => $this -> $e);
		}
		return $this -> result;
	}

	public function update($id,$values='',$status='')
	{
		$this->sql = "UPDATE USUARIO SET  ";
		$postData = filter_input_array(INPUT_POST);
		$i=0;
		foreach ($postData as $key => $value) {
			if ($key!='csrf_token' and $key!='key' and $key!='opcion' and $key!='action' and $key!='null') {
				$this->sql .= "$key = '$value', ";
			}
		}
		$this->sql .= "WHERE IDUSUARIO=$id";
		$this->sql = str_replace(', WHERE',' WHERE',$this->sql);

		try {
			$this -> result = $this -> conexion -> insertUpdate ($this -> sql);
			if ($this -> result===true) {
				if ($status==0) {
					$this -> result = array ('codigo' => $this -> result, "message" => $this -> mensaje[5],"data"=>Self::get($id,$values,$status));
				}else{
					$this -> result = array ('codigo' => $this -> result, "message" => $this -> mensaje[6]);
				}
			}else{
				$this -> result = array ('codigo' => false, "message" => $this -> mensaje[3]);
			}
		} catch (Exception $e) {
			$this -> result = array ('codigo'=> false, 'message' => $e);
		}
		return $this -> result;

	}

	public function save($id,$values='',$status='')
	{
		$this->sql = "INSERT INTO USUARIO";

		$fileds = "(";

		foreach ($_POST as $key => $value) {
			if ($key!="csrf_token" and $key!="opcion" and $key!="action") {
				$fileds .= "$key,";
			}
		}
		$fileds .= ")";

		$this->sql .= $fileds;

		$values = "";

		$this->sql.=" VALUES(";
		foreach ($_POST as $key => $value) {
			if ($key!="csrf_token" and $key!="opcion" and $key!="action") {
				if ($value==="") {
					$value=0;
				}
				$values .= "'$value',";
			}
		}
		$this->sql .= $values;
		$this->sql .= ")";

		$this->sql = str_replace(",)",")",$this->sql);

		//return $this->sql;

		try {
			$this -> result = $this -> conexion -> insertUpdate ($this -> sql);
			if ($this -> result===true) {
				$this -> result = array ('codigo' => $this -> result, "message" => 'Se registro nuevo usuario');
			}else{
				$this -> result = array ('codigo' => false, "message" => $this -> result );
			}
		} catch (Exception $e) {
			$this -> result = array ('codigo'=> false, 'message' => $e);
		}
		return $this -> result;

	}

	public function setNewPass($id,$values='',$status='')
	{
		$pass = $_POST['newpass'];
		$KEY = $_POST['key'];
		$this->sql = "UPDATE USUARIO SET PASS='$pass', VERIFICADO=1 WHERE IDUSUARIO=$KEY";
		try {
			$this -> result = $this -> conexion -> insertUpdate ($this -> sql);
			if ($this -> result===true) {
				$this -> result = array ('codigo' => $this -> result, "message" => 'Se cambiarón datos de cuenta. Se cerrará esta sesión para ingresar con sus nuevos accesos!');
			}else{
				$this -> result = array ('codigo' => false, "message" => $this -> result );
			}
		} catch (Exception $e) {
			$this -> result = array ('codigo'=> false, 'message' => $e);
		}
		return $this -> result;
	}


}














?>
