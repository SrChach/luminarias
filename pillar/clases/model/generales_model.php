<?php
//session_start();

date_default_timezone_set('America/Mexico_City');

/**/

class Generales{
	protected $conexion;
	protected $utilities;
	protected $result;
	protected $Error;
	protected $sql;
	protected $mensaje;

	function __construct(){
		$this -> conexion = new conexion;
		//$this -> utilities = new utilities;
		$this -> Error = new ErrorController;
		$this -> result = null;
		$this -> mensaje = ['SIN DATOS',
		'EXITO! SE HA CANCELADO ESTA CUENTA.',
		'EXITO! SE ACTIVO ESTA CUENTA.',
		'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO',
		'EXITO! SE ELIMINO CUENTA DE USUARIO.'] ;
	}

	public function getPerfiles()
	{
		$this -> sql = "SELECT * FROM perfil ORDER BY PERFIL";
		try {
			$this -> result = $this -> conexion -> getResults($this -> sql);
			$this -> result = array('codigo' => true, 'data' => $this -> result);
		} catch (Exception $e) {
			$this -> result = array('codigo' => false, 'data' => $this -> $e);
		}
		return $this -> result;
	}

	public function getCatalogo($table,$order=null)
	{
		$this -> sql = "SELECT * FROM $table";

		if ($order!=null) $this -> sql .= " ORDER BY $order";
		$this -> result = $this -> conexion -> getResults($this -> sql);

		return $this -> result;
	}

	public function getCatalogoCustom($sql)
	{
		$this -> result = $this -> conexion -> getResults($sql);

		return $this -> result;
	}

	/** 
	 * value: [string] Nombre que llevará el campo 'value' en el <option/>
	 * text: [string] Nombre que llevará la opción visible en el <option/>
	 * extras: [array de strings, opcional] Campos no imprimibles, pero que se pueden usar
	 */
	public function getSelectOptions($table, $value, $text = 'NOMBRE', $extra_fields = null){
		$other_fields = '';
		if($extra_fields != null){
			$other_fields = (sizeof($extra_fields) > 0) ? ',' : '';
			$other_fields .= implode(',', $extra_fields);
		}
		$this->sql = "SELECT $value as 'value', $text as 'text' $other_fields FROM $table ORDER BY text";

		$this->result = $this->conexion->getResults( $this->sql );
		return $this->result;
	}

	public function getDatesRegistros()
	{
		$this -> sql = "SELECT DATE_FORMAT(FECHA,'%d-%m-%Y') AS FECHA
			FROM (
				SELECT DISTINCT DATE_FORMAT(FECHALEVANTAMIENTO, '%Y-%m-%d') AS FECHA  
					FROM REGISTROLUMINARIA WHERE ESTATUS = 1
					ORDER BY FECHA
			) ORDENADAS";
		$this -> result = $this -> conexion -> getResults($this -> sql);
		return $this -> result;
	}

	public function makeCsv($dates)
	{
		session_start();
		if (isset($_SESSION['_TOKEN'])) {
			//Self::makeExcel();
			Self::makeCsvAll($dates);
		}else{
			Self::makeCsvAll($dates);
		}

	}
	public function makeCsvAll($data)
	{
		$inicio = date("Y-m-d",strtotime($data['inicio']));
		$fin = date("Y-m-d",strtotime($data['fin'].' + 1 days'));
		$type = $data['tipo'];
		if ($type == "luminaria") {
			Self::makeCsvLuminarias($inicio,$fin, $data['fin']);
		}elseif ($type == "semaforo") {
			Self::makeCsvSemaforos($inicio,$fin, $data['fin']);
		}elseif ($type == "cd") {
			Self::makeCsvCd($inicio,$fin, $data['fin']);
		}else{
			echo json_encode(array('code'=>false,'message'=>'tipo no encontrado'));
		}




	}
	public function makeCsvLuminarias($inicio,$fin,$fin_org)
	{
		$csv_export = "FOLIO, USUARIO, IMEILEVANTAMIENTO, SUPERVISOR, TIPO, FECHA_INVENTARIO, LATITUD, LONGITUD, TIPOPOSTE, TENCNOLOGIALUMINARIA, TIPOCARCAZA, NUMLUMIARIAS, HORAS, WATTS, KILOWATTS, PORCPERDIDABALASTRO, CARGATOTALKW, TIPOBALASTRO, IMAGEN, CARGAACEPTADA, CVEMANZANA, CALLE, ENTRECALLES, TIPOVIALIDAD, REDVIAL, MUNICIPIO_ALCALDIA, COLONIA_POBLACION, ENTIDADFEDERATIVA, CODIGOPOSTAL, MEDIDOR";
		$sql = "SELECT
					RL.FOLIO, CONCAT(U.NOMBRE,' ',U.APATERNO, ' ', U.AMATERNO) USUARIO, RL.IMEILEVANTAMIENTO,
					CONCAT(SUP.NOMBRE,' ',SUP.APATERNO, ' ', SUP.AMATERNO) SUPERVISOR, TL.NOMBRE TIPO,
					DATE_FORMAT(RL.FECHALEVANTAMIENTO,'%d-%m-%Y')FECHA_INVENTARIO,RL.LATITUD,RL.LONGITUD, TP.TIPOPOSTE,
				    TCL.NOMBRE TENCNOLOGIALUMINARIA, TC.TIPOCARCAZA, RL.CANTIDAD NUMLUMIARIAS,
					'12' HORAS, W.VALOR WATTS,'' KILOWATTS,''PORCPERDIDABALASTRO, '' CARGATOTALKW, TB.TIPOBALASTRO,
					concat('http://207.249.158.187/cfe_luminarias/storage/images/',IMG.URLIMAGEN) IMAGEN,
					'' CARGAACEPTADA, RL.CVEMANZANA, '' CALLE, ''ENTRECALLES,''TIPOVIALIDAD,
					CASE WHEN CL.TIPOLUMINARIA=2 THEN 'ALUMBRADO PUBLICO (VÍA SECUNDARIA)' ELSE 'ALUMBRADO PUBLICO (VÍA PRIMARIA)' END REDVIAL,
					''MUNICIPIO_ALCALDIA,''COLONIA_POBLACION,'' ENTIDADFEDERATIVA,'' CODIGOPOSTAL,''MEDIDOR
				FROM (SELECT * FROM REGISTROLUMINARIA WHERE FECHALEVANTAMIENTO BETWEEN '$inicio' AND '$fin') RL
				JOIN USUARIO U ON U.IDUSUARIO = RL.USUARIOLEVANTAMIENTO
				LEFT JOIN USUARIO SUP ON U.SUPERVISOR = SUP.IDUSUARIO
				JOIN IMAGEN IMG ON IMG.IDREGISTROLUMINARIA=RL.IDREGISTRO AND TIPOIMAGEN='EVIDENCIA'
				JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
				LEFT JOIN TIPOPOSTE TP ON TP.IDTIPOPOSTE = IMG.TIPOPOSTE
				LEFT JOIN CATALOGO_CARACTERISTICATECNICA TCL ON TCL.IDCARATERISTICATECNICA = IMG.TIPOLAMPARA
				LEFT JOIN TIPOCARCAZA TC ON TC.IDTIPOCARCAZA = IMG.TIPOCARCAZA
				LEFT JOIN CATALOGO_WATTS W ON W.IDWATTS = IMG.WATTS
				LEFT JOIN TIPOBALASTRO TB ON TB.IDTIPOBALASTRO = IMG.TIPOBALASTRO
				LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
				WHERE RL.ESTATUS=1 AND RL.IDTIPOLUMINARIA=1 ";

		$this->result = $this->conexion -> getResults($sql);
		foreach ($this->result as $c) {
    		$csv_export.= '
    		';
      		// create line with field values
    		foreach ($c as $d){
    			$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
    			$reemplazar=array("", "", "", "");
    			$csv_export .= str_ireplace($buscar,$reemplazar, str_replace('\n','', utf8_decode ( str_replace('\n','', str_replace(',','' ,trim ( $d ) ) ) ))). ',';
    		}
        }

        header("Content-type: text/x-csv");
        header("Content-Disposition: attachment; filename=luminarias_de_".$inicio."_hasta_".$fin_org.".csv");
        header("Content-Type: application/force-download");
        echo $csv_export;
	}

	public function makeCsvSemaforos($inicio,$fin,$fin_org)
	{
		$csv_export = "FOLIO, USUARIO, IMEILEVANTAMIENTO, SUPERVISOR, TIPO, FECHA_INVENTARIO, LATITUD, LONGITUD, TIPOCABEZAL, TECNOLOGIACABEZAL, NUMEROASPECTOS, HORAS, WATTS, KILOWATTS, IMAGEN, CARGAACEPTADA, CVEMANZANA, CALLE, ENTRECALLES, TIPOVIALIDAD, REDVIAL, MUNICIPIO_ALCALDIA, COLONIA_POBLACION, ENTIDADFEDERATIVA, CODIGOPOSTAL";
		$sql = "SELECT
						RL.FOLIO,CONCAT(U.NOMBRE,' ',U.APATERNO, ' ', U.AMATERNO) USUARIO, RL.IMEILEVANTAMIENTO,
						CONCAT(SUP.NOMBRE,' ',SUP.APATERNO, ' ', SUP.AMATERNO) SUPERVISOR,
						TL.NOMBRE TIPO, DATE_FORMAT(RL.FECHALEVANTAMIENTO,'%d-%m-%Y')FECHA_INVENTARIO,RL.LATITUD,RL.LONGITUD,
						TC.TIPOCABEZAL,TCC.TECNOLOGIACABEZAL,
						TS.NOMBRE NUMEROASPECTOS,
						'24'HORAS,
						W.VALOR WATTS,'' KILOWATTS,
						concat('http://207.249.158.187/cfe_luminarias/storage/images/',IMG.URLIMAGEN) IMAGEN,
						'' CARGAACEPTADA,
						RL.CVEMANZANA,
						'' CALLE, ''ENTRECALLES,''TIPOVIALIDAD,
						'' REDVIAL,
						'' MUNICIPIO_ALCALDIA,'' COLONIA_POBLACION,'' ENTIDADFEDERATIVA,'' CODIGOPOSTAL
					FROM ( SELECT * FROM REGISTROLUMINARIA WHERE FECHALEVANTAMIENTO BETWEEN '$inicio' AND '$fin') RL
					JOIN USUARIO U ON U.IDUSUARIO = RL.USUARIOLEVANTAMIENTO
					LEFT JOIN USUARIO SUP ON U.SUPERVISOR = SUP.IDUSUARIO
					JOIN IMAGEN IMG ON IMG.IDREGISTROLUMINARIA=RL.IDREGISTRO AND TIPOIMAGEN='EVIDENCIA'
					JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
					LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
					LEFT JOIN TIPOCABEZAL TC ON TC.IDTIPOCABEZAL= IMG.TIPOCABEZAL
					LEFT JOIN TECNOLOGIACABEZAL TCC ON TCC.IDTECNOLOGIACABEZAL= IMG.TECNOLOGIACABEZAL
					LEFT JOIN TIPOSEMAFORO TS ON TS.IDTIPOSEMAFORO = CL.TIPOLUMINARIA
					LEFT JOIN CATALOGO_WATTS W ON W.IDWATTS = IMG.WATTS
					WHERE RL.ESTATUS=1 AND RL.IDTIPOLUMINARIA=2";

		$this->result = $this->conexion -> getResults($sql);

		foreach ($this->result as $c) {
			$csv_export.= '
			';
			// create line with field values
			foreach ($c as $d){
				$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
    			$reemplazar=array("", "", "", "");
    			$csv_export .= str_ireplace($buscar,$reemplazar, str_replace('\n','', utf8_decode ( str_replace('\n','', str_replace(',','' ,trim ( $d ) ) ) ))). ',';
			}
		}

		header("Content-type: text/x-csv");
		header("Content-Disposition: attachment; filename=semaforos_de_".$inicio."_hasta_".$fin_org.".csv");
		header("Content-Type: application/force-download");
		echo $csv_export;
	}

	public function makeCsvCd($inicio,$fin,$fin_org)
	{
		$csv_export = "FOLIO, USUARIO, IMEILEVANTAMIENTO, SUPERVISOR, TIPO, FECHA_INVENTARIO, CVEMANZANA, CALLE, ENTRECALLES, COLONIA_POBLACION, MUNICIPIO_ALCALDIA, ENTIDADFEDERATIVA, CODIGOPOSTAL, TIPOVIALIDAD, REDVIAL, EQUIPO_ATRIBUTO, USO, FABRICANTE, MARCA, MODELO, EMPRESAUSUARIA, CAPACIDAD_W, TIPOACOMETIDA, ENERGIZADO, ENCENDIDO_HRS, FECHA_CENSO, LATITUD, LONGITUD, COMPAÑIA, IMAGEN";
		$sql = "SELECT
						RL.FOLIO,CONCAT(U.NOMBRE,' ',U.APATERNO, ' ', U.AMATERNO) USUARIO, RL.IMEILEVANTAMIENTO,
						CONCAT(SUP.NOMBRE,' ',SUP.APATERNO, ' ', SUP.AMATERNO) SUPERVISOR,
						TL.NOMBRE TIPO, DATE_FORMAT(RL.FECHALEVANTAMIENTO,'%d-%m-%Y')FECHA_INVENTARIO,
						RL.CVEMANZANA,
						'' CALLE, ''ENTRECALLES,
						'' COLONIA_POBLACION,'' MUNICIPIO_ALCALDIA,'' ENTIDADFEDERATIVA,'' CODIGOPOSTAL,
						''TIPOVIALIDAD,'' REDVIAL,
						TCD.NOMBRE EQUIPO_ATRIBUTO,
						'' USO,
						F.FABRICANTE,
						M.MARCA,
						'' MODELO,
						CE.NOMBREEMPRESA EMPRESAUSUARIA,
						W.VALOR CAPACIDAD_W,
						TA.TIPOACOMETIDA,
						'' ENERGIZADO,
						'24' ENCENDIDO_HRS,
						DATE_FORMAT(RL.FECHALEVANTAMIENTO,'%d-%m-%Y')FECHA_CENSO,
						RL.LATITUD,RL.LONGITUD,
						C.COMPANIA COMPAÑIA,
						concat('http://207.249.158.187/cfe_luminarias/storage/images/',IMG.URLIMAGEN) IMAGEN
					FROM ( SELECT * FROM REGISTROLUMINARIA WHERE FECHALEVANTAMIENTO BETWEEN '$inicio' AND '$fin') RL
					JOIN USUARIO U ON U.IDUSUARIO = RL.USUARIOLEVANTAMIENTO
					LEFT JOIN USUARIO SUP ON U.SUPERVISOR = SUP.IDUSUARIO
					JOIN IMAGEN IMG ON IMG.IDREGISTROLUMINARIA=RL.IDREGISTRO AND TIPOIMAGEN='EVIDENCIA'
					JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
					LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
					LEFT JOIN TIPOCARGADIRECTA TCD ON TCD.IDTIPOCARGADIRECTA = CL.TIPOLUMINARIA
					LEFT JOIN FABRICANTE F ON F.IDFABRICANTE = IMG.FABRICANTE
					LEFT JOIN MARCA M ON M.IDMARCA = IMG.MARCA
					LEFT JOIN CATALOGO_EMPRESA CE ON CE.IDEMPRESA = CL.EMPRESA
					LEFT JOIN CATALOGO_WATTS W ON W.IDWATTS = IMG.WATTS
					LEFT JOIN TIPOACOMETIDA TA ON TA.IDTIPOACOMETIDA = IMG.TIPOACOMETIDA
					LEFT JOIN COMPANIA C ON C.IDCOMPANIA = IMG.COMPANIA
					WHERE RL.ESTATUS=1 AND RL.IDTIPOLUMINARIA=3";

		$this->result = $this->conexion -> getResults($sql);

		foreach ($this->result as $c) {
			$csv_export.= '
			';
			// create line with field values
			foreach ($c as $d){
				$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
    			$reemplazar=array("", "", "", "");
    			$csv_export .= str_ireplace($buscar,$reemplazar, str_replace('\n','', utf8_decode ( str_replace('\n','', str_replace(',','' ,trim ( $d ) ) ) ))). ',';
			}
		}

		header("Content-type: text/x-csv");
		header("Content-Disposition: attachment; filename=cargas_directas_de_".$inicio."_hasta_".$fin_org.".csv");
		header("Content-Type: application/force-download");
		echo utf8_decode(utf8_decode($csv_export));
	}


	public function makeExcel()
	{

	}

}














?>
