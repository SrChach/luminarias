<?php
session_start();
/**/

class ListadoLuminarias{
	protected $conexion;
	protected $utilities;
	protected $result;
	protected $Error;
	protected $sql;
	function __construct(){
		$this -> conexion = new conexion;
		$this -> result = null;
	}

	public function getListado($dateinit, $dateend)
	{
		$inicio = date("Y-m-d",strtotime($dateinit));
		$fin = date("Y-m-d",strtotime($dateend.' + 1 days'));
		$profile = $_SESSION['PERFIL'];
		$where = '';
		$campos_extra = '';

		if($_SESSION['PERFIL'] == 12) // Alcaldia - Luminarias
			$where = " AND RL.IDTIPOLUMINARIA = 1";
		if($_SESSION['PERFIL'] == 14) // Semaforos - Semaforos
			$where = " AND RL.IDTIPOLUMINARIA = 2";
		if($_SESSION['PERFIL'] == 15) // Telmex - C. Directas
			$where = " AND RL.IDTIPOLUMINARIA = 3";

		if ($_SESSION['PERFIL'] == 11 && $_SESSION['ZONA'] != '0') // COLOCAR ESTA VALIDACION EN LAS CONSULTAS
			$where = " AND RL.ZONA = '{$_SESSION['ZONA']}'";

		if ($_SESSION['PERFIL'] == 12 && $_SESSION['DELEGACION'] != null)
			$where = " AND RL.DELLOCMUN = '{$_SESSION['DELEGACION']}'";

		if( in_array($_SESSION['PERFIL'], [12, 14, 15]) )
			$campos_extra .= ', RL.CANTIDAD';

		if( in_array($_SESSION['PERFIL'], [11]) )
			$campos_extra .= ', RL.IMEILEVANTAMIENTO';
		
		if( in_array($_SESSION['PERFIL'], [12]) )
			$campos_extra .= ', RL.CVEMANZANA';

		$sql_registros_por_fecha = "SELECT
				RL.IDREGISTRO, RL.FOLIO, RL.ZONA, Z.NOMBRE NOMBREZONA,
				TL.NOMBRE TIPO, DATE_FORMAT(RL.FECHALEVANTAMIENTO,'%d-%m-%Y') FECHAL,
				CONCAT(U.NOMBRE) USUARIO, RL.LATITUD, RL.LONGITUD $campos_extra
			FROM
					REGISTROLUMINARIA RL
				JOIN
					TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
				JOIN
					USUARIO U ON U.IDUSUARIO = RL.USUARIOLEVANTAMIENTO
				LEFT JOIN
					ZONA Z ON Z.IDZONA = RL.ZONA
			WHERE RL.ESTATUS = 1 AND RL.FECHALEVANTAMIENTO BETWEEN '$inicio' AND '$fin' $where
			ORDER BY RL.FECHALEVANTAMIENTO";

		header('Content-type: Application/json');
		if($_SESSION['PERFIL'] == '12' && $_SESSION['DELEGACION'] == null)
			return ['status'=>false, 'message'=>'Aun no tienes delegacion asignada. Contacta a soporte'];

		if( in_array($_SESSION['PERFIL'], ['6', '11', '12', '13', '14', '15', '888', '5']) ){
			$data = $this->conexion->getResults($sql_registros_por_fecha);
			
			$this->result['status'] = true;
			$this->result['datas'] = $data;
			$this->result['total'] = count($data);
		} else {
			$this->result['status'] = false;
			$this->result['message'] = "Ocurrió un error al cargar los datos o no estás autorizado para verlos";
		}

		return $this->result;
	}

	public function getdetalle($key)
	{
		$sql = "SELECT * FROM REGISTROLUMINARIA WHERE IDREGISTRO=$key";
		$registro = $this -> conexion -> getResults($sql);
		$type = null;
		if (count($registro)>0) {
			$type = $registro[0]['IDTIPOLUMINARIA'];
		}
		if ($type==1) {
			$this -> sql = "SELECT RL.*,TL.NOMBRE TIPO,
								CASE WHEN CL.TIPOLUMINARIA=2 THEN 'ALUMBRADO PUBLICO (VÍA SECUNDARIA)' ELSE 'ALUMBRADO PUBLICO (VÍA PRIMARIA)' END TIPOLUMINARIA,CUF.IDUBICACIONFISICA,
								CUF.NOMBRE UBICACIONFISICA,CL.EXTRAUBICACIONFISICA,CCT.NOMBRE CARACTERISTICATECNICA
							FROM REGISTROLUMINARIA RL
								LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
								LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
								LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
								LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA
							WHERE RL.IDREGISTRO=$key";
		}
		if ($type==2) {
			$this -> sql = "SELECT
								RL.*,TL.NOMBRE TIPO,TS.NOMBRE TIPOLUMINARIA,CUF.IDUBICACIONFISICA,
								CUF.NOMBRE UBICACIONFISICA,CL.EXTRAUBICACIONFISICA,CCT.NOMBRE CARACTERISTICATECNICA
							FROM REGISTROLUMINARIA RL
								LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
								LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
    							LEFT JOIN TIPOSEMAFORO TS ON TS.IDTIPOSEMAFORO = CL.TIPOLUMINARIA
								LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
								LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA
							WHERE RL.IDREGISTRO=$key";
		}

		if ($type==3) {
			$this -> sql = "SELECT
								RL.*,TL.NOMBRE TIPO,TCD.NOMBRE TIPOLUMINARIA,
								CUF.NOMBRE UBICACIONFISICA,CUF.IDUBICACIONFISICA,CL.EXTRAUBICACIONFISICA,CCT.NOMBRE CARACTERISTICATECNICA,
								CE.NOMBREEMPRESA
							FROM REGISTROLUMINARIA RL
								LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
								LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
    							LEFT JOIN TIPOCARGADIRECTA TCD ON TCD.IDTIPOCARGADIRECTA = CL.TIPOLUMINARIA
								LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
								LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA
								LEFT JOIN CATALOGO_EMPRESA CE ON CE.IDEMPRESA = CL.EMPRESA
							WHERE RL.IDREGISTRO=$key";
		}


		$this -> result = $this -> conexion -> getResults($this -> sql);
		if (count($this -> result)>0) {
			$data['generales'] = $this -> result[0];
		}else{
			$data['generales'] = $this -> result;
		}


		$this -> sql = "SELECT * FROM IMAGEN WHERE IDREGISTROLUMINARIA = $key";
		$this -> result = $this -> conexion -> getResults($this -> sql);
		$data['images'] = $this -> result;
		$data['type'] = $type;
		//$data['reg'] = $registro;

		return $data;
	}



}














?>
