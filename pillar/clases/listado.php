<?php

/**/

class Listado{
	protected $conexion;
	protected $utilities;
	protected $result;
	protected $Error;
	protected $sql;
	function __construct(){
		$this -> conexion = new conexion;
		$this -> utilities = new utilities;
		$this -> Error = new ErrorController;
		$this -> result = null;
	}

	public function getGeneral($idDel='',$idSub='',$idMun='',$idLoc='')
	{	
		$this -> sql = "SELECT COUNT(*) TOTAL FROM (SELECT L.DELEGACION ID_DEL,DS.DELEGACION,L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
        					CASE WHEN P.BENEFICIOS IS NULL THEN 0 ELSE P.BENEFICIOS END BENEFICIOS
						FROM INTEGRADOR.LISTADO L
							LEFT JOIN (
    							SELECT P.IDLISTADO, COUNT (*)BENEFICIOS
    							FROM INTEGRADOR.PADRON P
    							GROUP BY P.IDLISTADO
							) P ON L.IDLISTADO=P.IDLISTADO
							JOIN (SELECT ID_DEL,DELEGACION FROM INTEGRADOR.DELEGACIONSUBDELEGACION GROUP BY ID_DEL,DELEGACION)DS ON L.DELEGACION = DS.ID_DEL)";

		$count = $this -> conexion -> getResults($this -> sql);

		$this -> sql = "SELECT L.DELEGACION ID_DEL,L.IDLISTADO,DS.DELEGACION DELEGACIÓN,L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
        					CASE WHEN P.BENEFICIOS IS NULL THEN 0 ELSE P.BENEFICIOS END BENEFICIOS
						FROM INTEGRADOR.LISTADO L
							LEFT JOIN (
    							SELECT P.IDLISTADO, COUNT (*)BENEFICIOS
    							FROM INTEGRADOR.PADRON P
    							GROUP BY P.IDLISTADO
							) P ON L.IDLISTADO=P.IDLISTADO
							JOIN (SELECT ID_DEL,DELEGACION FROM INTEGRADOR.DELEGACIONSUBDELEGACION GROUP BY ID_DEL,DELEGACION)DS ON L.DELEGACION = DS.ID_DEL-- AND DS.ID_DEL=9";
		try {
			$this -> result = $this -> conexion -> getResults($this -> sql,250);
			if ($this -> result != null) {
				$this -> result = array('CODIGO' => true, 'DATOS' => $this -> result , 'COUNT' => $count[0]['TOTAL']);
			}else{
				$this -> result = $this -> Error -> ErrorMessages('Null');
			}
		} catch (Exception $e) {
			$this -> result = array('CODIGO' => false, 'DATOS' => $this -> $e);
		}
		
		return $this -> result;	
	}
	function getSubdelegacion($idDel='',$idSub='',$idMun='',$idLoc='')
	{
		$this -> sql = "SELECT COUNT(*) TOTAL FROM ( SELECT DS.ID_SUB,DS.SUBDELEGACION,L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
        					CASE WHEN P.BENEFICIOS IS NULL THEN 0 ELSE P.BENEFICIOS END BENEFICIOS
						FROM INTEGRADOR.LISTADO L
							LEFT JOIN (
    							SELECT P.IDLISTADO, COUNT (*)BENEFICIOS
    							FROM INTEGRADOR.PADRON P
    							GROUP BY P.IDLISTADO
							) P ON L.IDLISTADO=P.IDLISTADO
							JOIN (
      							SELECT ID_SUB,SUBDELEGACION FROM INTEGRADOR.DELEGACIONSUBDELEGACION WHERE ID_DEL=9 GROUP BY ID_SUB,SUBDELEGACION
							)DS ON L.SUBDELEGACION = DS.ID_SUB)";
		$count = $this -> conexion -> getResults($this -> sql);

		$this -> sql = "SELECT DS.ID_SUB,L.IDLISTADO,DS.SUBDELEGACION SUBDELEGACIÓN,L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
        					CASE WHEN P.BENEFICIOS IS NULL THEN 0 ELSE P.BENEFICIOS END BENEFICIOS
						FROM INTEGRADOR.LISTADO L
							LEFT JOIN (
    							SELECT P.IDLISTADO, COUNT (*)BENEFICIOS
    							FROM INTEGRADOR.PADRON P
    							GROUP BY P.IDLISTADO
							) P ON L.IDLISTADO=P.IDLISTADO
							JOIN (
      							SELECT ID_SUB,SUBDELEGACION FROM INTEGRADOR.DELEGACIONSUBDELEGACION WHERE ID_DEL=9 GROUP BY ID_SUB,SUBDELEGACION
							)DS ON L.SUBDELEGACION = DS.ID_SUB";

		try {
			$this -> result = $this -> conexion -> getResults($this -> sql,250);
			if ($this -> result != null) {
				$this -> result = array('CODIGO' => true, 'DATOS' => $this -> result , 'COUNT' => $count[0]['TOTAL']);
			}else{
				$this -> result = $this -> Error -> ErrorMessages('Null');
			}
		} catch (Exception $e) {
			$this -> result = array('CODIGO' => false, 'DATOS' => $this -> $e);
		}
		
		return $this -> result;	
	}
	function getMunicipio($idDel='',$idSub='',$idMun='',$idLoc='')
	{
		$this -> sql = "SELECT COUNT(*) TOTAL FROM ( SELECT L.CVEMUN CVE_MUN,DS.MUNICIPIO,L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
        					CASE WHEN P.BENEFICIOS IS NULL THEN 0 ELSE P.BENEFICIOS END BENEFICIOS
						FROM INTEGRADOR.LISTADO L
							LEFT JOIN (
    							SELECT P.IDLISTADO, COUNT (*)BENEFICIOS
    							FROM INTEGRADOR.PADRON P
    							GROUP BY P.IDLISTADO
							) P ON L.IDLISTADO=P.IDLISTADO AND L.SUBDELEGACION=$idSub
							JOIN (
      							SELECT CVEMUN,NOMBREMUN MUNICIPIO FROM INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD GROUP BY CVEMUN,NOMBREMUN
							)DS ON L.CVEMUN = DS.CVEMUN)";
		$count = $this -> conexion -> getResults($this -> sql);

		$this -> sql = "SELECT L.CVEMUN CVE_MUN,L.IDLISTADO,DS.MUNICIPIO,L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
        					CASE WHEN P.BENEFICIOS IS NULL THEN 0 ELSE P.BENEFICIOS END BENEFICIOS
						FROM INTEGRADOR.LISTADO L
							LEFT JOIN (
    							SELECT P.IDLISTADO, COUNT (*)BENEFICIOS
    							FROM INTEGRADOR.PADRON P
    							GROUP BY P.IDLISTADO
							) P ON L.IDLISTADO=P.IDLISTADO AND L.SUBDELEGACION=$idSub
							JOIN (
      							SELECT CVEMUN,NOMBREMUN MUNICIPIO FROM INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD GROUP BY CVEMUN,NOMBREMUN
							)DS ON L.CVEMUN = DS.CVEMUN";

		try {
			$this -> result = $this -> conexion -> getResults($this -> sql,250);
			if ($this -> result != null) {
				$this -> result = array('CODIGO' => true, 'DATOS' => $this -> result , 'COUNT' => $count[0]['TOTAL']);
			}else{
				$this -> result = $this -> Error -> ErrorMessages('Null');
			}
		} catch (Exception $e) {
			$this -> result = array('CODIGO' => false, 'DATOS' => $this -> $e);
		}
		
		return $this -> result;	
	}

	public function getLocalidad($idDel='',$idSub='',$idMun='',$idLoc='')
	{	
		$this -> sql = "SELECT COUNT (*)TOTAL FROM (SELECT L.CVELOC CVE_LOC,DS.LOCALIDAD,L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
        					CASE WHEN P.BENEFICIOS IS NULL THEN 0 ELSE P.BENEFICIOS END BENEFICIOS
						FROM INTEGRADOR.LISTADO L
							LEFT JOIN (
    							SELECT P.IDLISTADO, COUNT (*)BENEFICIOS
    							FROM INTEGRADOR.PADRON P
    							GROUP BY P.IDLISTADO
							) P ON L.IDLISTADO=P.IDLISTADO AND  L.CVEMUN=$idMun
							JOIN (
      							SELECT CVELOC, NOMBRELOC LOCALIDAD FROM INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD WHERE CVEMUN=$idMun  GROUP BY CVELOC, NOMBRELOC
							)DS ON L.CVELOC = DS.CVELOC)";
		$count = $this -> conexion -> getResults($this -> sql);
		$this -> sql = "SELECT L.CVELOC CVE_LOC,L.IDLISTADO,DS.LOCALIDAD,L.FOLIOCUIS,L.NOMBRE,L.APATERNO,L.AMATERNO,
        					CASE WHEN P.BENEFICIOS IS NULL THEN 0 ELSE P.BENEFICIOS END BENEFICIOS
						FROM INTEGRADOR.LISTADO L
							LEFT JOIN (
    							SELECT P.IDLISTADO, COUNT (*)BENEFICIOS
    							FROM INTEGRADOR.PADRON P
    							GROUP BY P.IDLISTADO
							) P ON L.IDLISTADO=P.IDLISTADO AND  L.CVEMUN=$idMun
							JOIN (
      							SELECT CVELOC, NOMBRELOC LOCALIDAD FROM INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD WHERE CVEMUN=$idMun  GROUP BY CVELOC, NOMBRELOC
							)DS ON L.CVELOC = DS.CVELOC";
		try {
			$this -> result = $this -> conexion -> getResults($this -> sql,250);
			if ($this -> result != null) {
				$this -> result = array('CODIGO' => true, 'DATOS' => $this -> result , 'COUNT' => $count[0]['TOTAL']);
			}else{
				$this -> result = $this -> Error -> ErrorMessages('Null');
			}
		} catch (Exception $e) {
			$this -> result = array('CODIGO' => false, 'DATOS' => $this -> $e);
		}
		
		return $this -> result;	
	}
}














?>