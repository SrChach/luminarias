<?php

ini_set('memory_limit', -1);
ignore_user_abort(1); 
ini_set('max_execution_time',0);


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 

//include_once '/home/integrador/desarrollo_des/pillar/clases/MailGeneral.php';
//include_once '/home/integrador/parametros.php'; 

class Reports2 {
	
	protected $enviaMailPrueba;

	function csv($query,$ruta,$nombre,$idUsuarioLogin,$opcion,$values='',$titulos='',$numRows = 20000){
	
		$skipRows = 0;
		$a = 0;
		$pintaTitulos = 0;
		$csv_export  = "";


		$conexion = new conexion;

		$fp = fopen($ruta, 'w');
		while ( $datos = $conexion->getResults($query,$numRows,$skipRows) ) {
			if($titulos == "" and $pintaTitulos == 0){
				foreach ($datos[0] as $key => $value) {

					if(!is_numeric($key)){
						if($csv_export==""){
							$csv_export = $key ;
						}else{
							$csv_export = $csv_export . "," . $key ; 
						}
					}
				}
				$csv_export.= '
				'; 

				fwrite($fp, $csv_export);
				$titulos = 1;
			}
			
			foreach ($datos as $c) {
				$csv_export  = "";
				foreach ($c as $d){
					$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r","/\t+/","\t","		    ","		    ",chr(9));

					$reemplazar=array('','','','','','','','','');

					$csv_export .= trim(str_ireplace($buscar,$reemplazar, str_replace('\n','', utf8_decode ( str_replace('\n','', str_replace(',','' ,trim ( $d ) ) ) )))). ',';
				} 
				$csv_export.= '
				';  
				
				fwrite($fp, $csv_export);
			}

			$skipRows =  $skipRows + $numRows;
			
		}
		
		fclose($fp);
		
		
		$parametros= new parametros;
		$rutaArchivo=$parametros -> urlReportes().$nombre;
		$otro = new MailRepoGeneral2;
		$respuestapp = $otro -> verificaMail($idUsuarioLogin, $rutaArchivo, $opcion,$values);


		
		return $respuestapp ;

	}
}



?>