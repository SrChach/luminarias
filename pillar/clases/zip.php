<?php
ini_set('memory_limit', -1);
ignore_user_abort(1); 
ini_set('max_execution_time',0); 
/**
 * 
 */
class Zip
{
	protected $conexion;
	protected $result;
	protected $error;
	protected $sql;
	protected $schemaSedever;
	protected $urlPath;
	protected $ok;
	function __construct()
	{
		$this -> conexion = new conexion;
		$this -> error = new ErrorController;
		$this -> result = null;
		$this -> sql = null;
		$this -> schemaSedever = "SEDEVER.";
		$this -> pathImages = "../../../../../sedever/evidencia/Sync/imageData/";
		$this -> pathZip = "../../../../Media/imagenes_impresion_credenciales/";
	}
	function makeZip($lote='',$user='')
	{
		$this -> sql = "select 
						    fotos.FIRMA, 
						    fotos.HUELLA, 
						    fotos.JEFE FOTO 
						    
						  from INTEGRADOR.DETALLELOTEIMPRESION DLI 
						  JOIN INTEGRADOR.CREDENCIAL C ON DLI.FOLIOCUIS = C.FOLIOCUIS 
						  JOIN INTEGRADOR.LISTADO L ON DLI.FOLIOCUIS = L.FOLIOCUIS AND L.PARENTESCO = 1 
						  JOIN INTEGRADOR.DELEGACIONSUBDELEGACION DS ON L.CVEMUN = DS.ID_MUN 
						  JOIN INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD EML ON L.CVELOC = EML.CVELOC 
						  JOIN SEDEVER.CUIS_ENCUESTA e on DLI.FOLIOCUIS = e.FOLIO_CUIS AND e.ENVIADO = 0 
						  JOIN integrador.USUARIO u on C.V_USUARIO = u.IDUSUARIO
						  JOIN SEDEVER.CUIS_VIVIENDA vi on vi.C_ENCUESTA = E.C_ENCUESTA 
						  JOIN SEDEVER.CUIS_INTEGRANTE i on vi.C_VIVIENDA = i.C_VIVIENDA and i.C_CD_PARENTESCO = 1 
						  JOIN SEDEVER.CUIS_PERSONA p on i.C_PERSONA = p.C_PERSONA LEFT 
						  JOIN SEDEVER.CUIS_INTEGRANTE conyuge on vi.C_VIVIENDA = conyuge.C_VIVIENDA and conyuge.C_CD_PARENTESCO = 2 
						  LEFT JOIN SEDEVER.CUIS_PERSONA pConyuge on conyuge.C_PERSONA = pConyuge.C_PERSONA 
						  LEFT JOIN (
						      select * from ( 
						          select ROW_NUMBER() OVER(PARTITION BY v.C_VIVIENDA ORDER BY i.C_INTEGRANTE) AS tupla,i.* from SEDEVER.CUIS_VIVIENDA v 
						          join SEDEVER.CUIS_INTEGRANTE i on v.C_VIVIENDA = i.C_VIVIENDA and i.C_CD_PARENTESCO = 3 and i.EDAD >= 18) 
						          where tupla = 1
						  ) hijo on vi.C_VIVIENDA = hijo.C_VIVIENDA and hijo.C_CD_PARENTESCO = 3 and hijo.EDAD >= 18 
						  LEFT JOIN SEDEVER.CUIS_PERSONA pHijo on hijo.C_PERSONA = pHijo.C_PERSONA 
						  LEFT JOIN (
						          select * from ( 
						              select ROW_NUMBER() OVER(PARTITION BY v.C_VIVIENDA ORDER BY i.C_INTEGRANTE) AS tupla,i.* from SEDEVER.CUIS_VIVIENDA v 
						              join SEDEVER.CUIS_INTEGRANTE i on v.C_VIVIENDA = i.C_VIVIENDA and i.C_CD_PARENTESCO = 3 and i.EDAD >= 18
						          ) 
						          where tupla = 2
						  )hijo2 on vi.C_VIVIENDA = hijo2.C_VIVIENDA and hijo2.C_CD_PARENTESCO = 3 and hijo2.EDAD >= 18 
						  LEFT JOIN SEDEVER.CUIS_PERSONA pHijo2 on hijo2.C_PERSONA = pHijo2.C_PERSONA 
						  LEFT JOIN SEDEVER.CUIS_IMAGENES_VIVIENDA FOTOS ON VI.C_VIVIENDA = FOTOS.C_VIVIENDA 
						  where IDLOTEIMPRESION in ($lote)";

        $this -> result = $this -> conexion -> getResults($this -> sql);
        
        if ($this -> result != null) {
        	
        	$zip = new ZipArchive();										// ZIP FIRMAS
        	$zip->open($this-> pathZip."FIRMA.zip",ZipArchive::CREATE);
        	$zip->addEmptyDir('FIRMA');
        	foreach ($this -> result as $file) {
        		$zip->addFile($this -> pathImages.$file['FIRMA'],"FIRMA/".$file['FIRMA']);	
        	}
 			$zip->close();

 			$zip = new ZipArchive();										// ZIP HUELLAS
        	$zip->open($this-> pathZip."HUELLA.zip",ZipArchive::CREATE);
        	$zip->addEmptyDir('HUELLA');
        	foreach ($this -> result as $file) {
        		$zip->addFile($this -> pathImages.$file['HUELLA'],"HUELLA/".$file['HUELLA']);	
        	}
        	$zip->close();

        	$zip = new ZipArchive();										// ZIP FOTOS
        	$zip->open($this-> pathZip."FOTO.zip",ZipArchive::CREATE);
        	$zip->addEmptyDir('FOTO');
        	foreach ($this -> result as $file) {
        		$zip->addFile($this -> pathImages.$file['FOTO'],"FOTO/".$file['FOTO']);	
        	}
        	$zip->close();

        	$zip = new ZipArchive();										// ZIP CONTENEDOR
        	$zip->open($this-> pathZip."lote_".$lote.".zip",ZipArchive::CREATE);
        	$zip->addFile($this -> pathZip."FIRMA.zip","FIRMA.zip");		//AGREGAR ZIP FIRMA AL ZIP CONTENEDOR
        	$zip->addFile($this -> pathZip."HUELLA.zip","HUELLA.zip");		//AGREGAR ZIP HUELLA AL ZIP CONTENEDOR
        	$zip->addFile($this -> pathZip."FOTO.zip","FOTO.zip");			//AGREGAR ZIP FOTO AL ZIP CONTENEDOR
        	$zip->close();

        	unlink($this-> pathZip."FIRMA.zip");							//ELIMINAR ZIPS TEMPORALES
        	unlink($this-> pathZip."HUELLA.zip");
        	unlink($this-> pathZip."FOTO.zip");

        	$this -> result = true;
        }else{
        	$this -> result = array('CODIGO'=> null,'DATOS'=> 'Sin datos');
        }

        $values = array('lote'=>$lote);
		$mail = new MailRepoGeneral;
		$respuestapp = $mail -> verificaMail($user, null, 'credenciales',$values);

        return $this -> result;
        
	}
}
?>