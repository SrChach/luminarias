<?php

ini_set('memory_limit', -1);
ignore_user_abort(1); 
ini_set('max_execution_time', 300);




ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 



include_once './controlador.php';
include_once './utilities.php';
include_once './QueryController.php';
include_once './conexion.php';
include_once 'ErrorController.php';

// include_once './mailReportes.php';
// include_once './exporta_reportesMail.php';



class ReportsControllerNew extends controlador
{
	protected $result;
	protected $model;
  protected $conexion;
  protected $sql;



  function __construct(){
    $this -> conexion = new conexion;
    $this -> utilities = new utilities;
    $this -> result = null;
  }


  public function traeQuery($opcion,$values){
    $this -> model = new QueryReports;
    $function="query_".$opcion;
    $query = $this -> model -> $function($values);
    return $query;
  }


  public function enviaRespuesta($DATOS){
    $this -> result=$DATOS;
    if ($this -> result != null) {
      $this -> result = array ('CODIGO'=> true, 'DATOS' => $this -> result);
    }else{
      $this -> result = array ('CODIGO'=> false, 'DATOS' => $this -> mensaje[0]);
    }
    return $this -> result;
  }

  /*     ---------------    ESTABLECER A PARTIR DE AQUI     ---------------    */

  public function traeReportes($opcion,$values){
    $query1=$this -> traeQuery($opcion,$values);
    $DATOS['REPORTES'] = $this -> conexion -> getResults($query1);
    return $this -> enviaRespuesta($DATOS);
  }

}




if( isset( $_POST['opcion'] ) ){
  $opcion = $_POST['opcion'];  
  if (isset( $_POST['values'] )) {
    $values = $_POST['values'];  
  } 
}


$reportes = new ReportsControllerNew;
echo json_encode($reportes-> $opcion($opcion,$values)); 


?>