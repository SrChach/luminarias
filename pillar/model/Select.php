<?php
/*!
 * Class Select - G214
 * Copyright G214
 * Created by  José Juan del Prado Lugo 
*/
include_once '../clases/conexion.php';

class Select extends conexion
{
	protected $_tb;
	protected $_sql;
	protected $_data;
	protected $_select = [
							'TODO' => 'SELECT * FROM ',
							'CAMPOS' => 'SELECT ',
							'DE'=>' FROM ' ,
							'DONDE'=>' WHERE '];
	protected $conexion = null;
	protected $result =null;

	function all($_tb)
	{ 
		$this -> _sql = $this -> _select['TODO'] . $_tb;
		$this -> _data = conexion::getResults($this -> _sql);
		return $this -> _data;
	}


  	function columns($_tb,$_columns,$where)
	{
		$this -> _sql = $this -> _select['CAMPOS'] . $_columns . $this -> _select['DE'] . $_tb;
		$this -> _data = conexion::getResults($this -> _sql);
		return $this -> _data;
		
	}
	function where ($_tb,$_complete,$campos){
		if ($campos=='') {
			$this -> _sql = $this -> _select['TODO'] . $_tb . $this -> _select['DONDE'].$_complete;	
		}else{
			$this -> _sql = $this -> _select['CAMPOS'] .$campos[0] .$this -> _select['DE'] . $_tb . $this -> _select['DONDE'].$_complete;
		}
		$this -> _data = conexion::getResults($this -> _sql);
		return $this -> _data;
	}

	function join($tb_pivot,$elements,$where)
	{	
		$this -> sql = "";
		return $elements;
		/*"SELECT * FROM USUARIO U 
			JOIN  TIPOUSUARIO P ON P.IDTIPOUSUARIO = U.PERFIL";*/
	}

	function custom($tb,$select,$where)
	{
		$this -> _sql = $select;
		$this -> _data = conexion::getResults($this -> _sql);
		return $this -> _data;
	}

}
?>