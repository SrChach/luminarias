<?php
/*!
 * Class Select - G214
 * Copyright G214
 * Created by  José Juan del Prado Lugo 
*/
include_once '../clases/conexion.php';

class Update extends conexion
{
	protected $_tb;
	protected $_sql;
	protected $_data;
	protected $_update =['UPDATE'=> 'UPDATE ','SET'=>' SET ', 'WHERE' => ' WHERE '];

	function this($_tb,$_columns,$where)
	{ 
		$this -> _sql = $this -> _update['UPDATE'] . $_tb . $this -> _update['SET'] ;

		for ($i=0; $i < count($_columns); $i++) {
			if ($i!=(count($_columns)-1)) {
				$this -> _sql.= explode('=', $_columns[$i])[0] . '=' . "'". explode('=', $_columns[$i])[1] . "'" . ', ';	
			}else{
				$this -> _sql.= explode('=', $_columns[$i])[0] .'=' . "'" . explode('=', $_columns[$i])[1] ."'" ;
			}
			
		}
		$this -> _sql.= $this -> _update['WHERE'] . $where;
		$this -> _data = conexion::insertUpdate($this -> _sql);
		return $this -> _data;
	}


}
?>