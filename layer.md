# Layer - Guia para guardar datos en nuestra db

## Catalogos

### Tabla *RegistroLuminarias*

| Campo | Tipo | Descripcion | Opciones |
|-:|:-|-|-|
|FOLIO|Numérico|-|-|
|TIPO|Numérico|Indica tipo de Luminaria|**[1]** Luminaria<br>**[2]** Semáforo<br>**[3]** Carga Directa|
|LATITUD|Número con decimales|||
|LONGITUD|Número con decimales|||
|FECHA||||
|USUARIO||Como lo voy a manejar? D:||
|IMEI||Como? D: x2||
|ZONA||||
|OBSERVACION|Texto|||

CSV as csv

``` csv
FOLIO,TIPO,LATITUD
21387617361783,3,19.282777674345
```

csv as a table

|FOLIO|TIPO|LATITUD|
|-|-|-|
|21387617361783|3|19.282777674345|

