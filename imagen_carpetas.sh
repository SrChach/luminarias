_user="cfe_luminarias"
_pass=""

_find_in="../storage/images"
_poner_en="../storage/images"

_listado_fechas="imagen_carpetas.listado"
_limit="LIMIT 30"

mysql --user="$_user" --password="$_pass" -e \
   	"SELECT DISTINCT(DATE_FORMAT(RL.FECHALEVANTAMIENTO, '%Y%m%d')) fecha FROM CFELUMINARIAS.REGISTROLUMINARIA RL ORDER BY fecha $_limit" |
	tail -n +2 > $_listado_fechas

[[ -d $_poner_en ]] || mkdir $_poner_en

echo "-- copiando --"

while IFS= read -r line
do

	[[ -d "$_poner_en/$line"  ]] && echo "$line" || { mkdir "$_poner_en/$line";  echo "creada $line"; }
	find $_find_in -maxdepth 1 -mindepth 1 -name "*$line*.jpg" \
		 -exec bash -c "echo '{}' | sed -e 's/..\/.*\///g' |
			{ \
				read test; \
				[[ -f $_poner_en/$line/\$test ]] \
					&& echo -n '-' \
					|| { \
						convert '{}' -strip -quality 50 -resize 75% -interlace JPEG $_poner_en/$line/\$test && echo -n '.';
						[[ -f $_poner_en/$line/\$test ]] || cp $_poner_en/\$test $_poner_en/$line;
						mysql --user="$_user" --password="$_pass" -sNe \"UPDATE CFELUMINARIAS.IMAGEN SET URLIMAGEN='$line/\$test' WHERE URLIMAGEN='\$test';SELECT row_count()\" |
						{ \
							read afectadas; \
							[[ \$afectadas != 0 ]] && echo -n '.' || mv $_poner_en/$line/\$test $_poner_en/not_in_database | rm -f '{}' | echo \"failed: \$test\" \
						;} \
					;}\
			;}" \;
	echo -e '\n'

done < $_listado_fechas

rm -f $_listado_fechas

echo "-- finalizado --"
