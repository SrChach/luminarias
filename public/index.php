<?php
/**
* 
*/
class RoutesController
{
	protected $url;
	function __construct()
	{
		$this -> url = './portal/';
		$thisUrl=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	}
	public function RedirectToIndex()
	{
		header('Location:'.$this -> url);
	}
}

$controller = new RoutesController;
$controller -> RedirectToIndex();
?>