<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">


<style type="text/css">
  
.social {
    position: fixed;
    bottom:  20px;
    left:15px;
}

.social ul {
  padding: 0px;
  -webkit-transform: translate(-270px, 0);
  -moz-transform: translate(-270px, 0);
  -ms-transform: translate(-270px, 0);
  -o-transform: translate(-270px, 0);
  transform: translate(-270px, 0);
}

.social ul li {
    display: block;
    margin: 3px;
    background: rgba(171, 219, 209, 0.36);
    width: 300px;
    text-align: right;
    padding: 5px;
    -webkit-border-radius: 0px 30px 30px 0px;
    -moz-border-radius: 0px 30px 30px 0px;
    border-radius: 0px 30px 30px 0px;
    -webkit-transition: all 1s;
    -moz-transition: all 1s;
    -ms-transition: all 1s;
    -o-transition: all 1s;
    transition: all 1s;
}

.social ul li:hover {
  -webkit-box-shadow: 0 12px 15px 0 rgba(0,0,0,.35),0 17px 50px 0 rgba(0,0,0,.19);
  box-shadow: 0 12px 15px 0 rgba(0,0,0,.35),0 17px 50px 0 rgba(0,0,0,.19);
  -webkit-transform: translate(110px, 0);
  -moz-transform: translate(110px, 0);
  -ms-transform: translate(110px, 0);
  -o-transform: translate(110px, 0);
  transform: translate(110px, 0);
  background: rgba(173, 178, 177, 0.4);
}

.social ul li:hover a {
  color: #000;
}

.social ul li:hover i {
  color: #fff;
  background: #16a085;
}

.social ul li i {
  margin-right: 0px;
  margin-left: 10px;
  color: #000;
  background: #fff;
  padding: 10px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  width: 40px;
  height: 40px; 
  font-size: 20px;
  background: #ffffff;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -ms-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);

}
</style>



<nav class="social">
          <ul>
              <li><a href="ref"><b>Facebook</b><i class="fa fa-facebook"></i></a></li>
              <li><a href="ref"><b>Twitter</b><i class="fa fa-twitter"></i></a></li>
              <li><a href="ref"><b>Google +</b><i class="fa fa-google-plus"></i></a></li>
              <li><a href="ref"><b>LinkedIn</b><i class="fa fa-linkedin"></i></a></li>
              <li><a href="ref"><b>YouTube</b><i class="fa fa-youtube"></i></a></li>
              <li><a href="ref"><b>Instagram</b><i class="fa fa-instagram"></i></a></li>
          </ul>
</nav>