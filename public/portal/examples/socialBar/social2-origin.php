<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">


<style type="text/css">
	
.social {
    position: fixed;
    bottom: 20px;
    right:15px;
}

.social ul {
  padding: 0px;
  -webkit-transform: translate(270px, 0);
  -moz-transform: translate(270px, 0);
  -ms-transform: translate(270px, 0);
  -o-transform: translate(270px, 0);
  transform: translate(270px, 0);
}

.social ul li {
    display: block;
    margin: 3px;
    background: rgba(171, 219, 209, 0.36);
    width: 300px;
    text-align: left;
    padding: 5px;
    -webkit-border-radius: 30px 0 0 30px;
    -moz-border-radius: 30px 0 0 30px;
    border-radius: 30px 0 0 30px;
    -webkit-transition: all 1s;
    -moz-transition: all 1s;
    -ms-transition: all 1s;
    -o-transition: all 1s;
    transition: all 1s;
}

.social ul li:hover {
  -webkit-transform: translate(-110px, 0);
  -moz-transform: translate(-110px, 0);
  -ms-transform: translate(-110px, 0);
  -o-transform: translate(-110px, 0);
  transform: translate(-110px, 0);
  background: rgba(173, 178, 177, 0.4);
}

.social ul li:hover a {
  color: #000;
}

.social ul li:hover i {
  color: #fff;
  background: #16a085;
}

.social ul li i {
  margin-right: 10px;
  color: #000;
  background: #fff;
  padding: 10px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  width: 40px;
  height: 40px; 
  font-size: 20px;
  background: #ffffff;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -ms-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);

}
</style>


<nav class="social">
          <ul>
              <li><a href="ref"><i class="fa fa-facebook"></i><b>Facebook</b></a></li>
              <li><a href="ref"><i class="fa fa-twitter"></i><b>Twitter</b></a></li>
              <li><a href="ref"><i class="fa fa-google-plus"></i><b>Google +</b></a></li>
              <li><a href="ref"><i class="fa fa-linkedin"></i><b>LinkedIn</b></a></li>
              <li><a href="ref"><i class="fa fa-youtube"></i><b>YouTube</b></a></li>
              <li><a href="ref"><i class="fa fa-instagram"></i><b>Instagram</b></a></li>
          </ul>
</nav>

