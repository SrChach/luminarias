<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

.icon-bar {
  position: fixed;
  bottom: 10px;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 16px;
  transition: all 0.3s ease;
  color: white;
  font-size: 20px;
  border-radius: 50%;
  margin-top:10%;
  margin-left:5px;
}

.icon-bar a:hover {
  background-color: #000;
  -webkit-box-shadow: 0 12px 15px 0 rgba(0,0,0,.35),0 17px 50px 0 rgba(0,0,0,.19);
  box-shadow: 0 12px 15px 0 rgba(0,0,0,.35),0 17px 50px 0 rgba(0,0,0,.19);
  -webkit-transition: all .55s ease-in-out;
  -o-transition: all .55s ease-in-out;
  transition: all .55s ease-in-out;
  -webkit-transform:scale(1.2);
  -moz-transform:scale(1.2);
  -o-transform:scale(1.2);
  opacity: 1;
  z-index: 10;
}

.facebook {
  background: #3B5998;
  color: white;
}


.twitter {
  background: #55ACEE;
  color: white;
}

.google {
  background: #dd4b39;
  color: white;
}

.linkedin {
  background: #007bb5;
  color: white;
}

.youtube {
  background: #bb0000;
  color: white;
}

.content {
  margin-left: 75px;
  font-size: 30px;
}
</style>
<body>

<div class="icon-bar">
  <a href="#" class="facebook"><i class="fa fa-facebook"></i></a> 
  <a href="#" class="twitter"><i class="fa fa-twitter"></i></a> 
  <a href="#" class="google"><i class="fa fa-google"></i></a> 
  <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
  <a href="#" class="youtube"><i class="fa fa-youtube"></i></a> 
</div>


</body>
</html> 
