function ajax(data,url) {  //Funcion estandar ajax
    var regresa;
    $.ajax({
        type: "POST",
        dataType: "HTML",
        url: url,
        data: data,
        async: false,
        success: function (respuesta) {
            regresa = respuesta;
        },
        error: function (e) {
            console.log(e.message);
        }
    });
    return regresa;
    //console.log(data);
}

