<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
header('Content-Type: text/html; charset=UTF-8');

date_default_timezone_set('America/Mexico_City');

include_once 'support.php';

class datasController {
   public function getImg()
   {
        $getDatos=new getDatos();
        $result=$getDatos->imagenes();
        return $result;
   }
}

$datas=new datasController;
if ($_POST['option']=="getImg") {
    $img=$datas->getImg();
    echo json_encode($img);
}
