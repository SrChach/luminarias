<html class="" lang="en">
<head>
  
    <meta charset="UTF-8"><meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">

<style class="cp-pen-styles">
    #qrcode {
  width:160px;
  height:160px;
  margin-top:15px;
    }
</style>
</head>
<body>
    <input id="text" value="jose juan" style="width:80%" type="text"><br>
    <div id="qrcode" title="">
        <img style="display: block;" src="">
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
<script>var qrcode = new QRCode("qrcode");

function makeCode() {
    var elText = document.getElementById("text");

    if (!elText.value) {
        alert("Input a text");
        elText.focus();
        return;
    }

    qrcode.makeCode(elText.value);
}

makeCode();

$("#text").
on("blur", function () {
    makeCode();
}).
on("keydown", function (e) {
    if (e.keyCode == 13) {
        makeCode();
    }
});
//# sourceURL=pen.js
</script>
</body></html>