<?php

/* * ******************************************************           
 * Copyright G214                                         *                                                      
 *                                                        *   
 * Author:  Angel Alam Gonzalez Gonzalez                  *   
 *                                                        *
 * ****************************************************** */


date_default_timezone_set('America/Mexico_City');


class conexion {
    protected $DB;
    protected $DB_USER;
    protected $DB_PASS;
    protected $DB_CHAR;
    protected $conn;
    public $esquema;

    public function __construct() {
        $this -> DB = '//173.192.101.211:1521/uatportdb';
        $this -> DB_USER = 'USRPORTALINTEGRADOR'; 
        $this -> DB_PASS = 'g6p3jXCK24'; 
        $this -> DB_CHAR = 'AL32UTF8';
        $this -> esquema = 'INTEGRADOR.';
        $this -> conn = oci_connect($this->DB_USER, $this->DB_PASS, $this->DB, $this->DB_CHAR);
        if (!$this->conn) {
            $m = oci_error();
            trigger_error(htmlentities($m['message']), E_USER_ERROR);
        }
    }

    public function getResults($query,$maxRows = -1,$skipRow = 0) {

        $stid = oci_parse($this->conn, $query);
        $aa = oci_execute($stid);
        $nrows = oci_fetch_all($stid, $res, $skipRow, $maxRows, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    public function getResult($query) {
        $stid = oci_parse($this->conn, $query);
        $aa = oci_execute($stid);
        $var = oci_fetch_array($stid,OCI_RETURN_NULLS + OCI_ASSOC);
        return $var;
    }

    public function insertUpdate($query,$commit = 1) {
        if($commit == 1){
            $comm = 'OCI_DEFAULT';
        }else{
            $comm = 'OCI_NO_AUTO_COMMIT';
        }
        $stid = oci_parse($this->conn, $query);
        $ok=oci_execute($stid);
        return $ok;
    }
}

