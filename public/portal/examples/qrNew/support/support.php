<?php
set_time_limit(60000);
ini_set('memory_limit', -1);
ignore_user_abort(1);
ini_set('max_execution_time', 30000);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
header('Content-Type: text/html; charset=UTF-8');

date_default_timezone_set('America/Mexico_City');

include_once 'conexion2.php';

class getDatos {
	
   public function imagenes()
   {
   		$conexion = new conexion;
        $sql="select img.idimagen,img.folio_cuis,img.url,img.idexpediente,exp.folio LOTE from (
select * from padrunic.imagen
where idExpediente in (
select idExpediente from padrunic.expedientes where folio like '%M%') and idtipoimagen != 10000 order by idImagen) img
join padrunic.expedientes exp on exp.idexpediente=img.idexpediente
order by img.idimagen asc";
        $result=$conexion -> getResults($sql,50000,350000);
        return $result;
   }

   public function updateCuis($idImg,$cuis,$index,$autocommit)
   {
      $complete="";
   		$conexion = new conexion;
   		$sql = "update padrunic.imagen set folio_cuis = $cuis, IDTIPOIMAGEN=10017, IDTIPODOCUMENTOESPECIFICO=10091 where idimagen = $idImg";
   		$result = $conexion -> insertUpdate($sql);
      //$result=true;
      if ($result===true) {
        $complete=": OK";
      }else{
        $complete=": NO";
      }
      $datas['sql']=$sql."; --".$index.$complete;
      $datas['datos']= $index." = ( ".$idImg.' -> '.$result." )";

      $txt = fopen("../sql/querys.txt","a+");
      fwrite($txt, $datas['sql'] .chr(10));

      if ($autocommit==='ok') {
        $txt = fopen("../sql/querys.txt","a+");
        fwrite($txt, "commit;" .chr(10));
      }

   		return  $datas;
   }
}

