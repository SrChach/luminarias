<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');


//$path = './img/codigos-qr.jpg';
$path = $_POST['path'];
$type = pathinfo($path, PATHINFO_EXTENSION);
$data = file_get_contents($path);
$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
echo $base64;
?>