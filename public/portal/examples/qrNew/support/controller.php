<?php
set_time_limit(60000);
ini_set('memory_limit', -1);
ignore_user_abort(1);
ini_set('max_execution_time', 30000);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
header('Content-Type: text/html; charset=UTF-8');

date_default_timezone_set('America/Mexico_City');

include_once 'support.php';

class datasController {
   public function getImg()
   {
        $getDatos=new getDatos();
        $result=$getDatos->imagenes();
        return $result;
   }
   public function updateCuis($idImg,$cuis,$index,$autocommit)
   {
        $update=new getDatos();
        $result=$update->updateCuis($idImg,$cuis,$index,$autocommit);
        return $result;
   }
}

$datas=new datasController;
if ($_POST['option']=="getImg") {
    $img=$datas->getImg();
    echo json_encode($img);
}
if ($_POST['option']=='update') {
	$idIMG=$_POST['idimagen'];
	$foliocuis=$_POST['foliocuis'];
  $index=$_POST['count'];
  $autocommit=$_POST['commit'];
	$update=$datas->updateCuis($idIMG,$foliocuis,$index,$autocommit);
	echo json_encode($update['datos']);
}
if ($_POST['option']=='excel') {
	$datos=$_POST['datos'];
	$csv_export="INCORRECTOS";
	foreach ($datos as $c) {
    		$csv_export.= '
    		';
      		// create line with field values
    		foreach ($c as $d){
    			$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");

    			$reemplazar=array("", "", "", "");

    			$csv_export .= str_ireplace($buscar,$reemplazar, str_replace('\n','', utf8_decode ( str_replace('\n','', str_replace(',','' ,trim ( $d ) ) ) ))). ',';
    		} 
        		/*$csv_export.= '
        		';*/
    }
    header("Content-type: text/x-csv");
    header("Content-Disposition: attachment; filename=incorrectos.csv");
    header("Content-Type: application/force-download");

    echo $csv_export;
}
