<?php

/* * ******************************************************           
 * Copyright Match-Roomie by SmartDeveloping              *                                                      
 *                                                        *   
 * Author 1:  Angel Alam Gonzalez Gonzalez                *   
 *                                                        *
 * Author 2:  Israel López Tiscareño                      *   
 *                                                        *   
 * ****************************************************** */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
header('Content-Type: text/html; charset=UTF-8');

date_default_timezone_set('America/Mexico_City');


class conexion {
    protected $DB;
    protected $DB_USER;
    protected $DB_PASS;
    protected $DB_CHAR;
    protected $conn;

    public function __construct() {
        $this -> DB = '//75.126.28.83:1521/SIDATU';
        $this -> DB_USER = 'JPRADO';
        $this -> DB_PASS = 'hiIysmNY2h';
        $this -> DB_CHAR = 'AL32UTF8';
        $this -> conn = oci_connect($this->DB_USER, $this->DB_PASS, $this->DB, $this->DB_CHAR);
        if (!$this->conn) {
            $m = oci_error();
            trigger_error(htmlentities($m['message']), E_USER_ERROR);
        }
    }

    public function getResults($query) {
        $stid = oci_parse($this->conn, $query);
        $aa = oci_execute($stid);
        $nrows = oci_fetch_all($stid, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    public function getResult($query) {
        $stid = oci_parse($this->conn, $query);
        $aa = oci_execute($stid);
        $var = oci_fetch_array($stid,OCI_ASSOC);
        return $var;
    }

    public function insertUpdate($query,$commit = 1) {
        if($commit == 1){
            $comm = 'OCI_DEFAULT';
        }else{
            $comm = 'OCI_NO_AUTO_COMMIT';
        }
        $stid = oci_parse($this->conn, $query);
        $ok=oci_execute($stid);
        return $ok;
            
        
    }
    public function delete($query,$commit = 1) {
        if($commit == 1){
            $comm = 'OCI_DEFAULT';
        }else{
            $comm = 'OCI_NO_AUTO_COMMIT';
        }
        $stid = oci_parse($this->conn, $query);
        $ok=oci_execute($stid);
        return $ok;
    }
}

