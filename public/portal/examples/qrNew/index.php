<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript" src="./js/ajax.js"></script>
        <title>Decode QRCode from image using javascript</title>
    </head>
    <body>
        <input type="button" id="action" value="Decode"/>
        <div id="content-Table">
           
        </div>
        <script type="text/javascript" src="./src/grid.js"></script>
        <script type="text/javascript" src="./src/version.js"></script>
        <script type="text/javascript" src="./src/detector.js"></script>
        <script type="text/javascript" src="./src/formatinf.js"></script>
        <script type="text/javascript" src="./src/errorlevel.js"></script>
        <script type="text/javascript" src="./src/bitmat.js"></script>
        <script type="text/javascript" src="./src/datablock.js"></script>
        <script type="text/javascript" src="./src/bmparser.js"></script>
        <script type="text/javascript" src="./src/datamask.js"></script>
        <script type="text/javascript" src="./src/rsdecoder.js"></script>
        <script type="text/javascript" src="./src/gf256poly.js"></script>
        <script type="text/javascript" src="./src/gf256.js"></script>
        <script type="text/javascript" src="./src/decoder.js"></script>
        <script type="text/javascript" src="./src/qrcode.js"></script>
        <script type="text/javascript" src="./src/findpat.js"></script>
        <script type="text/javascript" src="./src/alignpat.js"></script>
        <script type="text/javascript" src="./src/databr.js"></script>
        <script>
            var incorrect=[];
            $(function () {
                var data={option:'getImg'};
                var url='./support/controller.php';
                var result=JSON.parse(ajax(data,url));
                var urlImg;
                console.log(result);
                /*$.each(result,function (i,item) {
                    urlImg=item.URL.split(".");
                    getQR(urlImg[0],i,item.IDIMAGEN);
                    /*if (i==9) {
                        return false;    
                    }
                });*/

                //ajax({datos:incorrect,option:'excel'},'./support/controller.php');
                console.log(incorrect);
                var tableIncorrects="<table id='tableIncorrects'>\
                                        <thead>\
                                            <thead>INCORRECTOS</thead>\
                                        </thead>\
                                        <tbody>";
                    $.each(incorrect,function (i,item) {
                        tableIncorrects+= "<tr>\
                                                <td>"+item.id+"</td>\
                                            </tr>";
                    });
                    tableIncorrects+= "</tbody>\
                                    </table>";
                $('#content-Table').html(tableIncorrects);
                //descargarExcel();
                
            });
            var count=0;
            var update;
            var objet;
            var decode=null;
            function getQR(url,position,idimagen) {
                
                i=position;
                var data={path:'http://75.126.28.84:9033/img_tmp/img/'+url+'.png'};
                
                var urlFile="./support/fopen.php";
                var res=ajax(data,urlFile);
                var urlUpdate='./support/controller.php';
                decodeImageFromBase64(res,function(decodedInformation){
                    
                    if (parseInt(decodedInformation)) {
                        decode=decodedInformation;
                    }else{
                        incorrect.push({id:idimagen});
                    }
                    objet={option:'update',idimagen:idimagen,foliocuis:decode};
                    update=$.parseJSON(ajax(objet,urlUpdate));
                    console.log(update);
                });
                
            }
            // A qrcode with "ourcodeworld.com" as value in format base64 encoded
            var imageURI = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASIAAAEiAQMAAABncE31AAAABlBMVEX///8AAABVwtN+AAABA0lEQVRoge3ZUQ6CMAzG8SYcwCNx9R2JA5jU0a6ARKIP60z0/z0wZD+fmm1siBBCCPmfaMu93t/sEk8KKlv5jwrq3VT7jO4dqEy1lsT65mVS92vBUAOVKupbKmYm1DhljffpB/MXqp/aVmQfIuXNuo3qp46p4KIHlaFiTKzXRdrS4GszKlnJHCPBhkgF2z9RyWqvkNWlUdWrOqL6qZiP2kuoVWivGipT+cAosfsNOqHy1cm34txOuwBUhtKW2Hf52Zu8XLdRnVWxZtt3+duQ3gU1QD2d+kSFBDVUFX8ytZcj1Eh1OHMWQQ1Q1kSF2hb4ev5CdVTHFVl1/86FSleEEEJ+PQ/ANYzwx13NHQAAAABJRU5ErkJggg==";

            /**
             * The function decodeImageFromBase64 expects as first parameter a base64 string from a QRCode.
             * As second parameter the callback that expects the data from the QRCode as first parameter.
             */
            function decodeImageFromBase64(data, callback){
                // set callback
                qrcode.callback = callback;
                // Start decoding
                qrcode.decode(data)
            }

            // On button click, decode the qrCode from the base64 format.
            document.getElementById("action").addEventListener('click',function(){
                decodeImageFromBase64(imageURI,function(decodedInformation){
                    alert(decodedInformation);
                });
            },false);


        </script>
    </body>
</html>
