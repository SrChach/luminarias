// requires: ../dist/js/vue.js

Vue.component('input-field-horizontal', {
	props: ['select_by', 'select_dictionary', 'father_index'],
	template: `
		<div class="row">
			<form class='col-12'>
				<div class="row">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">{{ select_by.text_field_as }}</span>
						</div>
						<select class="form-control" required v-model="seleccionado" @change="imprime">
							<option value="-1" seleccionado>-- SELECCIONAR --</option>
							<option v-for="(leader_value, leader_index) in select_dictionary" :value="leader_index">{{ leader_value[select_by.text_field] }}</option>
						</select>
					</div>
				</div>
				<div v-for="(value, key) in select_dictionary[seleccionado]" v-if="key != select_by.text_field && key != select_by.value_field" class="row">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text">{{ key }}</span>
						</div>
						<input class="form-control" type="text" :value="(value != null) ? value : 'n/a'" readonly/>
					</div>
				</div>
			</form>
		</div>
	`,
	data: function () {
		return {
			seleccionado: -1
		}
	},
	methods: {
		imprime: function () {
			let llave = this.select_by.value_field
			this.$emit('interface', {
				'key': llave,
				'selected': (this.seleccionado != -1) ? this.select_dictionary[this.seleccionado][llave] : null,
				'father_index': this.father_index
			})
		}
	}
})