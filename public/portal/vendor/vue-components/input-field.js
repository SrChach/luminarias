// requires: ../dist/js/vue.js

// usage: <input-field :select="[{}]"></input-field>
Vue.component('input-field', {
    props: ['select'],
    template: `
			<div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text">{{ select.titulo }}:</span>
					</div>
					<select class="form-control" :name="select.name" required :disabled="select.disabled">
						<option value="">-- SELECCIONE --</option>
						<option v-for="el in select.options"
							:value="el.value" :selected="(el.selected == true) ? 'true' : ''">
								{{ el.text }}
						</option>
					</select>
				</div>
			</div>
		`
})