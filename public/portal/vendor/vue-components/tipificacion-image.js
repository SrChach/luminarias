// requires:
//      ../dist/js/vue.js
//      ./vue-zoomer.js


// usage: 
Vue.component('tipificacion-image', {
	props: ['referencias', 'evidencia', 'folio', 'observacion', 'no_separar'],
	template: `
			<div class="card">
				<div class="card-header bg-dark">
					<div class="row">
						<div class="col-3">
							<h5 class="float-left">IMÁGEN</h5>&nbsp;
						</div>
						<div class="col-9" align="right">
							<button v-if="referencias.length > 1 && !isEvidencia" class="btn btn-light" @click="cambiar(-1)">&#60;</button>
							<button v-if="referencias.length > 1 && !isEvidencia" class="btn btn-light" @click="cambiar(1)">&#62;</button>
							<button v-if="no_separar != true" class="btn btn-light" @click="isEvidencia = !isEvidencia">
								Ver {{ (isEvidencia) ? 'Referencias' : 'Evidencia' }}
							</button>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<div class="row justify-content-between">
								<div class="col-sm-4 text-left">
									<span v-if="no_separar != true">{{ (isEvidencia) ? 'Evidencia ' : 'Referencias ' }}</span>
									<span v-if="no_separar">Imagen </span>
									<span v-if="!isEvidencia">{{ num + 1 }} / {{ referencias.length }}</span>
								</div>
								<div class="col-sm-8 text-right">
									FOLIO: {{ (folio == null) ? 'n/a' : folio }}
								</div>
							</div>
							<div class="row" align="center">
								<div class="col-md-12">
									<vue-zoomer
										ref="zoomer"
										style="width: auto; height: 25rem; border: solid 1px silver;"
										:max-scale="10"
									>
										<img v-if=" isEvidencia == false "
											:src=" (referencias[num] == null) ? '' : 'http://207.249.158.187/cfe_luminarias/storage/images/' + referencias[num]" 
											style="object-fit: contain; width: 100%; height: 100%;"
											alt="no hay imagenes disponibles">
										<img v-if=" isEvidencia == true "
											:src="(evidencia == null) ? '' : 'http://207.249.158.187/cfe_luminarias/storage/images/' + evidencia" 
											style="object-fit: contain; width: 100%; height: 100%;"
											alt="no hay imagenes disponibles">
									</vue-zoomer>
								</div>
								<div class="col-md-12">
									observaciones: {{ (observacion == null || observacion == 'null') ? 'no hay observaciones disponibles' : observacion }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		`,
	data: () => {
		return {
			num: 0,
			isEvidencia: true
		}
	},
	methods: {
		cambiar(num) {
			if ((this.num + num) < 0) {
				this.num = this.referencias.length - 1;
			} else if ((this.num + num) > this.referencias.length - 1) {
				this.num = 0
			} else {
				this.num += num
			}
		}
	},
	created: function () {
		if (this.no_separar == true) {
			this.isEvidencia = false
		}
	}
})