// requires:
//      ../dist/js/vue.js
//      ./input-field.js

Vue.component('tipificacion-card', {
    props: ['select_list', 'tipo'],
    template: `
			<div class="card" style="height:40rem;">
				<div class="card-header bg-dark">
					<h5 class="float-left">Tipificando - <b>{{ tipo }}</b></h5>
				</div>
				<div class="card-body">
					<input-field class="row" v-for="(item, key) in select_list" :key="key" :select="item"></input-field>
				</div>
			</div>
		`
})