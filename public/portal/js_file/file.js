var url=urlthis+"storage/vehiculosVer/support/file/";
function subirArchivos(idV,idD,periodo,origen) {
    var file=$('#archivo')[0].files[0]['name'];
    var opcion='vehiculo';
    var idB=$('#idB').val();
    //console.log(url+"subir_archivo.php");
                $("#archivo").upload(url+"subir_archivo.php",
                {
                    nombre_archivo: file,idV:idV,idD:idD,periodo:periodo,origen:origen,opcion:opcion,idB:idB
                },
                function(respuesta) {
                    //Subida finalizada.
                    $("#barra_de_progreso").val(0);
                    if (respuesta == 1) {
                        mostrarRespuesta('El archivo se ha cargado correctamente.', true);
                        //$("#nombre_archivo, #archivo").val('');
                    } else {
                        mostrarRespuesta('El archivo <strong>N O</strong> se ha podido subir.', false);
                    }
                    //mostrarArchivos();
                }, function(progreso, valor) {
                    //Barra de progreso.
                    $("#barra_de_progreso").val(valor);
                });
            }
            function eliminarArchivos(archivo) {
                var origen=$('#origen').val();
                var opcionVer;
                if (origen=='kilometraje') {
                    origen="kilometraje";
                    opcionVer=1;
                }else{
                    origen="Localizacion";
                    opcionVer=2;
                }

                $.ajax({
                    url: url+'eliminar_archivo.php',
                    type: 'POST',
                    timeout: 10000,
                    data: {archivo: archivo,origen,origen},
                    error: function() {
                        mostrarRespuesta('Error al intentar eliminar el archivo.', false);
                    },
                    success: function(respuesta) {
                        if (respuesta == 1) {
                            mostrarRespuesta('El archivo '+archivo+' ha sido eliminado.', false);
                        } else {
                            mostrarRespuesta('Error al intentar eliminar el archivo.', false);                            
                        }
                        mostrarArchivos(opcionVer);
                    }
                });
            }
            function mostrarArchivos(opcion) {
                var origen="";
                if (opcion==1) {
                    origen="kilometraje";
                }else{
                    origen="Localizacion";
                }
                $.ajax({
                    url: url+'mostrar_archivos.php',
                    dataType: 'JSON',
                    type:'POST',
                    data:{origen:origen},
                    success: function(respuesta) {
                        if (respuesta) {
                            var html = '';
                            for (var i = 0; i < respuesta.length; i++) {
                                if (respuesta[i] != undefined) {
                                    html += "<div class='row'>\
                                                <span class='col-md-12'> " + respuesta[i] +   "\
                                                    <span class='glyphicon glyphicon-file'></span>\
                                                </span> \
                                                <div class='col-md-12'>\
                                                    <a class='eliminar_archivo btn btn-danger' href='javascript:void(0);' > Eliminar <span class='glyphicon glyphicon-remove'></span></a> \
                                                </div>\
                                                <div class='col-md-12'>\
                                                    <a  type='button' class='btn btn-success' onclick='DBupload()' > \
                                                    Cargar datos<span class='glyphicon glyphicon-share-alt' ></span> \
                                                    </a> \
                                                </div>\
                                            </div>\
                                             <hr>\
                                    ";
                                }
                            }
                            $("#archivos_subidos").html(html);
                        }
                    }
                });
            }
            function mostrarRespuesta(mensaje, ok){
                $("#respuesta").removeClass('alert-success').removeClass('alert-danger').html(mensaje);
                if(ok){
                    $("#respuesta").addClass('alert-success');
                }else{
                    $("#respuesta").addClass('alert-danger');
                }
            }
            $(document).ready(function() {
                
                
                $("#archivos_subidos").on('click', '.eliminar_archivo', function() {
                    var archivo = $(this).parents('.row').eq(0).find('span').text();
                    archivo = $.trim(archivo);
                    eliminarArchivos(archivo);
                });
            });


function subirArchivosO(idO,idD,periodo,origen) {
    var file=$('#archivoO')[0].files[0]['name'];
    var opcion='operador';
    var idB=$('#idB').val();
    //console.log(url+"subir_archivo.php");
                $("#archivoO").upload(url+"subir_archivo.php",
                {
                    nombre_archivo: file,idO:idO,idD:idD,periodo:periodo,origen:origen,opcion:opcion,idB:idB
                },
                function(respuesta) {
                    //Subida finalizada.
                    $("#barra_de_progreso").val(0);
                    if (respuesta == 1) {
                        mostrarRespuesta('El archivo se ha cargado correctamente.', true);
                        //$("#nombre_archivo, #archivo").val('');
                    } else {
                        mostrarRespuesta('El archivo <strong>N O</strong> se ha podido subir.', false);
                    }
                    //mostrarArchivos();
                }, function(progreso, valor) {
                    //Barra de progreso.
                    $("#barra_de_progreso").val(valor);
                });
            }

function subirArchivoskL() {
    var file=$('#archivok')[0].files[0]['name'];
    var opcion=$('#origen').val();
    var idUser=$('#usuario').val();
    var opcionBorrar;
    //console.log(periodo);
    if (opcion=='kilometraje') {
        opcionBorrar=1;
    }else{
        opcionBorrar=2;
    }
                $("#archivok").upload(url+"subir_archivo.php",
                {
                    nombre_archivo: file,opcion:opcion
                },
                function(respuesta) {
                    //Subida finalizada.
                    $("#barra_de_progreso").val(0);
                    if (respuesta == 1) {
                        mostrarRespuesta('El archivo se ha cargado correctamente.', true);
                        //$("#nombre_archivo, #archivo").val('');
                    } else {
                        mostrarRespuesta('El archivo <strong>N O</strong> se ha podido subir.', false);
                    }
                    mostrarArchivos(opcionBorrar);
                }, function(progreso, valor) {
                    //Barra de progreso.
                    $("#barra_de_progreso").val(valor);
                });
}


function subirArchivosBitacora(elemento,nameInput,itemRespuesta) {
    var file=$(elemento)[0].files[0]['name'];
    console.log(nameInput);
    var opcion='bitacoraFiles';
    var idB=$('#idB').val();
    //console.log(url+"subir_archivo.php");
    
                $(elemento).upload(url+"subir_archivo.php",
                {
                   nombre_archivo: file,nameInput:nameInput,opcion:opcion
                },
                function(respuesta) {
                    //Subida finalizada.
                    console.log(respuesta);
                    $("#barra_de_progreso").val(0);
                    if (respuesta == 1) {
                        mostrarRespuestaUpload('OK! <span class="glyphicon glyphicon-ok-circle"></span>', true,itemRespuesta);
                        //$("#nombre_archivo, #archivo").val('');
                    } else {
                        mostrarRespuestaUpload('UPS! <span class="glyphicon glyphicon-remove-circle"></span>', false,itemRespuesta);
                    }
                    //mostrarArchivos();
                }, function(progreso, valor) {
                    //Barra de progreso.
                    $("#barra_de_progreso").val(valor);
                });
}

function mostrarRespuestaUpload(mensaje, ok,itemRespuesta){
                $(itemRespuesta).removeClass('alert-success').removeClass('alert-danger').html(mensaje);
                if(ok){
                    $(itemRespuesta).addClass('alert-success');
                }else{
                    $(itemRespuesta).addClass('alert-danger');
                }
            }

