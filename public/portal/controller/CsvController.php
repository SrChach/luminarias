<?php
set_time_limit(60000);
ini_set('memory_limit', -1);
ignore_user_abort(1);
ini_set('max_execution_time', 30000);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');


include '../../../pillar/clases/conexion_mysql.php';
include '../../../pillar/clases/model/generales_model.php';
include 'ErrorController.php';

class CsvController{
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Generales;
	}
	public function getDatesRegistros()
	{
		echo json_encode($this-> model->getDatesRegistros());
	}
	public function makeCsv(){
		$this-> model->makeCsv($_GET);
	}
}

$controller = new CsvController;
$ErrorController= new ErrorController;

$controller->$_GET['opcion']();

?>
