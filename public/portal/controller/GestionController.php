<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
date_default_timezone_set('Mexico/General');
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/conexion_mysql.php';
include '../../../pillar/clases/model/gestion_model.php';
include '../../../pillar/clases/model/generales_model.php';
include '../../../pillar/clases/model/utilities.php';

include 'ErrorController.php';
/**
*
*/


class GestionController extends controlador
{
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Gestion;
	}

	public function getCatalogosLuminarias(){
		echo json_encode( $this->model->getCatalogosLuminarias() );
	}

	public function getAsignacionLuminarias(){
		echo json_encode( $this->model->getAsignacionLuminarias() );
	}

	public function getCatalogo_CD_telmex(){
		echo json_encode( $this->model->getCatalogo_CD_telmex() );
	}

	public function getAsignacion_CD_telmex(){
		echo json_encode( $this->model->getAsignacion_CD_telmex() );
	}

	public function update_CD_telmex(){
		echo json_encode( $this->model->update_CD_telmex($_POST) );
	}

	public function getAsignacion()
	{
		echo json_encode($this->model -> getAsignacion());
	}
	public function checkAsignacion()
	{
		echo json_encode($this->model -> checkAsignacion());
	}

	public function saveData()
	{
		echo json_encode($this->model -> saveData($_POST));
	}

	public function setConciliacion()
	{
		echo json_encode($this->model -> setConciliacion($_POST));
	}
	public function setConciliacionCFE()
	{
		echo json_encode($this->model -> setConciliacionCFE($_POST));
	}
	public function setConciliacionTIA()
	{
		echo json_encode($this->model -> setConciliacionTIA($_POST));
	}
	public function setConciliacionAlcandia()
	{
		echo json_encode($this->model -> setConciliacionAlcandia($_POST));
	}

}

header('Content-type: Application/json');

$controller = new GestionController;

$ErrorController= new ErrorController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion'])) {
		$controller -> {$_POST['opcion']}();
	}
	else{
		echo json_encode($ErrorController->ErrorOption());
	}

}else{
	echo json_encode($ErrorController->ErrorToken());
}
?>
