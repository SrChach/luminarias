<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/conexion_mysql.php';
include '../../../pillar/clases/model/luminarias_model.php';
include '../../../pillar/clases/model/main_model.php';
include '../../../pillar/clases/model/generales_model.php';

include 'ErrorController.php';
/**
*
*/


class LuminariasController extends controlador
{
	protected $result;
	protected $modelmain;
	protected $modelgenerales;
	protected $model;

    function __construct()
    {
        $this-> result = null;
        $this-> model = new Luminaria;
		$this-> modelmain = new ListadoLuminarias;
		$this->modelgenerales = new Generales;
    }

	public function getLuminaria($idluminaria)
	{
		$this-> result = $this-> modelmain -> getdetalle($idluminaria);
		//$catalogos[]
		//$this-> result['catalogo'] = $this->modelgenerales->

		echo json_encode($this-> result);
	}

}


$controller = new LuminariasController;

$ErrorController= new ErrorController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion']) and $_POST['opcion']=='getLuminaria') {
		$controller -> getLuminaria($_POST['luminaria']);
	}
	else{
		echo json_encode($ErrorController->ErrorOption());
	}

}else{
	//echo json_encode($UserController -> Error(0));
	echo json_encode($ErrorController->ErrorToken());
}
?>
