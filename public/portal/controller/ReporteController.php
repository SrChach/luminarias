<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
date_default_timezone_set('America/Mexico_City');
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/conexion_mysql.php';
include '../../../pillar/clases/model/reporte_model.php';
include '../../../pillar/clases/model/utilities.php';

include 'ErrorController.php';
/**
*
*/


class GestionController extends controlador
{
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Reporte;
	}

	public function eficiencia_usuario()
	{
		echo json_encode($this->model -> eficiencia_usuario());
	}
	public function conciliacion()
	{
		echo json_encode($this->model -> conciliacion());
	}

	public function teams()
	{
		echo json_encode($this->model -> teams());
	}

	public function eficiencia_usuario_dia()
	{
		$userId = $_POST['filter'];
		$date = explode('_', $_POST['date']);
		$dateFilter = $date[2] .'-'.$date[1].'-'.$date[0];
		echo json_encode($this->model -> eficiencia_usuario_dia($userId, $dateFilter));
	}

	public function check_images(){
		header('Content-Type: application/json');
		echo json_encode( $this->model->check_images() );
	}

}


$controller = new GestionController;

$ErrorController= new ErrorController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion'])) {
		$controller -> {$_POST['opcion']}();
	}
	else{
		echo json_encode($ErrorController->ErrorOption());
	}

}else{
	echo json_encode($ErrorController->ErrorToken());
}
?>
