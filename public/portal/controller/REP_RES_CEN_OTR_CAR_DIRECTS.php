<?php
    /**
     *
     */
    date_default_timezone_set('America/New_York');
    require_once '../libraries/PHPEXCEL/Classes/PHPExcel.php';


    class REP_RES_CEN_OTR_CAR_DIRECTS{

        function __construct()
        {
            $this->getReporte();
        }

        public function getReporte(){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);


            $objPHPExcel = new PHPExcel();

            // Set document properties
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            							 ->setLastModifiedBy("Maarten Balliauw")
            							 ->setTitle("Office 2007 XLSX Test Document")
            							 ->setSubject("Office 2007 XLSX Test Document")
            							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            							 ->setKeywords("office 2007 openxml php")
            							 ->setCategory("Test result file");



            $gdImage = imagecreatefromjpeg('../assets/img/CFE_logo.jpg');
            // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
            $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
            $objDrawing->setName('Sample image');
            $objDrawing->setDescription('Sample image');
            $objDrawing->setImageResource($gdImage);
            $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
            $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
            $objDrawing->setOffsetX(20); // setOffsetX funciona correctamente
            $objDrawing->setOffsetY(10); // setOffsetY funciona correctamente
            $objDrawing->setWidth(250); // establece ancho, alto
            $objDrawing->setHeight(70);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objDrawing->setCoordinates('A1', 'C4');


            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A6', 'RESUMEN DEL CENSO DE OTRAS CARGAS DIRECTAS')->mergeCells("A6:J6")
                        ->setCellValue('H8', 'División __________________________________________________')->mergeCells("H8:J8")
                        ->setCellValue('H9', 'Zona _____________________________________________________')->mergeCells("H9:J9")
                        ->setCellValue('A11', 'RELOJES')->mergeCells("A11:B12")
                        ->setCellValue('C11', 'CASETAS TELEFONICAS')->mergeCells("C11:D12")
                        ->setCellValue('E11', 'ALARMAS')->mergeCells("E11:F12")
                        ->setCellValue('G11', 'MAMPARAS')->mergeCells("G11:H12")
                        ->setCellValue('I11', 'BUZONES CON PANTALLA')->mergeCells("I11:J12")

                        ->setCellValue('A13', 'Fecha')
                        ->setCellValue('A14', 'Domicilio/Dirección')
                        ->setCellValue('A15', 'Colonia/Delegación')
                        ->setCellValue('A16', 'Zona')
                        ->setCellValue('A17', 'Fotografía')
                        ->setCellValue('A18', 'Voltaje y/o carga')
                        ->setCellValue('A19', 'Modelo y/o código')
                        ->setCellValue('A20', 'Estado Físico')
                        ->setCellValue('A21', 'Tipo de iluminación')
                        ->setCellValue('A22', 'Cuantos focos')
                        ->setCellValue('A23', 'Latitud')
                        ->setCellValue('A24', 'Longitud')
                        ->setCellValue('A35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('A37', 'CARGA KW')

                        ->setCellValue('C13', 'Fecha')
                        ->setCellValue('C14', 'Domicilio/Dirección')
                        ->setCellValue('C15', 'Colonia/Delegación')
                        ->setCellValue('C16', 'Zona')
                        ->setCellValue('C17', 'Fotografía')
                        ->setCellValue('C18', 'Voltaje y/o carga')
                        ->setCellValue('C19', 'Modelo y/o código')
                        ->setCellValue('C20', 'Estado Físico')
                        ->setCellValue('C21', 'Tipo de iluminación')
                        ->setCellValue('C22', 'Cuantos focos')
                        ->setCellValue('C23', 'Latitud')
                        ->setCellValue('C24', 'Longitud')
                        ->setCellValue('C35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('C37', 'CARGA KW')

                        ->setCellValue('E13', 'Fecha')
                        ->setCellValue('E14', 'Domicilio/Dirección')
                        ->setCellValue('E15', 'Colonia/Delegación')
                        ->setCellValue('E16', 'Zona')
                        ->setCellValue('E17', 'Fotografía')
                        ->setCellValue('E18', 'Voltaje y/o carga')
                        ->setCellValue('E19', 'Modelo y/o código')
                        ->setCellValue('E20', 'Estado Físico')
                        ->setCellValue('E21', 'Latitud')
                        ->setCellValue('E22', 'Longitud')
                        ->setCellValue('E35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('E37', 'CARGA KW')

                        ->setCellValue('G13', 'Fecha')
                        ->setCellValue('G14', 'Domicilio/Dirección')
                        ->setCellValue('G15', 'Colonia/Delegación')
                        ->setCellValue('G16', 'Zona')
                        ->setCellValue('G17', 'Fotografía')
                        ->setCellValue('G18', 'Voltaje y/o carga')
                        ->setCellValue('G19', 'Modelo y/o código')
                        ->setCellValue('G20', 'Estado Físico')
                        ->setCellValue('G21', 'Tipo de iluminación')
                        ->setCellValue('G22', 'Cuantos focos')
                        ->setCellValue('G23', 'Latitud')
                        ->setCellValue('G24', 'Longitud')
                        ->setCellValue('G35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('G37', 'CARGA KW')

                        ->setCellValue('I13', 'Fecha')
                        ->setCellValue('I14', 'Domicilio/Dirección')
                        ->setCellValue('I15', 'Colonia/Delegación')
                        ->setCellValue('I16', 'Zona')
                        ->setCellValue('I17', 'Fotografía')
                        ->setCellValue('I18', 'Voltaje y/o carga')
                        ->setCellValue('I19', 'Modelo y/o código')
                        ->setCellValue('I20', 'Estado Físico')
                        ->setCellValue('I21', 'Tipo de iluminación')
                        ->setCellValue('I22', 'Cuantos focos')
                        ->setCellValue('I23', 'Latitud')
                        ->setCellValue('I24', 'Longitud')
                        ->setCellValue('I35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('I37', 'CARGA KW')
                        ->setCellValue('C42', 'Por parte de CFE Distribución')->mergeCells("C42:D42")
                        ->setCellValue('C45', '__________________________________')->mergeCells("C45:D45")
                        ->setCellValue('G42', 'Por parte del Usuario')->mergeCells("G42:H42")
                        ->setCellValue('G45', '__________________________________')->mergeCells("G45:H45")
                        ->setCellValue('A47', 'Nota: este formato puede ser modificado de acuerdo a las condiciones de la Zona y de los equipos que se encuentran en los recorridos en campo, manteniendo los datos relevantes a obtener de carga y total de equipos encontrados.')->mergeCells("A47:J48");

            $style = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
               'font' => array( 'bold' => true )
            );

            $textCenter = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $alignLeft = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $fontColor = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'a8adad')
                )
            );
            $foreColor = array(
                'font' => array( 'bold' => true )
            );

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('25')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('25')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('25')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('25')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('25')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('25')->setAutoSize(FALSE);

            $objPHPExcel->getActiveSheet()->getStyle("A21")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A26")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A27")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("G35")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A47")->getAlignment()->setWrapText(true);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A11:J12")->applyFromArray($fontColor);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A11:J12")->applyFromArray($foreColor);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A11:J12")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A13:A37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("C13:C37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("E13:E37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("G13:G37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("I13:I37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A6")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A41")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A43")->applyFromArray($textCenter);
            // $objPHPExcel->setActiveSheetIndex(0)->getStyle("A47")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("E41")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("E43")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("C42")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("C45")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("G42")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("G45")->applyFromArray($textCenter);
            // $objPHPExcel->setActiveSheetIndex(0)->getStyle("A10:N13")->applyFromArray($style);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A11:J38")->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '211D1D')
                        )
                    )
                )
            );

            $objPHPExcel->getActiveSheet()->setTitle('Reporte');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);


            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="01simple.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;

        }
    }

    $reporte = new REP_RES_CEN_OTR_CAR_DIRECTS;
?>
