<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include_once '../../../pillar/clases/controlador.php';
include_once 'AuthController.php';
include_once '../model/conexion.php';

/**
*
*/
class methodController
{
	protected $request;
	protected $Errors;
	protected $AuthController;
	protected $UserController;

	function __construct()
	{
		$this-> request = null;
		$this-> Errors= null;
		$this-> AuthController = new authController;
	}
	public function CallFuntionsSession($c,$k,$_t,$set=false)
	{
		if (!$set) {
			$this-> request = $this-> AuthController -> session($c,$k,$_t);
		}
		if ($set) {
			$this-> request = $this-> AuthController -> sessionOut();
		}

		echo json_encode($this-> request);
	}
	public function Error($Error)
	{
		if ($Error==0) {
			$this-> Errors = array('CODIGO' => $Error,
									'DATOS' => 'FALLO DE AUTENTICACIÓN'
									);
		}
		return $this-> Errors;
	}

}
$controller=new methodController;
if ($_POST) {
	if (isset($_POST['csrf_token'])) {
		if ($_POST['opcion']=='session') {
			$set=(isset($_POST['set']) and $_POST['set']== $_POST['set']) ? $_POST['set']:'';
			$controller->CallFuntionsSession($_POST['c'],$_POST['k'],$_POST['csrf_token'],$set);
		}
		if ($_POST['opcion']=='Users') {
			$id= (isset($_POST['id']) and $_POST['id']== $_POST['id']) ? $_POST['id']:'';
			$values= (isset($_POST['values']) and $_POST['values']== $_POST['values']) ? $_POST['values']:'';
			$controller->CallFuntionsUsers($_POST['action'], $id,$values);
		}
	}else{
		echo json_encode($controller->Error(0));
	}
}else{
	if (isset($_GET['_t']) and isset($_SESSION['_TOKEN'])) {
		if ($_GET['_t']==$_SESSION['_TOKEN']) {
			echo "OK";
		}else{
			echo $controller->Error(0)['DATOS'];
			session_destroy();
		}

	}else{
		echo $controller->Error(0)['DATOS'];
	}

}
?>
