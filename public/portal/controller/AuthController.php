<?php
//include_once '../model/conexion.php';
include_once '../model/session.php';
/**
* 
*/
class authController
{
	protected $usr;
	protected $psw;
	protected $session;
	protected $regresa;
	
	function __construct()
	{
		$this -> usr = null;
		$this -> psw = null;
		$this -> regresa = null;
		$this -> session = new session;
	}
	public function session($credencial,$key,$_token)
	{
		$this-> regresa = $this-> session -> start($credencial,$key,$_token);
		return $this-> regresa;
	}
	public function sessionOut()
	{
		$this-> regresa = $this-> session -> out();
		return $this-> regresa;
	}
}


?>