<?php
    /**
     *
     */
    date_default_timezone_set('America/New_York');
    require_once '../libraries/PHPEXCEL/Classes/PHPExcel.php';

    class REP_DES_CEN_ALUM_PUB_SEM{

        function __construct()
        {
            $this->getReporte();
            // return 'Hola mundo';
        }

        public function getReporte(){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);


            $objPHPExcel = new PHPExcel();

            // Set document properties
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            							 ->setLastModifiedBy("Maarten Balliauw")
            							 ->setTitle("Office 2007 XLSX Test Document")
            							 ->setSubject("Office 2007 XLSX Test Document")
            							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            							 ->setKeywords("office 2007 openxml php")
            							 ->setCategory("Test result file");



            $gdImage = imagecreatefromjpeg('../assets/img/CFE_logo.jpg');
            // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
            $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
            $objDrawing->setName('Sample image');
            $objDrawing->setDescription('Sample image');
            $objDrawing->setImageResource($gdImage);
            $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
            $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
            $objDrawing->setOffsetX(20); // setOffsetX funciona correctamente
            $objDrawing->setOffsetY(10); // setOffsetY funciona correctamente
            $objDrawing->setWidth(250); // establece ancho, alto
            $objDrawing->setHeight(70);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objDrawing->setCoordinates('A1', 'C4');


            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('F2', 'División __________________________________________________')->mergeCells("F2:G2")
                        ->setCellValue('F3', 'Zona _____________________________________________________')->mergeCells("F3:G3")
                        ->setCellValue('A5', 'PROGRAMA ANUAL DE CENSOS DE OTRAS CARGAS DIRECTAS')->mergeCells("A5:G5")
                        ->setCellValue('A6', 'CORRESPONDIENTE AL MUNICIPIO DE _________________________________')->mergeCells("A6:G6");

                        // ->setCellValue('A9', 'MUNICIPIO, Ciudad o Población a censar')
                        // ->setCellValue('B9', 'PROCESO')
                        // ->setCellValue('C9', 'Rpu´s totales a censar')
                        // ->setCellValue('D9', '% de avance');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A7', 'NÚMERO DE CUENTA: ')->mergeCells("A7:C7")
                        ->setCellValue('D7', 'POBLACIÓN: ')->mergeCells("D7:G7")
                        ->setCellValue('A8', 'TENSIÓN: ')->mergeCells("A8:C8")
                        ->setCellValue('D8', 'SECCIÓN DEL PLANO: ')->mergeCells("D8:G8")
                        ->setCellValue('A9', 'DESCRIPCIÓN')
                        ->setCellValue('B9', 'CANTIDAD')
                        ->setCellValue('C9', 'TOTALWatts')
                        ->setCellValue('D9', 'SUBTOTAL')
                        ->setCellValue('E9', 'EQUIPO AUXILIAR')
                        ->setCellValue('F9', 'POTENCIA TOTAL')
                        ->setCellValue('G9', 'CONSUMO PROMEDIO DIARIO (Potencia * 12 horas)');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A10', 'VAPOR DE SODIO DE ALTA PRESIÓN')
                        ->setCellValue('A11', '400 W')
                        ->setCellValue('A12', '250 W')
                        ->setCellValue('A13', '150 W')
                        ->setCellValue('A14', '100 W')
                        ->setCellValue('A15', '70 W')
                        ->setCellValue('A16', 'OTRA CAPACIDAD')
                        ->setCellValue('A17', 'VAPOR DE SODIO DE BAJA PRESIÓN')
                        ->setCellValue('A18', '250 W')
                        ->setCellValue('A19', '150 W')
                        ->setCellValue('A20', '100 W')
                        ->setCellValue('A21', 'OTRA CAPACIDAD')
                        ->setCellValue('A22', 'VAPOR DE MERCURIO')
                        ->setCellValue('A23', '400 W')
                        ->setCellValue('A24', '250 W')
                        ->setCellValue('A25', '150 W')
                        ->setCellValue('A26', '100 W')
                        ->setCellValue('A27', 'OTRA CAPACIDAD')
                        ->setCellValue('A28', 'LUZ MIXTA')
                        ->setCellValue('A29', '500 W')
                        ->setCellValue('A30', '250 W')
                        ->setCellValue('A31', '160 W')
                        ->setCellValue('A32', 'OTRA CAPACIDAD')
                        ->setCellValue('A33', 'INCANDESCENTES')
                        ->setCellValue('A34', '200 W')
                        ->setCellValue('A35', '150 W')
                        ->setCellValue('A36', '100 W')
                        ->setCellValue('A37', '75 W')
                        ->setCellValue('A38', '60 W')
                        ->setCellValue('A39', 'OTRA CAPACIDAD')
                        ->setCellValue('A40', 'FLUORESCENTES')
                        ->setCellValue('A41', '74 W')
                        ->setCellValue('A42', '39 W')
                        ->setCellValue('A43', '22 W')
                        ->setCellValue('A44', '13 W')
                        ->setCellValue('A45', 'OTRA CAPACIDAD')
                        ->setCellValue('A46', 'REFLECTORES')
                        ->setCellValue('A47', '1000 W')
                        ->setCellValue('A48', '500 W')
                        ->setCellValue('A49', 'OTRA CAPACIDAD')
                        ->setCellValue('A50', 'OTRO TIPO DE LÁMPARAS')
                        ->setCellValue('A51', 'CAPACIDAD')
                        ->setCellValue('A52', 'CAPACIDAD')
                        ->setCellValue('A53', 'CAPACIDAD')
                        ->setCellValue('A54', 'Nota: cuando el valor de la eficiencia de los equipos auxiliares sea menor al 25 %, se multiplicará por el valor comprobado')->mergeCells("A54:G54")
                        ->setCellValue('A56', 'RPU _______________________________________________')->mergeCells("A56:B56")
                        ->setCellValue('A57', 'CARGA ANTERIOR ____________________________________')->mergeCells("A57:B57")
                        ->setCellValue('A58', 'No. DE CONTRATO ___________________________________')->mergeCells("A58:B58")
                        ->setCellValue('A59', 'UBICACIÓN _________________________________________')->mergeCells("A59:B59")
                        ->setCellValue('A60', 'TARIFA ____________________________________________')->mergeCells("A60:B60")
                        ->setCellValue('D59', '_____________________________')->mergeCells("D59:E59")
                        ->setCellValue('D60', 'POR CFE DISTRIBUCIÓN')->mergeCells("D60:E60")
                        ->setCellValue('G59', '__________________________')
                        ->setCellValue('G60', 'POR EL SISTEMA DE ALUMBRADO PÚBLICO')->mergeCells("G60:G61")
                        ->setCellValue('A63', 'Nota: este formato puede ser modificado de acuerdo a las condiciones de la Zona y de los equipos que se encuentran en los recorridos en campo, manteniendo los datos relevantes a obtener de carga y total de equipos encontrados.')->mergeCells("A63:G64");

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('30')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('15')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('15')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('15')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('10')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('15')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('23')->setAutoSize(FALSE);

            $style = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
               'font' => array( 'bold' => true )
            );

            $textCenter = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $fontColor = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'a8adad')
                )
            );
            $foreColor = array(
                'font' => array( 'bold' => true )
            );

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A10:G16")->applyFromArray($fontColor);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A22:G27")->applyFromArray($fontColor);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A33:G39")->applyFromArray($fontColor);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A46:G49")->applyFromArray($fontColor);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A10:A53")->applyFromArray($foreColor);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A54:G54")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("D60:E60")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("G60")->applyFromArray($textCenter);
            // $objPHPExcel->setActiveSheetIndex(0)->getStyle("A63")->applyFromArray($textCenter);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A5:G5")->applyFromArray($style);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A9:G9")->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle("E9")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("G9")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("G60")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A63")->getAlignment()->setWrapText(true);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A7:G53")->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '211D1D')
                        )
                    )
                )
            );

            // Rename worksheet
            $objPHPExcel->getActiveSheet()->setTitle('Reporte');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);


            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="01simple.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;

        }
    }

    $reporte = new REP_DES_CEN_ALUM_PUB_SEM;
?>
