<?php
    /**
     *
     */
    date_default_timezone_set('America/New_York');
    require_once '../libraries/PHPEXCEL/Classes/PHPExcel.php';

    class REP_RES_CEN_ALUM_PUB_SEM{

        function __construct()
        {
            $this->getReporte();
            // return 'Hola mundo';
        }

        public function getReporte(){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);


            $objPHPExcel = new PHPExcel();

            // Set document properties
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            							 ->setLastModifiedBy("Maarten Balliauw")
            							 ->setTitle("Office 2007 XLSX Test Document")
            							 ->setSubject("Office 2007 XLSX Test Document")
            							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            							 ->setKeywords("office 2007 openxml php")
            							 ->setCategory("Test result file");



            $gdImage = imagecreatefromjpeg('../assets/img/CFE_logo.jpg');
            // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
            $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
            $objDrawing->setName('Sample image');
            $objDrawing->setDescription('Sample image');
            $objDrawing->setImageResource($gdImage);
            $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
            $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
            $objDrawing->setOffsetX(20); // setOffsetX funciona correctamente
            $objDrawing->setOffsetY(10); // setOffsetY funciona correctamente
            $objDrawing->setWidth(250); // establece ancho, alto
            $objDrawing->setHeight(70);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objDrawing->setCoordinates('A1', 'C4');


            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('K4', 'División __________________________________________________')->mergeCells("K4:N4")
                        ->setCellValue('K5', 'Zona _____________________________________________________')->mergeCells("K5:N5")
                        ->setCellValue('A7', 'RESUMEN DEL CENSO DE ALUMBRADO PÚBLICO Y SEMÁFOROS')->mergeCells("A7:N7")
                        ->setCellValue('A9', 'CORRESPONDIENTE AL MUNICIPIO DE ____________________________')->mergeCells("A9:N9")
                        ->setCellValue('A10', 'RPU')->mergeCells("A10:A11")
                        ->setCellValue('B10', 'NÚMERO DE CUENTA')->mergeCells("B10:B11")
                        ->setCellValue('C10', 'POBLACIÓN, DIRECCIÓN')->mergeCells("C10:C11")
                        ->setCellValue('D10', 'CENSO ACTUAL')->mergeCells("D10:G10")
                        ->setCellValue('D11', 'FECHA')
                        ->setCellValue('E11', 'LÁMPARAS')
                        ->setCellValue('F11', 'CARGA')
                        ->setCellValue('G11', 'C.P.D')
                        ->setCellValue('H10', 'CENSO ANTERIOR')->mergeCells("H10:K10")
                        ->setCellValue('H11', 'FECHA')
                        ->setCellValue('I11', 'LÁMPARAS')
                        ->setCellValue('J11', 'CARGA')
                        ->setCellValue('K11', 'C.P.D')
                        ->setCellValue('L10', 'DIFERENCIAS')->mergeCells("L10:N10")
                        ->setCellValue('L11', 'LÁMPARAS')
                        ->setCellValue('M11', 'CARGA')
                        ->setCellValue('N11', 'C.P.D')
                        ->setCellValue('A34', '__________________________')->mergeCells("A34:C34")
                        ->setCellValue('A35', 'LUGAR Y FECHA')->mergeCells("A35:C35")
                        ->setCellValue('F34', '__________________________')->mergeCells("F34:H34")
                        ->setCellValue('F35', 'POR CFE DISTRIBUCIÓN')->mergeCells("F35:H35")
                        ->setCellValue('L34', '__________________________')->mergeCells("L34:N34")
                        ->setCellValue('L35', 'POR EL SISTEMA DE ALUMBRADO PÚBLICO')->mergeCells("L35:N36")
                        ->setCellValue('A38', 'Nota: este formato puede ser modificado de acuerdo a las condiciones de la Zona y de los equipos que se encuentran en los recorridos en campo, manteniendo los datos relevantes a obtener de carga y total de equipos encontrados.')->mergeCells("A38:N39");

            $style = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
               'font' => array( 'bold' => true )
            );

            $textCenter = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A34")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A35")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("F34")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("F35")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("L34")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("L35")->applyFromArray($textCenter);

            $objPHPExcel->getActiveSheet()->getStyle("L35")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A38")->getAlignment()->setWrapText(true);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A10:N30")->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '211D1D')
                        )
                    )
                )
            );

            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('15')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('10')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('10')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('10')->setAutoSize(FALSE);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A7")->applyFromArray($style);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A10:N11")->applyFromArray($style);

            $objPHPExcel->getActiveSheet()->getStyle("A10:N11")->getAlignment()->setWrapText(true);

            // Rename worksheet
            $objPHPExcel->getActiveSheet()->setTitle('Reporte');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);


            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="01simple.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;

        }
    }

    $reporte = new REP_RES_CEN_ALUM_PUB_SEM;
?>
