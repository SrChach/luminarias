<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/conexion_mysql.php';
include '../../../pillar/clases/model/main.php';

include 'ErrorController.php';

class MainController extends controlador
{

    protected $result;
    protected $model;
    function __construct()
    {
        $this-> result = null;
        $this-> model = new Main;
    }

    public function getDatasInit()
    {
        echo json_encode($this->model -> getDatasInit());
    }

    public function getDatas()
    {
        echo json_encode($this->model -> getDatas($_POST));
    }

}

$controller = new MainController;

$ErrorController= new ErrorController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion'])) {
		$controller -> {$_POST['opcion']}();
	}
	else{
		echo json_encode($ErrorController->ErrorOption());
	}

}else{
	echo json_encode($ErrorController->ErrorToken());
}


?>
