<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/conexion_mysql.php';
include '../../../../sincronizador/sincronizador_independiente_2.php';
//include '../../../pillar/clases/model/devices_model.php';

include 'ErrorController.php';
/**
*
*/


class SyncController extends controlador
{
	protected $result;
	protected $model;

    function __construct()
    {
		$this->result = null;
		$this->model = new sincronizador;
    }

	public function getDevicesByZone()
	{
		$this->result = $this->model->getDevicesByZone();
		echo json_encode($this->result);
	}

	public function syncFile()
	{
		date_default_timezone_set('Mexico/General');
		$pathFile = null;
        $dirNameFile = '../../../../storage/zips_independientes/' . basename($_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $dirNameFile)) {
            $pathFile = $dirNameFile;
        }
		//$this->result = $this->model->addDevicesToZone($pathFile);
		echo json_encode($this->result);
	}

}


$controller = new SyncController;

$ErrorController= new ErrorController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion'])) {
		$controller -> $_POST['opcion']();
	}
	else{
		echo json_encode($ErrorController->ErrorOption());
	}

}else{
	echo json_encode($ErrorController->ErrorToken());
}
?>
