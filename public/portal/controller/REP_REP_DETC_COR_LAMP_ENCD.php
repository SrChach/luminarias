<?php
    /**
     *
     */
    date_default_timezone_set('America/New_York');
    require_once '../libraries/PHPEXCEL/Classes/PHPExcel.php';


    class REP_REP_DETC_COR_LAMP_ENCD{

        function __construct()
        {
            $this->getReporte();
        }

        public function getReporte(){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);


            $objPHPExcel = new PHPExcel();

            // Set document properties
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            							 ->setLastModifiedBy("Maarten Balliauw")
            							 ->setTitle("Office 2007 XLSX Test Document")
            							 ->setSubject("Office 2007 XLSX Test Document")
            							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            							 ->setKeywords("office 2007 openxml php")
            							 ->setCategory("Test result file");



            $gdImage = imagecreatefromjpeg('../assets/img/CFE_logo.jpg');
            // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
            $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
            $objDrawing->setName('Sample image');
            $objDrawing->setDescription('Sample image');
            $objDrawing->setImageResource($gdImage);
            $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
            $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
            $objDrawing->setOffsetX(20); // setOffsetX funciona correctamente
            $objDrawing->setOffsetY(10); // setOffsetY funciona correctamente
            $objDrawing->setWidth(250); // establece ancho, alto
            $objDrawing->setHeight(70);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objDrawing->setCoordinates('A1', 'C4');


            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('K4', 'División __________________________________________________')->mergeCells("K4:N4")
                        ->setCellValue('K5', 'Zona _____________________________________________________')->mergeCells("K5:N5")
                        ->setCellValue('A7', 'REPORTE DETECCIÓN Y CORRECCIÓN DE LÁMPARAS ENCENDIDAS 24 HORAS')->mergeCells("A7:N7")
                        ->setCellValue('A9', 'CORRESPONDIENTE AL MUNICIPIO DE ____________________________')->mergeCells("A9:N9")
                        ->setCellValue('A10', 'CFE DISTRIBUCIÓN')->mergeCells("A10:I10")
                        ->setCellValue('J10', 'SISTEMA DE ALUMBRADO PÚBLICO')->mergeCells("J10:N10")
                        ->setCellValue('A11', 'CONSECUTIVO')->mergeCells("A11:A13")
                        ->setCellValue('B11', 'FECHA')->mergeCells("B11:B13")
                        ->setCellValue('C11', 'No. de poste')->mergeCells("C11:C13")
                        ->setCellValue('D11', 'DATOS DE LÁMPARAS ENCENDIDAS 24 HRS.')->mergeCells("D11:F12")
                        ->setCellValue('D13', 'CAPACIDAD W')
                        ->setCellValue('E13', 'TIPO')
                        ->setCellValue('F13', 'VOLTAJE')
                        ->setCellValue('G11', 'DATOS DE FACTURACIÓN')->mergeCells("G11:I12")
                        ->setCellValue('G13', 'SECTOR')
                        ->setCellValue('H13', 'SUBSECTOR')
                        ->setCellValue('I13', 'CUENTA')
                        ->setCellValue('J11', 'DATOS DE CORRECCIÓN DE LÁMPARAS ENCENDIDAS 24 HRS.')->mergeCells("J11:N12")
                        ->setCellValue('L13', 'FECHA')
                        ->setCellValue('M13', 'CAPACIDAD W')
                        ->setCellValue('N13', 'MARCA')
                        ->setCellValue('J13', 'TIPO')
                        ->setCellValue('K13', 'VOLTAJE')
                        ->setCellValue('A26', '__________________________')->mergeCells("A26:D26")
                        ->setCellValue('A27', 'LUGAR Y FECHA')->mergeCells("A27:D27")
                        ->setCellValue('F26', 'FECHA DE ENTREGA POR EL REP. SISTEMA AL. _________________')->mergeCells("F26:I27")
                        ->setCellValue('K26', '__________________________')->mergeCells("K26:N26")
                        ->setCellValue('K27', 'POR EL SISTEMA DE ALUMBRADO PÚBLICO')->mergeCells("K27:N27")
                        ->setCellValue('A29', 'Nota: este formato puede ser modificado de acuerdo a las condiciones de la Zona y de los equipos que se encuentran en los recorridos en campo, manteniento los datos relevantes a obtener de carga y total de equipos encontrados.')->mergeCells("A29:N30");

            $style = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
               'font' => array( 'bold' => true )
            );

            $textCenter = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $alignLeft = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('15')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('12')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('8')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('8')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('10')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('12')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('7')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('9')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('7')->setAutoSize(FALSE);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A7")->applyFromArray($style);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A10:N13")->applyFromArray($style);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A26")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A27")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("F26")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("K26")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("K27")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("D11")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A29")->applyFromArray($alignLeft);

            $objPHPExcel->getActiveSheet()->getStyle("C11")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("D11")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("D13")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("M13")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("F26")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("J11")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A29")->getAlignment()->setWrapText(true);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A10:N22")->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '211D1D')
                        )
                    )
                )
            );

            $objPHPExcel->getActiveSheet()->setTitle('Reporte');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);


            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="01simple.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;

        }
    }

    $reporte = new REP_REP_DETC_COR_LAMP_ENCD;
?>
