<?php
    /**
     *
     */
    date_default_timezone_set('America/New_York');
    require_once '../libraries/PHPEXCEL/Classes/PHPExcel.php';

    class REP_PGR_ANU_CEN_ALU_PUB_SEM{

        function __construct()
        {
            $this->getReporte_1();
            // return 'Hola mundo';
        }

        public function getReporte_1(){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);


            $objPHPExcel = new PHPExcel();

            // Set document properties
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            							 ->setLastModifiedBy("Maarten Balliauw")
            							 ->setTitle("Office 2007 XLSX Test Document")
            							 ->setSubject("Office 2007 XLSX Test Document")
            							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            							 ->setKeywords("office 2007 openxml php")
            							 ->setCategory("Test result file");



            $gdImage = imagecreatefromjpeg('../assets/img/CFE_logo.jpg');
            // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
            $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
            $objDrawing->setName('Sample image');
            $objDrawing->setDescription('Sample image');
            $objDrawing->setImageResource($gdImage);
            $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
            $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
            $objDrawing->setOffsetX(20); // setOffsetX funciona correctamente
            $objDrawing->setOffsetY(10); // setOffsetY funciona correctamente
            $objDrawing->setWidth(250); // establece ancho, alto
            $objDrawing->setHeight(70);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objDrawing->setCoordinates('A1', 'C4');


            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('Y5', 'División __________________________________________________')->mergeCells("Y5:AD5")
                        ->setCellValue('Y6', 'Zona _____________________________________________________')->mergeCells("Y6:AD6")
                        ->setCellValue('Y7', 'Año ______________________________________________________')->mergeCells("Y7:AD7")
                        ->setCellValue('A8', 'PROGRAMA ANUAL DE CENSOS DE ALUMBRADO PÚBLICO Y SEMAFOROS')->mergeCells("A8:AD8")
                        ->setCellValue('A9', 'MUNICIPIO (Fecha de último censo)')
                        ->setCellValue('B9', 'PROCESO')
                        ->setCellValue('C9', 'Rpu´s totales a censar')
                        ->setCellValue('D9', '% de avance');

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A9:A11");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B9:B11");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C9:C11");
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells("D9:D11");


            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('E9', 'RPU´S PROGRAMADOS PARA EFECTUAR CENSO')
                        ->mergeCells("E9:AB9");

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('E10', 'ENE')
                        ->mergeCells("E10:F10")
                        ->setCellValue('E11', 'P')
                        ->setCellValue('F11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('G10', 'FEB')
                        ->mergeCells("G10:H10")
                        ->setCellValue('G11', 'P')
                        ->setCellValue('H11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('I10', 'MAR')
                        ->mergeCells("I10:J10")
                        ->setCellValue('I11', 'P')
                        ->setCellValue('J11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('K10', 'ABR')
                        ->mergeCells("K10:L10")
                        ->setCellValue('K11', 'P')
                        ->setCellValue('L11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('M10', 'MAY')
                        ->mergeCells("M10:N10")
                        ->setCellValue('M11', 'P')
                        ->setCellValue('N11', 'R');;

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('O10', 'JUN')
                        ->mergeCells("O10:P10")
                        ->setCellValue('O11', 'P')
                        ->setCellValue('P11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('Q10', 'JUL')
                        ->mergeCells("Q10:R10")
                        ->setCellValue('Q11', 'P')
                        ->setCellValue('R11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('S10', 'AGO')
                        ->mergeCells("S10:T10")
                        ->setCellValue('S11', 'P')
                        ->setCellValue('T11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('U10', 'SEP')
                        ->mergeCells("U10:V10")
                        ->setCellValue('U11', 'P')
                        ->setCellValue('V11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('W10', 'OCT')
                        ->mergeCells("W10:X10")
                        ->setCellValue('W11', 'P')
                        ->setCellValue('X11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('Y10', 'NOV')
                        ->mergeCells("Y10:Z10")
                        ->setCellValue('Y11', 'P')
                        ->setCellValue('Z11', 'R');;

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('AA10', 'DIC')
                        ->mergeCells("AA10:AB10")
                        ->setCellValue('AA11', 'P')
                        ->setCellValue('AB11', 'R');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('AC9', 'Recuperación')
                        ->mergeCells("AC9:AC10")
                        ->setCellValue('AC11', 'KWH');

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('AD9', 'Observaciones')
                        ->mergeCells("AD9:AD11");

            for ($i = 'E'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
            }

            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(TRUE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('20')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth('20')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth('20')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('15')->setAutoSize(FALSE);


            $rowFinal = 0;
            $lastColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
            $arrayItems = ['LEVANTAMIENTO', 'MODIFICACIÓN SICOM', 'AJUSTE SICOM', 'DIGITALIZACIÓN'];
            for($row = 0; $row <= 4; $row++){
                $objPHPExcel->setActiveSheetIndex(0)
                            ->mergeCells("A".(12+(4*$row)).":A".(12+(4*$row)+3))
                            ->mergeCells("AC".(12+(4*$row)).":AC".(12+(4*$row)+3))
                            ->mergeCells("AD".(12+(4*$row)).":AD".(12+(4*$row)+3));
                $countArray = 0;
                for($i = (12+(4*$row)); $i <= (12+(4*$row)+3); $i++){
                    for ($column = 'B'; $column != 'AC'; $column++) {
                        if($column == 'B'){
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column.$i, $arrayItems[$countArray]);
                        }else{
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column.$i, '');
                        }
                    }
                    $countArray++;
                    $rowFinal = (12+(4*$row)+3);
                }
            }

            for ($column = 'A'; $column != 'AC'; $column++) {
                if($column == 'B'){
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column.($rowFinal+1), 'AVANCE MENSUAL');
                }else{
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column.($rowFinal+1), '');
                }
            }


            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.($rowFinal+4), '_________________________________')
                        ->mergeCells("A".($rowFinal+4).":C".($rowFinal+4))
                        ->setCellValue('A'.($rowFinal+5), 'ELABORÓ')
                        ->mergeCells("A".($rowFinal+5).":C".($rowFinal+5));

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('I'.($rowFinal+4), '_________________________________')
                        ->mergeCells("I".($rowFinal+4).":U".($rowFinal+4))
                        ->setCellValue('I'.($rowFinal+5), 'REVISÓ')
                        ->mergeCells("I".($rowFinal+5).":U".($rowFinal+5));

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('AA'.($rowFinal+4), '_________________________________')
                        ->mergeCells("AA".($rowFinal+4).":AD".($rowFinal+4))
                        ->setCellValue('AA'.($rowFinal+5), 'AUTORIZÓ')
                        ->mergeCells("AA".($rowFinal+5).":AD".($rowFinal+5));

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('B'.($rowFinal+7), 'Nota: este formato puede ser modificado de acuerdo a las condiciones de la Zona y de los equipos que se encuentran en los recorridos en campo, manteniendo los datos relevantes a obtener de carga y total de equipos encontrados')
                        ->mergeCells("B".($rowFinal+7).":AC".($rowFinal+8));

            $style = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $alignLeft = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $objPHPExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
            $objPHPExcel->getDefaultStyle()->applyFromArray($style);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle('B'.($rowFinal+7))->applyFromArray($alignLeft);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A9:AD".($rowFinal+1))->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '211D1D')
                        )
                    )
                )
            );
            // $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);

            // Rename worksheet
            $objPHPExcel->getActiveSheet()->setTitle('Reporte');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);


            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="01simple.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;

        }
    }

    $reporte = new REP_PGR_ANU_CEN_ALU_PUB_SEM;

?>
