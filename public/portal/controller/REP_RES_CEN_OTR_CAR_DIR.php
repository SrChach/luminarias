<?php
    /**
     *
     */
    date_default_timezone_set('America/New_York');
    require_once '../libraries/PHPEXCEL/Classes/PHPExcel.php';

    class REP_RES_CEN_OTR_CAR_DIR{

        function __construct()
        {
            $this->getReporte();
        }

        public function getReporte(){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);


            $objPHPExcel = new PHPExcel();

            // Set document properties
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            							 ->setLastModifiedBy("Maarten Balliauw")
            							 ->setTitle("Office 2007 XLSX Test Document")
            							 ->setSubject("Office 2007 XLSX Test Document")
            							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            							 ->setKeywords("office 2007 openxml php")
            							 ->setCategory("Test result file");



            $gdImage = imagecreatefromjpeg('../assets/img/CFE_logo.jpg');
            // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
            $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
            $objDrawing->setName('Sample image');
            $objDrawing->setDescription('Sample image');
            $objDrawing->setImageResource($gdImage);
            $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
            $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
            $objDrawing->setOffsetX(20); // setOffsetX funciona correctamente
            $objDrawing->setOffsetY(10); // setOffsetY funciona correctamente
            $objDrawing->setWidth(250); // establece ancho, alto
            $objDrawing->setHeight(70);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objDrawing->setCoordinates('A1', 'C4');


            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A6', 'RESUMEN DEL CENSO DE OTRAS CARGAS DIRECTAS')->mergeCells("A6:H6")
                        ->setCellValue('G8', 'División __________________________________________________')->mergeCells("G8:H8")
                        ->setCellValue('G9', 'Zona _____________________________________________________')->mergeCells("G9:H9")
                        ->setCellValue('A11', 'CÁMARAS')->mergeCells("A11:B12")
                        ->setCellValue('C11', 'FUENTES DE PODER')->mergeCells("C11:D12")
                        ->setCellValue('E11', 'PARABUSES Y/O MUPY')->mergeCells("E11:F12")
                        ->setCellValue('G11', 'ESPECTACULARES/PANORAMICOS')->mergeCells("G11:H12")
                        ->setCellValue('A13', 'Fecha')
                        ->setCellValue('A14', 'Id (consecutivo)')
                        ->setCellValue('A15', 'Tipo de cámara')
                        ->setCellValue('A16', 'Altura de la cámara')
                        ->setCellValue('A17', 'Tipo de montado')
                        ->setCellValue('A18', 'Número de bocinas')
                        ->setCellValue('A19', 'Voltaje y/o carga')
                        ->setCellValue('A20', 'Tiempo de encendido')
                        ->setCellValue('A21', 'Lugar donde esta instalada (vp, vs, bajo puente, sobre puente, camellón central o lateral, puente peatonal, parques y jardines)')
                        ->setCellValue('A22', 'Modelo y/ó código')
                        ->setCellValue('A23', 'Estado Físico')
                        ->setCellValue('A24', 'Imagen fotográfica de cada cámara')
                        ->setCellValue('A25', 'Observaciones generales')
                        ->setCellValue('A26', 'Georeferencia y en coordenadas de grados decimales (latitud)')
                        ->setCellValue('A27', 'Georeferencia y en coordenadas de grados decimales (longitud)')
                        ->setCellValue('A28', 'Fecha de censo del punto de luz')
                        ->setCellValue('A29', 'Nombre de la vía primaria')
                        ->setCellValue('A30', 'Nombre de la vía secundaria')
                        ->setCellValue('A31', 'Nombre de la delegación')
                        ->setCellValue('A32', 'Nombre de la colonia')
                        ->setCellValue('A33', 'Nombre de la calle')
                        ->setCellValue('A34', 'Nombre de las entre calles')
                        ->setCellValue('A35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('A37', 'CARGA KW')
                        ->setCellValue('C13', 'Fecha')
                        ->setCellValue('C14', 'Domicilio/Dirección')
                        ->setCellValue('C16', 'Colonia/Delegación')
                        ->setCellValue('C17', 'Zona')
                        ->setCellValue('C18', 'Fotografía')
                        ->setCellValue('C19', 'Voltaje y/o carga')
                        ->setCellValue('C20', 'Modelo y/o código')
                        ->setCellValue('C21', 'Estado Físico')
                        ->setCellValue('C22', 'Compañía')
                        ->setCellValue('C23', 'Tipo de fuente de poder')
                        ->setCellValue('C24', 'Tipo de poste')
                        ->setCellValue('C25', 'Latitud')
                        ->setCellValue('C26', 'Longitud')
                        ->setCellValue('C35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('C37', 'CARGA KW')
                        ->setCellValue('E13', 'Fecha')
                        ->setCellValue('E14', 'Domicilio/Dirección')
                        ->setCellValue('E15', 'Colonia/Delegación')
                        ->setCellValue('E16', 'Zona')
                        ->setCellValue('E17', 'Fotografía')
                        ->setCellValue('E18', 'Voltaje y/o carga')
                        ->setCellValue('E19', 'Modelo y/o código')
                        ->setCellValue('E20', 'Estado Físico')
                        ->setCellValue('E21', 'Tipo de iluminación')
                        ->setCellValue('E22', 'Latitud')
                        ->setCellValue('E23', 'Longitud')
                        ->setCellValue('E35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('E37', 'CARGA KW')
                        ->setCellValue('G13', 'Fecha')
                        ->setCellValue('G14', 'Domicilio/Dirección')
                        ->setCellValue('G15', 'Colonia/Delegación')
                        ->setCellValue('G16', 'Zona')
                        ->setCellValue('G17', 'Fotografía')
                        ->setCellValue('G18', 'Voltaje y/o carga')
                        ->setCellValue('G19', 'Modelo y/o código')
                        ->setCellValue('G20', 'Estado Físico')
                        ->setCellValue('G21', 'Tipo de iluminación')
                        ->setCellValue('G22', 'Cuantos focos')
                        ->setCellValue('G23', 'Latitud')
                        ->setCellValue('G24', 'Longitud')
                        ->setCellValue('G35', 'TOTAL DE EQUIPOS INSTALADOS')
                        ->setCellValue('G37', 'CARGA KW')
                        ->setCellValue('A41', 'Por parte de CFE Distribución')->mergeCells("A41:C41")
                        ->setCellValue('A43', '__________________________________')->mergeCells("A43:C43")
                        ->setCellValue('E41', 'Por parte del Usuario')->mergeCells("E41:G41")
                        ->setCellValue('E43', '__________________________________')->mergeCells("E43:G43")
                        ->setCellValue('A45', 'Nota: este formato puede ser modificado de acuerdo a las condiciones de la Zona y de los equipos que se encuentran en los recorridos en campo, manteniendo los datos relevantes a obtener de carga y total de equipos encontrados.')->mergeCells("A45:H46");
                        // ->setCellValue('A11', 'CONSECUTIVO')->mergeCells("A11:A13")
                        // ->setCellValue('B11', 'FECHA')->mergeCells("B11:B13")
                        // ->setCellValue('C11', 'No. de poste')->mergeCells("C11:C13")
                        // ->setCellValue('D11', 'DATOS DE LÁMPARAS ENCENDIDAS 24 HRS.')->mergeCells("D11:F12")
                        // ->setCellValue('D13', 'CAPACIDAD W')
                        // ->setCellValue('E13', 'TIPO')
                        // ->setCellValue('F13', 'VOLTAJE')
                        // ->setCellValue('G11', 'DATOS DE FACTURACIÓN')->mergeCells("G11:I12")
                        // ->setCellValue('G13', 'SECTOR')
                        // ->setCellValue('H13', 'SUBSECTOR')
                        // ->setCellValue('I13', 'CUENTA')
                        // ->setCellValue('J11', 'DATOS DE CORRECCIÓN DE LÁMPARAS ENCENDIDAS 24 HRS.')->mergeCells("J11:N12")
                        // ->setCellValue('L13', 'FECHA')
                        // ->setCellValue('M13', 'CAPACIDAD W')
                        // ->setCellValue('N13', 'MARCA')
                        // ->setCellValue('J13', 'TIPO')
                        // ->setCellValue('K13', 'VOLTAJE')
                        // ->setCellValue('A26', '__________________________')->mergeCells("A26:D26")
                        // ->setCellValue('A27', 'LUGAR Y FECHA')->mergeCells("A27:D27")
                        // ->setCellValue('F26', 'FECHA DE ENTREGA POR EL REP. SISTEMA AL. _________________')->mergeCells("F26:I27")
                        // ->setCellValue('K26', '__________________________')->mergeCells("K26:N26")
                        // ->setCellValue('K27', 'POR EL SISTEMA DE ALUMBRADO PÚBLICO')->mergeCells("K27:N27")
                        // ->setCellValue('A29', 'Nota: este formato puede ser modificado de acuerdo a las condiciones de la Zona y de los equipos que se encuentran en los recorridos en campo, manteniento los datos relevantes a obtener de carga y total de equipos encontrados.')->mergeCells("A29:N30");

            $style = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
               'font' => array( 'bold' => true )
            );

            $textCenter = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $alignLeft = array(
                'alignment' => array(
                   'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                   'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );

            $fontColor = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'a8adad')
                )
            );
            $foreColor = array(
                'font' => array( 'bold' => true )
            );

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('35')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('35')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('35')->setAutoSize(FALSE);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('20')->setAutoSize(FALSE);

            $objPHPExcel->getActiveSheet()->getStyle("A21")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A26")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A27")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("G35")->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle("A45")->getAlignment()->setWrapText(true);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A11:H12")->applyFromArray($fontColor);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A11:H12")->applyFromArray($foreColor);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A11:H12")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A13:A37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("C13:C37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("E13:E37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("G13:G37")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A6")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A41")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A43")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("E41")->applyFromArray($textCenter);
            $objPHPExcel->setActiveSheetIndex(0)->getStyle("E43")->applyFromArray($textCenter);
            // $objPHPExcel->setActiveSheetIndex(0)->getStyle("A10:N13")->applyFromArray($style);

            $objPHPExcel->setActiveSheetIndex(0)->getStyle("A11:H38")->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '211D1D')
                        )
                    )
                )
            );

            $objPHPExcel->getActiveSheet()->setTitle('Reporte');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);


            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="01simple.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;

        }
    }

    $reporte = new REP_RES_CEN_OTR_CAR_DIR;
?>
