<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/conexion_mysql.php';
include '../../../pillar/clases/model/user_model.php';

include 'ErrorController.php';
/**
*
*/


class UserController extends controlador
{
	protected $result;
	protected $model;

	public function Error($Error)
	{
		if ($Error==0) {
			$this-> Errors = array('CODIGO' => $Error,
				'DATOS' => 'FALLO DE AUTENTICACIÓN'
			);
		}
		if ($Error==1) {
			$this-> Errors = array('CODIGO' => $Error,
				'DATOS' => 'METODO NO ENCONTRADO'
			);
		}
		return $this-> Errors;
	}
	public function getAll()
	{
		$this-> model = new User;
		$this-> result = $this-> model -> getAll();

		$sql = "SELECT * FROM perfil";
		$conexion = new conexion;
		$perfiles = $conexion->getResults($sql);
		$this-> result['perfiles'] = $perfiles;
		$supervisores = $conexion->getResults("SELECT IDUSUARIO, CONCAT(NOMBRE, ' ', APATERNO, ' ', AMATERNO) SUPERVISOR FROM USUARIO WHERE IDPERFIL = 4 AND STATUS = 1");
		$this-> result['supervisores'] = $supervisores;

		echo json_encode($this-> result);
	}
	public function getUser($function,$id='')
	{
		$this-> model = new User;
		$this-> result= $this-> model -> $function($id);
		return $this-> result;
	}
	public function Account($function,$id='',$values,$status='')
	{
		$this-> model = new User;
		$this-> result= $this-> model -> $function($id,$values,$status);
		echo json_encode($this-> result);
	}
	public function SeeProfile($function,$id='')
	{
		$this-> model = new User;
		$this-> result= $this-> model -> $function($id);
		return $this-> result;
	}
}


$controller = new UserController;

$ErrorController= new ErrorController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion']) and $_POST['opcion']=='getAll') {
		$controller -> getAll();
	}elseif (isset($_POST['opcion']) and $_POST['opcion']=='Account') {
		$id = isset($_POST['key']) ? base64_decode($_POST['key']) : '';
		$values='';
		$status = isset($_POST['status']) ? $_POST['status'] : '';
		if ($_POST['opcion']='putUser' and isset($_POST['values'])) {
			$values=$_POST['values'];
		}
		$controller -> Account($_POST['action'],$id,$values,$status);
	}/*elseif (isset($_POST['opcion']) and $_POST['opcion']=='SeeProfile') {
		$id= (isset($_POST['id']) and $_POST['id']== $_POST['id']) ? $_POST['id']:'';
		echo json_encode($controller -> SeeProfile($_POST['action'],$id));

	}*/
	else{
		echo json_encode($ErrorController->ErrorOption());
	}

}else{
	//echo json_encode($UserController -> Error(0));
	echo json_encode($ErrorController->ErrorToken());
}
?>
