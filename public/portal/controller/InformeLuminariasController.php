<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include 'ErrorController.php';
/**
*
*/


class InformeLuminariasController
{
    protected $conexion;
	protected $result;
	protected $model;

    function __construct()
    {
        $this->conexion = new conexion;
        $this-> result = null;
        // $this-> model = new InformeLuminarias;
    }

	public function getLuminarias($idRegistroLuminaria)
	{
		// $this->result = $this->model->getRegistroLuminariaImg($idRegistroLuminaria);
		// $this->result['catalogo'] = $this->result;
        //
		// return json_encode($this->result);


		$sql = "SELECT * FROM REGISTROLUMINARIA WHERE IDREGISTRO=$idRegistroLuminaria AND ESTATUS=1";
		$registros = $this->conexion->getResults($sql);

		$respuesta['code'] = false;
		$respuesta['data'] = [];
		if (count($registros)>0) {
			$respuesta = $this->getRegistros($idRegistroLuminaria);
		}

		$generales = new Generales;
		$utilities = new utilities;

		$catalogos['ubicacionfisica'] = $generales->getCatalogo("CATALOGO_UBICACIONFISICA");
		$catalogos['extraubicacion'] = $utilities->objetivisa('CATEGORIAUBICACION',$generales->getCatalogo("EXTRAUBICACIONFISICA"));
		$catalogos['clasificaionlampara'] = $generales->getCatalogoCustom("SELECT DISTINCT IDCATEGORIA,NOMBRECATEGORIA FROM CATALOGO_TIPOLAMPARA");
		$catalogos['tipolampara'] = $utilities->objetivisa('IDCATEGORIA',$generales->getCatalogo("CATALOGO_TIPOLAMPARA"));
		$catalogos['watts'] = $generales->getCatalogo("CATALOGO_WATTS");


		$respuesta['catalogos'] = $catalogos;

		return $respuesta;
	}


    public function getRegistros($registroLuminaria)
	{
        $utilities = new utilities;
		$registros=[];
		$data=[];
		$respuesta['code'] = false;
		$respuesta['data'] = [];
		// if ($asignacion) {
			$sql = "SELECT * FROM REGISTROLUMINARIA WHERE IDREGISTRO=$registroLuminaria AND ESTATUS=1";
			$registros = $this->conexion->getResults($sql);

			// foreach ($registros  as $key => $resgitro) {
				$type = $registros[0]['IDTIPOLUMINARIA'];
				$key = $registros[0]['IDREGISTRO'];
				if ($type==1) {
					$this -> sql = "SELECT RL.*,TL.NOMBRE TIPO,
										CL.TIPOLUMINARIA KEYTIPOLUMINARIA,
										CASE WHEN CL.TIPOLUMINARIA=2 THEN 'ALUMBRADO PUBLICO (VÍA SECUNDARIA)' ELSE 'ALUMBRADO PUBLICO (VÍA PRIMARIA)' END TIPOLUMINARIA,
										CUF.IDUBICACIONFISICA,
										CUF.NOMBRE UBICACIONFISICA/*CL.EXTRAUBICACIONFISICA*/,CCT.NOMBRE CARACTERISTICATECNICA
									FROM REGISTROLUMINARIA RL
										LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
										LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
										LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
										LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA

                                        -- LEFT JOIN EXTRAUBICACIONFISICA EXTFIS ON EXTFIS.IDEXTRAUBICACIONFISICA = CL.EXTRAUBICACIONFISICA
                                        -- LEFT JOIN CATALOGO_TIPOLAMPARA TPLAMP ON TPLAMP.IDTIPOLAMPARA = CL.TIPOLAMAPARA
                                        -- LEFT JOIN CATALOGO_WATTS CATWAT ON CATWAT.IDWATTS = CL.VALORWATTS
									WHERE RL.IDREGISTRO=$key";
				}
				if ($type==2) {
					$this -> sql = "SELECT
										RL.*,TL.NOMBRE TIPO,TS.NOMBRE TIPOLUMINARIA,
										CUF.NOMBRE UBICACIONFISICA,CUF.IDUBICACIONFISICA/*CL.EXTRAUBICACIONFISICA*/,CCT.NOMBRE CARACTERISTICATECNICA
									FROM REGISTROLUMINARIA RL
										LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
										LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
		    							LEFT JOIN TIPOSEMAFORO TS ON TS.IDTIPOSEMAFORO = CL.TIPOLUMINARIA
										LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
										LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA
									WHERE RL.IDREGISTRO=$key";
				}

				if ($type==3) {
					$this -> sql = "SELECT
										RL.*,TL.NOMBRE TIPO,TCD.NOMBRE TIPOLUMINARIA,
										CUF.NOMBRE UBICACIONFISICA,CUF.IDUBICACIONFISICA/*CL.EXTRAUBICACIONFISICA*/,CCT.NOMBRE CARACTERISTICATECNICA
									FROM REGISTROLUMINARIA RL
										LEFT JOIN TIPOLUMINARIA TL ON TL.IDTIPOLUMINARIA = RL.IDTIPOLUMINARIA
										LEFT JOIN CUESTIONARIOLUMINARIA CL ON CL.IDREGISTROLUMINARIA = RL.IDREGISTRO
		    							LEFT JOIN TIPOCARGADIRECTA TCD ON TCD.IDTIPOCARGADIRECTA = CL.TIPOLUMINARIA
										LEFT JOIN CATALOGO_UBICACIONFISICA CUF ON CUF.IDUBICACIONFISICA = CL.UBICACIONFISICA
										LEFT JOIN CATALOGO_CARACTERISTICATECNICA CCT ON CCT.IDCARATERISTICATECNICA = CL.CARACTERITICATECTNICA
									WHERE RL.IDREGISTRO=$key";
				}

				$this->result = $this->conexion->getResults($this -> sql);

				$sql = "SELECT IMAGEN.*, EXTFIS.NOMBRE MODELO, TPLAMP.NOMBRECATEGORIA CLASIFICACIONLUMINARIA, TPLAMP.NOMBRETIPOLAMAPARA, CATWAT.VALOR VALOR_WATTS FROM IMAGEN
                            LEFT JOIN EXTRAUBICACIONFISICA EXTFIS ON EXTFIS.IDEXTRAUBICACIONFISICA = IMAGEN.MODELO
                            LEFT JOIN CATALOGO_TIPOLAMPARA TPLAMP ON TPLAMP.IDTIPOLAMPARA = IMAGEN.TIPOLAMPARA
                            LEFT JOIN CATALOGO_WATTS CATWAT ON CATWAT.IDWATTS = IMAGEN.WATTS
                            WHERE IDREGISTROLUMINARIA = $key";

				$images = $this->conexion->getResults($sql);

                $response['images'] = $utilities->objetivisa('TIPOIMAGEN',$images);
				// $response['images'] = $images;
				$response['type'] = $type;
				if (count($this->result)>0) {
					//$data = $this -> result[0];
					$response['generales'] = $this->result[0];
					//$response['code'] = true;
					array_push($data, $response);
				}
			// }
			$respuesta['code'] = true;
			$respuesta['data'] = $data;
		// }
		return $respuesta;
	}

}


// $controller = new InformeLuminariasController;
//
// $ErrorController= new ErrorController;
//
// if (isset($_POST['csrf_token'])) {
// 	if (isset($_POST['opcion']) and $_POST['opcion']=='getLuminaria') {
// 		$controller -> getLuminaria($_POST['luminaria']);
// 	}
// 	else{
// 		echo json_encode($ErrorController->ErrorOption());
// 	}
//
// }else{
// 	//echo json_encode($UserController -> Error(0));
// 	echo json_encode($ErrorController->ErrorToken());
// }
?>
