<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
ini_set('memory_limit', '-1');
include '../../../pillar/clases/conexion_mysql.php';
include '../../../pillar/clases/model/main_model.php';
include '../../../pillar/clases/model/generales_model.php';
include 'ErrorController.php';

class main_controller{

    protected $result;
    protected $model;
    function __construct()
    {
        $this-> result = null;
        $this-> model = new ListadoLuminarias;
        $this-> model_g = new Generales;
    }

    public function dates()
    {
        $this -> result = $this -> model_g -> getDatesRegistros();
        echo json_encode($this -> result);
    }

    public function main(){
        $this -> result = $this -> model -> getListado($_GET['init'],$_GET['end']);
        echo json_encode($this -> result);
    }
    public function detalle($key){
        $this -> result = $this -> model -> getdetalle($key);
        echo json_encode($this -> result);
    }
}


    $controller = new main_controller;
    if(isset($_GET['option'])){
        $controller->{$_GET['option']}();
    }
    elseif (!isset($_GET['key'])) {
        $controller->main();
    }
    elseif(isset($_GET['key'])){
        $controller->detalle($_GET['key']);
    }

?>
