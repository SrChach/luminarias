<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
header('Content-Type: text/html; charset=UTF-8');
include_once './vendor/parametros.php';

if(isset($_GET['salir']) && $_GET['salir'] == 1){
  	session_destroy();
  	echo "<script>window.location='./'</script>";
}

include_once './view/template.php';

?>