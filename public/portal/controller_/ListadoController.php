<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include_once '../../../pillar/clases/controlador.php';
include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/utilities.php';
include_once 'ErrorController.php';

include_once '../../../pillar/clases/listado.php';
/**
* Develop By: JOSE JUAN DEL PRADO
*/
class Listadocontroller extends controlador
{
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Listado;
	}
	public function Listado($function,$idDel='',$idSub='',$idMun='',$idLoc='')
	{
		$this -> result = $this -> model -> $function($idDel,$idSub,$idMun,$idLoc);
		return $this -> result;
	}
}

$controller= new Listadocontroller;
$ErrorController= new ErrorController;
if ($_POST) {
	if (isset($_POST['csrf_token'])) {
		if (isset($_POST['opcion']) and $_POST['opcion']=='Listado') {
			//$idP= (isset($_POST['idP']) and $_POST['idP']== $_POST['idP']) ? $_POST['idP']:'';
			$idDel= (isset($_POST['idDel']) and $_POST['idDel']== $_POST['idDel']) ? $_POST['idDel']:'';
			$idSub= (isset($_POST['idSub']) and $_POST['idSub']== $_POST['idSub']) ? $_POST['idSub']:'';
			$idMun= (isset($_POST['idMun']) and $_POST['idMun']== $_POST['idMun']) ? $_POST['idMun']:'';
			$idLoc= (isset($_POST['idLoc']) and $_POST['idLoc']== $_POST['idLoc']) ? $_POST['idLoc']:'';
			echo json_encode(
							$controller->Listado(
												$_POST['action'],
												$idDel,$idSub,
												$idMun,$idLoc
											)
							);
		}else{
			echo json_encode($ErrorController->ErrorOption());	
		}
	}else{
		echo json_encode($ErrorController->ErrorToken());
	}

}else{
	echo json_encode($ErrorController->ErrorMethod());
}
?>
