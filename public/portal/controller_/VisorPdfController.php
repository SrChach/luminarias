<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 

include '../../../pillar/clases/controlador.php';
include_once '../../../pillar/clases/generales.php';
/**
* 
*/


class VisorPdfController extends controlador
{
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Generales;
	}
	public function Error($Error)
	{
		if ($Error==0) {
			$this-> Errors = array('CODIGO' => $Error,
									'DATOS' => 'FALLO DE AUTENTICACIÓN'
									);
		}
		return $this-> Errors;
	}
	public function generales($function,$idPrograma='',$values='')
	{
		$this-> result= $this-> model -> $function($idPrograma,$values);
		return $this-> result;
	}
	
}

$controller = new VisorPdfController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion']) and $_POST['opcion']=='generales') {
		$idPrograma = '';
		if (isset($_POST['idPrograma'])) {
 			$idPrograma=$_POST['idPrograma'];
                $values = '';
				if (isset($_POST['values'])) {
							$values=$_POST['values'];	
						}
		}
		echo json_encode($controller -> generales($_POST['action'],$idPrograma,$values));
	}
	else{
		echo json_encode($controller -> Error(0));
	}
	
}else{
	echo json_encode($controller -> Error(0));
}
 ?>