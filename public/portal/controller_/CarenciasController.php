<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/controlador.php';
include_once 'ErrorController.php';
include_once '../../../pillar/clases/utilities.php';

include_once '../../../pillar/clases/carencias.php';
/**
* 
*/
class CarenciasController extends controlador
{
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Carencias;
	}
	public function Carencias($function,$id_del,$id_sub,$id_mun,$id_loc,$org)
	{
		return $this -> model -> $function($id_del,$id_sub,$id_mun,$id_loc,$org);
		
	}
}

$ErrorController= new ErrorController;
$controller= new CarenciasController;
$controller->VerificaSolicitud();

if ($_POST) {
	if (isset($_POST['csrf_token'])) {
		if (isset($controller->opcion) and $controller->opcion == 'Carencias') {
			$id_del=isset($_POST['id_del']) ? $_POST['id_del']:'';
			$id_sub=isset($_POST['id_sub']) ? $_POST['id_sub']:'';
			$id_mun=isset($_POST['id_mun']) ? $_POST['id_mun']:'';
			$id_loc=isset($_POST['id_loc']) ? $_POST['id_loc']:'';
			$org=isset($_POST['org']) ? $_POST['org']:'';
			echo json_encode($controller -> Carencias ($_POST['action'],$id_del,$id_sub,$id_mun,$id_loc,$org));
		}else{
			echo json_encode($ErrorController->ErrorMessages('Option'));	
		}
	}else{
		echo json_encode($ErrorController->ErrorMessages('Token'));
	}
	
}else{
	echo json_encode($ErrorController->ErrorMessages('Method'));
}
?>