<?php
    include '../../../pillar/clases/conexion.php';
    include '../../../pillar/clases/cambiosCuis_model.php';

    class cambiosCuis_controller{

        function getTipoCambiosCuis(){
            $cambiosCuis = new cambiosCuis_model;
            $results = $cambiosCuis->getTypeChangesCuis();
            return $results;
        }
        function getCambiosCuis(){
            $cambiosCuis = new cambiosCuis_model;            
            $results = $cambiosCuis->getChangesCuis();
            return $results;
        }

        function getFilterCambioCuis($objectData){
            //if(array_key_exists('folio', $objectData) && array_key_exists('fechaInicio', $objectData) && array_key_exists('fechaFin', $objectData) && array_key_exists('tipoCambio', $objectData)) return '';
            $cambiosCuis = new cambiosCuis_model;            
            $results = $cambiosCuis->getChangesCuisFilter($objectData);
            return $results;
        }
    }


    $cambios_cuis = new cambiosCuis_controller;
    $option = $_POST['option'];


    if($option == 'getCambiosCuis'){
        echo json_encode($cambios_cuis->getCambiosCuis());
    }
    else if($option == 'getTipoCambiosCuis'){
        echo json_encode($cambios_cuis->getTipoCambiosCuis());
    }
    else if($option == 'getFilterCambioCuis'){
        if(!empty($_POST))
        echo json_encode($cambios_cuis->getFilterCambioCuis($_POST));
    }

?>