<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 

/**
* 
*/
include_once '../../../pillar/clases/padrones.php';
include_once '../../../pillar/clases/controlador.php';

class PadronController  extends controlador
{
	protected $result;
	protected $model;
	public function Error($Error)
	{
		if ($Error==0) {
			$this-> Errors = array('CODIGO' => $Error,
									'DATOS' => 'FALLO DE AUTENTICACIÓN'
									);
		}
		return $this-> Errors;
	}

	public function Padrones($function,$idPrograma,$cveedo='',$cvemun='',$cveloc='',$cvedel='',$cvesubdel='',$foliocuis='',$nombre='',$inicio='',$fin='')
	{	
		$this -> model = new padrones;
		return $this -> model -> $function($idPrograma,$cveedo,$cvemun,$cveloc,$cvedel,$cvesubdel,$foliocuis,$nombre,$inicio,$fin);
	}
}

$controller = new PadronController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion']) and $_POST['opcion']=='Padron') {
		$idPrograma= (isset($_POST['idPrograma']) and $_POST['idPrograma']== $_POST['idPrograma']) ? $_POST['idPrograma']:'';
		$cveedo= (isset($_POST['cveedo']) and $_POST['cveedo']== $_POST['cveedo']) ? $_POST['cveedo']:'';
		$cvemun= (isset($_POST['cvemun']) and $_POST['cvemun']== $_POST['cvemun']) ? $_POST['cvemun']:'';
		$cveloc= (isset($_POST['cveloc']) and $_POST['cveloc']== $_POST['cveloc']) ? $_POST['cveloc']:'';
		$cvedel= (isset($_POST['cvedel']) and $_POST['cvedel']== $_POST['cvedel']) ? $_POST['cvedel']:'';
		$cvesubdel= (isset($_POST['cvesubdel']) and $_POST['cvesubdel']== $_POST['cvesubdel']) ? $_POST['cvesubdel']:'';
		$foliocuis= (isset($_POST['foliocuis']) and $_POST['foliocuis']== $_POST['foliocuis']) ? $_POST['foliocuis']:'';
		$nombre= (isset($_POST['nombre']) and $_POST['nombre']== $_POST['nombre']) ? $_POST['nombre']:'';
		$inicio= (isset($_POST['inicio']) and $_POST['inicio']== $_POST['inicio']) ? $_POST['inicio']:'';
		$fin= (isset($_POST['fin']) and $_POST['fin']== $_POST['fin']) ? $_POST['fin']:'';
		echo json_encode($controller -> Padrones($_POST['action'],$idPrograma,$cveedo,$cvemun,$cveloc,$cvedel,$cvesubdel,$foliocuis,$nombre,$inicio,$fin));
	}
	else{
		echo json_encode($controller -> Error(0));
	}
}else{
	echo json_encode($controller -> Error(0));
}

?>