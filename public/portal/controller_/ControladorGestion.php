<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/evidencia.php';
include '../../../pillar/clases/gestion.php';
include '../../../pillar/clases/utilities.php';

class ControladorGestion extends controlador{
	
	function validacion($lote = "",$foliocuis = "",$idListado = "",$c_persona = "",$asignacion = ""){
		$evidencia = new evidencia;
		$gestion = new gestion;
		$utilities = new utilities;


		$query_sec = "";
		if($lote != ""){
			$imagenes = $evidencia -> evidencia_lote($lote);
		}elseif($foliocuis != ""){
			$imagenes = $evidencia -> evidencia_eventos_folio_cuis($foliocuis);
		}elseif($idListado != ""){
			$imagenes = $evidencia -> evidencia_eventos_idListado($idListado);	
		}elseif($c_persona != "" ){
			$imagenes = $evidencia -> evidencia_c_persona($c_persona);	 
		}

		
		$tiposDocumento = $gestion -> traeTipoImagen();
		$tiposDocumentoEspecifico = $gestion -> traeTipoDocumentoEspecifico();
		$preguntas = $gestion -> preguntas();
		$respuestas = $gestion -> respuestas($utilities->serializa("IDIMAGEN",$imagenes) );

		$regresa['imagenes'] = $imagenes;
		$regresa['tiposDocumento'] = $tiposDocumento;
		$regresa['tiposDocumentoEspecifico'] = $tiposDocumentoEspecifico;
		$regresa['preguntas'] = $preguntas;
		$regresa['respuestas'] = $respuestas;
		//$regresa['folios'] = $respuestas;

		return $regresa;
	}	
}


$ControladorGestion = new ControladorGestion;
$ControladorGestion->VerificaSolicitud();
$opcion = $ControladorGestion->opcion;

if($opcion == "imagenes"){

	$lote = (isset($_POST['lote']))?$_POST['lote']:"";
	$foliocuis = (isset($_POST['foliocuis']))?$_POST['foliocuis']:"";
	$idListado = (isset($_POST['idListado']))?$_POST['idListado']:"";
	$c_persona = (isset($_POST['c_persona']))?$_POST['c_persona']:"";
	$asignacion = (isset($_POST['asignacion']))?$_POST['asignacion']:"";

	echo json_encode( $ControladorGestion-> validacion($lote,$foliocuis,$idListado,$c_persona,$asignacion)); 
}











?>