<?php
include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/utilities.php';
include '../../../pillar/clases/gruposTrabajo_model.php';
    class gruposTrabajo_controller{
        function getInitInfo(){
            $grupTbMod = new gruposTrabajo_model;
            $utilities = new utilities;

            $workGroups = $grupTbMod->getWorkGroups();
            $listTypeImg = $grupTbMod->getTypeImg();
            $listOperativeUsers = $grupTbMod->getUsersOperative();


            $results['listTypeImg'] = $utilities->objetivisa('IDTIPOIMAGEN',$listTypeImg);
            $results['listOperativeUsers'] = $listOperativeUsers;
            $results['workGroups'] = $workGroups;
            
            return $results;
        }

        function findWorkGroups(){
            $grupTbMod = new gruposTrabajo_model;
            $utilities = new utilities;
            $workGroups = $grupTbMod->getWorkGroups();
            $listTypeImg = $grupTbMod->getTypeImg();

            $results['listTypeImg'] = $utilities->objetivisa('IDTIPOIMAGEN',$listTypeImg);
            $results['workGroups'] = $workGroups;
            return $results;
        }
        
        function findUsers(){
            $grupTbMod = new gruposTrabajo_model;
            $listOperativeUsers = $grupTbMod->getUsersOperative();

            $results['listOperativeUsers'] = $listOperativeUsers;
            
            return $results;
        }
        
        function addWorkGroup($objectData){
            if(empty($objectData['nameGroup']) || empty($objectData['origin']) || empty($objectData['typeImg']) )
            return 'Información Incompleta';
            
            
            $grupTbMod = new gruposTrabajo_model;
            $result = $grupTbMod->insertWorkGroup($objectData['nameGroup'], $objectData['origin'], $objectData['typeImg']);
            return $result;
        }
        
        function removeWorkGroup($keyGroup){
            if(empty($keyGroup)) return false;

            $grupTbMod = new gruposTrabajo_model;
            $result = $grupTbMod->deleteWorkGroup($keyGroup);
            return $result;
        }

        function asignUsersGroup($keyWorkGroup, $objectData){
            if(empty($keyWorkGroup) || empty($objectData)) return false;

            $grupTbMod = new gruposTrabajo_model;
            $result = $grupTbMod->asignGroupToUsers($keyWorkGroup, $objectData);
            return $result;
        }
    }

    $option = $_POST['option'];
    $grupTbCont = new gruposTrabajo_controller;


    switch ($option){
        case "getInitInfo": 
            echo json_encode( $grupTbCont->getInitInfo());
            break;
        case "addWorkGroup": 
            echo json_encode( $grupTbCont->addWorkGroup($_POST));
            break;
        case "findWorkGroups": 
            echo json_encode( $grupTbCont->findWorkGroups());
            break;
        case "findUsers": 
            echo json_encode( $grupTbCont->findUsers());
            break;
        case "removeWorkGroup": 
            echo json_encode( $grupTbCont->removeWorkGroup($_POST['keyWork']));
            break;
        case "asignUsers": 
            echo json_encode( $grupTbCont->asignUsersGroup($_POST['keyWorkGroup'], $_POST['users']));
            break;
        default : 
            echo 'Error';
            break;
    }

?>