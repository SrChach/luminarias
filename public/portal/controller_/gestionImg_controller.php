<?php
include "../../../pillar/clases/conexion.php";
include "../../../pillar/clases/gestionImg_model.php";

header('Content-type: application/json');

    class gestionImg_prueba{

        function getCategories(){
            $gestionImg_model = new gestionImg_model;
            $results = $gestionImg_model->getCategories();
            
            $filterResults = array();
            foreach($results as $item){
                array_push($filterResults, array('CATEGORIA' =>$item['CATEGORIA']));
            }
            return $filterResults;
        }
        
        function getTypeImages($category){            
            $gestionImg_model = new gestionImg_model;
            $results = $gestionImg_model->getTypeImages($category);
            return $results;
        }

        function getTypeDocs($typeImage){                        
            $gestionImg_model = new gestionImg_model;
            $results = $gestionImg_model->getTypeDocs($typeImage);
                        
            // $filterResults = array();
            // foreach($results as $item){
            //     array_push($filterResults, array('ClAVEDOCUMENTO' => $item['IDTIPODOCUMENTO'], 'TIPODOCUMENTO' => $item['TIPODOCUMENTO']));
            // }
            // return $filterResults;
            return $results;
        }

        function newCategory($objectData){
            $gestionImg_model = new gestionImg_model;
            $resultCat = $gestionImg_model->createCategory($objectData);
            
            if($resultCat){
                $resultTypeImg = $gestionImg_model->createTypeImageLastCat($objectData);                
                if($resultTypeImg){
                    $resultTypeDoc = $gestionImg_model->createTypeDocumentLastCat($objectData);
                    return $resultTypeDoc;
                }
            }
            return false;
        }

        function newTypeImage($objectData){
            $gestionImg_model = new gestionImg_model;
            $result = $gestionImg_model->createTypeImage($objectData);
            
            if($result){
                $resultTypImg = $gestionImg_model->createTypeDocumentLastCat($objectData);
                return $resultTypImg;
            }

            return false;
        }

        function newTypeDocument($objectData){
            $gestionImg_model = new gestionImg_model;
            $result = $gestionImg_model->createTypeDocument($objectData);
            return $result;
        }

        function editTypeImage($objectData){            
            $gestionImg_model = new gestionImg_model;
            $result = $gestionImg_model->updateTypeImage($objectData);
            return $result;
        }

        function editTypeDocument($objectData){
            $gestionImg_model = new gestionImg_model;
            $result = $gestionImg_model->updateTypeDocument($objectData);
            return $result;
        }
    }

    $newGestionImg = new gestionImg_prueba;
    $option = $_POST['option'];

    if($option == 'categories'){
        echo json_encode($newGestionImg->getCategories());
    }
    elseif($option == 'typeDocument'){
        if(empty($_POST['typeImg']))echo 'No se recibio información';
        else{
            $typeImage = $_POST['typeImg'];
            echo json_encode($newGestionImg->getTypeDocs($typeImage));  
        }        
    }
    elseif($option == 'typeImage'){        
        if(empty($_POST['category']))echo 'No se recibio información';
        else{
            $category = $_POST['category'];
            echo json_encode($newGestionImg->getTypeImages($category));  
        } 
    }
    elseif($option == 'newCategory'){
        echo $newGestionImg->newCategory($_POST);        
    }
    elseif($option == 'newTypeImg'){
        echo $newGestionImg->newTypeImage($_POST);
    }
    elseif($option == 'newTypeDoc'){
        echo $newGestionImg->newTypeDocument($_POST);
    }
    elseif($option == 'editTypeImg'){        
        echo $newGestionImg->editTypeImage($_POST);
    }
    elseif($option == 'editTypeDoc'){        
        echo $newGestionImg->editTypeDocument($_POST);
    }
    





?>