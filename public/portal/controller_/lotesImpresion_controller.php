<?php
include '../../../pillar/clases/lotesImpresion_model.php';
include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/exporta_reportesMail.php';
include '../../../../parametros.php';
include '../../../pillar/clases/mailReportes.php';
include '../../../public/portal/libraries/phpmailer-master/class.phpmailer.php';

    class lotesImpresion_controller{
        function getAllLotesImpresion(){
            $lotesImpresion_model = new lotesImpresion_model;
            $result = $lotesImpresion_model->getAllLotsImpresion();
            return $result;
        }

        function getLoteActual(){
            $lotesImpresion_model = new lotesImpresion_model;
            $result = $lotesImpresion_model->getLoteActual();
            return $result;
        }

        function closeLoteActual($cveLoteActual){
            $lotesImpresion_model = new lotesImpresion_model;
            $result = $lotesImpresion_model->closeLotActive($cveLoteActual);
            return $result;
        }

        function printLote($cveLoteImprimir){
            if(empty($cveLoteImprimir)) return false;

            $lotesImpresion_model = new lotesImpresion_model;
            $queryResult = $lotesImpresion_model->printLote($cveLoteImprimir);

            $reporte = new Reports;
            session_start();
            $nombreCsv = 'LoteImpresion_'.$cveLoteImprimir.'_'.date("Ymdhis");
            $ruta = '../../../../reportes/'.$nombreCsv;
            $nombreCsv .= '.csv';
            $reporte->csv($queryResult, $ruta, $nombreCsv, $_SESSION['IDUSUARIO'], '');                        
            
            return true;
            
            // return $results;
        }
    }

    $lotesImpresion = new lotesImpresion_controller;
    $option = $_POST['option'];

    if($option == 'getAllLots'){
        echo json_encode( $lotesImpresion->getAllLotesImpresion() );
    }
    else if($option == 'getLotActive'){
        echo json_encode( $lotesImpresion->getLoteActual() );
    }
    else if($option == 'closeLot'){
        echo json_encode( $lotesImpresion->closeLoteActual($_POST['cveLoteActual']) );
    }
    else if($option == 'printLote'){
        echo json_encode( $lotesImpresion->printLote($_POST['cveLote']) );
    }
?>
