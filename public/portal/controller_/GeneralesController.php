<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 

include '../../../pillar/clases/controlador.php';
include_once '../../../pillar/clases/generales.php';
/**
* 
*/


class GeneralesController extends controlador
{
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Generales;
	}
	public function Error($Error)
	{
		if ($Error==0) {
			$this-> Errors = array('CODIGO' => $Error,
				'DATOS' => 'FALLO DE AUTENTICACIÓN'
			);
		}
		return $this-> Errors;
	}
	public function generales($function,$idPrograma='',$item='',$key='',$values='')
	{	
		if ($function!='comboMainSubitem') {
			if ($function=='getProfilesAll') {
				$this-> result= $this-> model -> $function($values,$idPrograma);
			}else{
				$this-> result= $this-> model -> $function($idPrograma);	
			}
			
		}else{
			$this-> result= $this-> model -> $function($item,$key,$idPrograma);
		}


		return $this-> result;
	}

	/*public function generalesValues($function,$idPrograma='',$values='')
	{	
		$this-> result= $this-> model -> $function($idPrograma,$values);
		return $this-> result;
	}*/

}
$controller = new GeneralesController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion']) and $_POST['opcion']=='generales') {
		$idPrograma = '';
		if (isset($_POST['idPrograma'])) {
			$idPrograma=$_POST['idPrograma'];
		}
		$item = (isset($_POST['item'])) ? $_POST['item']:'';
		$key = (isset($_POST['key'])) ? $_POST['key']:'';
		$values = (isset($_POST['values'])) ? $_POST['values']:'';
		echo json_encode($controller -> generales($_POST['action'],$idPrograma,$item,$key,$values));
	}else{
		echo json_encode($controller -> Error(0));
	}

	/*if (isset($_POST['opcion']) and $_POST['opcion']=='generalesValues') {
		$idPrograma = '2';
		if (isset($_POST['idPrograma'])) { $idPrograma=$_POST['idPrograma']; }
		$values = (isset($_POST['values'])) ? $_POST['values']:'';
		echo json_encode($controller -> generalesValues($_POST['action'],$idPrograma,$values));
	}*/



}else{
	echo json_encode($controller -> Error(0));
}
?>