<?php
include '../../../pillar/clases/conexion_mysql.php';
include '../../../pillar/clases/model/main_model.php';

class main_controller{

    protected $result;
    protected $model;
    function __construct()
    {
        $this-> result = null;
        $this-> model = new ListadoLuminarias;
    }

    public function main(){
        $this -> result = $this -> model -> getListado();
        echo json_encode($this -> result);
    }
    public function detalle($key){
        $this -> result = $this -> model -> getdetalle($key);
        echo json_encode($this -> result);
    }
}


    $controller = new main_controller;
    if (!isset($_GET['key'])) {
        $controller->main();
    }else{
        $controller->detalle($_GET['key']);
    }


    //$option = $_POST['option'];


?>
