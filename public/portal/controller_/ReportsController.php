<?php

set_time_limit(60000);
ini_set('memory_limit', -1);
ignore_user_abort(1);
ini_set('max_execution_time', 30000);


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 

/**
* 
*/
include_once '../../../pillar/clases/controlador.php';
include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/utilities.php';

include_once '../../../pillar/clases/reportes.php';

include_once 'ErrorController.php';
class ReportsController extends controlador
{
	protected $result;
	protected $model;
	public function Reportes($function,$params)
	{	
        if ($function=='credencialesDelegacion') {
            //var_dump($params);
            return false;
        }
		$this -> model = new Reports;
		$this -> result = $this -> model -> $function($params);

		if ($function=='ReportPogramaEvento') {
			$csv_export ="IDPADRON, IDPROGRAMA, IDLISTADO, FECHAINSERCION, FECHAMODIFICACION, DISTRIBUCION_OPERATIVA, ESTATUS, IDTIPOEVENTO, NOMBREPROGRAMA, TIPOEVENTO, FOLIOCUIS, NOMBRE, APATERNO, AMATERNO, BENEFICIARIO, ORIGEN, DELEGACION, SUBDELEGACION, CVEEDO, NOMBREENT, CVEMUN, NOMBREMUN, CVELOC, NOMBRELOC";
		}
		if ($function=='Avances') {
			$csv_export =" IDEVENTO, ID_DEL, DELEGACION, ID_SUB, SUBDELEGACION, CVEEDO, NOMBREENT, CVEMUN, NOMBREMUN, CVELOC, NOMBRELOC, LATITUD, LONGITUD, IDPADRON, ID_PROGRAMA, PROGRAMA, ID_EVENTO, EVENTO, IDLISTADO, FOLIOCUIS, C_PERSONA, FOLIOPDA, FECHAVISITA, FECHA_INI, FECHA_TERMINO, IDUSUARIOSINCRONIZACION, IMEI, OBSERVACION, VALIDACION, IDUSUARIOVALIDACION, OBSERVACIONVALIDACION, ID_CODIGO_RESULTADO, CODIGO_RESULTADO, AVANCE, DISTRIBUCION_OPERATIVA, ESTATUS, NOMBRE, APATERNO, AMATERNO, GENERO, PARENTESCO, C_INTEGRANTE";
		}

		if ($function=='Credenciales') {
			$csv_export ="  ID_DEL, DELEGACION, ID_SUBDEL, SUBDELEGACION, CVEEDO, NOMBREENT, CVEMUN, NOMBREMUN, CVELOC, NOMBRELOC, IDPADRON, IDLISTADO, IDCREDENCIAL, CLAVE_PER, IDPROGRAMA, FOLIOCUIS, C_PERSONA, IDTIPOEVENTO, TIENE";
		}

    	/*$csv_export.= '
    	';*/
    	foreach ($this -> result ['DATOS'] as $c) {
    		$csv_export.= '
    		';
      		// create line with field values
    		foreach ($c as $d){
    			$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");

    			$reemplazar=array("", "", "", "");

    			$csv_export .= str_ireplace($buscar,$reemplazar, str_replace('\n','', utf8_decode ( str_replace('\n','', str_replace(',','' ,trim ( $d ) ) ) ))). ',';
    		} 
        		/*$csv_export.= '
        		';*/
        }

        	header("Content-type: text/x-csv");
        	header("Content-Disposition: attachment; filename=".$this -> result ['NAME'].".csv");
            header("Content-Type: application/force-download");

        	return $csv_export;
        }
    }







    $controller= new ReportsController;
    $ErrorController= new ErrorController;
    if ($_GET) {
    	if (isset($_GET['csrf_token'])) {
    		if (isset($_GET['opcion']) and $_GET['opcion']=='Reportes') {
    			$params= (isset($_GET['params']) and $_GET['params']== $_GET['params']) ? json_decode($_GET['params'],true):'';
    			echo $controller->Reportes($_GET['tipo'],$params);

    		}else{
    			echo json_encode($ErrorController->ErrorOption());	
    		}
    	}else{
    		echo json_encode($ErrorController->ErrorToken());
    	}
    }else{
    	echo json_encode($ErrorController->ErrorMethod());
    }
    ?>