<?php
ini_set('memory_limit', -1);
// ignore_user_abort(1); 
ini_set('max_execution_time', 10000);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/altaBajaPadron_model.php';
include '../../../pillar/clases/utilities.php';
include '../../../pillar/clases/exporta_reportesMail.php';
include '../../../pillar/clases/MailGeneral.php';

include '../../../pillar/clases/mailReportes.php';
include '../../../pillar/clases/controlador.php'; 
include '../../../pillar/clases/BodyMail.php';
include '../../../../parametros.php'; 
include '../../../public/portal/libraries/phpmailer-master/class.phpmailer.php';


// include 'exportaRepoGeneral.php';

    class altaBajaPadron_controller{

        function getInfoPadron($objectData){
            if(empty($objectData)) return false;
            
            $altaBajaPadron_model = new altaBajaPadron_model;
            $results = $altaBajaPadron_model->getPadron(false,
                                                        $objectData['idPrograma'],$objectData['estado'],
                                                        $objectData['municipio'],$objectData['localidad'],
                                                        $objectData['delegacion'],$objectData['subdelegacion'],
                                                        $objectData['folio_cuis'], $objectData['nombre']);
            return $results;
        }

        function downloadInfoPadron($objectData){
            if(empty($objectData)) return false;
            
            $altaBajaPadron_model = new altaBajaPadron_model;
            $queryResult = $altaBajaPadron_model->getPadron(true,
                                                        $objectData['idPrograma'],$objectData['estado'],
                                                        $objectData['municipio'],$objectData['localidad'],
                                                        $objectData['delegacion'],$objectData['subdelegacion'],
                                                        $objectData['folio_cuis'], $objectData['nombre']);
            $reporte = new Reports;
            session_start();
            $nombreCsv = 'padron_'.$objectData['idPrograma'].'_'.date("Ymdhis");
            $ruta = '../../../../reportes/'.$nombreCsv;
            $nombreCsv .= '.csv';
            $reporte->csv($queryResult, $ruta, $nombreCsv, $_SESSION['IDUSUARIO'], '');                        
            
            return true;
        }


        function searchPrograms(){
            $altaBajaPadron_model = new altaBajaPadron_model;
            $results = $altaBajaPadron_model->getPrograms();
            return $results;
        }

        function searchProgramTypeEvt($cvePrograma){
            if(empty($cvePrograma)) return false;
            $altaBajaPadron_model = new altaBajaPadron_model;
            $results = $altaBajaPadron_model->getProgramsTypeEvt($cvePrograma);
            return $results;
        }



        // function initSearchPadron(){
        //     $altaBajaPadron_model = new altaBajaPadron_model;
        //     $utilities = new utilities;

        //     $delegaciones = $altaBajaPadron_model->delegaciones();
        //     $subdelegaciones = $utilities->objetivisa("ID_DEL",$altaBajaPadron_model->subdelegaciones());

        //     $estados = $altaBajaPadron_model -> estados();
        //     $municipios = $utilities->objetivisa("CVE_ENTIDAD_FEDERATIVA",$altaBajaPadron_model -> municipios());
        //     $localidades = $utilities->objetivisa("CVE_MUNICIPIO",$altaBajaPadron_model -> localidades());


        //     $regresa['delegaciones'] = $delegaciones;
        //     $regresa['subdelegaciones'] = $subdelegaciones;
        //     $regresa['estados'] = $estados;
        //     $regresa['municipios'] = $municipios;
        //     $regresa['localidades'] = $localidades;
        //     return $regresa;
        // }


        function updateRowsPadron(){
            $fichero = $_FILES["actsPadron"]["name"];
            if(empty($fichero)) 
                return 'No se cargo ningun archivo';

            $fichero_tmp = $_FILES["actsPadron"]["tmp_name"];

            $file = fopen("$fichero_tmp", "r");

            
            // $actualizaciones = [];
            $conexion = new conexion;
            $altaBajaPadron_model = new altaBajaPadron_model;
            
            // $results['Error de Insercion'] = [];
            // $results['Error_de_Datos'] = [];
            // $results['Error_de_CPersona_NoExist'] = [];
            // $results['Error_de_CPersona_Existe'] = [];
            // $results['Error_de_Accion'] = [];

            $fila = 0;

            $datos = fgetcsv($file, ",");
            $datos = fgetcsv($file, ",");
            $values = [];
            $values['accion'] = $datos[0];
            $values['programa'] = $datos[2];
            $values['finalizadas'] = 0;
            $values['errores'] = 0;        
            fclose($file);
            $file = fopen("$fichero_tmp", "r");

            //Escribir en el log
            session_start();
            $ruta = '../../../../reportes/LOG';
            $nombreReporte = 'LOG_'.$values['accion'].'_'.$values['programa'].'_'.date("Ymdhis").'.csv';
            $carpetaReporte = '../../../../reportes/'.$nombreReporte;
            $idUsuarioLogin = $_SESSION['IDUSUARIO'];
            $opcion='acciones';            

            $fp = fopen("$carpetaReporte", 'w');
            fwrite($fp, 'Error, Fila'."\r\n");

            while (($datos = fgetcsv($file, ",")) == true) 
            {
                
                if( strnatcasecmp($datos[0], 'bajapadron') == 0 && !empty($datos[1]) && !empty($datos[2]) ){
                    $resultGetPadron = $altaBajaPadron_model->getPadronPrograma(preg_replace("/\s/", "" ,$datos[1]) , preg_replace("/\s/", "", $datos[2]));
                    if(!empty($resultGetPadron) && !empty($resultGetPadron['IDPADRON'])){
                        $queryResult = $altaBajaPadron_model->deletePadron( preg_replace("/\s/", "" ,$datos[1]) , preg_replace("/\s/", "", $datos[2]) );
                        if($queryResult == false){
                            fwrite($fp, 'Error de Datos, '.$fila."\r\n");
                            // array_push($results['Error_de_Datos'], array('fila'=>$fila));
                        }
                    }
                    else{
                        fwrite($fp, 'Error de Datos, '.$fila."\r\n");
                        // array_push($results['Error_de_Datos'], array('fila'=>$fila));
                    }
                }
                else if(strnatcasecmp($datos[0], 'altapadron') == 0)
                {
                    $queryResult = $altaBajaPadron_model->searchProgTipoEvt($datos[2], $datos[3]);
                    if(empty($queryResult)){
                        // array_push($results['Error_de_Datos'], array('fila'=>$fila));
                        fwrite($fp, 'Error de Datos, '.$fila."\r\n");
                    }else{
                        //Consultar si el C_PERSONA existe en la tabla listado
                        $queryResultCpersona = $altaBajaPadron_model->searchCPersona($datos[1]);
                        if(empty($queryResultCpersona) && empty($queryResultCpersona['IDLISTADO']) ){
                            // array_push($results['Error_de_CPersona_NoExist'], array('fila'=>$fila, 'C_PERSONA'=>$datos[1]));
                            fwrite($fp, 'Error de C_PERSONA no existe, '.$fila."\r\n");
                        }else{
                            $queryPersonaExiste = $altaBajaPadron_model->searchCPersonaPrograma($datos[1], $datos[2]);
                            
                            if(empty($queryPersonaExiste) && empty($queryPersonaExiste['IDLISTADO']) ){
                                $resultAddPadron = $altaBajaPadron_model->addNewPadron($datos[2], $queryResultCpersona['IDLISTADO'], $datos[3]);
                                if($resultAddPadron == true)$values['finalizadas'] += 1;
                            }
                            elseif(!empty($queryPersonaExiste['IDLISTADO']) ){
                                if(!empty($queryPersonaExiste['ESTATUS']) &&  $queryPersonaExiste['ESTATUS'] == 1){
                                    // array_push($results['Error_de_CPersona_Existe'], array('fila'=>$fila, 'C_PERSONA'=>$datos[1]));
                                    fwrite($fp, 'Error de C_PERSONA ya existe, '.$fila."\r\n");
                                }elseif($queryPersonaExiste['ESTATUS'] == 0){
                                    $resultAddPadron2 = $altaBajaPadron_model->addNewPadron($datos[2], $queryResultCpersona['IDLISTADO'], $datos[3]);
                                    if($resultAddPadron == true)$values['finalizadas'] += 1;
                                    // echo "Estatus 0 Insertar fila". $fila;
                                }
                            }
                        }
                    }
                }else if($fila > 0){
                    // array_push($results['Error_de_Accion'], array('fila'=>$fila));.
                    fwrite($fp, 'Error de Accion, '.$fila."\r\n");
                }
                $fila++;
            }

            fclose($fp);
            fclose($file);

            // if(count($results['Error_de_Datos']) > 0 || count($results['Error_de_CPersona_NoExist']) > 0 || count($results['Error_de_CPersona_Existe']) > 0 || count($results['Error_de_Accion']>0) ){
            //     $result = $altaBajaPadron_model->commitRollBack(0);
            //     return $results;
            // }                
            // else{
            //     $result = $altaBajaPadron_model->commitRollBack(1);
            //     return $result;
            // }

            $values['errores'] = $fila - $values['finalizadas'] - 1;
            

            $parametros = new parametros;
            $rutaArchivo = $parametros->urlReportes().$nombreReporte;
            $otro = new MailRepoGeneral2;
            $respuestapp = $otro->verificaMail($idUsuarioLogin, $rutaArchivo, $opcion,$values);
            return true;

        }
    }

    $altaBajaPadron = new altaBajaPadron_controller;
    $option = $_POST['option'];

    // if($option == 'inicioBusquedaPadron')
    //     echo json_encode( $altaBajaPadron->initSearchPadron());
    if($option == 'getPrograms')
        echo json_encode( $altaBajaPadron->searchPrograms());
    else if($option == 'getPadron')
        echo json_encode( $altaBajaPadron->getInfoPadron($_POST));
    else if($option == 'downloadPadron')
        echo json_encode( $altaBajaPadron->downloadInfoPadron($_POST));
    else if($option == 'getProgTypeEvt')
        echo json_encode( $altaBajaPadron->searchProgramTypeEvt($_POST['cvePrograma']));
    else if($option == 'updatePadron')
        echo json_encode( $altaBajaPadron->updateRowsPadron());
?>