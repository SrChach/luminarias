<?php
include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/adminTipoEventos_model.php';
    class adminTipoEventos_controller{

        function typeEvents(){
            $adminTipoEventos_model = new adminTipoEventos_model;
            $results = $adminTipoEventos_model->getTypeEvents();
            return $results;
        }
        
        function phases(){
            $adminTipoEventos_model = new adminTipoEventos_model;
            $results = $adminTipoEventos_model->getPhases();
            return $results;
        }

        function newTypeEvent($typeEvt){
            if($typeEvt === false || $typeEvt == '' || $typeEvt == null) return false;
            $adminTipoEventos_model = new adminTipoEventos_model;
            $result = $adminTipoEventos_model->addTypeEvent($typeEvt);
            return $result;
        }
       
        function newPhase($typePhase){
            if($typePhase === false || $typePhase == '' || $typePhase == null) return false;
            $adminTipoEventos_model = new adminTipoEventos_model;
            $result = $adminTipoEventos_model->addPhase($typePhase);
            return $result;
        }

        function programs(){
            $adminTipoEventos_model = new adminTipoEventos_model;
            $results = $adminTipoEventos_model->getPrograms();
            return $results;
        }

        function programsTypeEvent(){
            $adminTipoEventos_model = new admintipoEventos_model;
            $results = $adminTipoEventos_model->getProgramsTypeEvents();
            if(!empty($results))$results = $this->groupItemsForField($results, 'IDPROGRAMA');
            return $results;
        }

        function saveProgramTypeEvent($objectData){ 
            if(empty($objectData))return false;
            $adminTipoEventos_model = new adminTipoEventos_model;
            $result = $adminTipoEventos_model->addAllProgramTypeEvent($objectData['claveProgram'], $objectData['listItemsSave']);
            return $result;
        }
        
        function savePhaseProgTypeEvent($objectData){ 
            if(empty($objectData))return false;
            
            $adminTipoEventos_model = new adminTipoEventos_model;
            $result = $adminTipoEventos_model->addAllPhaseProgTypeEvent($objectData['claveProgramTypeEvt'], $objectData['listItemsSave']);
            return $result;
        }

        function phaseProgTypeEvent(){
            $adminTipoEventos_model = new admintipoEventos_model;
            $results = $adminTipoEventos_model->getPhaseProgTypeEvent();
            if(!empty($results))$results = $this->groupItemsForField($results, 'IDPROGRAMATIPOEVENTO');
            return $results;
        }
        
        
        function programTypeEventById($idProgram){            
            if($idProgram === false || $idProgram == '' || $idProgram == null) return false;
            $adminTipoEventos_model = new adminTipoEventos_model;
            $results = $adminTipoEventos_model->findProgramTypeEvent($idProgram);
            return $results;
        }
        
        function phaseProgTypeEventById($idPhase){            
            if($idPhase === false || $idPhase == '' || $idPhase == null) return false;
            $adminTipoEventos_model = new adminTipoEventos_model;
            $results = $adminTipoEventos_model->findPhaseProgTypeEvent($idPhase);
            return $results;
        }

        function groupItemsForField($objectData, $itemGroup){            
            $resultsObj;
            foreach($objectData as $item){
                if(!empty($resultsObj[$item[$itemGroup]])){
                    array_push($resultsObj[$item[$itemGroup]], $item);
                }
                else $resultsObj[$item[$itemGroup]] = array(0=>$item);
            }
            return $resultsObj;
        }

        function labelsProgram(){
            $adminTipoEventos_model = new adminTipoEventos_model;
            $results = $adminTipoEventos_model->getLabelsProgram();
            if(!empty($results))$results = $this->groupItemsForField($results, 'GRUPO');
            return $results;
        }

        function labelsPhaseProgram(){
            $adminTipoEventos_model = new adminTipoEventos_model;
            $results = $adminTipoEventos_model->getLabelsPhaseProgram();
            if(!empty($results))$results = $this->groupItemsForField($results, 'IDFASEPROGRAMAEVENTO');
            return $results;
        }

        function saveLabelsPhaseProgram($objectData){
            if(empty($objectData)) return false;

            $adminTipoEventos_model = new adminTipoEventos_model;
            $result = $adminTipoEventos_model->addAllLabelsPhaseProg($objectData['clavePhaseProgEvt'], $objectData['listItems']);
            return $result;
        }


    }


    $adminTipoEventos = new adminTipoEventos_controller;
    $option = $_POST['option'];

    if($option == 'typeEvents'){
        echo json_encode($adminTipoEventos->typeEvents());
    }
    else if($option == 'phases'){
        echo json_encode($adminTipoEventos->phases());
    }
    else if($option == 'newEvent'){
        echo json_encode($adminTipoEventos->newTypeEvent($_POST['typeEvent']));
    }
    else if($option == 'newPhase'){
        echo json_encode($adminTipoEventos->newPhase($_POST['phase']));
    }
    else if($option == 'programs'){
        echo json_encode($adminTipoEventos->programs());
    }
    else if($option == 'programTypeEvent'){
        echo json_encode($adminTipoEventos->programsTypeEvent());
    }
    else if($option == 'saveProgramTypeEvt'){
        echo json_encode($adminTipoEventos->saveProgramTypeEvent($_POST));
    }
    else if($option == 'savePhaseProgTpEvt'){
        echo json_encode($adminTipoEventos->savePhaseProgTypeEvent($_POST));
    }
    else if($option == 'phaseProgTypeEvt'){
        echo json_encode($adminTipoEventos->phaseProgTypeEvent());
    }
    else if($option == 'programTypeEventUnique'){
        echo json_encode($adminTipoEventos->programTypeEventById($_POST['findProgramTypeEvt']));
    }
    else if($option == 'phaseProgEventUnique'){
        echo json_encode($adminTipoEventos->phaseProgTypeEventById($_POST['findProgTypEvt']));
    }
    else if($option == 'labelsProgram'){
        echo json_encode($adminTipoEventos->labelsProgram());
    }
    else if($option == 'labelPhaseProgram'){
        echo json_encode($adminTipoEventos->labelsPhaseProgram());
    }
    else if($option == 'newLabelsPhaseProg'){
        echo json_encode($adminTipoEventos->saveLabelsPhaseProgram($_POST));
    }
    
?>