<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
/**
* 
*/
include_once '../../../pillar/clases/controlador.php';
include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/utilities.php';

include_once '../../../pillar/clases/cedula.php';

include_once 'ErrorController.php';
class CedulaController extends controlador
{
	protected $model;
	protected $result;

	public function getCedula($c_persona)
	{
		$this -> model = new cedula;
		$generales = $this -> model -> generales($c_persona);
		$traeEventos = $this -> model -> traeEventos( $generales['IDLISTADO'] );

		$regresa['generales'] = $generales;
		$regresa['traeEventos'] = $traeEventos;

		return $this -> result = array('CODIGO' => true , 'DATOS' => $regresa);
	}
}
$controller = new CedulaController;
$ErrorController= new ErrorController;

if ($_POST) {
	if (isset($_POST['csrf_token'])) {
		if (isset($_POST['opcion']) and $_POST['opcion']=='Cedula') {
			$c_persona= (isset($_POST['cve']) and $_POST['cve']== $_POST['cve']) ? $_POST['cve']:'';
			echo json_encode($controller -> $_POST['action']($c_persona));
		}
		else{
			echo json_encode($ErrorController->ErrorOption());
		}
	}else{
		echo json_encode($ErrorController->ErrorToken());
	}
}else{
	echo json_encode($ErrorController->ErrorMethod());
}
?>