<?php
//header('Content-type: application/json');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/panelEjecutivo_model.php';


class panelEjecutivo_controller extends controlador{	

	//-----------------------------------------------------
	function generales_cuis($del,$subdel,$mun,$loc){
		$panelEjecutivo_model = new panelEjecutivo_model;

		if($del != ""){
			return $panelEjecutivo_model->cuis_subdelegacion($del);
		}else if($subdel != ""){
			return $panelEjecutivo_model->cuis_municipio($subdel);
		}elseif($mun != ""){
			return $panelEjecutivo_model->cuis_localidad($mun);
		}else{
			return $panelEjecutivo_model->cuis_delegacion();
		}
	}	
	//-----------------------------------------------------
}

$controlador_pruebas = new panelEjecutivo_controller;
$controlador_pruebas->VerificaSolicitud();
$error = $controlador_pruebas->error;

if($error != null){
	echo $error;
	return false;
}

$opcion = $controlador_pruebas->opcion;

//------------------------------------------------------------------
if($opcion == "generales_cuis"){
	$del = (isset($_POST['delegacion']))?$_POST['delegacion']:"";
	$subdel = (isset($_POST['subdelegacion']))?$_POST['subdelegacion']:"";
	$mun = (isset($_POST['municipio']))?$_POST['municipio']:"";
	$loc = (isset($_POST['localidad']))?$_POST['localidad']:"";

	echo json_encode( $controlador_pruebas->generales_cuis($del,$subdel,$mun,$loc) );

}
?>