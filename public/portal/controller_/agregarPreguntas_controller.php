<?php

include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/agregarPreguntas_model.php';

    class agregarPreguntas_controller{
        function getCuestions(){
            $agregarPreguntas_model = new agregarPreguntas_model;            
            $results = $agregarPreguntas_model->getAllCuestions();
            return $results;
        }

        function newCuestion($objectData){
            if(empty($objectData))return false;
                        
            $agregarPreguntas_model = new agregarPreguntas_model;
            $result = $agregarPreguntas_model->addNewCuestion($objectData);
            return $result;
        }

        function updateCuestion($objectData){
            if(empty($objectData))return false;

            $agregarPreguntas_model = new agregarPreguntas_model;
            $result = $agregarPreguntas_model->updateCuestionSimple($objectData);
            return $result;
        }
        
        function updateCuestCombo($objectData){
            if(empty($objectData))return false;

            $agregarPreguntas_model = new agregarPreguntas_model;
            $result = $agregarPreguntas_model->updateCuestionCombo($objectData);
            return $result;
        }
        
        function updateCuestComboPers($objectData){            
            if(empty($objectData))return false;

            $agregarPreguntas_model = new agregarPreguntas_model;
            $result = $agregarPreguntas_model->updateCuestionComboPersonalized($objectData);
            return $result;
        }
    }
    
    $agregarPreguntas_controller = new agregarPreguntas_controller;
    $option = $_POST['option'];

    if($option == "Cuestions"){        
        echo json_encode($agregarPreguntas_controller->getCuestions());
    }
    else if($option == "addCuestion"){        
        echo json_encode($agregarPreguntas_controller->newCuestion($_POST));
    }
    else if($option == "updateCuestion"){        
        echo json_encode($agregarPreguntas_controller->updateCuestion($_POST));
    }
    else if($option == "updateCuestionCombo"){        
        echo json_encode($agregarPreguntas_controller->updateCuestCombo($_POST));
    }
    else if($option == "updateCuestionComboPers"){        
        echo json_encode($agregarPreguntas_controller->updateCuestComboPers($_POST));
    }

?>