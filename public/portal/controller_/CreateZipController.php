<?php
ini_set('memory_limit', -1);
ignore_user_abort(1); 
ini_set('max_execution_time',0);
 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 

include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/controlador.php';
include_once 'ErrorController.php';
include_once '../../../pillar/clases/utilities.php';
include_once '../../../pillar/clases/mailDemo.php';
//include_once '../../../pillar/clases/BodyMail.php';
include_once '../libraries/phpmailer-master/class.phpmailer.php';
/**
 * 
 */
include_once '../../../pillar/clases/zip.php';

class CreateZipController extends controlador
{
	protected $result;
	protected $model;
	protected $path;
	function __construct()
	{
		$this -> result=null;
		$this -> model=new Zip;
		$this -> path = '../../../../Media/imagenes_impresion_credenciales/';
	}
	public function Zip($lote='',$user='')
	{
		return $this -> model -> makeZip($lote,$user);
	}
	public function fileExist($lote='')
	{
		$ok=false;
		if (file_exists($this -> path.'lote_'.$lote.'.zip') ) {
			$ok=true;
		}
		return $ok;
	}
}


$ErrorController= new ErrorController;
$controller= new CreateZipController;
$controller->VerificaSolicitud();
if ($_POST) {
	if (isset($_POST['csrf_token'])) {
		if (isset($controller->opcion) and $controller->opcion=='makeZip') {
			$lote= (isset($_POST['lote'])) ? $_POST['lote'] : '';
			$user= (isset($_POST['user'])) ? $_POST['user'] : '';
			echo json_encode($controller -> Zip($lote,$user));
		}elseif (isset($_POST['opcion']) and $_POST['opcion']=='fileExist') {
			$lote= (isset($_POST['lote'])) ? $_POST['lote'] : '';
			echo json_encode($controller -> fileExist($lote));
		}
		else{
			echo json_encode($ErrorController->ErrorMessages('Option'));
		}
	}else{
		echo json_encode($ErrorController->ErrorMessages('Token'));
	}
}else{
	echo json_encode($ErrorController->ErrorMessages('Method'));
}

?>