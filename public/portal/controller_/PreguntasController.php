<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');


include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/controlador.php';
include_once 'ErrorController.php';
include_once '../../../pillar/clases/utilities.php';

include_once '../../../pillar/clases/preguntas.php';
/**
* 
*/
class PreguntasController extends controlador
{
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Preguntas;
	}
	public function Preguntas($function,$idImg='')
	{
		return $this -> model -> $function($idImg);
	}
}

$ErrorController= new ErrorController;
$controller= new PreguntasController;
$controller->VerificaSolicitud();

if ($_POST) {
	if (isset($_POST['csrf_token'])) {
		if (isset($controller->opcion) and $controller->opcion == 'Preguntas') {
			$idImg = isset($_POST['idImg']) ? $_POST['idImg']:'';
			echo json_encode($controller -> Preguntas ($_POST['action'],$idImg));
		}else{
			echo json_encode($ErrorController->ErrorMessages('Option'));	
		}
	}else{
		echo json_encode($ErrorController->ErrorMessages('Token'));
	}
	
}else{
	echo json_encode($ErrorController->ErrorMessages('Method'));
}
?>