<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
/**
* CREATED BY: 	JOSÉ JUAN DEL PRADO LUGO.
*/


include_once 'ErrorController.php';
include_once '../../../pillar/clases/utilities.php';
class FilesController
{
	protected $destination_route;
	protected $Storage_route;
	protected $route;
	protected $routeEvidencia;
	protected $utilities;
	function __construct()
	{	
		$this -> route = '../Storage/Evidencias/';
		$this -> routeEvidencia = '../../../../Media/imagenes/';
		$this -> Storage_route = ['imgBeneficiarios/','imgProfiles/','imgPrograms/','imgSites/',];
		$this -> destination_route = null;
		$this -> utilities = new utilities;
	}

	function insert($file,$org,$rename='',$tipo='')
	{	$ok=false;
		$folder="";
		if ($org=='programas') {
			if ($tipo!='' and $tipo==1) {
				$folder="alargada/";
			}
			if ($tipo!='' and $tipo==2) {
				$folder="cuadrada/";
			}
			if ($tipo!='' and $tipo==3) {
				$folder="vertical/";
			}
			$this -> destination_route = $this -> route . $this -> Storage_route[2] . $folder ;
			
			
		}

		if ($org=='users') {
			$this -> destination_route = $this -> route . $this -> Storage_route[1] ;
		}

		if ($org=='evidenciaDocumento') {
			$this -> destination_route = $this -> routeEvidencia ;
		}

		if ($rename=='') {
			$name = $file['name'];
		}else{
			$name = $rename;
		}
		if (move_uploaded_file($file['tmp_name'], $this -> destination_route . $this -> utilities -> trim($name) )) {
			$ok=true;
		}
		return $ok;
	}


	function delete($file,$org)
	{
		$files= explode(",", $file);
		if ($org=='programas') {
			$this -> destination_route = $this -> route . $this -> Storage_route[2] ;
		}
		for ($i=0; $i < count($files); $i++) {
			if ($i==2) {
				$folder="alargada/";
			}
			if ($i==1) {
				$folder="cuadrada/";
			}
			if ($i==0) {
				$folder="vertical/";
			}
			$archive = $this -> destination_route . $folder .$files[$i];
			if (file_exists($archive)) {
				unlink($archive);
			}
		}
		return count($files)." files delete";
	}
}

$ErrorController= new ErrorController;
$controller = new FilesController;
if ($_POST) {
	if (isset($_POST['csrf_token'])) {

		if (isset($_POST['index'])) {
			$index = $_POST['index'];
			if (isset($_FILES[$index])) {
				$archivo = $_FILES[$index];
				$tipo = (isset($_POST['tipo'])) ? $_POST['tipo'] : '';
				
				echo json_encode( $controller -> $_POST['action']($archivo,$_POST['org'],$_POST['rename'],$tipo) );
			}else{
				echo json_encode($ErrorController->ErrorMessages('IMG'));		
			}
		}else {
			$file= $values= (isset($_POST['file']) and $_POST['file']== $_POST['file']) ? $_POST['file']:'';
			echo json_encode( $controller -> $_POST['action']($file,$_POST['org']) );
		}
		
	}else{
		echo json_encode($ErrorController->ErrorMessages('Token'));	
	}
}else{
	echo json_encode($ErrorController->ErrorMessages('Method'));
}

?>