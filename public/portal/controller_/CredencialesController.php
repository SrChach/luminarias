<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/controlador.php';
include_once 'ErrorController.php';
include_once '../../../pillar/clases/utilities.php';

include_once '../../../pillar/clases/credenciales2.php';

/**
* 
*/
class CredencialesController extends controlador
{
	protected $result;
	protected $model;

	public function Credenciales($function,$idDel,$idSub,$idMun,$idLoc,$fechaInicio,$fechaTermino)
	{	
		$this -> model = new Credenciales;
		return $this -> model -> $function($idDel,$idSub,$idMun,$idLoc,$fechaInicio,$fechaTermino);
	}
}

$controller= new CredencialesController;
$ErrorController= new ErrorController;
if ($_POST) {
	if (isset($_POST['csrf_token'])) {
		if (isset($_POST['opcion']) and $_POST['opcion']=='Credenciales') {
			//$idP= (isset($_POST['idP']) and $_POST['idP']== $_POST['idP']) ? $_POST['idP']:'';
			$idDel= (isset($_POST['idDel']) and $_POST['idDel']== $_POST['idDel']) ? $_POST['idDel']:'';
			$idSub= (isset($_POST['idSub']) and $_POST['idSub']== $_POST['idSub']) ? $_POST['idSub']:'';
			$idMun= (isset($_POST['idMun']) and $_POST['idMun']== $_POST['idMun']) ? $_POST['idMun']:'';
			$idLoc= (isset($_POST['idLoc']) and $_POST['idLoc']== $_POST['idLoc']) ? $_POST['idLoc']:'';
			$fechaInicio= (isset($_POST['fechaInicio']) and $_POST['fechaInicio']== $_POST['fechaInicio']) ? $_POST['fechaInicio']:'';
			$fechaTermino= (isset($_POST['fechaTermino']) and $_POST['fechaTermino']== $_POST['fechaTermino']) ? $_POST['fechaTermino']:'';
			echo json_encode(
							$controller->Credenciales(
												$_POST['action'],
												$idDel,$idSub,
												$idMun,$idLoc,
												$fechaInicio,$fechaTermino
											)
							);
		}else{
			echo json_encode($ErrorController->ErrorOption());	
		}
	}else{
		echo json_encode($ErrorController->ErrorToken());
	}
	
}else{
	echo json_encode($ErrorController->ErrorMethod());
}
?>