<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/controlador.php';
include_once 'ErrorController.php';
include_once '../../../pillar/clases/utilities.php';
/**
 * 
 */
include_once '../../../pillar/clases/estadisticos.php';
class EstadisitcosController extends controlador
{
	
	protected $result;
	protected $model;
	function __construct()
	{
		$this-> result = null;
		$this-> model = new Estadicticos;
	}
	public function Estadicticos($function,$id_del,$id_sub,$id_mun)
	{
		return $this -> model -> $function($id_del,$id_sub,$id_mun);
		
	}
}

$ErrorController= new ErrorController;
$controller= new EstadisitcosController;
$controller->VerificaSolicitud();
if ($_POST) {
	if (isset($_POST['csrf_token'])) {
		if (isset($controller->opcion) and $controller->opcion == 'Estadisticos') {
			$id_del=isset($_POST['id_del']) ? $_POST['id_del']:'';
			$id_sub=isset($_POST['id_sub']) ? $_POST['id_sub']:'';
			$id_mun=isset($_POST['id_mun']) ? $_POST['id_mun']:'';
			echo json_encode($controller -> Estadicticos ($_POST['action'],$id_del,$id_sub,$id_mun));
		}else{
			echo json_encode($ErrorController->ErrorMessages('Option'));	
		}
	}else{
		echo json_encode($ErrorController->ErrorMessages('Token'));
	}
	
}else{
	echo json_encode($ErrorController->ErrorMessages('Method'));
}
?>