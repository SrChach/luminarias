<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include_once 'ErrorController.php';
include_once '../../../../parametros.php';

/**
 * 
 */
class HashController
{//8F252459B054BD8105880F834DA9FE8F
	protected $model;
	protected $checkHash;
	function __construct()
	{
		$this -> model = new parametros;
		$this -> checkHash = false;
	}
	public function comparaHash($hash='')
	{
		$getHash = $this -> model -> getHash();
		$getActHash = $this -> model -> getActHash();

		if ($getHash == $getActHash) {
			$this -> checkHash = true;
		}

		return $this -> checkHash;
	}
	public function setHash($hash='')
	{	
		$this -> model -> setActHash($hash);
		return true;
	}
}

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion'])) {
		$controller = new HashController;
		$hash = (isset($_POST['hash'])) ? $_POST['hash'] : '';
		$option = $_POST['opcion'];
		echo json_encode( $controller -> $option($hash));
	}else{
		echo json_encode($ErrorController->ErrorMessages('Option'));
	}
}else{
	echo json_encode($ErrorController->ErrorMessages('Token'));
}

?>