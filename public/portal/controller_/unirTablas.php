
<?php
/* ------- Comentario ------ */
/* ------------------------- */
// error_reporting(-1);
// ini_set('display_errors', 'On');

include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/unirTablas.php';

class unirTablas{

    // public function getList($tableName, $objectData){
    //     // if(empty($objectData) || $tableName) return 'Manda algo';
    //     $unirTablas_model = new unirTablas_model;

    //     $tableResults = $unirTablas_model->getInfoTable($tableName);
        
    //     // $listValues_1 = $unirTablas_model->getInfoList($fields1, $tableList1, $fieldName1, $fieldValue1);
    //     // $listValues_2 = $unirTablas_model->getInfoList($fields2, $tableList2, $fieldName2, $fieldValue2);
        
        
    //     $results['tableResults'] = $tableResults;
    //     return $results;
    // }

    public function getTable($tableName){
        if(empty($tableName)) return 'Error Campo Vacio';

        $unirTablas_model = new unirTablas_model;
        $results = $unirTablas_model->getInfoTable($tableName);
        
        if(empty($results)) return false;
        return $results;
    }

    public function getTableFilter($objectData){
        if(empty($objectData)) return 'No se han recibido valores';

        if( ($objectData['firstOp'] === '0' || !empty($objectData['firstOp']) ) && !empty($objectData['firstField']) ){
            $complement = $objectData['firstField'] . " = " . $objectData['firstOp'];
        }
        
        if( ($objectData['secondOp'] === '0' || !empty($objectData['secondOp'])) && !empty($objectData['secondField']) ){
            if(!empty($complement)) $complement .= ' and ';
            $complement .= $objectData['secondField'] . " = " . $objectData['secondOp'];
        }

        if(!empty($complement)){
            $complement = " WHERE " . $complement;
        }

        $unirTablas_model = new unirTablas_model;
        $results = $unirTablas_model->getInfoTable($objectData['tableName'], $complement);

        return $results;
    }

    public function addNewRowToUnion($objectData){
        if(empty($objectData)) return 'No se han recibido valores';

        if( (!empty($objectData['firstOp']) || $objectData['firstOp'] === '0') && !empty($objectData['firstField']) && (!empty($objectData['secondOp']) || $objectData['secondOp'] === '0' ) && !empty($objectData['secondField']) ){
            $unirTablas_model = new unirTablas_model;
            $resultInsert = $unirTablas_model->addNewFieldToTab($objectData['firstField'], 
                                                            $objectData['firstOp'], 
                                                            $objectData['secondField'], 
                                                            $objectData['secondOp'], 
                                                            $objectData['tableName']);
            if($resultInsert == true){
                $complementSearch = " WHERE " . $objectData['firstField'] . " = " . $objectData['firstOp'] . ' and ' . $objectData['secondField'] . " = " . $objectData['secondOp'];
                $results = $unirTablas_model->getInfoTable($objectData['tableName'], $complementSearch);
            }
            else{
                return false;
            }
        }else{
            return false;
        }
        
        return $results;
    }

    public function updateStateRow($objectData, $tabName, $newState){
        if(empty($objectData) || empty($tabName)) 
            return false;
        
        $fieldsSearch = [];
        foreach($objectData as $item){
            if(!empty($item['name'] && ($item['fieldValue'] === '0' || !empty($item['fieldValue']) ) )){
                array_push($fieldsSearch, $item['name'] . ' = ' . $item['fieldValue'] );
            }else{
                return false;
            }
        };
        $complement = join(' AND ', $fieldsSearch);
        // $complement = ' WHERE ' . $complement;
        
        $unirTablas_model = new unirTablas_model;
        $result = $unirTablas_model->updateTableState($complement, $tabName, $newState);
        return $result;
    }
}


$opcion = $_POST['opcion'];
$unirTablas = new unirTablas;

switch($opcion){
    case 'getTableInfo' : 
        echo json_encode($unirTablas->getTable($_POST['tableName']));
        break;
    case 'getTableFilter' : 
        echo json_encode($unirTablas->getTableFilter($_POST['fieldOptions']));
        break;
    case 'addFieldToTab' : 
        echo json_encode($unirTablas->addNewRowToUnion($_POST['fieldOptions']));
        break;
    case 'upState' : 
        echo json_encode($unirTablas->updateStateRow($_POST['fieldsUpdate'], $_POST['tabName'], 1 ));
        break;
    case 'downState' : 
        echo json_encode($unirTablas->updateStateRow($_POST['fieldsUpdate'], $_POST['tabName'], 2 ));
        break;
    default : 
        echo 'Error 404';
}

