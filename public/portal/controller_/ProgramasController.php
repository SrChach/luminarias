<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
/*
*/
include_once '../../../pillar/clases/controlador.php';
include_once '../../../pillar/clases/conexion.php';
include_once '../../../pillar/clases/utilities.php';

include_once '../../../pillar/clases/programas.php';

include_once 'ErrorController.php';


class ProgramasController extends controlador
{
	protected $result;
	protected $model;
	public function Error($Error)
	{
		if ($Error==0) {
			$this-> Errors = array('CODIGO' => $Error,
									'DATOS' => 'FALLO DE AUTENTICACIÓN'
									);
		}
		return $this-> Errors;
	}

	public function Programas($function,$programas='',$id='',$values='',$fase = '',$modalidad='')
	{
		if ($function=='generalesPrograma') {
			return $this -> $function($programas,$id,$values,$fase);
		}else{
			$this -> model = new programas;
			return $this -> model -> $function($programas,$id,$values,$modalidad);
		}

	}
	public function ProgramasM2($function,$idPrograma='',$evento='',$fase='',$del='',$subdel='',$mun='')
	{
		$this -> model = new programas;
		return $this -> model -> $function($idPrograma,$evento,$fase,$del,$subdel,$mun);
	}

	public function generalesPrograma($listaProgramas='',$id='',$values='',$fase = ''){

		$programas = new programas;
		$utilities = new utilities;

		$generales = $programas->traeProgramas($id);
		$tiposEventoPrograma = $programas->tiposEventoPrograma($id);
		$metasPrograma = $programas->metasPrograma($id);
		$avances = $programas->avances_programa_acumulativo($id,$fase);
		//$avances = "";


		$regresa['generales'] =  $generales;
		$regresa['metas'] = $metasPrograma;
		$regresa['tiposEventoPrograma'] = $tiposEventoPrograma;
		$regresa['avances'] = $utilities->objetivisa ('IDTIPOEVENTO',$avances);
		$data1=[];
		if ($generales[0]['MODALIDAD']!=1) {
			for ($i=0; $i < count($tiposEventoPrograma[$id]); $i++) {
				array_push($data1, array(
										'EVENTO'=> $tiposEventoPrograma[$id][$i]['TIPOEVENTO'],
										'META' => intval($metasPrograma[$tiposEventoPrograma[$id][$i]['IDTIPOEVENTO']][0]['META']),
										'AVANCE'=>200
									)
						);
			}
		}


		$regresa['dataTable'] =$data1;
		return $this -> result = array('CODIGO' => true,'DATOS' => $regresa);
	}
}

$controller = new ProgramasController;

if (isset($_POST['csrf_token'])) {
	if (isset($_POST['opcion']) and $_POST['opcion']=='Programas') {
		$id= (isset($_POST['id']) and $_POST['id']== $_POST['id']) ? $_POST['id']:'';
		$programas= (isset($_POST['programas']) and $_POST['programas']== $_POST['programas']) ? $_POST['programas']:'';
		$values='';
		if ($_POST['action']=='addPrograma') {
			$values=$_POST['values'];
		}
		$evento=(isset($_POST['evento'])) ? $_POST['evento'] : '';
		$modalidad=(isset($_POST['modalidad'])) ? $_POST['modalidad'] : '';
		echo json_encode($controller -> Programas($_POST['action'],$programas,$id,$values,$evento,$modalidad));
	}elseif (isset($_POST['opcion']) and $_POST['opcion']=='ProgramasModalidad2') {
		$idPrograma = ( isset($_POST['idprograma']) ) ? $_POST['idprograma']:'';
		$evento = ( isset($_POST['evento']) ) ? $_POST['evento']:'';
		$fase = ( isset($_POST['fase']) ) ? $_POST['fase']:'';
		$del = ( isset($_POST['del']) ) ? $_POST['del']:'';
		$subdel = ( isset($_POST['subdel']) ) ? $_POST['subdel']:'';
		$mun = ( isset($_POST['mun']) ) ? $_POST['mun']:'';
		echo json_encode($controller -> ProgramasM2($_POST['action'],$idPrograma,$evento,$fase,$del,$subdel,$mun));
	}
	else{
		echo json_encode($controller -> Error(0));
	}
}else{
	echo json_encode($controller -> Error(0));
}

?>
