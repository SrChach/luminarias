<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    error_reporting(-1);
    ini_set('display_errors', 'On');

    include '../../../pillar/clases/conexion.php';
    include '../../../pillar/clases/combos-291118.php';

    class constructCombos{
        public function getInfoTab($key, $field, $tabName, $searchKey, $searchKeyVal){
            if(empty($key) || empty($field) || empty($tabName) ) return false;

            $complement = '';
            if(!empty($searchKey) && (!empty($searchKeyVal) || $searchKeyVal === '0')){
                $complement = "WHERE $searchKey = $searchKeyVal";
            }

            $constructCombos_model = new constructCombos_model;
            $result = $constructCombos_model->getInfoTable($tabName, $key, $field, $complement);
            return $result;

        }
    }


    $option = $_POST['opcion'];
    $constructCombos = new constructCombos;

    switch($option){
        case 'getInfoTable' : 
            echo json_encode($constructCombos->getInfoTab($_POST['key'], $_POST['field'], $_POST['tabName'], $_POST['searchKey'], $_POST['searchKeyVal'] ));
            break;
        default : 
            echo 'Error 404';
    }

?>