<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On'); 
include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/utilities.php';
include '../../../pillar/clases/programas.php';
include '../../../pillar/clases/padrones.php';
include '../../../pillar/clases/controlador.php';

class intro_controller extends controlador{

	function main($ListProgramas=''){
		$programas = new programas;
		//$padrones = new padrones;
		$traeProgramas = $programas->traeProgramas($ListProgramas);
		
		$regresa['programas'] = $traeProgramas;
		
		return $regresa;
	}

	function traePadrones($idPadron = ""){
		$padrones = new padrones;
		$traePadrones = $padrones->generalesPadrones($idPadron);
		$regresa['padrones'] = $traePadrones;

		return $regresa;
	}

}

$intro_controller = new intro_controller;

$verifica = $intro_controller->VerificaSolicitud();
if($verifica['error'] != ""){
	echo $error;
	return false;
}

$opcion = $intro_controller->opcion;

if($opcion == "main"){
	$programas = (isset($_POST['programas']) and $_POST['programas']== $_POST['programas']) ? $_POST['programas']:'';
	echo json_encode( $intro_controller -> main($programas) );
}else if($opcion == "traePadrones"){

	echo json_encode( $intro_controller -> traePadrones() );
}


?>