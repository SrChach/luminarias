<?php
include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/adminPreguntasDoc_model.php';

    class adminPreguntasDoc_controller{
        
        function getCategories(){
            $adminPreguntasDocModel = new adminPreguntasDoc_model;
            $results = $adminPreguntasDocModel->getAllCategories();
            return $results;
        }

        function getTypeImages(){
            $adminPreguntasDocModel = new adminPreguntasDoc_model;
            $results = $adminPreguntasDocModel->getAllTypeImages();
            if(empty($results))return false;
            
            $resultsObj = $this->groupItemsForField($results, 'IDCATEGORIA');      
            
            return $resultsObj;
        }

        function getTypeDocuments(){
            $adminPreguntasDocModel = new adminPreguntasDoc_model;
            $results = $adminPreguntasDocModel->getAllTypeDocuments();
            if(empty($results))return false;

            $resultsObj = $this->groupItemsForField($results, 'IDTIPOIMAGEN');
            return $resultsObj;
        }

        function getCuestions(){            
            $adminPreguntasDocModel = new adminPreguntasDoc_model;
            $results = $adminPreguntasDocModel->getAllCuestions();
            return $results;
        }



        function adminCuestions($objectData){            
            $adminPreguntasDocModel = new adminPreguntasDoc_model;
            $result = array();

            // print_r($objectData['itemsInsert']);
            if(empty($objectData['itemsInsert']) && empty($objectData['itemsUpdateOrder']) && empty($objectData['itemsUpdateState'])){
                return false;
            }

            if(!empty($objectData['itemsInsert'])){
                $resultInserts = $adminPreguntasDocModel->addListCuestions($objectData['itemsInsert'], $objectData['IDTYPEDOC']);
                $result['inserts'] = $resultInserts;
            }
            if(!empty($objectData['itemsUpdateOrder'])){
                $resultUpdatesOrder = $adminPreguntasDocModel->updateOrderListCuestions($objectData['itemsUpdateOrder']);
                $result['updateOrders'] = $resultUpdatesOrder;
            }
            if(!empty($objectData['itemsUpdateState'])){
                $resultUpdatesState = $adminPreguntasDocModel->updateStatusListCuestions($objectData['itemsUpdateState']);
                $result['updateState'] = $resultUpdatesState;
            }
            
            return $result;
        }

        function getCuestionsTypeDoc(){
            $adminPreguntasDocModel = new adminPreguntasDoc_model;
            $result = $adminPreguntasDocModel->getAllCuestionsTypeDoc();
            if(empty($result)) return false;
            
            $resultsObj = $this->groupItemsForField($result, 'IDTIPODOCUMENTO');            
            return $resultsObj;
        }

        function groupItemsForField($objectData, $itemGroup){
            $resultsObj;
            foreach($objectData as $item){
                if(!empty($resultsObj[$item[$itemGroup]])){
                    array_push($resultsObj[$item[$itemGroup]], $item);
                }
                else $resultsObj[$item[$itemGroup]] = array(0=>$item);
            }
            return $resultsObj;
        }
        
    }

    
    $adminPreguntasDoc = new adminPreguntasDoc_controller;    
    $option = $_POST['option'];

    if($option == 'categories'){
        echo json_encode($adminPreguntasDoc->getCategories());
    }
    else if($option == 'typeImages'){
        echo json_encode($adminPreguntasDoc->getTypeImages());
    }
    else if($option == 'typeDocuments'){
        echo json_encode($adminPreguntasDoc->getTypeDocuments());
    }
    else if($option == 'cuestions'){
        echo json_encode($adminPreguntasDoc->getCuestions());
    }
    else if($option == 'addUpCuestions'){
        echo json_encode($adminPreguntasDoc->adminCuestions($_POST));
    }
    else if($option == 'cuestionsTypeDoc'){
        echo json_encode($adminPreguntasDoc->getCuestionsTypeDoc());
    }

?>