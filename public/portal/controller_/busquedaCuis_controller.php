<?php
//header('Content-type: application/json');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/niveles_info.php';
include '../../../pillar/clases/utilities.php';
include '../../../pillar/clases/busquedaCuis_model.php';
include '../../../pillar/clases/MailGeneral.php';
include '../../../pillar/clases/exportaRepoGeneral.php';
include '../../../pillar/clases/BodyMail.php';
include '../../../../parametros.php';
include_once '../../../public/portal/libraries/phpmailer-master/class.phpmailer.php';

class busquedaCuis_controller extends controlador{
	
	function inicioBusquedaCUIS(){
		$niveles_info = new niveles_info;
		$utilities = new utilities;

		$delegaciones = $niveles_info->delegaciones_cuis();
		$subdelegaciones = $utilities->objetivisa("ID_DEL",$niveles_info->subdelegaciones_cuis());

		$estados = $niveles_info -> estados_cuis();
		$municipios = $utilities->objetivisa("CVE_ENTIDAD_FEDERATIVA",$niveles_info -> municipios_cuis());
		$localidades = $utilities->objetivisa("CVE_MUNICIPIO",$niveles_info -> localidades_cuis());


		$regresa['delegaciones'] = $delegaciones;
		$regresa['subdelegaciones'] = $subdelegaciones;
		$regresa['estados'] = $estados;
		$regresa['municipios'] = $municipios;
		$regresa['localidades'] = $localidades;
		return $regresa;
	}

	function buscarCUIS($del = "",$subdel = "",$edo = "",$mun = "",$loc = "",$folio = "",$nombre = "",$idUsuario,$tipo=""){
		$busquedaCuis = new busquedaCuis_model;
		$csv = new Reports2;
		$regresa ="";
		if ($tipo=='complete') {

			$query = $busquedaCuis->buscaCuis($del,$subdel,$edo,$mun,$loc,$folio,$nombre,$tipo);
			$fecha=strftime( "%Y%m%d%H%M%S", time() );
			$nombreReporte='ReporteCuis_'.$fecha.'.csv';
			$carpetaReporte='../../../../reportes/'.$nombreReporte;
			$opcion='consulta_cuis';
			$respuestaGenera = $csv -> csv($query,$carpetaReporte,$nombreReporte,$idUsuario,$opcion);
			$regresa='OK';
		}else{
			$busca = $busquedaCuis->buscaCuis($del,$subdel,$edo,$mun,$loc,$folio,$nombre);
			$regresa['generales'] = $busca['generales'];
			$regresa['detalle'] = $busca['detalle'];
			
		}
		return $regresa;
	}

}

$controlador_pruebas = new busquedaCuis_controller;
$controlador_pruebas->VerificaSolicitud();
$error = $controlador_pruebas->error;

if($error != null){
	echo $error;
	return false;
}

$opcion = $controlador_pruebas->opcion;

if($opcion == "inicioBusquedaCUIS"){
	echo json_encode( $controlador_pruebas->inicioBusquedaCUIS() ) ;		
}
elseif($opcion == "buscarCUIS"){
	$del = (isset($_POST['delegacion']))?$_POST['delegacion']:"";
	$subdel = (isset($_POST['subdelegacion']))?$_POST['subdelegacion']:"";
	$edo = (isset($_POST['estado']))?$_POST['estado']:"";
	$mun = (isset($_POST['municipio']))?$_POST['municipio']:"";
	$loc = (isset($_POST['localidad']))?$_POST['localidad']:"";
	$folio = (isset($_POST['folio_cuis']))?$_POST['folio_cuis']:"";
	$nombre = (isset($_POST['nombre']))?$_POST['nombre']:"";
	$tipo = (isset($_POST['tipo']))?$_POST['tipo']:"";
	$idUsuario = (isset($_POST['idUsuario']))?$_POST['idUsuario']:"";
	
	// echo $del.' '.$subdel.' '.$edo.' '.$mun.' '.$loc.' '.$folio.' '.$nombre;
	echo json_encode( $controlador_pruebas->buscarCUIS($del,$subdel,$edo,$mun,$loc,$folio,$nombre,$idUsuario,$tipo) ) ;		

}

?>