<?php
include '../../../pillar/clases/integraExpListado_model.php';
include '../../../pillar/clases/utilities.php';
include '../../../pillar/clases/conexion.php';

	class integraExpListado_controller{

		function inicioBusquedaListado(){

			$integraExpListado = new integraExpListado_model;
			$utilities = new utilities;

			$delegaciones = $integraExpListado->listado_delegacion();
			$subdelegaciones = $utilities->objetivisa("ID_DEL",$integraExpListado->listado_subdelegacion());

			$estados = $integraExpListado -> listado_estados();
			$municipios = $utilities->objetivisa("CVE_ENTIDAD_FEDERATIVA",$integraExpListado -> listado_municipio());
			$localidades = $utilities->objetivisa("CVE_MUNICIPIO",$integraExpListado -> listado_localidad());

			$regresa['delegaciones'] = $delegaciones;
			$regresa['subdelegaciones'] = $subdelegaciones;
			$regresa['estados'] = $estados;
			$regresa['municipios'] = $municipios;
			$regresa['localidades'] = $localidades;
			return $regresa;
		}

		function buscarListado($del = "",$subdel = "",$edo = "",$mun = "",$loc = "",$folio = "",$nombre = "", $parentesco){				
			$integraExpListado = new integraExpListado_model;
			$results = $integraExpListado->buscaIntExpListado($del,$subdel,$edo,$mun,$loc,$folio,$nombre, $parentesco);
			return $results;
		}
	}

	$integraExpListado = new integraExpListado_controller;
	$option = $_POST['option'];

	if($option == "inicioIntegraListado"){	
		echo json_encode( $integraExpListado->inicioBusquedaListado() );
	}
	else if($option == 'buscarListado'){		
		$del = (isset($_POST['delegacion']))?$_POST['delegacion']:"";
		$subdel = (isset($_POST['subdelegacion']))?$_POST['subdelegacion']:"";
		$edo = (isset($_POST['estado']))?$_POST['estado']:"";
		$mun = (isset($_POST['municipio']))?$_POST['municipio']:"";
		$loc = (isset($_POST['localidad']))?$_POST['localidad']:"";
		$folio = (isset($_POST['folio_cuis']))?$_POST['folio_cuis']:"";
		$nombre = (isset($_POST['nombre']))?$_POST['nombre']:"";
		$parentesco = (isset($_POST['parentesco']))?$_POST['parentesco']: false;

		echo json_encode( $integraExpListado->buscarListado($del,$subdel,$edo,$mun,$loc,$folio,$nombre, $parentesco) );
	}

?>