<?php
header('Content-type: application/json');
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// error_reporting(-1);
// ini_set('display_errors', 'On');

include '../../../pillar/clases/controlador.php';
include '../../../pillar/clases/conexion.php';
include '../../../pillar/clases/cuis.php';
include '../../../pillar/clases/niveles_info.php';
include '../../../pillar/clases/utilities.php';



class controlador_cuis extends controlador{
    function getDataGenerals(){        
        if(empty($_POST['idCuis'])) return false;

        $idCuis = $_POST['idCuis'];
        $catalogos = $_POST['getCatalogos'];

        $cuis = new cuis;   
        $niveles_info = new niveles_info;
        $utilities = new utilities;

        $datosGeneralesCuis = $cuis->traeInfoCuis($idCuis);
        $lotes = $cuis->lotesFolio($idCuis);        

        if($catalogos || $catalogos == 'true'){
            $estadosCuis = $cuis ->estados();            
            $regresa['cuisEstados'] = $estadosCuis;            
        }           

        $regresa['lotes'] = $lotes;
        $regresa['datosGeneralesCuis'] = $datosGeneralesCuis;
        
        return $regresa;
    }

    function validacionImg($idCuis){
        $cuis = new cuis;
        $validacionImagenes = $cuis->infoValidacionImagenes($idCuis);
        $regresa = $validacionImagenes;
        return $regresa;
    }

    function cambioValidacionImg($objectData){
        if(empty($objectData)) return false;
        
        $cuis = new cuis;        
        $results['validaciones'] = $cuis->actualizaValidacionImg($objectData);

        if($results['validaciones']){
            if($objectData['tipoImpresion'] == 0) 
                $results['impresion'] = true;
            else
                $results['impresion'] = $cuis->addNewLotImpresion($objectData['tipoImpresion'], $objectData['idCuis']);
        }
            
        return $results;
    }

	function cuis(){
        $idCuis = $_POST['idCuis'];
        $cuis = new cuis;
        $utilities = new utilities;	
        $niveles_info = new niveles_info;

		// $datosGeneralesCuis = $cuis->traeInfoCuis($idCuis);
		$infoIntegrantes = $cuis->traeInfoIntegrantes($idCuis);
        // $imagenesProgramas = $cuis->traerImagenesPrograma($idCuis);
        // $estados  = $cuis -> estados();  
        $catalogoParentezco  = $cuis -> parentescoCat(); 

        
        $estadosIntegrantes = [];
        foreach($infoIntegrantes as $item){
            array_push($estadosIntegrantes, $item['C_PERSONA']);
        }        
        $estatusIntegrantes = $cuis->getInfoStateIntegranted($estadosIntegrantes);
        $estatusIntegrantes = $utilities->objetivisa('C_PERSONA', $estatusIntegrantes);
        $regresa['estatusIntegrantes'] = $estatusIntegrantes;        
		// $regresa['datosGeneralesCuis'] = $datosGeneralesCuis;
        $regresa['infoIntegrantes'] = $infoIntegrantes;
		// $regresa['imagenesProgramas'] = $imagenesProgramas;
        // $regresa['estados'] = $estados;
        $regresa['catalogoParentezco'] = $catalogoParentezco;
 		return $regresa;
    }
    
    function getProgramasCuis(){
        $idCuis = $_POST['idCuis'];
        $cuis = new cuis;
		
        // $infoIntegrantes = $cuis->traeInfoIntegranteJefe($idCuis);
        $imagenesProgramas = $cuis->traerImagenesPrograma($idCuis);
        $carencias = $cuis -> carencias($idCuis); 

        $regresa['imagenesProgramas'] = $imagenesProgramas;   
        // $regresa['infoIntegranteJefe'] = $infoIntegrantes;
        $regresa['carencias'] = $carencias;
        
 		return $regresa;
    }
    
    function updatePersona($objectData){          
        if(empty($objectData)) return 'No se recibio información';
        // $conexion_c = new conexion;
        $cuis = new cuis;
        $fieldSearch = (!empty($objectData['field'])) ? $objectData['field'] : "";
        $fieldSearch_Intg = (!empty($objectData['field'])) ? $objectData['field'] : "";
        $folioCedula = (!empty($objectData["folioCedula"])) ? $objectData["folioCedula"] : "";

        // if($fieldSearch == 'nombre') {
        //     $fieldSearch = 'NB_NOMBRE';
        //     $fieldSearch_Intg = 'NOMBRE';
        // }

        // if($fieldSearch == 'apPaterno') {
        //     $fieldSearch = 'NB_PRIMER_AP';
        //     $fieldSearch_Intg = 'APATERNO';
        // }

        // if($fieldSearch == 'apMaterno') {
        //     $fieldSearch = 'NB_SEGUNDO_AP';
        //     $fieldSearch_Intg = 'AMATERNO';
        // }

        // if($fieldSearch == 'parentesco') {
        //     $fieldSearch_Intg = 'C_INTEGRANTE'; 
        // }

        // if($fieldSearch == 'fNacimiento'){
        //     $fieldSearch = 'FCH_NACIMIENTO';
        // }

        // if($fieldSearch == 'curp'){
        //     $fieldSearch = 'NB_CURP';
        // }

        // if($fieldSearch == 'edoNacimiento'){
        //     $fieldSearch = 'C_CD_EDO_NAC';
        // }
        
        switch ($fieldSearch){
            case 'nombre': 
                $fieldSearch = 'NB_NOMBRE';
                $fieldSearch_Intg = 'NOMBRE';
                break;
            case 'apPaterno': 
                $fieldSearch = 'NB_PRIMER_AP';
                $fieldSearch_Intg = 'APATERNO';
                break;
            case 'apMaterno':
                $fieldSearch = 'NB_SEGUNDO_AP';
                $fieldSearch_Intg = 'AMATERNO';
                break;
            case 'parentesco':
                $fieldSearch_Intg = 'C_INTEGRANTE'; 
                break;
            case 'fNacimiento':
                $fieldSearch = 'FCH_NACIMIENTO';
                break;
            case 'curp':
                $fieldSearch = 'NB_CURP';
                break;
            case 'edoNacimiento':
                $fieldSearch = 'C_CD_EDO_NAC';
                break;
        }
        
        // ------------------------------------------------------------------------------------------------------------------------
        $idDocSoporte = $this->getDocSoporteId($objectData['C_Person'],$objectData['folio_cuis']);                
        if(($idDocSoporte == false || $idDocSoporte == 'false') && $idDocSoporte != ''){ 
            return Array('ok' => FALSE, 'msg' => "Ocurrio un error al subir el archivo. ".$nombre_archivo." No pudo guardarse.", 'status' => 'error');
        }
                
        // ------------------------------------------------------------------------------------------------------------------------

        if(!empty($objectData['newValue'])){
            if(!empty($objectData['C_Person'])){
                $C_Persona = $objectData['C_Person'];

                // if($results['SEDEVER'] && $results['INTEGRADOR']){                    
                    if($fieldSearch == 'parentesco') {
                        $query = "SELECT C_CD_PARENTESCO FROM SEDEVER.CUIS_INTEGRANTE WHERE C_PERSONA = " . $objectData["C_Person"];                        
                        $valorAnterior1 = $cuis->valorAnterior($query);
                        
                        if($valorAnterior1['C_CD_PARENTESCO'] == 1)
                            $cuis->newHistorialCambios('Cambio de Jefe de Hogar Baja Como Jefe', $_SESSION['IDUSUARIO'], $objectData['C_Person'], $valorAnterior1['C_CD_PARENTESCO'], $objectData["newValue"], 'SEDEVER.CUIS_INTEGRANTE', 'C_CD_PARENTESCO', 'CUIS', $objectData["folio_cuis"], $idDocSoporte, $folioCedula);
                        else if($objectData["newValue"] == 1)
                            $cuis->newHistorialCambios('Cambio de Jefe de Hogar Alta Como Jefe', $_SESSION['IDUSUARIO'], $objectData['C_Person'], $valorAnterior1['C_CD_PARENTESCO'], $objectData["newValue"], 'SEDEVER.CUIS_INTEGRANTE', 'C_CD_PARENTESCO', 'CUIS', $objectData["folio_cuis"], $idDocSoporte, $folioCedula);
                        else
                            $cuis->newHistorialCambios('Actualización de Parentesco', $_SESSION['IDUSUARIO'], $objectData['C_Person'], $valorAnterior1['C_CD_PARENTESCO'], $objectData["newValue"], 'SEDEVER.CUIS_INTEGRANTE', 'C_CD_PARENTESCO', 'CUIS', $objectData["folio_cuis"], $idDocSoporte, $folioCedula);
                    }
                    else{
                        $query = "SELECT $fieldSearch FROM SEDEVER.CUIS_PERSONA WHERE C_PERSONA = " . $objectData["C_Person"];                                            
                        $valorAnterior2 = $cuis->valorAnterior($query);                        
                        $cuis->newHistorialCambios('Actualización de información', $_SESSION['IDUSUARIO'], $objectData['C_Person'], $valorAnterior2[$fieldSearch], $objectData["newValue"], 'SEDEVER.CUIS_PERSONA', $fieldSearch, 'CUIS', $objectData["folio_cuis"], $idDocSoporte, $folioCedula);
                    }
                    $results['SEDEVER'] = $cuis->updateDataCuis($fieldSearch, $objectData["newValue"], $objectData["C_Person"]);            
                    

                    if($fieldSearch != 'FCH_NACIMIENTO' || $fieldSearch != 'NB_CURP' || $fieldSearch != 'C_CD_EDO_NAC'){
                        $query = "SELECT $fieldSearch_Intg INTEGRADOR.LISTADO WHERE FOLIOCUIS = ".$objectData['idCuis']." AND C_PERSONA = " .$objectData["C_Person"];
                        $valorAnterior3 = $cuis->valorAnterior($query);
                        $cuis->newHistorialCambios('Actualización de información', $_SESSION['IDUSUARIO'], $objectData['C_Person'], $valorAnterior3[$fieldSearch_Intg], $objectData["newValue"], 'INTEGRADOR.LISTADO', $fieldSearch_Intg, 'CUIS', $objectData["folio_cuis"], $idDocSoporte, $folioCedula);
                    }
                    $results['INTEGRADOR'] = $cuis->updateDataCuisIntg($fieldSearch, $fieldSearch_Intg, $objectData["newValue"], $objectData['idCuis'], $objectData["C_Person"]);
                // }
                        
                
                return $results;
                
                /*$query = "SELECT " .$fieldSearch. " FROM SEDEVER.CUIS_ENCUESTA CE
                join SEDEVER.CUIS_VIVIENDA CV ON CE.C_ENCUESTA = CV.C_ENCUESTA
                JOIN SEDEVER.CUIS_DOMICILIO CD ON CD.C_VIVIENDA = CV.C_VIVIENDA
                JOIN CUIS_INTEGRANTE CI ON CV.C_VIVIENDA = CI.C_VIVIENDA
                JOIN CUIS_PERSONA CP ON CI.C_PERSONA = CP.C_PERSONA
                WHERE folio_cuis = ".$objectData['idCuis']." and enviado = 0 and CP.C_PERSONA = '".$C_Persona."' ";  */                
            }
        }        
        
        return 'Error';

    }

    function getDocSoporteId($cPersona, $folioCuis){
        $cuis = new cuis;
        $nombre_archivo = $_FILES['documentoSoporte']['name'];
        $idDocSoporte = '';                             
        if(!empty($nombre_archivo)){            
            $tipo_archivo = $_FILES['documentoSoporte']['type'];
            $tamano_archivo = $_FILES['documentoSoporte']['size'];
            $tmp_archivo = $_FILES['documentoSoporte']['tmp_name'];
            
            $directorioDestino = '../../../soportecambios/' . $nombre_archivo;
            if (!move_uploaded_file($tmp_archivo, $directorioDestino)) {
                return false;
            }            
            $result = $cuis->addDocument($cPersona, $folioCuis, $nombre_archivo);
            if($result == false || $result == 'false') return false; 

            $idDocSoporte = $result['IDIMAGEN'];

        }

        return $idDocSoporte;
    }

    function rotateImage($objectData){
        if(!empty($objectData['fotoImg'])){                                

            $imagick = new Imagick($objectData['pathFotoImg']);
            $imagick->rotateimage(new ImagickPixel(), $objectData['fotoImg']);                                    
        }
        if(!empty($objectData['huellaImg'])){                                

            $imagick = new Imagick($objectData['pathHuellaImg']);
            $imagick->rotateimage(new ImagickPixel(), $objectData['huellaImg']);            
        }
        if(!empty($objectData['firmaImg'])){                                

            $imagick = new Imagick($objectData['pathFirmaImg']);
            $imagick->rotateimage(new ImagickPixel(), $objectData['firmaImg']);
            // header("Content-Type: image/jpg");
            // echo $imagick->getImageBlob();            
        }
        // file_put_contents($objectData['pathFotoImg'], $imagick);
    }

    function updatePerson($claveIntegrante, $folio_cuis, $estatus = 1, $folioCedula = ''){
        if(empty($claveIntegrante)) return false;
        
        // ------------------------------------------------------------------------------------------------------------------------
        $idDocSoporte = $this->getDocSoporteId($claveIntegrante, $folio_cuis);
        if(($idDocSoporte == false || $idDocSoporte == 'false') && $idDocSoporte != '') 
            return Array('ok' => FALSE, 'msg' => "Ocurrio un error al subir el archivo. ".$nombre_archivo." No pudo guardarse.", 'status' => 'error');
                
        // ------------------------------------------------------------------------------------------------------------------------

        $cuis = new cuis;
        $query = "SELECT ESTATUS FROM INTEGRADOR.LISTADO WHERE C_PERSONA = " . $claveIntegrante;        
        $valorAnterior = $cuis->valorAnterior($query);         
        if($estatus == 0)
            $cuis->newHistorialCambios('Baja de Integrante', $_SESSION['IDUSUARIO'], $claveIntegrante, $valorAnterior['ESTATUS'], $estatus, 'INTEGRADOR.LISTADO', 'ESTATUS', 'CUIS', $folio_cuis, $idDocSoporte, $folioCedula);
        elseif($estatus == 1)
            $cuis->newHistorialCambios('Reincorporación de Integrante', $_SESSION['IDUSUARIO'], $claveIntegrante, $valorAnterior['ESTATUS'], $estatus, 'INTEGRADOR.LISTADO', 'ESTATUS', 'CUIS', $folio_cuis, $idDocSoporte, $folioCedula);

        $idIntegrante = $claveIntegrante;
        $cuis = new cuis;
        $result = $cuis->toggleRemoveIntegrated($idIntegrante, $estatus);
        return $result;
    }

    function updateFamily($claveCuis, $folio_cuis, $nuevoEstado, $folioCedula){
        if(empty($claveCuis)) return false;                
        
        // ------------------------------------------------------------------------------------------------------------------------
        $idDocSoporte = $this->getDocSoporteId(0, $folio_cuis);
        if( ($idDocSoporte == false || $idDocSoporte == 'false') && $idDocSoporte != '') 
            return Array('ok' => FALSE, 'msg' => "Ocurrio un error al subir el archivo. ".$nombre_archivo." No pudo guardarse.", 'status' => 'error');
                
        // ------------------------------------------------------------------------------------------------------------------------
        
        $cuis = new cuis;
        $query = "SELECT ENVIADO FROM SEDEVER.CUIS_ENCUESTA WHERE FOLIO_CUIS = '". $claveCuis. "' ";        
        $valorAnterior = $cuis->valorAnterior($query);
        if($nuevoEstado == 2)         
            $cuis->newHistorialCambios('Baja de Familia', $_SESSION['IDUSUARIO'], $claveCuis, $valorAnterior['ENVIADO'], $nuevoEstado, 'SEDEVER.CUIS_ENCUESTA', 'ENVIADO', 'CUIS', $folio_cuis, $idDocSoporte, $folioCedula);
        elseif($nuevoEstado == 0)
            $cuis->newHistorialCambios('Reincorporación de Familia', $_SESSION['IDUSUARIO'], $claveCuis, $valorAnterior['ENVIADO'], $nuevoEstado, 'SEDEVER.CUIS_ENCUESTA', 'ENVIADO', 'CUIS', $folio_cuis, $idDocSoporte, $folioCedula);

        $result = $cuis->toggleRemoveFamily($claveCuis, $nuevoEstado);
        return $result;
    }
    
    // function updateCuisInformationGeneral($objectData){
    //     if(empty($objectData)) return false;

    //     print_r($objectData);
    // }

    // function saveDocSupport(){        
    //     $return = Array('ok'=>TRUE);        
    //     $nombre_archivo = $_FILES['documentoSoporte']['name'];        
    //     $tipo_archivo = $_FILES['documentoSoporte']['type'];
    //     $tamano_archivo = $_FILES['documentoSoporte']['size'];
    //     $tmp_archivo = $_FILES['documentoSoporte']['tmp_name'];
        
    //     $directorioDestino = './img_tmp/' . $nombre_archivo;
    //     if (!move_uploaded_file($tmp_archivo, $directorioDestino)) {
    //         $return = Array('ok' => FALSE, 'msg' => "Ocurrio un error al subir el archivo. ".$nombre_archivo." No pudo guardarse.", 'status' => 'error');
    //     }
    //     // header('Content-Type: application/json');        
    //     // echo json_encode($return);
    //     return $return;
    // }

    function municipios($idEstado){
        $cuis = new cuis;
        $results = $cuis->consulta_municipio($idEstado);
        return $results;
    }
    
    function localidades($idMunicipio){
        $cuis = new cuis;
        $results = $cuis->consulta_localidad($idMunicipio);
        return $results;
    }

    function updateLocation($objectData){
        $cveEstado = (!empty($objectData['cveEstado'])) ? $objectData['cveEstado'] : '';
        $cveMunicipio = (!empty($objectData['cveMunicipio'])) ? $objectData['cveMunicipio'] : '';
        $cveLocalidad = (!empty($objectData['cveLocalidad'])) ? $objectData['cveLocalidad'] : '';

        $resultados['correctos'] = 0;
        // ------------------------------------------------------------------------------------------------------------------------
        // $idDocSoporte = $this->getDocSoporteId(0, $objectData['cveCuis']);
        // if( ($idDocSoporte == false || $idDocSoporte == 'false') && $idDocSoporte != '') 
        //     return Array('ok' => FALSE, 'msg' => "Ocurrio un error al subir el archivo. ".$nombre_archivo." No pudo guardarse.", 'status' => 'error');                
        // ------------------------------------------------------------------------------------------------------------------------
        
        $cuis = new cuis;
        $valorAnterior = $cuis->consultaDomicilioCuis($objectData['cveCuis']);
        // print_r($valorAnterior);
        if(!empty($cveEstado)){
            $query = "SELECT * FROM INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD WHERE CVEENT = $cveEstado AND ROWNUM < 2";
            $nuevoValor = $cuis->valorAnterior($query);
            
            $result = $cuis->updateDomicilioCuis('CVE_ENTIDAD_FEDERATIVA', $nuevoValor['CVEENT'], 'NOM_ENTIDAD_FEDERATIVA', $nuevoValor['NOMBREENT'], $objectData['cveCuis']);
            
            // $cuis->newHistorialCambios('Actualización Domicilio', $_SESSION['IDUSUARIO'], $objectData['cveCuis'], $valorAnterior['CVE_ENTIDAD_FEDERATIVA'], $nuevoValor['CVEENT'], 'SEDEVER.CUIS_DOMICILIO', 'CVE_ENTIDAD_FEDERATIVA', 'CUIS', $objectData['cveCuis'], $idDocSoporte, $objectData['folioCedula']);
            // $cuis->newHistorialCambios('Actualización Domicilio', $_SESSION['IDUSUARIO'], $objectData['cveCuis'], $valorAnterior['NOM_ENTIDAD_FEDERATIVA'], $nuevoValor['NOMBREENT'], 'SEDEVER.CUIS_DOMICILIO', 'NOM_ENTIDAD_FEDERATIVA', 'CUIS', $objectData['cveCuis'], $idDocSoporte, $objectData['folioCedula']);                    
            $resultados['correctos']++;
        }
        
        if(!empty($cveMunicipio)){
            $query = "SELECT * FROM INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD WHERE CVEMUN = $cveMunicipio AND ROWNUM < 2";
            $nuevoValor = $cuis->valorAnterior($query);

            $result = $cuis->updateDomicilioCuis('CVE_MUNICIPIO', $nuevoValor['CVEMUN'], 'NOM_MUNICIPIO', $nuevoValor['NOMBREMUN'], $objectData['cveCuis']);

            // $cuis->newHistorialCambios('Actualización Domicilio', $_SESSION['IDUSUARIO'], $objectData['cveCuis'], $valorAnterior['CVE_MUNICIPIO'], $nuevoValor['CVEMUN'], 'SEDEVER.CUIS_DOMICILIO', 'CVE_MUNICIPIO', 'CUIS', $objectData['cveCuis'], $idDocSoporte, $objectData['folioCedula']);
            // $cuis->newHistorialCambios('Actualización Domicilio', $_SESSION['IDUSUARIO'], $objectData['cveCuis'], $valorAnterior['NOM_MUNICIPIO'], $nuevoValor['NOMBREMUN'], 'SEDEVER.CUIS_DOMICILIO', 'NOM_MUNICIPIO', 'CUIS', $objectData['cveCuis'], $idDocSoporte, $objectData['folioCedula']);                    
            $resultados['correctos']++;
        }
        
        if(!empty($cveLocalidad)){
            $query = "SELECT * FROM INTEGRADOR.ESTADOMUNICIPIOLOCALIDAD WHERE CVELOC = $cveLocalidad AND ROWNUM < 2";
            $nuevoValor = $cuis->valorAnterior($query);

            $result = $cuis->updateDomicilioCuis('CVE_LOCALIDAD', $nuevoValor['CVELOC'], 'NOM_LOCALIDAD', $nuevoValor['NOMBRELOC'], $objectData['cveCuis']);
            
            // $cuis->newHistorialCambios('Actualización Domicilio', $_SESSION['IDUSUARIO'], $objectData['cveCuis'], $valorAnterior['CVE_LOCALIDAD'], $nuevoValor['CVELOC'], 'SEDEVER.CUIS_DOMICILIO', 'CVE_LOCALIDAD', 'CUIS', $objectData['cveCuis'], $idDocSoporte, $objectData['folioCedula']);
            // $cuis->newHistorialCambios('Actualización Domicilio', $_SESSION['IDUSUARIO'], $objectData['cveCuis'], $valorAnterior['NOM_LOCALIDAD'], $nuevoValor['NOMBRELOC'], 'SEDEVER.CUIS_DOMICILIO', 'NOM_LOCALIDAD', 'CUIS', $objectData['cveCuis'], $idDocSoporte, $objectData['folioCedula']);                    
            $resultados['correctos']++;
        }
        
        return $resultados;
    }

    function addLotImpresion($tipoImpresion, $cveFolioCuis){  
        if(empty($tipoImpresion) || ($tipoImpresion != 1 && $tipoImpresion != 2) || empty($cveFolioCuis)) return false;
        $cuis = new cuis;
        $result = $cuis->addNewLotImpresion($tipoImpresion, $cveFolioCuis);
        return $result;
    }

    function datosDomicilio($cveCuis){
        if(empty($cveCuis)) return false;
        $cuis = new cuis;
        $result = $cuis->getDataAddress($cveCuis);
        return $result;        
    }

    function updateAddress($folio_cuis, $cveVivienda, $newValue, $fieldUpdate, $folioCedula = ''){
        if( empty($cveVivienda) || empty($fieldUpdate) ) return 'Error Información Invalida';
        // ------------------------------------------------------------------------------------------------------------------------
        $idDocSoporte = $this->getDocSoporteId('', $folio_cuis);
        if(($idDocSoporte == false || $idDocSoporte == 'false') && $idDocSoporte != '') 
            return Array('ok' => FALSE, 'msg' => "Ocurrio un error al subir el archivo. ".$nombre_archivo." No pudo guardarse.", 'status' => 'error');
                
        // ------------------------------------------------------------------------------------------------------------------------

        $cuis = new cuis;                                                       
        $valorAnterior = $cuis->lastValueDomicilio($fieldUpdate, $cveVivienda);
        $valorAnterior = (!empty($valorAnterior[$fieldUpdate])) ? $valorAnterior[$fieldUpdate].'' : '';                                        
        $cuis->newHistorialCambios('Actualización de Domicilio', $_SESSION['IDUSUARIO'], $folio_cuis, $valorAnterior, $newValue, 'SEDEVER.CUIS_DOMICILIO', $fieldUpdate, 'CUIS', $folio_cuis, $idDocSoporte, $folioCedula);
                    
        $result = $cuis->updateDataDomicilio($fieldUpdate, $newValue, $cveVivienda);        
        return $result;
    }

}

$controlador_cuis = new controlador_cuis;
$controlador_cuis->VerificaSolicitud();
$error = $controlador_cuis->error;

if($error != null){
    echo $error;
	return false;
}

$opcion = $controlador_cuis->opcion;

if($opcion == "cuis"){
	echo json_encode( $controlador_cuis->cuis() ) ;
}
elseif($opcion == 'programasCuis'){    
    echo json_encode( $controlador_cuis->getProgramasCuis() );
}
elseif($opcion == 'generales'){    
    echo json_encode( $controlador_cuis->getDataGenerals() );
}
elseif($opcion == 'estadoValidacionImg'){    
    echo json_encode( $controlador_cuis->validacionImg($_POST['idCuis']) );
}
elseif($opcion == 'cambioValidacionesImg'){    
    echo json_encode( $controlador_cuis->cambioValidacionImg($_POST) );
}
elseif($opcion == 'datosDomicilio'){    
    echo json_encode( $controlador_cuis->datosDomicilio($_POST['idCuis']) );
}
elseif($opcion == 'updateFields'){ 
    if($_SESSION['PERFIL'] == '10' || $_SESSION['PERFIL'] == '8')   
        echo json_encode( $controlador_cuis->updatePersona($_POST) );
    else {
        echo 'Permisos Insuficientes';
    }
}
elseif($opcion == 'updateIntegrated'){    
    if($_SESSION['PERFIL'] == '10' || $_SESSION['PERFIL'] == '8')   
        echo json_encode( $controlador_cuis->updatePerson($_POST['claveIntegrante'], $_POST['folio_cuis'], $_POST['nuevoEstado'], $_POST['folioCedula']) );
    else {
        echo 'Permisos Insuficientes';
    }
}
elseif($opcion == 'updateFamily'){
    if($_SESSION['PERFIL'] == '10' || $_SESSION['PERFIL'] == '8')   
        echo json_encode( $controlador_cuis->updateFamily($_POST['claveCuis'], $_POST['folio_cuis'], $_POST['nuevoEstado'], $_POST['folioCedula']) );
    else {
        echo 'Permisos Insuficientes';
    }      
}
elseif($opcion == 'updateAddress'){
    if($_SESSION['PERFIL'] == '10' || $_SESSION['PERFIL'] == '8')   
        echo json_encode( $controlador_cuis->updateAddress($_POST['folio_cuis'], $_POST['cVivienda'], $_POST['newValue'], $_POST['field'], $_POST['folioCedula'] ) );
    else {
        echo 'Permisos Insuficientes';
    }      
}

// elseif($opcion == 'updateFieldsCuisGenerales'){
//     // if($_SESSION['PERFIL'] == '10' || $_SESSION['PERFIL'] == '8')   
//         echo json_encode( $controlador_cuis->updateCuisInformationGeneral($_POST) );
//     // else {
//         // echo 'Permisos Insuficientes';
//     // }        
// }
elseif($opcion == 'municipios'){    
    echo json_encode( $controlador_cuis->municipios($_POST['idEstado']) );  
}
elseif($opcion == 'localidades'){    
    echo json_encode( $controlador_cuis->localidades($_POST['idMunicipio']) );  
}
elseif($opcion == 'updateLocation'){    
    echo json_encode( $controlador_cuis->updateLocation($_POST) );      
}
elseif($opcion == 'agregarLoteImpresion'){    
    echo json_encode( $controlador_cuis->addLotImpresion($_POST['tipoImpresion'], $_POST['cveFolioCuis']) );      
}



// elseif($opcion == "rotateImage"){        
//     if(!empty($_POST)) 
//         echo json_encode( $controlador_cuis->rotateImage($_POST));
// }







?>