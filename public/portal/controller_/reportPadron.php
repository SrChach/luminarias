<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');


include_once '../../../pillar/clases/controlador.php';
include_once '../../../pillar/clases/utilities.php';
include_once '../../../pillar/clases/QueryController.php';
// include_once './MailGeneral.php';
include '../../../pillar/clases/MailGeneral.php';
include '../../../pillar/clases/exportaRepoGeneral.php';
include '../../../pillar/clases/BodyMail.php';
include '../../../../parametros.php';
include_once '../../../public/portal/libraries/phpmailer-master/class.phpmailer.php';



// include_once './exportaRepoGeneral.php';
// include_once './BodyMail.php'; 
// include_once 'ErrorController.php'; 
// include_once '../../public/portal/libraries/phpmailer-master/class.phpmailer.php';


class reportesPadron  extends controlador
{

	protected $result;
	protected $modelRepo;
	protected $modelRepoPadron;



	function __construct()
	{
		$this -> conexion = new conexion;
		$this -> modelRepo = new QueryReports;
		$this -> modelRepoPadron = new Reports2;
	}



	function generaReportePadron($opcion,$values=''){

		$idUsuarioLogin=$values['idusuario'];
		$fecha=strftime( "%Y%m%d%H%M%S", time() );

		
		$resQueryNombre = $this -> modelRepo -> nombrePrograma($values);

		$resNombre = $this -> conexion ->getResult($resQueryNombre);


		$resPrograma=$resNombre['NOMBREPROGRAMA'];
		$nom_padron=str_replace(" ", "_", $resPrograma);
		$nombreReporte='Padron_'.$nom_padron."_".$fecha.".csv";


		$carpetaReporte='../../../../reportes/'.$nombreReporte;

		$resQuery = $this -> modelRepo -> repoPadron($values);


		$respuestaGenera = $this -> modelRepoPadron -> csv($resQuery,$carpetaReporte,$nombreReporte,$idUsuarioLogin,$opcion,$values);
		return $respuestaGenera;
	}

}





if( isset( $_POST['opcion'] ) ){
	$opcion = $_POST['opcion'];   
	$values = ($_POST['values'] != "") ? $_POST['values']:"0";
	$reportPadron = new reportesPadron;
	echo json_encode($reportPadron-> $opcion($opcion,$values) ); 
}




?>

