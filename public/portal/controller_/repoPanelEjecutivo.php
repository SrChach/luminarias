<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');


include_once '../../../pillar/clases/controlador.php';
include_once '../../../pillar/clases/utilities.php';
include_once '../../../pillar/clases/QueryController.php';
include_once '../../../pillar/clases/MailGeneral.php';
include_once '../../../pillar/clases/exportaRepoGeneral.php';
include '../../../pillar/clases/BodyMail.php';
include_once '../libraries/phpmailer-master/class.phpmailer.php';
include '../../../../parametros.php';
include_once 'ErrorController.php';



class repoPanelEjecutivo  extends controlador
{

	protected $result;
	protected $modelRepoEjecutivo;
	protected $modelRepoPadron;



	function __construct()
	{
		//$this -> conexion = new conexion;
		$this -> modelRepoEjecutivo = new QueryReports;
		$this -> modelRepoPadron = new Reports2;
	}



	function generaRepoPanelEjec($opcion,$values=''){
		$idUsuarioLogin="";
		$idUsuarioLogin=$values['idUsuarioLogin'];
		$fecha=strftime( "%Y%m%d%H%M%S", time() );


		$resQuery = $this -> modelRepoEjecutivo -> traeCuis($values);
		$nombreReporte='PanelEjecutivo_'.$values['cve_tipo']."_".$fecha.".csv";


		$carpetaReporte='../../../../reportes/'.$nombreReporte;
		$resGenera = $this -> modelRepoPadron -> csv($resQuery,$carpetaReporte,$nombreReporte,$idUsuarioLogin,$opcion,$values);
		return 'OK';
	}

}






if( isset( $_POST['opcion'] ) ){
	$opcion = $_POST['opcion'];   
	$values = ($_POST['values'] != "") ? $_POST['values']:"0";
	$repoEje = new repoPanelEjecutivo;
	echo json_encode($repoEje-> $opcion($opcion,$values) ); 
}




?>

