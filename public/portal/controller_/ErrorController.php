<?php

/**
*
*/
class ErrorController
{
	protected $Errors;
	protected $code;
	function __construct()
	{
		$this -> Errors	= array('Method' => 'Petición No Valida',
								'Token' => 'FALLO DE AUTENTICACIÓN',
								'Option' => 'Opción No Valida',
								'OK' => 'Exito!',
								'NO' => 'Lo siento!',
								'Null' => 'Sin Datos',
								'Err1' => 'LO SIENTO! NO SE HA PODIDO COMPLETAR ESTA ACCIÓN. INTENTE DE NUEVO',
								'Err2' => 'LO SIENTO! POR EL MOMENTO NO SE PUEDE MOSTRAR INFORMACIÓN',
								'Fatal' => 'Algo sucedio! Si el problema persiste comuniquese con el Administrador.',
								'IMG' => 'Lo siento! no hay Imagen.',
								'addP' => 'Se ha cargado nuevo programa');
		$this -> code = false;

	}
	public function ErrorMethod()
	{
		$this-> Errors = array('codigo' => 405,
								'message' => $this -> Errors['Method']
								);
		return $this-> Errors;
	}
	public function ErrorToken()
	{
		$this-> Errors = array('codigo' => 403,
								'message' => $this -> Errors['Token']
								);
		return $this-> Errors;
	}
	public function ErrorOption()
	{
		$this-> Errors = array('codigo' => 404,
								'message' => $this -> Errors['Option']
								);
		return $this-> Errors;
	}
	public function ErrorMessages($type)
	{
		if ($type=='OK'|| $type=='addP') {
			$this -> code=true;
		}
		if ($type=='Null') {
			$this -> code=Null;
		}
		$this-> Errors = array('codigo' => $this -> code,
								'message' => $this -> Errors[$type]);
		return $this-> Errors;
	}
}
?>
