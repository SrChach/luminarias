<?php
/**
*
*/
date_default_timezone_set('America/Mexico_City');
include_once 'SqlCalled.php';
class session extends SqlCalled
{
	protected $sql;
	protected $result;
	protected $conexion;
	function __construct()
	{
		$this -> sql = null;
		$this -> result = null;
		$this -> conexion = new conexion;
		$this -> last_session = date("d-m-Y H:i:s");
		//echo $this-> last_session;
	}

	public function start($_c,$_k,$_t)
	{	//$respuesta=mysql_query("SELECT * FROM `Usuarios` WHERE `user`='".mysql_real_escape_string($name)."' AND `pass`='".mysql_real_escape_string($password)."'")

		if (!$this -> verifyCrednetials($_c,$_k)) {
			$this -> Datos = array('CODIGO' => false,
										'DATOS' => 'APARICIÓN DE CARACTERES NO VALIDOS! INTENTE DE NUEVO.' );
			return $this -> Datos;
		}

		$this -> sql = SqlCalled::session('start',$_c,$_k,$_t,$this -> last_session);
		//echo $this -> sql;
		try {
			$this -> result = $this -> conexion -> getResults($this -> sql);

			//echo json_encode($this -> result);
			if ($this -> result!=null) {
				session_destroy();
        		session_start();

            	$_SESSION['IDUSUARIO'] = $this-> result[0]['IDUSUARIO'];
            	$_SESSION['NAME'] = $this-> result[0]['NOMBRE'];
            	$_SESSION['PERFIL'] = $this-> result[0]['IDPERFIL'];
            	$_SESSION['STATUS'] = $this-> result[0]['STATUS'];
				$_SESSION['_TOKEN'] = $_t;
				$_SESSION['Auth'] = 1;
				$_SESSION['VERIFICADO'] = $this-> result[0]['VERIFICADO'];
				$_SESSION['ZONA'] = $this-> result[0]['ZONATRABAJO']; // SE ASIGNA VALOR DE ZONA DE TRABAJO A LA SESSION
				$_SESSION['DELEGACION'] = $this->result[0]['IDDELEGACION'];

/*            	if ($this-> result[0]['PERFIL']==10) {
            		$_SESSION['programs'] = '';
            		$_SESSION['modules'] = '';
            	}else{
            		$_SESSION['programs'] = $this-> result[0]['PROGRAMAS'];
            		$_SESSION['modules'] = $this-> result[0]['MODULOS'];
            	}*/


            	if ($_SESSION['STATUS']==1) {
            		//$this -> sql = SqlCalled::session('token',$_c,$_k,$_t,$this -> last_session);
    				//$this -> result = $this -> conexion -> insertUpdate($this -> sql);

    				$this -> Datos = array('CODIGO' => true,
											'DATOS' => true,
											'CREDENTIAL' => $this-> result[0]['IDPERFIL'],
											'VERIFICADO' => 
											$this-> result[0]['VERIFICADO'] );
    			}else{
    				$this -> Datos = array('CODIGO' => false,
											'DATOS' => 'CUENTA CANCELADA! CONTACTE A SU ADMINISTRADOR PARA ACLARACIONES.' );
    				session_destroy();
    			}
			}else{
				$this -> Datos = array('CODIGO' => false,
										'DATOS' => 'CREDENCIALES INCORRECTAS! INTENTE DE NUEVO.' );
			}

		} catch (Exception $e) {
			$this -> Datos = array('CODIGO' => false,
									'DATOS' => $e );
		}
		return $this -> Datos;
	}
	public function out()
	{
		session_unset();
		session_destroy();
		return true;
	}

	public function verifyCrednetials($_c,$_k)
	{	$check=true;
		if ($_c=="'or'1'='1" || $_c==" ' or ' 1 ' = ' 1 " || $_k=="'or'1'='1" || $_k==" ' or ' 1 ' = ' 1 ") {
			$check=false;
		}
		$chart="'";
		$exist_c = strpos($_c, $chart);
		$exist_k = strpos($_k, $chart);

		if ($exist_c !== false || $exist_k !== false) {
			$check=false;
		}

		return $check;
	}
}
?>
