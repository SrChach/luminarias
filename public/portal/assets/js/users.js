  var tok;
  var tableU;
  function tableUsers(datas,heads,_t) {
    tok=_t;
    var cadena="<table class='table table-bordered table-condensed table-striped  table-hover' 	id='tbUsers'>\
    <thead>\
    <tr style='font-size:11px;'>\
    ";
    $.each(heads, function(i, item) {
     cadena+="<th>"+item+"</th>";
   });
    cadena+="	</tr>\
    </thead>\
    <tbody>";
    var opcion="";
    var status="";
    $.each(datas, function(i, item) {

      var displaynone="rr";

      //console.log("perfil:" + perfilLogin+ "  perfil_registro:" + item.PERFIL);

      

      if (item.STATUS==1) {
       item.STATUS="<span class='right badge badge-success' id='act"+item.IDUSUARIO+"'>ACTIVO</span>";
       opcion="<button type='button' id='btn"+item.IDUSUARIO+"' onclick='cancelAccount("+item.IDUSUARIO+",\""+_t+"\")' class='btn btn-outline-dark btn-sm' data-toggle='tooltip' data-placement='top' title='CANCELAR CUENTA'><i class='icon ion-minus-circled'></i></button>";
     }
     if (item.STATUS==0) {
       item.STATUS="<span class='right badge badge-danger' id='act"+item.IDUSUARIO+"'>INACTIVO</span>";
       opcion="<button type='button' id='btn"+item.IDUSUARIO+"' onclick='upAccount("+item.IDUSUARIO+",\""+_t+"\")' class='btn btn-outline-success btn-sm' data-toggle='tooltip' data-placement='top' title='ACTIVAR CUENTA'><i class='icon ion-plus-circled'></i></button>";
     }
     if (item.IMG_PERFIL!=null) {
      item.IMG_PERFIL=item.IMG_PERFIL;
    }else{
      item.IMG_PERFIL='sin_perfil.png';
    }


    if (perfilLogin == 3 || perfilLogin == 8  || perfilLogin == 1 || perfilLogin == 2 || perfilLogin == 4 || perfilLogin == 5 || perfilLogin == 6 || perfilLogin == 11 ){
      if (item.PERFIL == 10 || item.PERFIL == 6 || item.PERFIL == 5 || item.PERFIL == 4 || item.PERFIL == 2 || item.PERFIL == 1 || item.PERFIL == 0) {
       displaynone=" style='display:none'";
     }
   }


   //console.log(displaynone);

   cadena+="<tr"+displaynone+">\
   <td>"+(i+1)+"</td>\
   <td align='center'><img src="+urlImgProfile+item.IMG_PERFIL+" class='img-fluid img-rounded' style='height:70px;'></td>\
   <td>"+item.USUARIO+"</td>\
   <td>"+item.TIPOUSUARIO+"</td>\
   <!--td>"+item.PROYECTO+"</td-->\
   <td>"+item.EMAIL+"</td>\
   <td>"+item.STATUS+"</td>\
   <td>\
   "+opcion+"\
   <!--button type='button' onclick='values(\""+item.USUARIO+"\","+item.IDUSUARIO+","+i+");' class='btn btn-outline-danger btn-sm' data-toggle='modal' data-target='#modalDelete'><i class='icon ion-close-circled'></i></button-->\
   <!--button type='button' onclick='seeAccount("+item.IDUSUARIO+",\""+_t+"\")' class='btn btn-outline-info btn-sm' data-toggle='tooltip' data-placement='top' title='VER CUENTA'><i class='icon ion-arrow-resize'></i></button-->\
   <a type='button' href='?view=users&act=add&id="+item.IDUSUARIO+"' class='btn btn-outline-secondary btn-sm' data-toggle='tooltip' data-placement='top' title='EDITAR CUENTA'><i class='icon ion-edit'></i></a>\
   </td>\
   </tr>";

 });

    cadena+="</tbody>\
    </table>";
    $('#tableUsers').html(cadena);
    tableExport2('#tbUsers','USUARIOS');
    $('[data-toggle="tooltip"]').tooltip(); 
    $('.popover-dismiss').popover({
     trigger: 'manual'
   });
    $('[data-toggle="popover"]').popover({ html : true  });
    settingsTable();
  }




  function cancelAccount(id,_t) {
   objet={opcion:'Account',action:'cancelAccount',id:id,csrf_token:_t};
   ajaxCallback(objet,url,function (respuesta){
    res = JSON.parse(respuesta);
    if (res.CODIGO==true) {
     notificar('EXITO! ',res.DATOS,'success');
   //getUsers();
   updateRow(id,'cancelAccount',_t);
 }else{
  notificar('ERROR! ',res.DATOS,'error');
}
});
 }

 function upAccount(id,_t) {

   objet={opcion:'Account',action:'upAccount',id:id,csrf_token:_t};
   ajaxCallback(objet,url,function (respuesta){
    res = JSON.parse(respuesta);
    if (res.CODIGO==true) {
     notificar('EXITO! ',res.DATOS,'success');
   //getUsers();
   updateRow(id,'upAccount',_t);
 }else{
  notificar('ERROR! ',res.DATOS,'error');
}

});
 }

 function deleteAccount(id) {
  var passDelete=$('#ipPD').val();
  if (passDelete==' ') {
    objet={opcion:'Account',action:'deleteAccount',id:id,csrf_token:tok};
    ajaxCallback(objet,url,function (respuesta){
      res = JSON.parse(respuesta);
      if (res.CODIGO==true) {
        cancel('');
        notificar('EXITO! ','SE HA ELIMINADO CUENTA','error');
        updateRow(id,'deleteAccount');
        $("#modalDelete").modal("hide");
      }else{
        notificar('ERROR! ',res.DATOS,'error');
        $("#modalDelete").modal("hide");
      }
    });
  }else{
    notificar('LO SIENTO! ','CONTRASEÑA INVALIDA... VERIFIQUE QUE EL DATO SEA CORRECTO E INTENTE DE NUEVO.','warning');
    $('#ipPD'+id).val('');
  }

}

/*function cancel(action){
    if (action=='') {//close popover;
      $('[data-toggle="popover"]').popover('hide');
    }
    if (action==1) {//deleteAccount();
      $('[data-toggle="popover"]').popover('hide');
      notificar('NOTIFICACIÓN! ','PUEDE ELIMINAR CUENTAS CUANDO LO REQUIERÁ. RECUERDE QUE LAS CUENTAS ELIMINADAS NO SE PODRAN RECUPERAR.','info');
    }
  }*/

  function seeAccount(id,_t) {
    objet={opcion:'Account',action:'seeAccount',id:id,csrf_token:_t};
    ajaxCallback(objet,url,function (respuesta){
      res = JSON.parse(respuesta);
      //console.log(res);
      /*if (res.CODIGO==true) {
        notificar('EXITO! ',res.DATOS,'success');
        //getUsers();
        updateRow(id,'upAccount',_t);
      }else{
        notificar('ERROR! ',res.DATOS,'error');
      }*/

    });
  }

  function editAccount(argument) {
   // body...
 }

 function values(user,id,i) {
  $('#nameAccount').html(user);
  $('#idUser').val(id);
    //$('#rowNum').val(i);
  }

  function updateRow(element,action,_t='',datas='') {
    if (action=='cancelAccount') {
      $('#act'+element).removeClass('badge-success');
      $('#act'+element).addClass('badge-danger');
      $('#act'+element).html('INACTIVO');
      $('#btn'+element).removeClass('btn-outline-dark');
      $('#btn'+element).addClass('btn-outline-success');
      $('#btn'+element).attr('data-original-title','ACTIVAR CUENTA');
      $('#btn'+element).attr('onclick','upAccount('+element+',"'+_t+'")');
      $('#btn'+element).html("<i class='icon ion-plus-circled'></i>");
    }
    if (action=='upAccount') {
      $('#act'+element).removeClass('badge-danger');
      $('#act'+element).addClass('badge-success');
      $('#act'+element).html('ACTIVO');
      $('#btn'+element).removeClass('btn-outline-success');
      $('#btn'+element).addClass('btn-outline-dark');
      $('#btn'+element).attr('data-original-title','CANCELAR CUENTA');
      $('#btn'+element).attr('onclick','cancelAccount('+element+',"'+_t+'")');
      $('#btn'+element).html("<i class='icon ion-minus-circled'></i>");
    }
    if (action=='deleteAccount') {
        //table.row(':eq('+element+')').remove().draw(false);
        table
        .row( $('#act'+element).parents('tr') )
        .remove()
        .draw();
      }
    }





/////////////////////////////////////// ADD AND EDIT
values='';
function getProfilesDB(perfilLogin){ 
  var perfilLoginJS=perfilLogin;
  var url2='./controller/GeneralesController.php';


  values={pato:"patito",perro:"perrito"}
  objet={opcion:'generales',action:'getProfilesAll',csrf_token:'algo',values:values};
  ajaxCallback(objet,url2,function (respuesta){
    res = JSON.parse(respuesta);
    console.log(res);
    if (res.CODIGO==true) {
      var options=buildOptions(res.DATOS,'TIPOUSUARIO','IDTIPOUSUARIO');
      $('#perfilUser').html(options);




    }else{
      notificar('ERROR! ',res.DATOS,'error');
    }
  });
}



function showProyectosInicio(datas,select='') {
  $.each(datas.programas, function(i, item) {
    DOM=Render(item,i,'Programas','');
    $('#contentProgramas').append(DOM);
  });
}



function showSitiosInicio(datas,_t='') {
 $.each(datas, function(i, item) {
  DOM=Render(item,i,'Sitios',_t);
  $('#ContentSitios').append(DOM);
});
}



function Render(item,i,org,_t) {
  var render="";
  if (org=='Programas') {
    render=' <div class="input-group mb-3 col-md-5 offset-md-1" style="margin-top: 3em;"">\
    <div class="col-md-2 " >\
    <div class="input-group-prepend" style="margin-top: 30px">\
    <div class="input-group-text">\
    <td><input type="checkbox"  name="idPrograma" id="idPrograma'+item.IDPROGRAMA+'" value="'+item.IDPROGRAMA+'"    ></td>\
    </div>\
    </div>\
    </div>\
    <div class=" col-md-10" >\
    <img class="card-img-top img-fluid" src="./Storage/Evidencias/imgPrograms/cuadrada2/'+item.IMAGEN_CUADRADA+'" alt="image">\
    </div>\
    </div>';
  }
  if (org=='Sitios') {
    if (item.on) {
      var a="";
      render="<a class='col-md-3 col-sm-4 col-xs-2 ' style='margin-left:0%'>\
      "+a+"\
      <div class='row'>\
      <div class=' col-md-9 card hover offset-md-1' style='padding:0px; height:20%'>\
      <div class='input-group-text'>\
      <td><input type='checkbox'  name='idSites' id='idModulo"+item.idSitio+"' value='"+item.idSitio+"'  ></td>\
      </div>\
      <img class='' src='./Storage/Evidencias/imgSites/"+item.img+"' alt='Card image cap' >\
      <p class='upperCase' style='text-align: center; width: 100%'  >"+item.name+"</p>\
      </div>\
      </div>\
      </a>\
      </div>";
    }
  }
  return render;
}


function previewImage(){
  $("#vista-previa").html('');
  var archivos = document.getElementById('img1').files;

  var navegador = window.URL || window.webkitURL;
  for(x=0; x<archivos.length; x++)
  {
    var size = archivos[x].size;
    var type = archivos[x].type;
    var nameImg_2 = archivos[x].name;
    if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png' )
    {
      $("#vista-previa").append("<div class='col-md-12' style='width: 100%; height: 400px;'><br><br><br><p style='color: red'>El archivo "+nameImg_2 +" no es del tipo de imagen permitida.</p></div>");
    }else{
      var objeto_url = navegador.createObjectURL(archivos[x]);

      $("#vista-previa").append("<div class='col-md-12'><div class='row'><img src="+objeto_url+" width='100%' height='400'></div><br><br><div class='row' id='name_img'>"+nameImg_2+"</div></div>");
    }
  } 
}





function verifica_contrasena(){
  var pass1=document.getElementById("pass_temp").value;
  var pass2=document.getElementById("pass_temp_conf").value;
  if (pass1 != pass2 || pass2 != pass1 ) {
    alert ("LAS CONTRASEÑAS NO COINCIDEN");
    document.getElementById('pass_temp').value="";
    document.getElementById('pass_temp_conf').value="";
  } 
}


var imgDB="";
function showUserProfileData(id,_t){
  objet={opcion:'SeeProfile',action:'getUserProfile',id:id,csrf_token:_t};
  ajaxCallback(objet,url,function (respuesta){
    res = JSON.parse(respuesta);

    var generales=res.DATOS['GENERALES'];
    var programas=res.DATOS['PROGRAMAS'];
    var modulos=res.DATOS['MODULOS'];

    //imgDB=generales['IMG_PERFIL'];


    typeof generales['IMG_PERFIL'] !== 'undefined' ?  imgDB=generales['IMG_PERFIL'] : imgDB="";


    if (generales) {
      typeof generales['APELLIDOMATERNO'] !== 'undefined' ?  aMaterno=generales['APELLIDOMATERNO'] : aMaterno="";
      typeof generales['TELEFONO_C'] !== 'undefined' ?  telefono_c=generales['TELEFONO_C'] : telefono_c="";
      var pintar = [
      {clave:'existe',     valor:'actualizar'},
      {clave:'usuario',    valor:id},
      {clave:'nameUSer',   valor:generales['NOMBRE']},
      {clave:'apP',        valor:generales['APELLIDOPATERNO']},
      {clave:'apM',        valor:aMaterno},
      {clave:'tele_c',     valor:telefono_c},
      {clave:'tele_t',     valor:generales['TELEFONO_T']},
      {clave:'email',      valor:generales['EMAIL']},
      {clave:'rol',        valor:generales['ROL']},
      {clave:'perfilUser', valor:generales['PERFIL']},
      {clave:'pass_temp',  valor:generales['PASS']},
      {clave:'pass_temp_conf', valor:generales['PASS']},
      {clave:'passInicial', valor:generales['PASS']},
      {clave:'statusValidacionDB', valor:generales['VALIDACION']}
      ];

      var pintarValores = pintar.map(function(obj){ 
       document.getElementById([obj.clave]).value=obj.valor;
     });

      $("#vista-previa").append("<div class='col-md-12'><div class='row'>\
        <img src='"+urlImgProfile+generales['IMG_PERFIL']+"' width='100%' height='400'></div><br><br><div class='row'>"+generales['IMG_PERFIL']+"</div></div>");

      for (var i = 0; i < programas.length; i++) {
        document.getElementById('idPrograma'+programas[i]['IDPROGRAMA']).checked=true;
        //document.getElementById('idPrograma'+programas[i]['IDPROGRAMA']).disabled = true;
      }



      for (var i = 0; i < modulos.length; i++) {
        document.getElementById('idModulo'+modulos[i]['IDMODULO']).checked=true;
      }



    }else{
      notificar('ERROR! ','El usuario no existe','error');
    }
  });
}





var url="./controller/UserController.php";
function addUser(_t,org,tipo,idusuario) {

  var generales = serialize('#dataGenerales','array');
  var programas = serialize('#contentProgramas','array');
  var aplicativos = serialize('#ContentSitios','array');
  var archivos = document.getElementById('img1').files;
  var name_img="";


  if (archivos.length !=0 ){
   var name2 = archivos[0].name;
   var dataFile=name2.split(".");
   var name_img= (dataFile[0]+makeid()+"."+dataFile[1]).replace(" ","_");
   var upload = Files(urlUploadFile,"#img1",'insert',_t,org,'',name_img);
 } else {
  var name_img=imgDB;
}

valuesPOST={generales:generales, programas:programas, aplicativos:aplicativos,imagen:name_img,tipo:tipo,idusuario:idusuario}
objet={opcion:'Account',action:'putUser',values:valuesPOST,csrf_token:_t};

ajaxCallback(objet,url,function (respuesta){
  res = JSON.parse(respuesta);
  exitoAddUser(res,generales);
});

}






function exitoAddUser(res, generales) {
  //console.log("respuesta:"+res);

  if (res=='OK'){
    var nombre=`${generales[0]['value']} ${generales[1]['value']} ${generales[2]['value']}`; 
    var mail_user=generales[5]['value'];
    var pass=generales[6]['value'];
    var url="./controller/MailController.php"
    objet={nombre:nombre, mail_user:mail_user, pass:pass};
    ajaxCallback(objet,url,function (respuesta){
      res = JSON.parse(respuesta);
      notificar('EXITO! ',"\n Usuario actualizado/registrado exitosamente",'success');
      cerrarTime();
    });
  }else{
    notificar('ERROR! ',"\n No se pudo actualizar/registrar el usuario",'success');
    cerrarTime();
  }



  function cerrarTime(){
    $("#FormAddUser")[0].reset();
    setTimeout(cierra(),10000);  
  }




 //  if (res=='OK' || res=='NO' || res=='REGISTRADO' || res=='ACTUALIZADO') {
 //   notificar('EXITO! ',"\n Usuario actualizado/registrado exitosamente",'success');
 //   $("#FormAddUser")[0].reset();
 //   setTimeout(cierra(),3000);  

 // } 
}

function cierra(){

 window.location='?view=users&t=2';  
}




