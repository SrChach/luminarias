var table;
var colorsBars=['#2BB273','#28A6DE'];
var colorsArea=[colorsBars[1],colorsBars[0]];

function checaValorDefault(cadena){
    if(cadena == null || cadena == 'null'){
        cadena = "";
    }
    return cadena;
}
function separa(str) {
  if (parseInt(str)) {
    //var str = "35758";
    if(str != null){
        var string  = str.toString();
        var res = string.split("");
        var cuantos = res.length;
        var regresa = "";
        if(cuantos<=3){
          regresa= str;
        }else
        if(cuantos==4){
          regresa = res[0] + "," + res[1] + res[2] +  res[3]; 
        }else
        if(cuantos == 5){
          regresa = res[0] + res[1] + "," + res[2] +  res[3] + res[4];
        }else
        if(cuantos == 6){
             regresa = res[0] + res[1] + res[2] + "," + res[3] + res[4] + res[5];
        }else
        if(cuantos == 7){
             regresa = res[0] + ","  + res[1] + res[2] + res[3] + "," + res[4] + res[5] + res[6];
        }
        if(cuantos == 8){
             regresa = res[0] + res[1] + "," + res[2] + res[3] + res[4] + "," + res[5] + res[6] + res[7];
        }
        return regresa;
    }
  }
  else{
    return str; 
  }
    
    
    
}
function showAlertInwindow(elemento,tipo,mensaje) {
    var msj="<div class='col-4 offset-4 alert alert-"+tipo+"'>"+mensaje+"</>"
    $(elemento).html(msj);
}

function cancel(action){
    if (action=='') {//close popover;
        $('[data-toggle="popover"]').popover('hide');
    }
    if (action==1) {//deleteAccount();
        $('[data-toggle="popover"]').popover('hide');
        notificar('NOTIFICACIÓN! ','PUEDE ELIMINAR CUENTAS CUANDO LO REQUIERÁ. RECUERDE QUE LAS CUENTAS ELIMINADAS NO SE PODRAN RECUPERAR.','info');
    }
}

function buildOptions(datas,index,value) {
  var options="<option value=''>SELECCIONE ...</option>";
  for (var i = 0; i < datas.length; i++) {
   options+="<option value='"+datas[i][value]+"'>"+datas[i][index]+"</option>";
  }
  return options;
}
function href($option,$index) {
  location.href = "?view="+$option+"&index="+$index; 
}

function onlyNumber(evt,element) {
  var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
        $(element).addClass('is-invalid');
        $('#FNumbers').show();
        return false;
    }
    $('#FNumbers').hide();
    $(element).removeClass('is-invalid');
    return true;

}

function onlyLetter(evt,element) {
       evt = (evt) ? evt : event;
       var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
          ((evt.which) ? evt.which : 0));
       if (charCode > 31 && (charCode < 65 || charCode > 90) &&
          (charCode < 97 || charCode > 122)) {
          $(element).addClass('is-invalid');
          $('#FLetters').show();
          return false;
       }
       $('#FLetters').hide();
        $(element).removeClass('is-invalid');
       return true;
}
function capitalLetter(str) {
  if (str==null) str='null';
  str = str.toLowerCase();
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function ValidafileType(tField,iType) {
  file=tField.value;
    if (iType==1) {
        extArray = new Array(".gif",".jpg",".png",".jpeg",".jpeeg",".jpej");
        }
    if (iType==2) {
        extArray = new Array(".swf");
    }
    if (iType==3) {
        extArray = new Array(".exe",".sit",".zip",".tar",".swf",".mov",".hqx",".ra",".wmf",".mp3",".qt",".med",".et");
    }
    if (iType==4) {
        extArray = new Array(".mov",".ra",".wmf",".mp3",".qt",".med",".et",".wav");
    }
    if (iType==5) {
        extArray = new Array(".html",".htm",".shtml");
    }
    if (iType==6) {
        extArray = new Array(".xls",".xlsx");
    }
    if (iType==7) {
        extArray = new Array(".zip");
    }
    if (iType==8) {
        extArray = new Array(".pdf");
    }
    allowSubmit = false;
 
    if (!file) return;
    while (file.indexOf("\\") != -1) file = file.slice(file.indexOf("\\") + 1);
    ext = file.slice(file.indexOf(".")).toLowerCase();
    for (var i = 0; i < extArray.length; i++) {
            if (extArray[i] == ext) {
            allowSubmit = true;
            break;
            }
    }
    if (allowSubmit) {
    } else {
    tField.value="";
    alert("Usted solo puede subir archivos con extensiones " + (extArray.join(" ")) + "\nPor favor seleccione otro archivo");
    }
}

function complementosRenderTable(index,id,title='',idP='',org=''){
  var complete=null;
  if (org=='') {
    complete='callNewgrafica("'+index+'",'+id+',"'+title+'",'+idP+');';
  }else{
    complete=org+'("'+index+'","'+id+'","'+title+'",'+idP+');';//org+'("'+index+'",'+id',"'+title'",'+idP+');';
  }
  return complete;
}

function renderTable(element,data,idP,jump='',exeption='',idtable='',onclick='',org='') {
  $(element).html('');
  var key=null;
  var keys=null;
  var click='';
  
  if (jump=='' && jump!=false) {jump=4}
  if (idP==3||idP==2) {
    keys = Object.keys(data[0]);
    key=keys;
  }else{
    if (org=='') {
      keys = Object.keys(data[idP]); 
    }else{
      keys = Object.keys(data[0]); 
    }
    key=keys; 
  }

  if (org.org!='') {
      click=org.click;
  }
  
  if (exeption!=0||exeption!='') {
    key=[];
    for (var i = exeption; i < keys.length; i++) {
      key.push(keys[i]);
    }
  }

  if (idtable=='') {
    idtable='table-dinamic';
  }
  var index=null,title=null;
  if (onclick=='subdelegacion') {
    index='ID_DEL';
    title='DELEGACIÓN';
  }
  if (onclick=='municipio') {

    index='ID_SUB';
    title='SUBDELEGACIÓN';
  }
  if (onclick=='localidad') {
    index='CVE_MUN';
    title='MUNICIPIO';
  }

  var tr="<tr>";
  
  var totales = [];
  var cadena = "<table id='"+idtable+"' class='table table-bordered table-hover' >\
                                    <thead class='thead-light'>\
                                        <tr>";
                                        $.each(key,function (i,item) {
                                          cadena += "<th>"+item+"</th>";
                                            if(i>=jump){
                                                totales[i] = 0;
                                            }
                                        });
                              cadena += "</tr>\
                                    </thead>\
                                    <tbody>";
  var numNotif = 0;
  var numExp = 0;
  var tot_visitas = 0;
  var value=null;
                      for(var i = 0;i < data.length;i++){
                        if (onclick!='') {
                          tr="<tr onclick='"+complementosRenderTable(onclick,data[i][index],data[i][title],idP,click)+"'>"
                        }
                        cadena += tr;
                                for (var j = 0; j < key.length; j++) {
                                  value=data[i][key[j]];
                                    cadena += "<td>"+separa(value)+"</td>";
                                    if(j>=jump){
                                        totales[j] = Number(totales[j]) + Number(data[i][key[j]]);
                                    }
                                }    
                        cadena += "</tr>";
                      }
                      
                    cadena += "</tbody>";

                    if (jump!=false) {
                          cadena+="<tfoot class='thead-light'>\
                                    <tr>\
                                      <th colspan='"+jump+"'>Totales</th>";
                                    for (var j = jump; j < (totales.length); j++) {
                                cadena += "<th>"+separa(totales[j])+"</th>";
                                    } 
                      }
                                            
                          cadena += "</tr>\
                                    </tfoot>\
                                  </table>";
  $(element).html(cadena);                     
}


function tablePadron(elemento,nameReport,columns='',count='') {
    var fills=[];
    if (columns!='') {
      for (var i = 0; i < columns; i++) {
        if (i==0) {
          fills.push({"width":"5%"});
        }else{
          fills.push({"width":"20%"});  
        }
        
      }
    }else{
      fills=[
            { "width": "5%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" }
          ];
    }

  var lengthMenu="Ver _MENU_ registros";
  if (count!='') {
    lengthMenu="Ver _MENU_ registros de <strong>"+count+"</strong> Encontrados";
  }
  table=$(elemento).removeAttr('width').DataTable({
            "columns": fills,
            "language": {
                        "emptyTable":     "No hay datos disponibles en la tabla.",
                        "info":           "Viendo _END_ de _TOTAL_ registros"/*"Mostrando _START_ al _END_ de _TOTAL_ registros"*/,
                        "infoEmpty":      "Mostrando 0 registros de un total de 0.",
                        "infoFiltered":     "(filtrados de un total de _MAX_ registros)",
                        "lengthMenu":     lengthMenu,
                        "loadingRecords":   "Cargando...",
                        "processing":     "Procesando...",
                        "search":       "Buscar:",
                        "searchPlaceholder":  "Ingresa dato para filtrar",
                        "zeroRecords":      "No se encontraron resultados.",
                        "paginate": {
                                    "first":      "Primera",
                                    "last":       "Última",
                                    "next":       "Siguiente",
                                    "previous":     "Anterior"
                                },
                        "aria":{
                                "sortAscending":  "Ordenación Ascendente",
                                "sortDescending":   "Ordenación Descendente"
                            }
            },
            "lengthMenu": [[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
            "iDisplayLength":   10,
            "responsive":   true,
            scrollY:        "400px",
            scrollX:        true,
            scrollCollapse: true,
            columnDefs: [
                        { width: 10, targets: 0 }
                      ]
           
    });
  table.columns.adjust().draw();
  $('a.toggle-vis').on( 'click', function (e) {
      e.preventDefault();      
      var column = table.column( $(this).attr('data-column') );
      column.visible( ! column.visible() );
  });
}

function tableExport2(elemento,nameReport) {
    $(elemento).dataTable({
      //"pageLength": 9,
             "language": {
                        "emptyTable":     "No hay datos disponibles en la tabla.",
                        "info":           "Viendo _END_ de _TOTAL_ registros",
                        "infoEmpty":      "Mostrando 0 registros de un total de 0.",
                        "infoFiltered":     "(filtrados de un total de _MAX_ registros)",
                        "lengthMenu":     "Mostrar _MENU_ registros",
                        "loadingRecords":   "Cargando...",
                        "processing":     "Procesando...",
                        "search":       "Buscar:",
                        "searchPlaceholder":  "Ingresa dato para filtrar",
                        "zeroRecords":      "No se encontraron resultados.",
                        "paginate": {
                                    "first":      "Primera",
                                    "last":       "Última",
                                    "next":       "Siguiente",
                                    "previous":     "Anterior"
                                },
                        "aria":{
                                "sortAscending":  "Ordenación Ascendente",
                                "sortDescending":   "Ordenación Descendente"
                            }
            },
            "lengthMenu": [[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
            "iDisplayLength":   10,
            "responsive":   true,
            scrollY:        "300px",
            scrollX:        false,
            scrollCollapse: true,

            dom: 'Blfrtip',
            buttons: [{
                    extend: 'collection',
                    text: 'EXPORTAR',
                    orientation: 'landscape',
                    buttons: [{
                            text: 'Copiar',
                            extend: 'copy'
                        }, {
                            extend: 'pdf',
                            orientation: 'landscape',
                            pageSize: 'LEGAL',
                            title:'REPORTE_'+nameReport
                        },{
                            extend:'excelHtml5',
                            filename:'REPORTE_'+nameReport,
                            title:'REPORTE_'+nameReport
                        },{
                            extend:'csvHtml5',
                            filename:'REPORTE_'+nameReport
                        },{
                          extend: 'print',
                          text: 'Impimir Datos',
                          exportOptions: {
                                          modifier: {
                                                    selected: null
                                                    }
                                        },
                          filename:'REPORTE_'+nameReport,
                          title:'REPORTE_'+nameReport
                        },
                        //'csv', 'excel', 'print'
                    ]
                },
                {
                    extend: 'colvis',
                    text: 'Visor de columnas',
                    collectionLayout: 'fixed three-column'
                }
            ],
            "autoWidth": true,
            
           
            

            /*dom: 'Blfrtip',
            "language": {
                        "emptyTable":     "No hay datos disponibles en la tabla.",
                        "info":           "Viendo _END_ de _TOTAL_ registros",
                        "infoEmpty":      "Mostrando 0 registros de un total de 0.",
                        "infoFiltered":     "(filtrados de un total de _MAX_ registros)",
                        "lengthMenu":     "Mostrar _MENU_ registros",
                        "loadingRecords":   "Cargando...",
                        "processing":     "Procesando...",
                        "search":       "Buscar:",
                        "searchPlaceholder":  "Ingresa dato para filtrar",
                        "zeroRecords":      "No se encontraron resultados.",
                        "paginate": {
                                    "first":      "Primera",
                                    "last":       "Última",
                                    "next":       "Siguiente",
                                    "previous":     "Anterior"
                                },
                        "aria":{
                                "sortAscending":  "Ordenación Ascendente",
                                "sortDescending":   "Ordenación Descendente"
                            }
            },
            "lengthMenu": [[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
            "iDisplayLength":   10,
            "responsive":   true,
            scrollY:        "300px",
            scrollX:        false,
            scrollCollapse: true,
            buttons: [  
                        /*{
                          extend: 'collection',
                          text: 'Exportar',
                          buttons: [
                            {
                              extend:'excelHtml5',
                              filename:'REPORTE_'+nameReport,
                              title:'REPORTE_'+nameReport
                            },
                            {
                              extend: 'print',
                              text: 'Impimir Datos',
                              exportOptions: {
                                              modifier: {
                                                    selected: null
                                                    }
                                              },
                              filename:'REPORTE_'+nameReport,
                              title:'REPORTE_'+nameReport
                            }
                          ]
                        },
                        {
                          extend:'colvis'
                        },*/

                        /*{
                            extend:'excelHtml5',
                            filename:'REPORTE_'+nameReport,
                            title:'REPORTE_'+nameReport
                        },
                        /*{
                            extend:'csvHtml5',
                            filename:'REPORTE_'+nameReport
                        },
                        {
                          extend:'pdfHtml5',
                          filename:'REPORTE_'+nameReport
                        },*/
                        /*{
                            extend:'colvis'
                        },
                        {
                          extend: 'print',
                          text: 'Impimir Datos',
                          exportOptions: {
                                          modifier: {
                                                    selected: null
                                                    }
                                        },
                          filename:'REPORTE_'+nameReport,
                          title:'REPORTE_'+nameReport
                        }
                    ]*/
    });
}

function tableExport(elemento,nameReport) {
    table=$(elemento).DataTable({
            "language": {
                        "emptyTable":     "No hay datos disponibles en la tabla.",
                        "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "infoEmpty":      "Mostrando 0 registros de un total de 0.",
                        "infoFiltered":     "(filtrados de un total de _MAX_ registros)",
                        "lengthMenu":     "Mostrar _MENU_ registros",
                        "loadingRecords":   "Cargando...",
                        "processing":     "Procesando...",
                        "search":       "Buscar:",
                        "searchPlaceholder":  "Ingresa dato para filtrar",
                        "zeroRecords":      "No se encontraron resultados.",
                        "paginate": {
                                    "first":      "Primera",
                                    "last":       "Última",
                                    "next":       "Siguiente",
                                    "previous":     "Anterior"
                                },
                        "aria":{
                                "sortAscending":  "Ordenación Ascendente",
                                "sortDescending":   "Ordenación Descendente"
                            }
            },
            "lengthMenu": [[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
            "iDisplayLength":   10,
            "responsive":   true,
            buttons: [
                        {
                            extend:'excelHtml5',
                            filename:'REPORTE_'+nameReport
                        },
                        {
                            extend:'csvHtml5',
                            filename:'REPORTE_'+nameReport
                        },
                        {
                            extend:'colvis'
                        }
                    ],
    /*dom: 'Bfrtip',
        "lengthMenu": [[20, 100, 200, -1], [20, 100, 200, "Todos"]]*/
    });

    /*$(elemento+' tbody').on( 'click', '', function () {
      table
          .row( $(this).parents('tr') )
          .remove()
          .draw();
    } );*/
    /*$(elemento).dataTable({
    
      dom: 'Bfrtip',
        "lengthMenu": [[20, 100, 200, -1], [20, 100, 200, "Todo"]
      ],
     
        buttons: [
        {
          extend:'excelHtml5',
          filename:'REPORTE_'+nameReport
        },
        {
          extend:'csvHtml5',
          filename:'REPORTE_'+nameReport
        },
        /*{
          extend:'pdfHtml5',
          filename:'REPORTE_'+nameReport
        }*/

      /*],
      "language": {
            "emptyTable":     "No hay datos disponibles en la tabla.",
            "info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 registros de un total de 0.",
            "infoFiltered":     "(filtrados de un total de _MAX_ registros)",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords":   "Cargando...",
            "processing":     "Procesando...",
            "search":       "Buscar:",
            "searchPlaceholder":  "Ingresa dato para filtrar",
            "zeroRecords":      "No se encontraron resultados.",
            "paginate": {
                    "first":      "Primera",
                    "last":       "Última",
                    "next":       "Siguiente",
                    "previous":     "Anterior"
                  },
            "aria":{
                    "sortAscending":  "Ordenación Ascendente",
                    "sortDescending":   "Ordenación Descendente"
            }
      },
      "lengthMenu": [[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
      "iDisplayLength":   10,
      "responsive":   true,
    });*/

    /*$('#tbUsers').DataTable( {
          "language": {
                  "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
          "lengthMenu": [[5 , 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
          "iDisplayLength":   10,
          "responsive":   true,
          dom: 'Bfrtip',
            "lengthMenu": [[20, 100, 200, -1], [20, 100, 200, "Todo"]
            ],
          buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ]
      } );*/
        
}

function settingsTable() {
    $('.buttons-colvis').css('margin-left','2px');
    $('.buttons-colvis').removeClass('buttons-collection btn-secondary').addClass(' btn-outline-secondary');
    /*$('.buttons-excel').append('<i class="fa fa-file-excel-o" aria-hidden="true" style="padding-left:10%;"></i>');
    $('.buttons-excel').removeClass('dt-button buttons-excel buttons-html5').addClass('btn btn-outline-secondary');
    $('.buttons-csv').append('<i class="fa fa-file" aria-hidden="true" style="padding-left:10%;"></i>');
    $('.buttons-pdf').append('<i class="fa fa-file-pdf-o" aria-hidden="true" style="padding-left:10%;"></i>');

    $('.buttons-colvis').css('margin-left','2px');
    $('.buttons-colvis').removeClass('dt-button buttons-excel buttons-html5').addClass('btn btn-outline-secondary');
    $('.buttons-colvis').html('<span>Ver Columnas</span>');

    $('.buttons-print').css('margin-left','2px');
    $('.buttons-print').removeClass('dt-button buttons-excel buttons-html5').addClass('btn btn-outline-secondary');
    $('.buttons-print').append('<i class="fa fa-print" aria-hidden="true" style="padding-left:10%;"></i>');
  //$("#tbUsers_filter").addClass('col-2 offset-4');
  //$("#tbUsers_filter").attr("align","right");
  //$("#tbUsers_paginate").addClass('right_ok');
  //$("#tbUsers_paginate").attr("align","right");*/
}


  /*
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function() {scrollFunction()};
  
  function scrollFunction() {
    if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
        document.getElementById("myBtn").style.display = "block";
        //document.getElementById("myBtn2").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
        //document.getElementById("myBtn2").style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }*/
