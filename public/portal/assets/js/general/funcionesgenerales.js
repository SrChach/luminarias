/*var table;
var colorsBars=['#2BB273','#28A6DE'];
var colorsArea=[colorsBars[1],colorsBars[0]];*/

function checaValorDefault(cadena){
    if(cadena == null || cadena == 'null'){
        cadena = "";
    }
    return cadena;
}
function separa(str) {
  if (parseInt(str)) {
    if(str != null){
        var string  = str.toString();
        var res = string.split("");
        var cuantos = res.length;
        var regresa = "";
        if(cuantos<=3){
          regresa= str;
        }else
        if(cuantos==4){
          regresa = res[0] + "," + res[1] + res[2] +  res[3];
        }else
        if(cuantos == 5){
          regresa = res[0] + res[1] + "," + res[2] +  res[3] + res[4];
        }else
        if(cuantos == 6){
             regresa = res[0] + res[1] + res[2] + "," + res[3] + res[4] + res[5];
        }else
        if(cuantos == 7){
             regresa = res[0] + ","  + res[1] + res[2] + res[3] + "," + res[4] + res[5] + res[6];
        }else
        if (cuantos>7) {
          regresa=string;
        }
        return regresa;
    }
  }
  else{
    return str;
  }
}

function load(ok=true){

  if (ok) {
    $('#modalCargaPagina').fadeIn().modal('show');
  }else{
    $('#modalCargaPagina').fadeOut().modal('hide');
                //hide the modal
    //$('body').removeClass('modal-open');
                //modal-open class is added on body so it has to be removed
    //$('.modal-backdrop').remove();

    $('#modalCargaPagina').hide('slow');
  }
}

function showAlertInwindow(elemento,tipo,mensaje) {
    var msj="<div class='col-4 offset-4 alert alert-"+tipo+"'>"+mensaje+"</>"
    $(elemento).html(msj);
}

function showError(err) {
  $('#titlePage').html('ERROR !');
  showAlertInwindow('#contentBody','warning',err.message);
  console.log(err);
}

function cancel(action){
    if (action=='') {//close popover;
        $('[data-toggle="popover"]').popover('hide');
    }
    if (action==1) {//deleteAccount();
        $('[data-toggle="popover"]').popover('hide');
        notificar('NOTIFICACIÓN! ','PUEDE ELIMINAR CUENTAS CUANDO LO REQUIERÁ. RECUERDE QUE LAS CUENTAS ELIMINADAS NO SE PODRAN RECUPERAR.','info');
    }
}

function buildOptions(datas,index,value,check=null) {
    var options="<option value=''>SELECCIONE ...</option>";
    let selected = "";
    for (var i = 0; i < datas.length; i++) {
        selected = "";
        if (datas[i][value]==check) {
            selected = "selected";
        }
        options+="<option value='"+datas[i][value]+"'"+selected+">"+datas[i][index]+"</option>";
    }
    return options;
}
function href($option,$index) {
  location.href = "?view="+$option+"&index="+$index;
}

function onlyNumber(evt,element) {
  var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
        $(element).addClass('is-invalid');
        $('#FNumbers').show();
        return false;
    }
    $('#FNumbers').hide();
    $(element).removeClass('is-invalid');
    return true;

}

function onlyLetter(evt,element) {
       evt = (evt) ? evt : event;
       var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
          ((evt.which) ? evt.which : 0));
       if (charCode > 31 && charCode !=32 && (charCode < 65 || charCode > 90) && (charCode < 97 )) {
            $(element).addClass('is-invalid');
            $('#FLetters').show();
            return false;
       }
       $('#FLetters').hide();
        $(element).removeClass('is-invalid');
       return true;
}

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function capitalLetter(str) {
  if (str==null) str='null';
  str = str.toLowerCase();
  return str.charAt(0).toUpperCase() + str.slice(1);
}
function upperCase(element){
    $(element).on("keyup",function (){
      s=this.selectionStart;
      e=this.selectionEnd;
      a=this.value.split(" "),b="";
      for (n in a) {
        b+=" "+a[n].substr(0,1).toUpperCase()+a[n].substr(1).toUpperCase();
      }
      this.value=b.substr(1);
      this.selectionStart=s;
      this.selectionEnd=e;
    });
}

function ValidafileType(tField,iType) {
  file=tField.value;
    if (iType==1) {
        extArray = new Array(".gif",".jpg",".png",".jpeg",".jpeeg",".jpej");
        }
    if (iType==2) {
        extArray = new Array(".swf");
    }
    if (iType==3) {
        extArray = new Array(".exe",".sit",".zip",".tar",".swf",".mov",".hqx",".ra",".wmf",".mp3",".qt",".med",".et");
    }
    if (iType==4) {
        extArray = new Array(".mov",".ra",".wmf",".mp3",".qt",".med",".et",".wav");
    }
    if (iType==5) {
        extArray = new Array(".html",".htm",".shtml");
    }
    if (iType==6) {
        extArray = new Array(".xls",".xlsx");
    }
    if (iType==7) {
        extArray = new Array(".zip");
    }
    if (iType==8) {
        extArray = new Array(".pdf");
    }
    allowSubmit = false;

    if (!file) return;
    while (file.indexOf("\\") != -1) file = file.slice(file.indexOf("\\") + 1);
    ext = file.slice(file.indexOf(".")).toLowerCase();
    for (var i = 0; i < extArray.length; i++) {
            if (extArray[i] == ext) {
            allowSubmit = true;
            break;
            }
    }
    if (allowSubmit) {
      return allowSubmit;
    } else {
      tField.value="";
      alert("Usted solo puede subir archivos con extensiones " + (extArray.join(" ")) + "\nPor favor seleccione otro archivo");
      return false;
    }
}

function Files(url,inputFile='',opcion,_t,org='',file='',rename='',tipo='') {

  var formData = new FormData();
  var json;
  alert(tipo);

  if (file=='') {
    formData.append($(inputFile).attr("name"), $(inputFile)[0].files[0]);
    formData.append("index",$(inputFile).attr("name"));
    formData.append("rename",rename);
    formData.append("tipo",tipo);
  }else{
    formData.append("file",file);
  }
  formData.append("csrf_token",_t);
  formData.append("org",org);
  formData.append("action",opcion);

  $.ajax({
      url: url,
      type: 'POST',
      data: formData,
      dataType: "json",
      cache: false,
      contentType: false,
      processData: false,
      async: false,
      complete: function(res) {
          try {
              json = JSON.parse(res.responseText);
          } catch (e) {
              json = res.responseText;
          }
      }
  });
return json;
}

function serialize( element, serialize_method='' )
{
    var result=null;

    if (serialize_method=='array') {
      result = $(element+' :input').serializeArray();
    }else{
      result = $(element+' :input').serialize();
    }

    return result;
}



function complementosRenderTable(index,id,title='',idP='',org=''){
  var complete=null;
  if (org=='') {
    complete='callNewgrafica("'+index+'",'+id+',"'+title+'",'+idP+');';
  }else{
    complete=org+'("'+index+'","'+id+'","'+title+'",'+idP+');';//org+'("'+index+'",'+id',"'+title'",'+idP+');';
  }
  if (idP=='') {
    complete=org+'("'+index+'","'+id+'","'+title+'");';
  }
  return complete;
}

function renderTable(element,data,idP='',jump='',exeption='',idtable='',onclick='',org='',modalidad='') {
  //console.log(data);
  $(element).html('');
  var key=null;
  var keys=null;
  var click='';

  if (jump=='' && jump!=false) {jump=4}
  if (modalidad==1) {
    keys = Object.keys(data[0]);
    key=keys;
  }else{
    if (org=='') {
      //keys = Object.keys(data[idP]);
      //console.log(data[0]);
      keys = Object.keys(data[0]);
    }else{
      keys = Object.keys(data[0]);
    }
    key=keys;
  }

  if (org.org!='') {
      click=org.click;
  }

  if (exeption!=0||exeption!='') {
    key=[];
    for (var i = exeption; i < keys.length; i++) {
      key.push(keys[i]);
    }
  }

  if (idtable=='') {
    idtable='table-dinamic';
  }
  var index=null,title=null;
  if (onclick=='subdelegacion') {
    index='ID_DEL';
    title='DELEGACIÓN';
  }
  if (onclick=='municipio') {
    index='ID_SUB';
    title='SUBDELEGACIÓN';
  }
  if (onclick=='localidad') {
    index='CVE_MUN';
    title='MUNICIPIO';
  }



  var totales = [];

  var cadena = "<table id='"+idtable+"' class='table table-sm table-bordered table-hover' >\
                                    <thead class='thead-green fixed'>\
                                        <tr><th>#</th>";
                                        $.each(key,function (i,item) {
                                          if (item=='DELEGACIÓN') {
                                            item='OFICINA REGIONAL';
                                          }
                                          if (item=='SUBDELEGACIÓN') {
                                            item='OFICINA SUBREGIONAL';
                                          }
                                          cadena += "<th>"+item+"</th>";
                                            if(i>=jump){
                                                totales[i] = 0;
                                            }
                                        });
                              cadena += "</tr>\
                                    </thead>\
                                    <tbody>";
  var numNotif = 0;
  var numExp = 0;
  var tot_visitas = 0;
  var value=null;
  var tr="<tr>";
  var percent = " (100%)";
                      for(var i = 0;i < data.length;i++){
                        if (onclick!='') {
                          tr="<tr onclick='"+complementosRenderTable(onclick,data[i][index],data[i][title],idP,click)+"'>"
                        }
                        cadena += tr+"<th>"+(i+1)+"</th>";
                                for (var j = 0; j < key.length; j++) {
                                  value=data[i][key[j]];
                                  if (parseInt(value)) {
                                    if(key[j]!='AVANCE'){
                                      percent="";
                                    }else{
                                      percent = " (100%)";
                                    }
                                    cadena += "<td data-filter='"+((typeof data[i]['IDUSUARIO'] !==  undefined) ? data[i]['IDUSUARIO']: '')+"'>"+separa(value)+percent+"</td>";
                                  }else{
                                    cadena += "<td data-filter='"+((typeof data[i]['IDUSUARIO'] !== undefined ) ? data[i]['IDUSUARIO'] : '')+"'>"+value+"</td>";
                                  }

                                    if(j>=jump){
                                        totales[j] = Number(totales[j]) + Number(data[i][key[j]]);
                                    }
                                }
                        cadena += "</tr>";
                      }

                    cadena += "</tbody>";

                    if (jump!=false) {
                          cadena+="<tfoot class='thead-green'>\
                                    <tr>\
                                      <th colspan='"+(jump+1)+"'>Totales</th>";
                                    for (var j = jump; j < (totales.length); j++) {
                                        cadena += "<th>"+separa(totales[j])+"</th>";
                                    }
                      }
                          cadena += "</tr>\
                                    </tfoot>\
                                </table>";
  $(element).html(cadena);
}


function tablePadron(elemento,nameReport,columns='',count='') {
    var fills=[];
    if (columns!='') {
      for (var i = 0; i < columns; i++) {
        if (i==0) {
          fills.push({"width":"5%"});
        }else{
          fills.push({"width":"20%"});
        }

      }
    }else{
      fills=[
            { "width": "5%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" }
          ];
    }

  var lengthMenu="Ver _MENU_ registros";
  if (count!='') {
    lengthMenu="Ver _MENU_ registros de <strong>"+count+"</strong> Encontrados";
  }
  table=$(elemento).removeAttr('width').DataTable({
            "columns": fills,
            "language": {
                        "emptyTable":     "No hay datos disponibles en la tabla.",
                        "info":           "Viendo _END_ de _TOTAL_ registros"/*"Mostrando _START_ al _END_ de _TOTAL_ registros"*/,
                        "infoEmpty":      "Mostrando 0 registros de un total de 0.",
                        "infoFiltered":     "(filtrados de un total de _MAX_ registros)",
                        "lengthMenu":     lengthMenu,
                        "loadingRecords":   "Cargando...",
                        "processing":     "Procesando...",
                        "search":       "Buscar:",
                        "searchPlaceholder":  "Ingresa dato para filtrar",
                        "zeroRecords":      "No se encontraron resultados.",
                        "paginate": {
                                    "first":      "Primera",
                                    "last":       "Última",
                                    "next":       "Siguiente",
                                    "previous":     "Anterior"
                                },
                        "aria":{
                                "sortAscending":  "Ordenación Ascendente",
                                "sortDescending":   "Ordenación Descendente"
                            }
            },
            "lengthMenu": [[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
            "iDisplayLength":   10,
            "responsive":   true,
            scrollY:        "400px",
            scrollX:        true,
            scrollCollapse: true,
            columnDefs: [
                        { width: 10, targets: 0 }
                      ]

    });
  table.columns.adjust().draw();
  $('a.toggle-vis').on( 'click', function (e) {
      e.preventDefault();
      var column = table.column( $(this).attr('data-column') );
      column.visible( ! column.visible() );
  });
}

function tableExport2(elemento,nameReport='',columns='',count='',all=10) {
    var lengthMenu="Ver _MENU_ registros";
    var info = "_END_ de _TOTAL_ Registros";
    if (count!='') {
      lengthMenu="Ver _MENU_ registros de <strong>"+count+"</strong> Encontrados";
      info = "_END_  <strong>DE UNA MUESTRA DE "+count+"</strong> Registros.";
    }

    let config = {
      //"pageLength": 5,
      bAutoWidth: false ,
              "pagingType": "full_numbers",
              "language": {
                        "emptyTable":         "No hay datos disponibles en la tabla.",
                        "info":               info,
                        "infoEmpty":          "Mostrando 0 registros de un total de 0.",
                        "infoFiltered":       "(filtrados de un total de _MAX_ registros)",
                        "lengthMenu":         lengthMenu,
                        "loadingRecords":     "Cargando...",
                        "processing":         "Procesando...",
                        "search":             "Buscar:",
                        "searchPlaceholder":  "Ingresa filtro",
                        "zeroRecords":        "No se encontraron resultados.",
                        "paginate": {
                                    "first":      "Primera",
                                    "last":       "Última",
                                    "next":       "Siguiente",
                                    "previous":     "Anterior"
                                },
                        "aria":{
                                "sortAscending":  "Ordenación Ascendente",
                                "sortDescending":   "Ordenación Descendente"
                            }
            },
            "lengthMenu":       [[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
            "iDisplayLength":   all,
            "responsive":       true,
            "drawCallback": function ( settings ) {
                                $('[data-toggle="tooltip"]').tooltip();
                               $('.click').tooltip({trigger: "click"});
                            },
            dom:  "<'row'<'col-sm-12 col-md-12 text-center mb-4'B>>"+
                  "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4 optionsTable'><'col-sm-6 col-md-4'f>>" +
                  "<'row'<'#content-tb.col-sm-12 col-md-12 table-responsive boxTable'tr>>" +
                  "<'row'<'col-sm-12 col-md-3'i><'col-sm-12 col-md-9'p>>",
            "fixedHeader": true,
            buttons: [{
                        extend: 'collection',
                        text: 'EXPORTAR <i class="glyphicon glyphicon-export></i>',
                        orientation: 'landscape',
                        className: 'btn btn-secondary btn-sm',
                        buttons: [/*{
                            text: '<i class="right fa fa-files-o" aria-hidden="true"></i> Copiar',
                            extend: 'copy',
                            footer: true,
                            title:'REPORTE '+nameReport,
                            exportOptions: {
                              columns: [ 0, ':visible' ]
                            }
                        },*/ {
                            extend: 'pdf',
                            download: 'open',// or open
                            /*customize : function(doc){
                              doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                          },*/
                            text: '<i class="right fa fa-file-pdf-o" aria-hidden="true"></i> Pdf',
                            orientation: 'landscape',// [portrait,landscape]
                            pageSize: 'A4', // or A3, A4, A5, LEGAL, LETTER or TABLOID
                            title:'REPORTE '+nameReport,
                            footer: true,
                            exportOptions: {
                              columns: ':visible'
                            },
                            customize: function ( doc ) {
                                // Splice the image in after the header, but before the table
                                doc.content.splice( 1, 0, {
                                    margin: [ 0, 0, 0, 12 ],
                                    alignment: 'center',
                                    image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAHCAYAAAAf6f3xAAAAGElEQVQokWP4PwCAgYGBgWHU4lGLh53FAK5tKQJ+yjBLAAAAAElFTkSuQmCC'
                                } );
                                // Data URL generated by http://dataurl.net/#dataurlmaker
                            }
                        },{
                            extend:'excelHtml5',
                            text:'<i class="right fa fa-file-excel-o" aria-hidden="true"></i> Excel',
                            filename:'REPORTE '+nameReport,
                            title:'REPORTE '+nameReport,
                            footer: true,
                            exportOptions: {
                              columns: ':visible'
                            },
                            customizeData: function (data) {
                                console.log(data);
                                for (var i = 0; i < data.body.length; i++) {
                                    for (var j = 0; j < data.body[i].length; j++) {
                                        data.body[i][j] = '\u200C' + data.body[i][j];
                                    }
                                }
                            }
                        },/*{
                            extend:'csvHtml5',
                            charSet: "utf-8",
                            text:'<i class="right fa fa-file" aria-hidden="true"></i> Csv',
                            filename:'REPORTE '+nameReport,
                            footer: true,
                            exportOptions: {
                              columns: ':visible'
                            }
                        },*/{
                          extend: 'print',
                          text: '<i class="right fa fa-print" aria-hidden="true"></i> Impimir Datos',
                          footer: true,
                          exportOptions: {
                                          modifier: {
                                                    selected: null
                                                    }
                                        },
                          filename:'REPORTE '+nameReport,
                          title:'REPORTE '+nameReport
                        },
                    ]
                },
                {
                    extend: 'colvis',
                    text: 'Visor de columnas',
                    className:'btn btn-secondary btn-sm',
                    collectionLayout: 'fixed three-column'
                }/*,
                {
                  extend:'pageLength',
                  text: 'Ver filas:'
                }*/

            ]




            /*dom: 'Blfrtip',
            "language": {
                        "emptyTable":     "No hay datos disponibles en la tabla.",
                        "info":           "Viendo _END_ de _TOTAL_ registros",
                        "infoEmpty":      "Mostrando 0 registros de un total de 0.",
                        "infoFiltered":     "(filtrados de un total de _MAX_ registros)",
                        "lengthMenu":     "Mostrar _MENU_ registros",
                        "loadingRecords":   "Cargando...",
                        "processing":     "Procesando...",
                        "search":       "Buscar:",
                        "searchPlaceholder":  "Ingresa dato para filtrar",
                        "zeroRecords":      "No se encontraron resultados.",
                        "paginate": {
                                    "first":      "Primera",
                                    "last":       "Última",
                                    "next":       "Siguiente",
                                    "previous":     "Anterior"
                                },
                        "aria":{
                                "sortAscending":  "Ordenación Ascendente",
                                "sortDescending":   "Ordenación Descendente"
                            }
            },
            "lengthMenu": [[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
            "iDisplayLength":   10,
            "responsive":   true,
            scrollY:        "300px",
            scrollX:        false,
            scrollCollapse: true,
            buttons: [
                        /*{
                          extend: 'collection',
                          text: 'Exportar',
                          buttons: [
                            {
                              extend:'excelHtml5',
                              filename:'REPORTE_'+nameReport,
                              title:'REPORTE_'+nameReport
                            },
                            {
                              extend: 'print',
                              text: 'Impimir Datos',
                              exportOptions: {
                                              modifier: {
                                                    selected: null
                                                    }
                                              },
                              filename:'REPORTE_'+nameReport,
                              title:'REPORTE_'+nameReport
                            }
                          ]
                        },
                        {
                          extend:'colvis'
                        },*/

                        /*{
                            extend:'excelHtml5',
                            filename:'REPORTE_'+nameReport,
                            title:'REPORTE_'+nameReport
                        },
                        /*{
                            extend:'csvHtml5',
                            filename:'REPORTE_'+nameReport
                        },
                        {
                          extend:'pdfHtml5',
                          filename:'REPORTE_'+nameReport
                        },*/
                        /*{
                            extend:'colvis'
                        },
                        {
                          extend: 'print',
                          text: 'Impimir Datos',
                          exportOptions: {
                                          modifier: {
                                                    selected: null
                                                    }
                                        },
                          filename:'REPORTE_'+nameReport,
                          title:'REPORTE_'+nameReport
                        }
                    ]*/
    }

    table = $(elemento).removeAttr('width').DataTable(config);
    $('.dt-buttons').removeClass('btn-group');
    return table;
    /*table.columns.adjust().draw();
    $('.dt-buttons').removeClass('btn-group');
    return table;*/
}


function settingsTable(tb_id='') {
    $( "div.dataTables_filter" ).find( "input" ).css( "width", "70%" );
    $("#content-tb").find("table").css("width","100%");
    if (tb_id!='') {
      $('#'+idTable).css("width","100%");
    }
    //$("#content-tb").addClass("table-responsive");

    //$('.buttons-colvis').css('margin-left','2px');
    /*$( "div.dataTables_filter" ).find( "input" ).css( "width", "70%" );
    $(".dataTables_scrollHeadInner").css("width", "100%");
    $( "div.dataTables_scrollHeadInner" ).find( "table" ).css( "width", "100%" );
    $( "div.dataTables_scrollBody" ).find( "table" ).css( "width", "100%" );
    $(".dataTables_scrollFootInner").css("width", "100%");
    $("div.dataTables_scrollFootInner").find("table").css( "width", "100%" );*/

}



  function topFunction() {
    $('body,html').animate({scrollTop : 0}, 1000);
    $('#myBtn').tooltip('hide');
    return false;
  }

  $(function () {
    $(window).scroll(function(){
      if ($(this).scrollTop() > 30) {
          $('#myBtn').fadeIn().tooltip();
      } else {
          $('#myBtn').fadeOut();
      }
    });
  });








function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function contruyeOpcion_separador(arreglo,selected = 0){
    //recibe un arreglo del tipo arreglo[0] = {value: valor, nombre:nombre,separador:separador}
    var separador = "";
    var cadena = "";
    var iniciado = 0;
    for (var i = 0; i < arreglo.length ; i++) {
        if(arreglo[i]['VALUE']!='null' && arreglo[i]['VALUE']!=null){
            if(separador != arreglo[i]['SEPARADOR']){
                separador = arreglo[i]['SEPARADOR'];
                if(iniciado == 1){
                    cadena += "</optgroup>";
                }
                cadena += "<optgroup label='"+arreglo[i]['SEPARADOR']+"'>";
            }
            var select = "";
            if(arreglo[i]['VALUE'] == selected){
                select = "selected";
            }
            cadena += "<option value='"+arreglo[i]['VALUE']+"' "+select+">"+arreglo[i]['NOMBRE']+"</option>";
        }
    }
    cadena += "</optgroup>";
    return cadena;
}

function ZoomImg($) {//Recibe como parametro jQuery->($)
  $('.img-zoom').elevateZoom({
    /*zoomType: "lens",// Zoom dentro de la misma imagen con circulo
    lensShape : "round",
    lensSize    : 100,
    easing : true,
    scrollZoom : true//acercar y alejar con scrooll*/
    cursor: "pointer",
    zoomWindowFadeIn: 1000,
    zoomWindowFadeOut: 1000,
    easing : true,
    scrollZoom : true,
    zoomWindowWidth:200,
    zoomWindowHeight:260,
    loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
    /*easing : true,//coloca el Zoom dentro de la misma imagen
    scrollZoom : true,
    constrainType:"height",
    constrainSize:50,
    zoomType: "lens",
    containLensZoom: true,
    cursor: 'pointer'*/
  });
}


function makeQR(itemRender,value=null) {
  $('#load_QR').fadeIn('slow');
  if (value!=null) {
    new QRCode(itemRender,{
                            text: value,
                            width: 100,
                            height: 100
                          }
              );//
  }else{
    new QRCode(itemRender,'SIN DATO');//
  }
  $('#load_QR').fadeOut('slow');
}

function hrefCedula(cve) {//crear href a la vista de cedula
  var href="?view=cedula&indexs="+cve
  return href;
}

function hrefCuis(cve) {
  var href="?view=modules&act=consulta_cuis&index=1&folio="+cve;
  return href;
}

function hrefExpediente(cve) {
  var href="?view=validacion&foliocuis="+cve;
  return href;
}

$('html').on('click', function(e) {//OCULTA TOOLTIP AL DAR CLICK FUERA DE EL
  if (typeof $(e.target).data('original-title') == 'undefined') {
      $('.click').tooltip('hide');
  }
});



let cipher = salt => {
    let textToChars = text => text.split('').map(c => c.charCodeAt(0))
    let byteHex = n => ("0" + Number(n).toString(16)).substr(-2)
    let applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code)

    return text => text.split('')
        .map(textToChars)
        .map(applySaltToChar)
        .map(byteHex)
        .join('')
}

let decipher = salt => {
    let textToChars = text => text.split('').map(c => c.charCodeAt(0))
    let saltChars = textToChars(salt)
    let applySaltToChar = code => textToChars(salt).reduce((a,b) => a ^ b, code)
    return encoded => encoded.match(/.{1,2}/g)
        .map(hex => parseInt(hex, 16))
        .map(applySaltToChar)
        .map(charCode => String.fromCharCode(charCode))
        .join('')
}

function formEncode(obj) {
    var str = [];
    for(var p in obj)
    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    return str.join("&");
}

function Fetch(options) {
    let request = {
        /*headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },*/
        headers: {
            'Accept': 'text/html, */*; q=0.01',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        method: (options.data) ? 'POST' : (options.method) ? options.method : 'GET'
    };
    if (request.method!='GET'){
      request.body = (options.formData && options.formData==true) ? options.data : formEncode(options.data);
    }
    return new Promise((resolve,eject) => {
        fetch(options.route,request)
        .then((response) => response.json())
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((err) => {
            eject(err);
        })
    });
}

function serializeFormJSON (_this){
        var object = {};
        var array = _this.serializeArray();
        $.each(array, function () {
            if (object[this.name]) {
                if (!object[this.name].push) {
                    object[this.name] = [object[this.name]];
                }
                object[this.name].push(this.value || '');
            } else {
                object[this.name] = this.value || '';
            }
        });
        return object;
};

function renderChartBar (options) {
    let angle = 90;
    if (Object.keys(options.data[0]).length<5) {
        angle =90;
    }
    let view = window.location.href.split("?view=")[1];

    if (view == "inicio") {
        angle =0;
    }

    let config = {
        element: options.element,
        data: options.data,
        /*hoverCallback: function(index, options, content,row) {
            var aux= content.split("row-label'>");
            content = aux[0]+"row-label'>OFICINA REGIONAL: "+aux[1];
              $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
              return content;
          },*/
        xkey: options.xkey,
        ykeys: options.ykeys,
        stacked: (options.stack) ? options.stack : false,
        resize:true,
        redraw:true,
        xLabelAngle:angle,
        labels: (options.labels) ? options.labels : options.ykeys,
        fillOpacity: 0.6,
        hideHover: 'auto',
        behaveLikeLine: true,
        pointFillColors:['#ffffff'],
        pointStrokeColors: ['black'],
        //colors: ['#9d2449', '#C0C0C0']
    };
    if (options.colorBar) {
        if (Array.isArray(options.colorBar)) {
            config.barColors = options.colorBar;
            config.lineColors = options.colorBar;
        }else{
            config.barColors= (row, series, type) =>{return options.colorBar;}
        }

    }
    if (options.hovercustom) {
        config.hoverCallback = options.hovercustom;
    }
    let Morris = window.Morris;

    if (!options.type || options.type ==='bar') return Morris.Bar(config);
    if (options.type==='area') return Morris.Area(config);
    if (options.type==='line') return Morris.Line(config);
    if (options.type==='donut') {
        config.colors = (options.colorBar) ? options.colorBar : ['#0b62a4', '#C0C0C0'];
        if (options.formatter) {
            config.formatter = (x, row) =>{
                return x + "%";
            }
        }
        return Morris.Donut(config);
    }
}

function uploadMultiFile(url='/upload',formData) {

    return new Promise((resolve,eject) => {
      fetch(url,{
          method: 'POST',
          body: formData
      })
        .then((response) => response.json())
        .then((responseJson) => {
          resolve(responseJson);
        })
        .catch((err) => {
          eject(err);
        })
    });
}
