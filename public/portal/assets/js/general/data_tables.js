function tabla() {
	//console.log('10');
  $('#example').DataTable({

   dom: 'Bfrtip',
        "lengthMenu": [[20, 100, 200, -1], [20, 100, 200, "Todo"]
      ],
      buttons: [
        {
          extend:'excelHtml5',
          filename:'Reporte GENERAL'
        }
      ],

      
  "language": {
      		"emptyTable":			"No hay datos disponibles en la tabla.",
			"info":		   			"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"infoEmpty":			"Mostrando 0 registros de un total de 0.",
			"infoFiltered":			"(filtrados de un total de _MAX_ registros)",
			"lengthMenu":			"Mostrar _MENU_ registros",
			"loadingRecords":		"Cargando...",
			"processing":			"Procesando...",
			"search":				"Buscar:",
			"searchPlaceholder":	"Ingresa dato para filtrar",
			"zeroRecords":			"No se encontraron resultados.",
			"paginate": {
				"first":			"Primera",
				"last":				"Última",
				"next":				"Siguiente",
				"previous":			"Anterior"
				},
			"aria":{
				"sortAscending": 	"Ordenación Ascendente",
				"sortDescending": 	"Ordenación Descendente"
			}
    },
    "lengthMenu":	[[5, 10, 15 ,20, 25, 50, 100,-1], [5, 10, 15,20, 25, 50,100, "Todos"]],
	"iDisplayLength": 	10,
    "responsive":   true,
    "paging":   false,//quitar paginación
   });
}

function tabla2() {
        $('#example2').DataTable({
    "language": {
      "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
    },
    "lengthMenu":	[[5, 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
	"iDisplayLength": 	10,
	"responsive":   true
  });
}

function tablaPersonal(elemento) {
	
	$(elemento).DataTable({
    "language": {
      "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
    },
    "lengthMenu":	[[5, 7 , 10, 20, 25, 50, -1], [5, 10, 20, 25, 50, "Todos"]],
	"iDisplayLength": 	7,
	"responsive":   true,
	"paging":   false
  });
}