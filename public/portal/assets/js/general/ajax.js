/*var objet;
var res;
var url="./controller/MethodController.php";
var urlImgProfile="./Storage/Evidencias/imgProfiles/";
var table;
var colorsBars=['#2BB273','#28A6DE'];
var colorsArea=[colorsBars[1],colorsBars[0]];*/


function ajax(data,url) {  //Funcion estandar ajax
    var regresa;
    $.ajax({
        type: "POST",
        dataType: "HTML",
        url: url,
        data: data,
        async: false,
        success: function (respuesta) {
            regresa = respuesta;
        },
        error: function (e) {
            console.log(e.message);
        }
    });
    return regresa;
    //console.log(data);
}

function ajaxCallback(data,url,callback = null) {  //Funcion estandar ajax
    if(callback){
        var async = true;
    }else{
        var async = false;
    }
    var regresa;
    $.ajax({
        type: "POST",
        dataType: "HTML",
        url: url,
        data: data,
        async: async, 
        success: function (respuesta) {
            if(callback){
              callback(respuesta);
            }else{
                regresa = respuesta;
            }
        },
        error: function (e) {
            console.log(e.message);
            
        }
    });
    if(callback){
    }else{
        return regresa;
    }
}

function checaValorDefault(cadena){
    if(cadena == null || cadena == 'null'){
        cadena = "";
    }
    return cadena;
}




function soloNumeros(id){
    cantidad =  $("#"+id).val().replace(/[^\d,.]/g, '');
    $("#"+id).val( cantidad );
}



function contruyeOpcion(arreglo,selected = 0){
    //recibe un arreglo del tipo arreglo[0] = {value: valor, nombre:nombre}
    var cadena = "";
    for (var i = 0; i < arreglo.length ; i++) {
        if(arreglo[i]['VALUE']!='null' && arreglo[i]['VALUE']!=null){
            var select = "";
            if(arreglo[i]['VALUE'] == selected){
                select = "selected";
            }
            cadena += "<option value='"+arreglo[i]['VALUE']+"' "+select+">"+arreglo[i]['NOMBRE']+"</option>";
        }
    }
    return cadena;
}

//Conversion de un numero a formato moneda
function number_format(amount, decimals=null) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));

    decimals = decimals || 0; 
    
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}



function extraeID(cadena){
    var separa = cadena.split("_");
    return separa[1];
}













