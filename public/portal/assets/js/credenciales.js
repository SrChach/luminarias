    var next='subdelegacion',   back='delegacion',idTable='table-dinamic',element='graficaAvance',evento=null,content='#tableAvances', 
    elementTitle='',    content_Main='#content-Delegacion',     content_Prev='#content-Delegacion';
    var action='',    idDel='',     idSub='',     idMun='',     idLoc='',    inicio='',    termino='';
    var title=null;
    /*CLICK´S DE RETORNO DE NIVELES */
    $('#btnBack').click(function () {//BOTÓN DEL NIVEL PRINCIPAR (DELEGACIONES)
    	backLevel(next,back);
    });
    $('#btnBackDelegacion').click(function () {//BOTÓN DEL SEGUNDO NIVEL (SUBDELEGACIONES)
    	backLevel('','delegacion');
    });
    $('#btnBackSubdelegacion').click(function () {//BOTÓN DEL NIVEL DE MUNICIPIOS
    	backLevel('','subdelegacion');
    });
    $('#btnBackMunicipio').click(function () {//BOTÓN DEL CUARTO NIVEL (LOCALIDADES)
    	backLevel('','municipio');
    });

    /*CLICK DEL BOTÓN FILTRO*/
    $('#filtro').click(function () {
    	alert('llamdo de filtro');
    });


    /*FUNCION PARA TRAER VALORES INICIALES (NOMBRE DE PROGRAMA)*/
    $(function () {
    	$("#modalCargaPagina").modal('show');
    	$('#btnBack').tooltip({title: 'RECARGAR'});
    	var url="./controller/GeneralesController.php"
    	objet={opcion:'generales',action:'getNamePrograma',csrf_token: _t,idPrograma:idP};
    	ajaxCallback(objet,url,function (respuesta){
    		try {
    			res = JSON.parse(respuesta);
    			if (res.CODIGO==true) {
    				$('#titlePage-2').html(": "+res.DATOS[0]['NOMBREPROGRAMA']);
                    Index();//LLAMA A LA FUNCIÑON PRINCIPAL
                }
                else{
                	showAlertInwindow('#contentBody','warning',res.DATOS);
                	$('#titlePage-2').html(': PROGRAMA NO EXISTE!');
                }
            }
            catch(err) {
            	$('#titlePage-2').html(': ERROR !');
            	showAlertInwindow('#contentBody','warning',err.message);
            	console.log(err);
            	$("#modalCargaPagina").modal('hide');
            }

        });
    });

    /*FUNCIÓN PARA TRAER DATOS DE LAS GRAFICAS Y TABLAS (GENERALES)*/
    function Index() {
    	$("#modalCargaPagina").modal('show');
    	$(content_Prev).fadeOut(500);
    	var url = "./controller/CredencialesController.php";
    	objet={opcion:'Credenciales',action:'getCredencialesDelegacion',csrf_token: _t,idP:idP};
    	ajaxCallback(objet,url,function (respuesta){
    		res = JSON.parse(respuesta);
    		if (res.CODIGO==true) {
                evento=res.EVENTOS[idP][0].IDTIPOEVENTO;
    			var data=buildData(res.DATOS,'ID_DEL','DELEGACION','CREDENCIALES');
    			$('#titleGrafica').html("DELEGACIONES.");
    			renderTable(content,data.data,idP,1,1,'',next,{org:'credenciales',click:'nextLevel'});
    			tableExport2('#'+idTable,'AVANCE-GENERAL(Credenciales)');
    			settingsTable();
    			renderGrafica(next,back,element,idP,data.data,'DELEGACIÓN',['AVANCE','RESTAN'],true);
    			$('#meta').html(separa(res.META[evento][0].META));
    		}else{
    			showAlertInwindow('#contentBody','warning',res.DATOS);
    			$('#titlePage-2').html(': '+capitalLetter(res.CODIGO)+' !');
                $('#modalCargaPagina').modal('hide'); 
                //hide the modal
                $('body').removeClass('modal-open'); 
                //modal-open class is added on body so it has to be removed
                $('.modal-backdrop').remove();
    		}
    	});
    }


    /*CONSTRUYE EL DATA (OBJECT) PRINCIPAL QUE SE PASA COMO PARA METRO A LA TABLA Y A LA GRAFICA*/
    function buildData(datas,index,key,apuntador,idP='') {
        console.log(datas);
    	var data = new Object(),  aux=null,   metaIn = 1000,  meta = 0,   avance = 0, resto = 0,  objects = [];

    	$.each(datas,function(i,item) {
    		resto = parseInt(item.META)-parseInt(item.CREDENCIALES);

    		if (key=='DELEGACION')  aux={ID_DEL:item[index],DELEGACIÓN:item[key],META:item.META,AVANCE:parseInt(item.CREDENCIALES),RESTAN: resto};
    		if (key=='SUBDELEGACION')  aux={ID_SUB:item[index],SUBDELEGACIÓN:item[key],META:item.META,AVANCE:parseInt(item.CREDENCIALES),RESTAN: resto};
    		if (key=='NOMBREMUN')  aux={CVE_MUN:item[index],MUNICIPIO:item[key],META:item.META,AVANCE:parseInt(item.CREDENCIALES),RESTAN: resto};
    		if (key=='NOMBRELOC')  aux={CVELOC:item[index],LOCALIDAD:item[key],META:item.META,AVANCE:parseInt(item.CREDENCIALES),RESTAN: resto};

    		meta = meta+parseInt(item.META);
    		objects.push(aux);
    	});

    	data.data = objects;
    	data.meta = meta;
    	return data;
    }

    /*FUNCIÇON PARA REGRESAR A NIVEL ANTERIOR*/
    function backLevel(next,back) {//PARAMETROS : next-> (SIGUIENTE NIVEL), back->  (NIVEL ANTERIOR)
    	if (next=='subdelegacion') {
    		Index();
    	}
    	if (back=='delegacion') {
    		content_Main='#content-Subdelegacion';
    		content_Prev='#content-Delegacion';
    	}
    	if (back=='subdelegacion') {
    		content_Main='#content-Municipio';
    		content_Prev='#content-Subdelegacion';
    	}
    	if (back=='municipio') {
    		content_Main='#content-Localidad';
    		content_Prev='#content-Municipio';
    	}
    	$(content_Prev).fadeIn(2500);
    	$(content_Main).fadeOut(500);
    }

    /*FUNCIÓN PARA LLAMAR AL SIGIENTE NIVEL*/
    var nextNivel=null;
    function nextLevel(next,id,title,idP) {// PARAMETROS: next-> (nivel que debe traer), id-> (id de nivel anterior(id_del)), title-> (titulo que se mostrara en al gun lado),idP-> (identificador de programa)
    	$("#modalCargaPagina").modal('show');
    	var xkey=null,ykeys=null;
    	var T=null;
    	if (next=='subdelegacion') {nextNivel='municipio'; action='getCredencialesSubDelegacion'; idDel=id; index='ID_SUB'; key='SUBDELEGACION'; title='DELEGACIÓN: '+ title;   content='#tableAvancesSubDelegacion',elementTitle='#titleGraficaSubdelegacion'; idTable='table-dinamic-Subdel'; element='graficaAvanceSubDelegacion';   xkey='SUBDELEGACIÓN';   content_Main='#content-Subdelegacion';  content_Prev='#content-Delegacion'; T='DELEGACIONES';
    	   $('#btnBackDelegacion').tooltip({title: '<- REGRESAR: '+ T});
        }

        if (next=='municipio') {nextNivel='localidad'; action='getCredencialesMunicipio'; idSub=id; index='CVEMUN'; key='NOMBREMUN'; title='SUBDELEGACIÓN: '+ title;    content='#tableAvancesMunicipio',elementTitle='#titleGraficaMunicipio'; idTable='table-dinamic-Municipio'; element='graficaAvanceMunicipio';   xkey='MUNICIPIO';    content_Main='#content-Municipio';
            content_Prev='#content-Subdelegacion';  T='SUBDELEGACIONES';
            $('#btnBackSubdelegacion').tooltip({title: '<- REGRESAR: '+ T});
        }

        if (next=='localidad') {nextNivel=''; action='getCredencialesLocalidad'; idMun=id; index='CVELOC'; key='NOMBRELOC'; title='MUNICIPIO: '+ title;     content='#tableAvancesLocalidad',elementTitle='#titleGraficaLocalidad'; idTable='table-dinamic-Localidad'; element='graficaAvanceLocalidad';   xkey='LOCALIDAD';    content_Main='#content-Localidad';
            content_Prev='#content-Municipio';  T='MUNICIPIOS';
            $('#btnBackMunicipio').tooltip({title: '<- REGRESAR: '+ T});
        }

        $(content).html('<div class="text-center">Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i></div>');
        $('#'+element).html('<div class="text-center">Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i></div>');

        var url = "./controller/CredencialesController.php";
        objet={opcion:'Credenciales',action:action,csrf_token: _t,idP:idP,idDel:idDel,idSub:idSub,idMun:idMun,idLoc:idLoc};
        ajaxCallback(objet,url,function (respuesta){
        	res = JSON.parse(respuesta);
        	if (res.CODIGO==true) {
        		var data=buildData(res.DATOS,index,key,'CREDENCIALES');
        		$(elementTitle).html(title);
                        renderTable(content,data.data,idP,1,1,idTable,nextNivel,{org:'credenciales',click:'nextLevel'});//CREA LA TABLA
                        tableExport2('#'+idTable,'AVANCE-');//TABLA DINÁMICA
                        settingsTable(idTable); //CONFIGURACION DE LA TABLA DINÁMICA
                        renderGrafica(nextNivel,back,element,idP,data.data,xkey,['AVANCE','RESTAN'],true);
                    }else{
                    	showAlertInwindow('#contentBody','warning',res.DATOS);
                    	$('#titlePage-2').html(': '+capitalLetter(res.CODIGO)+' !');
                    }
                    $("#modalCargaPagina").modal('hide');
                });

    }

/*FUNCIÓN QUE CREA LA GRÁFICA*/
function renderGrafica(next,back,element,idP='',data='',xkey='',s='',stack='') {
	$(content_Prev).fadeOut(500);
	$(content_Main).fadeIn(2500);

	back=back;
	$('#'+element).html('');
	$("#"+element).empty();
	if (stack=='') {
		stack=false;
	}
	var index=null,value=null,x=null;
	if (next=='subdelegacion') {    index='ID_DEL';     value='DELEGACIÓN';}
	if (next=='municipio') {    index='ID_SUB';     value='SUBDELEGACIÓN';}
	if (next=='localidad') {    index='CVE_MUN';    value='MUNICIPIO';}

	if (idP==3||idP==2) {
		var graficaAvanceDelegacion=new Morris.Bar({
			element: element,
			data: data,
			hoverCallback: function(index, options, content,row) {
                  //var params=[row.ID_DEL,idP];
                  content=renderHoverGrafica(next,row,idP,xkey);
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
              xkey: xkey,
              ykeys: ykey,
              stacked: stack,
              resize:false,
              xLabelAngle:45,
              labels: [],
              barColors: colorsBars 
          });
	}
    $('#modalCargaPagina').modal('hide');
                //hide the modal
    $('body').removeClass('modal-open'); 
                //modal-open class is added on body so it has to be removed
    $('.modal-backdrop').remove();

    $('#modalCargaPagina').hide();
}
/*FUNCIÓN QUE RETORNA EL HOVER PERSONALIZDO DE LA GRAFICA */
function renderHoverGrafica(next,item,idP,index) {
	var capital=null,key=null;	
	if (next=='subdelegacion') {capital='DELEGACIÓN: '; index='ID_DEL';}
	if (next=='municipio') {capital='SUBDELEGACIÓN: '; index='ID_SUB';}
	if (next=='localidad') {capital='MUNICIPIO: '; index='CVE_MUN';}
	if (next=='') {capital='LOCALIDAD: '; index='CVELOC';}

	var renderHover="<div class='morris-hover-row-label' style='font-family:\"NeoSansPro-Bold\";color:#9aa1a4;'>"+capital+item[index]+"</div>\
	<div class='morris-hover-point' style='color:red;'>CREDENCIALES-> META: "+separa(item.META)+"</div>\
	<div class='morris-hover-point' style='color:"+colorsBars[0]+";'>CREDENCIALES-> ENTREGADAS: "+separa(item.AVANCE)+"</div>\
	<div class='morris-hover-point' style='color:"+colorsBars[1]+";'>CREDENCIALES-> RESTAN: "+separa(item.RESTAN)+"</div>\
	<div class='morris-hover-point'>\
	<a href='./controller/ReportsController.php?csrf_token=ok&opcion=Reportes&tipo=Credenciales&params={%22idP%22:%22"+idP+"%22,%22nivel%22:%22"+next+"%22,%22valor%22:%22"+item[index]+"%22}'>\
	REPORTE \
	<i class='fa fa-download'></i>\
	</a>\
	</div>";
	return renderHover;
}