function notificar(title,msg,tipo) {
   var data={
      0: 'Algo sucedio con la solicitud. Intente de nuevo o comuniquese con el Administrador.'
   };

   if (msg=='0') {
      msg = data[msg];
   }

   toastr.options = {
      "closeButton": true,//boton de cerras
      "debug": false,
      "newestOnTop": false,//poner la notificacion arriba de otra
      "progressBar": false,//barra de progreso
      "positionClass": "toast-top-right",//['toast-top-left','toast-bottom-right','toast-bottom-left','toast-top-full-width','toast-bottom-full-width','toast-top-center','toast-bottom-center']
      "preventDuplicates": false,//mostrar mas de una notificacion a la vez
      "onclick": null,//COMENTAR ESTA LINEA SI NECESITA EVEVNTO ONCLICK;
      "showDuration": "300",//tiempo que tarda para mostrar
      "hideDuration": "300",//tiempo que dura para ocultar
      "timeOut": "5000",//tiempo en pantalla
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "show",//['fadeIn','show']
      "hideMethod": "hide"//['fadeOut','hide']
   };

   /*toastr.options.onclick = function () {
      alert('You can perform some custom action after a toast goes away');
   };*/ //SI SE NECESITA EVENTO ONCLICK
            
   //var tipo=tipo;//['success','info','warning','error']
   var $toast = toastr[tipo](msg, title); // Wire up an event handler to a button in the toast, if it exists
}