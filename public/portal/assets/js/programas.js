
var idUsuarioLogin=document.getElementById("idUsuarioLogin").value;


function showProyectosInicio(datas,select='') {
  //console.log(datas);
  if (select==true|| select=='') {
    var select="<select class='form-control' id='selectProyectos'>"+
    buildOptions(datas.programas,'NOMBREPROGRAMA','IDPROGRAMA')+
    "</select>";
    $('#subItemsTitle').append(select);
  }

  var DOM;

  $.each(datas.programas, function(i, item) {
    DOM=Render(item,i,'Programas','');
    $('#contentProgramas').append(DOM);
  });
  $('#selectProyectos').change(function() {
    href('programas',this.value);
  });
  //combobox();
}

var sitesRender;
function showSitiosInicio(datas,_t,modules='') {
  sitesRender=datas;
  var DOM;
  var aux=modules.split(", ");
  if (modules!='') {
    $.each(datas, function(i, item) {
      for (var j = 0; j < aux.length; j++) {
        if (aux[j]==item.idSitio) {
          DOM=Render(item,i,'Sitios',_t);
          $('#ContentSitios').append(DOM);
        }
      }
    });
  }else{
    $.each(datas, function(i, item) {
      DOM=Render(item,i,'Sitios',_t);
      $('#ContentSitios').append(DOM);
    });
  }
}

function Render(item,i,org,_t) {

  var render="";
  if (org=='Programas') {
    render="<div class='col-md-4 col-sm-6 col-xs-12'>\
    <div class='card hover backCards hoverable z-depth-1'>\
    <li class='list-group-item'>\
    <img class='card-img-top' src='./Storage/Evidencias/imgPrograms/alargada/"+item.IMAGEN_ALARGADA+"' alt='Card image cap' height='70px'>\
    </li>\
    <div class='card-body'>\
    <h5 class='card-title'>"+item.NOMBREPROGRAMA+"</h5>\
    <div class='col-md-12'>\
    <a href='#!' class='badge badge-light' data-toggle='modal' style='color:grey' data-target='#modalDescripcion' onclick='RenderModalDescripcion(\""+item.NOMBREPROGRAMA+"\",\""+item.DESCRIPCIONPROGRAMA+"\",\"programas\");'><i>DETALLE DE PROGRAMA</i></a>\
    </div>\
    <br>\
    <div class='row'>\
    <div class='col-md-6'>\
    <a href='?view=programas&index="+item.IDPROGRAMA+"'>\
    <div class='small-box bg-secondary text-center'>\
    Generales <i class='fa fa-arrow-circle-right'></i>\
    </div>\
    </a>\
    </div>\
    <div class='col-md-6'>\
    <a href='?view=padron&index="+item.IDPROGRAMA+"'>\
    <div class='small-box bg-secondary text-center'>\
    Padron <i class='fa fa-arrow-circle-right'></i>\
    </div>\
    </a>\
    </div>\
    </div>\
    </div>\
    </div>\
    </div>";
  }
  if (org=='Sitios') {
      //console.log(optionsSites);
      var styleP="";
      if (i==0||i==1||i==6||i==7||i==10||i==11||i==12||i==13) {
        styleP="style='padding-top:12%'";
      }
      if (item.on) {
        var a="";
        if (item.type=='link') {
          var href='';
          var target='';
          if (item.t) {
            href=item.href+"?_t="+_t;
            target='_blank';
          }else{
            href=item.href;
          }
          a="<a href='"+href+"' style='color:black;' target='"+target+"'>";
        }else{
          a="<a href='' style='color:black;' data-toggle='modal' data-target='#modalDescripcion' onclick='RenderModalDescripcion(\""+item.name+"\",\""+item.options+"\",\"sitios\","+i+",\""+_t+"\")'>";
        }
        render="<div class='col-md-3 col-sm-4 col-xs-2'>\
        "+a+"\
        <div class='card hover hoverable z-depth-1'>\
        <div class='row'>\
        <div class='col-md-4 col-xs-3'>\
        <img class='' src='./Storage/Evidencias/imgSites/"+item.img+"' alt='Card image cap' width='100%'>\
        </div>\
        <div class='col-md-8 col-xs-9'>\
        <div class='card-body backCardsSites elevation-solid-Left-to-Top' "+styleP+">\
        <p class='upperCase'>"+item.name+"</p>\
        <br>\
        </div>\
        </div>\
        </div>\
        </div>\
        </a>\
        </div>";
      }

    }
    return render;
  }
  function RenderModalDescripcion(title,desc,org,i='',_t) {
   $('#titleModal').html(title);
   if (org=='programas') {
     $('#contenModal').html("\
      <div class='jumbotron-fluid' style='padding-top:5px;'>\
      <p class='text-justify' style='padding-top:5px;'>"+desc+".</p>\
      <!--hr class='my-4'-->\
      </div>");
   }
   if (org=='sitios') {
      //console.log(sitesRender[i].options);
      var href='';
      var index=i;
      var pt='';
      var style='';
      $.each(sitesRender[i].options,function (i,item) {
        if (i>3) {
          pt='pt-2';
        }
        if (item.on) {
          if (item.option.split(" ").length>4) {
            style='font-size: 13px;';
          }else{
            style='';
          }
          if (item.type=='view'||item.type=='') {
            href+="<div class='col-md-4 text-center "+pt+"' style='"+style+"'>\
            <a href='?view=modules&index="+(index+1)+"' class='btn btn-outline-secondary btn-block' style='"+style+"'>"+item.option+"</a>\
            </div>";
          }
          if (item.type=='act') {
            href+="<div class='col-md-4 text-center "+pt+"' style='"+style+"'>\
            <a href='?view=modules&act="+item.act+"&index="+(index+1)+"' class='btn btn-outline-secondary btn-block' style='"+style+"'>"+item.option+"</a>\
            </div>";
          }
          if (item.type=='link') {
            var link=item.href;
            if (item.t) {
              link+="?_t="+_t;
            }
            href+="<div class='col-md-4 text-center "+pt+"' style='"+style+"'>\
            <a href='"+link+"' class='btn btn-outline-secondary btn-block' style='"+style+"'>"+item.option+"</a>\
            </div>";
          }
        }
      });
      $('#contenModal').html("\
       <div class='col-md-12' style='padding-top:5px;'>\
       <div class='row'>\
       "+href+"\
       </div>\
       </div>");
    }
  }


  function tableAvances(datas,idP,index,key,modalidad=''){
  //console.log(datas);
  //console.log(datas.tiposEventoPrograma);
  var data=[];
  if (modalidad==2) {
    var eventos=datas.tiposEventoPrograma[idP];
    var metas=datas.metas;
    var avances= datas.avances;
    console.log(avances);

    var resta;
    var meta=0;
    var avance=0;
    var avanceG=0;
    $.each(eventos,function(i,item) {
      if (Array.isArray(avances)) {
        if (avances.length==0) {
          avance=0;
        }
      }else{
          var avanceLength=Object.keys(avances);
          if (avanceLength.length==0) {
              avance=0;
          }else{
              if (avances[item.IDTIPOEVENTO]) {
                  avance=avances[item.IDTIPOEVENTO][0].OBRAS;
              }else{
                  avance=0;
              }
          }
      }
        meta=meta+parseInt(metas[item.IDTIPOEVENTO][0].META);
        resta=(parseInt(metas[item.IDTIPOEVENTO][0].META))-(parseInt(avance));
        avanceG=avanceG+parseInt(avance);
        data.push({
            IDPROGRAMA:item.IDPROGRAMA,
            IDEVENTO:item.IDTIPOEVENTO,
            EVENTO:item.TIPOEVENTO,
            META:parseInt(metas[item.IDTIPOEVENTO][0].META),
            AVANCE:avance,
            RESTA:resta
        });
    });
  }
  if (modalidad==1) {
    if(key=='DELEGACION'){
      $.each(datas,function(i,item) {
        data.push({ID_DEL:item[index],DELEGACIÓN:item[key],AVANCE:item.ENTREGAS,META:item.ENTREGAS});
        meta=0;
      });
    }


    if (key=='SUBDELEGACION') {
      $.each(datas,function(i,item) {
        data.push({ID_SUB:item[index],SUBDELEGACIÓN:item[key],AVANCE:item.ENTREGAS,META:item.ENTREGAS});
        meta=0;
      });
    }
    if (key=='NOMBREMUN') {
      $.each(datas,function(i,item) {
        data.push({CVE_MUN:item[index],MUNICIPIO:item[key],AVANCE:item.ENTREGAS,META:item.ENTREGAS});
        meta=0;
      });
    }
    if (key=='NOMBRELOC') {
      $.each(datas,function(i,item) {
        data.push({CVELOC:item[index],LOCALIDAD:item[key],AVANCE:item.ENTREGAS,META:item.ENTREGAS});
        meta=0;
      });
    }
  }

  return [data,meta,avanceG];
}

function graficas(element='',data='',xkey='',ykey='',stack='') {
  $('#'+element).html('');
  if (stack=='') {
    stack=false;
  }
  var garf1=new Morris.Bar({
    element: element,
    data: data,
    hoverCallback: function(index, options, content,row) {
      content=`<div class='morris-hover-row-label'>${row.EVENTO}</div>
      <div class='morris-hover-point'>META: ${separa(row.META)}</div>
      <div class='morris-hover-point' style='color:${colorsBars[0]};'>AVANCE: ${separa(row.AVANCE)}</div>
      <div class='morris-hover-point' style='color:${colorsBars[1]};'>FALTANTE: ${separa(row.RESTA)}</div>
      <button type='button' class='btn btn-link' onclick='generaReporte("ReportPogramaEvento",${row.IDPROGRAMA},${row.IDEVENTO},${idUsuarioLogin});'>REPORTE<i class='fa fa-download'></i></button>
      <!--div class='morris-hover-point' style='color:red;'><a href='./controller/ReportsController.php?csrf_token=ok&opcion=Reportes&tipo=ReportPogramaEvento&params={"idP":"${row.IDPROGRAMA}","idE":"${row.IDEVENTO}"}'> REPORTE <i class='fa fa-download'></i></a></div-->`;

 // <!--button type='button' class='btn btn-link' onclick='generaReporte("ReportPogramaEvento",${idP}, "${next}", ${idUsuarioLogin},"${item[index]}",${eventoG},"${item[nombre]}");'>REPORTE<i class='fa fa-download'></i></button-->
 return content;

},
xkey: xkey,
ykeys: ykey,
stacked: stack,
resize:true,
xLabelAngle:45,
labels: [],
barColors: colorsBars
});
}

function graficaAvance(next,back,idP,element='',data='',xkey='',ykey='',stack='') {
	//alert('null');

  $('#'+element).html('');
  $("#"+element).empty();
  if (stack=='') {
    stack=false;
  }
  var index=null,value=null;
  if (next=='subdelegacion') {
    index='ID_DEL';
    value='DELEGACIÓN';
  }
  if (next=='municipio') {
    index='ID_SUB';
    value='SUBDELEGACIÓN';
  }
  if (next=='localidad') {
    index='CVE_MUN';
    value='MUNICIPIO';
  }
  var x=null;
  if (modalidad==1) {
    var graficaAvanceDelegacion=new Morris.Bar({
      element: element,
      data: data,
      hoverCallback: function(index, options, content,row) {
        content=renderHoverGrafica(next,row[xkey],row.AVANCE,idP,row);
        $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
        return content;
      },
      xkey: xkey,
      ykeys: ykey,
      stacked: stack,
      resize:false,
      xLabelAngle:45,
      labels: [],
      barColors: colorsBars
    });
  }
}

var origenAux;
function callNewgrafica(tipo,id,title,idP,x='') {
  /*console.log("tipo-> ", tipo);
  console.log("id-> ", id);
  console.log("title-> ", title);
  console.log("idP-> ", idP);
  console.log("------***********________**************----------------");*/

  origen=tipo;


  idGeneral=id;
  titleGeneral=title;
  $("#modalCargaPagina").modal("show");
  var url="../../pillar/clases/controlador_pruebas.php";
  var index='',value='',elementTable=null,jump=false,exception=null,idTable='',next=null,back=null,
  elementGragica='',key='',values=null,btn_back='',nameReport,  content_Main='content-AvancesGeneral',  dataNull='#dataNull';


  if (tipo=='subdelegacion') {
    titleDelegacion=title;
    $('#btnBackDelegacion').hide();
    $("#content-Subdelegacion").fadeIn(2000);
    $('#titleGraficaSubdel').html("OFICINA REGIONAL: "+titleDelegacion+".");
    idDelegacion=id;
    //if (x) {
      objet={opcion:'generales_programa',idPrograma:idP,delegacion:idDelegacion,fechaInicio:inicio,fechaTermino:fin,evento:eventoG,entrega:entrega};
    /*}else{
      objet={opcion:'generales_programa',idPrograma:idP,delegacion:id};
    }*/

    index='ID_SUB';
    value='SUBDELEGACION';
    elementTable='#tableAvancesSubdel';
    jump=1;
    exception=1;
    idTable='table-dinamicSubdel';
    next='municipio';
    back='delegacion';
    elementGragica='graficaAvanceSubdel';
    key='SUBDELEGACIÓN';
    values=['AVANCE','META'];
    btn_back='#btnBackDelegacion';
    $('#btnBack-Delegacion').attr('onclick','backLevel("'+back+'")');
    nameReport='Subdelegacion';
    content_Main='content-Subdelegacion';
  }
  if (tipo=='municipio') {
    titleSubdelegacion=title;
    $('#btnBackSubdelegacion').hide();
    $("#content-Municipio").fadeIn(2000);
    $('#titleGraficaMunicipio').html("OFICINA SUBREGIONAL: "+titleSubdelegacion+".");
    idSubdelegacion=id;
    objet={opcion:'generales_programa',idPrograma:idP,subdelegacion:idSubdelegacion,fechaInicio:inicio,fechaTermino:fin,evento:eventoG,entrega:entrega};
    index='CVE_MUN';
    value='NOMBREMUN';
    elementTable='#tableAvancesMunicipio';
    jump=1;
    exception=1;
    idTable='table-dinamicMunicipio';
    next='localidad';
    back='subdelegacion';
    elementGragica='graficaAvanceMunicipio';
    key='MUNICIPIO';
    values=['AVANCE','META'];
    btn_back='#btnBackMunicipio';
    $('#btnBack-SubDelegacion').attr('onclick','backLevel("'+back+'")');
    nameReport='Municipio';
    content_Main='content-Municipio';
  }
  if (tipo=='localidad') {
    titleMunicipio=title;
    $('#btnBackMunicipio').hide();
    $("#content-Localidad").fadeIn(2000);
    $('#titleGraficaLocalidad').html("Municipio: "+titleMunicipio+".");
    idMunicipio=id;
    objet={opcion:'generales_programa',idPrograma:idP,municipio:idMunicipio,fechaInicio:inicio,fechaTermino:fin,evento:eventoG,entrega:entrega};
    index='CVELOC';
    value='NOMBRELOC';
    elementTable='#tableAvancesLocalidad';
    jump=1;
    exception=1;
    idTable='table-dinamicLocalidad';
    next='';
    back='municipio';
    elementGragica='graficaAvanceLocalidad';
    key='LOCALIDAD';
    values=['AVANCE','META'];
    btn_back='#btnBackLocalidad';
    $('#btnBack-Municipio').attr('onclick','backLevel("'+back+'")');
    nameReport='Localidad';
    content_Main='content-Localidad';
  }

  clean(tipo,'');

  $('#Filtros').fadeOut('slow');
  ajaxCallback(objet,url,function (respuesta){
    res = JSON.parse(respuesta);
              //console.log(res);
              if (res.datos.length==0){
                showAlertInwindow(dataNull,'warning','Sin Datos');
                $('#'+content_Main).fadeOut('slow');
              }else{
                $(dataNull).html('');
                var data=tableAvances(res.datos,idP,index,value,modalidad);
                console.log(data);
                renderTable(elementTable,data[0],idP,jump,exception,idTable,next);
                tableExport2('#'+idTable,'AVANCE-'+nameReport);
                settingsTable();
                graficaAvance(next,back,idP,elementGragica,data[0],key,values,false);
                $('#Filtros').fadeIn('slow');
                /*if (tipo=='subdelegacion') {
                  graficaSubdelegacion(next,back,idP,elementGragica,data[0],key,values,false);
                }
                if (tipo=='municipio') {
                  graficaMunicipio(next,back,idP,elementGragica,data[0],key,values,false);
                }
                if (tipo=='localidad') {
                  graficaLocalidad(next,back,idP,elementGragica,data[0],key,values,false);
                }*/
                $(btn_back).show();
                $("#modalCargaPagina").modal("hide");
              }
              $('#Filtros').fadeIn('slow');
              $("#modalCargaPagina").modal("hide");
              //console.log(data);
            });
}

function clean(next,back) {
  if (next=='subdelegacion') {
    $('#content-AvancesGeneral').fadeOut(100);
    $('#btnBackDelegacion').hide();
    $('#tableAvancesSubdel').html('Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');
    $('#graficaAvanceSubdel').html('Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');
  }
  if (next=='municipio') {
    $('#content-Subdelegacion').fadeOut(100);
    $('#btnBackMunicipio').hide();
    $('#tableAvancesMunicipio').html('Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');
    $('#graficaAvanceMunicipio').html('Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');
  }
  if (next=='localidad') {
    $('#content-Municipio').fadeOut(100);
    $('#btnBackLocalidad').hide();
    $('#tableAvancesLocalidad').html('Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');
    $('#graficaAvanceLocalidad').html('Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');
  }
}

function backLevel(back) {
  origen=back;
  if (back=='delegacion') {
    $('#content-AvancesGeneral').fadeIn(1000);
    $('#content-Subdelegacion').hide();
    $('#content-Municipio').hide();
    $('#content-Localidad').hide();
  }
  if (back=='subdelegacion') {
    $('#btnBackDelegacion').show();
    $('#content-AvancesGeneral').hide();
    $('#content-Subdelegacion').fadeIn(1000);
    $('#content-Municipio').hide();
    $('#content-Localidad').hide();
  }
  if (back=='municipio') {
    $('#btnBackMunicipio').show();
    $('#content-AvancesGeneral').hide();
    $('#content-Subdelegacion').hide();
    $('#content-Municipio').fadeIn(1000);
    $('#content-Localidad').hide();
  }
}

function renderHoverGrafica(next,title,count,idP,item) {


  var capital=null; index=null;
  if (next=='subdelegacion') {capital='OFICINA REFIONAL: '; index='ID_DEL';  nombre="DELEGACIÓN"}
  if (next=='municipio') {capital='OFICINA SUBREGIONAL: '; index='ID_SUB'; nombre="SUBDELEGACIÓN"}
  if (next=='localidad') {capital='MUNICIPIO: '; index='CVE_MUN'; nombre="MUNICIPIO"}
  if (next=='') {capital='LOCALIDAD: '; index='CVELOC'; nombre="LOCALIDAD"}


  var renderHover=`<div class='morris-hover-row-label' style='font-family:\"NeoSansPro-Bold\";color:#9aa1a4;'>`+capital+title+`</div>
  <div class='morris-hover-point' style='color:${colorsBars[0]};'>AVANCE: ${separa(count)}</div>
  <div class='morris-hover-point' style='color:${colorsBars[1]};'>META: ${separa(count)}</div>
  <div class='morris-hover-point'>
  <button type='button' class='btn btn-link' onclick='generaReporte("Avances",${idP},${eventoG}, ${idUsuarioLogin}, "${next}", "${item[index]}","${item[nombre]}");'>REPORTE<i class='fa fa-download'></i></button>
  <!--a href='../../pillar/clases/RepoConMail.php?csrf_token=ok&opcion=Reportes&tipo=Avances&params={%22idP%22:%22"+idP+"%22,%22nivel%22:%22"+next+"%22,%22valor%22:%22"+item[index]+"%22,%22i%22:%22"+idUsuarioLogin+"%22}'>
  REPORTE
  <i class='fa fa-download'></i>
  </a-->
  </div>`;

  return renderHover;
}



function generaReporte(tipo,idP,eventoG,idUsuarioLogin, nivel='',valor='',nombreRep=''){
  if (tipo=='ReportPogramaEvento') {


    urlREPORTE='../../pillar/clases/RepoConMail.php';
    var paramsEvento={idP:idP,eventoG:eventoG,i:idUsuarioLogin}
    objet={opcion:'generaReporte',params:paramsEvento, function:'ReportPogramaEvento'};
    alert("Cuando el reporte termine de generarse se enviará una notificación al correo con el que inicio sesión.");

    ajaxCallback(objet,urlREPORTE,function (respuesta){
      resReporte = JSON.parse(respuesta);
    console.log(resReporte);
  });




}else if (tipo=='Avances') {

  urlREPORTE='../../pillar/clases/RepoConMail.php';
  var params={idP:idP,valor:valor,i:idUsuarioLogin,nivel:nivel,eventoG:eventoG,nombreRep:nombreRep}
  objet={opcion:'generaReporte',params:params, function:'Avances'};
  alert("Cuando el reporte termine de generarse se enviará una notificación al correo con el que inicio sesión.");
  ajaxCallback(objet,urlREPORTE,function (respuesta){
    resReporte = JSON.parse(respuesta);
    //console.log(resReporte);
  });

}





}


// function enviaReporteMail(usuarioLogin,rutaDescarga){
//   urlREPORTE='../../pillar/clases/RepoConMail.php';
//   objet={opcion:'enviaMail2',idUsuarioLogin:usuarioLogin, rutaDescarga:rutaDescarga};
//   ajaxCallback(objet,urlREPORTE,function (respuesta){
//     resEnvio = JSON.parse(respuesta);
//     //console.log("enviado fin");
//   });


// }



function addProgram(_t,org) {
  load();
  try{
    load();
    var url='./controller/ProgramasController.php';
    var valuesPOST=null;
    var datos = serialize('#dataGenerales','array');
    console.log(datos);

    if (modalidadGeneral==2) {
      if ($.isEmptyObject(EventofaseEtiquetas)) {
        alert('DATOS INCOMPLETOS!');
        load(false);
        notificar('ALERTA! ','DEBE SELECIONAR TODO LOS DATOS PARA AGREGAR PROGRAMA','warning');
      }else{
        valuesPOST={programa:datos,completes:EventofaseEtiquetas,img:nameImgs};
        objet={opcion:'Programas',action:'addPrograma',values:valuesPOST,csrf_token:_t,modalidad:modalidadGeneral};

        //console.log(objet);

        var upload = Files(urlUploadFile,"#img1",'insert',_t,org,'','',3);//Carga la primera imagen

        if (upload==true) {
          upload = Files(urlUploadFile,"#img2",'insert',_t,org,'','',2); //Carga la segunda imagen
          if (upload==true) {
            upload = Files(urlUploadFile,"#img3",'insert',_t,org,'','',1);//Carga la tercera imagen
            if (upload==true) {
              ajaxCallback(objet,url,function (respuesta){
                try {
                  res = JSON.parse(respuesta);
                  if (res.CODIGO==true) {
                    exitoAddProgram();
                  }
                  else{
                    upload=Files(urlUploadFile,"",'delete',_t,org,nameImgs);
                    load(false);
                    notificar('ALGO SUCEDIO... ',res.DATOS,'error');
                  }
                }
                catch(err) {
                  upload=Files(urlUploadFile,"",'delete',_t,org,nameImgs);
                  load(false);
                  notificar('ERROR! ','0','error');
                  console.log(err);
                }
              });

            }else{
              load(false);
              notificar('LO SIENTO!', 'Algo sucedion con la carga de imagenes, intente de nuevo.','error');
            }
          }else{
            load(false);
            notificar('LO SIENTO!', 'Algo sucedion con la carga de imagenes, intente de nuevo.','error')
          }
        }else{
          load(false);
          notificar('LO SIENTO!', 'Algo sucedion con la carga de imagenes, intente de nuevo.','error');
        }
      }
    }
    if (modalidadGeneral==1) {
      if (eventosSelected.length==0) {
        alert('DATOS INCOMPLETOS! INGRESE EVENTOS');
        load(false);
        notificar('ALERTA! ','DEBE SELECIONAR TODO LOS DATOS PARA AGREGAR PROGRAMA','warning');
      }else{
        valuesPOST={programa:datos,completes:eventosSelected,img:nameImgs};
        objet={opcion:'Programas',action:'addPrograma',values:valuesPOST,csrf_token:_t,modalidad:modalidadGeneral};

        console.log(objet);

        var upload = Files(urlUploadFile,"#img1",'insert',_t,org,'','',1);//Carga la primera imagen

        if (upload==true) {
          upload = Files(urlUploadFile,"#img2",'insert',_t,org,'','',2); //Carga la segunda imagen
          if (upload==true) {
            upload = Files(urlUploadFile,"#img3",'insert',_t,org,'','',3);//Carga la tercera imagen
            if (upload==true) {
              ajaxCallback(objet,url,function (respuesta){
                try {
                  res = JSON.parse(respuesta);
                  if (res.CODIGO==true) {
                    exitoAddProgram();
                  }
                  else{
                    upload=Files(urlUploadFile,"",'delete',_t,org,nameImgs);
                    load(false);
                    notificar('ALGO SUCEDIO... ',res.DATOS,'error');
                  }
                }
                catch(err) {
                  upload=Files(urlUploadFile,"",'delete',_t,org,nameImgs);
                  load(false);
                  notificar('ERROR! ','0','error');
                  console.log(err);
                }
              });

            }else{
              load(false);
              notificar('LO SIENTO!', 'Algo sucedion con la carga de imagenes, intente de nuevo.','error');
            }
          }else{
            load(false);
            notificar('LO SIENTO!', 'Algo sucedion con la carga de imagenes, intente de nuevo.','error')
          }
        }else{
          load(false);
          notificar('LO SIENTO!', 'Algo sucedion con la carga de imagenes, intente de nuevo.','error');
        }
      }
    }


  }
  catch(err) {
    load(false);
    notificar('ERROR!','Salida invalida','error');
    console.log(err);
  }
}


function exitoAddProgram() {
  load(false);
  notificar('EXITO! ',res.DATOS+"\n Puede revisar la lista de programas disponibles",'success');
  $("#formAddProgram")[0].reset();
  delete EventofaseEtiquetas.eventos;
  delete EventofaseEtiquetas.faseEtiqueta;
  setTimeout("location.reload()",5000);
}


function traeReporte(idP){
  alert("prueba"+idP);
}


/*function graficaDelegacion(next,back,idP,element='',data='',xkey='',ykey='',stack='') {
  $('#'+element).html('');
  $("#"+element).empty();
  if (stack=='') {
    stack=false;
  }
  var index=null,value=null;
  if (next=='subdelegacion') {
    index='ID_DEL';
    value='DELEGACIÓN';
  }
  if (next=='municipio') {
    index='ID_SUB';
    value='SUBDELEGACIÓN';
  }
  if (next=='localidad') {
    index='CVE_MUN';
    value='MUNICIPIO';
  }

  if (modalidad==1) {
    var graficaAvanceDelegacion=new Morris.Bar({
        element: element,
        data: data,
        hoverCallback: function(index, options, content,row) {
          var params=[row.ID_DEL,idP];
          content="<div class='morris-hover-row-label'>DELEGACIÓN: "+row[xkey]+"</div>\
                  <div class='morris-hover-point' style='color:"+colorsBars[1]+";'>ENTREGAS: "+separa(row.AVANCE)+"</div>\
                  <!--div class='morris-hover-point' style='color:red;'><a href='./controller/ReportsController.php?csrf_token=ok&opcion=Reportes&tipo=ReportPogramaEvento&params={%22idP%22:%22"+idP+"%22,%22idE%22:%22"+row.ID_DEL+"%22}'> REPORTE <i class='fa fa-download'></i></a></div-->";
          return content;
        },
        xkey: xkey,
        ykeys: ykey,
        stacked: stack,
        resize:false,
        xLabelAngle:45,
        labels: [],
        barColors: colorsBars
      }).on('click', function(i, row){
        if (next=='') {
          return false;
        }else{
          console.log(next+'-> ');
          console.log(row);
          clean(next,back);
          callNewgrafica(next,row.ID_DEL,row.DELEGACIÓN,idP);
        }
      });
  }
}

function graficaSubdelegacion(next,back,idP,element='',data='',xkey='',ykey='',stack='') {
  $('#'+element).html('');
  $("#"+element).empty();
  if (stack=='') {
    stack=false;
  }
  var index=null,value=null;
  if (next=='municipio') {
    index='ID_SUB';
    value='SUBDELEGACIÓN';
  }

  if (idP==3||idP==2) {
    var graficaAvanceSubDelegacion=new Morris.Bar({
        element: element,
        data: data,
        hoverCallback: function(index, options, content,row) {
          var params=[row.ID_DEL,idP];
          content="<div class='morris-hover-row-label'>SUBDELEGACIÓN: "+row[xkey]+"</div>\
                  <div class='morris-hover-point' style='color:"+colorsBars[1]+";'>ENTREGAS: "+separa(row.AVANCE)+"</div>\
                  <!--div class='morris-hover-point' style='color:red;'><a href='./controller/ReportsController.php?csrf_token=ok&opcion=Reportes&tipo=ReportPogramaEvento&params={%22idP%22:%22"+idP+"%22,%22idE%22:%22"+row.ID_DEL+"%22}'> REPORTE <i class='fa fa-download'></i></a></div-->";
          return content;
        },
        xkey: xkey,
        ykeys: ykey,
        stacked: stack,
        resize:false,
        xLabelAngle:45,
        labels: [],
        barColors: colorsBars
      }).on('click', function(i, row){
        if (next=='') {
          return false;
        }else{
          console.log(next+'-> ');
          console.log(row);
          clean(next,back);
          callNewgrafica(next,row.ID_SUB,row.SUBDELEGACIÓN,idP);
        }
      });
  }
}

function graficaMunicipio(next,back,idP,element='',data='',xkey='',ykey='',stack='') {
  $('#'+element).html('');
  $("#"+element).empty();
  if (stack=='') {
    stack=false;
  }
  var index=null,value=null;
  if (next=='localidad') {
    index='CVE_MUN';
    value='MUNICIPIO';
  }

  if (idP==3||idP==2) {
    var graficaAvanceMunicipio=new Morris.Bar({
        element: element,
        data: data,
        hoverCallback: function(index, options, content,row) {
          var params=[row.ID_DEL,idP];
          content="<div class='morris-hover-row-label'>MUNICIPIO: "+row[xkey]+"</div>\
                  <div class='morris-hover-point' style='color:"+colorsBars[1]+";'>ENTREGAS: "+separa(row.AVANCE)+"</div>\
                  <!--div class='morris-hover-point' style='color:red;'><a href='./controller/ReportsController.php?csrf_token=ok&opcion=Reportes&tipo=ReportPogramaEvento&params={%22idP%22:%22"+idP+"%22,%22idE%22:%22"+row.ID_DEL+"%22}'> REPORTE <i class='fa fa-download'></i></a></div-->";
          return content;
        },
        xkey: xkey,
        ykeys: ykey,
        stacked: stack,
        resize:false,
        xLabelAngle:45,
        labels: [],
        barColors: colorsBars
      }).on('click', function(i, row){
        if (next=='') {
          return false;
        }else{
          console.log(next+'-> ');
          console.log(row);
          clean(next,back);
          callNewgrafica(next,row.CVE_MUN,row.MUNICIPIO,idP);
        }
      });
  }
}


function graficaLocalidad(next,back,idP,element='',data='',xkey='',ykey='',stack='') {
  $('#'+element).html('');
  $("#"+element).empty();
  if (stack=='') {
    stack=false;
  }
  var index=null,value=null;
  if (next=='localidad') {
    index='CVELOC';
    value='LOCALIDAD';
  }

  if (idP==3||idP==2) {
    var graficaAvanceLocalidad=new Morris.Bar({
        element: element,
        data: data,
        hoverCallback: function(index, options, content,row) {
          var params=[row.ID_DEL,idP];
          content="<div class='morris-hover-row-label'>LOCALIDAD: "+row[xkey]+"</div>\
                  <div class='morris-hover-point' style='color:"+colorsBars[1]+";'>ENTREGAS: "+separa(row.AVANCE)+"</div>\
                  <!--div class='morris-hover-point' style='color:red;'><a href='./controller/ReportsController.php?csrf_token=ok&opcion=Reportes&tipo=ReportPogramaEvento&params={%22idP%22:%22"+idP+"%22,%22idE%22:%22"+row.ID_DEL+"%22}'> REPORTE <i class='fa fa-download'></i></a></div-->";
          return content;
        },
        xkey: xkey,
        ykeys: ykey,
        stacked: stack,
        resize:false,
        xLabelAngle:45,
        labels: [],
        barColors: colorsBars
      }).on('click', function(i, row){
        if (next=='') {
          return false;
        }else{
          console.log(next+'-> ');
          console.log(row);
          clean(next,back);
          callNewgrafica(next,row.CVELOC,row.LOCALIDAD,idP);
        }
      });
  }
}*/
