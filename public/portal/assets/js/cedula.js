function infoBeneficiario(datas) {
	console.log(datas);
	$('#nameBen').html(datas.NOMBRE+' '+datas.APATERNO+' '+datas.AMATERNO);
	$('#foliocuis').html("<a href='#!'>\
							<span class='click' data-toggle='tooltip' data-html='true' data-placement='right' title='<a href=\""+hrefCuis(datas.FOLIOCUIS)+"\" target=\"_blank\">CONSULTA FOLIOCUIS</a> <br> <a href=\""+hrefExpediente(datas.FOLIOCUIS)+"\" target=\"_blank\">CONSULTA EXPEDIENTE</a>' >"+datas.FOLIOCUIS+"</span>\
						</a>");
	$('#estadoBen').html(datas.NOMBREENT);
	$('#munBen').html(datas.NOMBREMUN);
	$('#locBen').html(datas.NOMBRELOC);
	$('#delBen').html(datas.DELEGACION);
	$('#subdelBen').html(datas.SUBDELEGACION);
	$('#imgBen').attr('src','./Storage/Evidencias/imagenes_vcc/50458275_583_0_58b8c6b658adb2.60328931.png');

	$('#hrefMap1').attr('href','?view=map&params={%22location%22:%22'+datas.LATITUD+','+datas.LONGITUD+'%22}');
	//$('#map').attr('src','./view/cedula/staticmaplite/staticmap.php?center='+datas.LATITUD+','+datas.LONGITUD+'&zoom=18&maptype=mapnik&markers='+datas.LATITUD+','+datas.LONGITUD+',ol-marker||19.415864,-99.1662342,bullseye');
	$('#hrefMap1').html('<img src="./view/cedula/staticmaplite/staticmap.php?center='+datas.LATITUD+','+datas.LONGITUD+'&zoom=16&size=150x150&maptype=mapnik&markers='+datas.LATITUD+','+datas.LONGITUD+',ol-marker|'+datas.LATITUD+','+datas.LONGITUD+',bullseye" class="img-fluid img-rounded" style="height: 150px;width: 100%;" id="map">');

	$('.click').tooltip({trigger: "click"});
    $('[data-toggle="tooltip"]').tooltip();

}


function Render(item,i,eventos,img_eve){
		var render='';
		render += '<div class="row pt-3"><!--PROGRAMAS DE BENEFICIARIO-->\
			<div class="col-md-12 text-center">\
				<hr>\
				<h2>'+item.NOMBREPROGRAMA+'</h2>\
				<h4 for="">'+res.eventos[item.IDPROGRAMA][0].TIPOEVENTO+'</h4>\
			</div>\
		</div>';
		var fecha='',org='',validacion='',length=0,name_doc='',n=0;

		$.each(eventos,function(j,evento){
			/*console.log(img_eve[0]);
			if (evento.IDEVENTO in img_eve) {
			}*/
			render += '<div class="row pt-3">\
							<div class="col-md-3 ml-4">\
								<div class="row">\
									<div class="col-md-12 text-center">\
										<h4><b>Entrega <small>'+(j+1)+'</small></b></h4>\
									</div>\
									<div class="col-md-12 containerJJProgramas table-responsive">\
										<table class="table">\
										<thead style="font-size: 10px;">\
											<tr>\
												<th>#</th>\
												<th>Documento</th>\
												<!--th>Validación</th-->\
												<th></th>\
											</tr>\
										</thead>\
											<tbody style="font-size: 12px;">';

								$.each(img_eve[evento.IDEVENTO],function(i,documento){
									render += '<tr>\
													<th>'+(i+1)+'</th>\
													<td>'+documento.TIPOIMAGEN+'</td>\
													<!--td align="center">Correcto</td-->\
													<td>\
														<a href="#!" class="badge badge-light" data-target="#carouselExampleIndicators'+(j+1)+'" data-slide-to="'+i+'" onclick="RenderInfoPrograma('+(j+1)+','+evento.IDPROGRAMA+','+i+',\''+documento.TIPOIMAGEN+'\',\''+documento.FECHAINSERCION+'\',\''+documento.IDEVENTO+'\',\''+documento.ACTUAL+'\','+i+','+img_eve[evento.IDEVENTO].length+',\''+documento.IDIMAGEN+'\','+item.IDPROGRAMA+')">\
															<i class="icon ion-eye nav-icon" style="font-size: 20px;"></i>\
														</a>\
													</td>\
												</tr>';
								});

								render +=  '</tbody>\
										</table>\
									</div>\
								</div>\
							</div>\
							<!--SLIDE DOCUMENTOSC-->\
							<div class="col-md-5 ml-4">\
								<div class="row">\
									<div class="col-md-12 text-center">\
										<h4><b>Encuestador <small>'+(j+1)+'</small></b></h4>\
									</div>\
									<div class="col-md-12 containerJJProgramas" style="background-color:rgb(227,225,224);">\
										<div id="carouselExampleIndicators'+(j+1)+'" class="carousel slide" data-ride="carousel" data-interval="false">\
											<ol class="carousel-indicators" id="indicators">';
    						if (evento.IDEVENTO in img_eve) {
    							var active="";
						  		$.each(img_eve[evento.IDEVENTO],function(i,documento){
									if (i==0) {
										active = 'active';
						    		}else{
						    			active = '';
						    		}
						    			render += '<li data-target="#carouselExampleIndicators'+(j+1)+'" data-slide-to="'+i+'" class="'+active+' btn btn-secondary" onclick="RenderInfoPrograma('+(j+1)+','+evento.IDPROGRAMA+','+i+',\''+documento.TIPOIMAGEN+'\',\''+documento.FECHAINSERCION+'\',\''+documento.IDEVENTO+'\',\''+documento.ACTUAL+'\','+i+','+img_eve[evento.IDEVENTO].length+',\''+documento.IDIMAGEN+'\','+item.IDPROGRAMA+')"></li>';
								});
							}else{
										render += '<li data-target="#carouselExampleIndicators'+(j+1)+'" data-slide-to="'+i+'" class="active btn btn-secondary"></li>';
							}
  								render += '</ol>\
						  					<div class="carousel-inner pt-3">';
						  	if (evento.IDEVENTO in img_eve) {
						  		$.each(img_eve[evento.IDEVENTO],function(i,documento){
						  			if (documento.TIPO=='DOCUMENTO') {
						  				if (i==0) {
											render += '<div class="carousel-item active" align="center">\
															<a href="'+documento.URL+'" download target="_blank">\
						      									<img id="" class="d-block" src="./assets/img/download.ico" height="220px;">\
						      									D E S C A R G A R\
						      								</a>\
						    							</div>';
						    			}else{
						    				render += '<div class="carousel-item" align="center">\
						      								<a href="'+documento.URL+'" download target="_blank">\
						      									<img id="" class="d-block" src="./assets/img/download.ico" height="220px;">\
						      									D E S C A R G A R\
						      								</a>\
						    							</div>';
						    			}
						  			}else{
						  				if (i==0) {
											render += '<div class="carousel-item active" align="center">\
						      								<img id="imgExp-ent_'+(j+1)+'-pro_'+evento.IDPROGRAMA+'-number:'+i+'" class="d-block img-rounded img-zoom" src="../../pillar/clases/imagen.php?idImagen='+documento.IDIMAGEN+'" alt="'+documento.URL+'" height="265px;" data-toggle="modal" data-target="#modalImgCedula" onclick="modalInfo(this,'+i+',\'pro\','+evento.IDEVENTO+');">\
						    							</div>';
						    			}else{
						    				render += '<div class="carousel-item" align="center">\
						      								<img id="imgExp-ent_'+(j+1)+'-pro_'+evento.IDPROGRAMA+'-number:'+i+'" class="d-block img-rounded img-zoom"  src="../../pillar/clases/imagen.php?idImagen='+documento.IDIMAGEN+'" alt="'+documento.URL+'" height="265px;" data-toggle="modal" data-target="#modalImgCedula" onclick="modalInfo(this,'+i+',\'pro\','+evento.IDEVENTO+');">\
						    							</div>';
						    			}
						  			}

								});
							}else{
										render += '<div class="carousel-item active" align="center">\
						      						<img id="imgExp-ent_'+(j+1)+'-pro_'+evento.IDPROGRAMA+'-number:0" class="d-block img-rounded" src="http://zazsupercentro.com/wp-content/uploads/2017/07/imagen-no-disponible.png" alt="NO DISPONIBLE" height="265px;">\
						    					</div>';
							}
							if (typeof img_eve[evento.IDEVENTO]!=='undefined') {
								fecha=img_eve[evento.IDEVENTO][0].FECHAINSERCION;
								if (img_eve[evento.IDEVENTO][0].IDEVENTO==null||img_eve[evento.IDEVENTO][0].IDEVENTO==0) {
									org='PORTAL';
								}else{
									org='DM';
								}
								if (img_eve[evento.IDEVENTO][0].ACTUAL==null||img_eve[evento.IDEVENTO][0].ACTUAL==0) {
									validacion='NO VALIDA';
								}
								if (img_eve[evento.IDEVENTO][0].ACTUAL==1) {
									validacion='CORRECTO';
								}
								if (img_eve[evento.IDEVENTO][0].ACTUAL==2) {
									validacion='INCORRECTO';
								}
								length=img_eve[evento.IDEVENTO].length;
								name_doc=img_eve[evento.IDEVENTO][0].TIPOIMAGEN;
								n=1;
							}
						  			render+='</div>\
						  					<a class="carousel-control-prev" href="#carouselExampleIndicators'+(j+1)+'" role="button" data-slide="prev" id="btn-prev'+(j+1)+'" style="width: 0%;" onclick="callNewImgPro('+item.IDPROGRAMA+',\''+evento.IDEVENTO+'\',\'less\','+(j+1)+')">\
						    					<span class="btn btn-secondary" aria-hidden="true"> <i class="carousel-control-prev-icon"></i></span>\
						    					<span class="sr-only">Previous</span>\
						  					</a>\
						  					<a class="carousel-control-next" href="#carouselExampleIndicators'+(j+1)+'" role="button" data-slide="next" id="btn-next'+(j+1)+'" style="width: 0%;" onclick="callNewImgPro('+item.IDPROGRAMA+',\''+evento.IDEVENTO+'\',\'more\','+(j+1)+')">\
						    					<span class="btn btn-secondary" aria-hidden="true"> <i class="carousel-control-next-icon"></i></span>\
						    					<span class="sr-only">Next</span>\
  											</a>\
										</div>\
									</div>\
								</div>\
							</div>\
							<!--INFO. DOCUMENTOS-->\
							<div class="col-md-3 ml-4">\
								<div class="row">\
									<div class="col-md-12 text-center">\
										<label>Fecha: <small>'+evento.FECHAVISITA+'</small></label>\
									</div>\
								</div>\
								<div class="row containerJJProgramas">\
									<div class="col-md-12">\
										<h5 class="text-center">Sello Dígital</h5>\
									</div>\
									<div class="col-md-4 offset-md-4 text-center" id="qr_P-'+item.IDPROGRAMA+'_Ent-'+j+'"></div>\
									<div class="col-md-12 pt-1">\
										<p class="text-center"><label>Documento</label></p>\
										<b><p>No.: <small id="no-'+(j+1)+'"> '+n+' / '+length+' </small></p></b>\
										<p>Nombre: <small id="nombreDoc-'+(j+1)+'">  '+name_doc+'  </small></p>\
										<p style="display: none;"> Fecha: <small id="fechaEnt-'+(j+1)+'">'+fecha+'</small></p>\
										<p>Origen: <small id="OrigenEnt-'+(j+1)+'">'+org+'</small></p>\
										<p style="display:none;">Validación:<small id="ValidacionEnt-'+(j+1)+'">'+validacion+'</small></p>\
										<p>\
										<!--a href="#" class="badge badge-light">Más Información</a-->\
										</p>\
									</div>\
									<div class="col-md-12" align="center" style="display:none;">\
										<input type="button" value="Más Fotos" class="btn btn-outline-primary">\
									</div>\
								</div>\
							</div>\
							<div class="col-md-10 offset-1 mt-3 containerOutH text-justify"><!--OBSRERVACIONES VISITA-->\
								<label>Observación de la Visita:</label><br>\
								<p>\
									'+checaValorDefault(evento.OBSERVACION)+'\
								</p>\
							</div>\
						</div>';
		});

		return render;

	}
var n=1;
var m=3;
var count=0;
var org;
	function ndm(option,i, element) {
		count=count+1;
		if (count==1) {
			org=element.id;
		}
		if (count>=1) {
			if (org==element.id) {
				if (option=='more') {n=n+1;}
				if (option=='less') {n=n-1;}
				if (n>m) {n=1}
				if (n<=0) {n=1}
			}else{
				n=1;
				m=3;
				count=0;
				//console.log('dif');
			}
		}
		$('#ndem'+i).html(n+'/'+m);
	}
