    /*
    variables generales
    */
    var url="./controller/GeneralesIntegraController.php";
    var idPrograma=getParameterByName('index');


    function printSearchElements(){
    	var pintar = [
    	{id:'delegacion',      tipo:'select',    title:'Delegación',      evento:'traeSubdelegacion'},
    	{id:'subdelegacion',   tipo:'select',    title:'Subelegación',    evento:''},
    	{id:'municipio',       tipo:'select',     title:'Municipio',       evento:''},
    	{id:'localidad',       tipo:'select',     title:'Localidad',       evento:''},
    	{id:'lote',            tipo:'input',     title:'Lote',            evento:''},
    	{id:'folioCuis',       tipo:'input',     title:'Folio Cuis',      evento:''},
    	];

    	var selects="";
    	pintar.forEach(function(item,i){
    		selects+=renderElement(item,item.tipo);	
    	});
    	document.getElementById("busqueda").innerHTML = selects;
    }



    var element="";
    function renderElement(item,tipo){
    	if (tipo=='input') {
    		element=`<div class="row input-group mb-3" style="margin-top:1em">
    		<div class="col-md-12 input-group-prepend">
    		<label class="input-group-text" style="font-size: 12px;" for="${item.id}">${item.title}</label>
    		<input type="text" class="form-control" id="${item.id}" placeholder="${item.title}">
    		</div>   
    		</div>`
    	} else if(tipo =='select'){
    		element=`<div class="row input-group mb-3" style="margin-top:1em"> 
    		<div class="col-md-12 input-group-prepend">
    		<label class="input-group-text" style="font-size: 12px;" for="${item.id}">${item.title}</label>
    		<select class="custom-select"  name="${item.id}" id="${item.id}" onchange="${item.evento}();"></select>
    		</div>  
    		</div>  `
    	}
    	return element
    }



    function traeDelegacion(){
    	objet={opcion:'generalesIntegra',action:'traeDelegacion',idPrograma:idPrograma,csrf_token:'_item'};
    	ajaxCallback(objet,url,function (respuesta){
    		resDEL = JSON.parse(respuesta);
    		var delegacion=buildOptions(resDEL.DATOS['delegacion'],'NOMBREDEL','CVEDEL');
    		$('#delegacion').html(delegacion);
    	});
    }



    function traeSubdelegacion(){
    	var opcionDel=document.getElementById('delegacion').value;	
    	valuesPost={cveDel:opcionDel}
    	objet={opcion:'generalesIntegra',action:'traeSubdelegacion',idPrograma:idPrograma,csrf_token:'_item',values:valuesPost};
    	ajaxCallback(objet,url,function (respuesta){
    		resSUBDEL = JSON.parse(respuesta);
    		console.log(resSUBDEL);
    		var subdelegacion=buildOptions(resSUBDEL.DATOS['subdelegacion'],'SUBDELEGACION','CVESUBDEL');
    		$('#subdelegacion').html(subdelegacion);
    	});
    }



