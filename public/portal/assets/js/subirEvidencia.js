
var url="../../pillar/clases/subirEvidencia.php";


function printElements(){
  var pintar = [
  {id:'tipoImagen',         tipo:'select',    title:'Tipo Imagen',       comTitle:'* ' ,    onchange:'traeTipoDocumento();'},
  {id:'tipoDocumento',      tipo:'select',    title:'Tipo Documento',    comTitle:'  ' ,      onchange:''},
  {id:'folioCuis',          tipo:'input',     title:'Folio Cuis',        comTitle:'* ' ,    onchange:'traeNombres();'},
  {id:'idListado',          tipo:'input',     title:'Id Listado',        comTitle:'* ' ,    onchange:''}
  ];

  var selects="";
  pintar.forEach(function(item,i){
    selects+=renderElement(item,item.tipo); 
  });
  document.getElementById("pintaPreguntas").innerHTML = selects;
}




var element="";
function renderElement(item,tipo){
  if (tipo=='input') {
    element=`<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-default">${item.comTitle}${item.title}</span>
    </div>
    <input type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" id="${item.id}" placeholder="${item.title}" onchange="${item.onchange}">
    </div>`
  } else if(tipo =='select'){
    element=`<div class="input-group mb-3">
    <div class="input-group-prepend">
    <label class="input-group-text" for="${item.id}">${item.comTitle}${item.title}</label>
    </div>
    <select class="custom-select" name="${item.id}" id="${item.id}" onchange="${item.onchange}">
    </select>
    </div>`
  }
  return element;
}








function pintaFolioCuis(){
  if (getParameterByName('folio')) {
    var getFolio=getParameterByName('folio');
    document.getElementById('folioCuis').value = getFolio; 
    traeNombres();
  } 
}






function traeTipoImagen(){
  objet = {opcion: 'traeTipoImagen'}
  ajaxCallback(objet,url,function (respuesta){
    resTipoImagen = JSON.parse(respuesta); 
    var tipoImagen=contruyeOpcion_separador(resTipoImagen['tipoImagen']);
    $('#tipoImagen').html(tipoImagen);
  });   
}


function traeTipoDocumento(){
  var valueTipoImagen=document.getElementById('tipoImagen').value;
  values={valueTipoImagen:valueTipoImagen}
  objet = {opcion: 'traeTipoDocumento', values:values}
  ajaxCallback(objet,url,function (respuesta){
    resTipoDocumento = JSON.parse(respuesta); 
    var tipoDocumento=buildOptions(resTipoDocumento['tipoDocumento'],'NOMBRE','VALUE');
    $('#tipoDocumento').html(tipoDocumento);
  });   
}



function traeNombres(){
 var valueFolioCuis=document.getElementById("folioCuis").value;
 values={valueFolioCuis:valueFolioCuis}
 objet = {opcion: 'traeIntFolioCuis', values:values}
 ajaxCallback(objet,url,function (respuesta){
  resIntegrantes = JSON.parse(respuesta); 
  var integrantes=resIntegrantes['integrantes'];


  var int = "";
  if (integrantes) {
    int+=`<table class="table table-sm" style="font-size:10px; margin-top:10px">
    <thead>
    <tr>
    <th scope="col">IDLISTADO</th>
    <th scope="col">Nombre</th>
    <th scope="col">Apellido Paterno</th>
    <th scope="col">Apellido Materno</th>
    </tr>
    </thead>
    <tbody>
    `
  }

  for (var i = 0; i < integrantes.length; i++) {
    var idListadoDB=integrantes[i]['IDLISTADO'];
    var nombre = integrantes[i]['NOMBRE'];
    var paterno= integrantes[i]['APATERNO'] != null ? integrantes[i]['APATERNO']: "";
    var materno=integrantes[i]['AMATERNO'] != null ? integrantes[i]['AMATERNO']: "";

    int += `
    <tr>
    <th><button type="button" class="btn btn-link" value="${idListadoDB}" onclick="cambiaIdListado(${idListadoDB});">${idListadoDB}</button></th>
    <td>${nombre}</td>
    <td>${paterno}</td>
    <td>${materno}</td>
    </tr>
    `;  
  }

  int += `<tr><tbody></table>`


  $("#integrantesContainer").html(int);
});   

}



function  cambiaIdListado(idListadoValor){
 document.getElementById('idListado').value = idListadoValor; 
}



function previewImage(){
  $("#vista-previa").html('');
  var archivos = document.getElementById('img1').files;

  var navegador = window.URL || window.webkitURL;
  for(x=0; x<archivos.length; x++)
  {
    var mjsError=`<div class='col-md-12' style='width: 100%; height: 400px;'>
    <p style='color: red'>El archivo no es del tipo de imagen permitida.</p>
    </div>`;

    var size = archivos[x].size;
    var type = archivos[x].type;
    var nameImg_2 = archivos[x].name;
    if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png' )
    {
      $("#vista-previa").append(mjsError);
    }else{
      var objeto_url = navegador.createObjectURL(archivos[x]);


      var pintaImagen=`
      <img src=${objeto_url} width='100%' height='400'>
      <div class='row' id='name_img'>${nameImg_2}</div>
      `;
      $("#vista-previa").append(pintaImagen);
    }
  } 
}





function guardarInfo(){
  var archivos = document.getElementById('img1').files;
  var f = new Date();
  var fecha=`${f.getFullYear()}${f.getMonth() +1}${f.getDate()}`;

  if (archivos.length !=0) {
    var v_tipoImagen=document.getElementById("tipoImagen").value;
    var v_tipoDocumento=document.getElementById("tipoDocumento").value;
    var v_folioCuis=document.getElementById("folioCuis").value;
    var v_idListado=document.getElementById("idListado").value;

    if (v_folioCuis =="" || v_idListado  =="" || v_tipoImagen =="" ||  v_tipoImagen =="0" ) {
      alert("Debe ingresar todos los campos obligatorios");
    } else {
      load();
      var nameOrigin = archivos[0].name;
      var dataFile=nameOrigin.split(".");
      var newNameImage= `${v_folioCuis}_${v_idListado}_${v_tipoImagen}_${fecha}.${dataFile[1]}`;
      values={v_tipoImagen:v_tipoImagen, v_tipoDocumento:v_tipoDocumento, v_folioCuis:v_folioCuis, v_idListado:v_idListado, newNameImage:newNameImage}
      objet = {opcion: 'registraImagen', values:values}

      var subir = Files(urlUploadFile,"#img1",'insert','23','evidenciaDocumento','',newNameImage);
      ajaxCallback(objet,url,function (respuesta){
        resRegistro = JSON.parse(respuesta); 
  load(false);
        if (resRegistro == true) {
          alert("La imagen fue registrada correctamente");
          window.location='?view=cedula&&act=subirevidencia';  
        } else {
          alert("No se pudo hacer el registro, verifique que haya ingresado datos existentes.");
        }
        
      });
    }
  } else {
    alert("Debe seleccionar la imagen a registrar");
  }

}
















