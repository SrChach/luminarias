<?php 
$messageError = array('400' => 'BAD REQUEST!');
?>
<div class="container-fluid spacing-12">
	<div class="row">
		<div class="col">
			<div class="offset-md-4 col-md-4">
				<div class="card backCardErrorRequest">
					<h1 class="card-header" style="background-color: rgb(224, 235, 235);"><?php echo $viewSites['error'] ?></h1>
  					<div class="card-body text-center">
    					<h5 class="card-title text-justify"><?php echo $messageError[$viewSites['error']] ?></h5>
    					<a class="btn btn-secondary" onclick="history.back();">BACK</a>
  					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#titlePage').html('ERROR!');
</script>