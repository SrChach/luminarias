
<div class="container-fluid spacing-12">
	<div class="row">
		<div class="col">
			<div class="offset-md-4 col-md-4">
				<div class="card backCardErrorRequest">
					<h2 class="card-header text-center" style="background-color: rgb(224, 235, 235);">ERROR DE KEY</h2>
  					<div class="card-body text-center">
    					<h5 class="card-title text-justify alert-warning">LA CLAVE DE ACCESO ES INCORRECTA</h5>
    					<form class="form-control mt-2" action="javascript:hash();" id="form-hash">
    						<div class="input-group mb-3">
  								<input type="password" class="form-control" id="key-in" name="key-in" autofocus="true" placeholder="Ingresa clave de acceso" required="true">
  								<div class="input-group-append">
    								<span class="input-group-text"><a href="#!" onclick="seeKey();"><i class="fa fa-eye" style="color: black;"></i></a></span>
  								</div>
  								<div class="invalid-feedback" id='msgError'>
        						</div>
							</div>
    						<input type="submit" value="VALIDAR" class="btn btn-secondary btn-block">
    					</form>
    					<div class="row" style="padding-top: 10%;">
                            <div class="offset-md-3 col-md-6" id="cargando" style="display: none;" align="center">
                                <img src='<?php echo $gifLoad ?>' width='60px'>
                            </div>
                        </div>
  					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#key-in').keyup(function(event) {
		$('#key-in').removeClass('is-invalid');
	});
	function hash() {
		$('#cargando').show('slow');
		var hash=$.trim( $('#key-in').val() );
		console.log(hash.length);
		$('#key-in').removeClass('is-invalid');
		if(hash.length==0){
			notificar('ALERTA !', 'INGRESE CLAVE DE ACCESO','warning');
			$('#msgError').html('CLAVE REQUERIDA.');
			$('#key-in').addClass('is-invalid');
			$('#key-in').val('');
			$('#cargando').hide('slow');
		}else{
			var urlController='./controller/HashController.php';
			var object={opcion:'setHash',csrf_token:'token',hash:hash};
			var res=null;
			$.post(urlController,object, function(data, textStatus, xhr) {
				/*optional stuff to do after success */
				res=$.parseJSON(data);
				console.log(res);
				if (res==true||res=='true') {
					//alert(res);
					location.reload(); 
				}else{
					notificar('ERROR !', 'CLAVE INCORRECTA','error');
					$('#msgError').html('CLAVE INVALIDAD.');
					$('#key-in').addClass('is-invalid');
					$('#key-in').val('');
				}
				$('#cargando').hide('slow');
			});
		}
	}
	var aux=false;
	function seeKey() {
		if (!aux) {
			$('#key-in').attr('type','text');
			aux=true;
		}else{
			$('#key-in').attr('type','password');
			aux=false;
		}
	}
</script>