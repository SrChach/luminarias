<div class="offset-md-4 col-md-4">
	<div class="card backCardErrorRequest">
		<h1 class="card-header" style="background-color: rgb(224, 235, 235);"><?php echo $viewSites['error'] ?></h1>
  		<div class="card-body text-center">
    		<h5 class="card-title text-justify"><?php echo $messageError[$viewSites['error']] ?></h5>
    		<a href="#" class="btn btn-primary">BACK</a>
  		</div>
	</div>
</div>