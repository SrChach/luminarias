<style type="text/css">
    .subtitle{
        background: #28A6DE ;
        color: white;
    }
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col" id="contenedorProgrmas">
			<form action="javascript:addProgram('<?php echo $csrf_token; ?>','<?php echo $_GET['view'] ?>');" id='formAddProgram' enctype="multipart/form-data" method="post">

                <div class="row">
                    <div class="col">
                        <h5 class="subtitle elevation-2 mb-4"> Datos Generales</h5>
                    </div>
                </div>
                <?php include_once './view/programas/completes/complete_input_generales.php'; ?>

                <div class="row" id="contentEventos">
                    <div class="col">
                        <h5 class="subtitle elevation-2 mb-4 col-md-12"> Tipos de Evento</h5>
                    </div>
                </div>
                <?php include_once './view/programas/completes/complete_input_tipoEvento.php'; ?>

              <div id="content-infoMore" style="direction: none">
                <div style="display: none;" id="contentFases">
                  <div class="row">
                    <div class="col">
                        <h5 class="subtitle elevation-2 mb-4 col-md-6"> Tipos de Fase</h5>
                    </div>
                  </div>
                  <?php include_once './view/programas/completes/complete_input_tipoFase.php'; ?>
                </div>

                <div style="display: none;" id="contentEtiquetas">
                  <div class="row">
                    <div class="col">
                        <h5 class="subtitle elevation-2 mb-4 col-md-6"> Tipos de Etiquetas</h5>
                    </div>
                  </div>
                  <?php include_once './view/programas/completes/complete_input_tipoEtiquetas.php'; ?>
                </div>

                <div class="row"  style="display: none;" id="contentResumen">
                    <div class="col-md-12">
                        <h5 class="subtitle elevation-2 mb-4"> RESUMEN</h5>
                        <?php include_once './view/programas/completes/complete_dataResumen.php'; ?>
                    </div>
                </div>
              </div>


			</form>

		</div>
	</div>
</div>



<script type="text/javascript">
  var outImg;
  var nameImgs=[];
  var idImgEle;
  var contenImg;
  var modalidadGeneral;
  $('#selectModalidad').change(function(event) {
    /* Act on the event */
    modalidadGeneral=this.value;
    if (this.value==2) {
      $('#content-infoMore').show();
      $('#contentFases').show('slow');
      $('#contentEtiquetas').show('slow');
      $('#saveOption2').hide('slow');
    }
    if (this.value==1) {
      $('#contentFases').hide('slow');
      $('#contentEtiquetas').hide('slow');
      $('#content-infoMore').hide('slow');
      $('#saveOption2').show('slow');
    }
  });

  $('#img1').change(function (e) {
    var allowSubmit = ValidafileType(this,1);
    outImg='#outImg1';
    idImgEle='outImg1';
    contenImg='#contentImg1';
    if (allowSubmit) {
      nameImgs[0]=$(this)[0].files[0].name;
      addImage(e);
    }
  });

  $('#img2').change(function (e) {
    var allowSubmit = ValidafileType(this,1);
    outImg='#outImg2';
    idImgEle='outImg2';
    contenImg='#contentImg2';
    if (allowSubmit) {
      nameImgs[1]=$(this)[0].files[0].name;
      addImage(e);
    }
  });

  $('#img3').change(function (e) {
    var allowSubmit = ValidafileType(this,1);
    outImg='#outImg3';
    idImgEle='outImg3';
    contenImg='#contentImg3';
    if (allowSubmit) {
      nameImgs[2]=$(this)[0].files[0].name;
      addImage(e);
    }
  });

  function addImage(e){
    var file = e.target.files[0],
    imageType = /image.*/;

    if (!file.type.match(imageType))
      return;

    var reader = new FileReader();
    reader.onload = fileOnload;
    reader.readAsDataURL(file);
  }

  function fileOnload(e) {
    var result=e.target.result;
    $(contenImg).html("<img src='"+result+"' id='"+idImgEle+"' width='100%' height='100px'>")
  }
</script>

<script type="text/javascript" src="./assets/js/<?php echo $_GET['view'] ?>.js"></script>
