<div class="col-md-12 spacing-4" style="display: none;" id="content-DelegacionModalidad2"><!--AVANCES-DELEGACION-->
	<div class="row">
		<div class="col-md-4 table-responsive boderSALL"  id="tableAvancesDelegacionM">
			Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
		</div>
		<div class="col-md-8 text-center boderSALL">
			<h3 id="titleGraficaDelegacionM">OFICINAS REGIONALES.</h3>
			<div class="col-md-12 text-left" id="btnBackEventos">
				<button class="btn btn-outline-secondary" id="btnBack-Eventos">
					<i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
				</button>
			</div>
			<div id="graficaAvanceDelegacionM" class="table-responsive" style="height: 400px; padding-bottom: 50px;" >
				Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12 spacing-4" style="display: none;" id="content-Subdelegacion"><!--AVANCES-SUBDELEGACION-->
	<div class="row">
		<div class="col-md-4 table-responsive boderSALL"  id="tableAvancesSubdel">
			Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
		</div>
		<div class="col-md-8 text-center boderSALL">
			<h3 id="titleGraficaSubdel"></h3>
			<div class="col-md-12 text-left" id="btnBackDelegacion">
				<button class="btn btn-outline-secondary" id="btnBack-Delegacion">
					<i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
				</button>
			</div>
			<div id="graficaAvanceSubdel" class="table-responsive" style="height: 400px; padding-bottom: 50px;" >
				Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12 spacing-4" style="display: none;" id="content-Municipio"><!--AVANCES-MUNICIPIO-->
	<div class="row">
		<div class="col-md-4 table-responsive boderSALL"  id="tableAvancesMunicipio">
			Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
		</div>
		<div class="col-md-8 text-center boderSALL">
			<h3 id="titleGraficaMunicipio"></h3>
			<div class="col-md-12 text-left" id="btnBackMunicipio">
				<button class="btn btn-outline-secondary" id="btnBack-SubDelegacion">
					<i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
				</button>
			</div>
			<div id="graficaAvanceMunicipio" class="table-responsive" style="height: 400px; padding-bottom: 50px;" >
				Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12 spacing-4" style=" display: none;" id="content-Localidad"><!--AVANCES-LOCALIDAD-->
	<div class="row">
		<div class="col-md-4 table-responsive boderSALL"  id="tableAvancesLocalidad">
			Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
		</div>
		<div class="col-md-8 text-center boderSALL">
			<h3 id="titleGraficaLocalidad"></h3>
			<div class="col-md-12 text-left" id="btnBackLocalidad">
				<button class="btn btn-outline-secondary" id="btnBack-Municipio">
					<i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
				</button>
			</div>
			<div id="graficaAvanceLocalidad" class="table-responsive" style="height: 400px; padding-bottom: 50px;" >
				Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
			</div>
		</div>
	</div>
</div>