      <div class="row" id='dataGenerales'>

        <div class="col-md-6">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">PROGRAMA: </span>
            </div>
            <input type="text" class="form-control upperCase" name="programa" id='programa' placeholder="Nombre de Programa" aria-label="Username" aria-describedby="basic-addon1" required="true" onkeyup="upperCase('#programa');" autofocus>
          </div>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">DECRIPCIÓN: </span>
            </div>
            <textarea class="form-control upperCase" name="descPrograma" id="descPrograma" aria-label="With textarea" style="overflow:auto;resize:none" rows="9" required="true" placeholder="¿En qúe consiste el programa?" onkeyup="upperCase('#descPrograma');"></textarea>
          </div>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">MODALIDAD: </span>
            </div>
            <select class="custom-select" name="selectModalidad" id="selectModalidad" required="true">
              <option selected>Selecciona Opción...</option>
              <option value="1">Independiente</option>
              <option value="2">Acumulativo</option>
            </select>
          </div>
        </div>

        <div class="col-md-6">

          <div class="col-md-12 mb-3">
            <div class="row">
              <div class="col-md-3 img-thumbnail" style="height: 110px;" id="contentImg1">
              </div>
               <div class="col-md-9 pt-4">
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="img1" id="img1" required="true">
                    <label class="custom-file-label" for="img1">Elige una imágen (500 x 104) de tu equipo</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12 mb-3">
            <div class="row">
              <div class="col-md-3 img-thumbnail" style="height: 110px;" id="contentImg2">
              </div>
              <div class="col-md-9 pt-4">
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="img2" id="img2" required="true">
                    <label class="custom-file-label" for="img1">Elige una imágen (340 x 103) de tu equipo</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12 mb-3">
            <div class="row">
              <div class="col-md-3 img-thumbnail" style="height: 110px;" id="contentImg3">
              </div>
              <div class="col-md-9 pt-4">
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" name="img3" id="img3" required="true">
                    <label class="custom-file-label" for="img1">Elige una imágen (251 x 93) de tu equipo</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>	

        
			</div>


