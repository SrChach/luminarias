<div class="row">

	<div class="col border-only-right-Shadow mr-5">
		<label>Seleccione Tipo de Fases</label>
		<div class="input-group mb-3">
            <div class="input-group-prepend">
              	<span class="input-group-text">TIPOS DE FASES: </span>
            </div>
            <select class="custom-select" name="selectFase" id="selectFase">
            </select>
            <div class="invalid-feedback w-100 text-center">
        		Seleccione o ingrese fase.
      		</div>
        </div>
        <label>Ó Ingrese Nueva Fase</label>

        <div class="col-md-12 mb-3" style="margin:0px; padding:0px;">
        	<div class="row">
        		<div class="input-group col-md-10">
        			<div class="input-group-prepend">
              			<span class="input-group-text">NOMBRE DE FASE: </span>
		            </div>
            		<input type="text" class="form-control upperCase" id="newFase" onkeyup="upperCase('#newFase');">
            		<div class="invalid-feedback w-100 text-center">
        				Ingrese Fase
      				</div>
        		</div>
        		<div class="col-md-2">
        			<div class="row">
        				<div class="col">
              				<input type="button" value="Agregar" class="btn btn-outline-secondary" id="faseMore" style="width: 100%;">
        				</div>
        				<!--div class="col">
              				<input type="button" value="Etiquetas" class="btn btn-outline-secondary" id="etiquetaShow" onclick="">
        				</div-->	
        			</div>
        		</div>
        	</div>
        </div>
	</div>

	<div class="col">
		<div class="row">
			<div class="col">
				<label>FASES SELECCIONADAS:</label>
				<div class="row" id="fasesSelected">
				</div>
			</div>
			<div class="col">
				<label>ETIQUETAS SELECCIONADAS:</label>
				<div class="row table-responsive" id="etiquetasSelected" style='height: 170px;'>
				</div>
			</div>
		</div>
		<div class="row">
			<div class='col text-center'>
    			<input type='button' value='Grabar Fase' id='saveFaseEtiquetas' class='btn btn-secondary' style='display: none;'>
    		</div>	
		</div>
		<div class="row">
			<div class='col text-center'>
    			<input type='button' value='RESUMEN' id='resumen' class='btn btn-secondary' style='display: none;'>
    		</div>	
		</div>
	</div>

</div>

<script type="text/javascript">
	var _t = '<?php echo $csrf_token ?>';
	var fasesSelected = []; // SI EL ID DEL LA FASE SELECCIONADA ES NULL INSERTAR EN LA BASE DE DATOS (FASE)
	var fases;
	var resFases;
	var savefaseEtiquetas = [];
	var EventofaseEtiquetas = new Object();
	//var EventofaseEtiquetas = new Object();
	$(function () {
		var url="./controller/GeneralesController.php";
		objet={opcion:'generales',action:'tipoFase',csrf_token:_t};
		ajaxCallback(objet,url,function (respuesta){
              try{
              	resFases = JSON.parse(respuesta);
              	if (resFases.CODIGO==true) {
              		fases=resFases.DATOS.tipo;
              		var options = buildOptions(resFases.DATOS.fases,'FASE','IDFASE');
              		$('#selectFase').html(options);
              	}
              }
              catch(err){
              	notificar('ERROR! ','0','error');
              }
          });
	});

	$('#selectFase').change(function (e) {
		if (this.value!='') {
			fasesSelected.push({id:this.value,fase:fases[this.value][0].FASE});
			$('#fasesSelected').html(renderFasesSelected(fasesSelected));
			$('#newFase').removeClass('is-invalid');
			$('#selectFase').attr("disabled","true");
			$('#newFase').attr("disabled","true");
			$("#faseMore").attr("disabled","true");
			$('#selectFase').removeClass("is-invalid");
			showEtiquetas();
		}
	});

	$('#faseMore').click(function () {
		if ($('#newFase').val()==null|| $.trim( $('#newFase').val())=='') {
			$('#newFase').addClass('is-invalid');
		}else{
			$('#newFase').removeClass('is-invalid');
			$('#selectFase').removeClass("is-invalid");
			$('#newFase').attr("disabled","true");
			$("#faseMore").attr("disabled","true");
			$('#selectFase').attr("disabled","true");
			fasesSelected.push({id:null,fase:$('#newFase').val()});
			$('#fasesSelected').html(renderFasesSelected(fasesSelected));
			showEtiquetas();	
		}
	});

	

	function renderFasesSelected(fasesSelected) {
		var cadena="";
		fasesSelected.forEach(function (item, i, array) {
    		cadena += "<div class='col-md-12'>\
    						<div class='row'>\
    							<div class='col-md-7' id='itemEvento_"+i+"'> "+(i+1)+".- "+item.fase+" </div>\
    							<div class='col-md-5'>\
    								<a type='button' class='btn btn-danger' style='color:white' onclick='removeFaseSelected("+i+")'>\
    									<i class='fa fa-remove'></i>\
    								</a>\
    							</div>\
    						</div>\
    						<hr class='hrSeparator'>\
    					</div>\
    					";
		});
		$('#newFase').val('');
		return cadena;
	}

	function removeFaseSelected(i) {
		fasesSelected.splice(i, 1);
		$('#fasesSelected').html(renderFasesSelected(fasesSelected));
		$('#newFase').removeAttr("disabled");
		$("#faseMore").removeAttr("disabled");
		$('#selectFase').removeAttr("disabled");
		$('#contentEtiquetas').fadeOut('slow');
	}

	function showEtiquetas() {
		if (fasesSelected.length==0) {
			$('#newFase').addClass('is-invalid');
			$('#selectFase').addClass("is-invalid");
		}else{
			$('#selectFase').removeClass("is-invalid");
			$('#contentEtiquetas').fadeIn('slow');
		}
	}
	
	var fasesGeneral=[];
	var etiquetasGeneral=[];
	$('#saveFaseEtiquetas').click(function () {
		savefaseEtiquetas.push({fase:fasesSelected,etiquetas:etiquetaSelected});

		fasesGeneral.push(fasesSelected);
		etiquetasGeneral.push(etiquetaSelected);

		saveFaseEtiquetas();

		etiquetaSelected = [];
		fasesSelected = [];

		$('#contentEtiquetas').fadeOut('slow');
		$('#saveFaseEtiquetas').fadeOut('slow');
		$('#resumen').fadeIn('slow');
	});

	function saveFaseEtiquetas() {
		$('#fasesSelected').html('');
		$('#etiquetasSelected').html('');
		$('#newFase').removeAttr("disabled");
		$("#faseMore").removeAttr("disabled");
		$('#selectFase').removeAttr("disabled");
	}

	$('#resumen').click(function () {
		EventofaseEtiquetas.eventos = eventosSelected;
		EventofaseEtiquetas.faseEtiqueta = savefaseEtiquetas;
		showResumen();
	});
</script>





