<div class="row">

	<div class="col border-only-right-Shadow mr-5">
		<label>Seleccione Etiquetas</label>
		<div class="input-group mb-3">
            <div class="input-group-prepend">
              	<span class="input-group-text">GRUPO DE ETIQUETAS: </span>
            </div>
            <select class="custom-select" name="selectGroupEtiqueta" id="selectGroupEtiqueta">
            </select>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">ETIQUETAS: </span>
            </div>
            <select class="custom-select" name="selectEtiqueta" id="selectEtiqueta">
            </select>
        </div>
        
	</div>
    <div class="col"></div>

</div>



<script type="text/javascript">
    var _t = '<?php echo $csrf_token ?>';
    var etiquetaSelected = []; // SI EL ID DEL LA FASE SELECCIONADA ES NULL INSERTAR EN LA BASE DE DATOS (FASE)
    var etiqueta;
    var tipo;
    var resEt;
    $(function () {
        var url="./controller/GeneralesController.php";
        objet={opcion:'generales',action:'tipoEtiqueta',csrf_token:_t};
        ajaxCallback(objet,url,function (respuesta){
              try{
                resEt = JSON.parse(respuesta);
                if (resEt.CODIGO==true) {
                    etiqueta=resEt.DATOS.etiquetas;
                    tipo=resEt.DATOS.tipo;
                    var options = buildOptions(resEt.DATOS.grupo,'GRUPO','GRUPO');
                    $('#selectGroupEtiqueta').html(options);
                }
              }
              catch(err){
                notificar('ERROR! ','0','error');
              }
          });
    });

    $('#selectGroupEtiqueta').change(function(e) {
        if (this.value=='') {
            $('#selectEtiqueta').html('');    
        }else{
            var options = buildOptions(etiqueta[this.value],'ETIQUETA','IDETIQUETA');
            $('#selectEtiqueta').html(options);    
        }
    });

    $('#selectEtiqueta').change(function(e) {
        if (this.value!='') {
            etiquetaSelected.push({id:this.value,etiqueta:tipo[this.value][0].ETIQUETA});
            $('#etiquetasSelected').html(renderEtiquetasSelected(etiquetaSelected)); 
        }
    });

    $('#etiquetaMore').click(function () {
        if ($('#newEtiqueta').val()==null|| $.trim( $('#newEtiqueta').val())=='') {
            $('#newEtiqueta').addClass('is-invalid');
        }else{
            $('#newEtiqueta').removeClass('is-invalid');
            etiquetaSelected.push({id:null,etiqueta:$('#newEtiqueta').val()});
            $('#etiquetasSelected').html(renderEtiquetasSelected(etiquetaSelected)); 
        }
    });

    function renderEtiquetasSelected(etiquetaSelected) {
        var cadena="";
        if (etiquetaSelected.length!=0) {
            etiquetaSelected.forEach(function (item, i, array) {
                cadena += "<div class='col-md-12'>\
                                <div class='row'>\
                                    <div class='col-md-7' id='itemEvento_"+i+"'> "+(i+1)+".- "+item.etiqueta+" </div>\
                                    <div class='col-md-5'>\
                                        <a type='button' class='btn btn-danger' style='color:white' onclick='removeEtiquetaSelected("+i+")'>\
                                            <i class='fa fa-remove'></i>\
                                        </a>\
                                    </div>\
                                </div>\
                                <br>\
                            </div>";
            });
            $('#newEtiqueta').val('');
            $('#saveFaseEtiquetas').fadeIn('slow');
        }
        return cadena;
    }

    function removeEtiquetaSelected(i) {
        etiquetaSelected.splice(i, 1);
        if (etiquetaSelected.length==0) {
            $('#saveFaseEtiquetas').fadeOut('slow');
        }
        $('#etiquetasSelected').html(renderEtiquetasSelected(etiquetaSelected));  
    }


</script>
