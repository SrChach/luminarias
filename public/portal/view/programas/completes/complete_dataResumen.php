<div class="row">
	<div class="col">
		<table class="table table-bordered">
			<thead>
			    <tr>
			    	<th scope="col">PROGRAMA</th>
			      	<th scope="col">EVENTOS</th>
			      	<th scope="col">FASES</th>
			      	<th scope="col">ETIQUETAS</th>
			    </tr>
  			</thead>
  			<tbody>
  				<tr>
  					<td style="vertical-align: middle;">
  						<span id="programaName"></span>
  					</td>
  					<td style="vertical-align: middle;" id="eventosResumen">
  						
  					</td>
  					<td colspan="2">
  						<table width="100%" class="table">
  							<tbody id="fasesResumen">
  							</tbody>
  						</table>
  					</td>
  				</tr>
  			</tbody>
		</table>
	</div>
</div>
<div class="row pt-5">
    <div class="col-md-6 offset-md-3 mb-3 text-center">
        <input type="buttom" class="btn btn-outline-primary btn-sm" value="REVISAR" onclick="ok(false);">
        <input type="submit" class="btn btn-outline-primary btn-sm" value="ENVIAR DATOS">
        <input type="buttom" class="btn btn-outline-primary btn-sm" value="CANCELAR" onclick="history.go(0);">
	</div>
</div>

<script type="text/javascript">
	function showResumen() {
		$('#contentResumen').fadeIn(5000);
		$('#programaName').html("<b>"+$('#programa').val()+"</b>");
		$('#eventosResumen').html(resumenEventos(EventofaseEtiquetas.eventos));
		$('#fasesResumen').html(resumenFases(EventofaseEtiquetas.faseEtiqueta));

		ok(true);
	}

	function resumenEventos(eventos) {
		var cadena="";
		eventos.forEach(function (item, i, array) {
    		cadena += "<p><b> "+(i+1)+".- </b> "+item.evento+"</p>\
    					<hr>";
		});
		return cadena;
	}
	function resumenFases(fases) {
		var cadena="";
		fases.forEach(function (item, i, array) {
    		cadena += "<tr>\
    					<td style='vertical-align: middle; width:40%'><b>"+(i+1)+".-</b>"+item.fase[0].fase+"</td>\
    					";
    		cadena += "<td style='vertical-align: middle;width:60%;'>"
    		item.etiquetas.forEach(function (itemE,j,arrayE) {
				cadena += "<p><b> "+(j+1)+".- </b> "+itemE.etiqueta+"</p>\
    					";	
			});
			cadena += "</td>\
						</tr>";
		});
		return cadena;
	}

	function ok (ok){
		if (ok) {
			$('#contentEventos').fadeOut('slow');
			$('#contentFases').fadeOut('slow');
			$('#contentEtiquetas').fadeOut('slow');
		}else{
			$('#contentEventos').fadeIn('slow');
			$('#contentFases').fadeIn('slow');
			$('#contentEtiquetas').fadeIn('slow');
			$('#contentResumen').fadeOut('slow');
		}
	}
	
</script>