<div class="row">
	<div class="col border-only-right-Shadow mr-5">
		
		<label>Seleccione Tipo de Evento</label>
		<div class="input-group mb-3">
            <div class="input-group-prepend">
              	<span class="input-group-text">TIPOS DE EVENTO: </span>
            </div>
            <select class="custom-select" name="selectTipoEvento" id="selectTipoEvento">
            </select>
        </div>
        <label>Ó Ingrese Nuevo Evento</label>

        <div class="col-md-12 mb-3" style="margin:0px; padding:0px;">
        	<div class="row">
        		<div class="input-group col-md-10">
        			<div class="input-group-prepend">
              			<span class="input-group-text">NOMBRE DE EVENTO: </span>
		            </div>
            		<input type="text" class="form-control upperCase" id="newTipoEvento" onkeyup="upperCase('#newTipoEvento');">
            		<div class="invalid-feedback w-100 text-center">
        				Ingrese tipo de evento
      				</div>
        		</div>
        		<div class="col-md-2">
        			<div class="input-group" style="font-size: 8px;">
              			<input type="button" value="Agregar" class="btn btn-outline-secondary" id="eventMore" style="width: 100%;">
        			</div>
        		</div>
        	</div>
        </div>

	</div>
	<div class="col mb-3 ">
		<label>EVENTOS SELECCIONADOS:</label>
		<div class="row table-responsive" id="evetosSelected" class="pl-5" style="height: 170px;">
			
		</div>
	</div>
</div>

<div class="row pt-5" id="saveOption2">
	<div class="col-md-6 offset-md-3 mb-3 text-center">
		<input type="submit" class="btn btn-outline-primary btn-sm" value="ENVIAR DATOS">
	</div>
</div>

<script type="text/javascript">
	var _t = '<?php echo $csrf_token ?>';
	var eventosSelected = []; // SI EL ID DEL EVENOTO SELECCIONADO ES NULL INSERTAR EN LA BASE DE DATOS (TIPOEVENTO)
	var eventos;
	var resEventos;
	$(function () {
		var url="./controller/GeneralesController.php";
		objet={opcion:'generales',action:'tipoEvento',csrf_token:_t};
		ajaxCallback(objet,url,function (respuesta){
              try{
              	resEventos = JSON.parse(respuesta);
              	if (resEventos.CODIGO==true) {
              		eventos=resEventos.DATOS.tipo;
              		var options = buildOptions(resEventos.DATOS.eventos,'TIPOEVENTO','IDTIPOEVENTO');
              		$('#selectTipoEvento').html(options);
              	}
              }
              catch(err){
              	notificar('ERROR! ','0','error');
              	console.log(err);
              }
          });
	});

	$('#selectTipoEvento').change(function (e) {
		if (this.value!='') {
			eventosSelected.push({id:this.value,evento:eventos[this.value][0].TIPOEVENTO});
			$('#evetosSelected').html(renderEventosSelected(eventosSelected));
			if (modalidadGeneral==2) {
				showFases();
			}
		}
	});

	$('#eventMore').click(function () {
		if ($('#newTipoEvento').val()==null|| $.trim( $('#newTipoEvento').val())=='') {
			$('#newTipoEvento').addClass('is-invalid');
		}else{
			$('#newTipoEvento').removeClass('is-invalid');
			eventosSelected.push({id:null,evento:$('#newTipoEvento').val()});
			$('#evetosSelected').html(renderEventosSelected(eventosSelected));
			if (modalidadGeneral==2) {
				showFases();
			}
		}
	});

	

	function renderEventosSelected(eventosSelected) {
		var cadena="";
		eventosSelected.forEach(function (item, i, array) {
    		cadena += "<div class='col-md-12'>\
    						<div class='row'>\
    							<div class='col-md-7' id='itemEvento_"+i+"'> "+(i+1)+".- "+item.evento+" </div>\
    							<div class='col-md-5'>\
    								<a type='button' class='btn btn-danger' style='color:white' onclick='removeEventoSelected("+i+")'>\
    									<i class='fa fa-remove'></i>\
    								</a>\
    							</div>\
    						</div>\
    					</div>";
		});
		$('#newTipoEvento').val('');
		return cadena;
	}

	function removeEventoSelected(i) {
		eventosSelected.splice(i, 1);
		$('#evetosSelected').html(renderEventosSelected(eventosSelected));	
		if (modalidadGeneral==2) {
				showFases();
			}
	}

	function showFases() {
		if (eventosSelected.length!=0) {
			$('#contentFases').fadeIn('slow');
		}else{
			$('#contentFases').fadeOut('slow');
		}
	}
</script>