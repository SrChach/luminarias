
<?php
ini_set('memory_limit', -1);
ignore_user_abort(1);
ini_set('max_execution_time', 300);




ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');
	include_once './view/programas/modalReportes.php';
?>

<!--link rel="stylesheet" type="text/css" href="./assets/css/meta-contents.css"-->

<!--link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script-->



	<div class="container-fluid" style="font-size: 12px;">
		<div class="row">
			<div class="col" id="contenedorProgramas">
				<div class="row" id="contenedorGraficas">
					<div class="col-12 mb-5">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">

							</div>
						</nav>
					</div>
					<input type="hidden" value="<?php echo $_SESSION['IDUSUARIO']; ?>" id="idUsuarioLogin">

					<div class="col-md-8">
						<div class="row">
							<div class="col-md-2" style="color:#7f8a8e;">
								<br>
								<h3>AVANCES</h3>

							</div>
							<div class="col-md-10" id="Filtros" style="display: none;">
								<div class="row">
									<div class="col-md-3">
										INICIO <br>
										<!--input type="date" class="form-control"-->
										<select class="custom-select" id="optionsStart">
											<option selected value="">Selecione...</option>
										</select>
									</div>
									<div class="col-md-3">
										FIN <br>
										<!--input type="date" class="form-control"-->
										<select class="custom-select" id="optionsEnd">
											<option selected value="">Selecione...</option>
										</select>
									</div>
								<div class="col-md-3" id="content-Entregas">
									ENTREGA
									<select class="custom-select" id="entregaSelected">
									</select>
								</div>
								<div class="col-md-3">
									<br>
									<input type="button" value="FILTRAR" class="btn btn-outline-primary" id="filtro">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class='col-md-2'>
					<div class='col-md-12 col-sm-12 col-12'>
						<div class='small-box bg-info hover my_Box2'>
							<div class='inner'>
								<h4 id="titleMeta">META</h4>
								<h4 id="meta"> </h4>
							</div>
							<div class='icon'>
								<i class='icon ion-arrow-graph-up-right'></i>
							</div>
						</div>
					</div>
				</div>
				<div class='col-md-2' style="display:none;" id="content-Target-Avance">
					<div class='col-md-12 col-sm-12 col-12'>
						<div class="small-box bg-success hover my_Box2">
							<div class='inner'>
								<h4 id="titleAvances">AVANCES</h4>
								<h4 id="avance"> </h4>
							</div>
							<div class='icon'>
								<i class="ion ion-stats-bars"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row" id="contentNull" style="display: none;">
				<div class="col-12 alert-warning">

				</div>
			</div>
			<div class="row spacing-4" id="content">
				<div class="col-md-12">
					<div class="row" id="dataNull">
					</div>
				</div>
				<div class="col-4" style="display: none;" id="optionsEventos">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" style="font-size: 12px;" for="eventosSelected"><i class="" id="loadSubdel"></i>Evento: </label>
						</div>
						<select class="custom-select" id="eventosSelected">

						</select>
					</div>
				</div>
				<div class="col-md-12" id="content-AvancesGeneral"><!--AVANCES-->
					<div class="row">
						<div class="col-md-4 table-responsive boderSALL" id="tableAvances">
							Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>
						</div>
						<div class="col-md-8 text-center boderSALL">
							<h3 id="titleGraficaGeneral">EVENTOS.</h3>
							<div id="graficaAvance" class="table-responsive" style="height: 400px; padding-bottom: 50px;font-family: 'NeoSansPro-Bold'" >
								Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
							</div>
						</div>
					</div>
				</div>
				<?php
					//if ($_GET['index']==3||$_GET['index']==2) {
				include_once './view/programas/completes/complete_canasta.php';
					//}
				?>
				<?php //if ($_GET['index']!=3 and $_GET['index']!=2): ?>
				<div class="col" style="display: none;" id="moreDatas">
					<div class="col-md-12">
						<button type="button" class="btn btn-outline-info" id="btnMore">
							<i class="fa fa-angle-double-down" style="color:#276092;" id="iconMore"></i>
						</button>
					</div>
				</div>
				<?php //endif ?>
				<div class="col-md-12 spacing-7" id="contentExpedientes" style="display: none;"><!--EXPEDIENTES-->
					<div class="row">
						<div class="col-md-12">
							<h3>Conformación de Expedientes</h3>
						</div>
						<div class="col-md-4 table-responsive">
							<table class="table table-bordered table-striped table-hover" id="table2">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">First</th>
										<th scope="col">Last</th>
										<th scope="col">Handle</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">1</th>
										<td>Mark</td>
										<td>Otto</td>
										<td>@mdo</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-8 table-responsive" id="grafica2" style="height: 250px;">
							Se mostrará otra gráfica!
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<!--div class="row">
	<div class="col-md-12 table-responsive" id="ejemplo">
		hola
	</div>
</div-->
<script type="text/javascript">
	var origen='';
	var inicio='';
	var entrega=null;
	var fin='';
	var idGeneral='';
	var titleGeneral='';
	var idP=<?php echo $_GET['index'] ?>;
	var modalidad=null;
	var namePrograma=null;
	var eventoG=null;
	var datasTable=null;
	var idDelegacion;
	var titleDelegacion;
	var idSubdelegacion;
	var titleSubdelegacion;;
	var idMunicipio;
	var titleMunicipio;
	var idLocalidad;
	$(function () {
		var url="./controller/GeneralesController.php"
		objet={opcion:'generales',action:'getNamePrograma',csrf_token:'<?php echo $csrf_token ?>',idPrograma:<?php echo $_GET['index'] ?>};
		ajaxCallback(objet,url,function (respuesta){
			res = JSON.parse(respuesta);
			if (res.CODIGO==true) {
				$('#contentNull').hide('slow');
				$('#content').show('slow');
				$('#titlePage-2').html(" "+res.DATOS[0]['NOMBREPROGRAMA']);
				namePrograma=res.DATOS[0]['NOMBREPROGRAMA'];
				modalidad=res.DATOS[0].MODALIDAD;
				Tabs(idP);
				btnPadron();
			}else{
				$('#contentNull').show('slow');
				$('#content').hide('slow');
				showAlertInwindow('#contentNull','warning',res.DATOS);
				$('#titlePage-2').html('PROGRAMA: NO EXISTE');
			}
		});

	});
	var resTabs;
	function Tabs(idP) {
		var url="./controller/GeneralesController.php"
		objet={opcion:'generales',action:'getPestañas',csrf_token:'<?php echo $csrf_token ?>',idPrograma:idP};
		var tabs="";
		var res=JSON.parse(ajaxCallback(objet,url));
		if (res.CODIGO==true) {
			$('#contentNull').hide('slow');
			$('#content').show('slow');
			resTabs=res.DATOS;
			eventoG=res.DATOS[0].IDTIPOEVENTO;
			res.DATOS.forEach(function(item,i){

				if (i==0) {
					tabs += `<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" onclick="Index(${modalidad},${item.IDTIPOEVENTO})">${item.TIPOEVENTO}</a>`;
				}else{
					tabs += `<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" onclick="Index(${modalidad},${item.IDTIPOEVENTO})">${item.TIPOEVENTO}</a>`;
				}
			});
			$('#nav-tab').html(tabs);
			Index(modalidad,eventoG);
		}else{
			$('#contentNull').show('slow');
			$('#content').hide('slow');
			showAlertInwindow('#contentNull','warning',res.DATOS);
			$('#titlePage-2').html('PROGRAMA: NO EXISTE');
		}
	}


	function btnPadron(){
		var complete='';
		var btnReportes='';
		/*if (idP==3) {
			complete="<div class='offset-md-6 col-md-6 mt-3 text-left'>\
			<a href='?view=programas&index=<?php echo $_GET['index'] ?>&act=files' type='button' class='btn btn-outline-secondary btn-block'>Documetación <i class='fa fa-arrow-circle-right'></i></a>\
			</div>";
		}*/


		// if (idP==3) {
		// 	btnReportes=`<div class='offset-md-6 col-md-6 mt-3 text-left'>
		// 	<button type="button" class="btn btn-outline-secondary btn-block"  data-toggle="modal" data-target="#modalReportes" onclick="traeReportes();">Reportes
		// 	<i class='fa fa-arrow-circle-right'></i></button>
		// 	</div>
		// 	`;
		// }


		$('#subItemsTitle').html("\
			<div class='offset-md-6 col-md-6 text-left'>\
			<a href='?view=padron&index=<?php echo $_GET['index'] ?>' type='button' class='btn btn-outline-secondary btn-block'>Padron <i class='fa fa-arrow-circle-right'></i></a>\
			</div>"+complete+btnReportes);
	}

	var aux=0;
	$( "#btnMore" ).on( "click", function() {
		if (aux==1) {
			table.destroy();
			$('#iconMore').removeClass('fa fa-angle-double-up').addClass('fa fa-angle-double-down');
			aux=0;
			$("#contentExpedientes").fadeOut('slow');
		}else{
			$('#iconMore').removeClass('fa fa-angle-double-down').addClass('fa fa-angle-double-up');
			//$('#contentExpedientes').show();
			$("#contentExpedientes").fadeIn(2000);
			tablePadron('#table2','Avance2',4);
			settingsTable();
			$('#table2_info').addClass('table-responsive');
			//graphExpedientes();
			aux=aux+1;
		}
	});




	function Index(modalidad,evento='') {
		$('#btnBack-Eventos').tooltip({title: 'REGRESAR A EVENTOS'});
		$('#btnBack-Delegacion').tooltip({title: 'REGRESAR A DELEGACIONES'});
		$('#btnBack-SubDelegacion').tooltip({title: 'REGRESAR A SUBDELEGACIÓNES'});
		$('#btnBack-Municipio').tooltip({title: 'REGRESAR A MUNICIPIOS'});

		$('#content-DelegacionModalidad2').hide();
		$('#content-Subdelegacion').hide();
		$('#content-Municipio').hide();
		$('#content-Localidad').hide();

		load();
		$('#content-AvancesGeneral').fadeIn(1000);
		eventoG=evento;
		if (modalidad==2) {
			var url="./controller/ProgramasController.php";
			objet={opcion:'Programas',action:'generalesPrograma',csrf_token:'<?php echo $csrf_token ?>',id:idP,evento:evento};
			ajaxCallback(objet,url,function (respuesta){
				res = JSON.parse(respuesta);
				if (res.CODIGO==true) {
					$('#contentNull').hide('slow');
					$('#content').show('slow');
					var data=tableAvances(res.DATOS,idP,'','',modalidad);
					$('#meta').html(separa(data[1]));
					$('#content-Target-Avance').fadeIn();
					$('#avance').html(separa(data[2]));
                	//tablePadron('#table1','Avance',4);
                	datasTable=data[0];

                	renderTable('#tableAvances',datasTable,idP,1,2,'','',modalidad);
                	//tablePadron('#table-dinamic','Avance',(Object.keys(datasTable[idP])).length);
                	tableExport2('#table-dinamic','AVANCE-GENERAL');
                	settingsTable();
                	graficas('graficaAvance',data[0],'EVENTO',['AVANCE','RESTA'],true);
                	$('#table1_info').addClass('table-responsive');
                	$('#eventosSelected').html(buildOptions(datasTable,'EVENTO','IDEVENTO'));
                	$('#optionsEventos').fadeIn(3000);
					//$('#moreDatas').show();
				}else{
					$('#contentNull').show('slow');
					$('#content').hide('slow');
					showAlertInwindow('#contentNull','warning',res.DATOS);
					$('#titlePage').html('PROGRAMA: NO EXISTE');
				}
				load(false);
			});
		}
		if (modalidad==1) {
			origen="delegacion";
			var url="../../pillar/clases/controlador_pruebas.php";
			objet={opcion:'generales_programa',idPrograma:idP,evento:evento,entrega:entrega};
			ajaxCallback(objet,url,function (respuesta){
				res = JSON.parse(respuesta);
				if (res.datos.length==0) {
					showAlertInwindow('#content-AvancesGeneral','warning','Sin Datos');
				}else{
					var data=tableAvances(res.datos,idP,'ID_DEL','DELEGACION',modalidad);
					var optionsInicio=buildOptions(res.fechas,'FECHA','FECHA');
					$('#optionsStart').html(optionsInicio);
					$('#optionsEnd').html(optionsInicio);
					$('#titleMeta').html('PADRÓN');
					var meta=0;
					res.generales[idP].forEach(function (item,i) {
						if (item.IDTIPOEVENTO==eventoG) {
							meta=item.CANTIDAD;
						}
					});
					$('#meta').html(separa(meta));
					$('#titleGraficaGeneral').html("OFICINAS REGIONALES.");

					renderTable('#tableAvances',data[0],idP,1,1,'','subdelegacion','',modalidad);
					tableExport2('#table-dinamic','AVANCE-DELEGACIÓN');
					settingsTable();
					console.log(data[0]);
					graficaAvance('subdelegacion','delegacion',idP,'graficaAvance',data[0],'DELEGACIÓN',['AVANCE','META'],false);
					if (res.entregas.length>1) {
						$('#entregaSelected').html(buildOptions(res.entregas,'ENTREGA','ENTREGA'));
						$('#content-Entregas').show('slow');
					}else{
						$('#entregaSelected').html('');
						$('#content-Entregas').hide('slow');
					}

					$('#Filtros').fadeIn('slow');
				}
				load(false);
			});
		}
	}


	/*--FILTROS--*/
	$( "#optionsStart" ).change(function() {
		inicio=$(this).val();
	});

	$( "#optionsEnd" ).change(function() {
		fin=$(this).val();
	});

	$('#entregaSelected').change(function() {
		entrega=this.value;
	});

	$('#filtro').click(function () {

		if (inicio=='' && fin=='') {
			if (origen=='delegacion') {
				Index(modalidad,eventoG);
			}
			if (origen=='subdelegacion') {
				callNewgrafica('subdelegacion',idDelegacion,titleDelegacion,idP);
			}
			if (origen=='municipio') {
				callNewgrafica('municipio',idSubdelegacion,titleSubdelegacion,idP);
			}
			if (origen=='localidad') {
				callNewgrafica('localidad',idMunicipio,titleMunicipio,idP);
			}
		}else{
			callAvanceFiltro(origen);
		}
	});

	function callAvanceFiltro(origen){

		load();
		if (origen=='delegacion') {
			$('#content-AvancesGeneral').fadeOut(500);
			$('#tableAvances').html('Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');
			$('#graficaAvance').html('Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');
			var url="../../pillar/clases/controlador_pruebas.php";
			objet={opcion:'generales_programa',idPrograma:idP,fechaInicio:inicio,fechaTermino:fin,evento:eventoG};

			ajaxCallback(objet,url,function (respuesta){
				$('#content-AvancesGeneral').fadeIn(1000);
				res = JSON.parse(respuesta);
				if (res.datos.length==0) {
					$('#contentNull').show('slow');
					$('#content').hide('slow');
					showAlertInwindow('#contentNull','warning','Sin Datos');
				}else{
					$('#contentNull').hide('slow');
					$('#content').show('slow');
					var data=tableAvances(res.datos,idP,'ID_DEL','DELEGACION',modalidad);

					$('#titleMeta').html('PADRÓN');
					$('#meta').html(separa(res.generales[idP][0].CANTIDAD));
					$('#titleGraficaGeneral').html("OFICINAS REGIONALES.");
					renderTable('#tableAvances',data[0],idP,1,1,'','subdelegacion',modalidad);
					tableExport2('#table-dinamic','AVANCE-DELEGACIÓN');
					settingsTable();
					graficaAvance('subdelegacion','delegacion',idP,'graficaAvance',data[0],'DELEGACIÓN',['AVANCE','META'],false);
					$('#Filtros').fadeIn('slow');
				}
				load(false);

			});
		}
		if (origen=='subdelegacion') {
				callNewgrafica('subdelegacion',idDelegacion,titleDelegacion,idP);
				load(false);
		}
		if (origen=='municipio') {
				callNewgrafica('municipio',idSubdelegacion,titleSubdelegacion,idP);
				load(false);
		}
		if (origen=='localidad') {
				callNewgrafica('localidad',idMunicipio,titleMunicipio,idP);
				load(false);
		}
	}

	var resDelegaciones;
	var index;
	$('#eventosSelected').change(function(){

		if (this.value==''||this.value==null) {
			Index(modalidad,eventoG);
		}else{
			load();
			index=this.value-1;
			$('#content-AvancesGeneral').fadeOut(1000);
			objet={opcion:'ProgramasModalidad2',action:'getAvanceDelegacionModalidad2',csrf_token:'<?php echo md5(uniqid('', true)); ?>',idprograma:datasTable[index].IDPROGRAMA,evento:datasTable[index].IDEVENTO,fase:eventoG,del:'',subdel:'',mun:''};
			$.post('./controller/ProgramasController.php', objet, function(response, textStatus, xhr) {
				try{
					resDelegaciones = $.parseJSON(response);
					if (resDelegaciones.CODIGO==true) {
						$('#contentNull').hide('slow');
						$('#content-DelegacionModalidad2').fadeIn(2000);
						renderTable('#tableAvancesDelegacionM',resDelegaciones.DATOS,idP,1,1,'table-delegacionesM2','subdelegacion',{org:'programas',click:'subdelegacionM2'},modalidad);
						var faseName;
						$.each(resTabs, function(i, item) {
							if (eventoG==item.IDTIPOEVENTO) {
								faseName=item.TIPOEVENTO;
							}
						});
						tableExport2('#table-delegacionesM2','AVANCES POR DELEGACIÓN EVENTO: '+datasTable[index].EVENTO+', FASE: '+faseName);
						settingsTable();
						graficaDelegacionesM2('graficaAvanceDelegacionM',resDelegaciones.DATOS,'DELEGACIÓN',['OBRAS','AVANCE','VISITAS'],true);

					}else{
						$('#contentNull').show('slow');
						$('#content').hide('slow');
						showAlertInwindow('#contentNull','warning',res.DATOS);
					}
				}catch(err){
					console.log(err);
				}
				load(false);
			});
		}
	});

	function graficaDelegacionesM2(element='',data='',xkey='',ykey='',stack='') {
		$('#'+element).html('');
		if (stack=='') {
			stack=false;
		}
		new Morris.Bar({
			element: element,
			data: data,
			hoverCallback: function(index, options, content,row) {
				var aux= content.split("row-label'>");
				content = aux[0]+"row-label'>OFICINA REGIONAL: "+aux[1];
				content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
				$('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');

				return content;
			},
			xkey: xkey,
			ykeys: ykey,
			stacked: stack,
			resize:true,
			xLabelAngle:45,
			labels: ykey
        	//barColors: colorsBars
        });
	}

	var resSubdelegaciones;
	function subdelegacionM2(org,delegacion,title) {
		load();
		$('#content-DelegacionModalidad2').fadeOut(1000);
		objet={opcion:'ProgramasModalidad2',action:'getAvanceSubDelegacionModalidad2',csrf_token:'<?php echo md5(uniqid('', true)); ?>',idprograma:datasTable[index].IDPROGRAMA,evento:datasTable[index].IDEVENTO,fase:eventoG,del:delegacion,subdel:'',mun:''};

		$.post('./controller/ProgramasController.php', objet, function(response, textStatus, xhr) {
			try{
				resSubdelegaciones = $.parseJSON(response);
				if (resSubdelegaciones.CODIGO==true) {
					$('#contentNull').hide('slow');
					$('#content-Subdelegacion').fadeIn(2000);
					renderTable('#tableAvancesSubdel',resSubdelegaciones.DATOS,idP,1,1,'table-SubdelegacionesM2','municipio',{org:'programas',click:'municipioM2'},modalidad);
					var faseName;
					$.each(resTabs, function(i, item) {
						if (eventoG==item.IDTIPOEVENTO) {
							faseName=item.TIPOEVENTO;
						}
					});
					tableExport2('#table-SubdelegacionesM2','AVANCES POR SUBDELEGACIÓN EVENTO: '+datasTable[index].EVENTO+', FASE: '+faseName);
					settingsTable();
					graficaSubDelegacionesM2('graficaAvanceSubdel',resSubdelegaciones.DATOS,'SUBDELEGACIÓN',['OBRAS','AVANCE','VISITAS'],true);
					$('#titleGraficaSubdel').html('OFICINA REGIONAL: ' + title + '.');

				}else{
					$('#contentNull').show('slow');
					$('#content').hide('slow');
					showAlertInwindow('#contentNull','warning',res.DATOS);
				}
			}catch(err){
				console.log(err);
			}
			load(false);
		});
	}
	function graficaSubDelegacionesM2(element='',data='',xkey='',ykey='',stack='') {
		$('#'+element).html('');
		if (stack=='') {
			stack=false;
		}
		new Morris.Bar({
			element: element,
			data: data,
			hoverCallback: function(index, options, content,row) {
				var aux= content.split("row-label'>");
				content = aux[0]+"row-label'>OFICINA SUBREGIONAL: "+aux[1];
				content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
				$('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');

				return content;
			},
			xkey: xkey,
			ykeys: ykey,
			stacked: stack,
			resize:true,
			xLabelAngle:45,
			labels: ykey
        	//barColors: colorsBars
        });
	}

	var resMunicipios;
	function municipioM2(org,subdelegacion,title) {
		load();
		$('#content-Subdelegacion').fadeOut(1000);
		objet={opcion:'ProgramasModalidad2',action:'getAvanceMunicipioModalidad2',csrf_token:'<?php echo md5(uniqid('', true)); ?>',idprograma:datasTable[index].IDPROGRAMA,evento:datasTable[index].IDEVENTO,fase:eventoG,del:'',subdel:subdelegacion,mun:''};

		$.post('./controller/ProgramasController.php', objet, function(response, textStatus, xhr) {
			try{
				resMunicipios = $.parseJSON(response);
				if (resMunicipios.CODIGO==true) {
					$('#contentNull').hide('slow');
					$('#content-Municipio').fadeIn(2000);
					renderTable('#tableAvancesMunicipio',resMunicipios.DATOS,idP,1,1,'table-MunicipiosM2','localidad',{org:'programas',click:'localidadM2'},modalidad);
					var faseName;
					$.each(resTabs, function(i, item) {
						if (eventoG==item.IDTIPOEVENTO) {
							faseName=item.TIPOEVENTO;
						}
					});
					tableExport2('#table-MunicipiosM2','AVANCES POR MUNICIPIO EVENTO: '+datasTable[index].EVENTO+', FASE: '+faseName);
					settingsTable();
					graficaMunicipiosM2('graficaAvanceMunicipio',resMunicipios.DATOS,'MUNICIPIO',['OBRAS','AVANCE','VISITAS'],true);
					$('#titleGraficaMunicipio').html('OFICINA SUBREGIONAL: ' + title + '.');

				}else{
					$('#contentNull').show('slow');
					$('#content').hide('slow');
					showAlertInwindow('#contentNull','warning',res.DATOS);
				}
			}catch(err){
				console.log(err);
			}
			load(false);
		});
	}
	function graficaMunicipiosM2(element='',data='',xkey='',ykey='',stack='') {
		$('#'+element).html('');
		if (stack=='') {
			stack=false;
		}
		new Morris.Bar({
			element: element,
			data: data,
			hoverCallback: function(index, options, content,row) {
				var aux= content.split("row-label'>");
				content = aux[0]+"row-label'>MUNICIPIO: "+aux[1];
				content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
				$('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');

				return content;
			},
			xkey: xkey,
			ykeys: ykey,
			stacked: stack,
			resize:true,
			xLabelAngle:45,
			labels: ykey
        	//barColors: colorsBars
        });
	}

	var resLocalidades;
	function localidadM2(org,municipio,title) {
		load();
		$('#content-Municipio').fadeOut(1000);
		objet={opcion:'ProgramasModalidad2',action:'getAvanceLocalidadModalidad2',csrf_token:'<?php echo md5(uniqid('', true)); ?>',idprograma:datasTable[index].IDPROGRAMA,evento:datasTable[index].IDEVENTO,fase:eventoG,del:'',subdel:'',mun:municipio};

		$.post('./controller/ProgramasController.php', objet, function(response, textStatus, xhr) {
			try{
				resLocalidades = $.parseJSON(response);
				if (resLocalidades.CODIGO==true) {
					$('#contentNull').hide('slow');
					$('#content-Localidad').fadeIn(2000);
					renderTable('#tableAvancesLocalidad',resLocalidades.DATOS,idP,1,1,'table-LocalidadesM2','',{org:'',click:''},modalidad);
					var faseName;
					$.each(resTabs, function(i, item) {
						if (eventoG==item.IDTIPOEVENTO) {
							faseName=item.TIPOEVENTO;
						}
					});
					tableExport2('#table-LocalidadesM2','AVANCES POR LOCALIDADES EVENTO: '+datasTable[index].EVENTO+', FASE: '+faseName);
					settingsTable();
					graficaLocalidadesM2('graficaAvanceLocalidad',resLocalidades.DATOS,'LOCALIDAD',['OBRAS','AVANCE','VISITAS'],true);
					$('#titleGraficaLocalidad').html('MUNICIPIO: ' + title + '.');

				}else{
					$('#contentNull').show('slow');
					$('#content').hide('slow');
					showAlertInwindow('#contentNull','warning',res.DATOS);
				}
			}catch(err){
				console.log(err);
			}
			load(false);
		});
	}

	function graficaLocalidadesM2(element='',data='',xkey='',ykey='',stack='') {
		$('#'+element).html('');
		if (stack=='') {
			stack=false;
		}
		new Morris.Bar({
			element: element,
			data: data,
			hoverCallback: function(index, options, content,row) {
				var aux= content.split("row-label'>");
				content = aux[0]+"row-label'>LOCALIDAD: "+aux[1];
				content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
				$('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');

				return content;
			},
			xkey: xkey,
			ykeys: ykey,
			stacked: stack,
			resize:true,
			xLabelAngle:45,
			labels: ykey
        	//barColors: colorsBars
        });
	}

	$('#btnBack-Eventos').click(function() {
		$('#content-DelegacionModalidad2').fadeOut(1000);
		$('#content-AvancesGeneral').fadeIn(2000);
		$(this).tooltip('hide');
	});
	$('#btnBack-Delegacion').click(function() {
		backLevel('delegacion');
		$(this).tooltip('hide');
	});
	$('#btnBack-SubDelegacion').click(function() {
		backLevel('subdelegacion');
		$(this).tooltip('hide');
	});
	$('#btnBack-Municipio').click(function() {
		backLevel('municipio');
		$(this).tooltip('hide');
	});



</script>



<script type="text/javascript" src='./assets/js/<?php echo $_GET['view'] ?>.js'></script>
