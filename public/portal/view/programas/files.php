
<div class="container-fluid">

	<form>
		<div class="form-row" id="select" >
			
		</div>
		<div class="form-row mb-2" >
			<div class="col">	
				<div id="load">
					CARGANDO <i class="fa fa-spinner fa-spin" style="font-size:30px"></i>
				</div>
			</div>
		</div>
		<div class="form-row mb-2" >
			<div class="col">	
				<input type="button" value="Buscar" id="btnSearch" class="btn btn-outline-primary" onclick="traePdf()">
			</div>
		</div>
	</form>
	<div class="row ">
		<div class="col-md-12">
			<div class="embed-responsive embed-responsive-16by9 col-md-12" id="iframe_pdf"></div>
		</div>
	</div>

</div>


<script type="text/javascript">

	var cveedo='',s_cveMun2='', s_nomMun='';
	var resEML, resTraeAnos;
	var url="./controller/VisorPdfController.php";

	$(function () {
		pintarSelect();
		cargaDatos();
	});



	function pintarSelect() {
		var pintar = [
		{id:'municipios',     valor:'Municipio',     evento:'traeAnos'},
		{id:'ano',            valor:'Año Visita',    evento:'traeMeses'},
		{id:'mes',            valor:'Mes',           evento:'traeVisitas'},
		{id:'entrega',        valor:'# Entrega',     evento:'comodin'}
		];

		var selects="";
		pintar.forEach(function(item,i){
			selects += `<div class="form-group col-md">
			<label for="${item.id}">${item.valor}</label>
			<select id="${item.id}" class="form-control" onchange="${item.evento}(this);">
			<option value="" selected>Seleccione ...</option>
			</select>
			</div>`;
		});
		document.getElementById("select").innerHTML = selects;
	}





	function cargaDatos(){
		
		var url="./controller/GeneralesController.php";
		objet={opcion:'generales',action:'infoPDF',idPrograma:'<?php echo $_GET['index'] ?>',csrf_token:'<?php echo $csrf_token ?>'};
		ajaxCallback(objet,url,function (respuesta){
			$('#load').hide();
			resEML = JSON.parse(respuesta);
			console.log(resEML);

			cveedo='30';
			if (cveedo=='') {
				$('#municipios').html('');
			}else{
				var Municipios=buildOptions(resEML.DATOS['municipios'][cveedo],'NOMBREMUN','CVEMUN');
				$('#municipios').html(Municipios);

			}
		});
	}






	var vs_cveMun="";
	function traeAnos (){
		$('#load').show();
		var opcionMun=document.getElementById('municipios');	
		vs_cveMun=opcionMun.value;

		valuesPOST={vs_cveMun:vs_cveMun}
		objet={opcion:'generales',action:'infoAnosVisitas',idPrograma:'<?php echo $_GET['index'] ?>',csrf_token:'<?php echo $csrf_token ?>',values:valuesPOST};
		ajaxCallback(objet,url,function (respuesta){
			$('#load').hide();
			resTraeAnos = JSON.parse(respuesta);
			var anos=buildOptions(resTraeAnos.DATOS['anosVisita'][vs_cveMun],'NOMBRE','VALOR');
			$('#ano').html(anos);
		});
	}



	var vs_ano="";
	function traeMeses (){
		$('#load').show();
		vs_cveMun=document.getElementById('municipios').value;
		vs_ano=document.getElementById('ano').value;
		
		valuesPOST={vs_cveMun:vs_cveMun, vs_ano:vs_ano}
		objet={opcion:'generales',action:'infoMesesVisitas',idPrograma:'<?php echo $_GET['index'] ?>',csrf_token:'<?php echo $csrf_token ?>',values:valuesPOST};
		ajaxCallback(objet,url,function (respuesta){
			$('#load').hide();
			resTraeMeses = JSON.parse(respuesta);
			console.log(resTraeMeses);
			var mes=buildOptions(resTraeMeses.DATOS['mesVisita'][vs_ano],'NOMBRE','VALOR');
			$('#mes').html(mes);
		});
	}




	function traeVisitas (){
		valuesPOST=="";
		objet={opcion:'generales',action:'infoEntregasVisitas',idPrograma:'<?php echo $_GET['index'] ?>',csrf_token:'<?php echo $csrf_token ?>',values:valuesPOST};
		ajaxCallback(objet,url,function (respuesta){
			resTraeEntrega = JSON.parse(respuesta);
			var entrega=buildOptions(resTraeEntrega.DATOS['entrega'],'NOMBRE','VALOR');
			$('#entrega').html(entrega);
			cambiaSelect();

		});
		
	}



	function cambiaSelect(){
		document.getElementById("entrega").selectedIndex = "1";
	}



	function comodin(item){
		console.log("");
	}


	function traePdf(){
		var opcionMun=document.getElementById('municipios');	
		s_cveMun=opcionMun.value;
		s_nomMun=opcionMun.options[opcionMun.selectedIndex].innerText;
		var nomMunCambio = s_nomMun.replace(/\ /g,"_");
		var opcionAno=document.getElementById('ano');	
		var opcionMes=document.getElementById('mes');	
		var opcionEntrega=document.getElementById('entrega');	


		s_ano=opcionAno.value;	
		s_mes=opcionMes.value;
		s_engtrega=opcionEntrega.value;

		urlToFile=`../../../DIF/${s_ano}_${s_mes}_${s_engtrega}_${s_cveMun}.pdf`;

		var result =doesFileExist(urlToFile);
		var iframe1 = "";
		if (result == true) {

			iframe1 = '<iframe class="z-depth-1 embed-responsive-item" src='+urlToFile+' ></iframe>'; 

		} else {
			iframe1 = `<div class="alert alert-warning" role="alert"> <strong>Entrega no valida! ! ! ! </strong> Debe seleccionar todos los combos.  </div>`;
		}
		document.getElementById('iframe_pdf').innerHTML=iframe1; 
	}






	function doesFileExist(urlToFile) {
		var xhr = new XMLHttpRequest();
		xhr.open('HEAD', urlToFile, false);
		xhr.send();
		if (xhr.status == "404") {
			return false;
		} else {
			return true;
		}
	}













</script>

