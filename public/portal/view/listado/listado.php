<style type="text/css">
    .boxGraph{
        height: 400px;
        margin-top: 20px;
        padding-bottom: 100px;
        font-family: 'NeoSansPro-Bold';
    }
    .boxTable{
        -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
        box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
    }
</style>

<div class="container-fluid">
    <div class="row" id="contentBody">

    <?php //include_once './view/modules/Credenciales/completes/filtros_Meta.php'; ?>

    <div class="col-md-12 pt-2" id="content-Delegacion" style=""><!--TABLAS Y AVENCES-->
        <div class="row boderSALL">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <button class="btn btn-outline-secondary" id="btnBack" data-toggle="tooltip" data-placement="right" title="">
                            <i class="fa fa-refresh" style="color:#276092;" id="iconMore"></i>
                        </button>
                    </div>
                    <div class="col-md-10">
                        <h3 id="titleGrafica" class="text-center">OFICINAS REGIONALES.</h3>    
                    </div>
                </div>
                <div id="graficaAvance" class="col-md-12 boxGraph">
                    
                </div>
            </div>
            <div class="col-md-12 text-center">
                <hr class="hrSeparator">
                <div class="" id="wait1">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>    
                </div>
            </div>
            <div class="col-md-12" style="font-size: 12px; margin-top: 0px">
                <div class="col-md-12" id="tableAvances" style="margin-top: 0px;">
                    
                </div>
            </div>
        </div>
    </div>

    <?php include_once './view/modules/Credenciales/completes/complete_avances2.php'; ?>

        
    </div>
</div>
<script type="text/javascript">
    var _t = '<?php echo $csrf_token ?>';

    $('#print').click(function () {
        printMe();
        //printDiv('content-Delegacion');
    });

    $('#btnBack').click(function () {//reload a la pagina
        Index();
    });

    $('#btnBackDelegacion').click(function () {//return nivel Delegación
        $('#content-Subdelegacion').fadeOut('slow');
        $('#content-Delegacion').fadeIn('slow');
    });

    $('#btnBackSubdelegacion').click(function () {//return nivel Delegación
        $('#content-Municipio').fadeOut('slow');
        $('#content-Subdelegacion').fadeIn('slow');
    });

    $('#btnBackMunicipio').click(function () {//return nivel Delegación
        $('#content-Localidad').fadeOut('slow');
        $('#content-Municipio').fadeIn('slow');
    });


    var next='subdelegacion',   back='delegacion',idTable='table-dinamic',element='graficaAvance',evento=null,content='#tableAvances', 
    elementTitle='',    content_Main='#content-Delegacion',     content_Prev='#content-Delegacion';
    var action='',    idDel='',     idSub='',     idMun='',     idLoc='',    inicio='',    termino='';
    var title=null;

    $(function () {
       $('#btnBack').tooltip({title: 'RECARGAR'});
       Index();//LLAMA A LA FUNCIÓN PRINCIPAL
    });
        
    /*FUNCIÓN PARA TRAER DATOS DE LAS GRAFICAS Y TABLAS (GENERALES)*/
    function Index() {
        load();
        $('#content-Delegacion').fadeOut(1000);
        var url = "./controller/CredencialesController.php";
        objet={opcion:'Credenciales',action:'getCredencialesDelegacion',csrf_token: _t};
        var maxBen=null;
        ajaxCallback(objet,url,function (respuesta){
            try {
                res = JSON.parse(respuesta);
                console.log('delegacion-> ' ,res);
                if (res.CODIGO==true) {
                    $('#content-Delegacion').fadeIn(2500);
                    maxBen=res.MAXBEN;
                    tableIndex();
                    var ykeys=[];
                    for (var i = 0; i <= maxBen; i++) {
                        ykeys.push("B_"+i);
                    }
                    graphMain(res.DATOS,'DELEGACIÓN',ykeys,true);
                }
                else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                    $('#titlePage-2').html(': ALERTA!');
                    load(false);
                }
            }
            catch(err) {
                $('#titlePage-2').html(': ERROR !');
                showAlertInwindow('#contentBody','warning',err.message);
                console.log(err);
                load(false);
            }            
        });
        
    }

    function tableIndex(){
        load();
        var url = "./controller/ListadoController.php";
        objet={opcion:'Listado',action:'getGeneral',csrf_token: _t};
        ajaxCallback(objet,url,function (respuesta){
            try {
                res = JSON.parse(respuesta);
                if (res.CODIGO==true) {
                    $('#wait1').css("display","none");
                    renderTableListado(content,res.DATOS,'',5,2,idTable,next,{org:'credenciales',click:'nextLevel'});
                    tableExport2('#'+idTable,'Listado_General','',res.COUNT);
                    settingsTable();
                    load(false);
                }
                else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                    $('#titlePage-2').html(": "+capitalLetter(res.CODIGO)+"!");
                    load(false);
                }
            }
            catch(err) {
                $('#titlePage-2').html(': ERROR !');
                showAlertInwindow('#contentBody','warning',err.message);
                console.log(err);
                load(false);
            }            
        });
    }

    function renderTableListado(element,data,idP,jump='',exeption='',idtable='',onclick='',org='',modalidad='') {
          $(element).html('');
          var key=null;
          var keys=null;
          var click='';
          
          if (jump=='' && jump!=false) {jump=4}
          if (modalidad==1) {
            keys = Object.keys(data[0]);
            key=keys;
          }else{
            if (org=='') {
              //keys = Object.keys(data[idP]); 
              keys = Object.keys(data[0]); 
            }else{
              keys = Object.keys(data[0]); 
            }
            key=keys; 
          }

          if (org.org!='') {
              click=org.click;
          }
          
          if (exeption!=0||exeption!='') {
            key=[];
            for (var i = exeption; i < keys.length; i++) {
              key.push(keys[i]);
            }
          }

          if (idtable=='') {
            idtable='table-dinamic';
          }
          var index=null,title=null;
          if (onclick=='subdelegacion') {
            index='ID_DEL';
            title='DELEGACIÓN';
          }
          if (onclick=='municipio') {
            index='ID_SUB';
            title='SUBDELEGACIÓN';
          }
          if (onclick=='localidad') {
            index='CVE_MUN';
            title='MUNICIPIO';
          }

 
  
          var totales = [];
          var cadena = "<table id='"+idtable+"' class='table table-bordered table-hover' >\
                                            <thead class='thead-light'>\
                                                <tr>";
                                                $.each(key,function (i,item) {
                                                    if (item=='DELEGACIÓN') {
                                                        item='OFICINA REGIONAL';
                                                    }
                                                    if (item=='SUBDELEGACIÓN') {
                                                        item='OFICINA SUBREGIONAL';
                                                    }
                                                  cadena += "<th>"+item+"</th>";
                                                    if(i>=jump){
                                                        totales[i] = 0;
                                                    }
                                                });
                                      cadena += "</tr>\
                                            </thead>\
                                            <tbody>";
          var numNotif = 0;
          var numExp = 0;
          var tot_visitas = 0;
          var value=null;
          var tr="<tr>";
                      for(var i = 0;i < data.length;i++){
                        if (onclick!='') {
                          tr="<tr>"
                        }
                        cadena += tr;
                                for (var j = 0; j < key.length; j++) {
                                  value=data[i][key[j]];
                                    if (j>1 && j<5) {
                                        cadena += "<td> <a href='?view=cedula&indexs="+data[i].IDLISTADO+"' target='_blank'>"+separa(value)+"</a></td>";
                                    }else{
                                        cadena += "<td onclick='"+complementosRenderTable(onclick,data[i][index],data[i][title],idP,click)+"'>"+separa(value)+"</td>";
                                    }
                                    if(j>=jump){
                                        totales[j] = Number(totales[j]) + Number(data[i][key[j]]);
                                    }
                                }    
                        cadena += "</tr>";
                      }
                      
                    cadena += "</tbody>";

                    if (jump!=false) {
                          cadena+="<tfoot class='thead-light'>\
                                    <tr>\
                                      <th colspan='"+jump+"'>Totales</th>";
                                    for (var j = jump; j < (totales.length); j++) {
                                cadena += "<th>"+separa(totales[j])+"</th>";
                                    } 
                      }
                                            
                          cadena += "</tr>\
                                    </tfoot>\
                                  </table>";
        $(element).html(cadena);                     
    }

    function nextLevel(next,id,title) {
        load();
        var capital='';
        var nextOrg=next;
        //alert(next);
        //alert('content_Prev-> '+content_Prev);
        //alert('content_Main-> '+content_Main);
        if (nextOrg=='subdelegacion') {
            $('#content-Delegacion').fadeOut(1000);
            action='getCredencialesSubdelegacion';
            capital='OFICINA REGIONAL: ';
            content_Main='#content-Subdelegacion';
            elementTitle='#titleGraficaSubdelegacion';
            next='municipio';
            content='#tableAvancesSubDelegacion';
            idTable='table-dinamicSubdel';
            $('#btnBackDelegacion').tooltip({title: '<- REGRESAR A OFICINAS REGIONALES'});
        }
        if (nextOrg=='municipio') {
            $('#content-Subdelegacion').fadeOut(1000);
            action='getCredencialesMunicipio';
            capital='OFICINA SUBREGIONAL: ';
            content_Main='#content-Municipio';
            elementTitle='#titleGraficaMunicipio';
            next='localidad';
            content='#tableAvancesMunicipio';
            idTable='table-dinamicMunicipio';
            $('#btnBackSubdelegacion').tooltip({title: '<- REGRESAR A OFICINAS SUBREGIONALES'});
        }
        if (nextOrg=='localidad') {
            $('#content-Municipio').fadeOut(1000);
            action='getCredencialesLocalidad';
            capital='MUNICIPIO: ';
            content_Main='#content-Localidad';
            elementTitle='#titleGraficaLocalidad';
            next='';
            content='#tableAvancesLocalidad';
            idTable='table-dinamicLocalidad';
            $('#btnBackMunicipio').tooltip({title: '<- REGRESAR A MUNICIPIOS'});
        }
        content_Prev = content_Main;
        /*alert('content_Prev-> '+content_Prev);
        alert('content_Main-> '+content_Main);*/
        $(elementTitle).html(capital+title);
        var url = "./controller/CredencialesController.php";
        objet={opcion:'Credenciales',action:action,csrf_token: _t,idDel:id,idSub:id,idMun:id,idLoc:id};
        var maxBen=null;
        ajaxCallback(objet,url,function (respuesta){
            try {
                res = JSON.parse(respuesta);
                if (res.CODIGO==true) {
                    $(content_Main).fadeIn(2500);
                    maxBen=res.MAXBEN;
                    console.log(nextOrg+'-> ' ,res);
                    /*renderTable(content,res.DATOS,'',1,1,idTable,next,{org:'credenciales',click:'nextLevel'});
                    tableExport2('#'+idTable,'Credencialecion_Delegacion_'+title,'','');
                    settingsTable();*/
                    var ykeys=[];
                    for (var i = 0; i <= maxBen; i++) {
                        ykeys.push("B_"+i);
                    }
                    if (nextOrg=='subdelegacion') {
                        graphSubD(res.DATOS,'SUBDELEGACIÓN',ykeys,true,'',next);
                        tableSubD(id);
                        $('#wait2').css("display","none");
                    }
                    if (nextOrg=='municipio') {
                        graphMunicipio(res.DATOS,'MUNICIPIO',ykeys,true,'',next);
                        $('#wait3').css("display","none");
                        tableMunicipio(id);
                    }
                    if (nextOrg=='localidad') {
                        graphLocalidad(res.DATOS,'LOCALIDAD',ykeys,true,'',next);
                        tableLocalidad(id);
                        $('#wait4').css("display","none");
                    }
                    load(false);
                }
                else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                    $('#titlePage-2').html(': ALERTA!');
                    load(false);
                }
            }
            catch(err) {
                $('#titlePage-2').html(': ERROR !');
                showAlertInwindow('#contentBody','warning',err.message);
                console.log(err);
                load(false);
            }  
        }); 
    }


    function graphMain(data,xkey,ykey,stack,labels='') {
        $('#graficaAvance').html('');
        if (labels=='') {
            labels=ykey;
        }

        new Morris.Bar({
            element: 'graficaAvance',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>OFICINA REGIONAL: "+aux[1];
                content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        }).on('click', function(i, row){
            nextLevel(next,row.ID_DEL,row.DELEGACIÓN);
        });

    }
    function graphSubD(data,xkey,ykey,stack,labels='',next=''){
        $('#graficaAvanceSubDelegacion').html('');
        if (labels=='') {
            labels=ykey;
        }

        new Morris.Bar({
            element: 'graficaAvanceSubDelegacion',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>OFICINA SUBREGIONAL: "+aux[1];
                content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesSubdelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        }).on('click', function(i, row){
            nextLevel(next,row.ID_SUB,row.SUBDELEGACIÓN);
        });
    }
    function tableSubD(id){
        load();
        var url = "./controller/ListadoController.php";
        objet={opcion:'Listado',action:'getSubdelegacion',csrf_token: _t,idDel:id,idSub:id,idMun:id,idLoc:id};
        ajaxCallback(objet,url,function (respuesta){
            try {
                res = JSON.parse(respuesta);
                if (res.CODIGO==true) {
                    $('#wait1').css("display","none");
                    renderTableListado(content,res.DATOS,'',5,2,idTable,next,{org:'credenciales',click:'nextLevel'});
                    tableExport2('#'+idTable,'Listado_General_SUBDELEGACIÓN','',res.COUNT);
                    settingsTable();
                    load(false);
                }
                else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                    $('#titlePage-2').html(": "+capitalLetter(res.CODIGO)+"!");
                    load(false);
                }
            }
            catch(err) {
                $('#titlePage-2').html(': ERROR !');
                showAlertInwindow('#contentBody','warning',err.message);
                console.log(err);
                load(false);
            }            
        });
    }

    function graphMunicipio(data,xkey,ykey,stack,labels='',next=''){
        $('#graficaAvanceMunicipio').html('');
        if (labels=='') {
            labels=ykey;
        }

        new Morris.Bar({
            element: 'graficaAvanceMunicipio',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>MUNICIPIO: "+aux[1];
                  content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesMunicipio&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        }).on('click', function(i, row){
            nextLevel(next,row.CVE_MUN,row.MUNICIPIO);
        });
    }
    function tableMunicipio(id){
        load();
        var url = "./controller/ListadoController.php";
        objet={opcion:'Listado',action:'getMunicipio',csrf_token: _t,idDel:id,idSub:id,idMun:id,idLoc:id};
        ajaxCallback(objet,url,function (respuesta){
            try {
                res = JSON.parse(respuesta);
                if (res.CODIGO==true) {
                    $('#wait1').css("display","none");
                    renderTableListado(content,res.DATOS,'',5,2,idTable,next,{org:'credenciales',click:'nextLevel'});
                    tableExport2('#'+idTable,'Listado_General_MUNICIPIO','',res.COUNT);
                    settingsTable();
                    load(false);
                }
                else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                    $('#titlePage-2').html(": "+capitalLetter(res.CODIGO)+"!");
                    load(false);
                }
            }
            catch(err) {
                $('#titlePage-2').html(': ERROR !');
                showAlertInwindow('#contentBody','warning',err.message);
                console.log(err);
                load(false);
            }            
        });
    }

    function graphLocalidad(data,xkey,ykey,stack,labels='',next=''){
        $('#graficaAvanceLocalidad').html('');
        if (labels=='') {
            labels=ykey;
        }
        new Morris.Bar({
            element: 'graficaAvanceLocalidad',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>LOCALIDAD: "+aux[1];
                  content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesLocalidad&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
         });
    }
    function tableLocalidad(id){
        load();
        var url = "./controller/ListadoController.php";
        objet={opcion:'Listado',action:'getLocalidad',csrf_token: _t,idDel:id,idSub:id,idMun:id,idLoc:id};
        ajaxCallback(objet,url,function (respuesta){
            try {
                res = JSON.parse(respuesta);
                if (res.CODIGO==true) {
                    $('#wait1').css("display","none");
                    renderTableListado(content,res.DATOS,'',5,2,idTable,next,{org:'credenciales',click:'nextLevel'});
                    tableExport2('#'+idTable,'Listado_General_LOCALIDAD','',res.COUNT);
                    settingsTable();
                    load(false);
                }
                else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                    $('#titlePage-2').html(": "+capitalLetter(res.CODIGO)+"!");
                    load(false);
                }
            }
            catch(err) {
                $('#titlePage-2').html(': ERROR !');
                showAlertInwindow('#contentBody','warning',err.message);
                console.log(err);
                load(false);
            }            
        });
    }
</script>













