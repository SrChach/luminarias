

<div class="container-fluid" id='viewCedula' ondragstart="return false">
	<?php 
		include_once './view/cedula/section/cedula_ben.php';
		include_once './view/cedula/section/exp_ben.php';
		include_once './view/cedula/section/programas_ben.php';
		include_once './view/cedula/section/modal.php';
	?>
	<!--div class="col-md-12 embed-responsive embed-responsive-16by9">
		<iframe class="embed-responsive-item" src="http://138.197.169.196/infraVerDes/cedula_ver/index.php?folio=<?php echo $_GET['index'] ?>"></iframe>
	</div-->
	<div class="row">
		<div class="col-md-3">
			<img id="zoom_01" src='http://173.192.101.210:9043/desarrollo_des/pillar/clases/imagen.php?idImagen=12193941' data-zoom-image="http://173.192.101.210:9043/desarrollo_des/pillar/clases/imagen.php?idImagen=12193941" class="img-fluid d-block" />					
		</div>
	</div>
	
</div>

<!-- <script src='./vendor/thisPlugins/zoom/jquery-1.8.3.min.js'></script> -->
<!-- <script src='./vendor/thisPlugins/zoom/jquery.elevatezoom.js'></script> -->
<!-- <script c="https://cdnjs.cloudflare.com/ajax/libs/elevatezoom/3.0.8/jquery.elevatezoom.js"></script> -->


<script src='./vendor/plugins/elevatedzoom/jquery-1.8.3.min.js'></script>
<script src='./vendor/plugins/elevatedzoom/jquery.elevatezoom.js'></script>
<script>

	 
	// function inicializarZoom(){

	 
	// }

	// $('document').ready(function(){
	// 	jQuery.noConflict();
	// 	inicializarZoom();
	// });	


</script>

<script type="text/javascript" src='./assets/js/cedula.js'></script>
<script type="text/javascript">
	var j = jQuery.noConflict();


	$('#titlePage-2').html(' INFORMATIVA');
	$(function () {

		$('#zoom_01').elevateZoom({
	    zoomType: "inner",
		cursor: "crosshair",
		zoomWindowFadeIn: 500,
		zoomWindowFadeOut: 750
	   }); 

		load();
		var url="../../pillar/clases/controlador_pruebas.php";
		var key='<?php echo $_GET['indexs'] ?>';
		var idListado=(key.split(","))[0];
		var idP=(key.split(","))[1];
        objet={opcion:'cedula_2',folio:idListado,idPrograma:idP};
        
        ajaxCallback(objet,url,function (respuesta){
            res = JSON.parse(respuesta);
            console.log(res);
            infoBeneficiario(res.generales);
            res.programas.forEach(function(item,i){
            	$('#contentProgramas').append(Render(item,i,res.eventos[item.IDPROGRAMA],res.imagenes_eventos));	
            });
            res.expediente.forEach(function (item,i) {
            	$('#tb_documents').append(RenderListDocuments_exp(item,i));
            	$('#indicators').append(RenderDocuments_exp(item,i,'indicators'));
            	$('#carousel').append(RenderDocuments_exp(item,i,'galery'));
            });
            RenderInfo(res.expediente[0].TIPOIMAGEN,res.expediente[0].FECHAINSERCION,res.expediente[0].IDEVENTO,res.expediente[0].ACTUAL);
            load(false);
        });
	}); 
	
	function RenderListDocuments_exp(item,i) {
		var x=item;
		return 		'<tr>\
						<td>'+item.TIPOIMAGEN+'</td>\
						<td>'+item.TIPODOCUMENTO+'</td>\
						<td>\
							<a href="#!" onclick="RenderInfo(\''+item.TIPOIMAGEN+'\',\''+item.FECHAINSERCION+'\',\''+item.IDEVENTO+'\',\''+item.ACTUAL+'\')" class="badge badge-light" data-target="#carouselExampleIndicators" data-slide-to="'+i+'">\
								<i class="icon ion-eye nav-icon" style="font-size: 20px;"></i>\
							</a>\
						</td>\
					</tr>';
	}
	function RenderDocuments_exp(item,i,option) {
		var render='';
		if (option=='indicators') {
			if (i==0) {
				render= '<li data-target="#carouselExampleIndicators" data-slide-to="'+i+'" class="active btn btn-secondary"></li>';
			}else{
				render = '<li data-target="#carouselExampleIndicators" data-slide-to="'+i+'" class="btn btn-secondary"></li>';
			}
		}
		if (option=='galery') {
			if (i==0) {
				render = '<div class="carousel-item active" align="center">\
      						<img id="ima-exp'+i+'" data-zoom-image="http://173.192.101.210:9043/desarrollo_des/pillar/clases/imagen.php?idImagen='+item.IDIMAGEN+'" class="d-block" src="http://173.192.101.210:9043/desarrollo_des/pillar/clases/imagen.php?idImagen='+item.IDIMAGEN+'" alt="First slide" width="50%" height="260px;" data-toggle="modal" data-target="#modalImgCedula" onclick="modalInfo(this);">\
    					</div>';
			}else{
				render = '<div class="carousel-item" align="center">\
      						<img id="ima-exp'+i+'" data-zoom-image="http://173.192.101.210:9043/desarrollo_des/pillar/clases/imagen.php?idImagen='+item.IDIMAGEN+'" class="d-block" src="http://173.192.101.210:9043/desarrollo_des/pillar/clases/imagen.php?idImagen='+item.IDIMAGEN+'" alt="Second slide" width="50%" height="260px;" data-toggle="modal" data-target="#modalImgCedula" onclick="modalInfo(this);">\
    					</div>';
			}
		}
		return render;
	}
	
	
	

	function RenderInfo(nombre,fecha_insercion,id_evento,actual) {
		
		$('#fecha_exp').html(fecha_insercion);
		$('#nombre_exp').html(nombre);

		if (id_evento==null||id_evento==0||id_evento=='null') {
			id_evento='PORTAL';
		}else{
			id_evento='DM';
		}
		$('#org_exp').html(id_evento);
		if (actual==null||actual==0|| actual=='null') {
			actual='NO VALIDADO';
		}
		if (actual==1) {
			actual='CORRECTO';
		}
		if (actual==2) {
			actual='INCORRECTO';
		}
		$('#validacion_exp').html(actual);
	}

	function modalInfo(elementoOrg) {
		$('#imgDigital').attr('src',elementoOrg.src);
		$('#imgDigital').attr('data-zoom-image',elementoOrg.src);

		/*$('#imgDigital').elevateZoom({
			cursor: "crosshair",
			zoomWindowFadeIn: 50,
			zoomWindowFadeOut: 50,
			easing : true,
			scrollZoom : true,
			zoomWindowWidth:200,
        	zoomWindowHeight:200
		});*/
		
	}
</script>