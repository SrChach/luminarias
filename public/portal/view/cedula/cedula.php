<style class="cp-pen-styles">
    .boxQR {
		position:absolute; 
		top: 7px; 
		right: -1px;
		width: 100px;
		height: 100px;
		font-size: 25px;
	}
</style>
<div class="container-fluid" id='viewCedula' ondragstart="return false">
	<?php 

		include_once './view/cedula/section/cedula_ben.php';
		include_once './view/cedula/section/exp_ben.php';
		include_once './view/cedula/section/programas_ben.php';
		include_once './view/cedula/section/modal.php';

	?>
	<!--div class="col-md-12 embed-responsive embed-responsive-16by9">
		<iframe class="embed-responsive-item" src="http://138.197.169.196/infraVerDes/cedula_ver/index.php?folio=<?php echo $_GET['index'] ?>"></iframe>
	</div-->
</div>

<script type="text/javascript" src='./assets/js/cedula.js'></script>
<script type="text/javascript">
	
	jQuery('#titlePage-2').html(' INFORMATIVA');
	var idP='';
	var query_G;
	var lengthExpedienetes;
	jQuery(function ($) {
		query_G = $;
		load();
		var url="../../pillar/clases/controlador_pruebas.php";
		var key='<?php echo $_GET['indexs'] ?>';
		var idListado=(key.split(","))[0];
		idP=(key.split(","))[1];
        objet={opcion:'cedula_2',folio:idListado,idPrograma:idP};
        
        ajaxCallback(objet,url,function (respuesta){
            res = JSON.parse(respuesta);
            console.log(res);
            console.log(res.generales['FOLIOCUIS']);
            $('#cedula_ben').fadeIn('slow');
            infoBeneficiario(res.generales);
            if (res.programas.length>0) {
            	$('#infoProgramasBen').fadeIn(2000);
            	$('#contentProgramas').fadeIn(4000);
            	res.programas.forEach(function(item,i){
            		$('#contentProgramas').append(Render(item,i,res.eventos[item.IDPROGRAMA],res.imagenes_eventos));
            		$('#PorgramasFromBneficiario').append(renderProgramaForCedula(item,i));
            		res.eventos[item.IDPROGRAMA].forEach(function (item,i) {
            			if (item.IDEVENTO in res.imagenes_eventos) {
            				makeQR('qr_P-'+item.IDPROGRAMA+'_Ent-'+i,thisHost+'/certificado.php?c='+res.imagenes_eventos[item.IDEVENTO][0].CERTIFICADO);
            				console.log(res.imagenes_eventos[item.IDEVENTO][0].CERTIFICADO);
            			}else{
            				makeQR('qr_P-'+item.IDPROGRAMA+'_Ent-'+i,'selloDigital_SIN DATO');
            			}

            		})
            	});
            	
            }
			ZoomImg($);
			if (res.expediente.length>0) {
				$('#exp_ben').fadeIn(3000);
				res.expediente.forEach(function (item,i) {
					$('#tb_documents').append(RenderListDocuments_exp(item,i));
					$('#indicators').append(RenderDocuments_exp(item,i,'indicators'));
					$('#carousel').append(RenderDocuments_exp(item,i,'galery'));
				});
				$('#carousel').append('<div class="boxQR text-center" id="qrCode"><i class="fa fa-spinner fa-spin" style="font-size:20px;" id="load_QR"></i></div>');
				makeQR('qr_principal',thisHost+'/certificado.php?c='+res.expediente[0].CERTIFICADO);
				$('#qr_principal').attr('title','null');
				//console.log(makeQR('qrCode',thisHost))/certificado.php?c=length;
				if(typeof (res.expediente[0]) !== "undefined"){
					RenderInfo(res.expediente[0].TIPOIMAGEN,res.expediente[0].FECHAINSERCION,res.expediente[0].IDEVENTO,res.expediente[0].ACTUAL,0,res.expediente[0].CERTIFICADO);	 
				}
			}
            
            load(false);
            ZoomImg($);
        });


	});

	function renderProgramaForCedula(item,i) {
		return "<div class='col-md-3 col-sm-4 col-xs-2'>\
              			<div class='card hover elevation-3'>\
                			<div class='row'>\
                    			<div class='col-md-4 col-xs-3'>\
                      				<img class='' src='./Storage/Evidencias/imgPrograms/PNG-CUADRADO/"+item.IMAGEN_CUADRADA+"' alt='Card image cap' width='100%' style='padding-top: 25%;'>\
                    			</div>\
                  				<div class='col-md-8 col-xs-9'>\
                    				<div class='card-body backCardsSites elevation-solid-Left-to-Top backCardsProgramsCedula'>\
                      					<p class='upperCase'>"+item.NOMBREPROGRAMA+"</p>\
                      					<br>\
                    				</div>\
                  				</div>\
                			</div>\
              			</div>\
         			</div>";
	}

	function zoom(elementImg,$) {
		/*$('#'+elementImg).elevateZoom({
			/*zoomType: "lens",
			lensShape : "round",
			lensSize    : 200
			cursor: "crosshair",
			zoomWindowFadeIn: 200,
			zoomWindowFadeOut: 200,
			easing : true,
			scrollZoom : true,
			zoomWindowWidth:200,
        	zoomWindowHeight:260
   		});*/
   		$('.img-zoom').elevateZoom({
			/*zoomType: "lens",
			lensShape : "round",
			lensSize    : 200*/
			cursor: "crosshair",
			zoomWindowFadeIn: 1000,
			zoomWindowFadeOut: 1000,
			easing : true,
			scrollZoom : true,
			zoomWindowWidth:200,
        	zoomWindowHeight:260
   		});
	}
	
	function RenderListDocuments_exp(item,i) {
		var x=item;
		return 		'<tr>\
						<th>'+(i+1)+'</th>\
						<td>'+item.TIPOIMAGEN+'</td>\
						<td>'+item.TIPODOCUMENTO+'</td>\
						<td>\
							<a href="#!" onclick="RenderInfo(\''+item.TIPOIMAGEN+'\',\''+item.FECHAINSERCION+'\',\''+item.IDEVENTO+'\',\''+item.ACTUAL+'\','+i+',\''+item.CERTIFICADO+'\')" class="badge badge-light" data-target="#carouselExampleIndicators" data-slide-to="'+i+'">\
								<i class="icon ion-eye nav-icon" style="font-size: 20px;"></i>\
							</a>\
						</td>\
					</tr>';
	}
	function RenderDocuments_exp(item,i,option) {
		var render='';
		var active='';
		if (option=='indicators') {
			if (i==0) {
				active='active';
			}else{
				active='';
			}
			render = '<li data-target="#carouselExampleIndicators" data-slide-to="'+i+'" class="'+active+' btn btn-secondary" onclick="callNewImg('+i+')"></li>';
		}
		if (option=='galery') {
			if (i==0) {
    			active="active";
			}else{
    			active='';
			}
			render = '<div class="carousel-item '+active+'" align="center">\
							<!--div class="row">\
								<div class="col-6"-->\
									<img id="ima-exp'+i+'" class="d-block img-zoom" src="../../pillar/clases/imagen.php?idImagen='+item.IDIMAGEN+'" alt="Second slide" width="70%" height="260px;" data-toggle="modal" data-target="#modalImgCedula" onclick="modalInfo(this,'+i+',\'exp\');">\
								<!--/div>\
								<div class="col-6">\
									<a class="btn btn-primary btn-sm" data-toggle="collapse" href="#collapseExample'+i+'" role="button" aria-expanded="false" aria-controls="collapseExample">\
    									QR\
  									</a>\
  									<div class="collapse" id="collapseExample'+i+'">\
  									<div class="card card-body" id="qrcode'+i+'">\
    									qr de imagen '+(i+1)+'\
  									</div>\
								</div>\
								</div>\
							</div-->\
    					</div>\
<!--a href="./view/cedula/section/modalResponsive.php?idimagen='+item.IDIMAGEN+'" id="prueba">.</a-->\
    					';
		}
		return render;
	}
	
	
	var aux=0;
	$('#prev-ExpBen').click(function () {
		if (res.expediente.length>1) {
			aux=aux-1;
			if (aux<0) {
				aux=lengthExpedienetes-1;
			}
			callNewImg(aux);
		}else{
			callNewImg(0);
		}
		
	});
	$('#next-ExpBen').click(function () {
		if (res.expediente.length>1) {
			aux=aux+1;
			if (aux>=lengthExpedienetes) {
				aux=0;
			}
			callNewImg(aux);
		}else{
			callNewImg(0);
		}
	});

	function callNewImg(position) {
		RenderInfo(res.expediente[position].TIPOIMAGEN,res.expediente[position].FECHAINSERCION,res.expediente[position].IDEVENTO,res.expediente[position].ACTUAL,position,res.expediente[position].CERTIFICADO);
	}
	

	function RenderInfo(nombre,fecha_insercion,id_evento,actual,i,idImagen = 0) {
		$("#qr_principal").html("");
		var infoQr = nombre;
		if(idImagen != 0){
			infoQr = idImagen;
		}
		makeQR('qr_principal',thisHost+'/certificado.php?c='+infoQr);
		aux=i;
		$('#fecha_exp').html('...');
		$('#nombre_exp').html('...');
		$('#org_exp').html('...');
		$('#validacion_exp').html('...');
		$('#number_exp').html('...');


		$('#fecha_exp').html(fecha_insercion);
		$('#nombre_exp').html(nombre);
		if (id_evento==null||id_evento==0||id_evento=='null') {
			id_evento='PORTAL';
		}else{
			id_evento='DM';
		}
		$('#org_exp').html(id_evento);
		if (actual==null||actual==0|| actual=='null') {
			actual='NO VALIDADO';
		}
		if (actual==1) {
			actual='CORRECTO';
		}
		if (actual==2) {
			actual='INCORRECTO';
		}
		$('#validacion_exp').html(actual);
		$('#number_exp').html((i+1)+' / '+lengthExpedienetes);
	}

	var aux2=0;
	function RenderInfoPrograma(x,j,k,nombre,fecha_insercion,id_evento,actual,i,length,idImagen=0,programa='') {
		$("#qr_P-"+programa+"_Ent-"+(x-1)).html("");
		console.log("qr_P-"+programa+"_Ent-"+(x-1));
		var infoQr = nombre;
		if(idImagen != 0){
			infoQr = idImagen;
		}
		makeQR("qr_P-"+programa+"_Ent-"+(x-1),'selloDigital_'+infoQr);

		aux2=i;
		$('#fechaEnt-'+x).html('...');
		$('#OrigenEnt-'+x).html('...');
		$('#ValidacionEnt-'+x).html('...');

		$('#fechaEnt-'+x).html(fecha_insercion);

		if (id_evento==null||id_evento==0||id_evento=='null') {
			id_evento='PORTAL';
		}else{
			id_evento='DM';
		}
		$('#OrigenEnt-'+x).html(id_evento);
		if (actual==null||actual==0|| actual=='null') {
			actual='NO VALIDADO';
		}
		if (actual==1) {
			actual='CORRECTO';
		}
		if (actual==2) {
			actual='INCORRECTO';
		}
		$('#ValidacionEnt-'+x).html(actual);
		$('#no-'+x).html((i+1)+' / '+length);
		$('#nombreDoc-'+x).html(nombre);
	}

	
	function callNewImgPro(org,evento,option,position='') {
		if (res.imagenes_eventos[evento].length>1) {
			if (option=='more') {
				aux2=aux2+1;
				if (aux2>=res.imagenes_eventos[evento].length) {
					aux2=0;
					console.log('ya');
				}	
			}
			if (option=='less') {
				aux2=aux2-1;
				if (aux2<0) {
					aux2=res.imagenes_eventos[evento].length-1;
				}
			}
		}

		RenderInfoPrograma(position,0,0,res.imagenes_eventos[evento][aux2].TIPOIMAGEN,res.imagenes_eventos[evento][aux2].FECHAINSERCION,res.imagenes_eventos[evento][aux2].IDEVENTO,res.imagenes_eventos[evento][aux2].ACTUAL,aux2,res.imagenes_eventos[evento].length,res.imagenes_eventos[evento][aux2].IDIMAGEN,org);

		
	}

	
	function modalInfo(elementoOrg,i,org,evento='') {
		console.log(elementoOrg);
			$('#imgDigital').attr('src','./assets/img/load2.gif');
			$('.completeIntfo').hide();
			var url = './controller/PreguntasController.php';
			var objet = {opcion:'Preguntas',csrf_token:'vkj',action:'getPreguntasFromImg', idImg:(elementoOrg.src).split("=")[1]};
			var get;
			var valido=null;
			ajaxCallback(objet,url,function (response) {
			 	try{
					get = JSON.parse(response);

					if (get.CODIGO==true) {
						$('#body_indexacion').html(BodyIndexacionsPreguntas(get.DATOS));
					}else{
						showAlertInwindow('#body_indexacion','warning',get.DATOS);
					}
						$('#imgDigital').attr('src',elementoOrg.src);
						$('#imgDigital').attr('data-zoom-image',elementoOrg.src);
						$('#M_foliocuis').html(res.generales.FOLIOCUIS);		
						$('#M_ben').html(res.generales.NOMBRE+' '+res.generales.APATERNO+' '+res.generales.AMATERNO);

					if (org=='exp') {
						$('#M_ti').html(res.expediente[i].TIPOIMAGEN);		
						$('#M_td').html(res.expediente[i].TIPODOCUMENTO);
						$('#M_fi').html(res.generales.FECHAINSERCION);				
						/*res.programas.forEach(function (item,i) {
							if (i==res.programas.length-1) {
								$('#M_p').append(item.NOMBREPROGRAMA+".");		
							}else{
								$('#M_p').append(item.NOMBREPROGRAMA+", ");	
							}
							
						});
						//$('#M_e').html(res.eventos[][0].TIPOEVENTO);*/

						if (res.expediente[i].ACTUAL==null||res.expediente[i].ACTUAL==''||res.expediente[i].ACTUAL==0) {
							valido='NO VALIDO';
						}
						if (res.expediente[i].ACTUAL==1) {
							valido='CORRRECTO';
						}
						if (res.expediente[i].ACTUAL==2) {
							valido='INCORRECTO';
						}
						$('#M_v').html(valido);
					}else{
						$('#M_ti').html(res.imagenes_eventos[evento][0].TIPOIMAGEN);
						$('#M_td').html(res.imagenes_eventos[evento][0].TIPODOCUMENTO);
						$('#M_fi').html(res.imagenes_eventos[evento][0].FECHAINSERCION);
						if (res.imagenes_eventos[evento][0].ACTUAL==null||res.imagenes_eventos[evento][0].ACTUAL==''||res.imagenes_eventos[evento][0].ACTUAL==0) {
							valido='NO VALIDO';
						}
						if (res.imagenes_eventos[evento][0].ACTUAL==1) {
							valido='CORRRECTO';
						}
						if (res.imagenes_eventos[evento][0].ACTUAL==2) {
							valido='INCORRECTO';
						}
						$('#M_v').html(valido);

						var programa_name;
						res.programas.forEach(function (programa,i) {
							if (programa.IDPROGRAMA==res.imagenes_eventos[evento][0].IDPROGRAMA) {
								programa_name=programa.NOMBREPROGRAMA
							}
						});	
						$('#M_p').html(programa_name);	

						var evento_name;
						res.eventos[res.imagenes_eventos[evento][0].IDPROGRAMA].forEach(function (item,i) {
							if (evento==item.IDEVENTO) {
								evento_name=item.TIPOEVENTO;
							}
						});
						$('#M_e').html(evento_name);

						$('.completeIntfo').show();
						
					}
					

			 	}catch (err){
			 		$('#texampleModalLongTitle').html('ERROR !');
                	showAlertInwindow('#modal-content','warning',err.message);
               	 	console.log(err);
			 	}
			 });
		
		

	}

	function BodyIndexacionsPreguntas(datas) {
		var render='';
		datas.forEach(function (item,i) {
			render+='<tr>\
						<td><b>'+item.PREGUNTA+'</b></td>\
						<td>'+item.RESPUESTA+'</td>\
					<tr>';
		});
		return render;
	}

	$('html').on('click', function(e) {
        if (typeof $(e.target).data('original-title') == 'undefined') {
            $('.click').tooltip('hide');
        }
    });
</script>