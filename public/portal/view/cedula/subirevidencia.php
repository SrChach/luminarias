<style type="text/css">
.centra{text-align: center;}.izquierda{text-align: left;}.centro{align:center;}

.sombra_y_borde{
  border:solid; border-radius: 15px 15px 15px 15px; 
  box-shadow: 6px 6px 3px 3px #505160;
  margin-top: 1em;
  margin-bottom: 1em;
  border-color:  #424949;
}
</style>

<div class="container-fluid">

  <form action=""  enctype="multipart/form-data" method="post" >
    <div class="row">


      <!--  IZQUIERDA. -->
      <div class="col-md-4 sombra_y_borde" style="margin-left: 25px">

        <div class="col-md-10 offset-md-1">
         <!--   <span>DATOS GENERALES</span> -->
         <input type="hidden" name="accion" id="accion" value="registrar" >
         <div class="input-group mb-3" style="margin-top: 2em;">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="img1"  name="img1"   accept="image/*"  value="Subir Imagen" onchange="previewImage()">
            <label class="custom-file-label" for="inputGroupFile01">Selecionar Imagen...</label>
          </div>
        </div>
      </div>

      <div class="col-md-10 offset-md-1"> 
        <div id="pintaPreguntas">
        </div>
      </div>

      <div class="col-md-10 offset-md-1">
        <div id="integrantesContainer">
        </div>
      </div>



      <div class="col-md-3 offset-md-1">
        <input type="button" class="btn btn-outline-primary btn-sm" style="margin-bottom: 15px" value="REGISTRAR IMAGEN" onclick="guardarInfo();">
      </div>
    </div>
    <!--  DERECHA  -->
    <div class="col-md-6 offset-md-1 sombra_y_borde">
      <div id="vista-previa" class="img-thumbnail" style="width: 350px">
      </div>
    </div>
  </div>
</form>

</div>




<script type="text/javascript" src="./assets/js/general/ajax.js"></script>
<script type="text/javascript" src="./assets/js/general/funcionesgenerales.js"></script>
<script type="text/javascript" src="./assets/js/subirEvidencia.js"></script>
<script type="text/javascript">





  $(function () {
    printElements();
    traeTipoImagen();
    pintaFolioCuis();
  });






</script>