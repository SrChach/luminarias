
<style>
.headerSectionTitle{
	text-align: center;
	background-color: #A1C4D8;        
	margin-top: 50px;
}



</style>

<div class="container-fluid">
	<div class="row">
		<div class="col" >
			<h4 class="text-center" >Imágen Digitalización</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12  col-md-12 col-lg-6" id="imagenContainer" style="margin-top: 50px;">
			<img src="./assets/img/load2.gif" class="img-fluid img-thumbnail" id="imgDigital"  alt="NO DISPONIBLE" >
		</div>

		<div class="col-sm-12 col-md-12 col-lg-6" style="width: 100%; " >
			<div class="row">
				<div class="col-md-12 col-lg-12" id="dataContainer"  >
					<h5 class="text-center headerSectionTitle">Generales Tipificación</h5>
					<table class="table table-bordered" style="font-size: 12px;">
						<tr>
							<td>
								<b>FOLIOCUIS:</b>
							</td>
							<td id="M_foliocuis"></td>
						</tr>
						<tr>
							<td><b>BENEFICIARIO:</b></td>
							<td id="M_ben"></td>
						</tr>
						<tr>
							<td><b>TIPO IMAGEN:</b></td>
							<td id="M_ti"></td>
						</tr>
						<tr>
							<td><b>TIPO DOCUMENTO:</b></td>
							<td id="M_td"></td>
						</tr>
						<tr style="display: none;">
							<td><b>FECHA DE CREACIÓN:</b></td>
							<td id="M_fi"></td>
						</tr>
						<tr class="completeIntfo" style="display: none;">
							<td><b>PROGRAMA:</b></td>
							<td id="M_p"></td>
						</tr>
						<tr class="completeIntfo" style="display: none;">
							<td><b>EVENTO:</b></td>
							<td id="M_e"></td>
						</tr>
						<tr style="display: none;">
							<td><b>VALIDACIÓN:</b></td>
							<td id="M_v"></td>
						</tr>
					</table>
				</div>
				<div class="col-md-12 col-lg-12">
					<h5 class="text-center headerSectionTitle">Datos indexación</h5>
					<table class="table table-bordered" style="font-size: 12px;">
						<tbody id="body_indexacion"></tbody>
					</table>
				</div>
			</div>
		</div>




	</div>
</div>

<script type="text/javascript" src='./assets/js/cedulaImagen.js'></script>
<script type="text/javascript" src='./assets/js/general/funcionesgenerales.js'></script>

<script>
	$(function(){
		//var idImage=getParameterByName('idImagen');
	var certificado=getParameterByName('certificado');

	buscaIdImagen(certificado);

	
});
</script>