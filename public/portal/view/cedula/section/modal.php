<!-- Modal -->
<div class="modal fade" id="modalImgCedula" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content" style="border: 3px solid #2bb273;">
      <div class="modal-header" style="background: #2bb273; color: white;">
        <h5 class="modal-title" id="exampleModalLongTitle">INFORMACIÓN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: white;">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid" id="modal-content">
          <div class="row" style="border: 3px solid #28A6DE;">
            <div class="col-md-12 text-center">
              <h5 class="text-center">Imágen Digitalización</h5>
              <img src="./assets/img/load2.gif" class="img-fluid img-thumbnail" id="imgDigital" data-zoom-image="" alt="NO DISPONIBLE">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6" style="border: 3px solid #28A6DE;">
              <h5 class="text-center">Generales Tipificación</h5>
              <table class="table table-bordered" style="font-size: 12px;">
                <tr>
                  <td><b>FOLIOCUIS:</b></td>
                  <td id="M_foliocuis"></td>
                </tr>
                <tr>
                  <td><b>BENEFICIARIO:</b></td>
                  <td id="M_ben"></td>
                </tr>
                 <tr>
                  <td><b>TIPO IMAGEN:</b></td>
                  <td id="M_ti"></td>
                </tr>
                 <tr>
                  <td><b>TIPO DOCUMENTO:</b></td>
                  <td id="M_td"></td>
                </tr>
                 <tr style="display: none;">
                  <td><b>FECHA DE CREACIÓN:</b></td>
                  <td id="M_fi"></td>
                </tr>
                 <tr class="completeIntfo" style="display: none;">
                  <td><b>PROGRAMA:</b></td>
                  <td id="M_p"></td>
                </tr>
                 <tr class="completeIntfo" style="display: none;">
                  <td><b>EVENTO:</b></td>
                  <td id="M_e"></td>
                </tr>
                 <tr style="display: none;">
                  <td><b>VALIDACIÓN:</b></td>
                  <td id="M_v"></td>
                </tr>
              </table>
            </div>
            <div class="col-md-6" style="border: 3px solid #28A6DE;">
              <h5 class="text-center">Datos indexación</h5>
              <table class="table table-bordered" style="font-size: 12px;">
                <tbody id="body_indexacion"></tbody>
              </table>
            </div>
          
          </div>
        </div>
      </div>
      <!--div class="modal-footer" style="background: #2bb273; color: white;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>