<div class="container-fluid" id="exp_ben" style="display: none;">
	<div class="row pt-3"><!--EXP. BENEFICIARIO-->
		<div class="col-md-12 text-center">
			<h3>Expediente de Beneficiario.</h3>
		</div>
		<div class="col-md-3 containerJJExpe table-responsive ml-4">
			<table class="table">
				<thead style="font-size: 10px;">
					<tr>
						<th>#</th>
						<th>Documento</th>
						<th>Documento Específico</th>
						<th></th>
					</tr>
				</thead>
				<tbody style="font-size: 12px;" id="tb_documents">
					
				</tbody>
			</table>
		</div>
		<div class="col-md-5 containerJJExpe ml-4" style="background-color:rgb(227,225,224);">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
  				<ol class="carousel-indicators" id="indicators">
    				
  				</ol>
  				
  				<div class="carousel-inner pt-3" id='carousel'>
    				
  				</div>
  				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" style="width: 0%;">
    				<span class="btn btn-secondary" aria-hidden="true" id='prev-ExpBen'> <i class="carousel-control-prev-icon"></i></span>
  				</a>
  				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style="width: 0%;">
    				<span class="btn btn-secondary" aria-hidden="true" id='next-ExpBen'> <i class="carousel-control-next-icon"></i></span>
  				</a>
			</div>
		</div>
		<div class="col-md-3 containerJJExpe ml-4">
			<div class="row ">	
				<div class="col-12 text-center" > 
					<h5 class="text-center">Sello Dígital</h5>
				</div>
			</div>
			<div class="row" align="center">	
				<div class="col-md-4 offset-4 pt-1 text-center" id="qr_principal" align="center">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 pt-1">
					<p class="text-center"><label>Documento</label></p>
					<b><p>No.: <small id="number_exp"></small></p></b>
					<p>Nombre: <small id="nombre_exp"></small></p>
					<p style="display: none;">Fecha: <small id="fecha_exp"></small></p>
					<p>Origen: <small id="org_exp"></small></p>
					<p style="display: none;">Validación: <small id="validacion_exp"></small></p>
				</div>
				<div class="col-md-12 text-center" style="display: none;">
					<input type="button" value="Más Fotos" class="btn btn-outline-primary">
				</div>
			</div>
		</div>
	</div>
	<hr>
</div>