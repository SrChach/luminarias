<div class="container-fluid" id="cedula_ben" style="display: none;">
	<div class="row"><!--HEAD CEDULA-->
		<div class="col-md-6 col-sm-6 col-xs-6">
			<div class="row">
				<div class="col-md-12 text-center">
					<h4>INFORMACIÓN GENERAL</h4>
				</div>
			</div>
			<div class="col-md-12 containerJJCedula">
			<div class="row col-md-12">
				<div class="col-md-7 ml-2 pt-3" id='infoBeneficiario'>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4" style="font-size: 14px;"><label>Beneficiario:</label></div>
						<div class="col-md-8 col-sm-7 col-xs-7" style="font-size: 12px;"><label id="nameBen"></label></div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4" style="font-size: 14px;"><label>FolioCuis:</label></div>
						<div class="col-md-8 col-sm-7 col-xs-7" style="font-size: 12px;"><label id="foliocuis"></label></div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4" style="font-size: 14px;"><label>Estado:</label></div>
						<div class="col-md-8 col-sm-7 col-xs-7" style="font-size: 12px;"><span id="estadoBen"></span></div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4" style="font-size: 14px;"><label>Municipio:</label></div>
						<div class="col-md-8 col-sm-7 col-xs-7" style="font-size: 12px;"><span id="munBen"></span></div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4" style="font-size: 14px;"><label>Localidad:</label></div>
						<div class="col-md-8 col-sm-7 col-xs-7" style="font-size: 12px;"><span id="locBen"></span></div>
					</div>
					<div class="row" style="display: none;">
						<div class="col-md-4 col-sm-4 col-xs-4" style="font-size: 14px;"><label>Dirección:</label></div>
						<div class="col-md-8 col-sm-7 col-xs-7" style="font-size: 12px;"><span id="dirBen"></span></div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4" style="font-size: 14px;"><label>Oficina Regional:</label></div>
						<div class="col-md-8 col-sm-7 col-xs-7" style="font-size: 12px;"><span id="delBen"></span></div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4" style="font-size: 14px;"><label>Oficina Subregional></div>
						<div class="col-md-8 col-sm-7 col-xs-7" style="font-size: 12px;"><span id="subdelBen"></span></div>
					</div>
					<br>
					<br>
				</div>
				<div class="col-md-4 pt-2 ml-3 text-center">
					<img src="" class="img-fluid img-rounded" id="imgBen" width="140px" height="150px">
					<!--input type="button" value="Más Fotos" class="btn btn-outline-secondary"-->
				</div>
			</div>
			</div>
		</div>
		<div class="offset-md-1 col-md-5 col-sm-5 col-xs-5" id="geoBeneficiario">
			<div class="row">
				<div class="col-md-12 text-center">
					<h4>GEOREFERENCIA</h4>
				</div>
			</div>
			<div class="col-md-12 containerJJCedula table-responsive">
				<div class="row">
					<div class="offset-md-3 col-md-6 pt-4 text-center">
						<a href="" id="hrefMap1" target="_blank">
							<i class="fa fa-spinner fa-spin" style="font-size:40px"></i>
						</a>
						<br>
					</div>
				</div>
				<!--div class="row">
					<div class="col-md-6 pt-4">
						<a href="?view=map&params={%22location%22:%2219.415864,-99.1662342%22}" id="hrefMap1" target="_blank">
							<img src="./view/cedula/staticmaplite/staticmap.php?center=19.415864,-99.1662342&zoom=18&maptype=mapnik&markers=19.415864,-99.1662342,ol-marker||19.415864,-99.1662342,bullseye" class="img-fluid img-rounded"  style="height: 150px;width: 100%;">
						</a>
					</div>
					<br>
					<div class="col-md-6 pt-4 pb-4">
						<a href="?view=map&params={%22location%22:%2219.415864,-99.1662342%22}" id="hrefMap1" target="_blank">
							<img src="./view/cedula/staticmaplite/staticmap.php?center=19.415864,-99.1662342&zoom=18&maptype=mapnik&markers=19.415864,-99.1662342,ol-marker||19.415864,-99.1662342,bullseye" class="img-fluid img-rounded"  style="height: 150px;width: 100%;">
						</a>
					</div>
				</div-->
			</div>
		</div>
		<div class="col-md-12 pt-5" id="infoProgramasBen" style="display: none;">
			<h4 class="text-center"> PROGRAMAS DE BENEFICIARIO</h4>
			<div class="row" id="PorgramasFromBneficiario">

			</div>
		</div>
	</div>
	<hr>
</div>
<script type="text/javascript">
	//$('#hrefMap1').attr('href','?view=map&params='+JSON.stringify({"location":""}));
</script>
