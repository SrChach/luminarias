<form class="card border-dark" id="form_user">
	<input type="hidden" name="csrf_token" value="<?php echo $csrf_token ?>">
	<h5 class="card-header bg-dark text-center">DATOS DE USUARIO</h5>
	<div class="card-body">
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">Nombre: </span>
  			</div>
  			<input type="text" name="NOMBRE" class="form-control" required>
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">Apellido Paterno: </span>
  			</div>
  			<input type="text" name="APATERNO" class="form-control" required>
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">Apellido Materno: </span>
  			</div>
  			<input type="text" name="AMATERNO" class="form-control" required>
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">EMAIL: </span>
  			</div>
  			<input type="email" name="EMAILREAL" class="form-control" required>
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">USUARIO: </span>
  			</div>
  			<input type="text" name="EMAIL" class="form-control" required>
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">CONTRASEÑA: </span>
  			</div>
  			<input type="text" name="PASS" class="form-control" required>
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">CELULAR: </span>
  			</div>
  			<input type="text" name="CELULAR" class="form-control">
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">IMEI DISPOSITIVO: </span>
  			</div>
  			<input type="text" name="IMEI" class="form-control">
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">PERFIL: </span>
  			</div>
  			<select class="form-control" name="IDPERFIL" required>
				<option value="">Seleccione...</option>
  			</select>
		</div>
		<div class="input-group input-group-sm mb-3" id="content_supervisores" style="display:none">
  			<div class="input-group-prepend">
    			<span class="input-group-text">SUPERVISOR: </span>
  			</div>
  			<select class="form-control" name="SUPERVISOR" >
				<option value="">Seleccione...</option>
  			</select>
		</div>
		<div class="input-group input-group-sm mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">ESTATUS: </span>
  			</div>
  			<select class="form-control" name="STATUS">
				<option value="">Seleccione...</option>
				<option value="1" selected>ACTIVO</option>
				<option value="0">INACTIVO</option>
  			</select>
		</div>
		<div class="input-group input-group-sm mb-3" id="content_work_zone" style="display:none">
  			<div class="input-group-prepend">
    			<span class="input-group-text">ZONA DE TRABAJO: </span>
  			</div>
  			<select class="custom-select custom-select-sm" name="ZONATRABAJO">
				<option value="0" selected>Seleccione...</option>
  			</select>
		</div>
	</div>
	<div class="card-footer text-center">
		<button type="submit" class="btn btn-sm btn-outline-success">Guardar</button>
		<button type="reset" class="btn btn-sm btn-outline-secondary">Cancelar</button>
	</div>
</form>
