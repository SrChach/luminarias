<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 offset-md-4">
            <form action="javascript:savePass();" class="card">
                <h5 class="card-header">Nueva Contraseña</h5>
                <div class="card-body">
                    <input type="hidden" name="csrf_token" value="<?php echo $csrf_token ?>">
                    <input type="hidden" name="key" value="<?php echo $_SESSION['IDUSUARIO'] ?>">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Ingrese Nueva Contraseña</span>
                        </div>
                        <input type="password" class="form-control" name="newpass" required>
                        <div class="input-group-prepend">
                            <button type="button" class="input-group-text" style="cursor:pointer"><i class="fa fa-eye"></i></button typpe="button">
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Confirme Contraseña</span>
                        </div>
                        <input type="password" class="form-control" name="confirm_pass" required>
                        <div class="input-group-prepend">
                            <button type="button" class="input-group-text" style="cursor:pointer"><i class="fa fa-eye"></i></button typpe="button">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Guardar Cambios</button>
                    <button type="reset" class="btn btn-secondary">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    savePass = () =>{
        let data = serializeFormJSON($('.card'));
        data.opcion = 'Account';
        data.action = 'setNewPass';
        let request ={
			route: './controller/UserController.php',
			data: data,
		}
		Fetch(request).then((response)=>{
			console.log(response);
			if (response.codigo==true) {
                notificar('', response.message,'info');
                setTimeout(()=>{
                    location.href = '?salir=1';
                },3500);
			}else{
				notificar('', response.message,'error');
			}
		});
    }
</script>