
<div class="container-fluid">
	<div class="row">
		<div class="col table-responsive">
			<table class="table table-bordered table-hover table-sm" id="tableUsers">
				<thead class="thead-green">
					<tr>
						<th class="text-center">#</th>
						<th>NOMBRE</th>
						<th>APELLIDO PATERNO</th>
						<th>APELLIDO MATERNO</th>
						<th>EMAIL</th>
						<th>USUARIO</th>
						<th>CONSTRASEÑA</th>
						<th>CELULAR</th>
						<th>PERFIL</th>
						<th>IMEI</th>
						<th>ESTATUS</th>
						<th></th>
					</tr>
				</thead>
				<tbody class="body_tbUsers">

				</tbody>
			</table>
		</div>
		<?php include 'add.php'; ?>
	</div>
</div>





	<!--script type="text/javascript" src="./assets/js/<?php echo $_GET['view'] ?>.js"></script-->
	<script type="text/javascript">
		var perfilLogin=<?php echo $_SESSION['PERFIL'] ?>;
		var tableUsers;

		$(function () {
			console.log(perfilLogin);
			getUsers();

			$('#form_user').attr('action','javascript:save();')
			$('button[type=reset]').click(function(event) {
				/* Act on the event */
				$('#modalAddUSer').modal('hide');
			});
		});

		function getUsers() {
			load();
			//$("#modalLoad").modal("show");
			var url="./controller/UserController.php";
			objet={opcion:'getAll',csrf_token:'<?php echo $csrf_token ?>'};

			ajaxCallback(objet,url,function (respuesta){
            	let res = JSON.parse(respuesta);
				if (res.codigo==true) {
					makeTable(res.data);
					tableUsers = tableExport2('#tableUsers','Usuarios');
					makeOptionAddUser();
					$('select[name=IDPERFIL]').html(buildOptions(res.perfiles,'PERFIL','IDPERFIL'));
					$('select[name=SUPERVISOR]').html(buildOptions(res.supervisores,'SUPERVISOR','IDUSUARIO'));
				}else{
					//console.error(res.message);
					notificar('ALERTA! ',res.message,'warning');
				}
				load(false);
    		});

			$('select[name=IDPERFIL]').change(function(event) {
				/* Act on the event */
				if (this.value==9) {
					$('#content_supervisores').show();
				}else{
					$('#content_supervisores').hide();
					$('select[name=SUPERVISOR]').val("");
				}
			});
		}

		makeTable = (users) =>{
			let tbody = "";
			let htmlstatus = "";
			let htmlstatusOption = "";
			let myCipher = cipher('G214SolucionesDeInformacion');
			$.each(users,function(index, user) {
				/*let encrypt = myCipher(user.IDUSUARIO);
				console.log(encrypt);
				let myDecipher = decipher('G214SolucionesDeInformacion')
				let desc = myDecipher(encrypt);
				console.log(desc);*/
				if (user.STATUS==1) {
					htmlstatus = `<span class="badge badge-success">ACTIVO</span>`;
					htmlstatusOption = `<a type="button" class="btn btn-sm btn-outline-danger" data-action="0" data-filter="${user.IDUSUARIO}" data-toggle='tooltip' data-placement='top' title='DESACTIVAR CUENTA'><i class="fa fa-minus"></i></a>`;
				}else{
					htmlstatus = `<span class="badge badge-danger">INACTIVO</span>`;
					htmlstatusOption = `<a type="button" class="btn btn-sm btn-outline-success" data-action="1" data-filter="${user.IDUSUARIO}" data-toggle='tooltip' data-placement='top' title='ACTIVAR CUENTA'><i class="fa fa-plus"></i></a>`;
				}
				tbody += `<tr>
							<th class="text-center">${index+1}</th>
							<td>${user.NOMBRE}</td>
							<td>${user.APATERNO}</td>
							<td>${user.AMATERNO}</td>
							<td>${(user.EMAILREAL!=null) ? '<a href="mailto:'+user.EMAILREAL+'">'+user.EMAILREAL+'</a>': 'Sin dato'}</td>
							<td>${user.EMAIL}</td>
							<td>${user.PASS}</td>
							<td>${user.CELULAR}</td>
							<td>${user.PERFIL}</td>
							<td>${(user.IMEI!=null) ? user.IMEI : 'NO'}</td>
							<td class="text-center">${htmlstatus}</td>
							<td>
								<a href="?view=users&act=edit&key=${myCipher(user.IDUSUARIO)}" class="btn btn-sm btn-outline-info" data-toggle='tooltip' data-placement='top' title='VER Y EDITAR CUENTA'><i class="fa fa-eye"></i></a>
								${htmlstatusOption}
							</td>
						</tr>`;
				//console.log(user);
			});
			$('.body_tbUsers').html(tbody);

			$('a[type="button"]').click(function(event) {
				/* Act on the event */
				let action = $(event.currentTarget).attr('data-action');
				let request ={
					route: './controller/UserController.php',
					data: {opcion:'Account',action:'downUp_Account',status:action,key:btoa($(this).data().filter),csrf_token:'<?php echo $csrf_token ?>'},
				}
				Fetch(request).then((response)=>{
					if (response.codigo==true) {
						if (action=='0') {
							$(this).attr('data-action', 1);
							$(this).attr('data-original-title', 'ACTIVAR CUENTA');
							$(this).removeClass('btn-outline-danger').addClass('btn-outline-success');
							$(this).find('i').removeClass('fa-minus').addClass('fa-plus');
							$(this).parents('tr').find('.badge').removeClass('badge-success').addClass('badge-danger').text('INACTIVO');
						}else{
							$(this).attr('data-action', 0);
							$(this).attr('data-original-title', 'DESACTIVAR CUENTA');
							$(this).removeClass('btn-outline-success').addClass('btn-outline-danger');
							$(this).find('i').removeClass('fa-plus').addClass('fa-minus');
							$(this).parents('tr').find('.badge').removeClass('badge-danger').addClass('badge-success').text('ACTIVO');
						}
						notificar('',response.message,'success');
					}else{notificar('',response.message,'danger');}
				});

			});
		}

		makeOptionAddUser = () =>{
			let buttonAddUser = `<button type="button" class="btn btn-sm btn-dark" data-toggle="modal" data-target="#modalAddUSer"><i class="fa fa-plus"></i>Agregar</button>`;
			$('.optionsTable').html(buttonAddUser);
		}

		save = () =>{
			let jsonSend = serializeFormJSON($('#form_user'));
			jsonSend.opcion = 'Account';
			jsonSend.action = 'save';

			let request ={
				route: './controller/UserController.php',
				data: jsonSend,
			}
			console.log(request);
			Fetch(request).then((response)=>{
				console.log(response);
				if (response.codigo===true) {
					tableUsers.destroy();
					notificar('EXITO !', response.message,'success');
					getUsers();
					$('#form_user')[0].reset();
					$('#modalAddUSer').modal('hide');
				}else{
					notificar('Lo siento !', response.message,'warning');
				}
			});
		}

	</script>
