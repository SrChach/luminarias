
<!-- Modal -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">ELIMINAR CUENTA: <small id="nameAccount"></small></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col">
            <label>INGRESE CONTRASEÑA PARA ELIMINAR CUENTA:</label>
            <br>
            <small>* Recuerde que las cuentas eliminadas no podrán recuperarse!</small>
            <input type="password" id="ipPD" class="form-control">
            <input type="hidden" id="idUser">
            <!--input type="text" id="rowNum"-->
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id='btnDeleteOK'>OK</button>
        <button type="button" class="btn btn-primary"  data-dismiss="modal" id="btnDeleteCancel">CANELAR</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#btnDeleteOK').click(function(){
    deleteAccount($('#idUser').val(),$('#rowNum').val());
    $('#nameAccount').html('');
    $('#ipPD').val('');
    $('#idUser').val('');
    //$('#rowNum').val('');
    $("#modalDelete").modal("hide");
  }); 
  $('#btnDeleteCancel').click(function(){
    $('#nameAccount').html('');
    $('#ipPD').val('');
    $('#idUser').val('');
    //$('#rowNum').val('');
    cancel(1);
  }); 
</script>