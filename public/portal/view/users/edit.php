<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<?php include 'form.php'; ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	var keyUserChiper = '<?php echo $_GET['key'] ?>'
	var myDecipher = decipher('G214SolucionesDeInformacion');
	let keyUser = myDecipher(keyUserChiper);
	$(()=>{
		//console.log(keyUser);
		load();

		$('#form_user').attr('action','javascript:update();')

		let request ={
			route: './controller/UserController.php',
			data: {opcion:'Account',action:'get',key:btoa(keyUser),csrf_token:'<?php echo $csrf_token ?>'},
		}
		Fetch(request).then((response)=>{
			console.log(response);
			if (response.codigo==true) {
				setValues(response.data);
			}else{
				notificar('Lo Siento!', response.message,'warning');
			}
			load(false);
		});


	});

	setValues = (data) =>{
		//console.log('SETVA',data);
		$('select[name=IDPERFIL]').html(buildOptions(data.perfiles,'PERFIL','IDPERFIL'));
		$('select[name=SUPERVISOR]').html(buildOptions(data.supervisores,'SUPERVISOR','IDUSUARIO'));
		let user = data.user;
		let keys = Object.keys(user);
		keys.forEach((value,index)=>{
			if (!parseInt(value) && value!=='0') {
				//console.log(value,user[value]);
				$('[name='+value+']').val(user[value]);
			}
		});
		$('#content_supervisores').show();
	}
	update = () => {
		load();
		let jsonSend = serializeFormJSON($('#form_user'));
		jsonSend.key = btoa(keyUser);
		jsonSend.opcion = 'Account';
		jsonSend.action = 'update';

		let request ={
			route: './controller/UserController.php',
			data: jsonSend,
		}
		console.log(request);
		Fetch(request).then((response)=>{
			console.log(response);
			if (response.codigo==true) {
				setValues(response.data.data);
				notificar('', response.message,'success');
			}else{
				notificar('', response.message,'error');
			}
			load(false);
		});
	}
</script>
