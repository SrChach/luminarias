<div class="container-fluid">
	<div class="row">
		<div class="col-12" id="Logs"></div>
	</div>
	<div class="row" id="content-body" style="display: none;">
		<div class="row">
			<div class="col-12">
				<button class="btn btn-outline-secondary" id="btnBack" style="display: none;">
					<i class="fa fa-angle-double-left" style="color:#276092;"></i>
				</button>
			</div>
		</div>
		<?php 
			include_once('./view/statistics/sections/cards.php');
			include_once('./view/statistics/sections/chartDonut.php');
			include_once('./view/statistics/sections/levelDelegacion.php');
			include_once('./view/statistics/sections/levelSubdelegacion.php');
			include_once('./view/statistics/sections/levelMunicipio.php');
			include_once('./view/statistics/sections/levelLocalidad.php');
		?>
	</div>
</div>

<script type="text/javascript">
	var delegaciones;
	var selectDelegaciones;
	var datasSubdelegaciones;
	var dataDonutDelegacion=[];
	var dataTableDelegaciones=[];
	var totalFamilias=0;
	var totalHabitantes=0;
	var ykeys=['0_A_5', '5_A_16', '16_A_30', '30_A_60', '60_A_200','TOTAL'];
	var onchange="estadisticosSubDel(this.value);";
	var optionsTooltip;
	$(function () {
		load();
		objet={opcion: 'Estadisticos',csrf_token:'<?php echo $csrf_token ?>',action:'Main'};
		$.post('./controller/EstadisitcosController.php', objet, function(response, textStatus, xhr) {
			try{
				delegaciones=$.parseJSON(response);
				if (delegaciones.CODIGO==true) {
					datasSubdelegaciones=delegaciones.DATOS;
					Index();
				}else{
					showAlertInwindow('#Logs','warning',delegaciones.DATOS);
					$('#content-body').hide('slow');
				}
			}catch (err){
				console.log(err);
			}
			load(false);
		});
	});

	function Index() {
		datasDelegaciones=delegaciones.DATOS;
		$('#content-body').fadeIn(2500);
		selectDelegaciones = '<div class="input-group mb-3">\
								<div class="input-group-prepend">\
									<span class="input-group-text" id="titleForFilter">OFICINA REGIONAL: </span>\
  								</div>\
								<select class="custom-select" id="nivelesSelected" onchange="estadisticosSubDel(this.value);">'+buildOptions(datasDelegaciones,'DELEGACIÓN','ID_DEL')+'</select>\
							</div>';
		$('#subItemsTitle').html(selectDelegaciones);
		var sumHombres=0;
		var sumMujeres=0;
		var dataChartHombresDelegaciones=[];
		var dataChartMujeresDelegaciones=[];
		var totalMujeresDelegacion=0;
		var totalHombresDelegacion=0;
		totalFamilias=0;
		totalHabitantes=0;
		dataTableDelegaciones=[];
		dataDonutDelegacion=[];
		$.each(datasDelegaciones,function (i,item) {
			dataTableDelegaciones.push({DELEGACIÓN:item.DELEGACIÓN,HOMBRES:item.HOMBRES,MUJERES:item. MUJERES});
			sumHombres=sumHombres+parseInt(item.HOMBRES);
			sumMujeres=sumMujeres+parseInt(item.MUJERES);
			totalFamilias=totalFamilias+parseInt(item.FAMILIAS);
			totalHabitantes=totalHabitantes+parseInt(item.HAB);
			totalMujeresDelegacion=totalMujeresDelegacion+parseInt(item.M_ENTRE_0_5)+parseInt(item.M_ENTRE_5_16)+parseInt(item.M_ENTRE_16_30)+parseInt(item.M_ENTRE_30_60)+parseInt(item.M_ENTRE_60_200);
			totalHombresDelegacion=totalHombresDelegacion+parseInt(item.H_ENTRE_0_5)+parseInt(item.H_ENTRE_5_16)+parseInt(item.H_ENTRE_16_30)+parseInt(item.H_ENTRE_30_60)+parseInt(item.H_ENTRE_60_200);
			dataChartMujeresDelegaciones.push({DELEGACIÓN:item.DELEGACIÓN,'0_A_5':item.M_ENTRE_0_5,'5_A_16':item.M_ENTRE_5_16,'16_A_30':item.M_ENTRE_16_30,'30_A_60':item.M_ENTRE_30_60,'60_A_200':item.M_ENTRE_60_200,TOTAL:totalMujeresDelegacion});
			dataChartHombresDelegaciones.push({DELEGACIÓN:item.DELEGACIÓN,'0_A_5':item.H_ENTRE_0_5,'5_A_16':item.H_ENTRE_5_16,'16_A_30':item.H_ENTRE_16_30,'30_A_60':item.H_ENTRE_30_60,'60_A_200':item.H_ENTRE_60_200,TOTAL:totalHombresDelegacion});
			totalMujeresDelegacion=0;
			totalHombresDelegacion=0;

		});
		dataDonutDelegacion.push({value:sumMujeres,label:'MUJERES'},{value:sumHombres,label:'HOMBRES'});
		$('#sumMujeres').html(separa(sumMujeres));
		$('#sumHombres').html(separa(sumHombres));
		$('#totalFamilias').html(separa(totalFamilias));
		$('#totalHabitantes').html(separa(totalHabitantes));
		$('#totalGenero').html(separa(sumMujeres));
		chartDonut(dataDonutDelegacion);
		renderTable('#tableDelegaciones',dataTableDelegaciones,'',1,0,'table-dinamicDel','','','');
		tableExport2('#table-dinamicDel','ESTADÍSTICOS DELEGACIÓN.');
		settingsTable();
		chartDelegaciones(dataTableDelegaciones,'DELEGACIÓN',['MUJERES','HOMBRES'],true);
		chartMujeresDelegacion(dataChartMujeresDelegaciones,'DELEGACIÓN',ykeys,true);
		chartHombresDelegacion(dataChartHombresDelegaciones,'DELEGACIÓN',ykeys,true);
	}

	function chartDonut(data='') {
		$("#chartDonut").empty();
		$('#chartDonut').html('');
		var donut=new Morris.Donut({
      			element: 'chartDonut',
      			data: data,
      			backgroundColor: '#ccc',
      			labelColor: '#000',
      			colors: [
		        	'#0b62a4',
		        	'#7991a2'
      				]
      			//formatter: function (x) { return x + "%"}
    	});
    	for(i = 0; i < donut.segments.length; i++) {
        	donut.segments[i].handlers['hover'].push( function(i){
            	$('#morrisdetails-item .morris-hover-row-label').text(donut.data[i].label);
            	$('#morrisdetails-item .morris-hover-point').text(separa(donut.data[i].value));
        	});
    	}
	}

	function chartDelegaciones(data,xkey,ykey,stack,labels='') {
        $("#chartDelegaciones").empty();
        $('#chartDelegaciones').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartDelegaciones',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>OFICINA REGIONAL: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function chartMujeresDelegacion(data,xkey,ykey,stack,labels='') {
        $('#chartMujeresDelegacion').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartMujeresDelegacion',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>OFICINA REGIONAL: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function chartHombresDelegacion(data,xkey,ykey,stack,labels='') {
        $('#chartHombresDelegacion').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartHombresDelegacion',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>OFICINA REGIONAL: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    var datasSubdelegaciones;
    var subdelegaciones;
	var dataDonutSubdelegacion=[];
	var dataTableSubdelegaciones=[];
	var totalFamiliasSubdel=0;
	var totalHabitantesSubdel=0;
    function estadisticosSubDel(idDel) {
    	$('#content-Delegacion').hide();
    	$('#content-body').fadeOut(1000);
    	$('#nivelesSelected').attr('onchange','estadisticosMunicipios(this.value)');
    	load();
		objet={opcion: 'Estadisticos',csrf_token:'<?php echo $csrf_token ?>',action:'getDatasSubdelegacion', id_del:idDel};
		$.post('./controller/EstadisitcosController.php', objet, function(response, textStatus, xhr) {
			try{
				subdelegaciones=$.parseJSON(response);
				if (subdelegaciones.CODIGO==true) {
					IndexSubdelegacion();
				}else{
					showAlertInwindow('#Logs','warning',subdelegaciones.DATOS);
					$('#content-body').hide('slow');
				}
			}catch (err){
				console.log(err);
			}
			load(false);
		});
    }
    function IndexSubdelegacion() {
    	datasSubdelegaciones=subdelegaciones.DATOS;
    	console.log(datasSubdelegaciones);
    	$('#content-body').fadeIn(2500);
		$('#content-Subdelegacion').fadeIn(2500);
		$('#titlePage-2').html(' OFICINA REGIONAL '+$('#nivelesSelected option:selected').text().toUpperCase());
		$('#titleForFilter').html('OFICINA SUBREGIONAL: ');
		$('#nivelesSelected').html(buildOptions(datasSubdelegaciones,'SUBDELEGACIÓN','ID_SUB'));
		var sumHombres=0;
		var sumMujeres=0;
		var dataChartHombresDelegaciones=[];
		var dataChartMujeresDelegaciones=[];
		var totalMujeresDelegacion=0;
		var totalHombresDelegacion=0;
		totalFamilias=0;
		totalHabitantes=0;
		dataTableSubdelegaciones=[];
		dataDonutSubdelegacion=[];
		$.each(datasSubdelegaciones,function (i,item) {
			dataTableSubdelegaciones.push({SUBDELEGACIÓN:item.SUBDELEGACIÓN,HOMBRES:item.HOMBRES,MUJERES:item. MUJERES});
			sumHombres=sumHombres+parseInt(item.HOMBRES);
			sumMujeres=sumMujeres+parseInt(item.MUJERES);
			totalFamilias=totalFamilias+parseInt(item.FAMILIAS);
			totalHabitantes=totalHabitantes+parseInt(item.HAB);
			totalMujeresDelegacion=totalMujeresDelegacion+parseInt(item.M_ENTRE_0_5)+parseInt(item.M_ENTRE_5_16)+parseInt(item.M_ENTRE_16_30)+parseInt(item.M_ENTRE_30_60)+parseInt(item.M_ENTRE_60_200);
			totalHombresDelegacion=totalHombresDelegacion+parseInt(item.H_ENTRE_0_5)+parseInt(item.H_ENTRE_5_16)+parseInt(item.H_ENTRE_16_30)+parseInt(item.H_ENTRE_30_60)+parseInt(item.H_ENTRE_60_200);
			dataChartMujeresDelegaciones.push({SUBDELEGACIÓN:item.SUBDELEGACIÓN,'0_A_5':item.M_ENTRE_0_5,'5_A_16':item.M_ENTRE_5_16,'16_A_30':item.M_ENTRE_16_30,'30_A_60':item.M_ENTRE_30_60,'60_A_200':item.M_ENTRE_60_200,TOTAL:totalMujeresDelegacion});
			dataChartHombresDelegaciones.push({SUBDELEGACIÓN:item.SUBDELEGACIÓN,'0_A_5':item.H_ENTRE_0_5,'5_A_16':item.H_ENTRE_5_16,'16_A_30':item.H_ENTRE_16_30,'30_A_60':item.H_ENTRE_30_60,'60_A_200':item.H_ENTRE_60_200,TOTAL:totalHombresDelegacion});
			totalMujeresDelegacion=0;
			totalHombresDelegacion=0;

		});
		dataDonutSubdelegacion.push({value:sumMujeres,label:'MUJERES'},{value:sumHombres,label:'HOMBRES'});

		$('#sumMujeres').html(separa(sumMujeres));
		$('#sumHombres').html(separa(sumHombres));
		$('#totalFamilias').html(separa(totalFamilias));
		$('#totalHabitantes').html(separa(totalHabitantes));
		$('#totalGenero').html(separa(sumMujeres));

		chartDonut(dataDonutSubdelegacion);

		renderTable('#tableSubdelegaciones',dataTableSubdelegaciones,'',1,0,'table-dinamicSubdel','','','');
		tableExport2('#table-dinamicSubdel','ESTADÍSTICOS SUBDELEGACIÓN.');
		settingsTable();

		chartSubdelegaciones(dataTableSubdelegaciones,'SUBDELEGACIÓN',['MUJERES','HOMBRES'],true);
		chartMujeresSubdelegacion(dataChartMujeresDelegaciones,'SUBDELEGACIÓN',ykeys,true);
		chartHombresSubdelegacion(dataChartHombresDelegaciones,'SUBDELEGACIÓN',ykeys,true);

		$('#btnBack').show('slow');
		$('#btnBack').tooltip('dispose');
		optionsTooltip={title:'REGRESAR A OFICINAS REGIONALES',placement:'right',back:'delegaciones'};
		$('#btnBack').tooltip(optionsTooltip);
    }

    function chartSubdelegaciones(data,xkey,ykey,stack,labels='') {
        $("#chartSubdelegaciones").empty();
        $('#chartSubdelegaciones').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartSubdelegaciones',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>OFICINA SUBREGIONAL: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function chartMujeresSubdelegacion(data,xkey,ykey,stack,labels='') {
        $('#chartMujeresSubdelegacion').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartMujeresSubdelegacion',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>OFICINA SUBREGIONAL: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function chartHombresSubdelegacion(data,xkey,ykey,stack,labels='') {
        $('#chartHombresSubdelegacion').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartHombresSubdelegacion',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>OFICINA SUBREGIONAL: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }


    var datasMunicipios;
    var municipios;
	var dataDonutMunicipio=[];
	var dataTableMunicipios=[];
	var totalFamiliasMunicipio=0;
	var totalHabitantesMmunicipio=0;
    function estadisticosMunicipios(idSubdel) {
    	$('#nivelesSelected').attr('onchange','estadisticosLocalidades(this.value)');
    	$('#content-body').fadeOut(1000);
    	$('#content-Subdelegacion').hide();
    	load();
		objet={opcion: 'Estadisticos',csrf_token:'<?php echo $csrf_token ?>',action:'getDatasMunicipio', id_sub:idSubdel};
		$.post('./controller/EstadisitcosController.php', objet, function(response, textStatus, xhr) {
			try{
				municipios=$.parseJSON(response);
				if (municipios.CODIGO==true) {
					IndexMunicipio();
				}else{
					showAlertInwindow('#Logs','warning',municipios.DATOS);
					$('#content-body').hide('slow');
				}
			}catch (err){
				console.log(err);
			}
			load(false);
		});
    }

    function IndexMunicipio() {
    	datasMunicipios=municipios.DATOS;
    	console.log(datasMunicipios);
    	$('#content-body').fadeIn(2500);
		$('#content-Municipio').show('slow');
		$('#titlePage-2').html(' OFICINA SUBREGIONAL '+$('#nivelesSelected option:selected').text().toUpperCase());
		$('#titleForFilter').html('MUNICIPIO: ');
		$('#nivelesSelected').html(buildOptions(datasMunicipios,'MUNICIPIO','CVE_MUN'));

		var sumHombres=0;
		var sumMujeres=0;
		var dataChartHombresMunicipios=[];
		var dataChartMujeresMunicipios=[];
		var totalMujeresDelegacion=0;
		var totalHombresDelegacion=0;
		totalFamilias=0;
		totalHabitantes=0;
		dataTableMunicipios=[];
		dataDonutMunicipio=[];

		$.each(datasMunicipios,function (i,item) {
			dataTableMunicipios.push({MUNICIPIO:item.MUNICIPIO,HOMBRES:item.HOMBRES,MUJERES:item. MUJERES});
			sumHombres=sumHombres+parseInt(item.HOMBRES);
			sumMujeres=sumMujeres+parseInt(item.MUJERES);
			totalFamilias=totalFamilias+parseInt(item.FAMILIAS);
			totalHabitantes=totalHabitantes+parseInt(item.HAB);
			totalMujeresDelegacion=totalMujeresDelegacion+parseInt(item.M_ENTRE_0_5)+parseInt(item.M_ENTRE_5_16)+parseInt(item.M_ENTRE_16_30)+parseInt(item.M_ENTRE_30_60)+parseInt(item.M_ENTRE_60_200);
			totalHombresDelegacion=totalHombresDelegacion+parseInt(item.H_ENTRE_0_5)+parseInt(item.H_ENTRE_5_16)+parseInt(item.H_ENTRE_16_30)+parseInt(item.H_ENTRE_30_60)+parseInt(item.H_ENTRE_60_200);
			dataChartMujeresMunicipios.push({MUNICIPIO:item.MUNICIPIO,'0_A_5':item.M_ENTRE_0_5,'5_A_16':item.M_ENTRE_5_16,'16_A_30':item.M_ENTRE_16_30,'30_A_60':item.M_ENTRE_30_60,'60_A_200':item.M_ENTRE_60_200,TOTAL:totalMujeresDelegacion});
			dataChartHombresMunicipios.push({MUNICIPIO:item.MUNICIPIO,'0_A_5':item.H_ENTRE_0_5,'5_A_16':item.H_ENTRE_5_16,'16_A_30':item.H_ENTRE_16_30,'30_A_60':item.H_ENTRE_30_60,'60_A_200':item.H_ENTRE_60_200,TOTAL:totalHombresDelegacion});
			totalMujeresDelegacion=0;
			totalHombresDelegacion=0;
		});
		dataDonutMunicipio.push({value:sumMujeres,label:'MUJERES'},{value:sumHombres,label:'HOMBRES'});

		$('#sumMujeres').html(separa(sumMujeres));
		$('#sumHombres').html(separa(sumHombres));
		$('#totalFamilias').html(separa(totalFamilias));
		$('#totalHabitantes').html(separa(totalHabitantes));
		$('#totalGenero').html(separa(sumMujeres));

		chartDonut(dataDonutMunicipio);

		renderTable('#tableMunicipios',dataTableMunicipios,'',1,0,'table-dinamicMunicipio','','','');
		tableExport2('#table-dinamicMunicipio','ESTADÍSTICOS MUNICIPIO.');
		settingsTable();

		chartMunicipios(dataTableMunicipios,'MUNICIPIO',['MUJERES','HOMBRES'],true);
		chartMujeresMunicipio(dataChartMujeresMunicipios,'MUNICIPIO',ykeys,true);
		chartHombresMunicipio(dataChartHombresMunicipios,'MUNICIPIO',ykeys,true);


		$('#btnBack').show('slow');
		$('#btnBack').tooltip('dispose');
		optionsTooltip={title:'REGRESAR A OFICINAS SUBREGIONALES',placement:'right',back:'subdel'};
		$('#btnBack').tooltip(optionsTooltip);
    }

    function chartMunicipios(data,xkey,ykey,stack,labels='') {
        $("#chartMunicipios").empty();
        $('#chartMunicipios').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartMunicipios',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>MUNICIPIO: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function chartMujeresMunicipio(data,xkey,ykey,stack,labels='') {
        $('#chartMujeresMunicipio').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartMujeresMunicipio',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>MUNICIPIO: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function chartHombresMunicipio(data,xkey,ykey,stack,labels='') {
        $('#chartHombresMunicipio').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartHombresMunicipio',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>MUNICIPIO: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }


    var datasLocalidades;
    var loacliades;
	var dataDonutLocalidad=[];
	var dataTableLocalidades=[];
	var totalFamiliasLocalidad=0;
	var totalHabitantesMLocalidad=0;
    function estadisticosLocalidades(idMun) {
    	$('#nivelesSelected').attr('onchange','');
    	$('#content-body').fadeOut(1000);
    	$('#content-Municipio').hide();
    	load();
		objet={opcion: 'Estadisticos',csrf_token:'<?php echo $csrf_token ?>',action:'getDatasLocalidad', id_mun:idMun};
		$.post('./controller/EstadisitcosController.php', objet, function(response, textStatus, xhr) {
			try{
				localidades=$.parseJSON(response);
				if (localidades.CODIGO==true) {
					IndexLocalidad();
				}else{
					showAlertInwindow('#Logs','warning',localidades.DATOS);
					$('#content-body').hide('slow');
				}
			}catch (err){
				console.log(err);
			}
			load(false);
		});
    }
    function IndexLocalidad() {
    	datasLocalidades=localidades.DATOS;
    	console.log(datasLocalidades);
    	$('#content-body').fadeIn(2500);
		$('#content-Localidad').show('slow');
		$('#titlePage-2').html(' MUNICIPIO '+$('#nivelesSelected option:selected').text().toUpperCase());
		$('#subItemsTitle').hide('slow');
		//$('#nivelesSelected').html(buildOptions(datasLocalidades,'MUNICIPIO','CVE_MUN'));

		var sumHombres=0;
		var sumMujeres=0;
		var dataChartHombresLocalidades=[];
		var dataChartMujeresLocalidades=[];
		var totalMujeresDelegacion=0;
		var totalHombresDelegacion=0;
		totalFamilias=0;
		totalHabitantes=0;
		dataTableLocalidades=[];
		dataDonutLocalidad=[];

		$.each(datasLocalidades,function (i,item) {
			dataTableLocalidades.push({LOCALIDAD:item.LOCALIDAD,HOMBRES:item.HOMBRES,MUJERES:item. MUJERES});
			sumHombres=sumHombres+parseInt(item.HOMBRES);
			sumMujeres=sumMujeres+parseInt(item.MUJERES);
			totalFamilias=totalFamilias+parseInt(item.FAMILIAS);
			totalHabitantes=totalHabitantes+parseInt(item.HAB);
			totalMujeresDelegacion=totalMujeresDelegacion+parseInt(item.M_ENTRE_0_5)+parseInt(item.M_ENTRE_5_16)+parseInt(item.M_ENTRE_16_30)+parseInt(item.M_ENTRE_30_60)+parseInt(item.M_ENTRE_60_200);
			totalHombresDelegacion=totalHombresDelegacion+parseInt(item.H_ENTRE_0_5)+parseInt(item.H_ENTRE_5_16)+parseInt(item.H_ENTRE_16_30)+parseInt(item.H_ENTRE_30_60)+parseInt(item.H_ENTRE_60_200);
			dataChartMujeresLocalidades.push({LOCALIDAD:item.LOCALIDAD,'0_A_5':item.M_ENTRE_0_5,'5_A_16':item.M_ENTRE_5_16,'16_A_30':item.M_ENTRE_16_30,'30_A_60':item.M_ENTRE_30_60,'60_A_200':item.M_ENTRE_60_200,TOTAL:totalMujeresDelegacion});
			dataChartHombresLocalidades.push({LOCALIDAD:item.LOCALIDAD,'0_A_5':item.H_ENTRE_0_5,'5_A_16':item.H_ENTRE_5_16,'16_A_30':item.H_ENTRE_16_30,'30_A_60':item.H_ENTRE_30_60,'60_A_200':item.H_ENTRE_60_200,TOTAL:totalHombresDelegacion});
			totalMujeresDelegacion=0;
			totalHombresDelegacion=0;
		});
		dataDonutLocalidad.push({value:sumMujeres,label:'MUJERES'},{value:sumHombres,label:'HOMBRES'});

		$('#sumMujeres').html(separa(sumMujeres));
		$('#sumHombres').html(separa(sumHombres));
		$('#totalFamilias').html(separa(totalFamilias));
		$('#totalHabitantes').html(separa(totalHabitantes));
		$('#totalGenero').html(separa(sumMujeres));

		chartDonut(dataDonutLocalidad);

		renderTable('#tableLocalidades',dataTableLocalidades,'',1,0,'table-dinamicLocalidad','','','');
		tableExport2('#table-dinamicLocalidad','ESTADÍSTICOS LOCALIDAD.');
		settingsTable();

		chartLocalidades(dataTableLocalidades,'LOCALIDAD',['MUJERES','HOMBRES'],true);
		chartMujeresLocalidad(dataChartMujeresLocalidades,'LOCALIDAD',ykeys,true);
		chartHombresLocalidad(dataChartHombresLocalidades,'LOCALIDAD',ykeys,true);

		$('#btnBack').show('slow');
		$('#btnBack').tooltip('dispose');
		optionsTooltip={title:'REGRESAR A MUNICIPIOS',placement:'right',back:'municipios'};
		$('#btnBack').tooltip(optionsTooltip);

    }

    function chartLocalidades(data,xkey,ykey,stack,labels='') {
        $("#chartLocalidades").empty();
        $('#chartLocalidades').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartLocalidades',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>LOCALIDAD: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function chartMujeresLocalidad(data,xkey,ykey,stack,labels='') {
        $('#chartMujeresLocalidad').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartMujeresLocalidad',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>MUNICIPIO: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function chartHombresLocalidad(data,xkey,ykey,stack,labels='') {
        $('#chartHombresLocalidad').html('');
        if (labels=='') {labels=ykey;}
        new Morris.Bar({
            element: 'chartHombresLocalidad',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>MUNICIPIO: "+aux[1];
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }



    $('#btnBack').click(function () {
    	$('#content-body').fadeOut(1000);
    	if (optionsTooltip.back=='delegaciones') {
    		$('#content-Subdelegacion').hide();
    		$('#content-Delegacion').show();
    		Index();
    		$(this).fadeOut('slow');
    	}
    	if (optionsTooltip.back=='subdel') {
    		$('#content-Subdelegacion').show();
    		$('#content-Municipio').hide();
    		IndexSubdelegacion();
    	}
    	if (optionsTooltip.back=='municipios') {
    		$('#content-Localidad').hide();
    		$('#content-Municipio').show();
    		IndexMunicipio();
    	}
    	$(this).tooltip('hide');
    });

</script>