<div class="container-fluid" id="content-Subdelegacion" style="display: none;">
	<div class="row mt-5">
		<div class="col-4 table-responsive boderSALL" id="tableSubdelegaciones" style="font-size: 12px;"></div>
		<div class="col-8 boderSALL table-responsive">
			<h3 class="text-center">OFICINAS SUBREGIONALES.</h3>
			<div id="chartSubdelegaciones" style="height: 400px; padding-bottom: 50px;"></div>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-12 table-responsive boderSALL">
			<h3 class="text-center">MUJERES.</h3>
			<div style="height: 400px; padding-bottom: 50px;" id="chartMujeresSubdelegacion"></div>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-12 table-responsive boderSALL">
			<h3 class="text-center">HOMBRES.</h3>
			<div style="height: 400px; padding-bottom: 50px;" id="chartHombresSubdelegacion"></div>
		</div>
	</div>
</div>