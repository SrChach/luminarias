<div class="container-fluid pt-5">
	<div class="row">
		<div class="col-8 offset-2">
			<div class="row">
				<div class="col-4 mt-3">
					<table class="table table-bordered table-sm">
						<thead class="thead-light">
							<tr>
								<th></th>
								<th>TOTAL</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th style="background: rgb(233,236,239);" align="center" class="text-center"><img src="./assets/img/ICONO-MUJERES - copia.png" class="img-fluid" width="100px" height="100px"></th>
								<td id="sumMujeres"></td>
							</tr>
							<tr>
								<th style="background: rgb(233,236,239);" align="center" class="text-center"><img src="./assets/img/ICONO-HOMBRE - copia.png" class="img-fluid" width="100px" height="100px"></th>
								<td id="sumHombres"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-7 offset-1">
					<div id="chartDonut" style="height: 250px; width: 80%; float: left;margin-left: 20%;">
						
					</div>
        			<!--div id="morrisdetails-item" class="morris-hover morris-default-style" style="position:static;float: left;margin-left:-100px;">
            			<div class="morris-hover-row-label">MUJERES:</div>
            			<div><h5 id="totalGenero" class="morris-hover-point" style="font-size: 10px;"></h5></div>
        			</div-->
				</div>
			</div>
		</div>
	</div>
</div>