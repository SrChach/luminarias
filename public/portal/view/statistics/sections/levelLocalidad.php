<div class="container-fluid" id="content-Localidad" style="display: none;">
	<div class="row mt-5">
		<div class="col-4 table-responsive boderSALL" id="tableLocalidades" style="font-size: 12px;"></div>
		<div class="col-8 boderSALL table-responsive">
			<h3 class="text-center">MUNICIPIOS.</h3>
			<div id="chartLocalidades" style="height: 400px; padding-bottom: 50px;"></div>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-12 table-responsive boderSALL">
			<h3 class="text-center">MUJERES.</h3>
			<div style="height: 400px; padding-bottom: 50px;" id="chartMujeresLocalidad"></div>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-12 table-responsive boderSALL">
			<h3 class="text-center">HOMBRES.</h3>
			<div style="height: 400px; padding-bottom: 50px;" id="chartHombresLocalidad"></div>
		</div>
	</div>
</div>