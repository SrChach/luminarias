<div class="container-fluid">
	<div class="row">
		<div class="col-3 offset-2">
            	<div class="small-box bg-info my_Box2" style="height: 200px;">
              		<div class="col-12 text-center">
                		<h4><img src="./assets/img/ICONO-FAMILIA - copia.png" class="img-fluid" width="150px;" height="150px;"></h4>
                		<h4 id="totalFamilias"></h4>
              		</div>
            	</div>
		</div>
		<div class="col-3 offset-2">
			<div class="small-box bg-success my_Box2" style="height: 200px;">
              	<div class="col-12 text-center">
                	<h4><img src="./assets/img/ICONO-HABITANTES - copia.png" class="img-fluid" width="150px;" height="150px;"></h4>
                	<h4 id="totalHabitantes"></h4>
              	</div>
            </div>
		</div>
	</div>
</div>
