<div class="modal spacing-10" id="modalVerificaPass" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-keyboard="false" data-backdrop="static" style="background: rgba(154, 161, 164, 0.2)">
 <div class="modal-dialog">
   <div class="modal-content">

    <form action="">
      <div class="col-12" id="cargando">
       <div class="modal-header">
        <h5 class="modal-title">VERIFICACIÓN DE CUENTA</h5>
      </div>
      <div class="modal-body" id >
        <p>Por seguridad le agradecemos que realice el cambio de contraseña</p>  
        <div class="row">
         <div class="input-group col-md-12 mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">Nueva contraseña: </span>
          </div>
          <input type="password" class="form-control" id='userValidation'>
        </div>
        <div class="input-group col-md-12 mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">Confirmar nueva contraseña: </span>
          </div>
          <input type="password" class="form-control" id='userValidation_conf'>
        </div>
      </div>
    </div>
    <div class="modal-footer text-center" align="center">
      <input type="button" class="btn btn-outline-primary btn-sm" value="GUARDAR" id="" onclick="guardar();">
    <!-- <label style="color:#2bb273"> Autenticando... </label>
      <i class="fa fa-spinner fa-spin" style="font-size:200%;color:#28A6DE;"></i> -->
    </div>
  </div>
</form>
</div>
</div>
</div>


<script>
  var pass1="0";
  var pass2="00";
  var idusuario='<?php echo $_SESSION['IDUSUARIO']; ?>';
  function  guardar(){
    pass1=document.getElementById("userValidation").value;
    pass2=document.getElementById("userValidation_conf").value;

    if (pass1 == pass2) {
      verificaContraseña();
    } else {
      alert("Los campos ingresados deben coincidir, por favor ingresa nuevamente la contraseña");
      document.getElementById('userValidation').value="";
      document.getElementById('userValidation_conf').value="";
    }

  }



  function  verificaContraseña(){
   var url="./controller/ValidacionContrasenaController.php"; 
   var values={idusuario:idusuario,password:pass1} 
   objet={opcion:'generales',action:'verificaPassword',csrf_token:'<?php echo $csrf_token ?>', values:values};
   ajaxCallback(objet,url,function (respuesta){
    resUser = JSON.parse(respuesta);
    var passDB=resUser.DATOS.user['PASS'];
    if (pass1 != passDB ) {
      cambiaContraseña();
    } else {
      alert("Debe ingresar una contraseña diferente a la registrada  inicialmente");
    }
  });
 }


 function  cambiaContraseña(){
   var url="./controller/ValidacionContrasenaController.php";
   var values={idusuario:idusuario,password:pass1} 
   objet={opcion:'generales',action:'cambiaPassword',csrf_token:'<?php echo $csrf_token ?>', values:values};

   ajaxCallback(objet,url,function (respuesta){
    resUserChange = JSON.parse(respuesta);
    if (resUserChange) {
      alert("Para verificar el cambio deberá ingresar nuevamente al sistema");
      cerrar();
    } else{
      alert("No se pudo realizar el cambio de contraseña, contacte al administrador");
      cerrar();
    }
  });
 }



 function cerrar(){
  var out;

  $.post('./controller/MethodController.php', {opcion:'session',csrf_token:',dbkj',set:true,c:null,k:null}, function(data, textStatus, xhr) {
    console.log(data);
    if (data=="true") {
      window.location='./';  
    }
  });

  //
}

</script>