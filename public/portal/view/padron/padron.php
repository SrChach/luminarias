<style type="text/css">
    .click{ color: blue; }
</style>

<div class="container-fluid pt-5">
	<div class="row">
		<div class="col-md-3" id="search" style="margin: 0px;padding: 0px">
			<?php include_once './view/general/combos.php'; ?>
		</div>
		<div class="col-md-9 table-responsive" id="tablePadron" style="margin: 0px;padding: 0px">
		</div>
	</div>
</div>

<script type="text/javascript">
    var namePrograma=null;
	$(function () {
		Generales();
	});

    function Generales(){
        var url="./controller/GeneralesController.php"
        objet={opcion:'generales',action:'getNamePrograma',csrf_token:'<?php echo $csrf_token ?>',idPrograma:<?php echo $_GET['index'] ?>};
        ajaxCallback(objet,url,function (respuesta){
            res = JSON.parse(respuesta);
            if (res.CODIGO==true) {
                namePrograma=res.DATOS[0]['NOMBREPROGRAMA'];
                $('#titlePage-2').html(' '+namePrograma);
                showAlertInwindow('#tablePadron','info','SELECCIONE UN FILTRO');
            }else{
                showAlertInwindow('#tablePadron','warning',res.DATOS);
                $('#titlePage').html('PADRON: NO EXISTE');
            }
        });
    }

    function getPadron() {
        $('#btnSearch').prop('disabled', true);
        load();
        //$("#modalLoad").modal("show");
        $('#tablePadron').html('BUSCANDO<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
        var url="./controller/PadronController.php"
        objet={opcion:'Padron',action:'getPadron',csrf_token:'<?php echo $csrf_token ?>',idPrograma:<?php echo $_GET['index'] ?>,cveedo:cveedo,cvemun:cvemun,cveloc:cveloc,cvedel:cvedel,cvesubdel:cvesubdel,foliocuis:folioCuis,nombre:nombre,inicio:'',fin:''};
        ajaxCallback(objet,url,function (respuesta){
            res = JSON.parse(respuesta);
            //console.log(res);
            if (res.CODIGO==true) {
                RenderPadron(res.DATOS,separa(res.COUNT),<?php echo $_GET['index']  ?>);
                //$('#countRegister').html(res.COUNT);
                //$('#tbPadron_length').append("<strong>"+separa(res.COUNT)+"</strong>");
                $('.click').tooltip({trigger: "click"});
            }else{
                showAlertInwindow('#tablePadron','warning',res.DATOS);
            }
            $('#btnSearch').prop('disabled', false);
            load(false);
            //$("#modalLoad").modal("hide");
        });
    }
    
    function RenderPadron(datas,count,idPrograma) {
        $('#tablePadron').html('');
        var cadena="<table class='table table-bordered table-condensed table-striped  table-hover'  id='tbPadron'>\
                      <thead class='thead-light' style='font-size:12px;'>\
                        <tr>\
                   ";
        var heads=['#', 'FOLIOCUIS', 'BENEFICIARIO','NOMBREPROGRAMA', 'TIPOEVENTO','OFICINA REGIONAL','OFICINA SUBREGIONAL','ESTADO','MUNICIPIO','LOCALIDAD',''];
        $.each(heads, function(i, item) {
              cadena+="<th>"+item+"</th>";
          });
            cadena+=" </tr>\
                      </thead>\
                    <tbody>";
            var option='';
            $.each(datas,function (i,item) {
                //if (item.IDPROGRAMA==1) {
                    option=hrefCedula(item.IDLISTADO)+","+idPrograma;
                /*}else{
                    option="?view=modules&act=consulta_cuis&index=1&folio="+item.FOLIOCUIS;
                }*/
               cadena+="<tr style='font-size:11px;'>\
                            <th>"+(i+1)+"</th>\
                            <td><a href='#!'><span class='click' data-toggle='tooltip' data-html='true' data-placement='right' title='<a href=\""+hrefCuis(item.FOLIOCUIS)+"\" target=\"_blank\">CONSULTA FOLIOCUIS</a> <br> <a href=\""+hrefExpediente(item.FOLIOCUIS)+"\" target=\"_blank\">CONSULTA EXPEDIENTE</a>' >"+item.FOLIOCUIS+"</span></a></td>\
                            <!--td>"+item.NOMBRE+"</td>\
                            <td>"+item.APATERNO+"</td>\
                            <td>"+item.AMATERNO+"</td-->\
                            <td>"+item.BENEFICIARIO+"</td>\
                            <td>"+item.NOMBREPROGRAMA+"</td>\
                            <td>"+item.TIPOEVENTO+"</td>\
                            <td>"+item.DELEGACION+"</td>\
                            <td>"+item.SUBDELEGACION+"</td>\
                            <td>"+item.NOMBREENT+"</td>\
                            <td>"+item.NOMBREMUN+"</td>\
                            <td>"+item.NOMBRELOC+"</td>\
                            <td>\
                                <a href='"+option+"' target='_blank'>\
                                    <i class='fa fa-share' style='font-size:25px;'></i>\
                                </a>\
                            </td>\
                        </tr>";
            });

            cadena+="</tbody>\
            </table>";
        $('#tablePadron').html(cadena);
        tableExport2('#tbPadron','Padron_Programa('+namePrograma+')','',count);
        settingsTable();
        //$('[data-toggle="tooltip"]').tooltip();
    }
    $('html').on('click', function(e) {
        if (typeof $(e.target).data('original-title') == 'undefined') {
            $('.click').tooltip('hide');
        }
    });
</script>