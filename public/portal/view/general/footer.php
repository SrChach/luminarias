<style type="text/css">
	.back-footer{
		background-color: #393D3E;
		height: 100%;
		color:#fff;
	}
</style>
<footer class="main-footer back-footer" >
    <strong>Copyright &copy;</strong>
    Todos los derechos reservados.
    <div class="float-right d-none d-sm-inline-block">
      <b>Versión</b> <?php echo $version; ?> Fecha:<?php echo $fecha; ?>.
    </div>
</footer>
