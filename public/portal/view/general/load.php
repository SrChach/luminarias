
<div class="modal spacing-14" id="modalCargaPagina" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog">
		<div class="col-6 offset-3 text-center" id="cargando">
			<!--img src='./assets/img/load1.gif' class="container-fluid"-->
			<label style="font-size: 200%; color:white"> Espere Porfavor... </label>
			<i class="fa fa-spinner fa-spin" style="font-size:500%;color:white"></i>
		</div>
	</div>
</div>
