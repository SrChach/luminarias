<div class="container-fluid">
	<div class="row">
		<form id="formPadronSearch" action="javascript:Search(1);">
			<div class="col-md-12 text-center">
				<div id="load">
					CARGANDO <i class="fa fa-spinner fa-spin" style="font-size:30px"></i>
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" style="font-size: 12px;" for="delegacion">Oficina Regional</label>
					</div>
					<select class="custom-select" id="delegacion">
					</select>
				</div>
			</div>
			<div class="col-md-12">
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" style="font-size: 12px;" for="subdelegacion"><i class="" id="loadSubdel"></i>Oficina Subregional</label>
					</div>
					<select class="custom-select" id="subdelegacion">

					</select>
				</div>
			</div>
			<hr class='my-4'>
			<div class="col-md-12">
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" style="font-size: 12px;" for="estados">Estado</label>
					</div>
					<select class="custom-select" id="estados">
					</select>
				</div>
			</div>
			<div class="col-md-12">
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" style="font-size: 12px;" for="municipios"><i class="" id="loadMunicipios"></i> Municipio</label>
					</div>
					<select class="custom-select" id="municipios">
						<option value="" selected>SELECCIONE ESTADO</option>
					</select>
				</div>
			</div>
			<div class="col-md-12">
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" style="font-size: 12px;" for="localidades"><i class="" id="loadLoc"></i>Localidad</label>
					</div>
					<select class="custom-select" id="localidades">
						<option value="" selected>SELECCIONE MUNICIPIO</option>
					</select>
				</div>
			</div>
			<hr class='my-4'>
			<div class="col-md-12">
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" style="font-size: 12px;" for="folioCuis">Folio Cuis: </label>
					</div>
					<input type="number" pattern="[0-9]" class="form-control" id="folioCuis" placeholder="FOLIO"  aria-describedby="Folio" onkeypress="return onlyNumber(event,'#folioCuis')" onchange="$('#folioCuis').removeClass('is-invalid')">
					<div class="invalid-feedback" style="display: none;" id="FNumbers">
						Campo Númerico.
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" style="font-size: 12px;" for="nombre">Nombre: </label>
					</div>
					<input type="text" class="form-control upperCase" id="nombre" placeholder="Nombre"  aria-describedby="Nombre" onkeypress="return onlyLetter(event,'#nombre')" onchange="$('#nombre').removeClass('is-invalid')">
					<div class="invalid-feedback" style="display: none;" id="FLetters">
						Sólo Letras.
					</div>
				</div>
			</div>
			<hr class='my-4'>

			<div class="row">
				<div class="col-md-6 text-center">
					<input type="submit" value="Buscar" id="btnSearch" class="btn btn-outline-primary">
				</div>
				<div class="col-md-6 text-center" >
					<input type="button" value="Descargar" id="btnDownload" class="btn btn-outline-primary" onclick="reporteDetalle();">
				</div> 
				
			</div>


    <!--div class="col-md-12">
      <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" style="font-size: 12px;" for="localidad">Inicio</label>
          </div>
          <select class="custom-select" id="inicio">
            <option selected>Selecciona Opción...</option>
            <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
            <option value="4">Abril</option>
            <option value="5">Mayo</option>
            <option value="6">Junio</option>
            <option value="7">Julio</option>
            <option value="8">Agosto</option>
            <option value="9">Septiembre</option>
          </select>
      </div>
    </div>


    <div class="col-md-12">
      <div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" style="font-size: 12px;" for="localidad">Fin</label>
          </div>
          <select class="custom-select" id="fin">
            <option selected>Selecciona Opción...</option>
            <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
            <option value="4">Abril</option>
            <option value="5">Mayo</option>
            <option value="6">Junio</option>
            <option value="7">Julio</option>
            <option value="8">Agosto</option>
            <option value="9">Septiembre</option>

          </select>
      </div>
  </div-->



</form>
</div>
</div>

<script type="text/javascript">
	var cveedo='',cvemun='',cveloc='',cvedel='',cvesubdel='',folioCuis='',nombre='';
	var resEML;
	$(function () {
		var url="./controller/GeneralesController.php";
		objet={opcion:'generales',action:'comboMain',idPrograma:'<?php echo $_GET['index'] ?>',csrf_token:'<?php echo $csrf_token ?>'};
		ajaxCallback(objet,url,function (respuesta){
			resEML = $.parseJSON(respuesta);
      //console.log(resEML);
      var Estados=buildOptions(resEML.DATOS['estados'],'NOMBREENT','CVEEDO');
      $('#estados').append(Estados);
      var Delegaciones=buildOptions(resEML.DATOS['delegaciones'],'DELEGACION','ID_DEL');
      $('#delegacion').append(Delegaciones);
      $('#load').hide();
  });
	});


	$( "#estados" ).change(function() {
		cveedo=$(this).val();
		cvedel='';
		cvesubdel='';
		cvemun='';
		cveloc='';
		folioCuis='';
		nombre='';

		if (cveedo=='') {
			$('#municipios').html('');
			$('#localidades').html('');
			$('#delegacion').removeAttr('disabled');
			$('#subdelegacion').removeAttr('disabled');
		}else{
      //var Municipios=buildOptions(resEML.DATOS['municipios'][cveedo],'NOMBREMUN','CVEMUN');
      getSubItems('estado',$(this).val());
      //$('#municipios').html(Municipios);
      $('#delegacion').attr('disabled','true');
      $('#subdelegacion').attr('disabled','true');
      //getPadron();
  }
});




	$( "#municipios" ).change(function() {
		cvemun=$(this).val();
		cvedel='';
		cvesubdel='';
		cveloc='';
		folioCuis='';
		nombre='';
		if (cvemun=='') {
			$('#localidades').html('<option value="" selected>SELECCIONE MUNICIPIO</option>');
		}else{
      //var Localidades=buildOptions(resEML.DATOS['localidades'][cvemun],'NOMBRELOC','CVELOC');
      getSubItems('municipio',$(this).val());
      //$('#localidades').html(Localidades);
      //getPadron();
  }
});

	$( "#localidades" ).change(function() {
		cveloc=$(this).val();
		cvedel='';
		cvesubdel='';
		folioCuis='';
		nombre='';
		if (cveloc=='') {
      //console.log('llamar función de municipios');
  }else{
      //console.log('llamar padron');
      //getPadron();
  }
});

	$( "#delegacion" ).change(function() {
		cvedel=$(this).val();
		cveedo='';
		cvemun='';
		cveloc='';
		cvesubdel='';
		folioCuis='';
		nombre='';
		if (cvedel=='') {
			$('#subdelegacion').html('');
			$("#estados").removeAttr('disabled');
			$('#municipios').removeAttr('disabled');
			$('#localidades').removeAttr('disabled');
		}else{
      //var Subdelegaciones=buildOptions(resEML.DATOS['subdelegaciones'][cvedel],'SUBDELEGACION','ID_SUB');
      getSubItems('delegacion',$(this).val());
      $("#estados").attr('disabled','true');
      $('#municipios').attr('disabled','true');
      $('#localidades').attr('disabled','true');
  }
});

	$( "#subdelegacion" ).change(function() {
		cvesubdel=$(this).val();
		cveedo='';
		cvemun='';
		cveloc='';
		folioCuis='';
		nombre='';
		if (cvesubdel=='') {
			console.log('llamar función de delegacion');
		}else{
			console.log('llamar padron');
		}
	});

  /*$('#btnSearch').on('click',function () {
    folioCuis=$('#folioCuis').val();
    nombre=$('#nombre').val();
    if (folioCuis!='' && nombre!='') {
        notificar('ERROR! ','Solamente debe ingresar un dato (FOLIOCUIS ó NOMBRE)','error');
    }else{
      $('#folioCuis').val('');
      $('#nombre').val('');
      getPadron();
    }
});*/

function Search(item) {
	if (item==1) {
		folioCuis=$('#folioCuis').val();
		nombre=$('#nombre').val();
		if (folioCuis!='' && nombre!='') {
			notificar('ERROR! ','Solamente debe ingresar un dato (FOLIOCUIS ó NOMBRE)','error');
		}else{
			$('#folioCuis').val('');
			$('#nombre').val('');
			$('#folioCuis').removeClass('is-invalid');
			getPadron();
		}
	}
}

function getSubItems(org,key) {
	var url="./controller/GeneralesController.php";
	var sub_items='';
	if (org=='delegacion') {
		$('#loadSubdel').addClass('fa fa-spinner fa-spin');
	}
	if (org=='estado') {
		$('#loadMunicipios').addClass('fa fa-spinner fa-spin');
	}
	if (org=='municipio') {
		$('#loadLoc').addClass('fa fa-spinner fa-spin');
	}
	objet={opcion:'generales',action:'comboMainSubitem',idPrograma:'<?php echo $_GET['index'] ?>',item:org,key:key,csrf_token:'<?php echo $csrf_token ?>'};
	ajaxCallback(objet,url,function (respuesta){
		try{
			subItems = JSON.parse(respuesta);
        //console.log(subItems);
        if (subItems.CODIGO==true) {
        	if (org=='delegacion') {
        		$('#subdelegacion').html(buildOptions(subItems.DATOS,'SUBDELEGACION','ID_SUB'));
        		$('#loadSubdel').removeClass('fa fa-spinner fa-spin');
        	}
        	if (org=='estado') {
        		$('#municipios').html(buildOptions(subItems.DATOS,'NOMBREMUN','CVEMUN'));
        		$('#loadMunicipios').removeClass('fa fa-spinner fa-spin');
        	}
        	if (org=='municipio') {
        		$('#localidades').html(buildOptions(subItems.DATOS,'NOMBRELOC','CVELOC'));
        		$('#loadLoc').removeClass('fa fa-spinner fa-spin'); 
        	}
        }else{
          //console.log(subItems.DATOS);
      }
  }catch(err) {
              //load(false);
              console.log(err);
          }
      });
}


function reporteDetalle(){
	alert("Cuando el reporte termine de generarse se enviará una notificación al correo con el que inicio sesión");
	var index=getParameterByName('index');
	var tipo=getParameterByName('view');
	//var urlReport="../../pillar/clases/reportPadron.php";
	var urlReport="./controller/reportPadron.php";
	var values={index:index,tipo:tipo,cveedo:cveedo, cvemun:cvemun, cveloc:cveloc,cvedel:cvedel,cvesubdel:cvesubdel,foliocuis:folioCuis,nombre:nombre,idusuario:idusuario}
	objet = {opcion: 'generaReportePadron', values:values}
	ajaxCallback(objet,urlReport,function (respuesta){
		resReport = JSON.parse(respuesta); 
		console.log(resReport);
	});   


}
</script>

