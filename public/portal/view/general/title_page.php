<?php
if ($_GET['view']=='inicio') {
   $titlePage="Panel de Resumen";
}elseif ($_GET['view']=='users') {
   $titlePage="USUARIOS";
   if (isset($_GET['act'])) {
      if ($_GET['act']=='add') {
         $titlePage="AGREGAR USUARIOS";
      }
      if ($_GET['act']=='edit') {
         $titlePage="EDITAR USUARIO";
      }
      if ($_GET['act']=='verificar') {
        $titlePage="VERIFICAR USUARIO";
     }
   }
}elseif ($_GET['view']=='programas') {
    $titlePage="CONSULTA DE AVANCE: ";
    if (isset($_GET['act'])) {
       if ($_GET['act']=='add') {
          $titlePage="AGREGAR PROGRAMA";
       }
       if ($_GET['act']=='files') {
          $titlePage="DOCUMENTACIÓN";
       }
    }
}
elseif ($_GET['view']=='padron') {
    $titlePage="PADRON: ";
}
elseif ($_GET['view']=='modules') {
    $titlePage="MÓDULO";
    if (isset($_GET['idP'])) {
      $titlePage='CREDENCIALES';
    }elseif (isset($_GET['act']) and $_GET['act']=='carencias') {
      $titlePage='CARENCIAS';
    }elseif (isset($_GET['act']) and $_GET['act']=='poverty') {
      $titlePage='CARENCIAS GENERAL';
    }elseif (isset($_GET['act']) and $_GET['act']=='PanelEjecutivo') {
      $titlePage='Ejecutivo CUIS';
    }
}
elseif ($_GET['view']=='cedula') {
    $titlePage="CÉDULA";
    if (isset($_GET['act'])) {
      if ($_GET['act']=='visitas') {
        $titlePage='VISITAS';
      }
      if ($_GET['act']=='validacion') {
        $titlePage='VALIDACIÓN CEDULA';
      }
    }
}

elseif ($_GET['view']=='map') {
    $titlePage="MAPA";
}
elseif ($_GET['view']=='detalleluminaria') {
    $titlePage="DESGLOCE";
}
elseif ($_GET['view']=='informeLuminarias') {
    $titlePage="DETALLES DE LAS LUMINARIAS";
}
elseif ($_GET['view']=='tipificacion') {
    $titlePage="TIPIFICACIÓN DE REGISTRO";
}
/* 22/08/2018 */
/* ---------------------*/
elseif ($_GET['view']=='admin') {
    $titlePage="ADMINISTRACIÓN";
}
/* ---------------------*/
elseif ($_GET['view']=='listado') {
  $titlePage="LISTADO MAESTRO";
}
elseif ($_GET['view']=='expedientes') {
  $titlePage="EXPEDIENTES";
}
elseif ($_GET['view']=='validacion') {
  $titlePage="VALIDACION";
}elseif ($_GET['view']=='cuis_completo') {
  $titlePage="CUESTIONARIO COMPLETO";
}elseif ($_GET['view']=='statistics') {
  $titlePage="ESTADÍSTICOS: GENERAL";
}elseif ($_GET['view']=='reporte') {// nuevos titulos
    $titlePage="REPORTE";
    if (isset($_GET['act'])) {
        if ($_GET['act']=="eficiencia_usuario") {
            $titlePage .= " DE EFICIENCIA POR USUARIO";
        }
        if ($_GET['act']=="conciliacion") {
            $titlePage .= " DE CONCILIACIÓN";
        }
        if ($_GET['act']=="teams") {
            $titlePage .= " POR EQUIPOS";
        }
    }
}elseif ($_GET['view']=='censo') {
    $titlePage="CENSO 2019";
}
elseif ($_GET['view']=='csv') {
    $titlePage="";
}elseif ($_GET['view']=='devices') {
    $titlePage="ADMINISTRACIÓN DE DISPOSITIVOS";
}
elseif ($_GET['view']=='sync') {
  $titlePage="SINCRONIZADOR";
}


?>
<style>
.metaUp {
  margin-top:-50px;
  height:70px;
}
@media screen and (max-width: 990px) {
   .metaUp {
      margin-top: 0px;
      height:90px;
   }
}
</style>
<!-- Content Header (Page header) -->
<div class="content-header" style="padding-top: 80px;">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-md-9">
            <h1 class="m-0 titlePage" id="titlePage">
                <?php
                    echo $titlePage;
                ?> <span id="title-complement"></span>
            </h1>
         </div><!-- /.col -->
         <div class="col-md-3">
            <div class="row">
            <?php if ($_GET['view']=='inicio'): ?>
               <!--label class="col-md-3">
                  PROYECTOS DISPONIBLES:
               </label-->
            <?php endif ?>
               <ol class="col-md-12 breadcrumb float-sm-right" id="subItemsTitle">
               </ol>
            </div>

         </div><!-- /.col -->
      </div><!-- /.row -->
   </div><!-- /.container-fluid -->
</div>
