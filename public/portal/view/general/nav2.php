<?php
    if(!isset($_SESSION['_TOKEN'])){
        $c="";
        if (isset($_GET['certificado'])) {
            $c="?c=".$_GET['certificado'];
        }else{
            echo "<script>window.location='./$c'</script>";
        }
   }
?>

<style type="text/css">

.brand-image-custom {
    float: left;
    line-height: .8;
    max-height: 60px;
    width: auto;
    margin-left: .8rem;
    margin-right: .5rem;
    margin-top: -3px;
}
  /** {
    /* To receive click events on iOS
      cursor: pointer;
  }*/
 .fading {
    animation: fadein .7s;
    -moz-animation: fadein .7s; /* Firefox */
    -webkit-animation: fadein .7s; /* Safari and Chrome */
    -o-animation: fadein .7s; /* Opera */
}
@keyframes fadein {
   from {opacity:0; }
   to {opacity:1; }
}
@-moz-keyframes fadein { /* Firefox */
   from {opacity:0; }
   to {opacity:1; }
}
@-webkit-keyframes fadein { /* Safari and Chrome */
   from {opacity:0; }
   to {opacity:1; }
} @-o-keyframes fadein { /* Opera */
   from {opacity:0; }
   to {opacity: 1; }
}

@media screen and (min-width: 990px) {
   .fading {
      visibility: hidden;
   }
}

</style>
<!-- Navbar -->
<nav class="main-header navbar fixed-top navbar-expand navbar-light border-bottom elevation-2" style="background-color:#999999; height:60px;">
    <!-- Left navbar links -->
   <ul class="navbar-nav fading">
      <li class="nav-item">
         <a class="nav-link" data-widget="pushmenu"><i class="fa fa-bars icon-menu"></i></a>
      </li>
   </ul>

   <ul class="navbar-nav ml-auto text-center" style="padding: 0px;">
      <h4><li class="nav-item d-none d-sm-inline-block titleHead upperCase" style="font-size: 25px;"><?php echo strtoupper($system);?></li></h4>
   </ul>

    <!-- Right navbar links -->
   <ul class="navbar-nav ml-auto" style="padding: 0px;">
         <img src="./assets/img/logos-pleca.png" class="brand-image-custom">
   </ul>
</nav>
  <!-- /.navbar -->

<script type="text/javascript">
  $(function () {
    var isMobile = {
      Android: function() {
          return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function() {
          return navigator.userAgent.match(/IEMobile/i);
      },
      any: function() {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }
    };
    if(isMobile.any()) {
      $('html').css('cursor','pointer');
    }
  });

</script>
