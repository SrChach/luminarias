<link rel="stylesheet" type="text/css" href="./assets/css/meta-contents.css">
<div class="container-fluid" style="padding-top: 0px;">
    <div class="row"  id='contents_tableros'>
          <div class="col-md-3 col-sm-6 col-12">
            <!-- small box -->
            <div class="small-box bg-info hover my_Box1">
              <div class="inner">
                <h4>PROYECTOS</h4>
                <p>* DLKN </p>
                <!--p style="font-size: 1.3em">837,347</p-->
              </div>
              <div class="icon">
                <i class="icon ion-arrow-graph-up-right"></i>
              </div>
              <div class="col-6 offset-3 small-box-footer">
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>  
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-md-3 col-sm-6 col-12">
            <!-- small box -->
            <div class="small-box bg-success hover my_Box1">
              <div class="inner">
                <h4>Bounce Rate</h4>
                <p>53%</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <div class="col-6 offset-3 small-box-footer">
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>  
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-md-3 col-sm-6 col-12">
            <!-- small box -->
            <div class="small-box bg-warning hover my_Box1">
              <div class="inner" style="color: white;">
                <h4>Registrations</h4>
                <p>44</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
             <div class="col-6 offset-3 small-box-footer">
                <a href="#" class="small-box-footer"><span style="color: white">More info </span><i class="fa fa-arrow-circle-right" style="color: white;"></i></a>  
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-md-3 col-sm-6 col-12">
            <!-- small box -->
            <div class="small-box bg-danger hover my_Box1">
              <div class="inner">
                <h4>Unique Visitors</h4>
                <p>65</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <div class="col-6 offset-3 small-box-footer">
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>  
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
</div>

<script type="text/javascript" src='./assets/js/contenedores/contents.js'></script>
<script type="text/javascript">
  $(function () {
    objet={opcion:'Contents',action:'getDatas',csrf_token:'<?php echo $csrf_token ?>'};
    ajaxCallback(objet,url,function (respuesta){
            res = JSON.parse(respuesta);
            if (res.CODIGO==true) {
              showDtas(res.DATOS);
            }else{
              showAlertInwindow('#contents_tableros','warning',res.DATOS);
            }
            $("#modalLoad").modal("hide");
    });
  });
</script>