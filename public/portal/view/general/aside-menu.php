 <link rel="stylesheet" type="text/css" href="./assets/css/aside-menu.css">
 <?php
 $activeitem1="";
 $activeitem2="";

 $activeitem3="";

 $activeitem4="";

 $activeitem5="";
 $menuAdmin_open="";
 $subitem5_1="";
 $subitem5_2="";
 $subitem5_3="";
 $subitem5_4="";
 $subitem5_5="";
 $subitem5_6="";

 $menuProgramas_open="";
 $menuModules_open="";

 if ($_GET['view']=='inicio'){
  $activeitem1='active-item elevation-1';
}

if ($_GET['view']=='programas') {
 $activeitem2="active-item elevation-1";
 $menuProgramas_open="menu-open";
}

if ($_GET['view']=='modules') {
 $activeitem3="active-item elevation-1";
 $menuModules_open="menu-open";
}


if ($_GET['view']=='users'||$_GET['view']=='admin') {
  $activeitem5="active-item elevation-1";
  $menuAdmin_open="menu-open";
      /*if (isset($_GET['act']) and $_GET['act']=='add') {
        $subitem5_4="active";
      }*/
      if (isset($_GET['act'])) {
        if ($_GET['act']=='Gestion_TipoImagen') {
          $subitem5_1="active elevation-1";
        }
        if ($_GET['act']=='Agregar_Preguntas') {
          $subitem5_2="active elevation-1";
        }
        if ($_GET['act']=='Admin_PreguntasDoc') {
          $subitem5_3="active elevation-1";
        }
        if ($_GET['act']=='Admin_Eventos') {
          $subitem5_5="active elevation-1";
        }
      }
      if ($_GET['view']=='users') {
        $subitem5_6="active elevation-1";
      }
    }
    if ($_GET['view']=='listado') {
     $activeitem4="active-item elevation-1";
   }
//echo $_SESSION['PERFIL'];
   ?>

   <style media="screen">
   .brand-image-logo {
       float: left;
       line-height: .8;
       max-height: 60px;
       width: auto;
       margin-left: .4rem;
       margin-right: .5rem;
       margin-top: -13px;
    }
   </style>

   <!-- Main Sidebar Container -->
   <aside class="main-sidebar bg-white sidebar-light-primary elevation-2 fondo-menu">
    <!-- Brand Logo -->
    <a data-widget="pushmenu" href="#" class="brand-link text-center d-lg-none" style="padding-left: 0%; height:60px;">
      <img src="./assets/img/logo-interno.png" alt="AdminLTE Logo" class="brand-image-logo img-circle">
      <span class="brand-text font-weight-light">Censo 2019</span>
    </a>
    <div href="#" class="brand-link text-center d-none d-lg-block" style="padding-left: 0%; height:60px;">
      <img src="./assets/img/logo-interno.png" alt="AdminLTE Logo" class="brand-image-logo img-circle">
      <span class="brand-text font-weight-light">Censo 2019</span>
    </div>

    <!-- Sidebar -->
    <div class="sidebar ">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <!--img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"-->
          <i class="nav-icon fa fa-user"></i>
        </div>
        <div class="info">
          <a class="d-block"><?php echo $_SESSION['NAME'] ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!--div id="contentMenu-Side"-->

           <!--ITEM INICIO-->
            <?php if ( !in_array($_SESSION['PERFIL'], [3, 4]) ): ?>
              <?php if ( !in_array($_SESSION['PERFIL'], [13]) ): ?>
                <li class="nav-item auth">
                    <a href="?view=inicio" class="nav-link <?php echo $activeitem1 ?>">
                        <i class="nav-icon fa icon ion-home"></i>
                        <p class="item">
                            INICIO
                        </p>
                    </a>
                </li>
              <?php endif; ?>
              <?php if ( in_array($_SESSION['PERFIL'], [6, 11, 12, 13, 14, 15, 888, 5]) /*&& ($_SESSION['VERIFICADO']==1)*/ ): ?>
                    <li class="nav-item auth">
                      <a href="?view=censo" class="nav-link">
                          <i class="nav-icon fa icon ion-clipboard"></i>
                          <p class="item">
                              Censo
                          </p>
                      </a>
                    </li>
              <?php endif; ?>
            <?php endif; ?>

          <!-- Añadido 11 -->
          <?php if ( in_array($_SESSION['PERFIL'], [3, 4, 5, 6, 8, 10, 11, 888]) ): ?>

            <?php if ( in_array($_SESSION['PERFIL'], [6, 10, 11, 888]) ): ?>


                <li class="nav-item has-treeview  auth">
                    <a href="#!" class="nav-link ">
                        <i class="nav-icon fa fa-bar-chart"></i>
                        <p class="item">
                            Reportes CFE
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <!--SUBMENU ADMINISTRACIÓN-->
                    <ul class="nav nav-treeview contentSubitem table-responsive" style="background-color: rgba(66,70,76,.3);">
                        <li class="nav-item">
                            <a  class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Clasificación Luminarias por Tecnología </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=reporte&act=VIEW_PGR_ANU_CEN_ALU_PUB_SEM" class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Anexo 1 Programa Anual Luminarias y Semáforos </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=reporte&act=VIEW_PGR_ANU_CEN_OTR_CGS_DIR" class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Anexo 2 Prpgrama Anual de Otras Cargas Directas </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=reporte&act=VIEW_DES_CEN_ALUM_PUB_SEM" class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Anexo 3 Desglose del Censo Luminarias y Semáforos </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=reporte&act=VIEW_RES_CEN_ALUM_PUB_SEM" class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Anexo 4 Resumen dek Censo de Luminarias y Semáforos </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=reporte&act=VIEW_REP_DETC_COR_LAMP_ENCD" class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Anexo 5 Reporte de Detección y Corrección de Lamparas Encedidas 24 hras </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=reporte&act=VIEW_RES_CEN_OTR_CAR_DIR" class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Formato 6 Resumen del Censo de otras Cargas directas </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=reporte&act=VIEW_RES_CEN_OTR_CAR_DIRECTS" class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Formato 7 Resumen del Censo de Otras Cargas </p>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>

              <?php if ( in_array($_SESSION['PERFIL'], [3, 4, 5, 8, 10, 888]) ): ?>
                <li class="nav-item has-treeview  auth">
                    <a href="#!" class="nav-link ">
                        <i class="nav-icon fa fa-bar-chart"></i>
                        <p class="item">
                            Reportes TIA
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <!--SUBMENU ADMINISTRACIÓN-->
                    <ul class="nav nav-treeview contentSubitem table-responsive" style="background-color: rgba(66,70,76,.3);">
                        <?php if ($_SESSION['PERFIL']!=3 and $_SESSION['PERFIL']!=4 and $_SESSION['PERFIL']!=5): ?>
                        <li class="nav-item">
                            <a href="?view=reporte&act=eficiencia_usuario"  class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Reporte Eficiencia por Usuario </p>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if ($_SESSION['PERFIL']==3 || $_SESSION['PERFIL']==4 || $_SESSION['PERFIL']==5 || $_SESSION['PERFIL']==888 || $_SESSION['PERFIL']==10): ?>
                            <li class="nav-item">
                                <a href="?view=reporte&act=teams"  class="nav-link" style='padding-left: 10%;'>
                                    <p> <i class="nav-icon fa fa-circle-o"></i> Reporte por Equipo </p>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($_SESSION['PERFIL']!=3 and $_SESSION['PERFIL']!=4 and $_SESSION['PERFIL']!=5): ?>
                        <li class="nav-item">
                            <a  class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Reporte Estimación </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=csv" class="nav-link" style='padding-left: 10%;'>
                                <p> <i class="nav-icon fa fa-circle-o"></i> Generar CSV </p>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
              <?php endif; ?>
          <?php endif ?>

            <?php if ( in_array($_SESSION['PERFIL'], [6, 10, 11, 888]) ): ?>
                <li class="nav-item">
                    <a href="?view=reporte&act=conciliacion" class="nav-link">
                        <i class="nav-icon fa fa-check-circle"></i>
                        <p class="item">
                            Conciliación
                        </p>
                    </a>
                </li>
            <?php endif; ?>

          <?php if ($_SESSION['PERFIL']==888 || $_SESSION['PERFIL']==7 ): ?>
                <li class="nav-item has-treeview  auth">
                  <a href="#!" class="nav-link ">
                    <i class="nav-icon fa fa-check-circle"></i>
                    <p class="item">
                      Tipificación
                      <i class="right fa fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview contentSubitem table-responsive" style="background-color: rgba(66,70,76,.3);">
                      <li class="nav-item">
                        <a href="?view=tipificacion" class="nav-link" style='padding-left: 10%;'>
                          <i class="nav-icon fa fa-circle-o"></i>
                          <p>Luminarias</p>
                        </a>
                      </li>
                      <li class="nav-item has-treeview">
                          <a href="#!" class="nav-link" style='padding-left: 10%;'>
                            <i class="nav-icon fa fa-circle-o"></i>
                            <p class='item'>Cargas directas
                              <i class="right fa fa-angle-left"></i>
                            </p>
                          </a>
                          <ul class="nav nav-treeview contentSubitem table-responsive" style="background-color: rgba(66,70,76,.3);">
                            <li class="nav-item">
                              <a href="?view=tipificacion&act=telmex" class="nav-link" style='padding-left: 15%;'>
                                <i class="nav-icon fa fa-circle-o"></i>
                                <p>Telmex</p>
                              </a>
                            </li>
                          </ul>
                      </li>
                  </ul>
                </li>
          <?php endif; ?>


          <?php if ($_SESSION['PERFIL']==888 || $_SESSION['PERFIL']==10 || $_SESSION['PERFIL']==5 ): ?>
              <li class="nav-item">
                  <a href="?view=users" class="nav-link">
                      <i class="nav-icon fa fa-users"></i>
                      <p class="item">
                          Usuarios
                      </p>
                  </a>
              </li>

              <li class="nav-item">
                  <a href="?view=devices" class="nav-link">
                      <i class="nav-icon fa fa-mobile"></i>
                      <p class="item">
                          Dispositivos
                      </p>
                  </a>
              </li>
              <li class="nav-item">
                  <a href="?view=sync" class="nav-link">
                      <i class="nav-icon fa fa-refresh"></i>
                      <p class="item">
                          Sincronizador
                      </p>
                  </a>
              </li>
          <!-- ITEM ADMINISTRACIÓN-->
          <!--li class="nav-item has-treeview <?php echo $menuAdmin_open ?> auth">
            <a href="#!" class="nav-link <?php echo $activeitem5 ?>">
              <i class="nav-icon fa icon ion-person-stalker"></i>
              <p class="item">
                Administración
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview contentSubitem" style="background-color: rgba(66,70,76,.3);">
              <li role="separator" class="divider"></li>
              <li class="nav-item">
                <a  class="nav-link <?php echo  $subitem5_6 ?>" style='padding-left: 10%;'>
                  <p> <i class="nav-icon fa fa-circle-o"></i> Usuarios <i class='nav-icon fa fa-caret-down'></i></p>
                </a>
                <ul class="nav nav-treeview" style="background-color:rgba(9, 0, 0, 0.2);">
                  <li class="nav-item">
                    <a id="subitem3_2" href="?view=users" class="nav-link" style='padding-left: 15%;'>
                      <p class="hover-item"><i class="left icon ion-eye nav-icon"></i> Ver Usuarios</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a id="subitem3_2" href="?view=users&act=add" class="nav-link" style='padding-left: 15%;'>
                      <p class="hover-item"><i class="icon ion-person-add nav-icon"></i> Agregar usuario</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
        </li-->
          <?php endif; ?>


          <!--/div-->
          <!--li class="nav-item">
            <a href="?salir=1" class="nav-link">
              <i class="nav-icon fa fa-sign-out"></i>
              <p class="item">
                SOCIAL RED
              </p>
            </a>
          </li-->
          <!-- ITEM CERRAR SESIÓN-->
          <li class="nav-item">
            <a href="?salir=1" class="nav-link elevation-1">
              <i class="nav-icon fa fa-sign-out"></i>
              <p class="item">
                Cerrar Sesión
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


  <script type="text/javascript">
    $(function () {
        ActiveSubItem();
    });

    function ActiveSubItem() {
        var item='<?php echo $_GET["view"] ?>';
        var act='<?php echo (isset($_GET['act'])) ? $_GET['act'] : '' ?>';
        var index='<?php echo isset($_GET["index"])? $_GET["index"]: "" ?>';

        if (item=='programas') {
            if (act != '' && act != null) {
                index="n";
            }
            $('#subitem2_'+index).addClass('active').addClass('elevation-1');
        }
        if (item=='modules') {
            $('#subitem3_'+index).addClass('active').addClass('elevation-1');
        }
    }

</script>
