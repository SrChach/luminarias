<style media="screen">
.sectionsMargTop-70{
    /*margin-top: 15px;*/
}
.headerSectionTitle{
    text-align: center;
    background-color: #A1C4D8;
    margin-top: 10px;
}
.contentPadding-15{
    padding-top: 0px;
}
.labelSpace-15{
    margin-top: 15px;
}
.labelSpaceBottom{
    margin-bottom: 25px;
}
.frameMapSize{
    max-height: 200px;
    max-width: 350px;
}
.frameMapSize > iframe {
    max-height: -webkit-fill-available;
    max-width: -webkit-fill-available;
}


.inputStyle{
    /* width: 75%; */
    border-radius: 6px;
    padding: 5px;
    min-width: 200px;
    vertical-align: middle;
}

.selectStyle{
    width: -webkit-fill-available !important;
    max-width: 300px;
}
.containerImg{
    max-width: 300px;
    max-height: 350px;
    margin: auto;
}
.containerBtnImg{
    width: inherit;
    padding: 15px 35px;
    max-width: 300px;
    margin: auto;
}
.buttomFloatLeft{
    float: left;
}
.buttomFloatRigth{
    float: right;
}
.buttonRotateStyle{
    background-color: #ffffff00;
    border-radius: 5px;
    width: 35px;
}
.buttonRotateStyle:hover{
    border-color: #2bb273;
    background-color: #2bb27359;
}
.spaceBottom-25{
    margin-bottom: 25px !important;
}

.containerTypeImg{
    width: inherit;
    text-align: center;
    margin: auto;
}

.containerStepInfo{
    display: inline-block;
    vertical-align: middle;
    max-width: 300px;
}
.stepCircle{
    background-color: #A1C4D8;
    width: 30px;
    height: 30px;
    display: table-cell;
    vertical-align: middle;
    text-align: center;
}
.stepInfo{
    display: inline-block;
    vertical-align: sub;
}


.displayBlock{
    display: block;
}
.minWidth-150{
    min-width: 150px;
}
.minWidth-210{
    min-width: 210px;
}
.padding-10{
    padding: 10px;
}
.marginAuto{
    margin: auto;
}
.marginBottom-50{
    margin-bottom: 50px;
}

/*#titlePage{
    font-family: 'NeoSansPro-Regular';
    color: #2BB273 !important;
    font-size: 39px;
}*/

.subTitles , .headerSectionTitle{
    color: #7F8A8E;
    font-size: 30px;
    /*text-align: right;*/
}

.img-thumbnail{
    max-width: 250px;
    max-height: 300px;
    width: 100%;
    height: 250px;
}



/*_________________ Dimensiones del mapa OpenStreetMap->Open layers*/
.map{
    height: -webkit-fill-available;
    width: -webkit-fill-available;
}
#marker {
    width: 20px;
    height: 20px;
    border: 1px solid #088;
    border-radius: 10px;
    background-color: #f00;
    opacity: 0.7;
}

.parrafoText{
    border: 1px solid;
    padding: 5px;
    border-radius: .3rem;
    min-height: 35px;
}
</style>

<link rel="stylesheet" href="./vendor/thisPlugins/map/leaflet.css">

<div class="container-fluid">
    <section class='sectionsMargTop-70'>
        <h4 class="col-md-12 headerSectionTitle">FOLIO LEVANTAMIENTO <span id='folio'></span></h4>
        <div class="row contentPadding-15">
            <div class="col-md-4">
                <label for="state" class='form-text'>TIPO:</label>
                <label class='form-text labelSpaceBottom' id='tipo'></label>

                <label for="municipality" class='form-text title-for-type'>TIPO LUMINARIA</label>
                <label class='form-text labelSpaceBottom' id='tipoluminaria'></label>

                <label for="locality" class='form-text'>IMEI Levantamiento</label>
                <label class='form-text labelSpaceBottom' id='imei'></label>

                <label for="state" class='form-text'>Folio Levantamiento</label>
                <label class='form-text labelSpaceBottom' id='folioL'></label>

                <label for="state" class='form-text'>Manzana:</label>
                <label class='form-text labelSpaceBottom' id='manzana'></label>

            </div>
            <div class="col-md-4">

                <label for="state" class='form-text'>Fecha de levantamiento</label>
                <label class='form-text labelSpaceBottom' id='fechaLevanta'></label>

                <div class="content_complete_cg" style="display:none">
                    <label>EMPRESA:</label>
                    <label id="txt_empresa" class='form-text labelSpaceBottom' ></label>
                </div>
                <div class="content_complete_luminaria">
                    <label>UBICACIÓN FÍSICA:</label>
                    <label id="txt_uFisica" class='form-text labelSpaceBottom' ></label>

                    <!--label>MUESTRA FÍSICA:</label>
                    <label id="muestraFisica" class='form-text labelSpaceBottom' ></label-->

                    <label>CARACTERÍSTICA TÉCNICA (TIPO):</label>
                    <label id="cara_tecnica" class='form-text labelSpaceBottom' ></label>

                    <img src="./controller/qrFile.php?key=<?php echo $_GET['item'] ?>" alt="" id="qrLuminaria">
                </div>




            </div>

            <div class="col-md-4">
                <a href="" target="_blank">
                    <div id="map" style="height:200px">
        				Cargando <i class="fa fa-spinner fa-spin" style="font-size:40px"></i>
        			</div>
                </a>
                <label for="" class='labelSpace-15'>Coordenadas:
                    <a href="" id="href-coordenates" target="_blank"><label for="" id='coordCalle'></label></a>
                </label>
            </div>
        </div>
        <hr>
        <div class="row elevation-1 p-3">
            <!--div class="col-md-5">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Cuestionario</h4>
                    </div>
                    <div class="col-md-12">
                        <label>UBICACIÓN FÍSICA:<small id="txt_uFisica"></small></label>
                        <br>
                        <label>MUESTRA FÍSICA:<small id="muestraFisica"></small></label>
                        <br>
                        <label>TIPO INTENSIDAD:<small id="tipointensidad"></small></label>
                        <br>
                        <label>VALOR DE INTENSIDAD:<small id="extratipointensidad"></small></label>
                        <br>
                        <label>TIPO LAMPARA INCANDESCENTE:<small id="incandescentes"></small></label>
                        <br>
                        <label>TIPO LAMPARA FLUORESCENTE:<small id="fluorescentes"></small></label>
                        <br>
                        <label>TIPO LAMPARA ALTA INTESNIDAD:<small id="alta_intensidad"></small></label>
                    </div>
                </div>
            </div-->
            <div class="col-md-12 text-center">
                <h3>EVIDENCIAS</h3>
            </div>
            <div class="col-md-12">
                <div class="row" id="content_eviencias">
                    <!--div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>IMÁGEN 1</h4>
                            </div>
                            <div class="col-md-2">
                                <button type="button" name="button" class="btn btn-info float-left" onclick="rotateLeft('content_imgEvidencia1');"><i class="fa fa-rotate-right"></i></button>
                            </div>
                            <div class="col-md-12 text-center" align="center">
                                <a href="" target="_blank" id="hrefImg1">
                                    <img src="https://media3.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" style="height:600px; width:600px;" alt="IMAGEN NO ENCONTRADA" id="content_imgEvidencia1">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>IMÁGEN 2</h4>
                            </div>
                            <div class="col-md-2">
                                <button type="button" name="button" class="btn btn-info float-left" onclick="rotateLeft('content_imgEvidencia2');"><i class="fa fa-rotate-right"></i></button>
                            </div>
                            <div class="col-md-12 text-center" align="center">
                                <a href="" target="_blank" id="hrefImg2">
                                    <img src="https://media3.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" style="height:600px; width:600px;" id="" alt="IMAGEN NO ENCONTRADA" id="content_imgEvidencia2">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>IMÁGEN 3</h4>
                            </div>
                            <div class="col-md-2">
                                <button type="button" name="button" class="btn btn-info float-left" onclick="rotateLeft('content_imgEvidencia3');"><i class="fa fa-rotate-right"></i></button>
                            </div>
                            <div class="col-md-12 text-center" align="center">
                                <a href="" target="_blank" id="hrefImg3">
                                    <img src="https://media3.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" style="height:600px; width:600px;" alt="IMAGEN NO ENCONTRADA" id="content_imgEvidencia3">
                                </a>
                            </div>
                        </div>
                    </div-->
                </div>
            </div>

        </div>
    </section>
</div>
<script src="./vendor/thisPlugins/map/leaflet.js"></script>
<script type="text/javascript">
    var urlImages = "../../../storage/images/";
    $(()=>{

        $.get('./controller/main_controller.php?key=<?php echo $_GET['item'] ?>', function(data, textStatus, xhr) {
            let respuesta = JSON.parse(data);
            let response = respuesta.generales;
            let images = respuesta.images;
            let type = respuesta.type;
            let icon="redL";

            $('.content_complete_luminaria').hide();
            $('.content_complete_cg').hide();


            if (type==1) {
                $('.content_complete_luminaria').show();
            }

            if (type==2) {
                $('.title-for-type').html('TIPO SEMAFORO');
                icon="redS";
            }
            if (type==3) {
                icon="redC";
                $('.title-for-type').html('TIPO CARGA DIRECTA');
                $('.content_complete_cg').show();
            }


            console.log(response);
            $("#folio").html(response.FOLIO);
            $("#tipo").html(response.TIPO);
            $("#tipoluminaria").html(response.TIPOLUMINARIA);
            $("#imei").html(response.IMEILEVANTAMIENTO);
            $("#folioL").html(response.FOLIO);
            $("#manzana").html(response.CVEMANZANA);

            $("#fechaLevanta").html(response.FECHALEVANTAMIENTO);
            //$('#urlMap').attr('href', '?view=map&params={%22location%22:%22'+latitud+','+longitud+'%22}');
            //https://maps.google.com/?q='+response.LATITUD+','+response.LONGITUD+'
            /*
            <img src="./view/cedula/staticmaplite/staticmap.php?center='+datas.LATITUD+','+datas.LONGITUD+'&zoom=16&size=150x150&maptype=mapnik&markers='+datas.LATITUD+','+datas.LONGITUD+',ol-marker|'+datas.LATITUD+','+datas.LONGITUD+',bullseye" class="img-fluid img-rounded" style="height: 150px;width: 100%;" >'

            <img src="http://geomap.nagvis.org/?module=map&lon='+response.LONGITUD+'&lat='+response.LATITUD+'&zoom=17&width=350&height=200&points='+response.LONGITUD+','+response.LATITUD+'&pointImagePattern='+icon+'" alt="" class="map img-fluid" id="mapimg">
            */
            /*$('#map').html('<a href="?view=map&params={%22location%22:%22'+response.LATITUD+','+response.LONGITUD+'%22}" target="_blank"><img src="./view/cedula/staticmaplite/staticmap.php?center='+response.LATITUD+','+response.LONGITUD+'&zoom=18&size=350x200&maptype=mapnik&markers='+response.LATITUD+','+response.LONGITUD+',ol-marker" class="img-fluid img-rounded" style="height: 200px;width: 100%;" ></a>');*/

            var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                osm = L.tileLayer(osmUrl, {maxZoom: 22});
            var map = L.map('map',{ zoomControl: false }).setView([response.LATITUD, response.LONGITUD], 18).addLayer(osm);

            //L.controls.remove();
            //L.control.scale().addTo(map);
            var marker= L.marker([response.LATITUD, response.LONGITUD])
                    .addTo(map);
            map.touchZoom.disable();
            map.doubleClickZoom.disable();
            map.scrollWheelZoom.disable();
            /*map.on('click', function(map) {
                console.log(map);
            });*/

            $('#map').parents('a').attr('href', `?view=map&params={%22location%22:%22${response.LATITUD},${response.LONGITUD}%22}`);

            $('#coordCalle').html(`(${response.LATITUD}, ${response.LONGITUD})`);

            $('#href-coordenates').attr('href', `https://maps.google.com/?q=${response.LATITUD},${response.LONGITUD}`);

            //$('#content_imgEvidencia1').attr('src', urlImages+response.URLIMAGEN);
            //$('#hrefImg').attr('href', urlImages+response.URLIMAGEN);

            $('#txt_uFisica').html(' '+response.UBICACIONFISICA);
            $('#muestraFisica').html(' '+response.EXTRAUBICACIONFISICA);
            $('#muestraFisica').html(' '+response.EXTRAUBICACIONFISICA);
            $('#cara_tecnica').html(' '+ response.CARACTERISTICATECNICA);

            $('#txt_empresa').html(' '+(response.NOMBREEMPRESA!=null)?response.NOMBREEMPRESA:'Sin dato');

            if (images.length==0) {
                $('#content_eviencias').html('<div class="alert alert-info offset-md-4 col-md-4 text-center">NO HAY EVIDENCIAS</div>');
            }else{
                let bodyImages = "";
                $.each(images,function(index, image) {
                    console.log(image.URLIMAGEN);
                    bodyImages += `
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>IMAGEN ${index+1}</h4>
                            </div>
                            <div class="col-md-2">
                                <button type="button" name="button" class="btn btn-info float-left" onclick="rotateLeft('content_imgEvidencia${index+1}');"><i class="fa fa-rotate-right"></i></button>
                            </div>
                            <div class="col-md-12 text-center" align="center">
                                <a href="${urlImages+image.URLIMAGEN}" target="_blank" id="hrefImg${index+1}">
                                    <img src="${urlImages+image.URLIMAGEN}" class="p-4" style="height:400px; width:400px;" alt="IMAGEN NO ENCONTRADA" id="content_imgEvidencia${index+1}">
                                </a>
                            </div>
                        </div>
                    </div>`;
                });

                bodyImages += '<div class="col-md-12">\
                                <span>Comentarios: </span>\
                                <p class="parrafoText">'+response.COMENTARIOVALIDACIONLEVANTAMIENTO+'</p>\
                                </div>\
                                <div class="col-md-12">\
                                    <span>Observaciones: </span>\
                                    <p class="parrafoText">'+response.OBSERVACIONLEVANTAMIENTO+'</p>\
                                </div>';
                $('#content_eviencias').html(bodyImages);

            }
            /*$('#tipointensidad').html(' '+response.TIPOINTENSIDAD);
            $('#extratipointensidad').html(' '+response.EXTRATIPOINTENSIDAD);
            $('#incandescentes').html(' '+response.INCANDESCENTES);
            $('#fluorescentes').html(' '+response.FLUORESCENTES);
            $('#alta_intensidad').html(' '+response.ALTAINTENSIDAD);*/

        });



    });
    var rotateLeftValue=90;
    var contenedorOrg=null;
    rotateLeft = (contenedor) =>{
        //console.log(rotateLeftValue);
        if (contenedorOrg!==contenedor) {
            otateLeftValue=90;
        }
        $('#'+contenedor).css('transform', 'rotate('+rotateLeftValue+'deg)');
        rotateLeftValue = rotateLeftValue + 90;
        if (rotateLeftValue>360) {
            rotateLeftValue=90;
        }
        contenedorOrg = contenedor;

    }
</script>
