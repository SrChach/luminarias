<pre>
<?php
    include_once('../../pillar/clases/controlador.php');
    include_once('../../pillar/clases/conexion_mysql.php');
    // include_once('../../pillar/clases/model/informeLuminarias_model.php');
    include_once('../../pillar/clases/model/generales_model.php');
    include_once('../../pillar/clases/model/utilities.php');
    include_once('./controller/InformeLuminariasController.php');

    $informeClass = new InformeLuminariasController;

    $resultImagenes = $informeClass->getLuminarias($_GET['key']);

    //Print the array in a simple JSON format
    // echo json_encode($resultImagenes['data'], JSON_PRETTY_PRINT);
    // echo json_encode($resultImagenes['data'][0]['generales'], JSON_PRETTY_PRINT);
    // echo json_encode($resultImagenes['catalogos'], JSON_PRETTY_PRINT);

?>
</pre>

<div class="contaner-fluid">
    <!-- <h5>Hola mundo 2  ../../../storage/images/31918320190918185816_foto_luminaria_1.jpg</h5> -->

    <?php   foreach ($resultImagenes['data'][0]['images']['EVIDENCIA'] as $key => $imagen) { ?>
            <div class="row row_content">
                <div class="col-md-7">
                    <form class="card" id="data" style="height:31rem;">
                        <div class="card-header bg-dark">
                            <h5 class="float-left">TIPO: <small id="tipo"><?php echo $resultImagenes['data'][0]['generales']['TIPO'] ?></small> </h5>
                        </div>
                        <div class="card-body" id="content_data">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Tipo Ubicación </div>
                                </div>
                                <div type="text" class="form-control">
                                    <?php
                                        if($resultImagenes['data'][0]['generales']['KEYTIPOLUMINARIA'] == 1){
                                            echo 'Vía Primaria';
                                        }
                                        elseif ($resultImagenes['data'][0]['generales']['KEYTIPOLUMINARIA'] == 2) {
                                            echo 'Vía Secundaria';
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Ubicación </div>
                                </div>
                                <div type="text" class="form-control">
                                    <?php
                                        echo $resultImagenes['data'][0]['generales']['UBICACIONFISICA'];
                                    ?>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Módelo </div>
                                </div>
                                <div type="text" class="form-control">
                                    <?php
                                        echo $imagen['MODELO'];
                                    ?>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Clasificación Luminaria </div>
                                </div>
                                <div type="text" class="form-control">
                                    <?php
                                        echo $imagen['CLASIFICACIONLUMINARIA'];
                                    ?>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Tipo Lampara </div>
                                </div>
                                <div type="text" class="form-control">
                                    <?php
                                        echo $imagen['TIPOLAMPARA'];
                                    ?>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Watts </div>
                                </div>
                                <div type="text" class="form-control">
                                    <?php
                                        echo $imagen['VALOR_WATTS'];
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">

                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="card" style="height:31rem;">
            			<div class="card-header bg-dark">
            				<h5 class="float-left">IMÁGEN</h5>
            			</div>
            			<div class="card-body">
            				<div class="row">
            					<div class="col-md-9 border-right">
            						<div class="row" align="center">
            							<div class="col-md-12">
            								EVIDENCIA <?php echo $key + 1; ?>
            							</div>
            							<div class="col-md-12">
            								<a href="../../../storage/images/<?php echo $imagen['URLIMAGEN'] ?>" target="_blank">
            									<img style="height:300px;width:300px;" src="../../../storage/images/<?php echo $imagen['URLIMAGEN']; ?>" alt="First slide">
            								</a>
            								<div class="col-md-12">
            									<p><span>Comentarios: </span>"<?php echo $resultImagenes['data'][0]['generales']['COMENTARIOVALIDACIONLEVANTAMIENTO']; ?>"</p>
            								</div>
            								<div class="col-md-12">
            									<p><span>Observaciones: </span>"<?php echo $resultImagenes['data'][0]['generales']['OBSERVACIONLEVANTAMIENTO']; ?>"</p>
            								</div>
            							</div>
            						</div>
            					</div>
            					<div class="col-md-3">
            					<h4>Referencias</h4>
            					        <?php   foreach ($resultImagenes['data'][0]['images']['REFERENCIA'] as $keyRef => $imagenRef) { ?>
                                            <a href="../../../storage/images/<?php echo $imagenRef['URLIMAGEN']; ?>" target="_blank">
                                                <img src="../../../storage/images/<?php echo $imagenRef['URLIMAGEN']; ?>" class="page-link d-block img-thumbnail m-0 mt-1" style="width:100px;height:100px;padding:0.20rem;" />
                                            </a>
                                        <?php } ?>
            					</div>
            				</div>
            			</div>
            		</div>
                </div>
            </div>;
    <?php } ?>
</div>
