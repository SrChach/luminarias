<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <label for="">NOTA:</label> <small>Debe seleccionar un rango de fechas.</small>
        </div>
        <div class="col-md-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Fecha Inicio</span>
                </div>
                <select class="form-control" name="from" required>
                    <option value="">Seleccione fecha de inicio...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Fecha Final</span>
                </div>
                <select class="form-control" name="to" required>
                    <option value="">Seleccione fecha de fin...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-success btn-block" name="getData">Obtener datos</button>
        </div>
    </div>
    <div class="row" id="content-cards-zone"></div>
    <div class="row" id="content-table">
        <div class="col-md-12 table-responsive">
            <table class="table table-hover table-stripped table-bordered table-sm" id="table_listado">
                <thead class="thead-green" id="listado_head">
                    <tr class="text-center">
                        <th>#</th>
                        <th>FOLIO</th>
                        <th>CVE_ZONA</th>
                        <th>ZONA</th>
                        <th>MANZANA</th>
                        <th>TIPO</th>
                        <th>CANTIDAD</th>
                        <th>FECHA LEVANTAMIENTO</th>
                        <th>USUARIO</th>
                        <th>IMEI</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="body_table_listado">

                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    var totalIndividual = 0;
    var countTotal=0;
    var fechas;
    var table_listado=null;
    $(()=>{
        load();
        var url="./controller/main_controller.php?option=dates";
        ajaxCallback('',url,function (respuesta){
            res = JSON.parse(respuesta);
            fechas = res;
			$('[name=from]').html(buildOptions(res,'FECHA','FECHA'));
            load(false);
    	});
        $("[name=from]").change(function(event) {
			let index = $(this).find(":selected").index();
			let fechasArray = [];
			for (var i = index-1; i < fechas.length; i++) {
				fechasArray.push(fechas[i]);
			}
			$("[name=to]").html(buildOptions(fechasArray,'FECHA','FECHA'));
		});

        $('[name=getData]').click(function(event) {
            if ($('[name=from]').val()=="") {
                notificar('','Seleccione fecha de inicio','warning');
                return;
            }
            if ($("[name=to]").val()=="") {
                notificar('','Seleccione fecha de fin','warning');
                return;
            }
            $('#body_table_listado').html('');
            let request = {
    			route: "./controller/main_controller.php?init="+$('[name=from]').val()+"&end="+$("[name=to]").val()
    		}

            load();
    		Fetch(request).then((response)=>{
                if(response.status != true){
                    notificar('', response.message ,'warning')
                    load(false);
                    return;
                }
                
                if(table_listado!=null)
                    table_listado.destroy();
                
                $('#body_table_listado').html('');
                if(response.datas.length < 1){
                    resumenZonas(response.total);
                    load(false);
                    return;
                }
                makeTable(response.datas);
                countTotal = response.total;

                resumenZonas(response.total);

                table_listado = tableExport2('#table_listado','listado_luminarias');
                load(false);
    		});
        });
    });



    makeTable = (data) =>{

        t_head = `<tr class="text-center"><th>#</th>`;
        if(typeof(data[0].ZONA) !== 'undefined') t_head += '<th>CVE_ZONA</th>';
        if(typeof(data[0].NOMBREZONA) !== 'undefined') t_head += '<th>ZONA</th>';
        if(typeof(data[0].TIPO) !== 'undefined') t_head += '<th>TIPO</th>';
        if(typeof(data[0].FOLIO) !== 'undefined') t_head += '<th>FOLIO</th>';
        if(typeof(data[0].USUARIO) !== 'undefined') t_head += '<th>USUARIO</th>';
        if(typeof(data[0].CVEMANZANA) !== 'undefined') t_head += '<th>MANZANA</th>';
        if(typeof(data[0].CANTIDAD) !== 'undefined') t_head += '<th>CANTIDAD</th>';
        if(typeof(data[0].FECHAL) !== 'undefined') t_head += '<th>FECHA LEVANTAMIENTO</th>';
        if(typeof(data[0].IMEILEVANTAMIENTO) !== 'undefined') t_head += '<th>IMEI</th>';
        if(typeof(data[0].LATITUD) !== 'undefined') t_head += '<th>LATITUD</th>';
        if(typeof(data[0].LONGITUD) !== 'undefined') t_head += '<th>LONGITUD</th>';
        if(typeof(data[0].IDREGISTRO) !== 'undefined') t_head += '<th></th>';
        t_head += '</tr>';

        $("#listado_head").html(t_head);

        let tbody = '';
        $.each(data,function(index, item) {
            totalIndividual = totalIndividual + parseInt(item.CANTIDAD);
            
            tbody = '';
            tbody += `<tr><th>${index+1}</th>`;
            if(typeof(item.ZONA) !== 'undefined') tbody += table_data(item.ZONA);
            if(typeof(item.NOMBREZONA) !== 'undefined') tbody += table_data(item.NOMBREZONA);
            if(typeof(item.TIPO) !== 'undefined') tbody += table_data(item.TIPO);
            if(typeof(item.FOLIO) !== 'undefined') tbody += table_data(item.FOLIO);
            if(typeof(item.USUARIO) !== 'undefined') tbody += table_data(item.USUARIO);
            if(typeof(item.CVEMANZANA) !== 'undefined')
                tbody += `<td>${(item.CVEMANZANA==null) ? 'Sin dato' : item.CVEMANZANA}</td>`;
            if(typeof(item.CANTIDAD) !== 'undefined') tbody += table_data(item.CANTIDAD);
            if(typeof(item.FECHAL) !== 'undefined') tbody += table_data(item.FECHAL);
            if(typeof(item.IMEILEVANTAMIENTO) !== 'undefined') tbody += table_data(item.IMEILEVANTAMIENTO);
            if(typeof(item.LATITUD) !== 'undefined') tbody += table_data(item.LATITUD);
            if(typeof(item.LONGITUD) !== 'undefined') tbody += table_data(item.LONGITUD);
            if(typeof(item.IDREGISTRO) !== 'undefined')
                tbody += `<td>
                            <a href="?view=detalleluminaria&item=${item.IDREGISTRO}" target="_blank"><i class="fa fa-external-link"></i>Ver detalle</a>
                            <!--a href="?view=tipificacion&item=${item.IDREGISTRO}" target="_blank"><i class="fa fa-external-link"></i>Tipificar</a-->
                        </td>`;

            tbody +=`</tr>`;
            $('#body_table_listado').append(tbody);
        });
    }

    function table_data(dato = null) {
        if(dato != null)
            return `<td>${dato}</td>`;
        return "<td></td>";
    }

    resumenZonas = (total) => {
        body_zonas = `<div class="col-md-3">
                <div class="card hover border-custom">
                    <div class="card-header bg-success">
                        <h5 class="float-left">TOTAL DE CENSO</h5>
                    </div>
                    <div class="card-body">
                        <h4 class="float-left">${total} Registros</h4>
                        <i class="fa fa-calculator float-right" style="font-size:30px;"></i>
                    </div>
                </div>
            </div>`;
        $('#content-cards-zone').html(body_zonas);
    }
</script>
