<div class="container-fluid">
    <div class="row" id="">
        <div class="col-12">
            <h2 class="float-left">FECHA: <small id="date"><?=$fecha?></small></h2>
            <button type="button" name="button" class="float-right btn btn-success" id="back" style="display:none;">Regresar</button>
        </div>
        <div class="col-md-12 table-responsive">
            <table class="table table-sm table-bordered table_avance_today">
                <thead class="thead-green">
                    <tr>
                        <th>#</th>
                        <th>SUPERVISOR</th>
                        <th>USUARIOS</th>
                        <th>TOTALES</th>
                        <th>ESTIMADO</th>
                    </tr>
                </thead>
                <tbody class="tbody">

                </tbody>
                <tfoot class="thead-green">
                    <tr>
                        <th colspan="3" class="text-right">TOTAL:</th>
                        <th id="th_total"></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
        <?php include './view/reporte/complementos/historial.php' ?>
        <?php include './view/reporte/complementos/modalhrs.php' ?>
</div>

<script type="text/javascript">
    var eficiencia;
    var profile = <?php echo $_SESSION['PERFIL'] ?>;
    $(()=>{

        init();
        $('#back').click(function(event) {
            /* Act on the event */
            $(this).hide('slow');
            $('#content-complete-history').hide('slow');
            init();
        });
    });

    init = (supervisor=null) =>{
        load();
        let data ={opcion:'teams',csrf_token:'<?php echo $csrf_token ?>'};
        if (supervisor!=null) {
            data.keysupervisor = supervisor;
        }
        let request={
            route: "./controller/ReporteController.php",
            data: data
        }
        Fetch(request).then((response)=>{
            //console.log(response);
            makeTable(response);
            load(false);
            eficiencia = response.eficiencia.data;
            if (supervisor!=null) {
                renderComplete(eficiencia);
                $('#back').show('slow');
            }
        });
    }

    makeTable = (data) =>{
        let body_table="";
        let tb_users="", tb_users_T="", tb_estimados = "";
        let parcial=0,total=0,parcial_estimado=0;
        let supervisor='Sin Asignar',cvesupervisor;
        let incremento=0;
        $.each(data.usuarios,function(index, usuarios) {
            //console.log(usuarios);
            //supervisor = data.supervisor[usuario[0].SUPERVISOR];
            //console.log(data.supervisores);
            incremento++;

            parcial = 0;
            parcial_estimado = 0;
            tb_users = "";
            tb_users_T = "";
            tb_estimados = "";
            $.each(usuarios,function(incr, usuario) {
                supervisor='Sin Asignar';
                cvesupervisor = 'null';
                if (data.supervisores[usuario.SUPERVISOR]) {
                    supervisor = data.supervisores[usuario.SUPERVISOR][0].SUPERVISOR;
                    cvesupervisor = data.supervisores[usuario.SUPERVISOR][0].IDUSUARIO;
                }
                tb_users+= `<tr><td>${usuario.USUARIO}</td></tr>`;
                tb_users_T+= `<tr><td>${usuario.TOTAL}</td></tr>`;
                tb_estimados+= `<tr><td>300</td></tr>`;
                parcial = parcial + parseInt(usuario.TOTAL);
                parcial_estimado = parcial_estimado + 300;
            });

            /*$.each(usuarios,function(incr, usuario_t) {
                tb_users_T+= `<tr><td>${usuario_t.TOTAL}</td></tr>`;
                tb_estimados+= `<tr><td>300</td></tr>`;
                parcial = parcial + parseInt(usuario_t.TOTAL);
                parcial_estimado = parcial_estimado + 300;
            });*/

            tb_users+= `<tr class="bg-secondary text-right"><th>TOTAL:</th></tr>`;
            tb_users_T+= `<tr class="bg-secondary"><th>${parcial}</th></tr>`;
            tb_estimados+= `<tr class="bg-secondary"><td>${parcial_estimado}</td></tr>`;

            total = total + parcial;

            body_table+=`<tr>
                            <th>${incremento}</th>
                            <th class="align-middle">
                                ${supervisor}
                                ${(profile==888 ||profile==10) ? '<a href="javascript:init('+cvesupervisor+');"cvesupervisor><i class="fa fa-eye"></i></a>' : '' }
                            </th>
                            <td>
                                <table class="table table-sm table-bordered">
                                    <tbody class="">
                                    ${tb_users}
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table table-sm table-bordered">
                                    <tbody class="">
                                    ${tb_users_T}
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table table-sm table-bordered">
                                    <tbody class="">
                                        ${tb_estimados}
                                    </tbody>
                                </table>
                            </td>
                        </tr>`;
        });
        /*<td>
            <table class="table table-sm table-bordered">
                <tbody class="">
                ${tb_users_T}
                </tbody>
            </table>
        </td>
        <td>
            <table class="table table-sm table-bordered">
                <tbody class="">
                    ${tb_estimados}
                </tbody>
            </table>
        </td>*/
        $('.tbody').html(body_table);
        $('#th_total').html(total);
    }
    renderComplete = (eficiencia) =>{
        $('#content-complete-history').show();
        $('#chart_produccion_gral').empty();
        $('#chart_produccion_gral').html();
        renderTable('.table_eficiencia',eficiencia,'',1,1);

        let rowsTable = document.getElementById('table-dinamic').rows;

        for (var i = 1; i < rowsTable.length - 1; i++) {
            for (var c = 2; c < rowsTable[i].cells.length - 1; c++) {
                rowsTable[i].cells[c].dataset.date = rowsTable[0].cells[c].innerHTML;
                // rowsTable[i].cells[c].dataset.user = rowsTable[i].cells[0].innerHTML;
                rowsTable[i].cells[c].addEventListener('click', showUserResumeProduction);
            }
        }


        let ykeys = [];
        if (eficiencia.length>0) {
            $.each(Object.keys(eficiencia[0]),function(index, value) {
                if ( value!="USUARIOLEVANTAMIENTO" && value!="USUARIO" && value!="TOTAL") {
                    ykeys.push(value);
                }
            });
        }
        renderChartBar({
            element:'chart_produccion_gral',
            type:'bar',
            data: eficiencia,
            xkey: 'USUARIO',
            ykeys: ykeys,
            stack:true
        }).redraw();
    }

    function showUserResumeProduction(_this=''){
        load();
        let request={
            route: "./controller/ReporteController.php",
            data: {
                opcion:'eficiencia_usuario_dia',
                filter:this.dataset.filter,
                date:this.dataset.date,
                csrf_token:'<?php echo $csrf_token ?>'
            }
        }
        Fetch(request).then((response)=>{
            if (response.code==true) {
                $('#userInfo').html('<p><span>'+response.usuario.USUARIO+'</span><br><span>Fecha: </span>'+this.dataset.date+'</p>');

                let hours_array = [
                    { hours: '00:00 Hrs', total: 0},
                    { hours: '01:00 Hrs', total: 0},
                    { hours: '02:00 Hrs', total: 0},
                    { hours: '03:00 Hrs', total: 0},
                    { hours: '04:00 Hrs', total: 0},
                    { hours: '05:00 Hrs', total: 0},
                    { hours: '06:00 Hrs', total: 0},
                    { hours: '07:00 Hrs', total: 0},
                    { hours: '08:00 Hrs', total: 0},
                    { hours: '09:00 Hrs', total: 0},
                    { hours: '10:00 Hrs', total: 0},
                    { hours: '11:00 Hrs', total: 0},
                    { hours: '12:00 Hrs', total: 0},
                    { hours: '13:00 Hrs', total: 0},
                    { hours: '14:00 Hrs', total: 0},
                    { hours: '15:00 Hrs', total: 0},
                    { hours: '16:00 Hrs', total: 0},
                    { hours: '17:00 Hrs', total: 0},
                    { hours: '18:00 Hrs', total: 0},
                    { hours: '19:00 Hrs', total: 0},
                    { hours: '20:00 Hrs', total: 0},
                    { hours: '21:00 Hrs', total: 0},
                    { hours: '22:00 Hrs', total: 0},
                    { hours: '23:00 Hrs', total: 0}
                ];

                response.data.forEach(function(item){
                    hours_array[parseInt(item.INDEXHORA)]['total'] = parseInt(item.TOTALHORA);
                });

                $('#graphicsResume').html('');
                $('#modalResumeUserDay').modal('show');

                setTimeout(function(){
                    chart = renderChartBar({
                        element:'graphicsResume',
                        type:'bar',
                        data: hours_array,
                        xkey: 'hours',
                        ykeys: ['total'],
                        //stack:true
                    });
                }, 200)


            }
            load(false);
        });
        load(false);
    }


</script>
