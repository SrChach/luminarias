<div class="container-fluid">
    <button id="btnReport" class="btn btn-small btn-info">Descargar Reporte</button>

    <script type="text/javascript">

        let btnReporte = document.getElementById("btnReport");
        btnReporte.addEventListener('click', downloadReport);

        function downloadReport(){

            fetch("./controller/REP_PGR_ANU_CEN_ALU_PUB_SEM.php", {
                method: 'GET'
            })
            .then(response => response.blob())
            .then(blob => {
                var url = window.URL.createObjectURL(blob);
                var a = document.createElement('a');
                a.href = url;
                a.download = "Reporte.xlsx";
                document.body.appendChild(a);
                a.click();
                a.remove();
            });
        }

        // function downloadReport(){
        //
        //     fetch("./controller/qrFile.php", {
        //         method: 'POST',
        //         body: ['hola']
        //     })
        //     .then(function(response){
        //         console.log('No arroja valores', response);
        //     });
        // }
    </script>
</div>
