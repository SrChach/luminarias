<style media="screen">
    body{
        padding-right: 0px !important;
    }
    nav{
        padding-right: 23px !important;
    }

    #graphicsResume{
        overflow-x: auto;
    }
</style>
<div class="container-fluid">
    <div class="row" id="">
        <div class="col-md-12 table-responsive table_eficiencia" style="height:700px;">

        </div>
        <div class="col-md-12">
            <div class="card border-success mt-5">
                <h5 class="card-header text-center bg-success">Avances por usuario</h5>
                <div class="card-body pt-4 pl-1" id="chart_produccion_gral" style="height:700px; padding-bottom:120px;">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modalResumeUserDay" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title" id="titleResumeUserDay">Resumen de productividad de usuario por Hora</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card border-info">
                            <h5 id="userInfo" class="card-header text-center bg-info">Avances por usuario</h5>
                            <div class="card-body pt-4 pl-1" id="graphicsResume" style="height:500px;width:100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
var chart;
    var table_eficiencia;
    $(()=>{
        load();
        let request={
            route: "./controller/ReporteController.php",
            data: {opcion:'eficiencia_usuario',csrf_token:'<?php echo $csrf_token ?>'}
        }
        Fetch(request).then((response)=>{
            if (response.code==true) {
                
                //makeTable({datas:response.data,hidden:['USUARIOLEVANTAMIENTO'],countAfter:"USUARIO"});
                renderTable('.table_eficiencia',response.data,'',1,1);

                let rowsTable = document.getElementById('table-dinamic').rows;

                for (var i = 1; i < rowsTable.length - 1; i++) {
                    for (var c = 2; c < rowsTable[i].cells.length - 1; c++) {
                        rowsTable[i].cells[c].dataset.date = rowsTable[0].cells[c].innerHTML;
                        // rowsTable[i].cells[c].dataset.user = rowsTable[i].cells[0].innerHTML;
                        rowsTable[i].cells[c].addEventListener('click', showUserResumeProduction);
                    }
                }

                table_eficiencia = tableExport2('#table-dinamic','','','',-1);
                let ykeys = [];
                if (response.data.length>0) {
                    $.each(Object.keys(response.data[0]),function(index, value) {
                        if ( value!="USUARIOLEVANTAMIENTO" && value!="USUARIO" && value!="TOTAL") {
                            ykeys.push(value);
                        }
                    });
                }

                renderChartBar({
                    element:'chart_produccion_gral',
                    type:'bar',
                    data: response.data,
                    xkey: 'USUARIO',
                    ykeys: ykeys,
                    stack:true
                });
            }
            load(false);
        });

        function showUserResumeProduction(_this=''){
            load();
            let request={
                route: "./controller/ReporteController.php",
                data: {
                    opcion:'eficiencia_usuario_dia',
                    filter:this.dataset.filter,
                    date:this.dataset.date,
                    csrf_token:'<?php echo $csrf_token ?>'
                }
            }
            Fetch(request).then((response)=>{
                if (response.code==true) {
                    $('#userInfo').html('<p><span>'+response.usuario.USUARIO+'</span><br><span>Fecha: </span>'+this.dataset.date+'</p>');

                    let hours_array = [
                        { hours: '00:00 Hrs', total: 0},
                        { hours: '01:00 Hrs', total: 0},
                        { hours: '02:00 Hrs', total: 0},
                        { hours: '03:00 Hrs', total: 0},
                        { hours: '04:00 Hrs', total: 0},
                        { hours: '05:00 Hrs', total: 0},
                        { hours: '06:00 Hrs', total: 0},
                        { hours: '07:00 Hrs', total: 0},
                        { hours: '08:00 Hrs', total: 0},
                        { hours: '09:00 Hrs', total: 0},
                        { hours: '10:00 Hrs', total: 0},
                        { hours: '11:00 Hrs', total: 0},
                        { hours: '12:00 Hrs', total: 0},
                        { hours: '13:00 Hrs', total: 0},
                        { hours: '14:00 Hrs', total: 0},
                        { hours: '15:00 Hrs', total: 0},
                        { hours: '16:00 Hrs', total: 0},
                        { hours: '17:00 Hrs', total: 0},
                        { hours: '18:00 Hrs', total: 0},
                        { hours: '19:00 Hrs', total: 0},
                        { hours: '20:00 Hrs', total: 0},
                        { hours: '21:00 Hrs', total: 0},
                        { hours: '22:00 Hrs', total: 0},
                        { hours: '23:00 Hrs', total: 0}
                    ];

                    response.data.forEach(function(item){
                        hours_array[parseInt(item.hora)]['total'] = parseInt(item.cantidad);
                    });

                    $('#graphicsResume').html('');
                    $('#modalResumeUserDay').modal('show');

                    setTimeout(function(){
                        chart = renderChartBar({
                            element:'graphicsResume',
                            type:'bar',
                            data: hours_array,
                            xkey: 'hours',
                            ykeys: ['total'],
                            //stack:true
                        });
                    }, 200)


                }
                load(false);
            });
            load(false);
        }
    });




</script>
