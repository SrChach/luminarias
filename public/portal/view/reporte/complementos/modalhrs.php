<div class="container">
	<div class="modal fade bd-example-modal-lg" id="modalResumeUserDay" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h5 class="modal-title" id="titleResumeUserDay">Resumen de productividad de usuario por Hora</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="card border-info">
						<h5 id="userInfo" class="card-header text-center bg-info">Avances por usuario</h5>
						<div class="card-body pt-4" id="graphicsResume" style="height:500px;width:100%;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
