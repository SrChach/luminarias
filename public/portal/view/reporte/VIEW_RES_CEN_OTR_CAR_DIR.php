<div class="container-fluid">
    <button id="btnReport" class="btn btn-small btn-info">Descargar Reporte</button>

    <script type="text/javascript">

        let btnReporte = document.getElementById("btnReport");
        btnReporte.addEventListener('click', downloadReport);

        function downloadReport(){

            fetch("./controller/REP_RES_CEN_OTR_CAR_DIR.php", {
                method: 'GET'
            })
            .then(response => response.blob())
            .then(blob => {
                var url = window.URL.createObjectURL(blob);
                var a = document.createElement('a');
                a.href = url;
                a.download = "Reporte.xlsx";
                document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
                a.click();
                a.remove();  //afterwards we remove the element again
            });
        }
    </script>
</div>
