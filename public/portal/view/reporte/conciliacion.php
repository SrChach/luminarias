<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <label for="">NOTA:</label> <small>Debe seleccionar un rango de fechas.</small>
        </div>
        <div class="col-md-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Fecha Inicio</span>
                </div>
                <select class="form-control" name="from" required>
                    <option value="">Seleccione fecha de inicio...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Fecha Final</span>
                </div>
                <select class="form-control" name="to" required>
                    <option value="">Seleccione fecha de fin...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-success btn-block" name="getData">Obtener datos</button>
        </div>
    </div>
    <div class="row" id="">
        <div class="col-md-12">
            <h5>
                <?php
                if ($_SESSION['PERFIL']==6) {
                    $title = "CFE";
                }elseif ($_SESSION['PERFIL']==10) {
                    $title = "TIA";
                }elseif ($_SESSION['PERFIL']==1) {
                    $title = "ALCALDIA";
                }else{
                    $title = "";
                }
                echo $title;
                ?>
            </h5>
        </div>
        <div class="col-md-12 table-responsive">
            <table class="table table-sm table-bordered table_conciliacion">
                <thead class="thead-green">
                    <tr>
                        <th>#</th>
                        <th>FOLIO</th>
                        <th>CLASIFICACIÓN</th>
                        <th>UBICACIÓN</th>
                        <th>MÓDELO</th>
                        <th>TECNOLOGÍA</th>
                        <th>WATTS</th>
                        <th>HRS</th>
                        <th>COORDENADAS</th>
                        <th>IMAGEN</th>
                        <th>CONCILIACIÓN</th>
                    </tr>
                </thead>
                <tbody class="tbody">

                </tbody>
            </table>
        </div>
    </div>


    <div class="modal fade" id="modalImg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">EVIDENCIA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" align="center">
                    <img id="imgevidencia" src="" alt="Evidencia" class="img-thumbnail img-fluid" style="height:600px; width:600px;">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table_conciliacion=null;
    var perfil = '<?php echo $_SESSION['PERFIL'] ?>';
    $(()=>{
        load();
        var url="./controller/main_controller.php?option=dates";
        ajaxCallback('',url,function (respuesta){
            res = JSON.parse(respuesta);
            fechas = res;
			$('[name=from]').html(buildOptions(res,'FECHA','FECHA'));
            load(false);
    	});
        $("[name=from]").change(function(event) {
			let index = $(this).find(":selected").index();
			let fechasArray = [];
			for (var i = index-1; i < fechas.length; i++) {
				fechasArray.push(fechas[i]);
			}
			$("[name=to]").html(buildOptions(fechasArray,'FECHA','FECHA'));
		});
        $('[name=getData]').click(function(event) {
            load();
            //$('#body_table_listado').html('');
            if ($('[name=from]').val()=="") {
                notificar('','Seleccione fecha de inicio','warning');
                return;
            }
            if ($("[name=to]").val()=="") {
                notificar('','Seleccione fecha de fin','warning');
                return;
            }
            let request={
                route: "./controller/ReporteController.php",
                data: {opcion:'conciliacion',csrf_token:'<?php echo $csrf_token ?>', init: $('[name=from]').val(), end: $("[name=to]").val() }
            }
            Fetch(request).then((response)=>{
                console.log(response);
                if (table_conciliacion!=null) {
                    table_conciliacion.destroy();
                }
                makeTable(response.data);
                table_conciliacion = tableExport2('.table_conciliacion');
                    /*if (table_conciliacion!=null) {
                        table_conciliacion.destroy();
                        tableExport2('.table_conciliacion');
                    }else{
                        table_conciliacion = tableExport2('.table_conciliacion');
                    }*/
                load(false);
            });
        });
    });

    makeTable = (data) =>{
        let tbody = "";

        $.each(data,function(index, row) {
            tbody += `<tr>
                        <th>${index+1}</th>
                        <td>${row.FOLIO}</td>
                        <td>${row.TIPO}</td>
                        <td>${(row.UBICACION==null) ? 'N/A' : row.UBICACION}</td>
                        <td>${(row.MODELO==null) ? 'Sin dato' : row.MODELO}</td>
                        <td>${(row.TECNOLOGIA==null) ? 'Sin dato' : row.TECNOLOGIA}</td>
                        <td>${(row.WATTS==null) ? 'Sin dato':row.WATTS}</td>
                        <td>${row.HRS}</td>
                        <td>${row.LATITUD}, ${row.LONGITUD}<a href="https://maps.google.com/?q=${row.LATITUD},${row.LONGITUD}" target="_blank"><i class="fa fa-map-marker text-success float-right"></i></a></td>
                        <td class="text-center"><a class="btn btn-success btn-sm btn-evidencia" data-url="${row.URLIMAGEN}"><i class="fa fa-eye text-white"></i></a></td>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio_row${index+1}" id="exampleRadios1" value="1" data-filter="${row.IDIMAGEN}">
                                <label class="form-check-label" for="radio_row${index+1}">
                                    De acuerdo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="radio_row${index+1}" id="exampleRadios1" value="0" data-filter="${row.IDIMAGEN}">
                                <label class="form-check-label" for="radio_row${index+1}">
                                    En revisión
                                </label>
                            </div>
                        </td>
                    </tr>`;
        });
        $('.tbody').html(tbody);

        $('.btn-evidencia').click(function(event) {
            /* Act on the event */
            let url = "../../../storage/images/" + $(this).data().url;
            console.log(url);
            $('#imgevidencia').attr('src', url);
            $('#modalImg').modal();
        });

        $('[type=radio]').click(function(event) {
            /* Act on the event */
            console.log(perfil);
            setConciliacion($(this).data().filter,this.value);
        });
    }

    setConciliacion = (idimg,value)=>{
        let opcion=null;
        if (perfil==6) {
            opcion="setConciliacionCFE";
        }else if (perfil==10) {
            opcion="setConciliacionTIA";
        }else if (perfil==1) {
            opcion="setConciliacionAlcandia";
        }else {
            opcion="setConciliacion";
        }
        let request={
            route: "./controller/GestionController.php",
            data: {opcion:opcion,value:value,keyimg:idimg,csrf_token:'<?php echo $csrf_token ?>'}
        }
        Fetch(request).then((response)=>{
            console.log(response);
            /*if (response.codigo==true) {
                makeTable(response.data);
                //table_conciliacion = tableExport2('#table-dinamic');
            }*/
        });
    }


</script>
