<?php include_once './view/template/head_config.php'; ?>

<?php if (isset($_SESSION['Auth']) and $_SESSION['Auth'] == 0) : ?>
  <script type="text/javascript">
    $(function() {
      $('#modalVerificaPass').modal('show');
      $('.auth').hide();
    });
  </script>
<?php endif; ?>
<?php
  include_once './view/template/body_config.php';
  if (isset($_GET['view'])) {
    include_once './view/profile/verifica_pass.php';
  }
?>

<?php include_once './view/template/footer_config.php'; ?>
