<style media="screen">
    .box-footer{
        background: rgba(0,0,0,.1);
        color: black;
        display: block;
        padding: 3px 0;
        position: relative;
        text-align: center;
        text-decoration: none;
        z-index: 10;
    }
    .box-footer:hover {
        background: rgba(0,0,0,.15);
        color: black;
        letter-spacing: 5px;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="row" id="content-cards">

            </div>
        </div>
        <div class="col-md-9 table-responsive tableFixHead" style="height: calc(100vh - 170px);">
            <div class="row">
                <div class="col-md-2" id="content-btn-export" style="display:none;">
                    <button type="button" name="button" onclick="ExportToExcel();">Exportar</button>
                </div>
                <div class="col-md-12" id="content_table_devices">
                    <div class="alert alert-info">
                        Seleccione zona para mostrar dispositivos.
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="modalAddDevices" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form class="modal-content" id="form-file" action="javascript:saveDevices();">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Dispositivos a Zona: <small id="text_zona"></small></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#form-file')[0].reset();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="load-file" style="display:none;">
                        <div class="col-md-4 offset-md-4 text-center">
                            <i class="fa fa-spin fa-spinner" style="font-size:100px;"></i>
                        </div>
                    </div>
                    <div class="input-group mb-3" id="content-input">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Seleccionar Archivo de Carga</span>
                        </div>
                        <input type="file" class="form-control" name="file" onchange="ValidafileType(this,6)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Guargar Dispositivos</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="$('#form-file')[0].reset();">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var cve_zona_filter="";
    var table=null;
    var name_zona=null;
    var filter_zona=null;
    $(()=>{
        _init();
    });

    _init = (filter=null,zona="",card="") =>{
        filter_zona=filter;
        name_zona = zona;
        $('#content-btn-export').hide('slow');
        load();
        let data = {opcion:'getDevicesByZone',csrf_token:'<?php echo $csrf_token ?>'};

        if (filter!=null) {
            data.zona = filter;
        }

        let request = {
            route: './controller/DevicesController.php',
            data: data
        }
        Fetch(request).then((response)=>{
            if (response.codigo===true) {
                makeCards(response.data);
                if (filter!=null) {
                    //alert(response.detalle.length);
                    if (response.detalle.length>0) {
                        $('#content-btn-export').show('slow');
                    }
                    $('.set-hover').removeClass('set-hover');
                    $(card).addClass('set-hover');
                    
                    makeTable(response.detalle);
                    //table = tableExport2('.table','','','',-1);
                }
            }else{
                notificar('Lo siento', 'Algo paso. Intente de nuevo!','error');
            }
            load(false);
        });
    };
    makeCards = (data) =>{
        let body_zonas="", set_hover="";
        $.each(data,function(index, zona) {
            set_hover = "";
            if (index==0) {
                set_hover = "set-hover";
            }
            body_zonas+=`<div class="col-md-12 mb-5">
                            <a href="javascript:_init('${zona.IDZONA}','${zona.ZONA}','.card-${index}');" style="text-decoration:none">
                                <div class="card hover border-custom ${set_hover} card-${index}">
                                    <div class="card-header bg-success">
                                        <h5 class="float-left">ZONA: ${zona.IDZONA}</h5>
                                        <label class="float-right">${zona.ZONA}</label>
                                    </div>
                                    <div class="card-body text-dark">
                                        <small><b>DISPOSITIVOS:</b></small><h5 class="float-right">${zona.TOTAL}</h5>
                                    </div>
                                    <a class="text-center box-footer" href="javascript:launchModal('${zona.ZONA}','${zona.IDZONA}')">Agregar Dispositivos</a>
                                </div>
                            </a>
                        </div>`;
        });
        $('#content-cards').html(body_zonas);
    }
    makeTable = (rows) =>{
        let table =`<table class="table table-sm table-bordered" id="table-devices">
                        <thead class="thead-green thead-fixed">
                            <tr>
                                <th>#</th>
                                <th>IMEI</th>
                                <th>IME2</th>
                                <th>No. SERIE SIM</th>
                                <th>CELULAR</th>
                                <th>ZONA</th>
                                <th>FECHA DE ENTREGA</th>
                                <th>NOMBRE</th>
                                <th>AP. PATERNO</th>
                                <th>AP. MATERNO</th>
                                <th>USUARIO</th>
                                <th>CONTRASEÑA</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>`;
            $.each(rows, function(index, row) {
                    table+=`<tr>
                                <th>${index+1}</th>
                                <td>${row.IMEI}</td>
                                <td>${(row.IMEI2==null) ? 'Sin dato' : row.IMEI2}</td>
                                <td>${(row.NUM_SERIE_SIM==null) ? 'Sin dato' : row.NUM_SERIE_SIM}</td>
                                <td>${(row.CELULAR==null) ? 'Sin dato': row.CELULAR}</td>
                                <td>${row.NOMBREZONA}</td>
                                <td>${(row.FECHAENTREGA==null) ? 'Sin dato' : row.FECHAENTREGA}</td>
                                <td>${row.NOMBRE}</td>
                                <td>${row.APATERNO}</td>
                                <td>${row.AMATERNO}</td>
                                <td>${row.USUARIO}</td>
                                <td>${row.CONTRASEÑA}</td>
                                <td>
                                    <button class="btn btn-danger" data-filter="${row.IDDISPOSITIVO}"><i class="fa fa-remove"></i></button>
                                </td>
                            </tr>`;
            });
                table+=`</tbody>
                    </table>`;
        $('#content_table_devices').html(table);
        $('.btn-danger').click((event)=>{
            console.log($(event.currentTarget).data().filter);
            
            let data = {opcion:'downDevice',csrf_token:'<?php echo $csrf_token ?>',key:$(event.currentTarget).data().filter};
            let request = {
                route: './controller/DevicesController.php',
                data: data
            }
            Fetch(request).then((response)=>{
                console.log(response);
                if(response.codigo==true){
                    notificar('',response.message,'success');
                    _init(filter_zona);
                }else{
                    notificar('',response.message,'warning');
                }
            });
        });
    }

    launchModal = (zona,cve_zona) =>{
        cve_zona_filter = cve_zona;        
        $('#text_zona').html(zona);
        $('#modalAddDevices').modal();
    }
    saveDevices = () =>{
        $('#load-file').show('slow');
        $('#content-input').hide('slow');
        var formData = new FormData();

        formData.append('file',$('input[name=file]')[0].files[0]);
        formData.append('csrf_token','<?php echo $csrf_token ?>');
        formData.append('zona',cve_zona_filter);
        formData.append('nombrezona',$('#text_zona').html());
        formData.append('opcion','addDevicesToZone');

        uploadMultiFile('./controller/DevicesController.php',formData).then((response)=>{
            let titleMsg = 'Lo siento', typeMsg = 'error';
            console.log(response);
            $('#load-file').hide('slow');
            if (response.codigo==true) {
                titleMsg = 'EXITO !', typeMsg = 'success';
                $('#form-file')[0].reset();
                $('#modalAddDevices').modal('hide');
                _init();
            }
            notificar(titleMsg,response.message,typeMsg);
            $('#content-input').show('slow');
        });
    }

    function ExportToExcel(){
       /*var htmltable= document.getElementById('table-devices');
       var html = htmltable.outerHTML;
       window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));*/

       var htmls = "";
            var uri = 'data:application/vnd.ms-excel;base64,';
            var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'; 
            var base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            };

            var format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                })
            };

            htmls = $('#table-devices').html();

            var ctx = {
                worksheet : 'Worksheet',
                table : htmls
            }


            var link = document.createElement("a");
            link.download = "Dispositivos_"+name_zona+".xls";
            link.href = uri + base64(format(template, ctx));
            link.click();
    }
</script>
