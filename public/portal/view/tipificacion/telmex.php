<div class="container-fluid" id="vue-father">
	<div class="row" id="app">
		
	</div>
</div>


<script src="./vendor/dist/js/vue.js"></script>
<script src="./vendor/dist/js/lodash.js"></script>

<!-- BEGIN - VUE COMPONENTS -->
<script src="./vendor/vue-components/vue-zoomer.js"></script>
<script src="./vendor/vue-components/input-field-horizontal.js"></script>
<script src="./vendor/vue-components/tipificacion-card.js"></script>
<script src="./vendor/vue-components/tipificacion-image.js"></script>
<!-- END --- VUE COMPONENTS -->

<script>
	var app = new Vue({
		el: '#app',
		data: {
			tipo: null,
			catalogo_selects: [],
			select_by: [],
			all_render: [],
			sin_datos: true
		},
		template: `
			<div>
				<div v-for="(rendering, index) in all_render" class="row row_content">
					<div class="col-md-7 row-eq-h">
						<div class="card w-100">
							<div class="card-header bg-dark">
								<h5 class="float-left">Tipificando <b>{{ (rendering.tipo == null) ? '' : '- ' + rendering.tipo }}</b></h5>
							</div>
							<div class="card-body col-12">
								<input-field-horizontal @interface="dateBack" :father_index="index" :select_by="rendering.select_by" :select_dictionary="rendering.catalogo_selects" :referente="'ID'"/>
								<div class="row">
									<div class="input-group mb-3">
										<div class="input-group-prepend"><span class="input-group-text">Número económico</span></div>
										<input class="form-control" v-model="rendering.no_economico" maxlength="10"/>
									</div>
								</div>
								<div class="row" v-if="rendering.selected">
									<div v-if="/^.*adtran.*$/i.test(rendering.catalogo_selects.find(o => o.ID == rendering.selected).TEXTO) == true" 
										class="input-group mb-3"
									>
										<div class="input-group-prepend"><span class="input-group-text">TBA</span></div>
										<input class="form-control" v-model="rendering.tba" maxlength="4"/>
									</div>
								</div>
							</div>
							<div class="card-footer text-center">
								<button class="btn btn-block btn-success" @click="enviar(index)"
									v-if="typeof(rendering['selected']) != 'undefined' && rendering['selected'] != null">
									Tipificar
								</button>
							</div>
						</div>
					</div>
					<div class="col-md-5 row-eq-h">
						<tipificacion-image 
							:referencias="rendering.referencias"
							:folio="rendering.folio"
							:observacion="rendering.observacion"
							:no_separar="true">
						</tipificacion-image>
					</div>
				</div>
				<div class="row" v-if="all_render.length < 1 && sin_datos != true">
					<div class="col-sm-6">
						<button class="btn btn-block btn-success" @click="pedir_registro()">SEGUIR TIPIFICANDO</button>
					</div>
					<div class="col-sm-6">
						<a href="?view=inicio">
							<button class="btn btn-block btn-danger">IR A INICIO</button>
						</a>
					</div>
				</div>
			</div>
		`,
		created: function(){
			Fetch({
				route: './controller/GestionController.php', 
				data: {opcion:'getCatalogo_CD_telmex', csrf_token:'<?php echo $csrf_token ?>'}
			})
			.then((response) => {
				this.tipo = response.tipo
				this.catalogo_selects = response.selects
				this.select_by = response.select_by				
			})
			.then(() => {
				this.pedir_registro()
			})
		},
		methods: {
			dateBack: function (event) {
				let now = event.father_index
				this.all_render[now]['selected'] = event.selected
				this.$forceUpdate()
			},
			enviar: function(index){
				let seleccionado = this.all_render[index]

				let check_tba = /^.*adtran.*$/i.test(seleccionado.catalogo_selects.find(o => o.ID == seleccionado.selected).TEXTO);
				let tba = (check_tba && typeof(seleccionado.tba) == 'string' && seleccionado.tba != '') ? seleccionado.tba : null;
				let no_economico = (seleccionado.no_economico == '') ? null : seleccionado.no_economico;

				let datos = {
					opcion:'update_CD_telmex', csrf_token:'<?php echo $csrf_token ?>',
					selected: seleccionado.selected, 
					folio: seleccionado.folio,
					tba: tba,
					no_economico: no_economico
				}

				Fetch({
					route: './controller/GestionController.php', 
					data: datos
				})
				.then(res => {
					if(typeof(res['error']) != 'undefined'){
						alert(res.error);
						return;
					}
					console.log(res)

					this.all_render.splice(index, 1)
				})
			},
			pedir_registro: function(){
				Fetch({
					route: './controller/GestionController.php', 
					data: {opcion:'getAsignacion_CD_telmex', csrf_token:'<?php echo $csrf_token ?>'}
				})
				.then((res) => {
					if( typeof(res.error) != 'undefined'){
						alert(res.error);
						return;
					}

					let all_render = []

					let temp = {}
					temp.tipo = JSON.parse(JSON.stringify(this.tipo))
					temp.catalogo_selects = JSON.parse(JSON.stringify(this.catalogo_selects))
					temp.select_by = JSON.parse(JSON.stringify(this.select_by))
					
					temp.observacion = res.actual.OBSERVACIONLEVANTAMIENTO
					temp.folio = res.actual.FOLIO
					temp.referencias = res.actual.evidencias
					res.actual.referencias.forEach((el) => {
						temp.referencias.push(el)
					})
					temp.no_economico = ''
					temp.tba = ''

					all_render.push(temp)

					this.all_render = all_render
					this.sin_datos = false
				})
			}
		}
	})

	$('#titlePage').text('Tipificacion')
</script>

<style>
	.row-eq-h {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
	}
	.vue-zoomer {
		overflow: hidden;
		transition: background-color .5s
	}

	.zoomer {
		transform-origin: 50% 50%;
		width: 100%;
		height: 100%
	}

	.zoomer>img {
		vertical-align: top;
		user-drag: none;
		-webkit-user-drag: none;
		-moz-user-drag: none
	}

	.vue-zoomer-gallery {
		position: relative;
		overflow: hidden;
		user-select: none;
		min-width: 100px;
		min-height: 100px
	}

	.vue-zoomer-gallery>* {
		display: inline-block
	}

	.vue-zoomer-gallery.anim .slide {
		transition: left .4s
	}

	.slide {
		position: absolute;
		top: 0;
		object-fit: contain;
		width: 100%;
		height: 100%;
		user-drag: none;
		-webkit-user-drag: none;
		-moz-user-drag: none;
		-ms-user-drag: none
	}
</style>