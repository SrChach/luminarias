<div class="container-fluid" id="vue-father">
	<div class="row" id="app">
		
	</div>
</div>


<script src="./vendor/dist/js/vue.js"></script>
<script src="./vendor/dist/js/lodash.js"></script>

<!-- BEGIN - VUE COMPONENTS -->
<script src="./vendor/vue-components/vue-zoomer.js"></script>
<script src="./vendor/vue-components/input-field.js"></script>
<script src="./vendor/vue-components/tipificacion-card.js"></script>
<script src="./vendor/vue-components/tipificacion-image.js"></script>
<!-- END --- VUE COMPONENTS -->

<script>
	var app = new Vue({
		el: '#app',
		data: {
			catalogo_selects: [],
			catalogo_tipificar: [],
			rendering_now: [],
			working: [],
			images: [
				"http://207.249.158.187/cfe_luminarias/storage/images/32559420190930102354_foto_luminaria_1.jpg",
				"http://207.249.158.187/cfe_luminarias/storage/images/32559420190930102354_foto_luminaria_2.jpg",
				"http://207.249.158.187/cfe_luminarias/storage/images/32559420190930102354_foto_luminaria_3.jpg"
			],
			actual: 0,
			id_registro_actual: null
		},
		template: `
			<div class="row row_content">
				<div class="col-md-7">
					<tipificacion-card :select_list="rendering_now" tipo="Cargas Directas"></tipificacion-card>
				</div>
				<div class="col-md-5">
					<tipificacion-image :urls="images"></tipificacion-image>
				</div>
				<div class='col-2'><button @click='siguiente_form'>tocame 7u7r {{ catalogo_tipificar.length }}</button></div>
				<div class='col-10'>{{ catalogo_selects[3] }}</div>
			</div>
		`,
		created: function(){
			Fetch({
				route: './controller/GestionController.php', 
				data: {opcion:'getCatalogosLuminarias', csrf_token:'<?php echo $csrf_token ?>'}
			})
			.then((catalogo_selects) => {
				this.catalogo_selects = catalogo_selects;

				return Fetch({ 
					route: './controller/GestionController.php', 
					data: {opcion:'getAsignacionLuminarias', csrf_token:'<?php echo $csrf_token ?>'}
				})
			})
			.then((response) => {
						this.catalogo_tipificar = response
						this.siguiente_form()
			})
			
		},
		methods: {
			crear_form: function(elemento_a_tipificar){
				if(this.catalogo_selects == []) return [];
				let template_selects = JSON.parse(JSON.stringify(this.catalogo_selects))

				Object.keys(elemento_a_tipificar).forEach((llave) => {
					let valor_actual = elemento_a_tipificar[llave]

					if( llave == 'IDREGISTRO' ){ // si es IDREGISTRO, lo asignamos como actual y seguimos
						this.id_registro_actual = (valor_actual != null) ? valor_actual : null;
						return;
					}

					if( typeof(valor_actual) == 'object' ) return;

					let check_padre = template_selects.find(o => o.name == llave)
					if( typeof(check_padre) == 'undefined' ) return;

					let check_hijo = check_padre.options.find(v => v.value == valor_actual)
					if( typeof(check_hijo) == 'undefined' ) return;
					
					template_selects
						.find(o => o.name == llave)
						.options.find(v => v.value == valor_actual)['selected'] = true

				})

				return template_selects
			},
			siguiente_form: function(){
				if(this.catalogo_tipificar.length > 0)
					this.rendering_now = this.crear_form(this.catalogo_tipificar.shift())
				else {
					this.rendering_now = this.catalogo_selects
					this.id_registro_actual = null
					alert('nada por tipificar')
				}
			}
		}
	})

	$('#titlePage').text('Tipificacion')
</script>

<style>
	.vue-zoomer {
		overflow: hidden;
		transition: background-color .5s
	}

	.zoomer {
		transform-origin: 50% 50%;
		width: 100%;
		height: 100%
	}

	.zoomer>img {
		vertical-align: top;
		user-drag: none;
		-webkit-user-drag: none;
		-moz-user-drag: none
	}

	.vue-zoomer-gallery {
		position: relative;
		overflow: hidden;
		user-select: none;
		min-width: 100px;
		min-height: 100px
	}

	.vue-zoomer-gallery>* {
		display: inline-block
	}

	.vue-zoomer-gallery.anim .slide {
		transition: left .4s
	}

	.slide {
		position: absolute;
		top: 0;
		object-fit: contain;
		width: 100%;
		height: 100%;
		user-drag: none;
		-webkit-user-drag: none;
		-moz-user-drag: none;
		-ms-user-drag: none
	}
</style>