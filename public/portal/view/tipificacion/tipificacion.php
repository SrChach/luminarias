<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 mb-3 text-left">
			<input type="button" name="requestAsignation" value="Solicitar Asignación" class="btn btn-success">

		</div>
		<div class="col-md-12">
			<h4 class="float-left">FOLIO: <small id="txt_folio"></small></h4>
			<label class="float-right" id="labelcount" style="display:none;"></label>
		</div>
		<div class="col-md-12" id="contenedor_rows">
			<div class="row">

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var urlImages = "../../../storage/images/";
	var catalogos;
	var positionCurrent=0;
	var dataOrigen;
	$(()=>{
		var url = "./controller/LuminariasController.php";
		let objectSend = {opcion:'getLuminaria',csrf_token:'<?php echo $csrf_token ?>'};

		checkAsginaction();

		$('[name=requestAsignation]').click(function(event) {
			requestAsignation();
		});
	});

	checkAsginaction = () =>{
		load();
		var url = "./controller/GestionController.php";
		let objectSend = {opcion:'checkAsignacion',csrf_token:'<?php echo $csrf_token ?>'};
		let request={
			route: url,
			data:objectSend
		}
		Fetch(request).then((response)=>{
			//console.log(response);
			if (response.code==true) {
				$('[name=requestAsignation]').hide('slow');
				$('#labelcount').show('slow');
				//$('#countRegister').html(response.data.length);
				catalogos = response.catalogos;
				dataOrigen = response.data;
				makeView(dataOrigen);
				notificar('ALERTA!','Aún tiene trabajo pendiente','info')
				//console.log(catalogos);
			}else{
				notificar('ALERTA!','Puede solicitar más asignación de registros','info')
			}
			load(false);
		});
	}

	requestAsignation = () =>{
		var url = "./controller/GestionController.php";
		let objectSend = {opcion:'getAsignacion',csrf_token:'<?php echo $csrf_token ?>'};
		let request={
			route: url,
			data:objectSend
		}
		Fetch(request).then((response)=>{
			if (response.code==true) {
				$('[name=requestAsignation]').hide('slow');
				$('#labelcount').show('slow');
				//$('#countRegister').html(response.data.length);
				catalogos = response.catalogos;
				dataOrigen = response.data;
				makeView(dataOrigen);
			}
		});
	}

	var currentCountImageEvidencias=0;

	makeView = (data,position=0) =>{
		if (data.length==0) {
			notificar('INFORMACIÓN!','Por el momento no hay lotes por asignar. Intente más tarde','info')
			return;
		}

		var item = data[position];
		console.log(item);
		var register = item.generales;

		$('#labelcount').html(`${position+1} de ${data.length}`);

		//console.log('item',register);

		$('#txt_folio').html(register.FOLIO);

		let html_forms="";
		let html_previewImg="";
		let increment_img=0;
		let content="";
		$.each(item.images,function(index, image) {
			//console.log('image',image);
			if (item.type==1) {
				content = makeContentLuminaria(register);
			}else if (item.type==2) {
				content = "Falta crear modulos";
			}else if (item.type==3) {
				content = "Falta crear modulos";
			}
			if (image.TIPOIMAGEN=="EVIDENCIA") {
				currentCountImageEvidencias++;
				increment_img++
				html_forms+=`
				<div class="row row_content">
					<div class="col-md-7">
						<form action="javascript:_save('#data_${increment_img}');" class="card" id="data_${increment_img}" style="height:40rem;">
							<div class="card-header bg-dark">
								<h5 class="float-left">TIPO: <small id="tipo">${register.TIPO}</small> </h5>
							</div>
							<div class="card-body" id="content_data">
								<input type="hidden" name="csrf_token" value="<?php echo $csrf_token ?>">
								<input type="hidden" name="keyimg" value="${image.IDIMAGEN}">
								${content}
							</div>
							<div class="card-footer text-center">
								<button type="submit" name="button" class="btn btn-success">Guardar</button>
							</div>
						</form>
					</div>
					<div class="col-md-5">
					${makePreview(image,increment_img,item.images, register.COMENTARIOVALIDACIONLEVANTAMIENTO, register.OBSERVACIONLEVANTAMIENTO)}
					</div>
				</div>
				`;
				//increment_img++;
				//makePreview(image,increment_img,item.images);
			}
		});
		$('#contenedor_rows').html(html_forms);
		console.log('actual img evidencia',currentCountImageEvidencias);
	}

	makeContentLuminaria = (register) =>{
		console.log(register)
		let bodyContent = `	<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Tipo Ubicación:</span>
								</div>
								<select class="form-control" name="TIPOLUMINARIA" value="${register.KEYTIPOLUMINARIA}" disabled required>
									<option value="">Seleccione</option>
									<option value="1" ${register.KEYTIPOLUMINARIA==1 ? 'selected': ''}>Vía Primaria</option>
									<option value="2" ${register.KEYTIPOLUMINARIA==2 ? 'selected': ''}>Vía Secundaria</option>
								</select>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Ubicación</span>
								</div>
								<select class="form-control" name="UBICACIONFISICA" required disabled>
								${buildOptions(catalogos.ubicacionfisica,'NOMBRE','IDUBICACIONFISICA',register.IDUBICACIONFISICA)}
								</select>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Tipo Poste:</span>
								</div>
								<select class="form-control" name="TIPOPOSTE" required>
									${buildOptions(catalogos.tipoposte,'TIPOPOSTE','IDTIPOPOSTE')}
								</select>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Módelo</span>
								</div>
								<select class="form-control" name="MODELO" required>
								${(register.IDUBICACIONFISICA!=3) ? buildOptions(catalogos.extraubicacion[0],'NOMBRE','IDEXTRAUBICACIONFISICA', register.EXTRAUBICACIONFISICA) : buildOptions(catalogos.extraubicacion[3],'NOMBRE','IDEXTRAUBICACIONFISICA', register.EXTRAUBICACIONFISICA)}
								</select>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Tipo Lampara:</span>
								</div>
								<select class="form-control" name="TIPOLAMPARA" required>
									${buildOptions(catalogos.tecnologia,'NOMBRE','IDCARATERISTICATECNICA')}
								</select>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Tipo Carcaza:</span>
								</div>
								<select class="form-control" name="TIPOCARCAZA" required>
									${buildOptions(catalogos.carcaza,'TIPOCARCAZA','IDTIPOCARCAZA')}
								</select>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Watts:</span>
								</div>
								<select class="form-control" name="WATTS" required>
									${buildOptions(catalogos.watts,'VALOR','IDWATTS')}
								</select>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Tipo Balastro:</span>
								</div>
								<select class="form-control" name="TIPOBALASTRO" required>
									${buildOptions(catalogos.balastro,'TIPOBALASTRO','IDTIPOBALASTRO')}
								</select>
							</div>
							`;
		return bodyContent;
	}
	setTipoLampara = (_this) =>{
		let select = $(_this).parents('.card-body').find('[name=TIPOLAMPARA]')
		select.html(buildOptions(catalogos.tipolampara[_this.value],'NOMBRETIPOLAMAPARA','IDTIPOLAMPARA'));
	}

	makePreview = (image,index,images, comentario, observaciones) =>{
		let body_refs = "";
		$.each(images,function(index, image) {
			if (image.TIPOIMAGEN!="EVIDENCIA") {
				body_refs += `<a href="${urlImages+image.URLIMAGEN}" target="_blank"><img src="${urlImages+image.URLIMAGEN}" class="page-link d-block img-thumbnail m-0 mt-1" style="width:100px;height:100px;padding:0.20rem;" /></a>`;
			}
		});

		let body_preview = `
		<div class="card" style="height:40rem;">
			<div class="card-header bg-dark">
				<h5 class="float-left">IMÁGEN</h5>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-9 border-right">
						<div class="row" align="center">
							<div class="col-md-12">
								EVIDENCIA ${index}
							</div>
							<div class="col-md-12">
								<a href="${urlImages+image.URLIMAGEN}" target="_blank">
									<img style="height:400px;width:400px;" src="${urlImages+image.URLIMAGEN}" alt="First slide">
								</a>
								<div class="col-md-12">
									<p><span>Comentarios: </span>"${comentario}"</p>
								</div>
								<div class="col-md-12">
									<p><span>Observaciones: </span>"${observaciones}"</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
					<h4>Referencias</h4>
					${body_refs}
					</div>
				</div>
			</div>
		</div>`;

		return body_preview;
	}


	makeCarouselImg = (data) =>{
		let images = data.imagenes;
		let key = data.key;
		let url = data.url;
		let elemento = data.elemento;


		if (images.length>0) {
			$('[name=keyimg]').val(images[0].IDIMAGEN);
		}

		let listadoImages = '';
		//images=null,key=null,url=null
		let bodyCarousel=`
				<div class="card">
					<div class="card-header bg-dark">
						<h5 class="float-left">IMÁGENES</h5>
						<span class="float-right" id="countCarousel">1/<b></b></span>
					</div>
					<div class="card-body">
						<div id="carouselImages_EVidencias" class="carousel slide" data-ride="carousel" data-interval="false">
  							<div class="carousel-inner">`;

		var active='';
		let to;
		var count_ref=0, count_evi=0;
		$.each(images,function(index, image) {
			//console.log(image);
			//console.log('url',url+image[key]);
			active = "active";
			//console.log(index);
			if (index!=0) {
				active = "";
			}
			to=0;
			if (index<images.length) {
				to = index;
			}
			if (image.TIPOIMAGEN=="EVIDENCIA") {
				count_evi++;
				bodyCarousel += `<div class="carousel-item ${active} text-center" data-slide-to="${to}">
									<div class="row">
										<div class="col-md-12">
											EVIDENCIA ${count_evi}
										</div>
										<div class="col-md-12">
											<a href="${url+image[key]}" target="_blank">
												<img style="height:300px;width:300px;" src="${url+image[key]}" alt="First slide">
											</a>
										</div>
									</div>
								</div>`;
			}else{
				count_ref++;
				//console.log(active);
				listadoImages += `
				<li class="page-item ${active} hover ml-1 mr-1" data-target="#carouselImages_EVidencias" data-slide-to="${to}">
						Referencia ${count_ref}
					<img onclick="_setAtiveImgCarorusel(this)" src="${url+image[key]}" alt="Slide" class="page-link d-block img-thumbnail m-0" style="width:100px;height:100px;padding:0.20rem;">
				</li>
				`;
			}


		});

		bodyCarousel += `	</div>
							<!--a class="carousel-control-prev" href="#carouselImages_EVidencias" role="button" data-slide="prev" onclick="_setActiveImage(this,false)">
								<span class="btn btn-secondary btn-sm" aria-hidden="true">
									<i class="carousel-control-prev-icon"></i>
								</span>
							</a>
							<a class="carousel-control-next" href="#carouselImages_EVidencias" role="button" data-slide="next" onclick="_setActiveImage(this,true)">
								<span class="btn btn-secondary btn-sm" aria-hidden="true">
									<i class="carousel-control-next-icon"></i>
								</span>
							</a-->
						</div>
					</div>
					<div class="card-footer p-0">
						<nav class="navbar navbar-light m-0 table-responsive p-0" style="height:110px;border-bottom-left-radius:0.25rem;border-bottom-right-radius:0.25rem" id="img-list">
							<ul class="pagination pagination-sm justify-content-center m-0 pl-3 pr-3 img-list">
								${listadoImages}
					        </ul>
					    </nav>
					</div>
				</div>`;

		//return bodyCarousel;

		elemento.html(bodyCarousel);
		$('#countCarousel').html(`1/<b>${count_evi}</b>`);
	}

	_setActiveImage = (_this,next=true) =>{
        //e.preventDefault();
        let active = (next===true) ? $(_this).parents('.carousel').find('.carousel-item.active').next().data() : $(_this).parents('.carousel').find('.carousel-item.active').prev().data();

		//console.log(active);

        $('ul.img-list').find('li').each(function(index, elemento) {
            $(elemento).removeClass('active elevation-3')
        });
        let _of=0,from=0;

        if (active===undefined) {
            if (next) {
                _of = 1;
                from = $(_this).parents('.carousel').find('.carousel-item').length;
                $(`li[data-slide-to=0]`).addClass('active elevation-3');
            }else{
                _of = $(_this).parents('.carousel').find('.carousel-item').length;
                from = $(_this).parents('.carousel').find('.carousel-item').length;
                $(`li[data-slide-to=${$(_this).parents('.carousel').find('.carousel-item').length-1}]`).addClass('active elevation-3');
            }
        }else{
            _of = active.slideTo + 1;
            from = $(_this).parents('.carousel').find('.carousel-item').length;
            $(`li[data-slide-to=${active.slideTo}]`).addClass('active elevation-3');
        }

		$('#countCarousel').html(`${_of}/${from}`);

    }

	var current_filter=0;
	_setAtiveImgCarorusel=(_this)=>{
        //e.preventDefault();
        let scrollTo = "-=100px";
        let li = $(_this).parents('li');
        $(_this).parents('ul').find('li').each(function(index, elemento) {
            $(elemento).removeClass('active elevation-3')
        });
        //console.log(current_filter,li.data().slideTo);
        if (current_filter === li.data().slideTo) {
            scrollTo = "+=0px";
        }
        if (current_filter < li.data().slideTo) {
            scrollTo = "+=100px";
        }
        //console.log(scrollTo);
        $('#img-list').animate({
            scrollLeft: scrollTo
        }, "slow");

        li.addClass('active elevation-3');

		let _of = li.data().slideTo + 1;
		let from = $('.carousel').find('.carousel-item').length;

		$('#countCarousel').html(`${_of}/${from}`);

        current_filter = li.data().slideTo;
    }


	_save = (_this) =>{
		//console.log(_this);
		let jsonSend = serializeFormJSON($(_this));
		jsonSend.opcion = "saveData";

		var url = "./controller/GestionController.php";
		let request={
			route: url,
			data:jsonSend
		}

		let index_img = _this.split("_")[1];

		Fetch(request).then((response)=>{
			//console.log(response);
			if (response.codigo==true) {
				//console.log('actual img evidencia after save',currentCountImageEvidencias);
			}else{
				notificar('Lo siento!','No se pudo tipificar la evidencia '+index_img,'warning');
				console.error(response.message);
			}
		});

		currentCountImageEvidencias--;
		//console.log('actual img evidencia after save',currentCountImageEvidencias);
		$(_this).parents('.row_content').hide('slow');
		if (currentCountImageEvidencias==0) {
			positionCurrent = positionCurrent+1;

			if (dataOrigen.length==positionCurrent) {
				notificar('Bien !', 'Has Concluido con tu lote. Ya puedes solicitar más asignaciones','info');
				$('[name=requestAsignation]').show('slow');
				$('#labelcount').html("");
				$('#txt_folio').html("")
			}else{
				makeView(dataOrigen,positionCurrent);
			}

		}

	}




</script>
