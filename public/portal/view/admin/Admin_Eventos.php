<style>
    /* _________________ Radio Buttons styles _____________*/
    .switch {
        position: relative;
        display: inline-block;
        width: 50px;
        height: 14px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 20px;
        width: 20px;
        left: -4px;
        bottom: -3px;
        background-color: white;
        border: 1px solid #007bff;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(35px);
        -ms-transform: translateX(35px);
        transform: translateX(35px);
    }
</style>

<style>
    .ui-sortable, .ui-sortable-2{
        width: auto;        
        padding: 10px 5px;
        border-radius: .35rem;
        background: rgb(214,214,214);
        background: -moz-linear-gradient(top, rgba(214,214,214,1) 0%, rgba(201,201,201,1) 50%, rgba(232,232,232,1) 100%);
        background: -webkit-linear-gradient(top, rgba(214,214,214,1) 0%,rgba(201,201,201,1) 50%,rgba(232,232,232,1) 100%);
        background: linear-gradient(to bottom, rgba(214,214,214,1) 0%,rgba(201,201,201,1) 50%,rgba(232,232,232,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d6d6d6', endColorstr='#e8e8e8',GradientType=0 );
    }

    .ui-sortable{
        min-height: 450px;
    }

    .ui-sortable-2{        
        min-height: 395px;        
    }

    /* .moveTipoEventoBtn{
        position: absolute;
        right: 70%;
        top: 35%;
        max-width: 50%;
    }
    .moveFasesBtn{
        position: absolute;
        z-index: 1;
        top: 25%;
        left: 75%;
        max-width: 50%;
    }
    .moveEtiquetaBtn{
        position: absolute;
        z-index: 1;
        bottom: 20%;
        right: 75%;
        max-width: 50%;
    } */


    #sortTypeEvent, #sortTypeEventCheck, #sortFases, #sortFasesCheck {         
        height: 450px;
    }
    #sortTypeEvent, #sortTypeEventCheck, #sortFases, #sortFasesCheck, #sortLabelProg, #sortLabelProgCheck {         
        list-style-type: none;
        border-radius: .35rem; 
        padding: 10px 5px;        
        overflow-y: scroll;
        overflow-x: scroll;
        font-size: .9rem;
        background: rgb(214,214,214); /* Old browsers */
        background: -moz-linear-gradient(top, rgba(214,214,214,1) 0%, rgba(201,201,201,1) 50%, rgba(232,232,232,1) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, rgba(214,214,214,1) 0%,rgba(201,201,201,1) 50%,rgba(232,232,232,1) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, rgba(214,214,214,1) 0%,rgba(201,201,201,1) 50%,rgba(232,232,232,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d6d6d6', endColorstr='#e8e8e8',GradientType=0 ); /* IE6-9 */
    }
    #sortTypeEvent > li:hover, 
    #sortTypeEventCheck > li:hover, 
    #sortFases > li:hover,         
    #sortFasesCheck > li:hover,         
    #sortLabelProg > li:hover,
    #sortLabelProgCheck > li:hover{         
        background-color: #71b3d8;
        cursor: pointer;
        color: white;   
    }

    #sortTypeEvent li, 
    #sortTypeEventCheck li, 
    #sortFases li,
    #sortFasesCheck li,
    #sortLabelProg li,
    #sortLabelProgCheck li{
        margin: 0 5px 5px 5px;
        padding: 5px 10px 5px 10px;        
        font-size: 1.2em;
        border-radius: .25rem;
        background-color: white;
        width: -webkit-fill-available; 
        float: right;                     
    }

    .typeEventCkeck{
        border-top-right-radius: 0 !important; 
    }

    .btn-width-50{
        display: block;
        /* width: 50%; */
        margin-left: auto;        
    }

    .btnSaveEvt{
        border-bottom: none;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    .itemCkeck{
        color: #12c512;
        margin: 5px;
    }

    .checkTypeEventStyle{
        float: right;        
        transform: translateY(10%);
    }

    .bloqueo{
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;       
        overflow: hidden;
        outline: 0;        
        position: fixed;
        /* background-color: rgba(154, 161, 164, 0.2); */
        background-color: rgba(154, 161, 164, 0.2);
        z-index: 10;
        display : none;
    }   
    /* .loaderStandar > i{
        color: #2bb273;
    } */
    .loaderStandar{
        margin: auto;
        vertical-align: middle;        
        font-size: 6rem;
        /* color: #2bb273; */
        color: #28A6DE;
        position: absolute;
        top: 45%;
        width : 100%;
        text-align : center;
        
        /* margin-left: 42.5%; */
        /* margin-right: 42.5%; */
    }

</style>

<div class="bloqueo text-center">
    <div class="loaderStandar text-center">        
        <h4 class="text-center bold" style="color: #2bb273;">Cargando ...</h4>
        <i class="fa fa-spinner fa-spin" style="color: #28A6DE;"></i>
    </div>
</div>
<div class="container-fluid">
    <div class="w-100 text-center"><h2>Administrador Eventos</h2></div>

    <div class="row my-3 mx-5 justify-content-end">
        <div class="col-lg-3 px-3">            
        </div>
    </div>

    <section class="my-3">
        <div class="row">
            <div class="col-lg-4 px-3 pb-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span for="" class="input-group-text">Tipo Evento</span>
                    </div>
                    <input type="text" id="nuevoTipoEvento" class="form-control" placeholder="Tipo Evento">                    
                </div>
                
            </div>
            <div class="col-lg-4 px-3 pb-3">                
                    <button class="btn btn-success" onclick="newTypeEvent();">Nuevo Tipo Evento</button>                
            </div>
            <div class="col-lg-4 px-3 pb-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span for="" class="input-group-text">Programas</span>
                    </div>
                    <select name="" id="selectPrograma" class="form-control" onchange="getOpsProgramTypeEvt();">
                        <option value="">PROGRAMA...</option>
                    </select>
                </div>
            </div>
        </div>
    </section>

    <section class="my-4 tipoEvento">
        <div class="row">
            <div class="col-sm-6 col-lg-4 px-3 offset-lg-2">
                <div class="row justify-content-center">
                    <h3 class="">Tipo Eventos</h3>
                </div>
                <ul id="sortTypeEvent" class="connectedTypeEvt px-3 ui-sortable"></ul>
            </div>            
            <div class="col-sm-6 col-lg-4 px-3">
                <div class="mt-1">
                    <button class="btn btn-primary btn-width-50 btnSaveEvt" onclick="saveProgramasTpEvt();">Guardar Cambios</button>
                </div>
                <ul id="sortTypeEventCheck" class="connectedTypeEvt px-3 ui-sortable typeEventCkeck">                    
                    <!-- <button class="btn btn-danger btn-block moveTipoEventoBtn">Piso Firme</button> -->
                </ul>
            </div>
        </div>
    </section>


    <section class="my-3">
        <div class="row">
            <div class="col-lg-4 px-3 pb-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span for="" class="input-group-text">Tipo Fase</span>
                    </div>
                    <input type="text" id="nuevaFase" class="form-control" placeholder="Tipo Fase">                    
                </div>
                
            </div>
            <div class="col-lg-4 px-3 pb-3">                
                    <button class="btn btn-success" onclick="newphase();">Nueva Fase</button>                
            </div>
            <div class="col-lg-4 px-3 pb-3"></div>

        </div>
    </section>

    <section class="">
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <div class="row">                    
                    <div class="col-sm-6 col-lg-6 px-3">
                        <div class="row justify-content-center">
                            <h3 class="">Fases</h3>
                        </div>                    
                        <ul id="sortFases" class="connectedPhases ui-sortable  px-3"></ul>
                    </div>
                    <div class="col-sm-6 col-lg-6 px-3">
                        <div class="mt-1">
                            <button class="btn btn-primary btn-width-50 btnSaveEvt" onclick="savePhasesProgTpEvt();">Guardar Cambios</button>
                        </div>                                          
                        <ul id="sortFasesCheck" class="connectedPhases ui-sortable typeEventCkeck"></ul>
                    </div>
                </div>                
            </div>
            <div class="col-sm-12 col-lg-6" style="align-content: end;display: grid;">
                <div class="row" style="align-content: flex-end;">
                    <div class="col-lg-12 text-end">
                        <!-- <div class="row justify-content-end">
                            <div class="col-lg-6 px-3">                            
                                <h3 class="text-center">Etiqueta</h3>
                                <div class="form-group">
                                    <select name="" id="groupsLabelProg" class="form-control" onchange="getOpsGroupLabelProg();">
                                        <option value="">Seleccione...</option>                                        
                                    </select>
                                </div>                    
                            </div>                        
                        </div> -->
                    </div>
                    <div class="col-sm-6 col-lg-6 px-3">
                        <div class="mt-1">
                            <button class="btn btn-primary btn-width-50 btnSaveEvt" onclick="saveLabelPhaseProg();">Guardar Cambios</button>
                        </div>                                        
                        <ul id="sortLabelProgCheck" class="ui-sortable-2"></ul>
                    </div>
                    <!-- <div class="col-sm-6 col-lg-6 px-3" style="display: grid;align-content: end;">                         -->
                    <div class="col-sm-6 col-lg-6 px-3" >
                        <div class="mt-1">
                            <div class="btn">&nbsp;</div>
                        </div>
                        <ul id="sortLabelProg" class="ui-sortable-2 px-3"></ul>
                    </div>
                </div>                
            </div>            
        </div>
    </section>

   
   <!-- <label class="switch align-bottom">
                                            <input type="radio" name="checkDay">
                                            <span class="slider round"></span>  
                                        </label>
                                                 -->
</div>

<script>
    var URL = './controller/adminTipoEventos_controller.php';
    var objectTipoEvento = new Object();
    var objectProgTipoEvt = new Object();    
    var objectFases = new Object();    
    var objectFaseProgTypEvt = new Object();    
    var labelsProgram = new Object();    
    var labelsPhaseProgram = new Object();    

    $(function(){
        getTypeEvents(true);
        getPrograms();
        getProgramTypeEvt(true);
        getPhases(true);
        getPhaseProgTypEvt(true);
        getLabelsProgram();
        getLabelsProgramCheked(true);

        $('#sortTypeEvent').sortable({
            connectWith : '.connectedTypeEvt',            
            over : function(event, ui){
                let classElement = $(ui.item).hasClass('no-moveSortTypeEvent');

                if(classElement){
                    $(ui.sender).sortable('cancel');
                }
            }            
        });

        $('#sortTypeEventCheck').sortable({
            connectWith : '.connectedTypeEvt',
            cancel : '.opCancel'            
        });
        


        $('#sortFases').sortable({
            connectWith : '.connectedPhases',
            over : function(event, ui){
                let classElement = $(ui.item).hasClass('no-movePhases');

                if(classElement){
                    $(ui.sender).sortable('cancel');
                }
            }            
        });       

        $('#sortFasesCheck').sortable({
            connectWith : '.connectedPhases',
            cancel : '.opCancel',
        });


        $('#sortLabelProgCheck').sortable({
            connectWith : "#sortLabelProg",
            cancel : '.opCancel'
        });
        
        $('#sortLabelProg').sortable({
            connectWith : "#sortLabelProgCheck"            
        });
       
        
    });

    /* Obtencion de la informacion */
    function getTypeEvents(render = false){
        if(render == false){
            $('#sortTypeEvent').html(construcItemsOps(objectTipoEvento, 'IDTIPOEVENTO', 'TIPOEVENTO', 'typeEvt', '', ''));
        }else{                    
            let metodo = 'POST';
            let objectSend = {
                option : 'typeEvents'            
            }

            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                let response = JSON.parse(res);
                objectTipoEvento = response;
                $('#sortTypeEvent').html(construcItemsOps(response, 'IDTIPOEVENTO', 'TIPOEVENTO', 'typeEvt', '', ''));
                getOpsProgramTypeEvt();
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }
    }

    function getPrograms(){
        let metodo = 'POST';
        let objectSend = {
            option : 'programs'            
        }

        let request = supportRequest(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);            
            $('#selectPrograma').html(construcOpsCombo(response, 'IDPROGRAMA', 'NOMBREPROGRAMA'));            
            $('.bloqueo').fadeOut('slow');
        });

        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }

    function getPhases(render = false){
        if(!render){            
            if(!jQuery.isEmptyObject(objectFases))$('#sortFases').html(construcItemsOps(objectFases, 'IDFASE', 'FASE', 'phase', '', ''));
        }else{
            let metodo = 'POST';
            let objectSend = {
                option : 'phases'
            }
            
            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){                
                if(res != null && res != ''){
                    let response = JSON.parse(res);            
                    $('#sortFases').html(construcItemsOps(response, 'IDFASE', 'FASE', 'phase', '', ''));
                    objectFases = response;
                    getOpsPhaseProgTypeEvt();
                }    
                $('.bloqueo').fadeOut('slow');
            });
            
            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }
    }

    function getLabelsProgram(){
        let metodo = 'POST';
        let objectSend = {
            option : 'labelsProgram'
        }

        let request = supportRequest(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){       
            let response = JSON.parse(res);
            labelsProgram = response;
            $('#sortLabelProg').html(construcItemsOps(labelsProgram['PORCENTAJE'], 'IDETIQUETA', 'ETIQUETA', 'labelProg', classItem = '', classIcon = ''));
            // let options = '<option value="">Seleccione...</option>';
            // Object.keys(response).forEach(function(key, count) {                
            //     options += '<option value="'+key+'">Grupo '+(++count)+' ('+key+')</option>';
            // });
            // $('#groupsLabelProg').html(options);
            $('.bloqueo').fadeOut('slow');
        });
        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }
    


    function newTypeEvent(){
        let tipoEvento = $('#nuevoTipoEvento').val();
        if(tipoEvento != '' && tipoEvento != null){
            let metodo = 'POST';
            let objectSend = {
                option : 'newEvent',
                typeEvent : tipoEvento
            }

            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                // let response = JSON.parse(res);
                if(res == true || res == 'true'){
                    alert('Nuevo Tipo de Evento Guardado');
                    getTypeEvents(true);
                }   
                $('.bloqueo').fadeOut('slow');     
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }
    }

    function newphase(){
        let phase = $('#nuevaFase').val();
        if(phase != ''){
            let metodo = 'POST';
            let objectSend = {
                option : 'newPhase',
                phase : phase
            }
            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res == true || res == 'true'){
                    alert("Fase agregada Correctamente");
                    $('#nuevaFase').val('');
                    getPhases(true);                    
                }   
                $('.bloqueo').fadeOut('slow');                             
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });

        }else{
            alert('Escribe el nombre de la fase');
        }        
    }


    function getPhaseProgTypEvt(render = false, renderKey = false, phaseRecharge = ''){
        if(render){
            let metodo = 'POST';
            let objectSend = {
                option : 'phaseProgTypeEvt'
            }

            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                let response = JSON.parse(res);
                objectFaseProgTypEvt = response;
                $('.bloqueo').fadeOut('slow');                
            });

            request.fail(function(jqXHR, textStatus){
                console.log('error', jqXHR, textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }else if(renderKey && phaseRecharge != '' && phaseRecharge != null){
            let metodo = 'POST';
            let objectSend = {
                option : 'phaseProgEventUnique',
                findProgTypEvt : phaseRecharge
            }
            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                let response = JSON.parse(res);
                if(response == false || response == 'false'){
                    alert('Error al sincronizar la información con la base de datos');
                }else{
                    objectFaseProgTypEvt[phaseRecharge] = response;
                    getOpsPhaseProgTypeEvt();
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }
    }

    function getProgramTypeEvt(render = false, renderKey = false, programRecharge = ''){        
        if(render){
            let metodo = 'POST';
            let objectSend = {
                option : 'programTypeEvent'
            }
            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show('slow');
            request.done(function(res){
                let response = JSON.parse(res);
                objectProgTipoEvt = response;
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }else if(renderKey && programRecharge != '' && programRecharge != null){
            let metodo = 'POST';
            let objectSend = {
                option : 'programTypeEventUnique',
                findProgramTypeEvt : programRecharge
            }
            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show('slow');
            request.done(function(res){
                let response = JSON.parse(res);
                if(response == false || response == 'false'){
                    alert('Error al sincronizar la información con la base de datos');
                }else{
                    objectProgTipoEvt[programRecharge] = response;
                    getOpsProgramTypeEvt();                                        
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }
    }


    function getOpsProgramTypeEvt(){
        let optionSelected = $('#selectPrograma').val();

        $('#sortTypeEvent').html(construcItemsOps(objectTipoEvento, 'IDTIPOEVENTO', 'TIPOEVENTO', 'typeEvt', '', ''));
        $('#sortFases').html(construcItemsOps(objectFases, 'IDFASE', 'FASE', 'phase', '', ''));
        $('#sortLabelProg').html(construcItemsOps(labelsProgram['PORCENTAJE'], 'IDETIQUETA', 'ETIQUETA', 'labelProg', classItem = '', classIcon = ''));

        if(optionSelected != '' && objectProgTipoEvt[optionSelected] != undefined && objectProgTipoEvt[optionSelected]){            
            objectProgTipoEvt[optionSelected].forEach(function(item){
                $('#sortTypeEvent li[data-typeEvt="'+item['IDTIPOEVENTO']+'"]').remove();
            });
            $('#sortTypeEventCheck').html(construcItemsOpsAction(objectProgTipoEvt[optionSelected], 'IDPROGRAMATIPOEVENTO', 'TIPOEVENTO', 'programTypeEvt', 'no-moveSortTypeEvent opCancel', 'checkProgTypEvt'));
            
            $('input[type="checkbox"][name="checkProgTypEvt"]').change(function(){                
                if($(this).is(':checked')){
                    $('input[type="checkbox"][name="checkProgTypEvt"]').prop('checked', false);
                    $(this).prop('checked', true);
                }                
                getOpsPhaseProgTypeEvt();
            });

        }else{
            $('#sortTypeEventCheck').html('');
            getPhases();
        }        
        $('#sortLabelProgCheck').html('');
        $('#sortFasesCheck').html('');
    }

    function getOpsPhaseProgTypeEvt(){
        let optionSelected = $('input[name="checkProgTypEvt"]:checked').val();
        $('#sortFases').html(construcItemsOps(objectFases, 'IDFASE', 'FASE', 'phase', '', ''));
        $('#sortLabelProg').html(construcItemsOps(labelsProgram['PORCENTAJE'], 'IDETIQUETA', 'ETIQUETA', 'labelProg', classItem = '', classIcon = ''));

        if(optionSelected != '' && optionSelected != undefined && optionSelected != null && objectFaseProgTypEvt[optionSelected]){
            objectFaseProgTypEvt[optionSelected].forEach(function(item){                
                $('#sortFases li[data-phase="'+item['IDFASE']+'"]').remove();
            });

            $('#sortFasesCheck').html(construcItemsOpsAction(objectFaseProgTypEvt[optionSelected], 'IDFASEPROGRAMAEVENTO', 'FASE', 'phaseProgTypeEvt', 'no-movePhases opCancel', 'checkPhasProgTpEvt'));

            $('input[type="checkbox"][name="checkPhasProgTpEvt"]').change(function(){
                if($(this).is(':checked')){
                    $('input[type="checkbox"][name="checkPhasProgTpEvt"]').prop('checked', false);
                    $(this).prop('checked', true);
                }
                getLabelsProgramCheked();
            });
        }else{
            $('#sortLabelProgCheck').html('');
            $('#sortFasesCheck').html('');
        }
    }

    function getLabelsProgramCheked(render = false){
        let optionsSelected = $('input[name="checkPhasProgTpEvt"]:checked').val();
        $('#sortLabelProg').html(construcItemsOps(labelsProgram['PORCENTAJE'], 'IDETIQUETA', 'ETIQUETA', 'labelProg', classItem = '', classIcon = ''));
        if(render){
            let metodo = 'POST';
            let objectSend = {
                option : 'labelPhaseProgram'
            }

            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show('slow');
            request.done(function(res){
                let response = JSON.parse(res);                
                labelsPhaseProgram = response;
                if(labelsPhaseProgram[optionsSelected] != undefined){
                    labelsPhaseProgram[optionsSelected].forEach(function(item){                
                        $('#sortLabelProg > li[data-labelprog="'+item["IDETIQUETA"]+'"]').remove();
                    });
                }
                $('#sortLabelProgCheck').html(construcItemsOps(labelsPhaseProgram[optionsSelected], 'IDETIQUETAPROGRAMA', 'ETIQUETA', 'labelPhasesProg', 'opCancel', 'itemCkeck fa fa-check'));                
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }else{
            // $('#sortLabelProg').html(construcItemsOps(labelsProgram['PORCENTAJE'], 'IDETIQUETA', 'ETIQUETA', 'labelProg', classItem = '', classIcon = ''));
            if(labelsPhaseProgram[optionsSelected] != undefined){
                labelsPhaseProgram[optionsSelected].forEach(function(item){                
                    $('#sortLabelProg > li[data-labelprog="'+item["IDETIQUETA"]+'"]').remove();
                });
            }
            $('#sortLabelProgCheck').html(construcItemsOps(labelsPhaseProgram[optionsSelected], 'IDETIQUETAPROGRAMA', 'ETIQUETA', 'labelPhasesProg', 'opCancel', 'itemCkeck fa fa-check'));            
        }
    }
    
    // function getOpsGroupLabelProg(){
    //     let optionSelected = $('#groupsLabelProg').val();
    //     if(optionSelected != '' && optionSelected != null && labelsProgram[optionSelected] != undefined){
    //         console.log('entro');
    //         $('#sortLabelProg').html(construcItemsOps(labelsProgram[optionSelected], 'IDETIQUETA', 'ETIQUETA', 'labelProg', classItem = '', classIcon = ''));
    //     }else{
    //         $('#sortLabelProg').html('');
    //     }
    // }

    // function getOpsLabelProgram(){
    //     let optionsSelected = $('input[name="checkPhasProgTpEvt"]:checked').val();
        
    //     // Reconstruir las opciones
    //     alert(optionsSelected);
    //     // if(optionSelected != '' && op)
    // }


    function saveProgramasTpEvt(){
        let clavePrograma = $('#selectPrograma').val();
        if(clavePrograma != ''){
            let metodo = 'POST';
            let objectSend = {
                option : 'saveProgramTypeEvt',
                claveProgram : clavePrograma,
                listItemsSave : []
            }

            $('#sortTypeEventCheck > li[data-typeEvt]').each(function(){                
                objectSend.listItemsSave.push($(this).attr('data-typeEvt'));
            });            

            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res == true || res == 'true'){
                    alert('Tipo Eventos Guardados Correctamente');
                    /* Limpiar los elementos seleccionados */
                    $('#sortTypeEvent').html(construcItemsOps(objectTipoEvento, 'IDTIPOEVENTO', 'TIPOEVENTO', 'typeEvt', '', ''));
                    $('#sortLabelProg').html(construcItemsOps(labelsProgram['PORCENTAJE'], 'IDETIQUETA', 'ETIQUETA', 'labelProg', classItem = '', classIcon = ''));
                    $('#sortFases').html(construcItemsOps(objectFases, 'IDFASE', 'FASE', 'phase', '', ''));
                    $('#sortTypeEventCheck').html('');
                    $('#sortLabelProgCheck').html('');
                    $('#sortFasesCheck').html('');
                    getProgramTypeEvt(false, true, clavePrograma+'');
                }else{
                    alert('Error al guardar los Tipo de Evento');
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });

        }else{
            alert('Selecciona un programa');
        }
    }

    function savePhasesProgTpEvt(){
        var claveProgTipoEvento = $('input[name="checkProgTypEvt"]:checked').val();              
        if(claveProgTipoEvento != undefined && claveProgTipoEvento != '' && claveProgTipoEvento != null){
            let metodo = 'POST';
            let objectSend = {
                option : 'savePhaseProgTpEvt',
                claveProgramTypeEvt : claveProgTipoEvento,
                listItemsSave : []
            }

            $('#sortFasesCheck > li[data-phase]').each(function(){
                objectSend.listItemsSave.push($(this).attr('data-phase'));                
            });

            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res == true || res == 'true'){
                    alert('Fases Guardadas Correctamente');
                    /* Limpiar las opciones seleccionadas */
                    $('#sortLabelProg').html(construcItemsOps(labelsProgram['PORCENTAJE'], 'IDETIQUETA', 'ETIQUETA', 'labelProg', classItem = '', classIcon = ''));
                    $('#sortLabelProgCheck').html('');
                    getPhaseProgTypEvt(false, true, claveProgTipoEvento);
                }else{
                    alert('Error al guardas las Fases');
                }
                $('.bloqueo').fadeOut('slow');                
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });        
        }else{
            alert('Seleccione un tipo de evento');
        }
    }


    function construcItemsOps(objectData, valueKey, textValue, targetItem, classItem = '', classIcon = ''){
        let ops = '';        

        if(objectData != undefined){
            if(objectData != '' && objectData != null && Object(objectData).length > 0){
                objectData.forEach(function(item){
                    ops += '<li class="ui-state-default '+classItem+'" data-order=""  data-'+targetItem+'="'+item[valueKey]+'">\
                                <i class="iconSortable '+classIcon+'" aria-hidden="true"></i>'+item[textValue]+'\
                            </li>';
                });
            }
        }

        return ops;
    }

    function saveLabelPhaseProg(){
        let optionsSelected = $('input[name="checkPhasProgTpEvt"]:checked').val();

        if(optionsSelected != null && optionsSelected != '' && optionsSelected != undefined){
            let metodo = 'POST';
            let objectSend = {
                option : 'newLabelsPhaseProg',
                clavePhaseProgEvt : optionsSelected,
                listItems : []
            }
            
            $('#sortLabelProgCheck > li[data-labelprog]').each(function(){                
                objectSend.listItems.push($(this).attr('data-labelprog'));
            });

            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res == true || res == 'true'){
                    getLabelsProgramCheked(true);
                    alert('Cambios Guardados Correctamente');
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            })
        }else{
            alert('Selecciona alguna fase');
        }
    }
    function construcItemsOpsAction(objectData, valueKey, textValue, targetItem, classItem = '', actionName=''){
        let ops = '';
        if(objectData != undefined){
            objectData.forEach(function(item){
                ops += '<li class="ui-state-default '+classItem+'" data-order=""  data-'+targetItem+'="'+item[valueKey]+'">\
                            <i class="itemCkeck fa fa-check" aria-hidden="true"></i>\
                            <span style="word-break: break-word;">'+item[textValue]+'\
                            <label style="margin: 10px 0;" class="switch align-bottom checkTypeEventStyle">\
                                <input type="checkbox" name="'+actionName+'" value="'+item[valueKey]+'">\
                                <span class="slider round"></span>\
                            </label></span>\
                        </li>';
            });
        }
        return ops;
    }
    
    function construcOpsCombo(objectData, key, value){        
        let result = '<option value="">Programas</option>';        
        if(objectData != undefined && Object.keys(objectData).length > 0){
            objectData.forEach(function(item){
                result += '<option value="'+item[key]+'">'+item[value]+'</option>';
            });
        }
        return result;
    }
    function supportRequest(objectData, metodo, URL){
        return $.ajax({
            url : URL,
            method: metodo,
            data : objectData
        });
    }
</script>