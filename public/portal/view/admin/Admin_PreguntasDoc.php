<style>
    #sortable1, #sortable2, #sortable3 {         
        list-style-type: none;
        border-radius: .35rem; 
        padding: 10px 5px;
        height: 450px;
        overflow-y: scroll;
        overflow-x: scroll;
        background: rgb(214,214,214); /* Old browsers */
        background: -moz-linear-gradient(top, rgba(214,214,214,1) 0%, rgba(201,201,201,1) 50%, rgba(232,232,232,1) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, rgba(214,214,214,1) 0%,rgba(201,201,201,1) 50%,rgba(232,232,232,1) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, rgba(214,214,214,1) 0%,rgba(201,201,201,1) 50%,rgba(232,232,232,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d6d6d6', endColorstr='#e8e8e8',GradientType=0 ); /* IE6-9 */
    }
    #sortable1 > li:hover, #sortable2 > li:hover, #sortable3 > li:hover {         
        background-color: #71b3d8;
        cursor: pointer;
    }
    #sortable1 li, #sortable2 li, #sortable3 li {
        margin: 0 5px 5px 5px;
        padding: 5px;
        font-size: 1.2em;
        border-radius: .25rem;
        background-color: white;
    }

    .formatLabelList > div{
        min-width: 145px;
    }

    .iconSortable{
        float: right;
        margin: 3px 15px 3px 0;
    }
    .bloqueo{
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;       
        overflow: hidden;
        outline: 0;        
        position: fixed;
        background-color: rgba(154, 161, 164, 0.2);
        z-index: 10;
        display : none;
    }   
    /* .loaderStandar > i{
        color: #2bb273;
    } */
    .loaderStandar{
        margin: auto;
        vertical-align: middle;        
        font-size: 6rem;
        /* color: #2bb273; */
        color: #28A6DE;
        position: absolute;
        top: 45%;
        width : 100%;
        text-align : center;       
    }

</style>

<div class="bloqueo text-center">
    <div class="loaderStandar text-center">        
        <h4 class="text-center bold" style="color: #2bb273;">Cargando ...</h4>
        <i class="fa fa-spinner fa-spin" style="color: #28A6DE;"></i>
    </div>
</div>

<div class="container-fluid">
    <section class="filters mx-3">
        <div class="row">
            <h3 class="text-center w-100">Administrador Preguntas Documento</h3>
        </div>    
        <div class="row">
            <div class="col-md-4 col-lg-3 mr-auto">                
                <div class="col-auto my-3">                        
                    <div class="input-group mb-2">
                        <div class="input-group-prepend formatLabelList">
                            <div class="input-group-text">Categoria</div>
                        </div>
                        <select name="" id="selectCategorias" class="form-control" onchange="getOpsTypeImg();">
                            <option value="" selected>Selecciona</option>
                        </select>
                    </div>
                </div>                
            </div>
            <div class="col-md-4 col-lg-3 offset-lg-1 ml-auto mr-auto">                                      
                <div class="col-auto my-3">                        
                    <div class="input-group mb-2">
                        <div class="input-group-prepend formatLabelList">
                            <div class="input-group-text">Tipo Imagen</div>
                        </div>
                        <select name="" id="selectTipoImg" class="form-control" onchange="getOpsTypeDoc();">
                            <option value="" selected>Selecciona</option>
                        </select>
                    </div>
                </div>                
            </div>
            <div class="col-md-4 col-lg-3 offset-lg-1 ml-auto">                                                     
                <div class="col-auto my-3">                        
                    <div class="input-group mb-2">
                        <div class="input-group-prepend formatLabelList">
                            <div class="input-group-text">Tipo Documento</div>
                        </div>
                        <select name="" id="selectTipoDoc" class="form-control" onchange="getOpsCuestTypeDoc();">
                            <option value="" selected>Selecciona</option>                            
                        </select>
                    </div>
                </div>                
            </div>                                               
        </div>    
    </section>
    
    <section class="sortAblesContent mt-3 mx-2">
        <div class="row">
            <div class="col-md-12">
                <div class="row justify-content-end">
                    <div class="col-md-3 px-5 my-3">
                        <button class="btn btn-primary btn-block" onclick="saveChangesCuest();">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center">
                <div class="container-fluid">
                    <h3>Preguntas</h3>
                </div>
                <ul id="sortable1" class="connectedSortable mx-3">
                    <!-- <li class="ui-state-default no-moveLast">Item 1</li>                     -->
                </ul>
            </div>
            
            <div class="col-lg-4 text-center">
                <div class="container-fluid">
                    <h3>Activas</h3>
                </div>
                <ul id="sortable2" class="connectedSortable mx-3 connectedSortable2">                    
                </ul>
            </div>
    
            <div class="col-lg-4 text-center">
                <div class="container-fluid">
                    <h3>Inactivas</h3>
                </div>
                <ul id="sortable3" class=" connectedSortable2 mx-3">                    
                </ul>
            </div>
        </div>
    </section>

</div>

<script>
    var URL = "../../public/portal/controller/adminPreguntasDoc_controller.php";
    var objectTypeImg = new Object();
    var objectTypeDoc = new Object();
    var objectAllCuestions = new Object();
    var objectAllCuestTypeDoc = new Object();

    const quickSort = ( [ x = [], ...xs ] ) => {
        return ( x.length === 0 ) ? [] : [
            ...quickSort( xs.filter( y => y <= x ) ),
            x,
            ...quickSort( xs.filter( y => y > x ) )
        ];
    }
    // const result = quickSort(arr);

    $(function(){
        getCategories();
        getTypeImag();
        getTypeDocs();
        getCuestions();
        getCuestionsTypeDoc();


        $("#sortable1").sortable({      
            connectWith: ".connectedSortable",
            over: function(event, ui){
                let classElement = $(ui.item).hasClass('no-moveFirst');

                if(classElement){
                    $(ui.sender).sortable('cancel');
                }       
            }      
        });

        $("#sortable2").sortable({
            connectWith: ".connectedSortable, .connectedSortable2",
            cancel: ".no-move",
            over: function(event, ui){
                let classElement = $(ui.item).hasClass('no-moveMiddle');

                if(classElement){
                    $(ui.sender).sortable('cancel');
                }       
            }
        });

        $("#sortable3").sortable({
            connectWith: ".connectedSortable2",
            cancel: ".no-move",
            over: function(event, ui){
                let classElement = $(ui.item).hasClass('no-moveLast');

                if(classElement){
                    $(ui.sender).sortable('cancel');
                }       
            }
        });    
    });

    function getCategories(){    
        let metodo = 'POST';
        let objectSend = {
            option : 'categories'
        };

        let request = supportRequest(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);            
            $('#selectCategorias').html(constructOps(response, 'IDCATEGORIA', 'CATEGORIA'));
            $('.bloqueo').fadeOut('slow');
        });        

        request.fail(function(jqXHR, textStatus){   
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }

    function getTypeImag(){        
        var metodo = 'POST';        
        let objectSend = {
            option : 'typeImages'                    
        }
        let request = supportRequest(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);
            objectTypeImg = response;
            $('.bloqueo').fadeOut('slow');
        });

        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });        
    }

    function getTypeDocs(){
        let metodo = 'POST';
        let objectSend = {
            option : 'typeDocuments'
        }
        let request = supportRequest(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);
            objectTypeDoc = response; 
            $('.bloqueo').fadeOut('slow');           
        });

        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }

    function getCuestions(){
        let metodo = 'POST';
        let objectSend = {
            option: 'cuestions'
        }

        let request = supportRequest(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);
            objectAllCuestions = response;
            $('#sortable1').html(construcItemsOps(response, 'IDPREGUNTA', 'PREGUNTA', 'cuestions', 'no-moveLast'));
            $('.bloqueo').fadeOut('slow');
        });

        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }

    function getCuestionsTypeDoc(){
        let valSelectTypeDoc = $('#selectTipoDoc').val();        
        let metodo = 'POST';
        let objectSend = {
            option : 'cuestionsTypeDoc'
        }

        let request = supportRequest(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);            
            objectAllCuestTypeDoc = response;
            getOpsCuestTypeDoc();
            $('.bloqueo').fadeOut('slow');
        });

        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
        
    }

    function getOpsTypeImg(){
        var valSelectTypeImg = $('#selectCategorias').val();     
        
        if( (valSelectTypeImg in objectTypeImg) ){            
            $('#selectTipoImg').html(constructOps(objectTypeImg[valSelectTypeImg], 'IDTIPOIMAGEN', 'TIPOIMAGEN'));            
        }
        else if(valSelectTypeImg == '' || !(valSelectTypeImg in objectTypeImg)){            
            $('#selectTipoImg').html(constructOps());                        
        }
        $('#selectTipoDoc').html(constructOps());
        $('#sortable1').html(construcItemsOps(objectAllCuestions, 'IDPREGUNTA', 'PREGUNTA', 'cuestions', 'no-moveLast')); 
        $('#sortable2').html(''); 
        $('#sortable3').html(''); 
    }

    function getOpsTypeDoc(){
        let valSelectTypeDoc = $('#selectTipoImg').val();
        
        if(valSelectTypeDoc in objectTypeDoc){
            $('#selectTipoDoc').html(constructOps(objectTypeDoc[valSelectTypeDoc], 'IDTIPODOCUMENTO', 'TIPODOCUMENTO'));
        }
        else if(valSelectTypeDoc == '' || !(valSelectTypeDoc in objectTypeDoc)){
            $('#selectTipoDoc').html(constructOps());
        }
        $('#sortable1').html(construcItemsOps(objectAllCuestions, 'IDPREGUNTA', 'PREGUNTA', 'cuestions', 'no-moveLast')); 
        $('#sortable2').html(''); 
        $('#sortable3').html(''); 
    }

    
    function getOpsCuestTypeDoc(){
        let valSelectTypeDoc = $('#selectTipoDoc').val();        
        $('#sortable1').html(construcItemsOps(objectAllCuestions, 'IDPREGUNTA', 'PREGUNTA', 'cuestions', 'no-moveLast'));        
        if(objectAllCuestTypeDoc[valSelectTypeDoc] != undefined)
        {
            let objectSeparateItems = { 
                ACTIVE : [], 
                INACTIVE: [] 
            };        
            objectAllCuestTypeDoc[valSelectTypeDoc].forEach(function(item, count){                 
                $('li[data-cuestions="'+item["IDPREGUNTA"]+'"]').remove();
                if(item['ESTATUS'] == 1){                
                    objectSeparateItems.ACTIVE.push(item);                
                }else if(item['ESTATUS'] == 2){
                    objectSeparateItems.INACTIVE.push(item)
                }            
            });            
            $('#sortable2').html(construcItemsOps(objectSeparateItems.ACTIVE, 'IDPREGUNTADOC', 'PREGUNTA', 'active', 'no-moveFirst', ''));
            $('#sortable3').html(construcItemsOps(objectSeparateItems.INACTIVE, 'IDPREGUNTADOC', 'PREGUNTA', 'inactive', 'no-moveFirst no-moveMiddle'));
        }else{            
            $('#sortable2').html(construcItemsOps());
            $('#sortable3').html(construcItemsOps());
        }
    }

    //Guarda las preguntas recien agregadas al sortable
    function saveChangesCuest(){
        let idTipoDoc = $('#selectTipoDoc').val();  
        if(idTipoDoc == '' || selectTipoDoc == null){
            alert('Por favor seleccione un tipo de documento');        
        }else{

            let metodo = 'POST';
            let objectSend = {
                option : 'addUpCuestions',
                itemsUpdateOrder : [],
                itemsUpdateState : [],
                itemsInsert : [],
                IDTYPEDOC : idTipoDoc            
            }

            let objTemp = new Object();
            let objTemp2 = new Object();
            let objTemp3 = new Object();
            $('#sortable2 > li').each(function(count){
                if($(this).attr('data-cuestions')){
                    objTemp = {
                        IDPREGUNTA : $(this).attr('data-cuestions'),
                        ORDER : (++count) 
                    }
                    objectSend.itemsInsert.push(objTemp);                
                }
                else if($(this).attr('data-active')){
                    if($(this).attr('data-order') != (count+1)){
                        objTemp2 = {
                            IDPREGUNTADOC : $(this).attr('data-active'),
                            ORDER : (++count)
                        };
                        objectSend.itemsUpdateOrder.push(objTemp2);                    
                    }
                }
            });


            $('#sortable3 > li').each(function(count){
                if($(this).attr('data-active')){
                    objTemp3 = {
                        IDPREGUNTADOC : $(this).attr('data-active')                    
                    }
                    objectSend.itemsUpdateState.push(objTemp3);                
                }            
            });                        
            

            let request = supportRequest(objectSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){                
                let response = JSON.parse(res);
                
                if(typeof response == 'object' && res != false){                    
                    var stringResp = '';
                    if('inserts' in response){                        
                        if(response.inserts == true || response.inserts == 'true') stringResp += 'Registros Insertados Correctamente\n';
                        else stringResp += 'Error al guardar la información\n';
                    }
                    if('updateOrders' != undefined){                        
                        if(response.updateOrders == true || response.updateOrders == 'true') stringResp += 'Actualización del orden correcto\n';
                        else stringResp += 'Error actualizar el orden de los items\n';                        
                    }
                    if('updateState' in response){                        
                        if(response.updateState == true || response.updateState == 'true') stringResp += 'Cambio de estatus correcto\n';
                        else stringResp += 'Error al cambiar el estatus de los items\n';
                    }
                    
                    alert(stringResp);                    
                    getCuestionsTypeDoc();                    
                }
                else
                    console.log('ERROR');

                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });

        }        
        
        
        
    }

    function supportRequest(objectData, metodo, URL){
        return $.ajax({
            url : URL,
            method : metodo,
            data : objectData,
            dataType : 'html'
        });
    }

    function constructOps(objectData, valueKey, textValue){
        let optionsResult = '<option value="">Seleccione...</option>';

        if(objectData != undefined){
            objectData.forEach(function(item){
                optionsResult += '<option value="'+item[valueKey]+'">'+item[textValue]+'</option>';
            });
        }        
        return optionsResult;
    }

    function construcItemsOps(objectData, valueKey, textValue, targetItem, classItem = '', classIcon = ''){
        let ops = '';

        if(objectData != undefined){
            objectData.forEach(function(item){
                ops += '<li class="ui-state-default '+classItem+'" data-order="'+item['ORDERN']+'"  data-'+targetItem+'="'+item[valueKey]+'">'+item[textValue]+'\
                            <i class="iconSortable '+classIcon+'" aria-hidden="true"></i>\
                        </li>'
            });
        }

        return ops;
    }
    
        
    
</script>