<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
<link rel="stylesheet" href="">

<style>    
    
    /* _________________ Radio Buttons styles _____________*/
    .switch {
        position: relative;
        display: inline-block;
        width: 50px;
        height: 14px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 20px;
        width: 20px;
        left: -4px;
        bottom: -3px;
        background-color: white;
        border: 1px solid #007bff;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(35px);
        -ms-transform: translateX(35px);
        transform: translateX(35px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .checkLabelStyle {
        width: fit-content;
    }
    .checkStyle + .checkLabelStyle {
        display: block;
        margin: 0.2em;
        cursor: pointer;
        padding: 0.2em;
    }

    .checkStyle {
        display: none;
    }

    .checkStyle + .checkLabelStyle:before {
        content: "\2714";
        border: solid #007bff;
        border-radius: 0.2em;
        display: inline-block;
        width: 1.3em;
        height: 1.3em;
        vertical-align: middle;
        color: transparent;
        transition: .2s;
        margin-right: .3em;
    }

    .checkStyle + .checkLabelStyle:active:before {
        transform: scale(0);
    }

    .checkStyle:checked + .checkLabelStyle:before {
        background-color: MediumSeaGreen;
        border-color: MediumSeaGreen;
        color: white;
        align-content: center;
        display: inline-grid;        
    }

    .checkStyle:disabled + .checkLabelStyle:before {
        transform: scale(1);
        border-color: #aaa;
    }

    .checkStyle:checked:disabled + .checkLabelStyle:before {
        transform: scale(1);
        background-color: #bfb;
        border-color: #bfb;
    }
    /* _________________ Radio Buttons styles _____________*/    



    .icon-question > i{
        color: #28a7d2;
    }

    .icon-question{
        border-left: none;
        border-top-right-radius: .25rem !important;
        border-bottom-right-radius: .25rem !important;
    }    

    .color-blue{
        color: #007bff
    }

    div[data-categoryType]{
        display: none;
    }

    .btn-red{
        background-color: #f56d6d;
        color: white;
    }


    /* _____________________ Modal Styles __________________ */
    .styleHeader{
        color: white;
        background-color: #2b94b2e6;
        z-index: 1;
        width: 100%;
    }    
      
    @media (min-width: 360px){
        .styleHeader{            
            top: -20px;
            position: absolute;
            width: 90%;
            transform: translateX(5.8%);
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            box-shadow: 10px 10px 10px cadetblue;
        }
        .close{
            color: white;
            opacity: inherit;
        }
        .modalLabels > div{
            min-width: 150px;
        }
    }
    /* _____________________ Modal Styles __________________ */

</style>


<style>
    @media (max-width: 430px){
        .labelsStyle{
            border-top-right-radius: .25rem !important; 
            border-bottom-left-radius: 0 !important;
            white-space: pre-wrap;
            /* width: 100%;        */
        }

        .inputStyle{
            border-top-left-radius: 0px !important;
            border-bottom-left-radius: .25rem !important;
        }

        .inputIconStyle > div{
            border-top-right-radius: 0 !important;
        }             
        .inputIconStyle{             
            margin-right: inherit !important;
        }
        .minWidth-labels, .minWidth-labels > div{
            width: 100%;            
        }
    }
    
    @media (min-width: 431px) and (max-width: 567px){        
        .minWidth-labels, .minWidth-labels > div{
            width: 150px;            
        }
    }
    @media (min-width: 567px) and (max-width: 767px){        
        .minWidth-labels, .minWidth-labels > div{
            width: 200px;            
        }
    }
    @media (min-width: 768px) and (max-width: 1250px){        
        .minWidth-labels, .minWidth-labels > div{
            width: 200px;            
        }
    }
    @media (min-width: 1201px){        
        .minWidth-labels, .minWidth-labels > div{
            width: 250px;            
        }
    }

</style>

<style>
    @media only screen and (max-width: 350px), (min-device-width: 350) and (max-device-width: 1024px)  {
        
        #tabComPers, #tabComPers > thead, #tabComPerst > tbody, #tabComPers > thead > th, #tabComPers > tbody > tr > td, #tabComPers > tbody > tr { 
            display: grid;                                    
        }
                
        #tabComPers > thead tr { 
            position: absolute;
            top: -9999px;
            left: -9999px;
        }
                
        #tabComPers td {         
            position: relative;
            padding-left: 50%; 
        }         
        
        #tabComPers td:before { 
            position: absolute;
            
            top: 6px;
            left: 6px;
            width: 45%;  
            white-space: nowrap;
            margin: 10px;
        }        
        
        #tabComPers td:nth-of-type(1):before { content: "Correcto"; font-weight: bold; }
        #tabComPers td:nth-of-type(2):before { content: "Opción"; font-weight: bold;}
        #tabComPers td:nth-of-type(3):before { content: "Remover"; font-weight: bold;}        
    }

    .bloqueo{
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;       
        overflow: hidden;
        outline: 0;        
        position: fixed;
        background-color: rgba(154, 161, 164, 0.2);
        z-index: 10;
        display : none;
    }   
    /* .loaderStandar > i{
        color: #2bb273;
    } */
    .loaderStandar{
        margin: auto;
        vertical-align: middle;        
        font-size: 6rem;
        /* color: #2bb273; */
        color: #28A6DE;
        position: absolute;
        top: 45%;
        width : 100%;
        text-align : center;       
    }

</style>

<div class="bloqueo text-center">
    <div class="loaderStandar text-center">        
        <h4 class="text-center bold" style="color: #2bb273;">Cargando ...</h4>
        <i class="fa fa-spinner fa-spin" style="color: #28A6DE;"></i>
    </div>
</div>


<div class="container-fluid">

    <div class="row justify-content-center mt-3">
        <h3>Administrador de Preguntas</h3>
    </div>
    <div class="row my-3">
        <div class="col-lg-12 my-3">
            <div class="row justify-content-end">
                <div class="col-lg-3" style="max-width: 315px;">
                    <button class="btn btn-primary btn-block" onclick="addCuestion();">Agregar</button>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-12"> -->
            <!-- <div class="col-auto my-3">                         -->
            <div class="col-md-6 col-lg-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend minWidth-labels">
                        <div class="input-group-text labelsStyle">PREGUNTA</div>
                    </div>
                    <input type="text" placeholder="Pregunta" id="cuestionText" class="form-control inputStyle">
                    <div class="input-group-prepend inputIconStyle">
                        <div class="input-group-text icon-question"><i class="fa fa-question-circle"></i></div>                        
                    </div>                    
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend minWidth-labels">
                        <div class="input-group-text labelsStyle">NOMBRE CORTO</div>
                    </div>
                    <input type="text" placeholder="Nombre Corto" id="cuestionTextShort" class="form-control inputStyle">
                    <div class="input-group-prepend inputIconStyle">
                        <div class="input-group-text icon-question"><i class="fa fa-question-circle"></i></div>                        
                    </div>                    
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend minWidth-labels">
                        <div class="input-group-text labelsStyle">TIPO</div>
                    </div>
                    <select name="" id="typeCuestion" class="form-control inputStyle" onchange="getOptions();">
                        <option value="">Seleccione...</option>
                        <option value="Abierta">Abierta</option>
                        <option value="Combo">Combo</option>
                        <option value="Fecha">Fecha</option>
                        <option value="Curp">Curp</option>
                    </select>
                    <div class="input-group-prepend inputIconStyle">
                        <div class="input-group-text icon-question"><i class="fa fa-question-circle"></i></div>                        
                    </div>          
                </div>
            </div>

            <div class="col-md-6 col-lg-6" data-categoryType="Abierta"> 
                <div class="input-group mb-2">
                    <div class="input-group-prepend minWidth-labels">
                        <div class="input-group-text labelsStyle">CONTENIDO</div>
                    </div>
                    <select name="" id="contentType" class="form-control inputStyle">
                        <option value="">Seleccione...</option>                        
                        <option value="Texto">Texto</option>                        
                        <option value="Numerico">Numérico</option>                        
                    </select>
                    <div class="input-group-prepend inputIconStyle">
                        <div class="input-group-text icon-question"><i class="fa fa-question-circle"></i></div>                        
                    </div>          
                </div>
            </div>

            <div class="col-lg-9" data-categoryType="Abierta">
                <div class="row">
                    <div class="col-sm-8 col-md-8 col-lg-8">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend minWidth-labels">
                                <div class="input-group-text labelsStyle">Extensión</div>
                            </div>
                            <input type="number" min="1" max="18" step="1" class="form-control inputStyle" placeholder="Posiciones" id="extensionLong">
                            <div class="input-group-prepend inputIconStyle">
                                <div class="input-group-text icon-question"><i class="fa fa-question-circle"></i></div>                        
                            </div>          
                        </div>                    
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="">
                            <input type="checkbox" id="openCuestionLong" class="checkStyle">
                            <label for="openCuestionLong" class="checkLabelStyle">Cerrada</label>
                        </div>                    
                    </div>
                </div> 
            </div>
                        
            <div class="col-md-6 col-lg-6" data-categoryType="Combo">
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <input type="radio" id="firstCombo" name="typeComboOption" value="1" class="checkStyle">
                        <label for="firstCombo" class="checkLabelStyle">SI | NO</label>
                    </div>  
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <input type="radio" id="secondCombo" name="typeComboOption" value="2" class="checkStyle">
                        <label for="secondCombo" class="checkLabelStyle">SI | NO | NO DEFINIDO</label>
                    </div>  
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <input type="radio" id="treeCombo" name="typeComboOption" value="9" class="checkStyle">
                        <label for="treeCombo" class="checkLabelStyle">PERSONALIZADO</label>
                    </div>                
                </div>                
            </div>

            <div class="col-md-6 col-lg-6" data-categoryType="Combo">
                <div class="input-group mb-2">
                    <div class="input-group-prepend minWidth-labels">
                        <div class="input-group-text labelsStyle">Mensaje de Error</div>
                    </div>
                    <input type="text" id="messageError" class="form-control inputStyle" placeholder="Mensaje Error">
                    <div class="input-group-prepend inputIconStyle">
                        <div class="input-group-text icon-question"><i class="fa fa-times-circle"></i></div>                        
                    </div>                    
                </div>
            </div>            

            <div class="col-lg-12 mt-3" data-categoryType="Fecha">
                <div class="row">                    
                    <div class="col-lg-6">
                        <table class="table bordeded">
                            <thead class="col-auto">
                                <th><i class="fa fa-check-square color-blue mr-2"></i>Fecha</th>
                                <th>Orden</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <label class="switch align-bottom">
                                            <input type="checkbox" id="checkDay">
                                            <span class="slider round"></span>  
                                        </label>
                                        <label class="radioLabel ml-2 font-weight-normal">Día</label>
                                    </td>
                                    <td>
                                        <div class="row justify-content-center">
                                            <div class="col-auto">
                                                <input type="radio" id="firstDay" name="typeDayOrder" value="1" class="checkStyle" data-catDate="1" data-dateOrder="1">
                                                <label for="firstDay" class="checkLabelStyle">1</label>
                                            </div>  
                                            <div class="col-auto">
                                                <input type="radio" id="secondDay" name="typeDayOrder" value="2" class="checkStyle" data-catDate="2" data-dateOrder="1">
                                                <label for="secondDay" class="checkLabelStyle">2</label>
                                            </div>  
                                            <div class="col-auto">
                                                <input type="radio" id="treeDay" name="typeDayOrder" value="3" class="checkStyle" data-catDate="3" data-dateOrder="1">
                                                <label for="treeDay" class="checkLabelStyle">3</label>
                                            </div>                
                                        </div> 
                                    </td>                                    
                                </tr>                                
                                <tr>
                                    <td>
                                        <label class="switch align-bottom">
                                            <input type="checkbox" id="checkMonth">
                                            <span class="slider round"></span>  
                                        </label>
                                        <label class="radioLabel ml-2 font-weight-normal">Mes</label>
                                    </td>
                                    <td>
                                        <div class="row justify-content-center">
                                            <div class="col-auto">
                                                <input type="radio" id="firstMonth" name="typeMonthOrder" value="1" class="checkStyle" data-catDate="1" data-dateOrder="2">
                                                <label for="firstMonth" class="checkLabelStyle">1</label>
                                            </div>  
                                            <div class="col-auto">
                                                <input type="radio" id="secondMonth" name="typeMonthOrder" value="2" class="checkStyle" data-catDate="2" data-dateOrder="2">
                                                <label for="secondMonth" class="checkLabelStyle">2</label>
                                            </div>  
                                            <div class="col-auto">
                                                <input type="radio" id="treeMonth" name="typeMonthOrder" value="3" class="checkStyle" data-catDate="3" data-dateOrder="2">
                                                <label for="treeMonth" class="checkLabelStyle">3</label>
                                            </div>                
                                        </div>
                                    </td>                                    
                                </tr>                                
                                <tr>
                                    <td>
                                        <label class="switch align-bottom">
                                            <input type="checkbox" id="checkYear">
                                            <span class="slider round"></span>  
                                        </label>
                                        <label class="radioLabel ml-2 font-weight-normal">Año</label>
                                    </td>
                                    <td>
                                        <div class="row justify-content-center">
                                            <div class="col-auto">
                                                <input type="radio" id="firstYear" name="typeYearOrder" value="1" class="checkStyle" data-catDate="1" data-dateOrder="3">
                                                <label for="firstYear" class="checkLabelStyle">1</label>
                                            </div>  
                                            <div class="col-auto">
                                                <input type="radio" id="secondYear" name="typeYearOrder" value="2" class="checkStyle" data-catDate="2" data-dateOrder="3">
                                                <label for="secondYear" class="checkLabelStyle">2</label>
                                            </div>  
                                            <div class="col-auto">
                                                <input type="radio" id="treeYear" name="typeYearOrder" value="3" class="checkStyle" data-catDate="3" data-dateOrder="3">
                                                <label for="treeYear" class="checkLabelStyle">3</label>
                                            </div>                
                                        </div>
                                    </td>                                    
                                </tr>                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mt-3" data-categoryType="comboPerzonalizado">            
                <div class="row">                    
                    <div class="col-lg-6">
                        <div class="col-lg-12 my-3">
                            <div class="row justify-content-end">
                                <div class="col-lg-3" style="max-width: 165px;">
                                    <button class="btn btn-primary btn-block" onclick="addOpsCombPer('contentCuestionsCombo');"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        <table class="table" id="tabComPers">
                            <thead class="col-auto">
                                <th><i class="fa fa-check-square color-blue mr-2"></i>Correcto</th>
                                <th>Opción</th>
                                <th>Remover</th>
                            </thead>
                            <tbody class="col-auto" id="contentCuestionsCombo">
                                <tr data-index="1" style="width: auto!important;">
                                    <td>
                                        <label class="switch align-middle">
                                            <input type="checkbox" data-comboPerCheck="1">
                                            <span class="slider round"></span>  
                                        </label>                                        
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" data-comboPerValue="1">
                                    </td>
                                    <td>
                                        <label for="" class="form-Text my-3"></label>
                                    </td>
                                </tr>                                
                            </tbody>
                        </table>
                    </div>                    
                </div>                    
            </div>        
    </div>

    <div class="container-fluid">
        <!-- <div class="row justify-content-center"> -->
            <!-- <div class="col-lg-10"> -->
                <div id="contentResultCuestions"></div>
            <!-- </div> -->
        <!-- </div> -->
    </div>

    <section>
        <!-- Modal -->
        <div class="modal fade" id="actualizarCampos" tabindex="-1" role="dialog" aria-hidden="true" style="padding-right: 0 !important">
            <div class="modal-dialog modal-lg mt-5 pt-5" role="document">
                <div class="modal-content">
                    <div class="modal-header styleHeader">
                        <h5 class="modal-title" id="">EDITAR</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mt-4">
                        <!-- <div class="row p-3"><h5 class="m-auto">Tipo Imagen</h5></div> -->
                        <div class="row">                            
                            <!-- <div class="col-md-6"> -->
                                <div class="col-md-12 my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend minWidth-labels">
                                            <div class="input-group-text labelsStyle">PREGUNTA</div>
                                        </div>
                                        <input type="text" placeholder="Pregunta" class="form-control inputStyle" id="upCuestionTab">
                                        <div class="input-group-prepend inputIconStyle">
                                            <div class="input-group-text icon-question"><i class="fa fa-question-circle"></i></div>                        
                                        </div>                    
                                    </div>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend minWidth-labels">
                                            <div class="input-group-text labelsStyle">NOMBRE CORTO</div>
                                        </div>
                                        <input type="text" placeholder="Nombre Corto" class="form-control inputStyle" id="upCuestionShortTab">
                                        <div class="input-group-prepend inputIconStyle">
                                            <div class="input-group-text icon-question"><i class="fa fa-question-circle"></i></div>                        
                                        </div>                    
                                    </div>
                                    <div class="" id="optionsUpdate"></div>
                                </div>                                
                            <!-- </div>                                                         -->
                        </div>
                                        
                    </div>                
                    <div class="modal-footer">
                        <div class="row">
                            <div id="btnActionUpdate" class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <button type="button" class="btn btn-red btn-block my-2" data-dismiss="modal">CANCELAR</button>                    
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<script>
    var URL = "../../public/portal/controller/agregarPreguntas_controller.php";            
    var indexComboOps = 1;
    $(function(){        
        // tablePadron($('#tabComPers'), 'Opciones', 3);
        getCuestions();          
        $('input[name="typeComboOption"]').on('change', toggleComboPersonalized);
        $('input[data-catDate]').change({ event: this}, toggleOrderDate);        
    });

    function getOptions(){
        let typeCuestion = $('#typeCuestion').val();

        $('div[data-categoryType]').each(function(){
            let categoryType = $(this).attr('data-categoryType');
            if(typeCuestion === categoryType){                
                $(this).show();
            }else{                                
                if(categoryType == 'comboPerzonalizado' && typeCuestion === 'Combo' && $('input[name="typeComboOption"]:checked').val() == 9)
                    $('div[data-categoryType="comboPerzonalizado"]').show();
                else
                    $(this).hide();                
            }                
        });           
    }

    function getCuestions(){
        let objectSend = {
            option : 'Cuestions'
        };
        let metodo = 'POST';

        let request = supportPetition(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);
            let headers = ['ID','PREGUNTA','TIPO','EXTRAS','NOMBRE CORTO','MENSAJE DE ERROR', 'VALORES CORRECTOS', 'EDITAR'];
            let fields = ['IDPREGUNTA','PREGUNTA','TIPOPREGUNTA','OPCIONES','NOMBRECORTO','MENSAJEERROR', 'VALORESCORRECTOS'];
            let table = constructTable(response, headers, fields, true);
            $('#contentResultCuestions').html(table);
            tablePadron('#cuestionsTableResult', 'Preguntas', headers.length);
            $('.bloqueo').fadeOut('slow');            
        });

        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }

/*  TIPOPREGUNTA = typeCuestion
    PREGUNTA = cuestion
    OPCIONES = optionsValues
    MENSAJEERROR = messageError
    VALORESCORRECTOS = correctValues
    NOMBRECORTO = cuestionShort
    ESTATUS = '' */

    function addCuestion(){
        let objectSend = new Object();
        let metodo = 'POST';
        
        let cuestion = $('#cuestionText').val();
        let cuestionShort = $('#cuestionTextShort').val();
        let typeCuestion = $('#typeCuestion').val();

        objectSend.cuestion = cuestion;
        objectSend.cuestionShort = cuestionShort;
        objectSend.typeCuestion = typeCuestion;
        objectSend.option = 'addCuestion';

        // console.log(objectSend);
        if(typeCuestion == "Abierta"){
            let extensionLong = $('#extensionLong').val();
            extensionLong += ($('#openCuestionLong').prop('checked')) == true ? '_1' : '_0';

            objectSend.optionsValues = $('#contentType').val();
            objectSend.correctValues = extensionLong;            
        }
        else if(typeCuestion == "Combo"){
            let typeComboOption = $("input[name='typeComboOption']:checked").val();
            let messageError = $("#messageError").val();
            let optionValues = '';
            if(typeComboOption == 9){
                $('input[data-combopervalue]').each(function(){
                    let index  = $(this).attr('data-combopervalue');
                    let statusCheck = $('input[data-combopercheck="'+index+'"]').is(':checked');                    
                    optionValues += $(this).val() + (statusCheck ? '_1' : '_0') + '/';                    
                });
                optionValues = optionValues.substr(0,optionValues.length-1);                

                objectSend.optionsValues = optionValues;
                objectSend.correctValues = typeComboOption;
                objectSend.messageError = messageError;                
            }else{
                objectSend.correctValues = typeComboOption;
                objectSend.messageError = messageError;                
            }
        }else if(typeCuestion == 'Fecha'){
            let day = 'DIA' + ($('#checkDay').is(':checked') ? '_1' : '_0');
            let month = 'MES' + ($('#checkMonth').is(':checked') ? '_1' : '_0');
            let year = 'ANIO' + ($('#checkYear').is(':checked') ? '_1' : '_0');

            day += '_' + $('input[name="typeDayOrder"]:checked').val();
            month += '_' + $('input[name="typeMonthOrder"]:checked').val();
            year += '_' + $('input[name="typeYearOrder"]:checked').val();

            objectSend.correctValues = day + '/' + month + '/' + year;
        }               
        
    
        let request = supportPetition(objectSend, metodo, URL);
        $('.bloqueo').show();

        request.done(function(res){
            console.log(res)
            if(res == true || res == 'true'){
                alert('Datos Guardados Correctamente');
                $('tr[data-tempOps]').remove();
                indexComboOps = 1;
                getCuestions();
            }
            else{
                alert('Ha ocurrido un error al guardar la información');
            }
            $('.bloqueo').fadeOut('slow');
        });

        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });


    }

    function toggleComboPersonalized(){        
        let optionSelected = $("input[name='typeComboOption']:checked").val();            

        if(optionSelected === '9'){
            $('div[data-categoryType=comboPerzonalizado]').show();
        }else{
            $('div[data-categoryType=comboPerzonalizado]').hide();
        }
    }

    function toggleOrderDate(event){        
        let elementSelected = $(this).attr('data-dateOrder');
        let groupCheck = $(this).attr('data-catDate');
        $('input[data-catDate="'+groupCheck+'"]').each(function(){
            if(elementSelected != $(this).attr('data-dateOrder'))                
                $(this).prop('checked', false);            
        });        
    }

    function addOpsCombPer(idElement, option = ''){
        indexComboOps++;
        $('#'+idElement).append('<tr data-tempOps="comboPersonalized">\
                                                <td>\
                                                    <label class="switch align-middle">\
                                                        <input type="checkbox" data-comboPerCheck'+option+'="'+indexComboOps+'">\
                                                        <span class="slider round"></span>\
                                                    </label>\
                                                </td>\
                                                <td>\
                                                    <input type="text" class="form-control" data-comboPerValue'+option+'="'+indexComboOps+'">\
                                                </td>\
                                                <td>\
                                                    <button class="btn btn-red btn-block" onclick="removeOpsCombPer($(this));"><i class="fa fa-minus"></i></button>\
                                                </td>\
                                            </tr>');          
    }

    function removeOpsCombPer(elementRemove){
        $(elementRemove).parent().parent().remove();        
    }

    function constructTable(objectData, headers, fields, btnEdit = false){
        let newTable = '<table class="table" id="cuestionsTableResult">\
                            <thead>';
        
        headers.forEach(function(item){
            newTable += '<th>'+item+'</th>';
        });

        newTable += '</thead>\
                        <tbody>';

        objectData.forEach(function(item){
            newTable += '<tr>'; 
            fields.forEach(function(field){
                if(item[field] == null) item[field] = "";
                newTable += '<td data-category="'+field+'">'+item[field]+'</td>' 
            });

            if(btnEdit)newTable += '<td><button class="btn btn-primary btn-md" onclick="updateCuestion($(this))">\
                                    <i class="fa fa-edit"></i>Editar</button></td>';
            newTable += '</tr>'; 
        });                        
        
        newTable += '</tbody>\
                    </table>';

        return newTable;
    }

    function supportPetition(objectData, metodo, url){
        return $.ajax({
            url : url, 
            data : objectData,
            method : metodo
        });
    }

    function updateCuestion(rowUpdate){        
        $('#optionsUpdate').html('');
        let cuestionID = rowUpdate.parent().siblings('td[data-category="IDPREGUNTA"]').text();
        let categoryCuestion = rowUpdate.parent().siblings('td[data-category="TIPOPREGUNTA"]').text();
        let cuestionTable = rowUpdate.parent().siblings('td[data-category="PREGUNTA"]').text();
        let cuestionTableShort = rowUpdate.parent().siblings('td[data-category="NOMBRECORTO"]').text();

        if(categoryCuestion == 'Combo'){
            var errorMessage = rowUpdate.parent().siblings('td[data-category="MENSAJEERROR"]').text();

            $('#optionsUpdate').html('<div class="input-group mb-2">\
                                        <div class="input-group-prepend minWidth-labels">\
                                            <div class="input-group-text labelsStyle">Mensaje de Error</div>\
                                        </div>\
                                        <input type="text" placeholder="Mensaje de Error" class="form-control inputStyle" id="upMessageTab" value="'+errorMessage+'">\
                                        <div class="input-group-prepend inputIconStyle">\
                                            <div class="input-group-text icon-question"><i class="fa fa-question-circle"></i></div>\
                                        </div>\
                                    </div>');
            let typeCombo = rowUpdate.parent().siblings('td[data-category="VALORESCORRECTOS"]').text();
            if(typeCombo == 9){
                let additionalValues = rowUpdate.parent().siblings('td[data-category="OPCIONES"]').text();
                let collectionValues = additionalValues.split('/');
                
                $('#optionsUpdate').append('<div class="col-lg-12 my-3">\
                                                <div class="row justify-content-end">\
                                                    <div class="col-lg-3">\
                                                        <button class="btn btn-primary btn-block" onclick="addOpsCombPer(\'opsUpdateCont\',\'Update\');"><i class="fa fa-plus"></i></button>\
                                                    </div>\
                                                </div>\
                                            </div>');

                let tableOptionsPers = '<div style="overflow-x:scroll">\
                                        <table class="table" style="min-width: 350px;">\
                                                <thead class="col-auto">\
                                                    <th><i class="fa fa-check-square color-blue mr-2"></i>Correcto</th>\
                                                    <th>Opción</th>\
                                                    <th>Remover</th>\
                                                </thead>\
                                                <tbody class="col-auto" id="opsUpdateCont">';


                for(cont = 0; cont < collectionValues.length; cont++){
                    let checkState = (collectionValues[cont].split('_').pop(-1) == 1) ? 'checked' : '';
                    let valueCheck = collectionValues[cont].slice(0, collectionValues[cont].lastIndexOf("_"));
                    console.log(checkState);
                    tableOptionsPers += '<tr>\
                                            <td>\
                                                <label class="switch align-middle">\
                                                    <input type="checkbox" data-comboPerCheckUpdate="update_'+(cont+1)+'" '+checkState+'>\
                                                    <span class="slider round"></span>\
                                                </label>\
                                            </td>\
                                            <td>\
                                                <input type="text" class="form-control" data-comboPerValueUpdate="update_'+(cont+1)+'" value="'+valueCheck+'">\
                                            </td>\
                                            <td></td>\
                                        </tr>';
                }                
                tableOptionsPers += '</tbody></table></div>';
                $('#optionsUpdate').append(tableOptionsPers);
                $('#btnActionUpdate').html('<button type="button" class="btn btn-success btn-block my-2" onclick="updateCuestionNewValues(\''+cuestionID+'\', 3);">GUARDAR</button>');
            }else{
                $('#btnActionUpdate').html('<button type="button" class="btn btn-success btn-block my-2" onclick="updateCuestionNewValues(\''+cuestionID+'\', 2);">GUARDAR</button>');
            }
        }else{
            $('#btnActionUpdate').html('<button type="button" class="btn btn-success btn-block my-2" onclick="updateCuestionNewValues(\''+cuestionID+'\', 1);">GUARDAR</button>');
        }
        
        //Valores para la modal
        $('#upCuestionTab').val(cuestionTable);
        $('#upCuestionShortTab').val(cuestionTableShort);

        $('#actualizarCampos').modal({backdrop: 'static', keyboard : false });
    }

    function updateCuestionNewValues(cuestionID, option){
        let cuestion = $('#upCuestionTab').val();
        let cuestionShort = $('#upCuestionShortTab').val();

        let objectSend = {            
            cuestionId : cuestionID,
            cuestion : cuestion,
            cuestionShort : cuestionShort
        }
        let metodo = 'POST';

        if(option == 1) objectSend.option = 'updateCuestion';
        if(option == 2){
            objectSend.option = 'updateCuestionCombo';
            objectSend.messageError = $('#upMessageTab').val();
        }
        if(option == 3){
            objectSend.option = 'updateCuestionComboPers';
            objectSend.messageError = $('#upMessageTab').val();
            let optionValues = '';
            $('input[data-combopervalueUpdate]').each(function(){
                let index  = $(this).attr('data-combopervalueUpdate');
                let statusCheck = $('input[data-combopercheckUpdate="'+index+'"]').is(':checked');                    
                optionValues += $(this).val() + (statusCheck ? '_1' : '_0') + '/';                    
            });
            optionValues = optionValues.substr(0,optionValues.length-1);

            objectSend.optionValues = optionValues; 
        }
        
        let request = supportPetition(objectSend, metodo, URL);
        $('.bloqueo').show();
        request.done(function(res){
            if(res == true || res == 'true'){
                alert('Registro Actualizado con exito');
                getCuestions();
            }else{
                console.log('Error');
            }
            $('.bloqueo').fadeOut('slow');
        });
        
        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }

</script>
