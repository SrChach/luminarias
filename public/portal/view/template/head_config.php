<!DOCTYPE html>
<html lang="es">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <?php if (!isset($_GET['view'])): ?>
         <title><?php echo strtoupper($sys).' | '."LOGIN"; ?></title>
      <?php endif ?>
      <?php if (isset($_GET['view'])): ?>
         <?php if ($_GET['view']=='users'): ?>
            <title><?php echo strtoupper($sys).' | '.strtoupper('USUARIOS'); ?></title>
         <?php endif ?>
         <?php if ($_GET['view']!='users'): ?>
            <?php if ($_GET['view']=='statistics'): ?>
               <title><?php echo strtoupper($sys).' | '.strtoupper('Estadísticas'); ?></title>   
            <?php endif ?>
            <?php if ($_GET['view']!='statistics'): ?>
               <title><?php echo strtoupper($sys).' | '.strtoupper($_GET['view']); ?></title>      
            <?php endif ?>
            
         <?php endif ?>
      <?php endif ?>
  <!-- Tell the browser to be responsive to screen width -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="shortcut icon" type="image/x-icon" href="./assets/img/favicon.png" />
  <!-- Font Awesome -->
   <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"-->
   <link rel="stylesheet" href="./vendor/thisPlugins/bootstrap/font-awesome.css">
  <!-- Ionicons -->
   <!--link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"-->
   <link rel="stylesheet" href="./vendor/thisPlugins/bootstrap/ionicons.css">
  <!-- Theme style -->
   <!--link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous"-->
   <link rel="stylesheet" href="./vendor/thisPlugins/bootstrap/bootstrap.css">
   
   <link rel="stylesheet" href="./vendor/dist/css/adminlte.min.css">

   <!--script src="https://code.jquery.com/jquery-1.12.4.js"></script-->
   <script src="./vendor/thisPlugins/jquery/jquery-1.12.4.js"></script>

   <link rel="stylesheet" type="text/css" href="./assets/css/Admin.css">
   <link rel="stylesheet" type="text/css" href="./assets/css/app.css">
   <link rel="stylesheet" type="text/css" href="./assets/css/meta-contents.css">

   
      <!--link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"-->
   <link rel="stylesheet" type="text/css" href="./vendor/thisPlugins/dataTable/dataTables.bootstrap4.css">

   <!--Css de notificaciones-->
   <link href="./assets/css/notify.css" rel="stylesheet" type="text/css" />


   <!--link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css"-->
   <link rel="stylesheet" href="./vendor/thisPlugins/morris/morris.css">
   <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script-->
   <script src="./vendor/thisPlugins/morris/jquery-1.9.0.js"></script>
   <!--script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script-->
   <script src="./vendor/thisPlugins/morris/raphael-2.1.0.js"></script>
   <!--script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script-->
   <script src="./vendor/thisPlugins/morris/morris.js"></script>


   
   <script type="text/javascript" src='./assets/js/general/jsConfig.js'></script>
   <script type="text/javascript" src='./assets/js/general/funcionesgenerales.js'></script>
   <script type="text/javascript" src='./assets/js/notify/alerts.js'></script>
   
   <script type="text/javascript" src='./assets/js/general/ajax.js'></script>

   
   <?php if (!isset($_GET['view'])): ?>
      <link rel="stylesheet" type="text/css" href="./assets/css/login.css">
   <?php endif ?>
</head>
<?php if (!isset($_GET['view'])): ?>
<body class="">
<?php endif ?>
<?php if (isset($_GET['view'])): ?>
<body class="sidebar-mini sidebar-collapse">
<?php endif ?>