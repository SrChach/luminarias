        <?php if (isset($_GET['certificado'])): ?>
            <?php include_once './controller/ViewsController.php'; ?>
        <?php endif; ?>

        <?php if (!isset($_GET['certificado'])): ?>

         <?php if (isset($_GET['view'])): ?>
            <div class="wrapper">
               <?php
                  include_once './view/general/nav2.php';
                  include_once './view/general/aside-menu.php';
               ?>
            <?php if (isset($_SESSION['Auth']) and $_SESSION['Auth']==1): ?>
               <div class="content-wrapper main-content">
               <?php include_once './view/general/title_page.php'; ?>
                  <section class="content" style="padding-bottom: 9%">
                     <button onclick="topFunction()" id="myBtn" class="myBtn" data-toggle="tooltip" data-placement="top" title="IR AL PRINCIPIO"><i class="fa fa-arrow-circle-up"></i></button>
            <?php endif ?>
         <?php endif ?>
            <?php
               if (isset($_SESSION['Auth']) and $_SESSION['Auth']==1 and isset($_GET['view'])) {
                  include_once './controller/ViewsController.php';
                  include_once './view/general/load.php';
               }elseif (!isset($_GET['view'])) {
                  include_once './controller/ViewsController.php';
               }
            ?>

         <?php if (isset($_GET['view'])): ?>
            <?php if (isset($_SESSION['Auth']) and $_SESSION['Auth']==1): ?>
                  </section>
               </div>
            <?php endif ?>
               <?php include_once './view/general/footer.php'; ?>
            </div>
            <!-- ./wrapper -->
         <?php endif ?>
        <?php endif; ?>
