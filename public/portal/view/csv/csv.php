<div class="container-fluid">
	<div class="row mt-5">
		<div class="col-md-4 offset-4">
			<div class="card">
				<h4 class="card-header text-center bg-success">DESCARGAR CSV</h4>
				<form class="" action="javascript:requestCsv();">
					<div class="card-body">
						<div class="input-group mb-3">
  							<div class="input-group-prepend">
    							<span class="input-group-text" id="basic-addon1">Fecha Inicio</span>
  							</div>
  							<select class="form-control" name="from" required>
								<option value="">Seleccione fecha de inicio...</option>
  							</select>
						</div>
						<div class="input-group mb-3">
  							<div class="input-group-prepend">
    							<span class="input-group-text" id="basic-addon1">Fecha Final</span>
  							</div>
  							<select class="form-control" name="to" required>
								<option value="">Seleccione fecha de fin...</option>
  							</select>
						</div>
						<div class="input-group mb-3">
  							<div class="input-group-prepend">
    							<span class="input-group-text" id="basic-addon1">TIPO</span>
  							</div>
  							<select class="form-control" name="type" required>
								<option value="luminaria" selected>Luminarias</option>
								<option value="semaforo">Semáforos</option>
								<option value="cd">Cargas Directas</option>
  							</select>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-md-4 offset-4">
								<button type="submit" name="makecsv" class="btn btn-success btn-block">Generar</button>
								<!--a href="" class="btn btn-success btn-block" data-type="luminaria" sty>Generar Csv L</a>
								<a href="" class="btn btn-success btn-block" data-type="semaforo">Generar Csv S</a>
								<a href="" class="btn btn-success btn-block" data-type="cd">Generar Csv CD</a-->
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var fechas;
	$(()=>{

		let request = {
			route:'./controller/CsvController.php?opcion=getDatesRegistros',
		}
		Fetch(request).then((dates)=>{
			fechas = dates;
			$('[name=from]').html(buildOptions(dates,'FECHA','FECHA'));
		});

		$("[name=from]").change(function(event) {
			let index = $(this).find(":selected").index();
			let fechasArray = [];
			for (var i = index-1; i < fechas.length; i++) {
				fechasArray.push(fechas[i]);
			}
			$("[name=to]").html(buildOptions(fechasArray,'FECHA','FECHA'));
		});

		/*$('[name=makecsv]').click(()=>{
			alert('make');
		});*/
	});

	requestCsv = () =>{
		console.log(`./controller/CsvController.php?opcion=makeCsv&tipo=${$("[name=type]").val()}&inicio=${$("[name=from]").val()}&fin=${$("[name=to]").val()}`);

		window.location = `./controller/CsvController.php?opcion=makeCsv&tipo=${$("[name=type]").val()}&inicio=${$("[name=from]").val()}&fin=${$("[name=to]").val()}`;

		/*console.log(`./controller/CsvController.php?opcion=makeCsv&tipo=semaforo&inicio=${$("[name=from]").val()}&fin=${$("[name=to]").val()}`);

		console.log(`./controller/CsvController.php?opcion=makeCsv&tipo=cd&inicio=${$("[name=from]").val()}&fin=${$("[name=to]").val()}`);

		window.location = `./controller/CsvController.php?opcion=makeCsv&tipo=luminaria&inicio=${$("[name=from]").val()}&fin=${$("[name=to]").val()}`;
		setTimeout(()=>{
			window.location = `./controller/CsvController.php?opcion=makeCsv&tipo=semaforo&inicio=${$("[name=from]").val()}&fin=${$("[name=to]").val()}`
		},1500);
		setTimeout(()=>{
			window.location = `./controller/CsvController.php?opcion=makeCsv&tipo=cd&inicio=${$("[name=from]").val()}&fin=${$("[name=to]").val()}`
		},2000);*/

	}



</script>
