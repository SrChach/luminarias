<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Mapa simple de OpenStreetMap con Leaflet</title>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css">
</head>
<body>
<h1>Mapa simple de OpenStreetMap con Leaflet</h1>  
<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>	  
<div id="map" class="map map-home" style="margin:12px 0 12px 0;height:600px;"></div>
<?php 
	if ($_GET) {
		echo 'VIDO';
	}

?>

<script>
	var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
		osm = L.tileLayer(osmUrl, {maxZoom: 18});
	var map = L.map('map').setView([23.1171659, -100.8880475], 6).addLayer(osm);

	var marker= L.marker([19.5297409, -96.932232],{draggable:'true'})
		.addTo(map)
		.bindPopup('La Catedral de la Habana.')
		.openPopup();

	marker.on('dragend', function(event) {
    	var marker = event.target;  // you could also simply access the marker through the closure
    	var result = marker.getLatLng();  // but using the passed event is cleaner
    	console.log(marker);
    	alert(result);
	});
</script>
  </body>
</html>
