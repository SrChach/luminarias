<link rel="stylesheet" href="./vendor/thisPlugins/map/leaflet.css">


<div class="container-fluid">
	<div class="row text-center">
		<div class="col-12">
			<div id="map" style="height:calc(100vh - 200px);">
				Cargando <i class="fa fa-spinner fa-spin" style="font-size:40px"></i>
			</div>
		</div>
	</div>
</div>


<?php
	if ($_GET) {
		$params=$_GET['params'];
		$data=json_decode($params);
		$lat=explode(",", $data->location)[0];
		$lng=explode(",", $data->location)[1];
	}
?>


<script src="./vendor/thisPlugins/map/leaflet.js"></script>

<script>
	$(function () {
		var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
			osm = L.tileLayer(osmUrl, {maxZoom: 22});
		var map = L.map('map').setView([<?php echo $lat ?>, <?php echo $lng ?>], 18).addLayer(osm);

		var address=(Geocoder(<?php echo $lat ?>,<?php echo $lng ?>)).display_name

		L.control.scale().addTo(map);
		var marker= L.marker([<?php echo $lat ?>, <?php echo $lng ?>])
				.addTo(map)
				.bindPopup(address+`.<br><a href="https://maps.google.com/?q=${<?php echo $lat ?>},${<?php echo $lng ?>}" target="_blank">Visitar en Google Maps<a>`)
				.openPopup();
				//https://maps.google.com/?q='+response.LATITUD+','+response.LONGITUD+'

		/*marker.on('click',function (event) {
			marker.openPopup();
		});
		/*marker.on('dragend', function(event) {
    	var marker = event.target;  // you could also simply access the marker through the closure
    		var result = marker.getLatLng();  // but using the passed event is cleaner
    		console.log(marker);
    		alert(result);
		});*/
	});

	function Geocoder(lat,lng) {  //Funcion estandar ajax
    	var regresa;
		var urlGeo ="https://nominatim.openstreetmap.org/reverse?format=json&lat="+lat+"&lon="+lng+"&zoom=18&addressdetails=1";
		console.log(urlGeo);
    	$.ajax({
        	type: "POST",
        	dataType: "JSON",
        	url: urlGeo,
        	async: false,
        	success: function (respuesta) {
            	regresa = respuesta;
        	},
        	error: function (e) {
            	console.log(e.message);
        	}
    	});
    	return regresa;
	}






</script>
