<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <table class="table table-sm bordered">
                <thead class="thead-green">
                    <tr>
                        <th>#</th>
                        <th>NOMBRE DE ARCHIVO</th>
                        <th>FECHA DE SINCRONIZACIÓN</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="col-md-7">
            <form class="card" action="javascript:syncFile();">
                <h5 class="card-header thead-green">Sincronizar Archivo</h5>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Archivo</span>
                        </div>
                        <input type="file" name="file" class="form-control" onchange="ValidafileType(this,7)" required>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Sincronizar</button>
                    <button type="reset" class="btn btn-secondary">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    syncFile = () => {
        /*$('#load-file').show('slow');
        $('#content-input').hide('slow');*/
        var formData = new FormData();

        formData.append('file',$('input[name=file]')[0].files[0]);
        formData.append('csrf_token','<?php echo $csrf_token ?>');
        formData.append('opcion','syncFile');

        uploadMultiFile('./controller/SyncController.php',formData).then((response)=>{
            console.log(response);
            /*let titleMsg = 'Lo siento', typeMsg = 'error';
            $('#load-file').hide('slow');
            if (response.codigo==true) {
                titleMsg = 'EXITO !', typeMsg = 'success';
                $('#form-file')[0].reset();
                $('#modalAddDevices').modal('hide');
                _init();
            }
            notificar(titleMsg,response.message,typeMsg);
            $('#content-input').show('slow');*/
        });
    }
</script>

