<div class="container-fluid">
    <div class="row border-bottom">
        <div class="col-md-3 col-6">
            <a class="getData" style="cursor: pointer" data-filter="luminarias">
                <div class="card hover border-custom">
                    <div class="row no-gutters">
                        <div class="col-md-4 border-right">
                            <img src="./assets/img/icons/icon-luminarias.png" width="100%">
                        </div>
                        <div class="col-md-8">
                            <h5 class="card-header p-0">LUMINARIAS</h5>
                            <div class="card-body">
                                <h4 class="card-text text-success"> 
                                    TOTAL: <small id="count-lum"></small>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-6">
            <a class="getData" style="cursor: pointer" data-filter="semaforos">
                <div class="card hover border-custom">
                    <div class="row no-gutters">
                        <div class="col-md-4 border-right">
                            <img src="./assets/img/icons/icon-semaforos.png" width="100%">
                        </div>
                        <div class="col-md-8">
                            <h5 class="card-header p-0">SEMÁFOROS</h5>
                            <div class="card-body">
                                <h4 class="card-text text-success">
                                    TOTAL: <small id="count-sem"></small>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-6">
            <a class="getData" style="cursor: pointer" data-filter="cargas">
                <div class="card hover border-custom">
                    <div class="row no-gutters">
                        <div class="col-md-4 border-right">
                            <img src="./assets/img/icons/icon-cargas.png" width="100%">
                        </div>
                        <div class="col-md-8">
                            <h5 class="card-header p-0">CAR. DIRECTAS</h5>
                            <div class="card-body">
                                <h4 class="card-text text-success">
                                    TOTAL: <small id="count-car"></small>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-6">
            <a class="getData" style="cursor: pointer" data-filter="censo">
                <div class="card border-custom set-hover">
                    <div class="row no-gutters">
                        <div class="col-md-4 border-right">
                            <img src="./assets/img/icons/icon-censo.png" width="100%">
                        </div>
                        <div class="col-md-8">
                            <h5 class="card-header p-0">CENSO 2019</h5>
                            <div class="card-body">
                                <h4 class="card-text text-success">
                                    TOTAL: <small id="count-censo"></small>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row elevation-2">
        <div class="col-md-12 text-center wait" style="display:none"><i class="fa fa-spinner fa-spin" style="font-size:100px;"></i></div>
        <div class="col-md-12">
            <div class="row content_resumen">
                <div class="col-md-5">
                    <div class="card border-success mt-5">
                        <h5 class="card-header text-center bg-success">Resumen General</h5>
                        <div class="card-body pt-4 table-responsive" style="height:300px" id="tb_resumen">
                            <table class="table table-sm table-bordered">
                                <thead class="thead-green">
                                    <tr>
                                        <th>TIPO</th>
                                        <th>AVANCE</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="card border-success mt-5">
                        <h5 class="card-header text-center bg-success">Resumen General - Gráfica</h5>
                        <div class="card-body pt-4 pl-1" id="chart_resumen" style="height:300px">
                        </div>
                    </div>
                </div>
                
                <div class="col-12">
                    <?php
                        if($_SESSION['PERFIL'] != '11' && $_SESSION['ZONA'] != '0' && $_SESSION['PERFIL'] != '12')
                            echo '<div id="resumenes-container" class="row"></div>';
                    ?>
                </div>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var urlController="./controller/MainController.php";
    $(()=>{
        init();
        if ('<?php echo $_SESSION["PERFIL"]; ?>' == '11')
            $('#title-complement').text('<?php echo " - Zona {$_SESSION['ZONA']}"?>')
        
        $('a.getData').click(function(event) {
            $('.set-hover').removeClass('set-hover');
            $(this).find('.card').toggleClass('hover set-hover');
            $('.wait').show('slow');
            $('.content_resumen').fadeOut('slow');

            let filter = $(this).data().filter;

            if (filter=="censo") {
                init(false);
            }else{
                let request = {
                    route:urlController,
                    data: {csrf_token:'<?php echo $csrf_token ?>',filter:filter,'opcion':'getDatas'}
                }
                let xkey;
                if (filter=="luminarias") {
                    xkey = "UBICACION_LUMINARIA";
                }else if (filter=="semaforos") {
                    xkey = "TIPO_SEMAFORO";
                }else{
                    xkey = "TIPO_C_DIRECTA";
                }
                Fetch(request).then((response)=>{
                    if (response.codigo===true) {
                        $('#chart_resumen').empty();
                        renderChartBar({
                            element:'chart_resumen',
                            data:response.data,
                            xkey:xkey,
                            ykeys:['TOTAL'],
                            type:'bar'
                        });

                        let total = 0;
                        let carga = 0;
                        let data;
                        $.each(response.data,function(index, row) {
                            total = total + parseInt(row.TOTAL);
                            carga = carga + parseInt(row.CARGA);
                        });
                        data = response.data;
                        if (filter=="luminarias") {
                            data.push({UBICACION_LUMINARIA:'TOTAL', CARGA:carga, TOTAL:total});
                        }else if (filter=="semaforos") {
                            data.push({TIPO_SEMAFORO:'TOTAL', CARGA:carga, TOTAL:total});
                        }else{
                            data.push({'TIPO_C_DIRECTA':'TOTAL', CARGA:carga, TOTAL:total});
                        }
                        renderTable('#tb_resumen',data);
                    } else if (typeof(response.message) != undefined){
                        notificar('', response.message ,'warning')
                    }
                    $('.wait').hide('slow');
                    $('.content_resumen').fadeIn('slow');
                });
            }
        });
    });

    init = (init=true) =>{
        $('#chart_resumen').empty();
        ajaxCallback(
            {opcion:"getDatasInit",csrf_token:'<?php echo $csrf_token ?>'},
            urlController,
            function (respuesta){
                let response = JSON.parse(respuesta);
                if (response.codigo==true) {
                    $('#chart_resumen').empty();
                    renderChartBar({
                        element:'chart_resumen',
                        data:response.data,
                        xkey:'TIPO',
                        ykeys:['TOTAL'],
                        type:'bar'
                    });
                    let total = 0;
                    let carga = 0;
                    let data;
                    $.each(response.data,function(index, row) {
                        total= total + parseInt(row.TOTAL);
                        carga = carga + parseInt(row.CARGA);
                    });

                    if(response.delegacion != null)
                        $('#title-complement').text(` - Delegación ${response.delegacion}`)
                    data = response.data;
                    data.push({TIPO:'TOTAL', CARGA: carga, TOTAL:total});
                    renderTable('#tb_resumen',data);

                    if( $('#resumenes-container').length != 0 )
                        llenar_tablas_zonas(response.data_by_zone);
                    
                    $("#count-lum").html(data.find(el => el.TIPO == "LUMINARIA" ).TOTAL);
                    $("#count-sem").html(data.find(el => el.TIPO == "SEMAFORO" ).TOTAL);
                    $("#count-car").html(data.find(el => el.TIPO == "CARGAS DIRECTAS" ).TOTAL);
                    $("#count-censo").html(total);
                } else if (typeof(response.message) != undefined){
                    notificar('', response.message ,'warning')
                }
                $('.wait').hide('slow');
                $('.content_resumen').fadeIn('slow');
            }
        );
    }

    function llenar_tablas_zonas(data_by_zone){
        let zonas = []
        $('#resumenes-container').html('')
        data_by_zone.forEach((el) => {
            if( !(el.NOMBRE in zonas) ){
                zonas[el.NOMBRE] = []
                $('#resumenes-container').append(
                    `<div class="col-md-4">
                        <div class="card border-success mt-5">
                            <h5 class="card-header text-center bg-success">RESUMEN ${el.NOMBRE}</h5>
                            <div class="card-body    pt-4 table-responsive" id="resumen_${el.NOMBRE}">
                                <table class="table table-sm table-bordered">
                                    <thead class="thead-green">
                                        <tr>
                                            <th>TIPO</th>
                                            <th>TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>`
                )
            }
            zonas[el.NOMBRE].push({"Tipo": el.TIPO, "Total": el.TOTAL})
        })
        Object.keys(zonas).forEach((el) => {
            let total_zona = 0
            zonas[el].forEach((una_zona) => {
                total_zona += parseInt(una_zona.Total)
            })
            zonas[el].push({'Tipo': 'TOTAL', "Total": total_zona})
            renderTable(`#resumen_${el}`, zonas[el])
        })
    }

</script>
