
    <div class="col-md-12" id="content-Municipio" style="display:none;"><!--TABLAS Y AVENCES MUNICIPIO-->
        <div class="row boderSALL">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <button class="btn btn-outline-secondary" id="btnBackSubdelegacion" data-toggle="tooltip" data-placement="right" title="">
                            <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                        </button>
                    </div>
                    <div class="col-md-10">
                        <h3 id="titleGraficaMunicipio" class="text-center"></h3>
                    </div>
                </div>
                <div id="graficaAvanceMunicipio" class="col-md-12 boxGraph">
                    
                </div>
            </div>
            <div class="col-md-12 text-center">
                <hr class="hrSeparator">
                <div class="" id="wait3" style="display:none;">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>    
                </div>
            </div>
            <div class="col-md-12" style="font-size: 12px;">
                <div class="col-md-12" id="tableAvancesMunicipio">
                    
                </div>
            </div>
        </div>
    </div>
