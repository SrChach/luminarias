
    <div class="col-md-12" id="content-Localidad" style="display:none;"><!--TABLAS Y AVENCES LOCALIDAD-->
        <div class="row boderSALL">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <button class="btn btn-outline-secondary" id="btnBackMunicipio" data-toggle="tooltip" data-placement="right" title="">
                            <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                        </button>
                    </div>
                    <div class="col-md-10">
                        <h3 id="titleGraficaLocalidad" class="text-center"></h3>
                    </div>
                </div>
                <div id="graficaAvanceLocalidad" class="col-md-12 boxGraph">
                    
                </div>
            </div>
            <div class="col-md-12 text-center">
                <hr class="hrSeparator">
                <div class="" id="wait4" style="display:none;">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>    
                </div>
            </div>
            <div class="col-md-12" style="font-size: 12px;">
                <div class="col-md-12" id="tableAvancesLocalidad">
                    
                </div>
            </div>
        </div>
    </div>




