
    <div class="col-md-12" id="content-Subdelegacion" style="display:none;"><!--TABLAS Y AVENCES SUBDELEGACIÓN-->
        <div class="row boderSALL">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <button class="btn btn-outline-secondary" id="btnBackDelegacion" data-toggle="tooltip" data-placement="right" title="">
                            <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                        </button>
                    </div>
                    <div class="col-md-10">
                        <h3 id="titleGraficaSubdelegacion" class="text-center"></h3>  
                    </div>
                </div>
                <div id="graficaAvanceSubDelegacion" class="col-md-12 boxGraph">
                    
                </div>
            </div>
            <div class="col-md-12 text-center">
                <hr class="hrSeparator">
                <div class="" id="wait2" style="display:none;">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>    
                </div>
            </div>
            <div class="col-md-12" style="font-size: 12px;">
                <div class="col-md-12" id="tableAvancesSubDelegacion">
                    
                </div>
            </div>
        </div>
    </div>
