<div class="col-md-12 pt-2" id="content-Delegacion" style="display:none;"><!--TABLAS Y AVENCES-->
    <div class="row boderSALL">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-1">
                    <button class="btn btn-outline-secondary" id="btnBack" data-toggle="tooltip" data-placement="right" title="">
                        <i class="fa fa-refresh" style="color:#276092;" id="iconMore"></i>
                    </button>
                </div>
                <div class="col-md-10">
                    <h3 id="titleGrafica" class="text-center">DELEGACIONES.</h3>    
                </div>
            </div>
            <div id="graficaAvance-Delegacion" class="col-md-12 boxGraph">
                    
            </div>
        </div>
        <div class="col-md-12 text-center">
            <hr class="hrSeparator">
            <div class="" id="wait1" style="display:none;">
                Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>    
            </div>
        </div>
        <div class="col-md-12" style="font-size: 12px; margin-top: 0px">
            <div class="col-md-12" id="tableAvances-Delegacion" style="margin-top: 0px;">
                    
            </div>
        </div>
    </div>
</div>