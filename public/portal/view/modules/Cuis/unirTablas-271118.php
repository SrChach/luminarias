<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pruebas Unir Tablas</title>
</head>
<body>
    <section class="container-fluid mt-5">

        <!-- Modal -->
        <div class="modal fade" id="modalConection" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg col-md-8 m-auto modal-dialog-centered" role="document">
                <div class="modal-content" style="max-heigth : 80vh;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal Titulo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <input type="hidden" name="tab" id="targetTab">
                            <input type="hidden" name="tab" id="hiddenKeyFirst">
                            <input type="hidden" name="tab" id="hiddenKeySecond">
                            <div class="col-12 row justify-content-center">
                                <div class="col-6 text-center">
                                    <button class="btn btn-primary btn-block">Limpiar</button>
                                </div>
                            </div>
                            <div class="col-12 row justify-content-center">
                                <div class="col-md-6 my-3" >
                                    <select name="" id="firstList" class="form-control" onchange="searchTable();"></select>
                                </div>
                                <div class="col-md-6 my-3" >
                                    <select name="" id="secondList" class="form-control" onchange="searchTable();"></select>
                                </div>
                            </div>

                            <div class="col-md-12" id="tableResult" style="max-width: 90%;max-height: 50vh;overflow: auto;"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="addRowTabUnion" onclick="searchTable('addFieldToTab');">Insertar</button>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script>
       
        let unionTab = './controller/unirTablas.php';
        let URL = '../../pillar/clases/controlador_pruebas.php';
        let searchFields = [];
        let firstList, secondList;
        let ObjectSend = {
            opcion : 'listaPreguntaDoc'
        }
        
        

        let request = petitionSupport(URL, ObjectSend);
        request.done(function(res){
            if(res != null && res != '' && res != undefined){
                
                let response = JSON.parse(res); 
                showModal(response);
            }
        });
    
        $(document).ready(function(){
            $('#modalConection').modal({
                keyboard: false,
                backdrop: 'static',
                show: true
            });
        });
        

        function petitionSupport(URL, ObjectSend){
            return $.ajax({
                method : 'POST',
                url : URL,
                data : ObjectSend 
            });
        }

        function searchTable(option = 'getTableFilter'){
            let valFirstList = $('#firstList').val();
            let valSecondList = $('#secondList').val();
            let tabName = $('#targetTab').val();
            let itemFieldFirst = $('#hiddenKeyFirst').val();
            let itemFieldSecond = $('#hiddenKeySecond').val();

            // if(valFirstList != '' || valSecondList != ''){
                let objectSend = {
                    opcion : option,
                    fieldOptions : {
                        firstField : itemFieldFirst,
                        firstOp : valFirstList,
                        secondField : itemFieldSecond,
                        secondOp : valSecondList,
                        tableName : tabName
                    }
                }

                let request = petitionSupport(unionTab, objectSend);

                request.done(function(res){
                    let response = JSON.parse(res);
                    if(response != false && response != null && response != undefined && response != ''){
                        $('#addRowTabUnion').hide();
                        if(option != 'getTableFilter')
                            alert('Inserción Correcta');
                        constructTable(tabName, response);
                    }else{
                        if(option != 'getTableFilter')
                            alert('Inserción Fallida');

                        $('#addRowTabUnion').show();
                        $('#tableResult').html('Sin Resultados');

                    }
                });
            // }   
        }
        
        
        function showModal(objectData){
            firstList = objectData['LISTA1'];
            secondList = objectData['LISTA2'];
            $('#firstList').html(constructTableUnion(firstList, 'hiddenKeyFirst'));
            $('#secondList').html(constructTableUnion(secondList, 'hiddenKeySecond'));
            
            objectSend = {
                opcion : 'getTableInfo',
                tableName : objectData['TABLAHIJA']
            }
            $('#targetTab').val(objectData['TABLAHIJA']);
            let secondRequest = petitionSupport(unionTab, objectSend);

            secondRequest.done(function(res){
                let secondResponse = JSON.parse(res);
                if(secondResponse != false && secondResponse != null && secondResponse != '' && secondResponse != undefined){
                    $('#addRowTabUnion').hide();
                    constructTable(objectData['TABLAHIJA'], secondResponse);
                }else{
                    $('#addRowTabUnion').show();
                    $('#tableResult').html('Sin Resultados');
                }
            });
        }


        function constructTableUnion(list, hiddenField){
            let key = '';
            let value = '';

            let listOptions = '<option value="">Seleccione...</option>';
            Object.keys(list).forEach(function(keyItem){
                list[keyItem].forEach(function(itemList){
                    if(itemList != undefined && itemList != null){
                        Object.keys(itemList).forEach(function(item){
                            if(item.substring(0,2).toUpperCase() == 'ID') {
                                key = item;
                            }else{
                                value = item;
                            }
                        });
                        if(key != null && key != '' && value != null && value != ''){
                            listOptions += '<option value="'+itemList[key]+'">'+itemList[value]+'</option>';
                        }
                    }
                });
            });
            // searchFields[0] = value;
            // console.log(searchFields[0]);
            
            $('#'+hiddenField).val(key);
            $('#'+hiddenField).html(value);

            return listOptions;
        }

        function constructTable(tableName = '', dataObject){
            let tableResponse = '<table class="table" id="tableUnion" style="font-size: .7em;">\
                    <thead>';

            Object.keys(dataObject[0]).forEach(function(item){
                if(item.toUpperCase() == 'ESTATUS')
                    tableResponse += '<th></th>';
                else
                    tableResponse += '<th>'+item+'</th>';
            });
            
            tableResponse += '</thead>\
                    <tbody>';

            
            dataObject.forEach(function(item){
                tableResponse += '<tr>';
                Object.keys(dataObject[0]).forEach(function(itemHeaders){
                    // console.log($('#hiddenKeyFirst').val());
                    if(itemHeaders.toUpperCase() == 'ESTATUS'){
                        console.log(item[itemHeaders]);
                        if(item[itemHeaders] == 1)
                            tableResponse += '<td><button class="btn btn-block btn-danger" onclick="updateFieldUnion(this, \''+tableName+'\', \'downState\');">Baja</button></td>';
                        if(item[itemHeaders] == 2)
                            tableResponse += '<td><button class="btn btn-block btn-primary" onclick="updateFieldUnion(this, \''+tableName+'\', \'upState\');">Alta</button></td>';
                    }
                    else if(itemHeaders == $('#hiddenKeyFirst').val()){
                        let valueTemp = firstList[item[itemHeaders]];
                        tableResponse += '<td data-name="'+itemHeaders+'" data-value="'+item[itemHeaders]+'">'+valueTemp[0][$('#hiddenKeyFirst').text()]+'</td>';
                    }
                    else if(itemHeaders == $('#hiddenKeySecond').val()){
                        let valueTempSecond = secondList[item[itemHeaders]];
                        tableResponse += '<td data-name="'+itemHeaders+'" data-value="'+item[itemHeaders]+'">'+valueTempSecond[0][$('#hiddenKeySecond').text()]+'</td>';
                    }
                    else{
                        if(itemHeaders.substring(0,2).toUpperCase() == 'ID') {
                            tableResponse += '<td data-name="'+itemHeaders+'" data-value="'+item[itemHeaders]+'">'+item[itemHeaders]+'</td>';
                        }else{
                            tableResponse += '<td>'+item[itemHeaders]+'</td>';
                        }
                    }
                });
                tableResponse += '<tr>';
            });

            tableResponse += '</tbody>\
                </table>';

            $('#tableResult').html(tableResponse);
            // tablePadron('#tableUnion', 'Reporte' , Object.keys(dataObject[0]).length)
            // settingsTable();
        }

        function updateFieldUnion(rowUpdate, tabName, option){
            let objectSend = {
                opcion : option,
                tabName : tabName,
                fieldsUpdate : {}
            };

            $(rowUpdate).parent().siblings('[data-name]').each(function(count, item){
                objectSend['fieldsUpdate'][count] = {
                    name : $(item).attr('data-name'),
                    fieldValue : $(item).attr('data-value')
                } 
            });

            let request = petitionSupport(unionTab, objectSend);

            request.done(function(res){
                let response = JSON.parse(res);
                if(response == 'true' || response == true){
                    alert('Actualización Correcta');
                    searchTable();
                }else{
                    alert('Error al realizar la actualización');
                }
            });

            request.fail(function(XHR, textStatus){
                console.log(textStatus);
            });
        }
        
    </script>
</body>
</html>