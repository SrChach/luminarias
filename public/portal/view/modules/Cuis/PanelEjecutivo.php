<style>
.padding-20-5{
    padding: 20px 5px;
}
.padding-50-0{
    padding: 50px 0px;
}
.padding-0-100{
    padding: 0 100px;
}
.margin-left-20{
    margin-left: 20px;
}

.encuestaTitle{
    border-top-right-radius: 25px;
    border-bottom-left-radius: 25px;
    border: 3px rgb(23,162,184);            
    padding: 5px 20px;
    box-shadow: 10px 10px rgba(22, 44, 44, 0.6);            
    background-color: #17a2b8;
    color: white;
    font-size: 1.5em;
    min-width: 250px;
    margin: 0 30px 30px 50px;
}

.icon-encuesta{
    transition: all .3s linear;
    position: absolute;
    top: -10px;
    right: 10px;
    z-index: 0;
    font-size: 90px;
    color: rgba(0,0,0,.15);
}

.conteoTotal{
    color: white;
    margin: 20px 0;
}

.loaderTable{
    margin: auto;
    vertical-align: middle;
    padding: 50px 0;
    width: auto;
    height: auto;
    max-width: 150px;
    max-height: 250px;
    /* display: none; */
}    

.loaderTableStandar{
    margin: auto;
    vertical-align: middle;
    padding: 50px 0;            
    font-size: 6rem;
    color: #007bff;
    /*color: #28A6DE;  Color estandar para el loader */
    display: none;
    top: 25%;
}

.margin-50{
    margin: 50px 0;
}

    /*.bloqueo {
        position: absolute;
        top: 0px;
        left: 0px;
        height:100%;
        width:100%;        
        z-index: 50;       
        background: #ffffff00;              
        }*/

        table > tbody > tr:hover{        
            cursor: pointer;
        }

        .destroyMorris{
            min-height: 430px;
        }

        .tabDelegaciones_filter:parent{
            background: red;
            width: 100%;
        }
        .levelUp{
            display: none;
        }

        table {
            width: 100% !important;
            font-size: .8rem;
        }

        .bloqueo{
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;       
            overflow: hidden;
            outline: 0;        
            position: fixed;
            background-color: rgba(154, 161, 164, 0.2);
            z-index: 10;
        }   
        .loaderStandar{
            margin: auto;
            vertical-align: middle;        
            font-size: 6rem;
            color: #2bb273;
            position: absolute;
            top: 45%;
            margin-left: 42.5%;
            margin-right: 42.5%;
        }
        .bold{
            font-weight: bold;
        }
    </style>


    <div class="bloqueo text-center">
        <div class="loaderStandar text-center">        
            <h4 class="text-center bold">Cargando ...</h4>
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </div>
    <div class="container-fluid">
        <section class="container-fluid">
            <div class="row">
                <div class="col-lg-4 "> <!-- padding-50-0 -->
                <!-- <div class="container-fluid text-center" id="optionsTable">
                    <div class="btn-group mb-5">
                        <button type="button" class="btn btn-outline-secondary" id="exportTableExcel" name="tableExpExc">Excel <i class="fa fa-file-excel-o" aria-hidden="true" style="padding-left:10%;"></i></button>
                        <button type="button" class="btn btn-outline-secondary">Imprimir Datos <i class="fa fa-print" aria-hidden="true" style="padding-left:5%;"></i></button>
                        <div class="btn-group">
                            <div class="dropdown dropright">
                                <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" style="border-top-left-radius: 0;border-bottom-left-radius: 0;">
                                    Ver Columnas
                                </button>
                                <div class="dropdown-menu" id="tableColumnsOp"></div>
                            </div>
                        </div>
                    </div>                
                </div> -->
                <div class="container-fluid my-5" id="primerNivelDelg"></div>
            </div>
            <div class="col-lg-8 container-levels">
                <div class="row justify-content-end">
                    <div class="col-md-3 encuestaTitle">
                        <label for="" >Encuesta</label>
                        <label for="" class="conteoTotal form-text">Total: <span id="totalRegistro"></span></label>
                        <div class="icon-encuesta">
                            <i class="icon ion-arrow-graph-up-right"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="loaderTable margin-50"> -->
                    <!-- <i class="fa fa-spinner fa-spin loaderTableStandar"></i> -->
                </div>
                <a href="#" class="btn btn-info btn-md margin-left-20 levelUp" onclick="cambiarNiveles();">
                    <span class="fa fa-level-up"></span>
                </a>
                <div id="contentGraf"></div>
                <!-- <div id="primerNivelGf"></div>
                <div id="segundoNivelGf" style="display: none;"></div>
                <div id="tercerNivelGf" style="display: none;"></div>
                <div id="cuartoNivelGf" style="display: none;"></div> -->                
            </div>
        </div>
    </section>


    <script>
        var MorrisBar;
        var objectContex = { 
            nivel: 1, 
            delegaciones: { 
                delegTab: '', 
                total: 0,
                morrisBar: {
                    idElement: null,
                    response: null,
                    valoresX: null,
                    valoresY: null,
                    labelHover: null
                } 
            }, 
            subDelegaciones: { 
                subDelegTab: '', 
                total: 0,
                morrisBar: {
                    idElement: null,
                    response: null,
                    valoresX: null,
                    valoresY: null,
                    labelHover: null
                } 
            }, 
            municipios: { 
                munTab: '', 
                total: 0,
                morrisBar: {
                    idElement: null,
                    response: null,
                    valoresX: null,
                    valoresY: null,
                    labelHover: null
                }
            },
            localidades: { 
                localTab: '', 
                total: 0,
                // morrisBar: {
                //     idElement: null,
                //     response: null,
                //     valoresX: null,
                //     valoresY: null,
                //     labelHover: null
                // }
            }  
        };


        $(function(){
            getDelegaciones();            
        });
        
        
        $("#exportTableExcel").click(function(e) {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#tabDelegaciones').html()));
            e.preventDefault();
        });

        function getDelegaciones(){            
            $('.loaderTableStandar').show();            
            let URL = '../../public/portal/controller/panelEjecutivo_controller.php';
            let METODO = 'POST';
            let objectData = { opcion: 'generales_cuis'};
            let request = petitionSupport(URL, METODO, objectData);

            request.done(function(res){
                let response = JSON.parse(res);
                let arrayHeadTab = ['OFICINA REGIONAL','CONTEO'];                
                let arrayBodyTab = ['DELEGACION','CONTEO'];
                let idTabla = 'tabDelegaciones';
                
                objectContex.nivel = 1;                                
                objectContex.delegaciones.delegTab = construcTable(arrayHeadTab, response, arrayBodyTab, idTabla, 'CONTEO', 'totalDel', 'ID_DEL');                
                objectContex.delegaciones.morrisBar.response =  response;
                objectContex.delegaciones.morrisBar.valoresX =  'DELEGACION';
                objectContex.delegaciones.morrisBar.valoresY =  'CONTEO';
                objectContex.delegaciones.morrisBar.labelHover =  'CONTEO';
                $('#tableColumnsOp').html(construcOpTable(arrayBodyTab));
                $('#primerNivelDelg').html(objectContex.delegaciones.delegTab);
                $('#contentGraf').empty();
                

                // $('.ftnNextTab').on('click', { objTh: $(this) }, getChangeLevels);
                tableExport2('#'+idTabla,'Niveles',1, '');


                newElemMorris(response, 'DELEGACION', 'CONTEO', 'CONTEO');                                
                // console.log(response);
                objectContex.delegaciones.total = $('#totalDel').text();
                $('#totalRegistro').html($('#totalDel').text());
                $('.levelUp').show('slow');
                $('.loaderTableStandar').hide();
                $('.bloqueo').hide();
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.loaderTableStandar').hide();
                $('.bloqueo').hide();
            });
        }                                
        





        function getSubdelegaciones(ID_DEL,row){
            $('.bloqueo').show();
            $('.loaderTableStandar').show();
            let URL = '../../public/portal/controller/panelEjecutivo_controller.php';
            let METODO = 'POST';
            let objectData = { opcion: 'generales_cuis', delegacion: ID_DEL };
            let request = petitionSupport(URL, METODO, objectData);
            
            request.done(function(res){
                let response = JSON.parse(res);                
                let arrayHeadTab = ['OFICINA SUBREGIONAL', 'CONTEO'];
                let arrayBodyTab = ['SUBDELEGACION', 'CONTEO'];
                let idTabla = 'tabSubDelegaciones';
                
                objectContex.nivel = 2;                
                objectContex.subDelegaciones.subDelegTab = construcTable(arrayHeadTab, response, arrayBodyTab, idTabla, 'CONTEO', 'totalSubDel', 'ID_SUB');
                objectContex.subDelegaciones.morrisBar.response =  response;
                objectContex.subDelegaciones.morrisBar.valoresX =  'SUBDELEGACION';
                objectContex.subDelegaciones.morrisBar.valoresY =  'CONTEO';
                objectContex.subDelegaciones.morrisBar.labelHover =  'CONTEO';
                $('#tableColumnsOp').html(construcOpTable(arrayBodyTab));
                $('#primerNivelDelg').html(objectContex.subDelegaciones.subDelegTab);
                $('#contentGraf').empty();                
                
                
                tableExport2('#'+idTabla,'Niveles',arrayBodyTab.length, '');
                newElemMorris(response, 'SUBDELEGACION', 'CONTEO', 'CONTEO');                

                objectContex.subDelegaciones.total = $('#totalSubDel').text();
                $('#totalRegistro').html($('#totalSubDel').text());                
                $('.loaderTableStandar').hide();
                $('.bloqueo').hide();
                
            });
            
            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.loaderTableStandar').hide();
                $('.bloqueo').hide();
            });
        }
        
        function getMunicipios(ID_SUB){
            $('.loaderTableStandar').show();
            $('.bloqueo').show();
            let URL = '../../public/portal/controller/panelEjecutivo_controller.php';
            let METODO = 'POST';
            let objectData = { opcion: 'generales_cuis', subdelegacion: ID_SUB};
            let request = petitionSupport(URL, METODO, objectData);
            
            request.done(function(res){
                let response = JSON.parse(res);
                let arrayHeadTab = ['MUNICIPIO', 'CONTEO'];
                let arrayBodyTab = ['NOM_MUNICIPIO', 'CONTEO'];
                let idTabla = 'tabMunicipios';
                
                objectContex.nivel = 3;                
                objectContex.municipios.munTab = construcTable(arrayHeadTab, response, arrayBodyTab, idTabla, 'CONTEO', 'totalMunip', 'CVE_MUNICIPIO');
                objectContex.municipios.morrisBar.response =  response;
                objectContex.municipios.morrisBar.valoresX =  'NOM_MUNICIPIO';
                objectContex.municipios.morrisBar.valoresY =  'CONTEO';
                objectContex.municipios.morrisBar.labelHover =  'CONTEO';
                $('#tableColumnsOp').html(construcOpTable(arrayBodyTab));
                $('#primerNivelDelg').html(objectContex.municipios.munTab);
                $('#contentGraf').empty();

                tableExport2('#'+idTabla,'Niveles',arrayBodyTab.length, '');
                newElemMorris(response, 'NOM_MUNICIPIO', 'CONTEO', 'CONTEO');                
                
                objectContex.municipios.total = $('#totalMunip').text();
                $('#totalRegistro').html($('#totalMunip').text());                
                $('.loaderTableStandar').hide();
                $('.bloqueo').hide();
                
            });
            
            request.fail(function(jqHXR, textStatus){
                consol.log(textStatus);
                $('.loaderTableStandar').hide();
                $('.bloqueo').hide();
            });
        }
        
        function getLocalidad(CVE_MUNICIPIO){
            $('.loaderTableStandar').show();
            $('.bloqueo').show();
            let URL = '../../public/portal/controller/panelEjecutivo_controller.php';
            let METODO = 'POST';
            let objectData = { opcion: 'generales_cuis', municipio: CVE_MUNICIPIO};
            let request = petitionSupport(URL, METODO, objectData);
            
            request.done(function(res){
                let response = JSON.parse(res);                
                let arrayHeadTab = ['LOCALIDAD', 'CONTEO'];
                let arrayBodyTab = ['NOM_LOCALIDAD', 'CONTEO'];
                let idTabla = 'tabLocalidades';
                
                objectContex.nivel = 4;                
                objectContex.localidades.localTab = construcTable(arrayHeadTab, response, arrayBodyTab, idTabla, 'CONTEO', 'totalLocad');
                // objectContex.localidades.morrisBar.response =  response;
                // objectContex.localidades.morrisBar.valoresX =  'NOM_LOCALIDAD';
                // objectContex.localidades.morrisBar.valoresY =  'CONTEO';
                // objectContex.localidades.morrisBar.labelHover =  'CONTEO';
                $('#tableColumnsOp').html(construcOpTable(arrayBodyTab));
                $('#primerNivelDelg').html(objectContex.localidades.localTab);
                $('#contentGraf').empty();

                // $('#tercerNivelGf').hide('slow');
                // $('#cuartoNivelGf').show('slow');                
                tableExport2('#'+idTabla,'Niveles',arrayBodyTab.length, '');
                newElemMorris(response, 'NOM_LOCALIDAD', 'CONTEO', 'CONTEO');
                
                objectContex.localidades.total = $('#totalLocad').text();
                $('#totalRegistro').html($('#totalLocad').text());                
                $('.loaderTableStandar').hide();
                $('.bloqueo').hide();
            });
            
            request.fail(function(jqHXR, textStatus){
                console.log(textStatus);
                $('.loaderTableStandar').hide();
                $('.bloqueo').hide();
            });
        }
        
        //Sin utilidad pór el momento Recibe un objecto con los encabezados de la grafica
        function construcOpTable(objectElem){
            let tableColumnsOp = "";
            objectElem.forEach(function(item, index){
                tableColumnsOp += "<a class='dropdown-item toggle-vis' data-column='"+index+"'>"+item+"</a>"
            });
            
            return tableColumnsOp;
        }


        function getChangeLevels(objectElement){            
            let optionNext = objectElement.getAttribute('data-typeOp');
            let idOption = objectElement.getAttribute('id'); 
            if(optionNext == "ID_DEL"){
                getSubdelegaciones(idOption);
            }else if(optionNext == "ID_SUB"){
                getMunicipios(idOption);                
            }else if(optionNext == "CVE_MUNICIPIO"){
                getLocalidad(idOption);
            }
        }

        function cambiarNiveles(){
            if(objectContex.nivel == 2){  
                //Pinta tabla delegaciones
                $('#totalRegistro').html(objectContex.delegaciones.total);      
                $('#primerNivelDelg').html(objectContex.delegaciones.delegTab);

                $('#contentGraf').empty();
                newElemMorris(
                    objectContex.delegaciones.morrisBar.response,
                    objectContex.delegaciones.morrisBar.valoresX,
                    objectContex.delegaciones.morrisBar.valoresY,
                    objectContex.delegaciones.morrisBar.labelHover);
                tablePadron('#tabDelegaciones','Niveles',2);
                
                // $('#segundoNivelGf').hide('slow');
                // $('#primerNivelGf').show('slow');
                objectContex.nivel = 1;
            }else if(objectContex.nivel == 3){                
                //Pinta tabla subdelegaciones
                $('#totalRegistro').html(objectContex.subDelegaciones.total);
                $('#primerNivelDelg').html(objectContex.subDelegaciones.subDelegTab);
                
                $('#contentGraf').empty();
                newElemMorris(
                    objectContex.subDelegaciones.morrisBar.response,
                    objectContex.subDelegaciones.morrisBar.valoresX,
                    objectContex.subDelegaciones.morrisBar.valoresY,
                    objectContex.subDelegaciones.morrisBar.labelHover);
                tablePadron('#tabSubDelegaciones','Niveles',2);
                
                // $('#tercerNivelGf').hide('slow');
                // $('#segundoNivelGf').show('slow');
                objectContex.nivel = 2;
            }else if(objectContex.nivel == 4){                
                //Pinta tabla subdelegaciones
                $('#totalRegistro').html(objectContex.municipios.total);
                $('#primerNivelDelg').html(objectContex.municipios.munTab);
                
                $('#contentGraf').empty();
                newElemMorris(
                    objectContex.municipios.morrisBar.response,
                    objectContex.municipios.morrisBar.valoresX,
                    objectContex.municipios.morrisBar.valoresY,
                    objectContex.municipios.morrisBar.labelHover);
                tablePadron('#tabMunicipios','Niveles',2);
                
                // $('#cuartoNivelGf').hide('slow');
                // $('#tercerNivelGf').show('slow');
                objectContex.nivel = 3;
            }                            
        }

        function newElemMorris(response, valoresX, valoresY, labelHover){
            $('#contentGraf').html();
            $('#contentGraf').html('<div id="destroyMorris" class="destroyMorris"></div>');
            var btnDownload=``;
            MorrisBar = Morris.Bar({
                element: 'destroyMorris',
                data: response,
                xkey: valoresX,
                ykeys: [valoresY, ''],
                labels: [labelHover,`<button type="button" id="oto" class="btn btn-link" >REPORTE<i class="fa fa-download"></i></button>`],
                hideHover: 'auto',
                resize: true,
                xLabelAngle: 10,
                gridTextSize: 6,
                gridTextWeight: 900,                                   
            });





            MorrisBar.on('click', function(i, row){
                if (row) {
                    generaReporte(valoresX,row);
                } else {
                    alert("sin datos");
                }

                   //  if('ID_DEL' in row){
                   //     getSubdelegaciones(row.ID_DEL,row);
                   // }
                   //  if('ID_SUB' in row){
                   //      getMunicipios(row.ID_SUB);
                   //  }
                   //  if('CVE_MUNICIPIO' in row){
                   //      getLocalidad(row.CVE_MUNICIPIO);
                   //  }

               });

            // contMorris++;
        }



        function generaReporte(tipo,row){
            var verif = row === undefined ? 0: 1;


            if ( verif != 0 ) {
                var idP=getParameterByName('index');
                var nivel=tipo;
                var cve_tipo=row[nivel];
                var idUsuarioLogin=idusuario;
            } 


            urlEjectutivo='./controller/repoPanelEjecutivo.php';
            var paramsEvento={idP:idP,nivel:nivel,cve_tipo:cve_tipo,idUsuarioLogin:idUsuarioLogin}
            objet={opcion:'generaRepoPanelEjec',values:paramsEvento};
            alert("Cuando el reporte termine de generarse se enviará una notificación al correo con el que inicio sesión.");

            ajaxCallback(objet,urlEjectutivo,function (respuesta){
              resRepoEje = JSON.parse(respuesta);
              console.log(resRepoEje);


                //  if('ID_DEL' in row){
                //      alert("delegacion")
                //  }
                //  if('ID_SUB' in row){
                //     alert("subdelegacion")
                // }
                // if('CVE_MUNICIPIO' in row){
                //      alert("municipi")
                // }



            });
        };


















        /*Opciones que Recibe  
            Titulos cabecera / Resultados / Valores / ID para la tabla / Clave del campo para formato moneda / El id para el campo del total
            trId = El campo identificador de la tabla actual*/        
            function construcTable(arrayHeadTab, objectData, arrayBodyTab, idTabla, indexNumber, idTotalTable, trId){
                let total = 0;
                let structTable = "<table class='table table-bordered table-condensed table-hover'  id='"+idTabla+"'>\
                <thead class='thead-light'>\
                <tr>";

                arrayHeadTab.forEach(function(item){
                    structTable += "<th>"+item+"</th>"
                });

                structTable += "</tr>\
                </thead>\
                <tbody>";

                objectData.forEach(function(item){
                    structTable += "<tr id='"+item[trId]+"' data-typeOp='"+trId+"' class='ftnNextTab' onclick='getChangeLevels(this);'>";
                    arrayBodyTab.forEach(function(itemBody){
                        if(indexNumber == itemBody){
                            total += parseInt(item[itemBody]);
                            structTable += "<td>"+number_format(item[itemBody])+"</td>";
                        }else{
                            structTable += "<td>"+item[itemBody]+"</td>";
                        }
                    });
                    structTable += "</tr>";

                });

                structTable += "</tbody>\
                <tfoot class='thead-light'>\
                <tr>\
                <th>Total</th>\
                <th id="+idTotalTable+">"+number_format(total)+"</th>\
                </tr>\
                </tfoot>\
                </table";

                return structTable;
            }

            function petitionSupport(URL, METODO, objectData){
                return $.ajax({
                    url: URL,
                    method: METODO,
                    data: objectData,
                    dataType: 'HTML'
                });
            }    
        </script>



    </div>
<!-- </body>
</html> -->