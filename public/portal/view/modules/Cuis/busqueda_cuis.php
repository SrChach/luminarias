<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
error_reporting(-1);
ini_set('display_errors', 'On');

?>
<style>
.min-width{
    min-width: 100px;
}
.formatImgLoading{
    width: 23px;
    height: 20px;
    display: none;
}
.loaderTable{
    margin: auto;
    vertical-align: middle;
    padding: 50px 0;
    width: auto;
    height: auto;
    max-width: 150px;
    max-height: 250px;
    display: none;
}

.loadingStandar{
    font-size: large;
    margin-right: .4rem;
}

.loaderTableStandar{
    margin: auto;
    vertical-align: middle;
    padding: 50px 0;
    font-size: 6rem;
    color: #007bff;
    /*color: #28A6DE;  Color estandar para el loader */
    display: none;
    top: 25%;
}
</style>


<section class='container-fluid'>
    <div class="row">
        <div class="col-md-3 row">
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="delegacion">
                            <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="formatImgLoading" id='loadingDelg'>Delegación -->
                            <i class="fa fa-spinner fa-spin loadingStandar" id='loadingDelg'></i>Oficina Regional
                        </label>
                    </div>
                    <select class="custom-select" id="delegacion" onchange="construcSubDelg();">
                        <option value="">Selecciona...</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="subDelegacion">Oficina Subregional</label>
                    </div>
                    <select class="custom-select" id="subDelegacion">
                        <option value='' selected>Selecciona...</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">
                            <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="formatImgLoading" id='loadingEst'>Estado -->
                            <i class="fa fa-spinner fa-spin loadingStandar" id='loadingEst'></i>Estado
                        </label>
                    </div>
                    <select class="custom-select" id="estados" onchange="construcMcp();">
                        <option value='' selected>Selecciona...</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">Municipio</label>
                    </div>
                    <select class="custom-select" id="municipios" onchange="construcLoc();">
                        <option value='' selected>Selecciona...</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">Localidad</label>
                    </div>
                    <select class="custom-select" id="localidades">
                        <option value='' selected>Selecciona...</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">Folio CUIS: </label>
                    </div>
                    <input type="text" class='form-control' id="folioCuis" onchange="disableElements('folioCuis', 'nombre');">
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">Nombre: </label>
                    </div>
                    <input type="text" class='form-control' id="nombre" onchange="disableElements('nombre', 'folioCuis');">
                </div>
            </div>

            <div class="col-md-6">
                <button class='btn btn-primary btn-block' onclick="getTableCuis();">Buscar</button>
            </div>
            <div class="col-md-6">
                <button class='btn btn-primary btn-block' onclick="downloadRepo();">Descargar</button>
            </div>
        </div>
        <div class="col-md-9">
            <div class="container-fluid">
                <label for="" class='form-text'>Numero de Registros: <span id="numRegistros"</span></label>
                </div>
                <div class="row justify-content-center">
                    <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="loaderTable"> -->
                    <i class="fa fa-spinner fa-spin loaderTableStandar"></i>
                    <div class="col-md-12" id="resultados"></div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(function(){
        getOptions();
    });
        var resultsGeneral;

    function getOptions(){
        let URL = '../../public/portal/controller/busquedaCuis_controller.php';
        let METODO = 'POST';
        let objectData = { opcion: 'inicioBusquedaCUIS' }

        $('#loadingDelg').show('slow');
        $('#loadingEst').show('slow');

        var request = petitionSupport(URL, METODO, objectData);

        request.done(function(res){
            resultsGeneral = JSON.parse(res);
            // console.log(resultsGeneral);
            let newOpDeleg = "<option value='' selected>Selecciona...</option>";
            newOpDeleg += constructOptions(resultsGeneral.delegaciones, 'ID_DEL', 'DELEGACION');
            $('#delegacion').html(newOpDeleg);

            let newOpEst = "<option value='' selected>Selecciona...</option>";
            newOpEst += constructOptions(resultsGeneral.estados, 'CVE_ENTIDAD_FEDERATIVA', 'NOMBRE');
            $('#estados').html(newOpEst);

            $('#loadingDelg').hide('slow');
            $('#loadingEst').hide('slow');
        });

        request.fail(function(jqXHR, textStatus){
            console.log('Error: ', textStatus);
            $('#loadingDelg').hide('slow');
            $('#loadingEst').hide('slow');
        });
    }






    function petitionSupport(URL, METODO, objectData){
        return $.ajax({
            url: URL,
            method: METODO,
            data: objectData,
            dataType: "HTML"
        });
    }

    function construcSubDelg(){
        let subDelgSelect = $('#delegacion option:selected').val();
        let newOpSubDeleg = "<option value='' selected>Selecciona...</option>";

        var disableOptions = false;
        if(subDelgSelect != '' && subDelgSelect != null){
            newOpSubDeleg += constructOptions(resultsGeneral.subdelegaciones[subDelgSelect], 'ID_SUB', 'NOMBRE');
            disableOptions = true;
        }
        changeSelectDefault("estados", disableOptions);
        changeSelectDefault("municipios", disableOptions);
        changeSelectDefault("localidades", disableOptions);
        changeInputDefault("folioCuis", disableOptions);
        changeInputDefault("nombre", disableOptions);
        $('#subDelegacion').html(newOpSubDeleg);
    }

    function construcMcp(){
        let selectEst = $('#estados option:selected').val();
        let newOpMcp = "<option value='' selected>Selecciona...</option>";

        var disableOptions = false;
        if(selectEst != '' && selectEst != null){
            newOpMcp += constructOptions(resultsGeneral.municipios[selectEst], 'CVE_MUNICIPIO', 'NOMBRE');
            $('#municipios').html(newOpMcp);
            disableOptions = true;
        }
        else{
            $('#municipios').html(newOpMcp);
            construcLoc();
            disableOptions = false;
        }

        changeSelectDefault("delegacion", disableOptions);
        changeSelectDefault("subDelegacion", disableOptions);
        changeInputDefault("folioCuis", disableOptions);
        changeInputDefault("nombre", disableOptions);
    }

    function construcLoc(){
        let selectLoc = $('#municipios option:selected').val();
        let newOpLoc = "<option value='' selected>Selecciona...</option>";
        if(selectLoc != '' && selectLoc != null)newOpLoc += constructOptions(resultsGeneral.localidades[selectLoc], 'CVE_LOCALIDAD', 'NOMBRE');
        $('#localidades').html(newOpLoc);
    }

    function changeSelectDefault(idSelect, option){
        document.getElementById(idSelect).options.selectedIndex = 0;
        $('#'+idSelect).attr('disabled', option);
    }
    function changeInputDefault(idInputText, option){
        $('#'+idInputText).attr('disabled', option);
        $('#'+idInputText).val('');
    }

    function disableElements(idInput, idInputSecond){
        var disableOptions = false;

        if($('#'+idInput).val() != "" && $('#'+idInput).val() != null){
            disableOptions = true;
        }else{
            if($('#'+idInputSecond).val() != "" && $('#'+idInputSecond).val() != null){
                disableOptions = true;
            }
        }

        changeSelectDefault("delegacion", disableOptions);
        changeSelectDefault("subDelegacion", disableOptions);
        changeSelectDefault("estados", disableOptions);
        changeSelectDefault("municipios", disableOptions);
        changeSelectDefault("localidades", disableOptions);
    }



    //Recibe un array de tipo objeto ejemplo delegaciones { 0:{ Id: '9', delegacion: 'coat'}}
    // Los parametros son el objeto, value = indice del objeto para obtener el value, text Value el indice del objeto que contiene el valor a mostrar el el select
    // optionSelected = opcion que se seleccionara por default
    function constructOptions(objectData, value, textValue, optionSelected = 0){
        var options = "";
        objectData.forEach(function(item){
            let select = (item == optionSelected[value]) ? 'selected' : "";
            options += "<option value='"+item[value]+"' "+select+">"+item[textValue]+"</option>"
        });

        return options;
    }

    function getTableCuis(){
        let delegacion = $('#delegacion option:selected').val();
        let subDelegacion = $('#subDelegacion option:selected').val();
        let estado = $('#estados option:selected').val();
        let municipio = $('#municipios option:selected').val();
        let localidad = $('#localidades option:selected').val();
        let folioCuis = $('#folioCuis').val();
        let nombre = $('#nombre').val();

        let URL = '../../public/portal/controller/busquedaCuis_controller.php';
        let METODO = 'POST';
        let objectData = { opcion: 'buscarCUIS', delegacion: delegacion, subdelegacion: subDelegacion, estado: estado, municipio: municipio, localidad: localidad, folio_cuis: folioCuis, nombre: nombre };
        let request = petitionSupport(URL, METODO, objectData);

        $('#resultados').hide('slow');
        $('.loaderTableStandar').show('slow');
        $('.tooltip').hide();
        request.done(function(res){
            let response = JSON.parse(res);

            $('#numRegistros').html(response.generales.CONTEO);
            $('.loaderTableStandar').hide('slow');
            $('#resultados').show('slow');

            createTableCuis(response.detalle);
            // tablePadron('#tbCuis','X',11);
        });

        request.fail(function(jqXHR, textStatus){
            $('.loaderTableStandar').hide('slow');
            $('#resultados').show('slow');
            console.log('Error: ', textStatus);
        });
    }




   function createTableCuis(datas) {

        var cadena = "<table class='table table-bordered table-condensed table-striped  table-hover'  id='tbCuis'>\
        <thead class='thead-light' style='font-size:12px;'>\
        <tr>\
        <th>#</th>\
        <th>FOLIOCUIS</th>\
        <th>NB_NOMBRE</th>\
        <th>NB_PRIMER_AP</th>\
        <th>NB_SEGUNDO_AP</th>\
        <th>OFICINA REGIONAL</th>\
        <th>OFICINA SUBREGIONAL</th>\
        <th>CVE_MUNICIPIO</th>\
        <th>NOM_MUNICIPIO</th>\
        <th>CVE_LOCALIDAD</th>\
        <th>NOM_LOCALIDAD</th>\
        <th>NUMERO_BENEFICIOS</th>\
        <th></th>\
        </tr>\
        </thead>\
        <tbody>";

        var option='';
        $.each(datas,function (i,item) {
                    //if (item.IDPROGRAMA==1) {
                        option=hrefCuis(item.FOLIO_CUIS);
                // }else{
                //     option="#!";
                // }

                cadena += "<tr style='font-size:11px;'>\
                <th>"+(i+1)+"</th>\
                <td><a href='#!'><span class='click' data-toggle='tooltip' data-html='true' data-placement='right' title='<a href=\""+hrefCuis(item.FOLIO_CUIS)+"\" target=\"_blank\">CONSULTA FOLIOCUIS</a> <br> <a href=\""+hrefExpediente(item.FOLIO_CUIS)+"\" target=\"_blank\">CONSULTA EXPEDIENTE</a>' >"+item.FOLIO_CUIS+"</span></a></td>\
                <td>"+item.NB_NOMBRE+"</td>\
                <td>"+item.NB_PRIMER_AP+"</td>\
                <td>"+item.NB_SEGUNDO_AP+"</td>\
                <td>"+item.DELEGACION+"</td>\
                <td>"+item.SUBDELEGACION+"</td>\
                <td>"+item.CVE_MUNICIPIO+"</td>\
                <td>"+item.NOM_MUNICIPIO+"</td>\
                <td>"+item.CVE_LOCALIDAD+"</td>\
                <td>"+item.NOM_LOCALIDAD+"</td>\
                <td>"+item.NUMERO_BENEFICIOS+"</td>\
                <td>\
                <a href='"+option+"' target='_blank'>\
                <i class='fa fa-share' style='font-size:25px;'></i>\
                </a>\
                </td>\
                </tr>";
            });

        cadena+="</tbody>\
        </table>";
        $('#resultados').html(cadena);

        $('.click').tooltip({trigger: "click"});
        $('[data-toggle="tooltip"]').tooltip();

        tablePadron('#tbCuis','CUIS',13)
        settingsTable();
    }



    function downloadRepo(){
        let delegacion = $('#delegacion option:selected').val();
        let subDelegacion = $('#subDelegacion option:selected').val();
        let estado = $('#estados option:selected').val();
        let municipio = $('#municipios option:selected').val();
        let localidad = $('#localidades option:selected').val();
        let folioCuis = $('#folioCuis').val();
        let nombre = $('#nombre').val();

        let URL = '../../public/portal/controller/busquedaCuis_controller.php';
        let METODO = 'POST';
        let objectData = { opcion: 'buscarCUIS', delegacion: delegacion, subdelegacion: subDelegacion, estado: estado, municipio: municipio, localidad: localidad, folio_cuis: folioCuis, nombre: nombre , idUsuario:idusuario,tipo:'complete'};
        alert("Cuando el reporte termine de generarse se enviará una notificación al correo con el que inicio sesión.");

        ajaxCallback(objectData,URL,function (respuesta){
            res = JSON.parse(respuesta);

        });
        //    let request = petitionSupport(URL, METODO, objectData);
        //    request.done(function(res){
        //     let response = JSON.parse(res);
        //     console.log(response);

        // });
    }



</script>
