<style>
    /* ------------------------------------------ Estilos boton flotante ------------------------*/
    .contenedor{
        width: 90px;
        height: 240px;
        position: fixed;
        right: 0px;
        top: -65px;
    }
    .botonF1:focus, .btnFloat:focus, btnCircleEstyle:focus{
        outline: none;
    }
    .botonF1 > button:focus, .btnFloat > button:focus, btnCircleEstyle > button:focus{
        outline: none;
    }
    .botonF1{
        width:50px;
        height:50px;
        border-radius:100%;
        background:#F44336;
        right:0;
        bottom:0;
        position:absolute;
        margin-right:25px;
        margin-bottom:16px;
        border:none;
        outline:none;
        color:#FFF;
        font-size:36px;
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        transition:.3s;
    }
    span{
        transition:.5s;
    }
    .botonF1:hover span{
        transform:rotate(360deg);
    }
    .botonF1:active{
        transform:scale(1.1);
    }
    .btnFloat{
        width: 45px;
        height: 45px;
        border-radius: 100%;
        border: none;
        color: #FFF;
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        font-size: 28px;
        outline: none;
        position: absolute;
        right: 0;
        top: 0;
        margin-right: 26px;
        transform: scale(0);
    }
    .botonF2{
        background: #2196F3;
        /* margin-top: 240px; */
        margin-top: 170px;
        transition: 0.5s;
    }

    .botonF3{
        background: #17a2b8;
        /* margin-top: 290px; */
        margin-top: 220px;
        transition: 0.5s;
    }

    .animacionVer{
        transform:scale(1);
    }

    .btnCircleEstyle{
        width: 45px;
        height: 45px;
        border-radius: 100%;
        background: #007bff;
        color: white;
        border: none;
    }
</style>

<style>
    .sectionsMargTop-50{
        margin-top: 50px;
    }
    .headerSectionTitle{
        text-align: center;
        background-color: #65b7de;
        margin-top: 30px;
        border-radius: .2em;
        padding: 5px;
    }
    .contentPadding-15{
        padding-top: 15px;
    }
    .labelSpace-15{
        margin-top: 15px;
    }
    .labelSpaceBottom{
        margin-bottom: 25px;
    }
    .frameMapSize{
        max-height: 200px;
        max-width: 350px;
    }
    .frameMapSize > iframe {
        max-height: -webkit-fill-available;
        max-height : -moz-available;
        max-width: -webkit-fill-available;
        max-width : -moz-available;
    }


    .inputStyle{
         width: 75%;
        border-radius: 6px;
        padding: 5px;
        min-width: 200px;
        vertical-align: middle;
    }

    .selectStyle{
        width: -webkit-fill-available !important;
        width: -moz-available !important;
        max-width: 300px;
    }
    .containerImg{
        max-width: 300px;
        max-height: 350px;
        margin: auto;
    }
    .containerBtnImg{
        width: inherit;
        padding: 15px 35px;
        max-width: 300px;
        margin: auto;
    }
    .buttomFloatLeft{
        float: left;
    }
    .buttomFloatRigth{
        float: right;
    }
    .buttonRotateStyle{
        background-color: #ffffff00;
        border-radius: 5px;
        width: 35px;
    }
    .buttonRotateStyle:hover{
        border-color: #2bb273;
        background-color: #2bb27359;
    }
    .spaceBottom-25{
        margin-bottom: 25px !important;
    }

    .containerTypeImg{
        width: inherit;
        text-align: center;
        margin: auto;
    }

    .containerStepInfo{
        display: inline-block;
        vertical-align: middle;
        max-width: 300px;
    }
    .stepCircle{
        background-color: #A1C4D8;
        width: 30px;
        height: 30px;
        display: table-cell;
        vertical-align: middle;
        text-align: center;
    }
    .stepInfo{
        display: inline-block;
        vertical-align: sub;
    }


    .displayBlock{
        display: block;
    }
    .minWidth-150{
        min-width: 150px;
    }
    .minWidth-210{
        min-width: 210px;
    }
    .padding-10{
        padding: 10px;
    }
    .marginAuto{
        margin: auto;
    }
    .marginBottom-50{
        margin-bottom: 50px;
    }

    /*#titlePage{
        font-family: 'NeoSansPro-Regular';
        color: #2BB273 !important;
        font-size: 39px;
    }*/

    .subTitles , .headerSectionTitle{
        font-family: 'NeoSansPro-Regular';
        color: white;
        font-size: 30px;
        /*text-align: right;*/
    }

    .img-thumbnail{
        max-width: 210px;
        max-height: 195px;
        width: 100%;
        height: 250px;
    }



    /*_________________ Dimensiones del mapa OpenStreetMap->Open layers*/
    .map{
        height: -webkit-fill-available;
        height: -moz-available;
        width: -webkit-fill-available;
        width: -moz-available;
    }
    /* #marker {
        width: 20px;
        height: 20px;
        border: 1px solid #088;
        border-radius: 10px;
        background-color: #f00;
        opacity: 0.7;
    } */
    #marker > i{
        font-size: 2.4rem;
    }
    .backCards{
        width: 100%;
        height: 150px;
        display: inline-block;
    }

    .widthImg{
        width: 185px;
        margin-left: 10px;
        height: 150px !important;
    }

    .card-title {
        font-size: 1em;
        font-weight: 400;
    }

    #saveChangesImages{
        display: none;
    }

    .btnDelete{
        width: 95%;
    }


    .inActive{
        background-color: #fd8e8e85;
    }

    .inputFileStyle{
        background-color: #17a2b8;
        padding: .8rem;
        border-radius: 50%;
        width: 40px;
        height: 40px;
        display: inline-table;
        align-content: center;
        cursor: pointer;
    }
    .inputFileStyle > i{
        color: white;
        font-size: x-large;
        margin: auto;
    }


    .inputFileStyle{
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.68);
        -webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
        transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    }
    .inputFileStyle:after{
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
        opacity: 0;
        -webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
        transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    }

    .inputFileStyle:hover {
        -webkit-transform: scale(1.25, 1.25);
        transform: scale(1.18, 1.18);
    }

    .inputFileStyle:hover::after {
        opacity: 1;
    }


    #nameFile{
        display: inline-grid;
        align-content: center;
        margin-bottom: .5rem;
        margin-left: 15px;
    }


    #toast {
        visibility: hidden;
        min-width: 100px;
        width: 52%;
        margin-left: -125px;
        background-color: #333;
        color: #fff;
        text-align: center;
        border-radius: 2px;
        padding: 16px;
        position: fixed;
        z-index: 1;
        /* left: 50%;  */
        bottom: 8%;
        background-image: linear-gradient(to left, #28A7DE 0%, #24A368 100%);
        margin: 0 21%;
    }


    #toast.show {
        visibility: visible;
        -webkit-animation: fadein 0.4s, fadeout 0.4s 2s;
        animation: fadein 0.4s, fadeout 0.4s 2s;
    }


    @-webkit-keyframes fadein {
        from {bottom: 0; opacity: 0;}
        to {bottom: 30px; opacity: 1;}
    }

    @keyframes fadein {
        from {bottom: 0; opacity: 0;}
        to {bottom: 30px; opacity: 1;}
    }

    @-webkit-keyframes fadeout {
        from {bottom: 30px; opacity: 1;}
        to {bottom: 0; opacity: 0;}
    }

    @keyframes fadeout {
        from {bottom: 30px; opacity: 1;}
        to {bottom: 0; opacity: 0;}
    }

    .sizeImage{
        height: 100px;
        margin: 0 4px;
    }

    .bloqueo {
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        overflow: hidden;
        outline: 0;
        position: fixed;
        background-color: rgba(154, 161, 164, 0.2);
        z-index: 10;
    }

    .loadingStandar{
        margin-left: 10px;
        font-size: larger;
    }

    .btn-green {
        color: #fff;
        background-color: #2bb273;
        border-color: #2bb273;
        box-shadow: 0 1px 1px rgba(0,0,0,.075);
    }

    .btn-green:hover{
        cursor: pointer;
    }

    .fieldSetBorder{
        border-bottom: 2px solid cornflowerblue;
        border-bottom-left-radius: 0.9rem;
        border-bottom-right-radius: 0.9rem;
    }

    .fieldsetLabel{
        background-color: cornflowerblue;
        color: white;
        padding: .25em .8rem;
        border-radius: .25rem .25rem 0 0;
    }

</style>

<style>
    .firstLabel{
        min-width: 115px;
    }

    .firstLabel > label{
        width: -webkit-fill-available;
        width: -moz-available;
    }

    .firstLabelSecondSection{
        min-width: -webkit-fill-available;
        width: -moz-available;
        padding: 0 .6px 0 0;
    }

    .firstLabelSecondSection > label{
        width: -webkit-fill-available;
        width: -moz-available;
        border-radius: .25rem .25rem 0 0 !important;
        white-space: inherit;
    }

    .secondLabelSecondSection{
        border-radius: 0 0 .25rem .25rem !important;
    }

    #imgCarencias > img{
        width: calc(20% + 10px);
        height: calc(20% + 20px);
    }

    @media only screen and (max-width: 815px){
        .contenedor{
            top: 10px;
        }
    }
    @media only screen and (max-width: 295px){
        .firstLabel{
            min-width: -webkit-fill-available;
            width: -moz-available;
            padding: 0 .6px 0 0;
        }

        .firstLabel > label{
            width: -webkit-fill-available;
            width: -moz-available;
            border-radius: .25rem .25rem 0 0 !important;
            white-space: inherit;
        }

        .secondLabel{
            border-radius: 0 0 .25rem .25rem !important;
        }
    }

    .carenciaGris{
        -webkit-filter: grayscale(1);
        filter: grayscale(1);
    }
</style>


<script>
    function imageDefault(elemento){
        elemento.src = './assets/img/Missing-Product-Card.jpg';
    }
</script>

<!-- <div class="bloqueo text-center">
    <div class="loaderStandar text-center">
        <h4 class="text-center bold">Cargando ...</h4>
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div> -->
<div class="container-fluid">
    <section class=''>
        <h4 class="col-md-12 headerSectionTitle">CUIS <span id="folioCuis"></span> Datos Generales</h4>
        <div class="row contentPadding-15" style="overflow-x: auto;">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6" id="stateMunLoc">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend firstLabel">
                                <label for="state" class='input-group-text'>
                                    Estado<i class="fa fa-spinner fa-spin loadingStandar" id="loadingEst" style="display: none;"></i>
                                </label>
                            </div>
                            <select class="form-control secondLabel" name="" id="cveEstado" onchange="getMunicipios(this.value)" disabled data-defaultValue="">
                                <option value="">Seleccione...</option>
                            </select>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend firstLabel">
                                <label for="municipality" class='input-group-text'>
                                    Municipio<i class="fa fa-spinner fa-spin loadingStandar" id="loadingMun" style="display: none;"></i>
                                </label>
                            </div>
                            <select class="form-control secondLabel" name="" id="cveMunicipio" onchange="getLocalidades(this.value)" disabled data-defaultValue="">
                                <option value="">Seleccione...</option>
                            </select>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend firstLabel">
                                <label for="locality" class='input-group-text'>
                                    Localidad<i class="fa fa-spinner fa-spin loadingStandar" id="loadingLoc" style="display: none;"></i>
                                </label>
                            </div>
                            <select class="form-control secondLabel" name="" id="cveLocalidad" disabled data-defaultValue="">
                                <option value="">Seleccione...</option>
                            </select>
                        </div>

                        <div id="actualzaInfoGeneral" class="row" style="margin: inherit;">
                            <button class="btn btn-default btn-block mb-3 col-md-12" onclick="activarSelect()">Editar</button>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend firstLabelSecondSection">
                                <label for="state" class='input-group-text'>Fecha de levantamiento</label>
                            </div>
                            <label for="" class='form-control secondLabelSecondSection' id='fechaLevanta'></label>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend firstLabelSecondSection">
                                <label class='input-group-text'>Carencias</label>
                            </div>
                            <div class="form-control secondLabelSecondSection">
                                <div class="row" id="imgCarencias"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend firstLabelSecondSection">
                                    <label for="" class='input-group-text'>
                                        <button style="padding: 0;font-size: large;" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsePrograms" aria-expanded="true" aria-controls="collapsePrograms">
                                            Programas
                                        </button>
                                    </label>
                                </div>

                                <div id="collapsePrograms" class="collapse show w-100" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="form-control secondLabelSecondSection">
                                        <div class="row" id="imgPrograms"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5>Ubicación Física de Expediente</h5>
                        <div class="row" style="overflow-x: auto;">
                            <div class="col-md-12" id="lotes"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class='frameMapSize' style="margin: auto;">
                    <a href="#" id="urlMap" target="_blank">
                        <div id="map" class="map"></div>
                    </a>
                    <!-- <a href="#" id="urlMap" target="_blank"><img src="" alt="" class="map" id="map"></a> -->
                </div>
                <label for="" class='labelSpace-15'>Coordenadas:
                    <label for="" id='coordCalle'></label>
                </label>

                <div class="row">
                    <div class='col-sm-6 col-md-12 mb-2 mt-3'>
                        <a href = '?view=cuis_completo&folio_cuis=<?php echo $_GET['folio']; ?>'><button class="btn btn-block btn-primary">CUIS COMPLETA</button></a>
                    </div>

                    <div id="contentBtnDeleteIntg" class="col-sm-6 col-md-12 mb-2 mt-3">
                        <!--?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                            echo '<button type="" class="btn btn-info btn-block" data-toggle="modal" data-target="#modalFamilia">Eliminar Familia</button>';
                        ?-->
                    </div>

                    <div class='col-sm-6 col-md-12 mb-2 mt-3'>
                        <button class="btn btn-block btn-primary" onclick="getDatosDomicilio();">Actualizar Domicilio</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class='sectionsMargTop-50'>
        <h4 class="col-md-12 headerSectionTitle">Datos del Jefe de Hogar</h4>
        <div class="row contentPadding-15">
            <div class="col-md-4 form-group" id="datosJefeHogar">
                <!-- <label for="name" class='displayBlock'>Nombre:</label>
                <input type="text" placeholder='Nombre' class='inputStyle' id='nomJefeHog'>
                <button class='btn'>Editar</button>
                <label for="firstLastName" class='displayBlock'>Apellido Paterno</label>
                <input type="text" placeholder='Apellido Paterno' class='inputStyle' id='apPatJefe'>
                <button class='btn'>Editar</button>
                <label for="secondLastName" class='displayBlock'>Apellido Materno</label>
                <input type="text" placeholder='Apellido Materno' class='inputStyle' id='apMatJefe'>
                <button class='btn'>Editar</button> -->
            </div>
            <div class="col-md-8">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="input-group mb-3">
                            <div class='container-fluid text-center'>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10 || $_SESSION['PERFIL'] == 2)
                                        echo '<select class="custom-select marginAuto spaceBottom-25 selectStyle" id="validacionFoto">
                                                <option value="0" >No Validada</option>
                                                <option value="1" >Válida</option>
                                                <option value="2" >No Válida</option>
                                            </select>';
                                ?>
                            </div>
                            <div class='containerImg container-fluid text-center'>
                                <img src="" onerror="imageDefault(this);" alt="Foto" class="img-fluid img-thumbnail" id='fotoImg'>
                            </div>
                            <div class='containerBtnImg container-fluid text-center'>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10 || $_SESSION['PERFIL'] == 2)
                                    echo '<button class="buttomFloatLeft buttonRotateStyle" onclick="rotar(\'fotoImg\',-90)">I</button>
                                        <button class="buttomFloatRigth buttonRotateStyle" onclick="rotar(\'fotoImg\',90)">D</button>';
                                ?>
                            </div>
                            <div class="containerTypeImg container-fluid text-center">
                                <div class="containerStepInfo">
                                    <label class="rounded-circle stepCircle">1</label>
                                </div>
                                <h4 class='stepInfo'>Foto</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="input-group mb-3">
                            <div class='container-fluid text-center'>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10 || $_SESSION['PERFIL'] == 2)
                                    echo '<select class="custom-select marginAuto spaceBottom-25 selectStyle" id="validacionHuella">
                                            <option value="0" >No Validada</option>
                                            <option value="1" >Válida</option>
                                            <option value="2" >No Válida</option>
                                        </select>';
                                ?>
                            </div>
                            <div class='containerImg container-fluid text-center'>
                                <img src="" onerror="imageDefault(this);" alt="Foto" class="img-fluid img-thumbnail" id='huellaImg'>
                            </div>
                            <div class='containerBtnImg container-fluid text-center'>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10 || $_SESSION['PERFIL'] == 2)
                                    echo '<button class="buttomFloatLeft buttonRotateStyle" onclick="rotar(\'huellaImg\',-90)">I</button>
                                    <button class="buttomFloatRigth buttonRotateStyle" onclick="rotar(\'huellaImg\',90)">D</button>';
                                ?>
                            </div>
                            <div class="containerTypeImg container-fluid text-center">
                                <div class="containerStepInfo">
                                    <label class="rounded-circle stepCircle">2</label>
                                </div>
                                <h4 class='stepInfo'>Huella</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="input-group mb-3">
                            <div class='container-fluid text-center'>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10 || $_SESSION['PERFIL'] == 2)
                                    echo '<select class="custom-select marginAuto spaceBottom-25 selectStyle" id="validacionFirma">
                                            <option value="0" >No Validada</option>
                                            <option value="1" >Válida</option>
                                            <option value="2" >No Válida</option>
                                        </select>';
                                ?>
                            </div>
                            <div class='containerImg container-fluid text-center'>
                                <img src="" onerror="imageDefault(this);" alt="Firma" class="img-fluid img-thumbnail" id='firmaImg'>
                            </div>
                            <div class='containerBtnImg container-fluid text-center'>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10 || $_SESSION['PERFIL'] == 2)
                                    echo '<button class="buttomFloatLeft buttonRotateStyle" onclick="rotar(\'firmaImg\',-90)">I</button>
                                        <button class="buttomFloatRigth buttonRotateStyle" onclick="rotar(\'firmaImg\',90)">D</button>';
                                ?>
                            </div>
                            <div class="containerTypeImg container-fluid text-center">
                                <div class="containerStepInfo">
                                    <label class="rounded-circle stepCircle">3</label>
                                </div>
                                <h4 class='stepInfo'>Firma</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <button class="btn btn-primary" id="saveChangesImages" onclick="sendImgRotate();">Guardar Cambios Imagenes</button>
                </div>
            </div>
        </div>
        <div class='col-md-12'>
            <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10 || $_SESSION['PERFIL'] == 2)
                echo '<div class="row">
                    <div class="col-md-4">
                        <label for="" class="form-text">Calificación</label>
                        <div class="input-group mb-3">
                            <select class="custom-select spaceBottom-25" id="validacionCalificacion">
                                <option value="0" >No Validada</option>
                                <option value="1" >Válida</option>
                                <option value="2" >No Válida</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="" class="form-text">Status</label>
                        <div class="input-group mb-3">
                            <select class="custom-select spaceBottom-25" id="validacionEstatus">
                                <option value="" selected>Seleccione ...</option>
                                <option value="5" >Correcto</option>
                                <option value="6" >Correcto Obscuro</option>
                                <option value="1" >Fotos Ilegibles</option>
                                <option value="2" >Sin Fotos</option>
                                <option value="3" >Foto Cruzada con el Nombre</option>
                                <option value="4" >Duplicado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="" class="form-text">Observación</label>
                        <textarea rows="3" id="validacionObservacion" placeholder="Observacion validación" class="form-control"></textarea>

                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-6">
                                <button type="" class="btn btn-info btn-block my-2" style="white-space: inherit;" onclick="guardarValidaciones(0);" >Guardar Validaciones</button>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-6">
                                <button type="" class="btn btn-info btn-block my-2" style="white-space: inherit;" onclick="guardarValidaciones(1);" >Guardar e Imprimir</button>
                            </div>
                        </div>
                    </div>
                </div>';
            ?>
        </div>

        <!-- <div class="col-md-12 mt-2">
            <div class="row justify-content-end">
                <div class="col-lg-2">
                    <button type="" class="btn btn-info btn-block" onclick="guardarValidaciones(0);" >Guardar Validaciones</button>
                </div>
                <div class="col-lg-2">
                    <button type="" class="btn btn-info btn-block" onclick="guardarValidaciones(1);" >Guardar e Imprimir</button>
                </div>
            </div>
        </div>         -->
    </section>

    <section class='marginBottom-50'>
        <h4 class="col-md-12 headerSectionTitle">Datos de los Integrantes</h4>
        <div class="row contentPadding-15" id='dataIntegrantes'></div>
    </section>

    <!--?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
        // echo '<section>
        //         <div class="row">
        //             <div class="col-md-6 col-sm-12" style="align-self: center;">
        //                 <div class="input-group mb-3">
        //                     <div class="input-group-prepend">
        //                         <label for="" class="input-group-text min-width">Folio Cédula de Cambio</label>
        //                     </div>
        //                     <input type="text" class="form-control" placeholder="Folio Cédula de Cambio" id="folioCedulaCambio">
        //                 </div>
        //             </div>

        //             <div class="col-md-6 col-sm-4 text-center" style="align-self: center;">
        //                 <div class="input-group-prepend">
        //                     <label class="inputFileStyle" for="docSoporte">
        //                         <i class="fa fa-cloud-upload" aria-hidden="true"></i>
        //                     </label>
        //                     <span id="nameFile">Cargar Documento Soporte...</span>
        //                     <input class="form-control" type="file" name="docSoporte" id="docSoporte" style="display: none;">
        //                 </div>
        //                 <!-- <button class="btn btn-primary" onclick="cargarDocSoporte();">Guardar Documento Soporte</button> ->
        //             </div>

        //             <div class="col-md-12 col-sm-8 mt-2">
        //                 <div class="row justify-content-end">
        //                     <div class="col-lg-4">
        //                         <button type="" class="btn btn-info btn-block" onclick="resetearInput();" >Eliminar Imagen</button>
        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //     </section>';
     ?-->

    <!-- <section class="">
        <div class="row ">
            <div class="col-md-4 mt-3" id="btnImprimirCuis">
            </div>
        </div>
    </section> -->

    <section>
        <!-- Modal -->
        <div class="modal fade" id="modalIntegrante" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" >Actualizar Integrante</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <label style="font-weight: normal;" for="" id="nameIntegrated"></label> ?
              </div>
              <input type="hidden" id="idIntegratedCuis">
              <input type="hidden" id="newStateIntg">
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">CANCELAR</button>
                <button type="button" class="btn btn-danger" onclick="updateIntegrated();">CONTINUAR</button>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Familia -->
        <div class="modal fade" id="modalFamilia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" >Actualizar Familia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <label style="font-weight: normal;" for="" id="optionTextFamily"></label>
              </div>
              <input type="hidden" id="folioCuisFamilia">
              <input type="hidden" id="newStateFamilia">
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">CANCELAR</button>
                <button type="button" class="btn btn-danger" onclick="deleteFamily();">CONTINUAR</button>
              </div>
            </div>
          </div>
        </div>
    </section>

    <div id="toast"></div>

    <div class="contenedor">
        <button class="botonF1" style="display: none;">
            <span>+</span>
        </button>

        <button class="btnFloat botonF2" type="button" data-toggle="modal" data-placement="left" title="Adjuntar Documento Soporte" data-target="#modalDocSoporte">
            <i class="fa fa-cloud-upload" aria-hidden="true"></i>
        </button>

        <div class="btnFloat botonF3" id="btnImprimirCuis"></div>


    </div>

</div>

    <div class="modal fade bd-example-modal-lg" id="modalDomicilio" tabindex="-1" role="dialog" aria-labelledby="Información Domicilio" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Actualizar Domicilio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="contentDomicilio">

                    <fieldset class="col-lg-12 mb-3">
                        <div class="row fieldSetBorder">
                            <label class="input-group fieldsetLabel">VIALIDAD</label>
                            <div class="col-lg-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label for="" class="input-group-text ">Tipo</label>
                                    </div>
                                    <select name="" id="tipo_vial" class="form-control secondLabel " data-nameField="C_TIPO_VIAL" disabled>
                                        <option value="1">AMPLIACIÓN</option>
                                        <option value="2">ANDADOR</option>
                                        <option value="3">AVENIDA</option>
                                        <option value="4">BOULEVARD</option>
                                        <option value="5">CALLE</option>
                                        <option value="6">CALLEJÓN</option>
                                        <option value="7">CALZADA</option>
                                        <option value="8">CERRADA</option>
                                        <option value="9">CIRCUITO</option>
                                        <option value="10">CIRCUNVALACIÓN</option>
                                        <option value="11">CONTINUACIÓN</option>
                                        <option value="12">CORREDOR</option>
                                        <option value="13">DIAGONAL</option>
                                        <option value="14">EJE VIAL</option>
                                        <option value="15">PASAJE</option>
                                        <option value="16">PEATONAL</option>
                                        <option value="17">PERIFÉRICO</option>
                                        <option value="18">PRIVADA</option>
                                        <option value="19">PROLONGACIÓN</option>
                                        <option value="20">RETORNO</option>
                                        <option value="21">VIADUCTO</option>
                                        <option value="22">NINGUNO</option>
                                        <option value="23">CARRETERA</option>
                                        <option value="24">BRECHA</option>
                                        <option value="25">CAMINO</option>
                                        <option value="26">TERRACERÍA</option>
                                        <option value="27">VEREDA</option>
                                    </select>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'tipo_vial\');">Editar</button>';
                                ?>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label for="" class="input-group-text ">Nombre</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Nombre" id="nombre_vial" data-nameField="NOMBRE_VIAL" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'nombre_vial\');">Editar</button>';
                                ?>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label for="" class="input-group-text ">C.P</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Codigo Postal" id="cp_vial" data-nameField="CP" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'cp_vial\');">Editar</button>';
                                ?>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="col-lg-12 mb-3">
                        <div class="row fieldSetBorder">
                            <label class="input-group fieldsetLabel">ASENTAMIENTO</label>
                            <div class="col-lg-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label for="" class="input-group-text ">Tipo</label>
                                    </div>
                                    <select name="" id="tipo_Acentamiento" class="form-control secondLabel " data-nameField="C_TIPO_ASENTAMIENTO" disabled>
                                        <option value="1 ">AEROPUERTO</option>
                                        <option value="2 ">AMPLIACIÓN</option>
                                        <option value="3 ">BARRIO</option>
                                        <option value="4 ">CANTÓN</option>
                                        <option value="5 ">CIUDAD</option>
                                        <option value="6 ">CIUDAD INDUSTRIAL</option>
                                        <option value="7 ">COLONIA</option>
                                        <option value="8 ">CONDOMINIO</option>
                                        <option value="9 ">CONJUNTO HABITACIONAL</option>
                                        <option value="10">CORREDOR INDUSTRIAL</option>
                                        <option value="11">COTO</option>
                                        <option value="12">CUARTEL</option>
                                        <option value="13">EJIDO</option>
                                        <option value="14">EXHACIENDA</option>
                                        <option value="15">FRACCIÓN</option>
                                        <option value="16">FRACCIONAMIENTO</option>
                                        <option value="17">GRANJA</option>
                                        <option value="18">HACIENDA</option>
                                        <option value="19">INGENIO</option>
                                        <option value="20">MANZANA</option>
                                        <option value="21">PARAJE</option>
                                        <option value="22">PARQUE INDUSTRIAL</option>
                                        <option value="23">PRIVADA</option>
                                        <option value="24">PROLONGACIÓN</option>
                                        <option value="25">PUEBLO</option>
                                        <option value="26">PUERTO</option>
                                        <option value="27">RANCHERÍA</option>
                                        <option value="28">RANCHO</option>
                                        <option value="29">REGIÓN</option>
                                        <option value="30">RESIDENCIAL</option>
                                        <option value="31">RINCONADA</option>
                                        <option value="32">SECCIÓN</option>
                                        <option value="33">SECTOR</option>
                                        <option value="34">SUPERMANZANA</option>
                                        <option value="35">UNIDAD</option>
                                        <option value="36">UNIDAD HABITACIONAL</option>
                                        <option value="37">VILLA</option>
                                        <option value="38">ZONA FEDERAL</option>
                                        <option value="39">ZONA INDUSTRIAL</option>
                                        <option value="40">ZONA MILITAR</option>
                                        <option value="41">NINGUNO</option>
                                        <option value="43">ZONA NAVAL</option>
                                    </select>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'tipo_Acentamiento\');">Editar</button>';
                                ?>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label for="" class="input-group-text ">Clave</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Clave" id="clave_tipo_acent" data-nameField="C_ASENTAMIENTO" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'clave_tipo_acent\');">Editar</button>';
                                ?>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label for="" class="input-group-text ">Nombre</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Nombre" id="nombre_tipo_acent" data-nameField="NOMBRE_ASEN" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'nombre_tipo_acent\');">Editar</button>';
                                ?>
                            </div>
                        </div>
                    </fieldset>

                    <hr class="my-3 w-100">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend firstLabel">
                                    <label class="input-group-text ">Nombre Entre Vialidad 1</label>
                                </div>
                                <input type="text" class="form-control secondLabel " placeholder="Nombre Entre Vialidad 1" id="nombre_entrevial_1" data-nameField="NOMBRE_ENTRE_VIAL_1" disabled>
                            </div>
                            <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'nombre_entrevial_1\');">Editar</button>';
                            ?>
                        </div>

                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend firstLabel">
                                    <label class="input-group-text ">Tipo Entre Vialidad 1</label>
                                </div>
                                <select name="" id="tipo_entevial_1" class="form-control secondLabel " data-nameField="C_TIPO_ENTRE_VIAL_1" disabled>
                                    <option value="1">AMPLIACIÓN</option>
                                    <option value="2">ANDADOR</option>
                                    <option value="3">AVENIDA</option>
                                    <option value="4">BOULEVARD</option>
                                    <option value="5">CALLE</option>
                                    <option value="6">CALLEJÓN</option>
                                    <option value="7">CALZADA</option>
                                    <option value="8">CERRADA</option>
                                    <option value="9">CIRCUITO</option>
                                    <option value="10">CIRCUNVALACIÓN</option>
                                    <option value="11">CONTINUACIÓN</option>
                                    <option value="12">CORREDOR</option>
                                    <option value="13">DIAGONAL</option>
                                    <option value="14">EJE VIAL</option>
                                    <option value="15">PASAJE</option>
                                    <option value="16">PEATONAL</option>
                                    <option value="17">PERIFÉRICO</option>
                                    <option value="18">PRIVADA</option>
                                    <option value="19">PROLONGACIÓN</option>
                                    <option value="20">RETORNO</option>
                                    <option value="21">VIADUCTO</option>
                                    <option value="22">NINGUNO</option>
                                    <option value="23">CARRETERA</option>
                                    <option value="24">BRECHA</option>
                                    <option value="25">CAMINO</option>
                                    <option value="26">TERRACERÍA</option>
                                    <option value="27">VEREDA</option>
                                </select>
                            </div>
                            <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'tipo_entevial_1\');">Editar</button>';
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend firstLabel">
                                    <label class="input-group-text ">Nombre Entre Vialidad 2</label>
                                </div>
                                <input type="text" class="form-control secondLabel " placeholder="Nombre Entre Vialidad 2" id="nombre_entrevial_2" data-nameField="NOMBRE_ENTRE_VIAL_2" disabled>
                            </div>
                            <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'nombre_entrevial_2\');">Editar</button>';
                            ?>
                        </div>

                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend firstLabel">
                                    <label for="" class="input-group-text ">Tipo Entre Vialidad 2</label>
                                </div>
                                <select name="" id="tipo_entrevial_2" class="form-control secondLabel " data-nameField="C_TIPO_ENTRE_VIAL_2" disabled>
                                    <option value="1">AMPLIACIÓN</option>
                                    <option value="2">ANDADOR</option>
                                    <option value="3">AVENIDA</option>
                                    <option value="4">BOULEVARD</option>
                                    <option value="5">CALLE</option>
                                    <option value="6">CALLEJÓN</option>
                                    <option value="7">CALZADA</option>
                                    <option value="8">CERRADA</option>
                                    <option value="9">CIRCUITO</option>
                                    <option value="10">CIRCUNVALACIÓN</option>
                                    <option value="11">CONTINUACIÓN</option>
                                    <option value="12">CORREDOR</option>
                                    <option value="13">DIAGONAL</option>
                                    <option value="14">EJE VIAL</option>
                                    <option value="15">PASAJE</option>
                                    <option value="16">PEATONAL</option>
                                    <option value="17">PERIFÉRICO</option>
                                    <option value="18">PRIVADA</option>
                                    <option value="19">PROLONGACIÓN</option>
                                    <option value="20">RETORNO</option>
                                    <option value="21">VIADUCTO</option>
                                    <option value="22">NINGUNO</option>
                                    <option value="23">CARRETERA</option>
                                    <option value="24">BRECHA</option>
                                    <option value="25">CAMINO</option>
                                    <option value="26">TERRACERÍA</option>
                                    <option value="27">VEREDA</option>
                                </select>
                            </div>
                            <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'tipo_entrevial_2\');">Editar</button>';
                            ?>
                        </div>
                    </div>

                    <hr class="my-3 w-100">

                    <fieldset class="col-lg-12 mb-3">
                        <div class="row fieldSetBorder">
                            <h5 class="input-group fieldsetLabel">VIALIDAD POSTERIOR</h5>
                            <div class="col-lg-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label class="input-group-text ">Nombre</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Nombre" id="nombre_vial_pos" data-nameField="NOMBRE_VIAL_POS" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'nombre_vial_pos\');">Editar</button>';
                                ?>
                            </div>

                            <div class="col-lg-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label class="input-group-text ">Tipo</label>
                                    </div>
                                    <select name="" id="tipo_vial_pos" class="form-control secondLabel " data-nameField="C_TIPO_VIAL_POS" disabled>
                                        <option value="1">AMPLIACIÓN</option>
                                        <option value="2">ANDADOR</option>
                                        <option value="3">AVENIDA</option>
                                        <option value="4">BOULEVARD</option>
                                        <option value="5">CALLE</option>
                                        <option value="6">CALLEJÓN</option>
                                        <option value="7">CALZADA</option>
                                        <option value="8">CERRADA</option>
                                        <option value="9">CIRCUITO</option>
                                        <option value="10">CIRCUNVALACIÓN</option>
                                        <option value="11">CONTINUACIÓN</option>
                                        <option value="12">CORREDOR</option>
                                        <option value="13">DIAGONAL</option>
                                        <option value="14">EJE VIAL</option>
                                        <option value="15">PASAJE</option>
                                        <option value="16">PEATONAL</option>
                                        <option value="17">PERIFÉRICO</option>
                                        <option value="18">PRIVADA</option>
                                        <option value="19">PROLONGACIÓN</option>
                                        <option value="20">RETORNO</option>
                                        <option value="21">VIADUCTO</option>
                                        <option value="22">NINGUNO</option>
                                        <option value="23">CARRETERA</option>
                                        <option value="24">BRECHA</option>
                                        <option value="25">CAMINO</option>
                                        <option value="26">TERRACERÍA</option>
                                        <option value="27">VEREDA</option>
                                    </select>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-block mb-3" onclick="actualizarDomicilio(this, \'tipo_vial_pos\');">Editar</button>';
                                ?>
                            </div>
                        </div>
                    </fieldset>

                    <hr class="my-3 w-100">

                    <div class="col-lg-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend firstLabel">
                                <label class="input-group-text ">Descripción Ubicación</label>
                            </div>
                            <input type="text" class="form-control secondLabel " placeholder="Descripción Ubicación" id="descripcion_ubicacion" data-nameField="DESC_UBIC" disabled>
                        </div>
                        <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                            echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'descripcion_ubicacion\');">Editar</button>';
                        ?>
                    </div>

                    <hr class="my-3 w-100">

                    <fieldset class="col-lg-12 mb-3">
                        <div class="row fieldSetBorder">
                            <h5 class="input-group fieldsetLabel">Exterior</h5>
                            <div class="col-lg-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label class="input-group-text ">Número</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Número" id="num_ext" data-nameField="NUM_EXT" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'num_ext\');">Editar</button>';
                                ?>
                            </div>

                            <div class="col-lg-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label class="input-group-text ">Letra</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Letra" id="letra_ext" data-nameField="LETRA_EXT" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'letra_ext\');">Editar</button>';
                                ?>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="col-lg-12 mb-3">
                        <div class="row fieldSetBorder">
                            <h5 class="input-group fieldsetLabel">INTERIOR</h5>
                            <div class="col-lg-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label class="input-group-text ">Número</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Número" id="num_int" data-nameField="NUM_INT" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'num_int\');">Editar</button>';
                                ?>
                            </div>

                            <div class="col-lg-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend firstLabel">
                                        <label class="input-group-text ">Letra</label>
                                    </div>
                                    <input type="text" class="form-control secondLabel " placeholder="Letra" id="let_int" data-nameField="LETRA_INT" disabled>
                                </div>
                                <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
                                    echo '<button class="btn btn-default btn-block mb-3" onclick="actualizarDomicilio(this, \'let_int\');">Editar</button>';
                                ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <?php if($_SESSION['PERFIL'] == 8 || $_SESSION['PERFIL'] == 10)
        echo '<div class="modal fade" id="modalDocSoporte" tabindex="-1" role="dialog" aria-labelledby="Documento Soporte" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">CARGAR DOCUMENTO SOPORTE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend firstLabel">
                                    <label for="" class="input-group-text ">Folio Cédula de Cambio</label>
                                </div>
                                <input type="text" class="form-control secondLabel " placeholder="Folio Cédula de Cambio" id="folioCedulaCambio">
                            </div>
                            <div class="row">
                                <div class="container-fluid text-center" style="align-self: center;">
                                    <div class="input-group-prepend">
                                        <label class="inputFileStyle" for="docSoporte">
                                            <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                        </label>
                                        <span id="nameFile">Cargar Documento Soporte...</span>
                                        <input class="form-control" type="file" name="docSoporte" id="docSoporte" style="display: none;">
                                    </div>
                                    <!-- <button class="btn btn-primary" onclick="cargarDocSoporte();">Guardar Documento Soporte</button> -->
                                </div>
                                <div class="col-md-12 col-sm-8 mt-2">
                                    <div class="row justify-content-end">
                                        <div class="col-lg-4">
                                            <button type="" class="btn btn-info btn-block" onclick="resetearInput();" >Eliminar Imagen</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
    ?>

<link rel="stylesheet" href="./vendor/thisPlugins/openStreetMap/ol.css" type="text/css">
<script src="./vendor/thisPlugins/openStreetMap/ol.js"></script>
<!-- <script src="http://www.openlayers.org/api/OpenLayers.js"></script> -->

<script>
    var URL = './controller/cuis_controller.php';
    // var routeImage = 'http://75.126.73.173:8025/imagenes/';
    var routeImage = 'Storage/Evidencias/imagenes_vcc/';
    var idConsulta = '<?php echo $_GET['folio'] ?>';
    var usuarioPerfil = '<?php echo $_SESSION['PERFIL'] ?>';
    var estatusFamilia = '';
    var programas = '';
    var cVivienda = '';

    // Variables Manejo de Rotacion ---------------------
    var primerslap=false;
    var segundoslap=false;
    // --------------------------------------------------

    var objectEstados = new Object();
    // $('.collapse').collapse();

    $(function(){
        // $('.bloqueo').show();
        estadoValidacionImagenes(idConsulta);
        datosGenerales(false);
        dataCUIS(idConsulta);
        getProgramas(idConsulta);
        $('[data-toggle=modal]').tooltip({ trigger: "hover" });
        $('[data-toggle="modal"]').tooltip();
        $('.btnFloat').addClass('animacionVer');
    });

    $("#docSoporte[type=file]").on("change", function(){
        var file = this.files[0].name;
        $('#nameFile').text(file);
    });

    function estadoValidacionImagenes(consultaCuis){
        let METODO = 'POST';
        let dataObject = { opcion: 'estadoValidacionImg', idCuis:  consultaCuis};
        let request = petitionSupport(URL, METODO, dataObject);

        request.done(function(res){
            if(res){
                $('#validacionFoto').val(res.V_FOTO);
                $('#validacionHuella').val(res.V_HUELLA);
                $('#validacionFirma').val(res.V_FIRMA);
                $('#validacionCalificacion').val(res.VALIDACION);
                if(res.STATUS != null && res.STATUS != '' && res.STATUS != 0)$('#validacionEstatus').val(res.STATUS);
                $('#validacionObservacion').val(res.OBSERVACION);

                if(res.V_FOTO == 1 && (res.V_HUELLA == 1 || res.V_FIRMA == 1)){
                    $('#btnImprimirCuis').show();
                    $('#btnImprimirCuis').html('<button class="btnCircleEstyle" type="button" data-toggle="tooltip" data-placement="left" title="IMPRIMIR" onclick="agregarLoteImpresion(1);">\
                                                    <i class="fa fa-print" aria-hidden="true"></i>\
                                                </button>');
                }else{
                    $('#btnImprimirCuis').hide();
                    $('#btnImprimirCuis').html('');
                }

                $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    function agregarLoteImpresion(tipoImpresion = 0) {
        let METODO = 'POST';
        let objectSend = {
            opcion : 'agregarLoteImpresion',
            tipoImpresion : tipoImpresion,
            cveFolioCuis : idConsulta
        };

        let request = petitionSupport(URL, METODO, objectSend);
        request.done(function(res){
            if(res)
                alert('Impresión Agregada Correctamente');
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
        });
    }

    function guardarValidaciones(tipoImpresion){
        let valFoto = $('#validacionFoto').val();
        let valHuella = $('#validacionHuella').val();
        let valFirma = $('#validacionFirma').val();
        let valCalificacion = $('#validacionCalificacion').val();
        let valValidStatus = $('#validacionEstatus').val();
        let valValidObs = $('#validacionObservacion').val();

        if( (tipoImpresion == 1 && valFoto == 1 && (valHuella == 1 || valFirma == 1) || tipoImpresion == 0) ){

            let METODO = 'POST';
            let dataObject = {
                opcion: 'cambioValidacionesImg',
                idCuis:  idConsulta,
                valFoto : valFoto,
                valHuella : valHuella,
                valFirma : valFirma,
                valCalificacion : valCalificacion,
                valValidStatus : valValidStatus,
                valValidObs : valValidObs,
                tipoImpresion : tipoImpresion
            };
            let request = petitionSupport(URL, METODO, dataObject);

            request.done(function(res){
                if(tipoImpresion == 1){
                    if(res.validaciones && res.impresion){
                        alert('Información Actualizada');
                        estadoValidacionImagenes(idConsulta);
                    }
                    else if(res.validaciones && res.impresion == false){
                        alert('Información Actualizada, Error al guardar la impresión');
                    }
                    else{
                        alert('Error al Realizar la Actualización y al guardar la impresión');
                    }
                }else{
                    if(res.validaciones){
                        alert('Información Actualizada');
                        estadoValidacionImagenes(idConsulta);
                    }else{
                        alert('Error al Realizar la Actualización y al guardar la impresión');
                    }
                }
            });
        }else{
            alert('Para imprimir se requiere que la foto y la firma o la huella sean validas');
        }
    }

    function getMunicipios(idEstado, cveMunicipio = ''){
        let METODO = 'POST';
        let dataObject = { opcion: 'municipios', idEstado : idEstado };
        let request = petitionSupportAsinc(URL, METODO, dataObject);

        $('#loadingMun').show();
        $('#cveMunicipio').html('<option value="">Seleccione...</option>');
        $('#cveLocalidad').html('<option value="">Seleccione...</option>');
        request.done(function(res){
            if(cveMunicipio != ''){
                $('#cveMunicipio').html(contructComboOps(res, 'CVE_MUNICIPIO', 'NOM_MUNICIPIO', cveMunicipio));
            }else{
                $('#cveMunicipio').html(contructComboOps(res, 'CVE_MUNICIPIO', 'NOM_MUNICIPIO'));
            }

            $('#loadingMun').hide();
        });
        request.fail(function(XHR, textStatus){
            console.log(textStatus);
            $('#loadingMun').hide();
        });
    }

    function getLocalidades(idMunicipio, cveLocalidad = ''){
        let METODO = 'POST';
        let dataObject = { opcion: 'localidades', idMunicipio : idMunicipio };
        let request = petitionSupportAsinc(URL, METODO, dataObject);
        $('#loadingLoc').show();
        $('#cveLocalidad').html('<option value="">Seleccione...</option>');
        request.done(function(res){
            if(cveMunicipio != ''){
                $('#cveLocalidad').html(contructComboOps(res, 'CVE_LOCALIDAD', 'NOM_LOCALIDAD', cveLocalidad));
            }else{
                $('#cveLocalidad').html(contructComboOps(res, 'CVE_LOCALIDAD', 'NOM_LOCALIDAD'));
            }
            $('#loadingLoc').hide();
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
            $('#loadingLoc').hide();
        });
    }

    function activarSelect(){
        $('#cveEstado').attr('disabled', false);
        $('#cveMunicipio').attr('disabled', false);
        $('#cveLocalidad').attr('disabled', false);
        $('#actualzaInfoGeneral').html('<button class="btn btn-block mt-2 col-md-6 btn-primary" onclick="actualizarInfoGeneral()">Actualizar</button>\
                                        <button class="btn btn-block mt-2 col-md-6 btn-green" onclick="calcelarActInfoGen()">Cancelar</button>');
    }

    function calcelarActInfoGen(){
        $('#cveEstado').attr('disabled', true);
        $('#cveMunicipio').attr('disabled', true);
        $('#cveLocalidad').attr('disabled', true);
        $('#actualzaInfoGeneral').html('<button class="btn btn-default btn-block mt-2 col-md-12" onclick="activarSelect()">Editar</button>');

        $('#cveEstado').val($('#cveEstado').attr('data-defaultValue'));
        if($('#cveMunicipio').attr('data-defaultValue') != $('#cveMunicipio').val())
            getMunicipios($('#cveEstado').val(), $('#cveMunicipio').attr('data-defaultValue'))
        if($('#cveLocalidad').attr('data-defaultValue') != $('#cveLocalidad').val())
            getLocalidades($('#cveMunicipio').val(), $('#cveLocalidad').attr('data-defaultValue'))
    }

    function actualizarInfoGeneral(){
        var inputFileImage = document.getElementById("docSoporte");
        var file = inputFileImage.files[0];
        var dataFormSend = new FormData();
        let countUpdates = 0;
        let METODO = 'POST';
        let folioCedula = $('#folioCedulaCambio').val();
        dataFormSend.append('opcion', 'updateLocation');
        dataFormSend.append('cveCuis', idConsulta);
        dataFormSend.append('documentoSoporte',file);
        dataFormSend.append('folioCedula',folioCedula);

        if($('#cveEstado').attr('data-defaultValue') != $('#cveEstado').val()){
            dataFormSend.append('cveEstado', $('#cveEstado').val());
            countUpdates++;
        }
        if($('#cveMunicipio').attr('data-defaultValue') != $('#cveMunicipio').val()){
            dataFormSend.append('cveMunicipio', $('#cveMunicipio').val());
            countUpdates++;
        }
        if($('#cveLocalidad').attr('data-defaultValue') != $('#cveLocalidad').val()){
            dataFormSend.append('cveLocalidad', $('#cveLocalidad').val());
            countUpdates++;
        }

        let request = petitionSupportFile(URL, METODO, dataFormSend);

        request.done(function(res){
            if(res['correctos'] == countUpdates)
                alert('Actualización Correcta');
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
        });
    }

    function datosGenerales(catalogos = false){
        let METODO = 'POST';
        let dataObject = { opcion: 'generales', idCuis:  idConsulta, getCatalogos: catalogos};
        let request = petitionSupport(URL, METODO, dataObject);
        $('#loadingEst').show();
        request.done(function(res){
            if(Object.keys(res).length !== 0){

                $('#folioCuis').html(res.datosGeneralesCuis.FOLIO_CUIS);

                // CONSTRUIR COMBOS DE ESTADO/MUNICIPIO/LOCALIDAD
                objectEstados = res.cuisEstados;
                $('#cveEstado').html(contructComboOps(res.cuisEstados, 'CVEENT', 'NOMBRE', res.datosGeneralesCuis.CVE_ENTIDAD_FEDERATIVA));
                $('#cveEstado').attr('data-defaultValue', res.datosGeneralesCuis.CVE_ENTIDAD_FEDERATIVA);
                $('#cveMunicipio').attr('data-defaultValue', res.datosGeneralesCuis.CVE_MUNICIPIO);
                $('#cveLocalidad').attr('data-defaultValue', res.datosGeneralesCuis.CVE_LOCALIDAD);
                getMunicipios(res.datosGeneralesCuis.CVE_ENTIDAD_FEDERATIVA, res.datosGeneralesCuis.CVE_MUNICIPIO);
                getLocalidades(res.datosGeneralesCuis.CVE_MUNICIPIO, res.datosGeneralesCuis.CVE_LOCALIDAD);

                $('#fechaLevanta').html(res.datosGeneralesCuis.FCH_LEVANTA);

                createMapOpenStreet(res.datosGeneralesCuis.LATITUD, res.datosGeneralesCuis.LONGITUD, res.datosGeneralesCuis.NOMBRE_ASEN);
                $('#latitud').html(res.datosGeneralesCuis.LATITUD);
                $('#longitud').html(res.datosGeneralesCuis.LONGITUD);

                // $('#fotoImg').attr('src', routeImage+res.datosGeneralesCuis.JEFE);
                // $('#huellaImg').attr('src', routeImage+res.datosGeneralesCuis.HUELLA);
                // $('#firmaImg').attr('src', routeImage+res.datosGeneralesCuis.FIRMA);
                $('#fotoImg').attr('src', routeImage+'foto.jpg');
                $('#huellaImg').attr('src', routeImage+'huella.jpg');
                $('#firmaImg').attr('src', routeImage+'firma.png');

                estatusFamilia = res.datosGeneralesCuis.ENVIADO;
                if(res.datosGeneralesCuis.ENVIADO == 0){
                    $('#contentBtnDeleteIntg').html('<button type="" class="btn btn-info btn-block" onclick="modalFamilia(\''+res.datosGeneralesCuis.FOLIO_CUIS+'\', 2)">Eliminar Familia</button>');
                }
                else if(res.datosGeneralesCuis.ENVIADO == 2){
                    $('#contentBtnDeleteIntg').html('<button type="" class="btn btn-danger btn-block" onclick="modalFamilia(\''+res.datosGeneralesCuis.FOLIO_CUIS+'\', 0)">Habilitar Familia</button>');
                }

                // Codigo de ALAM para los lotes -----------------------------------
                var lotes = res.lotes;
                if(lotes != null){
                    var cadena_lotes = "<table class='table'>\
                                            <thead>\
                                                <tr>\
                                                    <td>LOTE</td>\
                                                    <td>PASILLO</td>\
                                                    <td>FILA</td>\
                                                    <td>COLUMNA</td>\
                                                    <td>NIVEL</td>\
                                                </tr>\
                                            </thead>\
                                            <tbody>";
                    for (var i = 0; i < lotes.length; i++) {
                                cadena_lotes += "<tr>\
                                                    <td>"+lotes[i]['LOTE']+"</td>\
                                                    <td>"+lotes[i]['PASILLO']+"</td>\
                                                    <td>"+lotes[i]['FILA']+"</td>\
                                                    <td>"+lotes[i]['COLUMNA']+"</td>\
                                                    <td>"+lotes[i]['NIVEL']+"</td>\
                                                </tr>\
                                            </tbody>\
                                        </table>";
                    }
                    $("#lotes").html(cadena_lotes);
                }
                // Codigo de ALAM para los lotes -----------------------------------


            }
            $('#loadingEst').hide();
        });

        request.fail(function(jqXHR, textStatus){
            console.log(textStatus);
            $('#loadingEst').hide();
        });
    }

    function modalFamilia(folioCuis, nuevoEstado){
        let optionText = nuevoEstado == 2 ? "¿ Deseas eliminar a la familia completa ?" : "¿ Deseas habilitar a la familia completa ?";
        $('#optionTextFamily').html(optionText);
        $('#folioCuisFamilia').val(folioCuis);
        $('#newStateFamilia').val(nuevoEstado);
        $('#modalFamilia').modal('show');
    }

    function templateImagesPrograms(imagen, nombre, cveListado, cvePrograma, tipoEvento){
        let template = '<div class="widthImg">\
                            <a href="http://173.192.101.210:9043/desarrollo_des/public/portal/?view=cedula&indexs='+cveListado+","+cvePrograma+'" target="_blank"><div class="card hover backCards">\
                                <li class="list-group-item">\
                                    <img class="card-img-top img-fluid" src="./Storage/Evidencias/imgPrograms/PNG-CUADRADO/'+imagen+'" alt="Card image cap">\
                                </li>\
                                <div class="card-body" style="padding-top: .2em;">\
                                    <h6 class="card-title">'+nombre+'</h6>\
                                    <sub>'+tipoEvento+'</sub>\
                                </div>\
                            </div></a>\
                        </div>';
        return template;
    }

    function IsNumeric(valor){
        var log=valor.length; var sw="S";
        for (x=0; x<log; x++)
        {
            v1=valor.substr(x,1);
            v2 = parseInt(v1);
            //Compruebo si es un valor numérico
            if (isNaN(v2)) { sw= "N";}
        }
        if (sw=="S") {return true;} else {return false; }
    }

    function formatDate(fecha){
        var long = fecha.length;
        var dia;
        var mes;
        var ano;

        if ((long>=2) && (primerslap==false)) {
            dia=fecha.substr(0,2);
            if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) {
                fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true;
            }
            else {
                fecha=""; primerslap=false;
            }
        }
        else{
            dia=fecha.substr(0,1);
            if (IsNumeric(dia)==false){
                fecha="";
            }
            if ((long<=2) && (primerslap=true)) {
                fecha=fecha.substr(0,1); primerslap=false;
            }
        }

        if ((long>=5) && (segundoslap==false)){
            mes=fecha.substr(3,2);
            if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) {
                fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true;
            }
            else {
                fecha=fecha.substr(0,3);; segundoslap=false;
            }
        }
        else {
            if ((long<=5) && (segundoslap=true)) {
                fecha=fecha.substr(0,4); segundoslap=false;
            }
        }
        if (long>=7){
            ano=fecha.substr(6,4);
            if (IsNumeric(ano)==false) {
                fecha=fecha.substr(0,6);
            }
            else {
                if (long==10){
                    if ((ano==0) || (ano<1900) || (ano>2100)) {
                        fecha=fecha.substr(0,6);
                    }
                }
            }
        }

        if (long>=10)
        {
            fecha=fecha.substr(0,10);
            dia=fecha.substr(0,2);
            mes=fecha.substr(3,2);
            ano=fecha.substr(6,4);
            // Año no viciesto y es febrero y el dia es mayor a 28
            if ( (ano%4 != 0) && (mes ==02) && (dia > 28) ) {
                fecha=fecha.substr(0,2)+"/";
            }
        }
        return (fecha);
    }

    function getProgramas(idConsulta){
        let METODO = 'POST';
        let dataObject = { opcion: 'programasCuis', idCuis:  idConsulta};
        let request = petitionSupport(URL, METODO, dataObject);

        request.done(function(res){
            // let clavePersonaJefe = res.infoIntegranteJefe[0].C_PERSONA;

            let carencia1 = 'carenciaGris';
            let carencia2 = 'carenciaGris';
            let carencia3 = 'carenciaGris';
            let carencia4 = 'carenciaGris';
            let carencia5 = 'carenciaGris';
            let carencia6 = 'carenciaGris';

            // if(clavePersonaJefe != null && clavePersonaJefe != undefined){
                if(res != '' && Object.keys(res).length > 0){
                    $('#imgPrograms').html('');
                    if(res['imagenesProgramas'] != undefined && res['imagenesProgramas'].length > 0){
                        res['imagenesProgramas'].forEach(function(item){
                            if(item['IMAGEN_CUADRADA'] != "NA_CUADRADA.png"){
                                $('#imgPrograms').append(templateImagesPrograms(item['IMAGEN_CUADRADA'], item['NOMBREPROGRAMA'], item['IDLISTADO'],  item['PROGRAMA'], item['TIPOEVENTO']));

                                if(item['PROGRAMA'] == 1) carencia1 = '';
                                // else if(item['PROGRAMA'] == 'png') carencia2 = '';
                                else if(item['PROGRAMA'] == 5) carencia3 = '';
                                // else if(item['PROGRAMA'] == 'png') carencia4 = '';
                                // else if(item['PROGRAMA'] == 'png') carencia5 = '';
                                else if(item['PROGRAMA'] == 3) carencia6 = '';
                            }
                        });
                    }
                }
            // }

            /* Codigo de alam que agrega las carencias */
            var carencias = res.carencias;
            if(carencias != null){
                c1 = carencias['IC_CV1'];
                c2 = carencias['IC_SBV1'];
                c3 = carencias['IC_REZEDU1'];
                c4 = carencias['IC_ASALUD1'];
                c5 = carencias['IC_SS1'];
                c6 = carencias['IC_ALI1'];
                var imgs_carencias = "";
                if(c1 == 1) imgs_carencias += "<img class='sizeImage "+carencia1+" ' src='Storage/Evidencias/imgCarencias/5.Carencia-calidad-espacios.png' width='100px'>";
                if(c2 == 1) imgs_carencias += "<img class='sizeImage "+carencia2+" ' src='Storage/Evidencias/imgCarencias/4.Carencia-servicios-basicos-vivienda.png' width='100px'>";
                if(c3 == 1) imgs_carencias += "<img class='sizeImage "+carencia3+" ' src='Storage/Evidencias/imgCarencias/1.rezago-educativo.png' width='100px'>";
                if(c4 == 1) imgs_carencias += "<img class='sizeImage "+carencia4+" ' src='Storage/Evidencias/imgCarencias/2.Carencia-servicios-salud.png' width='100px'>";
                if(c5 == 1) imgs_carencias += "<img class='sizeImage "+carencia5+" ' src='Storage/Evidencias/imgCarencias/3.Carencia-seguridad-social.png' width='100px'>";
                if(c6 == 1) imgs_carencias += "<img class='sizeImage "+carencia6+" ' src='Storage/Evidencias/imgCarencias/6.Carencia-alimentaria.png' width='100px'>";

                $("#imgCarencias").html(imgs_carencias);
            }

        });

        request.fail(function(textStatus){
            console.log(textStatus);
        });
    }

    function getDatosDomicilio(){
        let METODO = 'POST';
        let objectSend = {
            opcion : 'datosDomicilio',
            idCuis:  idConsulta
        }
        let request = petitionSupport(URL, METODO, objectSend);

        request.done(function(response){
            if(response != undefined && Object.keys(response).length > 0){
                // var source = document.getElementById("templateDomicilio").innerHTML;
                // var template = Handlebars.compile(source);
                // document.getElementById("contentDomicilio").innerHTML = template(response);

                // $('#tipo_vial').val(response.C_TIPO_VIAL);
                $('#tipo_vial').val(response.C_TIPO_VIAL);
                $('#nombre_vial').val(response.NOMBRE_VIAL);
                $('#cp_vial').val(response.CP);

                $('#tipo_Acentamiento').val(response.C_TIPO_ASENTAMIENTO);
                $('#clave_tipo_acent').val(response.C_ASENTAMIENTO);
                $('#nombre_tipo_acent').val(response.NOMBRE_ASEN);

                $('#nombre_entrevial_1').val(response.NOMBRE_ENTRE_VIAL_1);
                $('#tipo_entevial_1').val(response.C_TIPO_ENTRE_VIAL_1);

                $('#nombre_entrevial_2').val(response.NOMBRE_ENTRE_VIAL_2);
                $('#tipo_entrevial_2').val(response.C_TIPO_ENTRE_VIAL_2);

                $('#nombre_vial_pos').val(response.NOMBRE_VIAL_POS);
                $('#tipo_vial_pos').val(response.C_TIPO_VIAL_POS);

                $('#descripcion_ubicacion').val(response.DESC_UBIC);

                $('#num_ext').val(response.NUM_EXT);
                $('#letra_ext').val(response.LETRA_EXT);

                $('#num_int').val(response.NUM_INT);
                $('#let_int').val(response.LETRA_INT);

                cVivienda = response.C_VIVIENDA;

                $('#modalDomicilio').modal('show');
            }
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
        });
    }

    function actualizarDomicilio(elemento, cveCampo){
        $('#'+cveCampo).attr('disabled', false);
        $(elemento).replaceWith("<button class='btn btn-block mb-3' onclick='actualizaDatosDomicilio(this, \""+cveCampo+"\");'>Actualizar</button>");

    }

    function actualizaDatosDomicilio(elemento, cveCampo){
        $('#'+cveCampo).attr('disabled', true);
        $(elemento).replaceWith("<button class='btn btn-default btn-block mb-3' onclick='actualizarDomicilio(this, \""+cveCampo+"\");'>Editar</button>");

        let newValue = $('#'+cveCampo).val();
        let field = $('#'+cveCampo).attr('data-nameField');

        let METODO = 'POST';
        let inputFileImage = document.getElementById("docSoporte");
        let file = inputFileImage.files[0];
        let dataFormSend = new FormData();
        let folioCedula = $('#folioCedulaCambio').val();

        dataFormSend.append('documentoSoporte',file);
        dataFormSend.append('opcion','updateAddress');
        dataFormSend.append('field',field);
        dataFormSend.append('newValue',newValue);
        dataFormSend.append('cVivienda',cVivienda);
        dataFormSend.append('folio_cuis',idConsulta);
        dataFormSend.append('folioCedula',folioCedula);

        let request = petitionSupportFile(URL, METODO, dataFormSend);

        request.done(function(res){
            if(res == true)
                alert('Información Actualizada');
            else
                alert('Error al actualizar');

            getDatosDomicilio();
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
        });

    }

    function dataCUIS(idConsulta){
        let METODO = 'POST';
        let dataObject = { opcion: 'cuis', idCuis:  idConsulta};
        let request = petitionSupport(URL, METODO, dataObject);

        request.done(function(res){
            if(Object.keys(res).length !== 0){
                $('#dataIntegrantes').html('');
                if(res.infoIntegrantes){
                    res.infoIntegrantes.forEach(function(integrante, index){
                        let dataIntegrate = {
                            tipoIntegrante : '',
                            nombre :  (integrante.NB_NOMBRE != null) ? integrante.NB_NOMBRE : '',
                            apPaterno :  (integrante.NB_PRIMER_AP != null) ? integrante.NB_PRIMER_AP : '',
                            apMaterno :  (integrante.NB_SEGUNDO_AP != null) ? integrante.NB_SEGUNDO_AP : '',
                            cPersona :  (integrante.C_PERSONA != null) ? integrante.C_PERSONA : '',
                            fNacimiento :  (integrante.FCH_NACIMIENTO != null) ? integrante.FCH_NACIMIENTO : '',
                            curp :  (integrante.NB_CURP != null) ? integrante.NB_CURP : '',
                            edoNacimiento :  (integrante.C_CD_EDO_NAC != null) ? integrante.C_CD_EDO_NAC : '',
                            cParentesco :  (integrante.C_CD_PARENTESCO != null) ? integrante.C_CD_PARENTESCO: '',
                            sizeDiv :  ''
                        }

            // $.noConflict();
            // $.tmpl("templateIntegrante", dataIntegrate).appendTo('#contenidoEjemplo');

                        let estatusPersona = (Object.keys(res.estatusIntegrantes).length > 0) ? res.estatusIntegrantes[integrante.C_PERSONA][0]['ESTATUS'] : 1;
                        if(integrante.C_CD_PARENTESCO == 1){

                            dataIntegrate.tipoIntegrante = '';
                            dataIntegrate.sizeDiv = 'col-md-12';

                            let newStructJefe  = consEstructInteg(dataIntegrate, objectEstados, res['catalogoParentezco'], estatusPersona, 'jefeFamilia');
                            $('#datosJefeHogar').html(newStructJefe);
                        }else{
                            let integrantes = consTipoIntegrante(integrante.C_CD_PARENTESCO, integrante.C_CD_SEXO);
                            dataIntegrate.tipoIntegrante = integrantes;
                            dataIntegrate.sizeDiv = 'col-md-4';
                            integrantes  = consEstructInteg(dataIntegrate, objectEstados, res['catalogoParentezco'], estatusPersona, 'Otro');
                            $('#dataIntegrantes').append(integrantes);
                        }

                    });

                }
            }
        });

        request.fail(function(jqXHR, textEstatus){
            console.log('Error: ',textEstatus);
        });
    }

    function petitionSupport(URL, METODO, dataObject){
        return $.ajax({
            url: URL,
            method: METODO,
            data: dataObject
            // async: false
        });
    }

    function petitionSupportAsinc(URL, METODO, dataObject){
        return $.ajax({
            url: URL,
            method: METODO,
            data: dataObject,
        });
    }

    function petitionSupportFile(URL, METODO, dataForm) {
        return $.ajax({
                url: URL,
                type:METODO,
                data: dataForm,
                processData : false,
                contentType : false,
                cache:false
            });
    }


    function consTipoIntegrante(tipoIntegrante, sexo = 0){
        let clasIntegrantes = {
            2: { 1: 'Cónyuge', 2: 'Cónyuge' },
            3: { 1: 'Hijo', 2: 'Hija'},
            4: { 1: 'Padre', 2: 'Madre'},
            5: { 1: 'Hermano', 2: 'Hermana'},
            6: { 1: 'Nieto', 2: 'Nieta'},
            7: { 1: 'Yerno', 2: 'Nuera'},
            8: { 1: 'Suegro', 2: 'Suegra'},
            9: { 1: 'Hijastro', 2: 'Hijastra'},
            10: { 1: 'Sobrino', 2: 'Sobrina'},
            11: { 1: 'Otro Parentesco', 2: 'Otro Parentesco' },
            12: { 1: 'No Tiene Parentesco', 2: 'No Tiene Parentesco' }
        }

        return '<label for="name" class="displayBlock">'+clasIntegrantes[tipoIntegrante][sexo]+'</label>'
    }

    function consEstructInteg(dataIntegrate, dataState, dataParent, estatusPersona, tipoPersona){
        let optionsState = contructComboOps(dataState, 'CVEENT', 'NOMBRE', dataIntegrate.edoNacimiento);
        let optionsParent = contructComboOps(dataParent, 'CVEPARENT', 'PARENTESCO', dataIntegrate.cParentesco);
        let isActive = (estatusPersona != 1 || estatusFamilia == 2) ? 'inActive' : '';

        /* _____________________________________________________________________________*/
        let structHtml = "<div class='"+dataIntegrate.sizeDiv+" spaceBottom-25'>\
                "+dataIntegrate.tipoIntegrante+"\
                <div class='input-group'>\
                    <div class='input-group-prepend firstLabel'>\
                        <label for='name' class='input-group-text displayBlock'>Nombre:</label>\
                    </div>\
                    <input type='text' placeholder='Nombre' class='form-control secondLabel "+isActive+" ' value='"+dataIntegrate.nombre+"' id='"+dataIntegrate.cPersona+"_nombre' disabled>\
                </div>";
        if((usuarioPerfil == 8 || usuarioPerfil == 10) && estatusPersona == 1 && estatusFamilia == 0 )
            structHtml +=  "<button class='btn btn-default btn-block mb-2' onclick='cambioElementoSave(this, "+dataIntegrate.cPersona+", \"nombre\")'>Editar</button>";

        /* _____________________________________________________________________________*/
        structHtml +=  "<div class='input-group'>\
                            <div class='input-group-prepend firstLabel'>\
                                <label for='firstLastName' class='input-group-text displayBlock'>Apellido Paterno</label>\
                            </div>\
                            <input type='text' placeholder='Apellido Paterno' class='form-control secondLabel "+isActive+"' value='"+dataIntegrate.apPaterno+"' id='"+dataIntegrate.cPersona+"_apPaterno' disabled>\
                        </div>";
        if((usuarioPerfil == 8 || usuarioPerfil == 10) && estatusPersona == 1 && estatusFamilia == 0 )
            structHtml +=  "<button class='btn btn-default btn-block mb-2' onclick='cambioElementoSave(this, "+dataIntegrate.cPersona+", \"apPaterno\")'>Editar</button>";

        /* _____________________________________________________________________________*/
        structHtml +=  "<div class='input-group'>\
                            <div class='input-group-prepend firstLabel'>\
                                <label for='secondLastName' class='input-group-text displayBlock'>Apellido Materno</label>\
                            </div>\
                            <input type='text' placeholder='Apellido Materno' class='form-control secondLabel "+isActive+"' value='"+dataIntegrate.apMaterno+"' id='"+dataIntegrate.cPersona+"_apMaterno' disabled>\
                        </div>";
        if((usuarioPerfil == 8 || usuarioPerfil == 10) && estatusPersona == 1 && estatusFamilia == 0 )
            structHtml +=  "<button class='btn btn-default btn-block mb-2' onclick='cambioElementoSave(this, "+dataIntegrate.cPersona+", \"apMaterno\")'>Editar</button>";

        /* _____________________________________________________________________________*/
        structHtml +=  "<div class='input-group'>\
                            <div class='input-group-prepend firstLabel'>\
                                <label for='birthDate' class='input-group-text displayBlock'>Fecha de Nacimiento</label>\
                            </div>\
                            <input type='text' placeholder='Fecha Nacimiento' onkeyup='this.value=formatDate(this.value);' class='form-control secondLabel "+isActive+"' value='"+dataIntegrate.fNacimiento+"' id='"+dataIntegrate.cPersona+"_fNacimiento' disabled>\
                        </div>";

        if((usuarioPerfil == 8 || usuarioPerfil == 10) && estatusPersona == 1 && estatusFamilia == 0 )
            structHtml +=  "<button class='btn btn-default btn-block mb-2' onclick='cambioElementoSave(this, "+dataIntegrate.cPersona+", \"fNacimiento\")'>Editar</button>";

        /* _____________________________________________________________________________*/
        structHtml +=  "<div class='input-group'>\
                            <div class='input-group-prepend firstLabel'>\
                                <label for='curp' class='input-group-text displayBlock'>CURP</label>\
                            </div>\
                            <input type='text' placeholder='CURP' class='form-control secondLabel "+isActive+"' value='"+dataIntegrate.curp+"' id='"+dataIntegrate.cPersona+"_curp' disabled>\
                        </div>";
        if((usuarioPerfil == 8 || usuarioPerfil == 10) && estatusPersona == 1 && estatusFamilia == 0 )
            structHtml +=  "<button class='btn btn-default btn-block mb-2' onclick='cambioElementoSave(this, "+dataIntegrate.cPersona+", \"curp\")'>Editar</button>";

        /* _____________________________________________________________________________*/
        structHtml +=  "<div class='input-group'>\
                            <div class='input-group-prepend firstLabel'>\
                                <label for='birthState' class='input-group-text displayBlock'>Lugar de Nacimiento</label>\
                            </div>\
                            <select name='' class='form-control secondLabel "+isActive+"' id='"+dataIntegrate.cPersona+"_edoNacimiento' disabled>"+optionsState+"</select>\
                        </div>";
        if((usuarioPerfil == 8 || usuarioPerfil == 10) && estatusPersona == 1 && estatusFamilia == 0 )
            structHtml +=  "<button class='btn btn-default btn-block mb-2' onclick='cambioElementoSave(this, "+dataIntegrate.cPersona+", \"edoNacimiento\")'>Editar</button>";


        /* _____________________________________________________________________________*/
        structHtml +=  "<div class='input-group'>\
                            <div class='input-group-prepend firstLabel'>\
                                <label for='typeC_Person' class='input-group-text displayBlock'>Parentesco</label>\
                            </div>\
                            <select name='' class='form-control secondLabel "+isActive+"' id='"+dataIntegrate.cPersona+"_parentesco'\
                                data-CPersona='"+dataIntegrate.cPersona+"'\
                                data-personType='"+tipoPersona+"'\
                                data-lastValue='"+dataIntegrate.cParentesco+"' disabled>"+optionsParent+"</select>\
                        </div>";

        if((usuarioPerfil == 8 || usuarioPerfil == 10) && estatusPersona == 1 && estatusFamilia == 0 )
            structHtml +=  "<button class='btn btn-block mb-2' onclick='cambioElementoSave(this, "+dataIntegrate.cPersona+", \"parentesco\")' data-CPersona='"+dataIntegrate.cPersona+"'>Editar</button>\
                <button class='btn btn-info btn-block mt-3' onclick='confirmUpdate(\"¿ Deseas eliminar el integrante "+dataIntegrate.nombre+"\","+dataIntegrate.cPersona+", 0)'>Eliminar Integrante</button>";

        if(estatusPersona == 0)
            structHtml +=  "<button class='btn btn-info btn-block mt-3' onclick='confirmUpdate(\"¿ Deseas habilitar el integrante "+dataIntegrate.nombre+"\","+dataIntegrate.cPersona+", 1)'>Habilitar Integrante</button>";


        structHtml +=  "</div>";

        return structHtml;
    }

    function confirmUpdate(name, idIntegrated, newState){

        $('#nameIntegrated').html(name);
        $('#idIntegratedCuis').val(idIntegrated);
        $('#newStateIntg').val(newState);
        $('#modalIntegrante').modal('show');
    }

    function contructComboOps(objectData, key, value, selectedItem = ''){
        let newElementInner = '<option value=\'\'>Seleccione...</option>';

        if(objectData){
            if(Object.keys(objectData).length > 0){
                objectData.forEach(function(item){
                    if(selectedItem == item[key])
                        newElementInner += '<option value='+item[key]+' selected>'+item[value]+'</option>';
                    else
                        newElementInner += '<option value='+item[key]+'>'+item[value]+'</option>';
                });
            }
        }
        return newElementInner;
    }

    function createMapOpenStreet(latitud, longitud, nombAsent){
        var map = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({
                source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                // Longitud-Latitud
                center: ol.proj.fromLonLat([parseFloat(longitud),  parseFloat(latitud)]),
                zoom: 14
            })
        });

        $("#map").append('<div id="marker" class="marker"><i class="fa fa-map-marker" aria-hidden="true"></i></div>');
         var marker = new ol.Overlay({
            position: ol.proj.fromLonLat([parseFloat(longitud), parseFloat(latitud)]),
            positioning: 'center-center',
            element: document.getElementById('marker'),
            stopEvent: false
        });

        map.addOverlay(marker);

        // document.getElementById('map').src = 'http://geomap.nagvis.org/?module=map&lon='+longitud+'&lat='+latitud+'&zoom=15&width=350&height=200&points='+longitud+','+latitud+'&pointImagePattern=sight';
        $('#urlMap').attr('href', '?view=map&params={%22location%22:%22'+latitud+','+longitud+'%22}');
    }


    function cambioElementoSave(elemento, idCampoAct, campoAct){
        $(elemento).replaceWith("<button class='btn btn-block mb-2' onclick='actualizaDatos(this, "+idCampoAct+", \""+campoAct+"\");'>Actualizar</button>");
        $("#"+idCampoAct+"_"+campoAct).prop("disabled", false);

        if(campoAct == 'parentesco' && $("#"+idCampoAct+"_"+campoAct).attr("data-personType") == "jefeFamilia"){
            $('select[data-CPersona]').prop("disabled", false);
        }
    }

    function cambioElementoSaveCuis(elemento, idCampoAct, campoAct){
        $(elemento).replaceWith("<button class='btn btn-block mb-2' onclick='actualizaCuisGenerales(this, "+idCampoAct+", \""+campoAct+"\");'>Actualizar</button>");
        $("#"+idCampoAct+"_"+campoAct).prop("disabled", false);
    }


    function actualizaCuisGenerales(elemento, idCampoAct, campoAct){
        $(elemento).replaceWith("<button class='btn btn-block mb-2' onclick='cambioElementoSaveCuis(this, "+idCampoAct+", \""+campoAct+"\");'>Editar</button>");
        $("#"+idCampoAct+"_"+campoAct).prop("disabled", true);

        let nuevoValor = $("#"+idCampoAct+"_"+campoAct).val();
        let idCuisCons = '<?php echo $_GET['folio'] ?>';

        let METODO = 'POST';

        var inputFileImage = document.getElementById("docSoporte");
        var file = inputFileImage.files[0];
        var dataFormSend = new FormData();
        let folioCedula = $('#folioCedulaCambio').val();

        dataFormSend.append('documentoSoporte',file);
        dataFormSend.append('opcion','updateFieldsCuisGenerales');
        dataFormSend.append('field',campoAct);
        dataFormSend.append('newValue',nuevoValor);
        dataFormSend.append('IdCuis',idCampoAct);
        dataFormSend.append('folioCedula',folioCedula);

        var request = petitionSupportFile(URL, METODO, dataFormSend);

        request.done(function(res){
            console.log(res);
        });
    }

    // function cargarDocSoporte(){
    //     let folioCedula = $('#folioCedulaCambio').val();

    //     if(folioCedula != null && folioCedula != ''){
    //         var inputFileImage = document.getElementById("docSoporte");
    //         var file = inputFileImage.files[0];
    //         console.log(file);
    //         if(file != undefined){
    //             var data = new FormData();

    //             data.append('documentoSoporte',file);
    //             data.append('opcion','saveDocSupport');


    //             $.ajax({
    //                 url: URL,
    //                 type:'POST',
    //                 data: data,
    //                 processData : false,
    //                 contentType : false,
    //                 cache:false
    //             }).done({function(res){
    //                 console.log(res);
    //             }});
    //         }else{
    //             $('#toast').html('Cargue un documento antes de continuar');
    //             showMessage();
    //         }
    //     }
    //     else{
    //         $('#toast').html('Por Favor Ingresa el Folio Cédula de Cambio');
    //         showMessage();
    //     }
    // }

    function showMessage() {
        var x = document.getElementById("toast");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }


    function actualizaDatos(elemento, idCampoAct, campoAct){
        // var inputFileImage = document.getElementById("docSoporte");
        // var file = inputFileImage.files[0];
        // var fileName = inputFileImage.files[0].name;
        // var imagen = new FormData();
        // imagen.append(fileName,file);



        let jefeFamilia = 0;
        if(campoAct == 'parentesco' && $("#"+idCampoAct+"_"+campoAct).attr("data-personType") == "jefeFamilia"){
            $('select[data-CPersona]').each(function(){
                if($(this).val() == 1){
                    jefeFamilia++;
                }
            });

            if(jefeFamilia == 0){
                alert('Por Favor Asigne un Jefe de Familia');
            }
            else if(jefeFamilia == 1){

                let countActualizaciones = 0;
                $('select[data-CPersona]').each(function(){
                    let nuevoValor = $(this).val();
                    let idCuisCons = idConsulta;
                    let c_personaCampoActual =  $(this).attr('data-CPersona');
                    let lastValue =  $(this).attr('data-lastValue');
                    let folioCedula = $('#folioCedulaCambio').val();
                    if(lastValue != nuevoValor){
                        countActualizaciones++;
                        let METODO = 'POST';

                        var inputFileImage = document.getElementById("docSoporte");
                        var file = inputFileImage.files[0];
                        var dataFormSend = new FormData();
                        dataFormSend.append('documentoSoporte',file);
                        dataFormSend.append('opcion','updateFields');
                        dataFormSend.append('field',campoAct);
                        dataFormSend.append('newValue',nuevoValor);
                        dataFormSend.append('C_Person',c_personaCampoActual);
                        dataFormSend.append('idCuis',idCuisCons);
                        dataFormSend.append('folio_cuis',idConsulta);
                        dataFormSend.append('folioCedula',folioCedula);

                        // let object = {
                        //     opcion: 'updateFields',
                        //     field: campoAct,
                        //     newValue: nuevoValor,
                        //     C_Person: c_personaCampoActual,
                        //     idCuis: idCuisCons,
                        //     folio_cuis : idConsulta
                        // }
                        var request = petitionSupportFile(URL, METODO, dataFormSend);
                        request.done(function(res){
                            if('SEDEVER' in res && 'INTEGRADOR' in res){
                                let response = res;

                                if(response.SEDEVER && response.INTEGRADOR){
                                    alert('Información actualizada correctamente');
                                    dataCUIS(idConsulta);
                                }else{
                                    alert('Error al realizar la actualización');
                                }
                            }
                            else if(res.ok){
                                alert(res.msg);
                            }
                        });
                    }
                });
                if(countActualizaciones == 0){
                    alert('No hemos encontrado Información para actualizar');
                }

            }
            else if(jefeFamilia > 1){
                alert("Solo Puede Haber un Jefe de Familia");
            }
        }
        else{
            $(elemento).replaceWith("<button class='btn btn-default btn-block' onclick='cambioElementoSave(this, "+idCampoAct+", \""+campoAct+"\");'>Editar</button>");
            $("#"+idCampoAct+"_"+campoAct).prop("disabled", true);

            let nuevoValor = $("#"+idCampoAct+"_"+campoAct).val();
            let idCuisCons = '<?php echo $_GET['folio'] ?>';

            let METODO = 'POST';

            var inputFileImage = document.getElementById("docSoporte");
            var file = inputFileImage.files[0];
            var dataFormSend = new FormData();
            let folioCedula = $('#folioCedulaCambio').val();

            dataFormSend.append('documentoSoporte',file);
            dataFormSend.append('opcion','updateFields');
            dataFormSend.append('field',campoAct);
            dataFormSend.append('newValue',nuevoValor);
            dataFormSend.append('C_Person',idCampoAct);
            dataFormSend.append('idCuis',idCuisCons);
            dataFormSend.append('folio_cuis',idConsulta);
            dataFormSend.append('folioCedula',folioCedula);
            // let object = {
            //     opcion: 'updateFields',
            //     field: campoAct,
            //     newValue: nuevoValor,
            //     C_Person: idCampoAct,
            //     idCuis: idCuisCons,
            //     folio_cuis : idConsulta
            // }
            var request = petitionSupportFile(URL, METODO, dataFormSend);

            request.done(function(res){
                if('SEDEVER' in res && 'INTEGRADOR' in res){
                    let response = res;

                    if(response.SEDEVER && response.INTEGRADOR){
                        alert('Información actualizada correctamente');
                        dataCUIS(idConsulta);
                    }else{
                        alert('Error al realizar la actualización');
                    }
                }
            });
        }
    }

    function updateIntegrated(){
        $('#exampleModal').modal('hide');
        let claveIntg = $('#idIntegratedCuis').val();
        let newState = $('#newStateIntg').val();

        let METODO = 'POST';
        var inputFileImage = document.getElementById("docSoporte");
        var file = inputFileImage.files[0];
        var dataFormSend = new FormData();
        let folioCedula = $('#folioCedulaCambio').val();

        dataFormSend.append('documentoSoporte',file);
        dataFormSend.append('opcion','updateIntegrated');
        dataFormSend.append('claveIntegrante',claveIntg);
        dataFormSend.append('nuevoEstado',newState);
        dataFormSend.append('folio_cuis',idConsulta);
        dataFormSend.append('folioCedula',folioCedula);
        // let objectSend = {
        //     opcion : 'updateIntegrated',
        //     claveIntegrante : claveIntg,
        //     nuevoEstado : newState,
        //     folio_cuis : idConsulta
        // }

        let request = petitionSupportFile(URL, METODO, dataFormSend);

        request.done(function(res){
            dataCUIS(idConsulta);
            $('#modalIntegrante').modal('hide');
        });
    }

    function deleteFamily(){
        let claveCuis = $('#folioCuisFamilia').val();
        let nuevoEstado = $('#newStateFamilia').val();
        let METODO = 'POST';

        var inputFileImage = document.getElementById("docSoporte");
        var file = inputFileImage.files[0];
        var dataFormSend = new FormData();
        let folioCedula = $('#folioCedulaCambio').val();
        dataFormSend.append('documentoSoporte',file);
        dataFormSend.append('opcion','updateFamily');
        dataFormSend.append('claveCuis',claveCuis);
        dataFormSend.append('nuevoEstado',nuevoEstado);
        dataFormSend.append('folio_cuis',idConsulta);
        dataFormSend.append('folioCedula',folioCedula);

        // let objectSend = {
        //     opcion : 'updateFamily',
        //     claveCuis : claveCuis,
        //     nuevoEstado : nuevoEstado,
        //     folio_cuis : idConsulta
        // }

        let request = petitionSupportFile(URL, METODO, dataFormSend);

        request.done(function(res){
            if(res){
                $('#modalFamilia').modal('hide');
                alert('Actualización Correcta');
                datosGenerales();
                dataCUIS(idConsulta);
            }
        });
    }


    var anguloFotoImg = 0;
    var anguloHuellaImg = 0;
    var anguloFirmaImg = 0;
    function rotar(obj, angulo){

        if(obj == 'fotoImg')  anguloFotoImg += angulo;
        if(obj == 'huellaImg')  anguloHuellaImg += angulo;
        if(obj == 'firmaImg') anguloFirmaImg += angulo;

        if(anguloFotoImg == -360 || anguloFotoImg == 360){
            anguloFotoImg = 0;
        }
        if(anguloHuellaImg == -360 || anguloHuellaImg == 360){
            anguloHuellaImg = 0;
        }
        if(anguloFirmaImg == -360 || anguloFirmaImg == 360){
            anguloFirmaImg = 0;
        }

        if( anguloFotoImg == 0 && anguloHuellaImg == 0 && anguloFirmaImg == 0){
            $('#saveChangesImages').fadeOut();
        }
        else{
            $('#saveChangesImages').fadeIn();
        }

        if(obj == 'fotoImg') {

            auxRotate(obj, anguloFotoImg);
        }
        if(obj == 'huellaImg'){
            auxRotate(obj, anguloHuellaImg);
        }
        if(obj == 'firmaImg'){
            auxRotate(obj, anguloFirmaImg);
        }



    }

    // $(document).ready(function(){
    function auxRotate(idElement, degrees){
        $("#"+idElement).css({
            'transform': 'rotate(' + degrees + 'deg)',
            '-ms-transform': 'rotate(' + degrees + 'deg)',
            '-moz-transform': 'rotate(' + degrees + 'deg)',
            '-webkit-transform': 'rotate(' + degrees + 'deg)',
            '-o-transform': 'rotate(' + degrees + 'deg)'
        });
    }
    // });

    function sendImgRotate(){
        let metodo = 'POST';
        let objectSend = {
            opcion : 'rotateImage'
        };
        let urlSecond = 'http://75.126.73.173:8025/veracruz-sistema/informacion/rotarImagenes.php';

        if(anguloFotoImg != 0){
            objectSend.pathFotoImg = $('#fotoImg').attr('src');
            objectSend.fotoImg = anguloFotoImg;
        }
        if(anguloHuellaImg != 0){
            objectSend.pathHuellaImg = $('#huellaImg').attr('src');
            objectSend.huellaImg = anguloHuellaImg;
        }
        if(anguloFirmaImg != 0){
            objectSend.pathFirmaImg = $('#firmaImg').attr('src');
            objectSend.firmaImg = anguloFirmaImg;
        }
        let request = petitionSupport(urlSecond, metodo, objectSend);

        request.done(function(res){
            console.log(res);
            let request = JSON.parse(res);
            if(anguloFotoImg != 0){
                if(res['fotoImg'] == false)
                    alert('Error al rotar la foto');
                else{
                    alert('Foto Rotada correctamente');
                    anguloFotoImg = 0;
                    var srcImage = $('#fotoImg').attr('src');
                    var img = document.getElementById('fotoImg');
                    img.src = "";
                    img.src = srcImage;
                    // $('#fotoImg').css("transform","");
                }
            }

            if(anguloHuellaImg != 0 ){
                if(res['huellaImg'] == false)
                    alert('Error al rotar la huella');
                else{
                    alert('Huella Rotada correctamente');
                    anguloHuellaImg = 0;
                    var srcImage = $('#huellaImg').attr('src');
                    var img = document.getElementById('huellaImg');
                    img.src = "";
                    img.src = srcImage;
                }
            }

            if(anguloFirmaImg != 0){
                if(res['firmaImg'] == false)
                    alert('Error al rotar la firma');
                else{
                    alert('Firma Rotada correctamente');
                    anguloFirmaImg = 0;
                    var srcImage = $('#firmaImg').attr('src');
                    var img = document.getElementById('firmaImg');
                    img.src = "";
                    img.src = srcImage;
                }
            }

        });

    }

    function resetearInput() {
        var $fileupload = $('#docSoporte');
        $fileupload.wrap('<form>').closest('form').get(0).reset();
        $fileupload.unwrap();

        $('#nameFile').text('Cargar Documento Soporte...');
    }

</script>
