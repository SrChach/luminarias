<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Combos Pruebas</title>

    <style>
        .div-control{
            height: calc(2.25rem + 2px);
            display: block;
            width: 100%;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }

        .input-group > .content-control {
            position: relative;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            width: 1%;
            margin-bottom: 0;
        }
    </style>

    <style>
        .min-w-141{
            min-width: 141px;
        }
        .styleHeader{
            color: white;
            background-color: #2b94b2e6;
            z-index: 1;
            width: 100%;
        }

        @media (max-width: 280px){
            .labelCat > div, .labelTipImg > div, .labelTipDoc > div, .modalLabels > div{
                white-space: inherit;
            }
            .inputNomDoc > div, .labelNomTpDoc-2 > div, .modalLabels > div{
                white-space: inherit;
            }
        }
        @media (max-width: 310px){
            .labelCat, .labelTipImg, .labelTipDoc, .modalLabels{
                width: 100%;
            }
            .labelCat > div, .labelTipImg > div, .labelTipDoc > div, .modalLabels > div{
                border-top-right-radius: .25rem !important;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
                width: 100%;
            }
            #selectCategorias, #selectTipoImg, #selectTipoDoc, .modalField{
                border-top-right-radius: 0;
                border-top-left-radius: 0;
                border-bottom-right-radius: .25rem !important;
                border-bottom-left-radius: .25rem !important;
            }
        }
        @media (max-width: 490px){
            .labelNomTpDoc-2{
                width: 100%;
            }
            .labelNomTpDoc-2 > div{
                border-top-right-radius: .25rem !important;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
                width: 100%;
            }
            .inputNomTpDoc-2{
                border-top-right-radius: 0;
                /* border-top-left-radius: 0; */
                /* border-bottom-right-radius: .25rem; */
                border-bottom-left-radius: .25rem !important;
            }
        }
        @media (min-width: 360px){
            .styleHeader{
                top: -20px;
                position: absolute;
                width: 90%;
                transform: translateX(5.8%);
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                box-shadow: 10px 10px 10px cadetblue;
            }
            .close{
                color: white;
                opacity: inherit;
            }
            .modalLabels > div{
                min-width: 150px;
            }
        }
        /* .nuevaCatText{
            background-color: #3298b7;
            color: white;
        } */


        .inputNomDoc{
            width: 100%;
        }
        .inputNomDoc > div{
            border-top-right-radius: .25rem !important;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            width: 100%;
        }
        .nomTpDoc{
            border-top-right-radius: 0;
            border-top-left-radius: 0;
            border-bottom-right-radius: .25rem;
            border-bottom-left-radius: .25rem !important;
        }

        .alignMiddleContent{
            display: grid;
            align-content: center;
        }

        .btn-red{
            background-color: #ec3535b0;
            border-color: #ec3535b0;
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
            color: white;
        }
        .display-none{
            display: none;
        }

        /* .datTipImg, .datTipDoc, .groupCat-1, .groupCat-2, .groupCat-3{ */
        .datTipImg, .datTipDoc{
            display: none;
        }

        #tableContent > table{
            width: 80%;
            margin: auto;
        }
        #tableContentCat > table{
            width: 60%;
            margin: auto;
        }

        .bloqueo{
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            overflow: hidden;
            outline: 0;
            position: fixed;
            background-color: rgba(154, 161, 164, 0.2);
            z-index: 10;
            display : none;
        }
        /* .loaderStandar > i{
            color: #2bb273;
        } */
        .loaderStandar{
            margin: auto;
            vertical-align: middle;
            font-size: 6rem;
            /* color: #2bb273; */
            color: #28A6DE;
            position: absolute;
            top: 45%;
            width : 100%;
            text-align : center;
        }

    </style>

</head>
<body>
    <div class="bloqueo text-center">
        <div class="loaderStandar text-center">
            <h4 class="text-center bold" style="color: #2bb273;">Cargando ...</h4>
            <i class="fa fa-spinner fa-spin" style="color: #28A6DE;"></i>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row justify-content-center"><h3>Administrador Imágenes</h3></div>
        <section>
            <div class="row">
                <div class="col-md-4 col-lg-3 mr-auto">
                    <div class="col-auto my-3">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend labelCat">
                                <div class="input-group-text min-w-141">Categoria</div>
                            </div>
                            <div class="content-control" id="contCategory"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 offset-lg-1 ml-auto mr-auto">
                    <div class="col-auto my-3">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend labelTipImg">
                                <div class="input-group-text min-w-141">Tipo Imagen</div>
                            </div>
                            <div class="content-control" id="contTipoImg">
                                <select name="" id="" class="div-control">
                                    <option value="">Seleccione...</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 offset-lg-1 ml-auto">
                    <div class="col-auto my-3 groupCat-1-btnAdd">
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('datCat', true)"><h6>Agregar Categoria</h6></button>
                    </div>
                    <div class="col-auto my-3 groupCat-2-btnAdd display-none">
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('datTipImg', true)"><h6>Agrega Tipo Imagen </h6></button>
                    </div>
                    <div class="col-auto my-3 groupCat-3-btnAdd display-none">
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('datTipDoc', true)"><h6>Agrega Tipo Documento </h6></button>
                    </div>
                    <!-- <div class="col-auto my-3">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend labelTipDoc">
                                <div class="input-group-text min-w-141">Tipo Documento</div>
                            </div>
                            <select name="" id="selectTipoDoc" class="form-control">
                                <option value="" selected>Selecciona</option>
                            </select>
                        </div>
                    </div>                 -->
                </div>
            </div>
        </section>

        <section class="datCat display-none" id="datCategoriasGroup">
            <!-- <div class="container-fluid"> -->
                <div class="row">
                    <div class="col-md-12 col-lg-9">
                        <div class="container-fluid mt-5 groupCat-1"><h4>Categorias</h4></div>
                        <div class="row">
                            <div class="col-md-6 groupCat-1">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre de la Categoria</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2" placeholder="Nombre Categoria" id="nameCategory">
                                    </div>
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-6 groupCat-1">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre del Tipo Imagen</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2" placeholder="Nombre Tipo Imagen" id="namTypImgCat">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 groupCat-1">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Corto</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2 limitCaracter" placeholder="Nombre Corto" id="namTypImgCatShort">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 groupCat-1">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Del Tipo Documento</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2" placeholder="Nombre Tipo Documento" id="namTypDoc">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 groupCat-1">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Corto</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2 limitCaracter" placeholder="Nombre Corto" id="namTypDocShort">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 alignMiddleContent pb-5">
                        <div class="col-auto pb-5 row">
                            <!-- <div class="col-md-6 col-lg-12 my-2 groupCat-1-btnAdd">
                                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('groupCat-1', true)"><h6>Agregar Categoria</h6></button>
                            </div> -->
                            <div class="col-md-6 col-lg-12 my-2 groupCat-1-btnCancel">
                                <button class="btn btn-red btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('datCat', false)"><h6>Cancelar</h6></button>
                            </div>
                            <div class="col-md-6 col-lg-12 my-2 groupCat-1-btnSave">
                                <button class="btn btn-success btn-block" data-toggle="modal" data-target="" onclick="addCategory();"><h6>Guardar</h6></button>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- </div>         -->
        </section>

        <section class="datTipImg display-none" id="datTipoImagGroup">
            <!-- <div class="container-fluid"> -->
                <div class="row">
                    <div class="col-md-12 col-lg-9">
                        <div class="container-fluid mt-5 groupCat-2"><h4>Tipo Imagen Para la Categoria <span id="catSelect"></span> </h4></div>
                        <div class="row">
                            <div class="col-md-6 groupCat-2">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Nuevo Tipo Imagen</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2" placeholder="Nombre Nuevo Tipo Imagen" id="nameTypeImg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 groupCat-2">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Corto</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2 limitCaracter" placeholder="Nombre Corto" id="nameTypeImgShort">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 groupCat-2">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Del Tipo Documento</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2" placeholder="Nombre Tipo Documento" id="nameTypeDocImg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 groupCat-2">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Corto</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2 limitCaracter" placeholder="Nombre Corto" id="nameTypeDocShortImg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 alignMiddleContent pb-5">
                        <div class="col-auto pb-5 row">
                            <!-- <div class="col-md-6 col-lg-12 my-2 groupCat-2-btnAdd">
                                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('groupCat-2', true)"><h6>Agrega Tipo Imagen </h6></button>
                            </div> -->
                            <div class="col-md-6 col-lg-12 my-2 groupCat-2-btnCancel">
                                <button class="btn btn-red btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('datTipImg', false)"><h6>Cancelar</h6></button>
                            </div>
                            <div class="col-md-6 col-lg-12 my-2 groupCat-2-btnSave">
                                <button class="btn btn-success btn-block" data-toggle="modal" data-target="" onclick="addTypeImg();"><h6>Guardar</h6></button>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- </div>         -->
        </section>

        <section class="datTipDoc display-none" id="datTipDocGroup">
            <!-- <div class="container-fluid"> -->
                <div class="row">
                    <div class="col-md-12 col-lg-9">
                        <div class="container-fluid mt-5 groupCat-3"><h4>Tipo de Documento Para el Tipo Imagen <span id="textSelectTpImg"></span> </h4></div>
                        <div class="row">
                            <div class="col-md-6 groupCat-3">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Del Tipo Documento</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2" placeholder="Nombre Tipo Documento" id="nameTypeDoc">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 groupCat-3">
                                <div class="col-auto my-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend labelNomTpDoc-2">
                                            <div class="input-group-text min-w-280">Nombre Corto</div>
                                        </div>
                                        <input type="text" class="form-control inputNomTpDoc-2 limitCaracter" placeholder="Nombre Corto" id="nameTypeDocShort">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 alignMiddleContent pb-5">
                        <div class="col-auto pb-5 row">
                            <!-- <div class="col-md-6 col-lg-12 my-2 groupCat-3-btnAdd">
                                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('groupCat-3', true)"><h6>Agrega Tipo Documento </h6></button>
                            </div> -->
                            <div class="col-md-6 col-lg-12 my-2 groupCat-3-btnCancel">
                                <button class="btn btn-red btn-block" data-toggle="modal" data-target="" onclick="toggleDisableGroup('datTipDoc', false)"><h6>Cancelar</h6></button>
                            </div>
                            <div class="col-md-6 col-lg-12 my-2 groupCat-3-btnSave">
                                <button class="btn btn-success btn-block" data-toggle="modal" data-target="" onclick="addTypeDoc();"><h6>Guardar</h6></button>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- </div>         -->
        </section>

        <section>
            <div class="container-fluid mt-4">
                <div id="tableContent"></div>
                <div id="tableContentCat"></div>
            </div>
        </section>

        <section>
            <!-- Modal -->
            <div class="modal fade" id="agregarTipoImagen" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg mt-5 pt-5" role="document">
                    <div class="modal-content">
                        <div class="modal-header styleHeader">
                            <h5 class="modal-title" id="">EDITAR</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mt-4">
                            <!-- <div class="row p-3"><h5 class="m-auto">Tipo Imagen</h5></div> -->
                            <div class="row">
                                <!-- <div class="col-md-6"> -->
                                    <div class="col-md-9 my-3" id="contentEdit"></div>
                                    <div class="col-md-3 my-3" id="contentButtonEdit"></div>
                                <!-- </div>                                                         -->
                            </div>

                        </div>
                        <div class="modal-footer" >
                            <button type="button" class="btn btn-red" data-dismiss="modal">CANCELAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

</body>

    <script>
        var URL = "../../public/portal/controller/gestionImg_controller.php";
        var comboURL = "../../public/portal/controller/combos-291118.php";

        var objectTipoImg = new Object();
        var objectTipoDoc = new Object();

        var getTypeImag = function(render = false){
            // let valueSelCat = $('#selectCategorias').text();
            let valueSelCat = $('#selectCategorias option:selected').text()
            if(valueSelCat == '' || valueSelCat == null || valueSelCat == 'Selecciona'){
                toggleContent('datTipImg', false);
                toggleContent('datTipDoc', false);
                $('.groupCat-3-btnAdd').hide();
                $('.groupCat-2-btnAdd').hide();
                $('.groupCat-1-btnAdd').show();

                // $('#selectTipoDoc').html(construcOp());
                console.log('_______________________________________');
                $('#selectTipoImg').html(construcOp());
                $('#tableContent').html('');
                $('#tableContentCat').show();
                $('#tableContent').hide();
            }
            else if(valueSelCat in objectTipoImg && render == false){
                // if(!$('.groupCat-1-btnCancel').is(':visible'))$('.groupCat-1-btnAdd').show();
                $('.groupCat-3-btnAdd').hide();
                $('.groupCat-2-btnAdd').show();
                $('.groupCat-1-btnAdd').hide();
                toggleContent('datCat', false);

                let objectHeaders = ['CLAVEIMAGEN', 'NOMBRE', 'NOMBRE_CORTO', 'EDITAR'];
                let objectFields = ['CLAVEIMAGEN', 'NOMBRE', 'NOMBRE_CORTO',];
                let newTable = construcTable(objectTipoImg[valueSelCat], objectHeaders, objectFields, true, 'tabTipoImgDt');
                let options = construcOp(objectTipoImg[valueSelCat], "CLAVEIMAGEN", "NOMBRE");
                $('#selectTipoImg').html(options);
                $('#tableContent').html(newTable);
                $('#tableContent').show();
                $('#tableContentCat').hide();
                $('#catSelect').text(valueSelCat);

                tablePadron('#tabTipoImgDt', 'Categorias', objectHeaders.length);
            }else{
                $('.groupCat-3-btnAdd').hide();
                $('.groupCat-2-btnAdd').show();
                $('.groupCat-1-btnAdd').hide();
                toggleContent('datCat', false);
                toggleContent('datTipImg', false);

                let metodo = "POST";
                let objectElement = {
                    option: "typeImage",
                    category: valueSelCat
                }
                let request = supportRequest(objectElement, metodo, URL);
                $('.bloqueo').show();
                request.done(function(res){
                    let response = JSON.parse(res);
                    let objectHeaders = ['CLAVEIMAGEN', 'NOMBRE', 'NOMBRE_CORTO', 'EDITAR'];
                    let objectFields = ['CLAVEIMAGEN', 'NOMBRE', 'NOMBRE_CORTO',];
                    let options = construcOp(response, "CLAVEIMAGEN", "NOMBRE");
                    let newTable = construcTable(response, objectHeaders, objectFields, true, 'tabTipoImgDt');
                    $('#tableContent').html(newTable);
                    $('#selectTipoImg').html(options);
                    $('#tableContent').show();
                    $('#tableContentCat').hide();
                    $('#catSelect').text(valueSelCat);
                    objectTipoImg[valueSelCat] = response;
                    tablePadron('#tabTipoImgDt', 'Categorias', objectHeaders.length);
                    $('.bloqueo').fadeOut('slow');
                });

                request.fail(function(jqXHR, textStatus){
                    console.log('Peticion fallida: ',textStatus);
                    $('.bloqueo').fadeOut('slow');
                });
            }
        }

        var getTypeDocs = function(render = false){
            let valueSelTipImg = $('#selectTipoImg').val();
            let textSelTipImg = $('#selectTipoImg option:selected').text();

            if(valueSelTipImg == '' || valueSelTipImg == null){
                toggleContent('datTipDoc', false);
                $('.groupCat-3-btnAdd').hide();
                $('.groupCat-2-btnAdd').show();
                // $('.groupCat-1-btnAdd').fadeOut();
                // $('#selectTipoDoc').html(construcOp());
                getTypeImag();
            }
            else if((valueSelTipImg in objectTipoDoc) && render == false){
                toggleContent('datTipImg', false);
                $('.groupCat-3-btnAdd').show();
                $('.groupCat-2-btnAdd').hide();
                // $('.groupCat-1-btnAdd').fadeOut();

                let objectHeaders = ['CLAVEDOCUMENTO', 'TIPODOCUMENTO', 'NOMBRE CORTO', 'EDITAR'];
                let objectFields = ['CLAVEDOCUMENTO', 'TIPODOCUMENTO', 'NOMBRE_CORTO'];
                let newTable = construcTable(objectTipoDoc[valueSelTipImg], objectHeaders, objectFields, true, 'tabDocTp');
                // let options = construcOp(objectTipoDoc[valueSelTipImg], 'CLAVEDOCUMENTO', 'TIPODOCUMENTO');
                // $('#selectTipoDoc').html(options);
                $('#tableContent').html(newTable);
                $('#textSelectTpImg').html(textSelTipImg);
                tablePadron('#tabDocTp', 'Documentos', objectHeaders.length);
            }
            else{
                toggleContent('datTipImg', false);
                $('.groupCat-3-btnAdd').show();
                $('.groupCat-2-btnAdd').hide();
                // $('.groupCat-1-btnAdd').fadeOut();

                let METODO = 'POST';
                let objectData = {
                    option: 'typeDocument',
                    typeImg: valueSelTipImg
                }
                let request = supportRequest(objectData, METODO, URL);
                $('.bloqueo').show();
                request.done(function(res){
                    let response = JSON.parse(res);
                    let objectHeaders = ['CLAVEDOCUMENTO', 'TIPODOCUMENTO', 'NOMBRE CORTO', 'EDITAR'];
                    let objectFields = ['CLAVEDOCUMENTO', 'TIPODOCUMENTO', 'NOMBRE_CORTO'];
                    let newTable = construcTable(response, objectHeaders, objectFields, true, 'tabDocTp');
                    // let options = construcOp(response, 'CLAVEDOCUMENTO', 'TIPODOCUMENTO');
                    // $('#selectTipoDoc').html(options);
                    $('#tableContent').html(newTable);
                    $('#textSelectTpImg').html(textSelTipImg);
                    tablePadron('#tabDocTp', 'Documentos', objectHeaders.length);
                    objectTipoDoc[valueSelTipImg] = response;
                    $('.bloqueo').fadeOut('slow');
                });

                request.fail(function(jqXHR, textStatus){
                    console.log(textStatus);
                    $('.bloqueo').fadeOut('slow');
                });
            }
        }

        function toggleContent(classInput, display){
            if(display){
                $('.'+classInput).fadeIn();
            }
            else{
                $('.'+classInput).fadeOut();
            }
        }

        function clickBtn(getElement, objectFields){
            if(objectFields.indexOf("CLAVEIMAGEN") == 0){
                let claveImagen = $(getElement).parent().siblings('td[data-category="CLAVEIMAGEN"]').text();
                let nombreImagen = $(getElement).parent().siblings('td[data-category="NOMBRE"]').text();
                let nombreImagenShort = $(getElement).parent().siblings('td[data-category="NOMBRE_CORTO"]').text();

                let contentForm = createForm("NOMBRE", "editName", nombreImagen);
                contentForm += createForm("NOMBRE CORTO", "editNameShort", nombreImagenShort, 'limitCaracter');
                $('#contentEdit').html(contentForm);
                $('#contentButtonEdit').html('<button type="button" class="btn btn-success mx-auto" onclick="editTypeImg('+claveImagen+');">GUARDAR</button>');
                $('#agregarTipoImagen').modal('show');
            }
            else if(objectFields.indexOf("CLAVEDOCUMENTO") == 0){
                let claveTipoDoc = $(getElement).parent().siblings('td[data-category="CLAVEDOCUMENTO"]').text();
                let nombreTipoDoc = $(getElement).parent().siblings('td[data-category="TIPODOCUMENTO"]').text();
                let nombreTipoDocShort = $(getElement).parent().siblings('td[data-category="NOMBRE_CORTO"]').text();

                let contentForm = createForm("TIPO DOCUMENTO", "editNameDoc", nombreTipoDoc);
                contentForm += createForm("NOMBRE CORTO", "editNameDocShort", nombreTipoDocShort, 'limitCaracter');

                $('#contentEdit').html(contentForm);
                $('#contentButtonEdit').html('<button type="button" class="btn btn-success mx-auto" onclick="editTypeDoc('+claveTipoDoc+');">GUARDAR</button>');
                $('#agregarTipoImagen').modal('show');
            }

            // .each(function(){
            //     console.log($(this).text());
            // });
            $('.limitCaracter').on('keyup',function(){
                let value = $(this).val();
                value = value.substring(0, 20).replace(/[^A-Z^a-z^0-9_-]/g, '');
                $(this).val(value)
            });
        }

        function createForm(field, idInputEdit, fieldValue, classLimit = ''){
            return '<div class="input-group mb-2">\
                        <div class="input-group-prepend modalLabels">\
                            <div class="input-group-text min-w-115">'+field+'</div>\
                        </div>\
                        <input type="text" class="form-control modalField '+classLimit+'" id="'+idInputEdit+'" placeholder="'+field+'" value="'+fieldValue+'">\
                    </div>';
        }


        let configCombos = {
            CATEGORIA : {
                tabName : 'CATEGORIA',
                keyVal : 'IDCATEGORIA',
                fieldVal : 'CATEGORIA',
                fieldAsign : 'contCategory',
                idSelect : 'selectCategorias',
                callBackFT : getTypeImag,
                subTab : 'TIPOIMAGEN'
            },
            TIPOIMAGEN : {
                tabName : 'TIPOIMAGEN',
                keyVal : 'IDTIPOIMAGEN',
                fieldVal : 'TIPOIMAGEN',
                fieldAsign : 'contTipoImg',
                idSelect : 'selectTipoImg',
                callBackFT : getTypeDocs,
                subTab : ''
            }
            // ,
            // TIPODOCUMENTO : {
            //     tabName : 'TIPODOCUMENTO',
            //     keyVal : 'IDTIPODOCUMENTO',
            //     fieldVal : 'TIPODOCUMENTO',
            //     fieldAsign : 'contTipoDoc',
            //     idSelect : 'selectTipoDoc',
            //     callBackFT : null,
            //     subTab : ''
            // }

        }


        $(function(){
            constComboPersonalized('CATEGORIA');
            getCategories();
        });

        $('.limitCaracter').on('keyup',function(){
            let value = $(this).val();
            value = value.substring(0, 20).replace(/[^A-Z^a-z^0-9_-]/g, '');
            $(this).val(value)
        });


        function constComboPersonalized(tabItem, searchKey = '', searchKeyVal = ''){

            let objectSend = {
                opcion : 'getInfoTable',
                key : configCombos[tabItem].keyVal,
                field : configCombos[tabItem].fieldVal,
                tabName : configCombos[tabItem].tabName,
                searchKey : searchKey,
                searchKeyVal : searchKeyVal
            }

            let request = petitionSupport(comboURL, objectSend);

            request.done(function(res){
                let response = JSON.parse(res);

                if(response != undefined && response != null && Object.keys(response).length > 0){
                    let optionsResult = '<select name="" class="div-control" id="'+configCombos[tabItem].idSelect+'">\
                                            <option value="" selected>Selecciona</option>';

                    response.forEach(function(item){
                        optionsResult += '<option value="'+item[objectSend.key]+'">'+item[objectSend.field]+'</option>';
                    });

                    optionsResult += '</select>';
                    $('#'+configCombos[tabItem].fieldAsign).html(optionsResult);

                    $('#'+configCombos[tabItem].idSelect).bind('change', function(){
                        let valueSelected = $('#'+configCombos[tabItem].idSelect).val();
                        if(configCombos[tabItem].subTab != '' && configCombos[tabItem].subTab != null && configCombos[tabItem].subTab != undefined && valueSelected !== ''){
                            console.log('entro');
                            constComboPersonalized(configCombos[tabItem].subTab, objectSend.key, valueSelected);
                            console.log('entro');
                        }

                        if(configCombos[tabItem].callBackFT != '' && configCombos[tabItem].callBackFT != null && configCombos[tabItem].callBackFT != undefined){
                            if(typeof configCombos[tabItem].callBackFT === 'function') configCombos[tabItem].callBackFT(response);
                        }
                    });
                }else{
                    alert('Error con el combo');
                }
            });

            request.fail(function(XHR, textStatus){
                console.log(textStatus);
            });
        }


        function petitionSupport(sendURL, objectData){
          return $.ajax({
                url : sendURL,
                method : 'POST',
                data : objectData,
                dataType : 'html'
            });
        }


        function supportRequest(objectElement, metodo, URL){
            return $.ajax({
                url: URL,
                method: metodo,
                data: objectElement,
                dataType: 'html'
            });
        }

        function getCategories(callBackResponse){
                // function getCategories(){
                let objectElement = { option: "categories" };
                let metodo = "POST";
                let request = supportRequest(objectElement, metodo, URL);
                $('.bloqueo').show();
                request.done(function(res){
                    let response = JSON.parse(res);
                    let objectHeaders = ['CATEGORIA'];
                    let objectFields = ['CATEGORIA'];
                    let newTable = construcTable(response, objectHeaders, objectFields, false, 'tabCategoria');
                    let options = construcOp(response, "CATEGORIA","CATEGORIA");
                    // $("#selectCategorias").html(options);
                    $("#selectAgNvCat").html(options);
                    $('#tableContentCat').html(newTable);
                    $('#tableContent').hide();

                    tablePadron('#tabCategoria', 'Categorias', 1);
                    $('.bloqueo').fadeOut('slow');
                });

                request.fail(function(jqXHR, textStatus){
                    console.log(textStatus);
                    $('.bloqueo').fadeOut('slow');
                });
            // }
        };


        function addCategory(){
            let nameCategory = $('#nameCategory').val();
            let namTypImgCat = $('#namTypImgCat').val();
            let namTypImgCatShort = $('#namTypImgCatShort').val();
            let namTypDoc = $('#namTypDoc').val();
            let namTypDocShort = $('#namTypDocShort').val();

            let objectDataSend = {
                option : 'newCategory',
                nameCategory : nameCategory,
                namTypImgCat : namTypImgCat,
                namTypImgCatShort : namTypImgCatShort,
                namTypDoc : namTypDoc,
                namTypDocShort : namTypDocShort
            }
            let metodo = 'POST';
            let request = supportRequest(objectDataSend, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res == true){
                    getCategories();
                    alert('Registro Insertado Con Exito');
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }

        function addTypeImg(){
            let nameCategory = $("#selectCategorias option:selected").text();
            let nameTypeImg = $('#nameTypeImg').val();
            let nameTypeImgShort = $('#nameTypeImgShort').val();
            let nameTypeDocImg = $('#nameTypeDocImg').val();
            let nameTypeDocShortImg = $('#nameTypeDocShortImg').val();

            let objectSendData = {
                option : 'newTypeImg',
                nameCategory : nameCategory,
                nameTypeImg : nameTypeImg,
                nameTypeImgShort : nameTypeImgShort,
                namTypDoc : nameTypeDocImg,
                namTypDocShort : nameTypeDocShortImg
            }
            let metodo = 'POST';

            let request = supportRequest(objectSendData, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res){
                    alert('Datos registrados correctamente');
                    getTypeImag(true);
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log('Error: ', textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }

        function addTypeDoc(){
            let keyTypeImg = $('#selectTipoImg').val();
            let nameTypeDoc = $('#nameTypeDoc').val();
            let nameTypeDocShort = $('#nameTypeDocShort').val();

            let objectSendData = {
                option: 'newTypeDoc',
                keyTypeImg: keyTypeImg,
                nameTypeDoc : nameTypeDoc,
                nameTypeDocShort : nameTypeDocShort
            }
            let metodo = 'POST';
            let request = supportRequest(objectSendData, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res){
                    alert('Registro insertado correctamente');
                    $('#datTipDocGroup').hide();
                    getTypeDocs(true);
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log('Error: ', textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }

        function editTypeImg(claveTypeImg){
            let nameTypeImg = $('#editName').val();
            let nameTypeImgShort = $('#editNameShort').val();

            let objectSendData = {
                option : 'editTypeImg',
                keyImg : claveTypeImg,
                nameTypeImg : nameTypeImg,
                nameTypeImgShort : nameTypeImgShort
            }
            let metodo = 'POST';

            let request = supportRequest(objectSendData, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res){
                    $('#agregarTipoImagen').modal('hide');
                    $('#datTipoImagGroup').hide();
                    alert('Actualización Correcta');
                    getTypeImag(true);
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }

        function editTypeDoc(claveTypeDoc){
            let nameTypeDoc = $('#editNameDoc').val();
            let nameTypeDocShort = $('#editNameDocShort').val();

            let objectSendData = {
                option : 'editTypeDoc',
                keyDoc : claveTypeDoc,
                nameTypeDoc : nameTypeDoc,
                nameTypeDocShort : nameTypeDocShort
            }
            let metodo = 'POST';
            let request = supportRequest(objectSendData, metodo, URL);
            $('.bloqueo').show();
            request.done(function(res){
                if(res){
                    $('#agregarTipoImagen').modal('hide');
                    $('#datTipDocGroup').hide();
                    alert('Actualización Correcta');
                    getTypeDocs(true);
                }
                $('.bloqueo').fadeOut('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log(textStatus);
                $('.bloqueo').fadeOut('slow');
            });
        }

        function construcOp(dataObject, valueElement, keyElement){
            let optionsResult = "<option value='' selected>Selecciona</option>";

            if(!$.isEmptyObject(dataObject)){
                dataObject.forEach(function(item){
                    optionsResult += "<option value='"+item[valueElement]+"'>"+item[keyElement]+"</option>";
                });
            }
            return optionsResult;
        }

        function construcTable(objectData, objectHeaders, objectFields, btnEdit = false, idTable = ''){
            let contentTable = '<table class="table table-bordeded" id="'+idTable+'">\
                                    <thead>\
                                        <tr>';
            objectHeaders.forEach(function(item){
                contentTable += '<th>'+item+'</th>';
            });

            contentTable += '</tr>\
                            </thead>\
                            <tbody>';

            objectData.forEach(function(item){
                contentTable += '<tr>';
                objectFields.forEach(function(subItem){
                    if(item[subItem] == null)item[subItem] = '';
                    contentTable += '<td data-category='+subItem+'>'+item[subItem]+'</td>';
                });
                if(btnEdit)contentTable += '<td><button class="btn btn-primary btn-md" onclick="clickBtn($(this), \''+objectFields+'\')">\
                                            <i class="fa fa-pencil-square-o"></i>Editar</button></td>';
                contentTable += '</tr>';
            });

            contentTable += '</tbody>\
                        </table>';

            return contentTable;
        }

        function toggleDisableGroup(classInput, displayGroup){
            let classGroup = '';
            if(classInput == 'datCat') classGroup = 'groupCat-1';
            if(classInput == 'datTipImg') classGroup = 'groupCat-2';
            if(classInput == 'datTipDoc') classGroup = 'groupCat-3';
            toggleContent(classInput, displayGroup);

            if(displayGroup){
                $('.'+classGroup+"-btnAdd").fadeOut();
                $('.'+classGroup+"-btnCancel").first().fadeIn(function showNext() {
                    $('.'+classGroup+"-btnSave").fadeIn();
                });
            }
            else{

                $('.'+classGroup+"-btnAdd").fadeIn();
                $('.'+classGroup+"-btnCancel").first().fadeOut(function showNext() {
                    $('.'+classGroup+"-btnSave").fadeOut();
                });
            }
        }

    </script>
</html>
