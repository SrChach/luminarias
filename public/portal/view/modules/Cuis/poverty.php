<style>
	.boxGraph{
        height: 400px;
        margin-top: 20px;
        padding-bottom: 100px;
        font-family: 'NeoSansPro-Bold';
    }
    .boxTable{
        -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
        box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75);
    }
</style>
<div class="container-fluid">
	<div class="row" id="contentBody">
		<?php 
			include_once './view/modules/Cuis/sections/sec_DelegacionesCarencias.php';
			include_once './view/modules/Cuis/sections/sec_SubdelegacionesCarencias.php';
			include_once './view/modules/Cuis/sections/sec_MunicipiosCarencias.php';
			include_once './view/modules/Cuis/sections/sec_LocalidadesCarencias.php';
		?>
	</div>
</div>



<script type="text/javascript">
    var url='./controller/CarenciasController.php';
    var ykeys=[
                'Carencia_en_la_Calidad_y_Espacios_de_la_Vivienda', 
                'Carencia_Acceso_a_los_Servicios_básicos_en_la_Vivienda', 
                'Carencia_por_Rezago_educativo', 
                'Carencia_por_Acceso_a_la_salud', 
                'Carencia_por_Acceso_a_la_seguridad_social', 
                'Carencia_por_Acceso_a_la_alimentación'
            ];
    var labels=[
                'Carencia_en_la_Calidad_y_Espacios_de_la_Vivienda', 
                'Carencia_Acceso_a_los_Servicios_básicos_en_la_Vivienda', 
                'Carencia_por_Rezago_educativo', 
                'Carencia_por_Acceso_a_la_salud', 
                'Carencia_por_Acceso_a_la_seguridad_social', 
                'Carencia_por_Acceso_a_la_alimentación'
            ];
    $(function () {
        Index();
    })

    function Index() {
        load();
        $('#content-Delegacion').fadeOut(1000);
        objet={opcion: 'Carencias',csrf_token:'sdbjh',action:'getCarenciasDelegacion',org:'general'};
        $.post(url, objet , function(response, textStatus, xhr) {
            try{
                res=$.parseJSON(response);
                if (res.CODIGO==true) {
                    var arreglado = res.DATOS.map( item => { 
                        return { ID_DEL: item.ID_DEL , DELEGACIÓN:item.DELEGACIÓN,FAMILIAS:item.FAMILIAS,
                                Carencia_en_la_Calidad_y_Espacios_de_la_Vivienda:item.IC_CV1,
                                Carencia_Acceso_a_los_Servicios_básicos_en_la_Vivienda:item.IC_SBV1,
                                Carencia_por_Rezago_educativo:item.IC_REZEDU1,
                                Carencia_por_Acceso_a_la_salud:item.IC_ASALUD1,
                                Carencia_por_Acceso_a_la_seguridad_social:item.IC_SS1,
                                Carencia_por_Acceso_a_la_alimentación:item.IC_ALI1
                                }; 
                    });
                    $('#content-Delegacion').fadeIn(2000);
                    graphCarenciasDelegacion(arreglado,'DELEGACIÓN',ykeys,true);
                    renderTable('#tableAvances-Delegacion',arreglado,'',1,1,'','subdelegacion',{org:'carencias',click:'levelSubdelegacion'});
                    tableExport2('#table-dinamic','Carencias Por Delegación');
                    settingsTable();
                    $('#btnBack').tooltip({title: 'RECARGAR'});
                }else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                }
            }catch(err){
                showError(err);
            }
            load(false);
        });
    }

    function graphCarenciasDelegacion(data,xkey,ykey,stack,labels='') {
        $('#graficaAvance-Delegacion').html('');
        if (labels=='') {
            labels=ykey;
        }
        new Morris.Bar({
            element: 'graficaAvance-Delegacion',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>DELEGACIÓN: "+aux[1];
                content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function levelSubdelegacion(estado,id,title) {
        load();
        $('#content-Delegacion').fadeOut(1000);
        $('#titleGraficaSubdelegacion').html('DELEGACIÓN: '+title);
        objet={opcion: 'Carencias',csrf_token:'sdbjh',action:'getCarenciasSubdelegacion',id_del:id,org:'general'};
        $.post(url, objet , function(response, textStatus, xhr) {
            try{
                res=$.parseJSON(response);
                if (res.CODIGO==true) {
                    var arreglado = res.DATOS.map( item => { 
                        return { ID_SUB: item.ID_SUB , SUBDELEGACIÓN:item.SUBDELEGACIÓN,
                                FAMILIAS:item.FAMILIAS,
                                Carencia_en_la_Calidad_y_Espacios_de_la_Vivienda:item.IC_CV1,
                                Carencia_Acceso_a_los_Servicios_básicos_en_la_Vivienda:item.IC_SBV1,
                                Carencia_por_Rezago_educativo:item.IC_REZEDU1,
                                Carencia_por_Acceso_a_la_salud:item.IC_ASALUD1,
                                Carencia_por_Acceso_a_la_seguridad_social:item.IC_SS1,
                                Carencia_por_Acceso_a_la_alimentación:item.IC_ALI1
                                }; 
                    });
                    $('#content-Subdelegacion').fadeIn(2000);
                    graphCarenciasSubdelegacion(arreglado,'SUBDELEGACIÓN',ykeys,true);
                    //function renderTable(element,data,idP,jump='',exeption='',idtable='',onclick='',org='',modalidad='')
                    renderTable('#tableAvancesSubDelegacion',arreglado,'',1,1,'table-dinamic2','municipio',{org:'carencias',click:'levelMunicipio'});
                    tableExport2('#table-dinamic2','Carencias Por Subdelegación');
                    settingsTable();
                    $('#btnBackDelegacion').tooltip({title: '<- REGRESAR A DELEGACIONES'});
                }else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                }
            }catch(err){
                showError(err);
            }
            load(false);
        });
    }

    function graphCarenciasSubdelegacion(data,xkey,ykey,stack,labels='') {
        $('#graficaAvanceSubDelegacion').html('');
        if (labels=='') {
            labels=ykey;
        }
        new Morris.Bar({
            element: 'graficaAvanceSubDelegacion',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>SUBDELEGACIÓN: "+aux[1];
                content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function levelMunicipio(estado,id,title) {
        load();
        $('#content-Subdelegacion').fadeOut(1000);
        $('#titleGraficaMunicipio').html('SUBDELEGACIÓN: '+title);
        objet={opcion: 'Carencias',csrf_token:'sdbjh',action:'getCarenciasMunicipio',id_sub:id,org:'general'};
        $.post(url, objet , function(response, textStatus, xhr) {
            try{
                res=$.parseJSON(response);
                if (res.CODIGO==true) {
                    var arreglado = res.DATOS.map( item => { 
                        return { CVE_MUN: item.CVE_MUN , MUNICIPIO:item.MUNICIPIO,
                                FAMILIAS:item.FAMILIAS,
                                Carencia_en_la_Calidad_y_Espacios_de_la_Vivienda:item.IC_CV1,
                                Carencia_Acceso_a_los_Servicios_básicos_en_la_Vivienda:item.IC_SBV1,
                                Carencia_por_Rezago_educativo:item.IC_REZEDU1,
                                Carencia_por_Acceso_a_la_salud:item.IC_ASALUD1,
                                Carencia_por_Acceso_a_la_seguridad_social:item.IC_SS1,
                                Carencia_por_Acceso_a_la_alimentación:item.IC_ALI1
                                }; 
                    });
                    $('#content-Municipio').fadeIn(2000);
                    graphCarenciasMunicipio(arreglado,'MUNICIPIO',ykeys,true);
                    //function renderTable(element,data,idP,jump='',exeption='',idtable='',onclick='',org='',modalidad='')
                    renderTable('#tableAvancesMunicipio',arreglado,'',1,1,'table-dinamic3','localidad',{org:'carencias',click:'levelLocalidad'});
                    tableExport2('#table-dinamic3','Carencias Por Municipio');
                    settingsTable();
                    $('#btnBackSubdelegacion').tooltip({title: '<- REGRESAR A SUBDELEGACIONES'});
                }else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                }
            }catch(err){
                showError(err);
            }
            load(false);
        });
    }

    function graphCarenciasMunicipio(data,xkey,ykey,stack,labels='') {
        $('#graficaAvanceMunicipio').html('');
        if (labels=='') {
            labels=ykey;
        }
        new Morris.Bar({
            element: 'graficaAvanceMunicipio',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>SUBDELEGACIÓN: "+aux[1];
                content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    function levelLocalidad(estado,id,title) {
        load();
        $('#content-Municipio').fadeOut(1000);
        $('#titleGraficaLocalidad').html('MUNICIPIO: '+title);
        objet={opcion: 'Carencias',csrf_token:'sdbjh',action:'getCarenciasLocalidad',id_mun:id,org:'general'};
        $.post(url, objet , function(response, textStatus, xhr) {
            try{
                res=$.parseJSON(response);
                if (res.CODIGO==true) {
                    var arreglado = res.DATOS.map( item => { 
                        return { ID_LOC: item.ID_LOC , LOCALIDAD:item.LOCALIDAD,
                                FAMILIAS:item.FAMILIAS,
                                Carencia_en_la_Calidad_y_Espacios_de_la_Vivienda:item.IC_CV1,
                                Carencia_Acceso_a_los_Servicios_básicos_en_la_Vivienda:item.IC_SBV1,
                                Carencia_por_Rezago_educativo:item.IC_REZEDU1,
                                Carencia_por_Acceso_a_la_salud:item.IC_ASALUD1,
                                Carencia_por_Acceso_a_la_seguridad_social:item.IC_SS1,
                                Carencia_por_Acceso_a_la_alimentación:item.IC_ALI1
                                }; 
                    });
                    $('#content-Localidad').fadeIn(2000);
                    graphCarenciasLocalidad(arreglado,'LOCALIDAD',ykeys,true);
                    //function renderTable(element,data,idP,jump='',exeption='',idtable='',onclick='',org='',modalidad='')
                    renderTable('#tableAvancesLocalidad',arreglado,'',1,1,'table-dinamic4');
                    tableExport2('#table-dinamic4','Carencias Por Localidad');
                    settingsTable();
                    $('#btnBackMunicipio').tooltip({title: '<- REGRESAR A MUNICIPIOS'});
                }else{
                    showAlertInwindow('#contentBody','warning',res.DATOS);
                }
            }catch(err){
                showError(err);
            }
            load(false);
        });
    }

    function graphCarenciasLocalidad(data,xkey,ykey,stack,labels='') {
        $('#graficaAvanceLocalidad').html('');
        if (labels=='') {
            labels=ykey;
        }
        new Morris.Bar({
            element: 'graficaAvanceLocalidad',
            data: data,
            hoverCallback: function(index, options, content,row) {
                var aux= content.split("row-label'>");
                content = aux[0]+"row-label'>MUNICIPIO: "+aux[1];
                content+="<div class='morris-hover-point' style='color: red;'><a class='text-danger' href='./controller/ReportsController.php?csrf_token=_t&opcion=Reportes&tipo=credencialesDelegacion&params={\"id\":\"iddelega\"}'>REPORTE</a></div>";
                  $('.morris-hover').attr('style','font-family:"NeoSansPro-Regular"');
                  
                  return content;
              },
            xkey: xkey,
            ykeys: ykey,
            stacked: stack,
            resize:true,
            xLabelAngle:45,
            labels: labels,
        });
    }

    $('#btnBack').click(function () { 
        Index();
    });
    $('#btnBackDelegacion').click(function () { 
        $('#content-Subdelegacion').fadeOut(1000);
        $('#content-Delegacion').fadeIn(2000);
        $('#btnBackDelegacion').tooltip('hide');
    });
    $('#btnBackSubdelegacion').click(function () { 
        $('#content-Municipio').fadeOut(1000);
        $('#content-Subdelegacion').fadeIn(2000);
        $('#btnBackSubdelegacion').tooltip('hide');
    });
    $('#btnBackMunicipio').click(function () { 
        $('#content-Localidad').fadeOut(1000);
        $('#content-Municipio').fadeIn(2000);
        $('#btnBackMunicipio').tooltip('hide');
    });

</script>