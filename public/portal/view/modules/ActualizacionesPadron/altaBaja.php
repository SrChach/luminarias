<style>
    #btnDesPadronProg{
        display: none;
    }
    .inputFileStyle{
        background-color: #17a2b8;
        padding: .8rem;
        border-radius: 50%;
        width: 40px;
        height: 40px;
        display: inline-table;
        align-content: center;
        cursor: pointer;        
    }
    .inputFileStyle > i{
        color: white;
        font-size: x-large;
        margin: auto;
    }


    .inputFileStyle{
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.68);               
        -webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
        transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    }
    .inputFileStyle:after{        
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
        opacity: 0;
        -webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
        transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    }

    .inputFileStyle:hover {
        -webkit-transform: scale(1.25, 1.25);
        transform: scale(1.18, 1.18);
    }

    .inputFileStyle:hover::after {
        opacity: 1;
    }

    #nameFile{
        display: inline-grid;
        align-content: center;
        margin-bottom: .5rem;
        margin-left: 15px;
    }

    .loadingStandar{
        display : none;
        font-size: large;
        margin-right: .4rem;
    }

    .tableEvt > thead > tr > th{
        padding: .3rem 1rem;
    }
    .tableEvt > tbody > tr > td{
        padding: .3rem 1rem;
    }


    .contentLabel{
        min-width : 130px;
    }
    .contentLabel > label{
        min-width : 130px;
    }


    .loaderTableStandar{
        margin: auto;
        vertical-align: middle;
        padding: 50px 0;            
        font-size: 6rem;
        color: #007bff;
        /*color: #28A6DE;  Color estandar para el loader */
        display: none;
        top: 25%;
    }

    .dataTables_scrollHeadInner{
        width : 100% !important;
    }

    .pointer:hover{
        cursor: pointer;
    }

    @media (max-width: 310px){
        .contentLabel{
            min-width : 100%;
        }
        .contentLabel > label{
            min-width : 100%;
            border-radius: .25rem .25rem 0 0 !important;
        }
        .secondLabel{
            border-radius: 0 0 .25rem .25rem !important;
        }           
    }
    @media (min-width : 768px) and (max-width: 1215px){
        .contentLabel{
            min-width : 100%;
        }
        .contentLabel > label{
            min-width : 100%;
            border-radius: .25rem .25rem 0 0 !important;
        }
        .secondLabel{
            border-radius: 0 0 .25rem .25rem !important;
        }
    }
</style>

<style>
    .spinLayout{
        color: white;
        vertical-align: middle;
        font-size: x-large;
        margin-right: 11px;
    }

    .snackbar {
        /* visibility: hidden; */
        display : none;
        min-width: 250px;
        margin-left: -125px;
        background-color: #2bb273;
        color: #fff;
        text-align: center;
        border-radius: 2px;
        padding: 10px;
        position: absolute;
        z-index: 1;
        right: 20px;
        top: 15px;
        font-size: 12px;
    }

    .animated {
        animation-duration: 1s;
        animation-fill-mode: both;
    }

    .bounceInRight {
        -webkit-animation-name: bounceInRight;
        animation-name: bounceInRight;
    }

    @keyframes bounceInRight {
    
        60%, 75%, 90%, 0%, 100%{
            animation-timing-function: cubic-bezier(.215,.61,.355,1);
        }
        0% {
            opacity: 0;
            transform: translate3d(800px,0,0);
        }
        60% {
            opacity: 1;
            transform: translate3d(-25px,0,0);
        }
        75% {
            transform: translate3d(10px,0,0);
        }
        90% {
            transform: translate3d(-5px,0,0);
        }
        100% {
            transform: none;
            }
    }
</style>

<div class="container-fluid">
    <section>
        <div class="row">
            <div class="col-md-4 col-lg-3 text-center">
                <a href="./Storage/Layout.csv" download style="color: #2f2529;">
                    <i class="fa fa-file-excel-o" aria-hidden="true" style="font-size: 4rem;color: #007bff;box-shadow: 9px -3px 0px 0px;border-radius: .2rem;"></i>
                    <label for="" class="form-text pointer">Descargar Layout Altas</label>
                </a>
            </div>
            <div class="col-sm-4 col-md-8 col-lg-4">
                <select name="" id="listProg" class="form-control" onchange="getOptions();">
                    <option value="">Programas...</option>
                </select>

                <div class="input-group-prepend text-center my-5" style="display: block;">
                    <label class="inputFileStyle" for="imgLayout">
                        <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    </label>                
                    <span id="nameFile">Cargar Layout</span>                    
                    <input class="form-control" type="file" name="imgLayout" id="imgLayout" style="display: none;" accept=".csv">
                    <!-- <button class="btn btn-small btn-primary" onclick="resetearInput();">Eliminar Layout</button> -->
                </div>
            </div>
            <div class="col-sm-8 col-md-12 col-lg-5">
                <div class="jumbotron p-2">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <label for="" class="input-group-text">ID Programa</label>
                        </div>
                        <label for="" class="form-control" id="idCvePrograma"></label>
                    </div>
                    
                    <div class="row" id="tipoEvtProg"></div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="col-md-3 row mb-3">
                <div class="col-md-12 col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend contentLabel">
                            <label class="input-group-text min-width" style="font-size: 12px;" for="delegacion">
                                <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="formatImgLoading" id='loadingDelg'>Delegación -->
                                <i class="fa fa-spinner fa-spin loadingStandar" id='loadingDelg'></i>Oficina Regional
                            </label>
                        </div>
                        <select class="custom-select secondLabel" id="delegacion" onchange="construcSubDelg();">
                            <option value="">Selecciona...</option>
                        </select>                        
                    </div>
                </div>
                <div class="col-md-12 col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend contentLabel">
                            <label class="input-group-text min-width" style="font-size: 12px;" for="subDelegacion">
                                <i class="fa fa-spinner fa-spin loadingStandar" id='loadingSubDelg'></i>Oficina Subregional
                            </label>
                        </div>
                        <select class="custom-select secondLabel" id="subDelegacion">
                            <option value='' selected>Selecciona...</option>                            
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend contentLabel">
                            <label class="input-group-text min-width" style="font-size: 12px;" for="">
                                <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="formatImgLoading" id='loadingEst'>Estado -->
                                <i class="fa fa-spinner fa-spin loadingStandar" id='loadingEst'></i>Estado
                            </label>
                        </div>
                        <select class="custom-select secondLabel" id="estados" onchange="construcMcp();">
                            <option value='' selected>Selecciona...</option>                            
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend contentLabel">
                            <label class="input-group-text min-width" style="font-size: 12px;" for="">
                                <i class="fa fa-spinner fa-spin loadingStandar" id='loadingMun'></i>Municipio
                            </label>
                        </div>
                        <select class="custom-select secondLabel" id="municipios" onchange="construcLoc();">
                            <option value='' selected>Selecciona...</option>                            
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend contentLabel">
                            <label class="input-group-text min-width" style="font-size: 12px;" for="">
                                <i class="fa fa-spinner fa-spin loadingStandar" id='loadingLoc'></i>Localidad
                            </label>
                        </div>
                        <select class="custom-select secondLabel" id="localidades">
                            <option value='' selected>Selecciona...</option>                            
                        </select>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend contentLabel">
                            <label class="input-group-text min-width" style="font-size: 12px;" for="">Folio CUIS: </label>
                        </div>                                                
                        <input type="text" class='form-control secondLabel' id="folioCuis" onchange="disableElements('folioCuis', 'nombre');">                        
                    </div>
                </div>
                <div class="col-md-12 col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend contentLabel">
                            <label class="input-group-text min-width" style="font-size: 12px;" for="">Nombre: </label>
                        </div>
                        <input type="text" class='form-control secondLabel' id="nombre" onchange="disableElements('nombre', 'folioCuis');">
                    </div>
                </div>                

                <div class="col-md-12">
                    <button class='btn btn-primary btn-block' onclick="getTablePadron();">Buscar</button>
                </div>
            </div>
            <div class="col-md-9 row">
                
                <div class="container-fluid">
                    <label for="" class='form-text'>Numero de Registros: <span id="numRegistros"</span></label>                    
                </div>
                <div class="col-lg-12 text-center mb-3">
                    <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="loaderTable"> -->
                    <div class="loaderTableStandar"><i class="fa fa-spinner fa-spin"></i></div>
                    <div class="col-md-12" id="resultados"></div>
                </div>

                <div class="col-12">                                        
                    <div class="row justify-content-end">
                        <div class="col-lg-4">
                            <button class="btn btn-primary btn-block" id="btnDesPadronProg" onclick="downloadTablePadron();">Descargar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="snackLayout" class="snackbar animated bounceInRight">
        <i class="fa fa-spinner fa-spin spinLayout" style=""></i>Procesando Layout...
    </div>
    
    <div id="snackReporte" class="snackbar animated bounceInRight">
        <i class="fa fa-spinner fa-spin spinLayout" style=""></i>
        <span>Generando Reporte...</span><br>
        <sub>(Recibira un correo electronico con su reporte)</sub>
    </div>
    
    
</div>


<script>
    var URL = '../../public/portal/controller/altaBajaPadron_controller.php';
    var resultsGeneral = new Object();
    $(function(){
        getPrograms();
        // getOptions();
    });

    function getPrograms(){
        let METODO = 'POST';
        let objectSend = {
            option : 'getPrograms'
        }

        let request = petitionSupport(URL, METODO, objectSend);

        request.done(function(res){
            let response = JSON.parse(res);

            if(response != undefined && Object.keys(response).length > 0){
                let newOpProg = "<option value='' selected>Programas...</option>";
                newOpProg += constructOptions(response, 'IDPROGRAMA', 'NOMBREPROGRAMA', optionSelected = 0);
                $('#listProg').html(newOpProg);
            }
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
        });
    }

    function getOptions(){
        let cvePrograma = $('#listProg').val();
        
        //Asigna la cve del programa al resumen
        $('#idCvePrograma').text(cvePrograma);
        
        if(cvePrograma != ''){            
            let METODO = 'POST';

            let objectSendProgram = {
                option : 'getProgTypeEvt',
                cvePrograma : cvePrograma
            }
            let requestProgram = petitionSupport(URL, METODO, objectSendProgram);
            
            requestProgram.done(function(res){
                let response = JSON.parse(res);

                if(response != undefined && Object.keys(response).length > 0){
                    let tablaEventosPrograma = '<table class="tableEvt table-striped w-100" style="font-size:11px;">\
                                                    <thead>\
                                                        <tr>\
                                                            <th scope="col">Clave</th>\
                                                            <th scope="col">Tipo Evento</th>\
                                                        </tr>\
                                                    </thead>\
                                                    <tbody>';
                    
                    response.forEach(function(item){
                        tablaEventosPrograma += '<tr>\
                                                    <td scope="row">'+item.IDTIPOEVENTO+'</td>\
                                                    <td>'+item.TIPOEVENTO+'</td>\
                                                </tr>';
                    });

                    tablaEventosPrograma += ' </tbody>\
                                            </table>';
                    $('#tipoEvtProg').html(tablaEventosPrograma);
                    
                }
            });

            requestProgram.fail(function(XHR, textStatus){
                console.log(textStatus);
            });

            let URLGeneralController = '../../public/portal/controller/GeneralesController.php';
            let objectData = { 
                opcion: 'generales',
                action: 'comboMain',
                idPrograma: cvePrograma,
                csrf_token : ''
            }

            $('#loadingDelg').show('slow');
            $('#loadingEst').show('slow');

            let request = petitionSupport(URLGeneralController, METODO, objectData);

            request.done(function(res){  
                let response = JSON.parse(res);

                if(response != undefined && Object.keys(response).length > 0){
                    if(response.DATOS.delegaciones != undefined && Object.keys(response.DATOS.delegaciones).length > 0){
                        let newOpDeleg = "<option value='' selected>Selecciona...</option>";
                        newOpDeleg += constructOptions(response.DATOS.delegaciones, 'ID_DEL', 'DELEGACION');
                        $('#delegacion').html(newOpDeleg);
                    }
                    if(response.DATOS.estados != undefined && Object.keys(response.DATOS.estados).length > 0){
                        let newOpEst = "<option value='' selected>Selecciona...</option>";
                        newOpEst += constructOptions(response.DATOS.estados, 'CVEEDO', 'NOMBREENT');
                        $('#estados').html(newOpEst);
                    }
                    $('#loadingDelg').hide('slow');
                    $('#loadingEst').hide('slow');
                }
            });

            request.fail(function(jqXHR, textStatus){
                console.log('Error: ', textStatus);
                $('#loadingDelg').hide('slow');
                $('#loadingEst').hide('slow');
            }); 
        }else{
            $('#delegacion').html("<option value='' selected>Selecciona...</option>");
            $('#estados').html("<option value='' selected>Selecciona...</option>");
            $('#tipoEvtProg').html('');
        }
    }

    function construcSubDelg(){
        let cveOficinaRegional = $('#delegacion').val();
        let cvePrograma = $('#listProg').val();
        
        let newOpSubDeleg = "<option value='' selected>Selecciona...</option>";

        if(cveOficinaRegional != '' && cvePrograma != ''){
            let METODO = 'POST';
            let URLGeneralController = '../../public/portal/controller/GeneralesController.php';
            let objectData = { 
                opcion: 'generales',
                action: 'comboMainSubitem',
                item: 'delegacion',
                idPrograma: cvePrograma,
                key : cveOficinaRegional,
                csrf_token : ''
            }
            
            $('#loadingSubDelg').show('slow');
            let request = petitionSupport(URLGeneralController, METODO, objectData);

            request.done(function(res){  
                let response = JSON.parse(res);
                if(response != undefined && Object.keys(response.DATOS).length > 0){
                    
                    newOpSubDeleg += constructOptions(response.DATOS, 'ID_SUB', 'SUBDELEGACION');
                    
                    changeSelectDefault("estados", true);
                    changeSelectDefault("municipios", true);
                    changeSelectDefault("localidades", true);
                    changeInputDefault("folioCuis", true);
                    changeInputDefault("nombre", true);
                    $('#listProg').attr('disabled', true);            
                    $('#subDelegacion').html(newOpSubDeleg);
                }
                $('#loadingSubDelg').hide('slow');
            });

            request.fail(function(jqXHR, textStatus){
                console.log('Error: ', textStatus);
                $('#loadingSubDelg').hide('slow');
            }); 
        }else{
            $('#subDelegacion').html(newOpSubDeleg);
            changeSelectDefault("estados", false);
            changeSelectDefault("municipios", false);
            changeSelectDefault("localidades", false);
            changeInputDefault("folioCuis", false);
            changeInputDefault("nombre", false);
            $('#listProg').attr('disabled', false);
        }
    }

    function construcMcp(){            
        let cveEstado = $('#estados option:selected').val();            
        let cvePrograma = $('#listProg').val();

        let newOpMcp = "<option value='' selected>Selecciona...</option>";
        if(cveEstado != '' && cvePrograma != ''){
            let METODO = 'POST';
            let URLGeneralController = '../../public/portal/controller/GeneralesController.php';
            let objectData = { 
                opcion: 'generales',
                action: 'comboMainSubitem',
                item: 'estado',
                idPrograma: cvePrograma,
                key : cveEstado,
                csrf_token : ''
            }

            $('#loadingMun').show('slow');
            let request = petitionSupport(URLGeneralController, METODO, objectData);

            request.done(function(res){  
                let response = JSON.parse(res);
                if(response != undefined && Object.keys(response.DATOS).length > 0){
                    newOpMcp += constructOptions(response.DATOS, 'CVEMUN', 'NOMBREMUN');

                    changeSelectDefault("delegacion", true);
                    changeSelectDefault("subDelegacion", true);            
                    changeInputDefault("folioCuis", true);
                    changeInputDefault("nombre", true);
                    $('#listProg').attr('disabled', true);
                    $('#municipios').html(newOpMcp);
                }
                $('#loadingMun').hide('slow');  
            });

            request.fail(function(jqXHR, textStatus){
                console.log('Error: ', textStatus);
                $('#loadingMun').hide('slow'); 
            }); 

        }
        else{
            $('#municipios').html(newOpMcp);
            construcLoc(); 
            changeSelectDefault("delegacion", false);
            changeSelectDefault("subDelegacion", false);            
            changeInputDefault("folioCuis", false);
            changeInputDefault("nombre", false);
            $('#listProg').attr('disabled', false);
        }
                               
    }

    function construcLoc(){            
        let cveMunicipio = $('#municipios option:selected').val();
        let cvePrograma = $('#listProg').val();

        let newOpLoc = "<option value='' selected>Selecciona...</option>";
        if(cveMunicipio != '' && cvePrograma != ''){
            let METODO = 'POST';
            let URLGeneralController = '../../public/portal/controller/GeneralesController.php';
            let objectData = { 
                opcion: 'generales',
                action: 'comboMainSubitem',
                item: 'municipio',
                idPrograma: cvePrograma,
                key : cveMunicipio,
                csrf_token : ''
            }

            $('#loadingLoc').show('slow');
            let request = petitionSupport(URLGeneralController, METODO, objectData);

            request.done(function(res){  
                let response = JSON.parse(res);
                if(response != undefined && Object.keys(response.DATOS).length > 0){
                    newOpLoc += constructOptions(response.DATOS, 'CVELOC', 'NOMBRELOC');
                    $('#localidades').html(newOpLoc);
                }
                $('#loadingLoc').hide('slow');  
            });

            request.fail(function(jqXHR, textStatus){
                console.log('Error: ', textStatus);
                $('#loadingLoc').hide('slow');
            }); 

        }
        else{
            $('#localidades').html(newOpLoc);            
        }

    }

    function getTablePadron() {
        let delegacion = $('#delegacion option:selected').val();
        let subDelegacion = $('#subDelegacion option:selected').val();
        let estado = $('#estados option:selected').val();
        let municipio = $('#municipios option:selected').val();
        let localidad = $('#localidades option:selected').val();
        let folioCuis = $('#folioCuis').val();
        let nombre = $('#nombre').val();
        let cvePrograma = $('#listProg').val();                        
        
        if(cvePrograma != ''){
            let METODO = 'POST';      
            let objectSend = { 
                option: 'getPadron', 
                delegacion: delegacion, 
                subdelegacion: subDelegacion, 
                estado: estado, 
                municipio: municipio, 
                localidad: localidad, 
                folio_cuis: folioCuis, 
                nombre: nombre, 
                idPrograma: cvePrograma
            };

            let request = petitionSupport(URL, METODO, objectSend);                    
            
            $('#resultados').hide('slow');
            $('#btnDesPadronProg').hide('slow');
            $('.loaderTableStandar').show('slow');
            
            // $('.tooltip').hide();
            request.done(function(res){                                
                let response = JSON.parse(res);

                if(response != undefined && Object.keys(response).length > 0){
                    if(response.total[0].TOTAL != undefined){
                        $('#numRegistros').html(response.total[0].TOTAL);
                    }

                    if(response.registrosPadron != undefined){
                        let tablaRes = '<table class="table table-striped w-100" id="tbPadronProgramas">\
                                                <thead class="">\
                                                    <tr style="font-size:11px;width: 100%;">\
                                                        <th scope="col">CVE PADRON</th>\
                                                        <th scope="col">FOLIO CUIS</th>\
                                                        <th scope="col">BENEFICIARIO</th>\
                                                        <th scope="col">NOMBREPROGRAMA</th>\
                                                        <th scope="col">TIPOEVENTO</th>\
                                                        <th scope="col">OFICINA REGIONAL</th>\
                                                        <th scope="col">OFICINA SUBREGIONAL</th>\
                                                        <th scope="col">ESTADO</th>\
                                                        <th scope="col">MUNICIPIO</th>\
                                                        <th scope="col">LOCALIDAD</th>\
                                                        <th scope="col"></th>\
                                                    </tr>\
                                                </thead>\
                                                <tbody>';

                        response.registrosPadron.forEach(function(item, i){
                            tablaRes += '<tr style="font-size:11px;">\
                                            <th>'+item.IDPADRON+'</th>\
                                            <td><a href="#!"><span class="click" data-toggle="tooltip" data-html="true" data-placement="right" title="<a href=\'?view=modules&act=consulta_cuis&index=1&folio='+item.FOLIOCUIS+'\' target=\'_blank\'>CONSULTA FOLIOCUIS</a> <br> <a href=\'?view=validacion&foliocuis='+item.FOLIOCUIS+'\' target=\'_blank\'>CONSULTA EXPEDIENTE</a>" >'+item.FOLIOCUIS+'</span></a></td>\
                                            <td scope="row">'+item.BENEFICIARIO+'</td>\
                                            <td scope="row">'+item.NOMBREPROGRAMA+'</td>\
                                            <td scope="row">'+item.TIPOEVENTO+'</td>\
                                            <td scope="row">'+item.DELEGACION+'</td>\
                                            <td scope="row">'+item.SUBDELEGACION+'</td>\
                                            <td scope="row">'+item.NOMBREENT+'</td>\
                                            <td scope="row">'+item.NOMBREMUN+'</td>\
                                            <td scope="row">'+item.NOMBRELOC+'</td>\
                                            <td><a href="?view=cedula&indexs='+item.IDLISTADO+","+item.IDPROGRAMA+'" target="_blank"><i class="fa fa-share" style="font-size:25px;"></i></a></td>\
                                        </tr>';
                        });

                        tablaRes += '</tbody>\
                                    </table>';
                        
                        $('#resultados').html(tablaRes);
                        $('.click').tooltip({trigger: "click"}); 
                        $('[data-toggle="tooltip"]').tooltip();
                        tablePadron('#tbPadronProgramas', 'PADRON', 11);
                    }


                }
                $('.loaderTableStandar').hide('slow');
                $('#btnDesPadronProg').show('slow');
                $('#resultados').show('slow');
                          
            });

            request.fail(function(jqXHR, textStatus){
                $('.loaderTableStandar').hide('slow');
                $('#btnDesPadronProg').show('slow');
                $('#resultados').show('slow');
                console.log('Error: ', textStatus);
            });
        }else{
            alert('Selecciona un programa');
        }
    }

    function downloadTablePadron(){
        let delegacion = $('#delegacion option:selected').val();
        let subDelegacion = $('#subDelegacion option:selected').val();
        let estado = $('#estados option:selected').val();
        let municipio = $('#municipios option:selected').val();
        let localidad = $('#localidades option:selected').val();
        let folioCuis = $('#folioCuis').val();
        let nombre = $('#nombre').val();
        let cvePrograma = $('#listProg').val();                        
        
        if(cvePrograma != ''){
            let METODO = 'POST';      
            let objectSend = { 
                option: 'downloadPadron', 
                delegacion: delegacion, 
                subdelegacion: subDelegacion, 
                estado: estado, 
                municipio: municipio, 
                localidad: localidad, 
                folio_cuis: folioCuis, 
                nombre: nombre, 
                idPrograma: cvePrograma
            };

            $('#snackReporte').show();
            setTimeout(function(){ $('#snackReporte').fadeOut('slow'); }, 3000);

            let request = petitionSupport(URL, METODO, objectSend);                    
            
            // $('#resultados').hide('slow');
            // $('.loaderTableStandar').show('slow');
            
            request.done(function(res){                                
                let response = JSON.parse(res);                             
                alert('Reporte Generado');        
            });

            request.fail(function(jqXHR, textStatus){                
                console.log('Error: ', textStatus);
            });

        }else{
            alert('Selecciona un programa');
        }
    }
        
    function changeSelectDefault(idSelect, option){
        document.getElementById(idSelect).options.selectedIndex = 0;
        $('#'+idSelect).attr('disabled', option);
    }
    function changeInputDefault(idInputText, option){
        $('#'+idInputText).attr('disabled', option);
        $('#'+idInputText).val('');
    }

    function disableElements(idInput, idInputSecond){            
        var disableOptions = false;
        
        if($('#'+idInput).val() != "" && $('#'+idInput).val() != null){
            disableOptions = true;
        }else{
            if($('#'+idInputSecond).val() != "" && $('#'+idInputSecond).val() != null){
                disableOptions = true;
            }
        }

        changeSelectDefault("delegacion", disableOptions);
        changeSelectDefault("subDelegacion", disableOptions);
        changeSelectDefault("estados", disableOptions);
        changeSelectDefault("municipios", disableOptions);
        changeSelectDefault("localidades", disableOptions);
        $('#listProg').attr('disabled', disableOptions);            
    }  

    function constructOptions(objectData, value, textValue, optionSelected = 0){
        var options = "";
        objectData.forEach(function(item){
            let select = (item == optionSelected[value]) ? 'selected' : "";                
            options += "<option value='"+item[value]+"' "+select+">"+item[textValue]+"</option>" 
        });

        return options;
    }

    function petitionSupport(URL, METODO, objectData){
        return $.ajax({
            url : URL, 
            method : METODO,
            data : objectData
        });
    }

    function petitionSupportFile(URL, METODO, dataForm){
        return $.ajax({
            url : URL,
            type : METODO,
            data : dataForm,
            processData : false,
            contentType :  false,
            cache : false
        });
    }

    $("#imgLayout[type=file]").on("change", function(){        
        let fileName = this.files[0].name;
        $('#nameFile').text(fileName);

        let METODO = 'POST';
        // var inputFileImage = document.getElementById("imgLayout");
        // var file = inputFileImage.files[0];            
        let file = this.files[0];            
        let dataFormSend = new FormData();        

        dataFormSend.append('actsPadron',file); 
        dataFormSend.append('option','updatePadron');
        
        $('#snackLayout').show();
        setTimeout(function(){ $('#snackLayout').fadeOut('slow'); }, 3000);       

        let request = petitionSupportFile(URL, METODO, dataFormSend);

        request.done(function(res){
            if(res == true){
                alert('EL PROCESAMIENTO DEL LAYOUT SE HA COMPLETADO');
                resetearInput();
            }
            else{
                // let response = JSON.parse(res);
                
                
                // if(response['Error_de_Accion'].length > 0 || response['Error_de_CPersona_Existe'].length > 0 || response['Error_de_CPersona_NoExist'].length > 0 || response['Error_de_Datos'].length > 0){
                
                //     var blob = 'Fila, Descripción Error, C_PERSONA\n';

                //     if(response['Error_de_Accion'].length > 0){
                //         response['Error_de_Accion'].forEach(function(item){
                //             blob += item['fila'] + ',Error de Acción' +'\n';
                //         });
                //     }

                //     if(response['Error_de_CPersona_Existe'].length > 0){
                //         response['Error_de_CPersona_Existe'].forEach(function(item){
                //             blob += item['fila'] + ',Error el C_PERSONA ya existe en el programa y esta activo' + ',' + item['C_PERSONA'] + '\n';
                //         });
                //     }
                    
                //     if(response['Error_de_CPersona_NoExist'].length > 0){
                //         response['Error_de_CPersona_NoExist'].forEach(function(item){
                //             blob += item['fila'] + ',Error el C_PERSONA no existe' + ',' + item['C_PERSONA'] + '\n';
                //         });
                //     }
                    
                //     if(response['Error_de_Datos'].length > 0){
                //         response['Error_de_Datos'].forEach(function(item){
                //             blob += item['fila'] + ',Error de Datos' +'\n';
                //         });
                //     }


                //     let d = new Date();
                //     blob =  new Blob(["\ufeff", blob], {type: 'text/csv'});
                    

                //     var reader = new FileReader();
                //     reader.onload = function (event) {
                                        
                //         //Escuchamos su evento load y creamos un enlace en dom
                //         save = document.createElement('a');
                //         save.href = event.target.result;
                //         save.target = '_blank';

                        
                //         save.download = "LoteErrores_"+ d.getFullYear() + (d.getMonth()+1) + d.getDate() + d.getHours() + d.getMinutes() + d.getSeconds() + ".csv";

                //         try {
                //             //creamos un evento click
                //             clicEvent = new MouseEvent('click', {
                //                 'view': window,
                //                 'bubbles': true,
                //                 'cancelable': true
                //             });
                //         } catch (e) {
                //             //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
                //             clicEvent = document.createEvent("MouseEvent");
                //             clicEvent.initEvent('click', true, true);
                //         }
                        
                //         save.dispatchEvent(clicEvent);
                        
                //         // (window.URL || window.webkitURL).revokeObjectURL(save.href);
                //     }
                    
                //     //Leemos como url
                //     reader.readAsDataURL(blob);
                // }else{
                    alert('PROCESAMIENTO DEL LAYOUT COMPLETO');
                // }

            }
            
            

        });

    });

    function resetearInput() {        
        var $fileupload = $('#imgLayout');
        $fileupload.wrap('<form>').closest('form').get(0).reset();
        $fileupload.unwrap();
        
        $('#nameFile').text('Cargar Layout');
    }



</script>