<style>
	* {
		cursor: default;
	}
	.bloqueo{
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;       
        overflow: hidden;
        outline: 0;        
        position: fixed;
        background-color: rgba(154, 161, 164, 0.2);
        z-index: 10;
        display : none;
    }   
    /* .loaderStandar > i{
        color: #2bb273;
    } */
    .loaderStandar{
        margin: auto;
        vertical-align: middle;        
        font-size: 6rem;
        /* color: #2bb273; */
        color: #28A6DE;
        position: absolute;
        top: 45%;
        width : 100%;
        text-align : center;       
    }

</style>

<div class="bloqueo text-center">
    <div class="loaderStandar text-center">        
        <h4 class="text-center bold" style="color: #2bb273;">Cargando ...</h4>
        <i class="fa fa-spinner fa-spin" style="color: #28A6DE;"></i>
    </div>
</div>

<section class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<h3>Consulta de Cambios en Beneficiarios</h3>
		</div>
		<div class="col-lg-12 mt-3">
			<div class="row">
				
				<div class="col-lg-3">
					<label for="" class="form-text">Folio Cuis</label>
					<input type="text" class="form-control" placeholder="Folio CUIS" id="folio">
				</div>
				<div class="col-lg-3">
					<label for="" class="form-text">Desde</label>
					<input type="date" class="form-control" placeholder="Desde" id="fechaInicioCuis">
				</div>
				<div class="col-lg-3">
					<label for="" class="form-text">Hasta</label>
					<input type="date" class="form-control" placeholder="Hasta" id="fechaFinCuis">
				</div>
				<div class="col-lg-3">
					<label for="" class="form-text">Tipo de Cambio</label>
					<select name="" id="tipoCambioCuis" class="form-control">
						<option value="">Selecciona...</option>						
					</select>
				</div>

			</div>
		</div>
	</div>
	<div class="row my-2 justify-content-center">
		<div class="col-lg-2">
			<button class="btn btn-block btn-primary" onclick="buscarCambiosCuis();">Buscar</button>
		</div>
	</div>

	<div class="row mt-5">
		<div class="col-lg-12">		
			<div id="resultados"></div>
		</div>
	</div>

</section>

<script>
	var URL = './controller/cambiosCuis_controller.php';

	$(function(){
		getTipoCambiosCuis();
		getInfoCambiosCuis();
	});

	function getTipoCambiosCuis(){
		let metodo = 'POST';
		let objectSend = {
			option : 'getTipoCambiosCuis'
		}

		let request = supportRequest(metodo, objectSend, URL);
		$('.bloqueo').show();

		request.done(function(res){			
			if(res){
				let response = JSON.parse(res);
				console.log(response);
				$('#tipoCambioCuis').html(constructOptions(response, 'TIPOCAMBIO', 'TIPOCAMBIO'));				
			}
			$('.bloqueo').fadeOut('slow');
		});

		request.fail(function(jqXHR, textStatus){
			console.log(textStatus);
			$('.bloqueo').fadeOut('slow');
		});
	}

	function getInfoCambiosCuis(){
		let metodo = 'POST';
		let objectSend = {
			option : 'getCambiosCuis'
		}

		let request = supportRequest(metodo, objectSend, URL);
		$('.bloqueo').show();

		request.done(function(res){			
			if(res){
				let response = JSON.parse(res);
				createTableCambiosCuis(response);
				// response.forEach(function(item){
				// 	console.log(item);
				// });
			}
			$('.bloqueo').fadeOut('slow');
		});

		request.fail(function(jqXHR, textStatus){
			console.log(textStatus);
			$('.bloqueo').fadeOut('slow');
		});
	}

	function supportRequest(metodo, objectData, URL){
		return $.ajax({
			url : URL,
			method : metodo,
			dataType: 'html',
			data : objectData
		});
	}


	function constructOptions(objectData, value, textValue, optionSelected = 0){
        var options = "<option value=''>Selecciona...</option>";
		if(objectData && Object.keys(objectData).length > 0){
			objectData.forEach(function(item){
				let select = (item == optionSelected[value]) ? 'selected' : "";                
				options += "<option value='"+item[value]+"' "+select+">"+item[textValue]+"</option>" 
			});
		}

        return options;
    }


	function createTableCambiosCuis(objectInfo) {           
        var cadena = "<table class='table table-bordered table-condensed table-striped  table-hover'  id='tablaCambiosCuis'>\
                        <thead class='thead-light' style='font-size:12px;'>\
                            <tr>\
                                <th>#</th>\
                                <th>IDCAMBIOCUIS</th>\
                                <th>TIPOCAMBIO</th>\
                                <th>FECHA DE CAMBIO</th>\
                                <th>REALIZO CAMBIO</th>\
                                <th>TABLA</th>\
                                <th>IDENTIFICADOR</th>\
                                <th>CAMPO ACTUALIZADO</th>\
                                <th>VALOR ANTERIOR</th>\
                                <th>CAMBIO</th>\
                                <th>MOVIMIENTO</th>\
                                <th>FOLIO-CEDULA DE CAMBIO</th>\
                                <th>DOCUMENTO SOPORTE</th>\
                                <th>FOLIO CUIS</th>\
								<th></th>\
                            </tr>\
                        </thead>\
                        <tbody>";
                    
        var option='';
        $.each(objectInfo,function (i,item) {
                //if (item.IDPROGRAMA==1) {
            // option=hrefCedula();
            // }else{
            //     option="#!";
            // }			 
		
			if(item.URL != undefined && item.URL != null && item.URL != '')
				item.URL = "<a href='../../../soportecambios/"+item.URL+"' download><i class='fa fa-cloud-download' aria-hidden='true' style='color: #007bff;font-size: xx-large;cursor: pointer;'></i></a>";
			else
				item.URL = "<i class='fa fa-times' aria-hidden='true' style='color: #ff0000;font-size: xx-large;'></i>";
            cadena += "<tr style='font-size:11px;'>\
                        <th>"+(i+1)+"</th>\
                        <td><a href='#!'><span class='click' data-toggle='tooltip' data-html='true' data-placement='right' title='<a href=\""+'#'+"\" target=\"_blank\">CONSULTA FOLIOCUIS</a> <br> <a href=\""+'#'+"\" target=\"_blank\">CONSULTA EXPEDIENTE</a>' >"+item.IDCAMBIOCUIS+"</span></a></td>\
                        <td>"+item.TIPOCAMBIO+"</td>\
                        <td>"+item.FECHACAMBIO+"</td>\
                        <td>"+item.NOMBRE+"</td>\
                        <td>"+item.TABLA+"</td>\
                        <td>"+item.IDENTIFICADOR+"</td>\
                        <td>"+item.CAMPOACTUALIZADO+"</td>\
                        <td>"+item.VALORANTERIOR+"</td>\
                        <td>"+item.CAMBIO+"</td>\
                        <td>"+item.MOVIENTO+"</td>\
                        <td>"+item.FOLIOCEDULA+"</td>\
                        <td class='text-center'>\
							"+item.URL+"\
						</td>\
                        <td>"+item.FOLIO_CUIS+"</td>\
                        <td>\
                            <a href='"+option+"' target='_blank'>\
                                <i class='fa fa-share' style='font-size:25px;'></i>\
                            </a>\
                        </td>\
                    </tr>";
        });

        cadena +="</tbody>\
        			</table>";
					
        $('#resultados').html(cadena);
        tablePadron('#tablaCambiosCuis','CUIS',15)
        //tablePadron('#tbCuis','PAdron');
        settingsTable();
        
        // $('.click').tooltip({trigger: "click"}); 
        // $('[data-toggle="tooltip"]').tooltip();
    }


	function buscarCambiosCuis(){
		folioCuis = $('#folio').val();
		fechaInicio = $('#fechaInicioCuis').val();
		fechaFin = $('#fechaFinCuis').val();
		tipoCambio = $('#tipoCambioCuis').val();
		
		let metodo = 'POST';
		let objectSend = {
			option : 'getFilterCambioCuis',
			folio : folioCuis,
			fechaInicio : fechaInicio,
			fechaFin : fechaFin,
			tipoCambio : tipoCambio
		}

		let request = supportRequest(metodo, objectSend, URL);
		$('.bloqueo').show();
		request.done(function(res){
			if(res){
				let response = JSON.parse(res);
				createTableCambiosCuis(response);
				// response.forEach(function(item){
				// 	console.log(item);
				// });
			}
			$('.bloqueo').fadeOut('slow');
		});
		
		request.fail(function(XHR,textStatus){
			$('.bloqueo').fadeOut('slow');
		});
	}

</script>