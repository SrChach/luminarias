
    <!-- <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet"> -->
    
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css"> -->

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="./js/parametros.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/ajax.js"></script> -->
    
    
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
    <script src="morris/morris.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>
    <script src="morris/examples/lib/example.js"></script>
    <link rel="stylesheet" href="morris/examples/lib/example.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
    <link rel="stylesheet" href="morris/morris.css"> -->
  
  
  <style type="text/css">
     
        .containersMVV{
            border-radius: 0 0 45px 45px;
            box-shadow: 6px 6px 3px 6px #505160;
            height: 140px;
        }
       
        .info_box{
            text-align: center;
            padding-top: 1.3em;
            color: white;
            font-size: 18px;
        }
        .bg-green{
            background-color: #4CAF50;
        }        
        .bg-blue{
            background-color: #1d49fc;
        }
        .bg-red{
            background-color: #F44336;
        }        
        .bg-purple {
            background-color: rgba(129, 0, 224, 1);
        }
        
        .text-center{
            text-align: center;
        }        
        .verticalAlignText{
            vertical-align: middle;
            height: fit-content;
        }

        .padding-15{
            padding: 0px 15px;   
        }

        .margin-y-50{
            margin: 50px 0px;
        }
        .fontSize-10{
            font-size: 10px;
        }
        .padding-top-15{
            padding-top: 15px;
        }
        .font-XLarge{
            font-size: x-large;
        }
        .overflow-x-scroll{
            overflow-x: scroll;
        }
        .margin-bottom-15{
            margin-bottom: 15px;
        }

  </style>
  


<div class='container-fluid'>                    
        <section>
            <!-- <h1 style='margin: 35px 0px;text-align: center;'>Veracruz Comienza Contigo - Expedientes digitales</h1> -->
            <div class="row justify-content-center">
                <div class='col-md-11 row justify-content-center'>
                    <div class='col-lg-4 col-md-6 col-sm-6 margin-bottom-15 padding-15'>
                        <div class='containersMVV margin-15'>
                            <div class="bg-green info_box" style="width:100%;">
                                <i class="fa fa-newspaper-o fa-2x"></i>
                                <span class="letra_25 text-uppercase">Lotes</span>
                            </div>
                            <div class="info-box-content centra align-middle padding-top-15">                                
                                <h3 class="centra text-center"><span class="font-XLarge" id="meta"></span></h3>
                            </div>                            
                        </div>
                    </div>

                    <div class='col-lg-4 col-md-6 col-sm-6 margin-bottom-15 padding-15'>
                        <div class='containersMVV margin-15'>
                            <div class="bg-blue info_box" style="width:100%;">
                                <i class="fa fa-newspaper-o fa-2x"></i>
                                <span class="letra_25 text-uppercase">Documentos</span>
                            </div>
                            <div class="info-box-content centra align-middle padding-top-15">                                
                                <h3 class="centra text-center"><span class="font-XLarge" id="imagenes"></span></h3>
                            </div>                            
                        </div>
                    </div>                       

                    <div class='col-lg-4 col-md-6 col-sm-6 margin-bottom-15 padding-15'>
                        <div class='containersMVV margin-15'>
                            <div class="bg-red info_box" style="width:100%;">
                                <i class="fa fa-newspaper-o fa-2x"></i>
                                <span class="letra_25 text-uppercase">Folio cuis <span class='fontSize-10'>(Al menos 1 doc)</span></span>
                            </div>
                            <div class="info-box-content centra align-middle padding-top-15">                                
                                <h3 class="centra text-center"><span class="font-XLarge" id="folios"></span></h3>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="container-fluid margin-y-50 text-center" >
            <h4>Distribución por tipo de Imagen</h4>
        </div>  
        

        <div class="row mtop2" >
            <div class="col-md-7 overflow-x-scroll" id="datos"></div>
            <div class="col-md-5"  id="tipoDoc" style="height:440px"></div>
        </div>

        <div class="margin-y-50">
            <div class='row'>
                <div class="col">
                    Tipo Imagen: <label for="" id='hojaBlanNomb'></label>
                </div>
                <div class="col">
                    Documentos: <label for="" id='hojaBlanDoc'></label>
                </div>
                <div class="col">
                    Reversos: <label for="" id='hojaBlanRev'></label>
                </div>
                <div class="col">
                    Imágenes: <label for="" id='hojaBlanTot'></label>
                </div>
            </div>
        </div>

        <div class="row mtop2" >
            <div class="col-md-3"></div>
            <div class="col-md-9"></div>
        </div>
        

        
        <!-- <div class="row">
            <div class="col-md-9 centra"><h3>Tabla Ejecutiva</h3></div>
            <div class="col-md-2 centra">
                <h3>
                    <button class="btn btn-success" onclick="tabla_ejecutiva()">Descargar Tabla Ejecutiva</button>
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" id="tabla"></div>
        </div -->
    
</div>

<script type="text/javascript">
    // _____________________________________________________ URL'S
    // var urlHome = "http://75.126.28.84:9031/prod2/";
    //var urlimagenes = "http://75.126.73.173:9029/";
    // var urlimagenes = "http://75.126.28.84:9033/img/";




        
    
    var URL = 'http://75.126.28.84:9031/prod2/support/intro_controller.php';
    $(document).ready(function (){            

        var data = { opcion:"action_inicio" };
        var res = ajaxCallback(data,URL);
        var respuesta = JSON.parse(res);
        
        
        var generales = respuesta['GENERALES']; 
        $("#meta").html(number_format(generales['expedientes'],0));
        $("#tipificadas").html(generales['TIPIFICADAS']);
        $("#folios").html(number_format(generales['folios'],0));

        var tipoDoc = generales['tipo_Doc'];

        var numeros = "";
        
        data = [];
        var cadena = "";                        
        
        cadena += "<table class='table table-bordered'>\
                    <thead><tr>\
                    <b>\
                    <th>Tipo Imagen</th>\
                    <th>Documentos</th>\
                    <th>Reversos</th>\
                    <th>Imágenes</th>\
                    <b>\
                    <tr></thead><tbody>";
        var tot = 0;
        var totDocs = 0;
        var totReversos = 0;
        var tipificadas = 0;
        var contMorrisData = 0;
        for (var i = 0; i < tipoDoc.length; i++) {
            if(tipoDoc[i]['NOMBRE'] == 'Hoja Blanca'){                
                $('#hojaBlanNomb').html(tipoDoc[i]['NOMBRE']);
                $('#hojaBlanDoc').html(tipoDoc[i]['DOCUMENTOS']);
                $('#hojaBlanRev').html(tipoDoc[i]['REVERSOS']);
                $('#hojaBlanTot').html(tipoDoc[i]['TOTAL']);
            }else{
                data[contMorrisData] = [];
                data[contMorrisData]['label'] = tipoDoc[i]['NOMBRE'];
                data[contMorrisData]['value'] = tipoDoc[i]['DOCUMENTOS'];
                totDocs += parseInt(tipoDoc[i]['DOCUMENTOS']) ;
                totReversos += parseInt(tipoDoc[i]['REVERSOS']) ;
                tot += parseInt(tipoDoc[i]['TOTAL']) ;  
                cadena += "<tr>\
                                <td>"+tipoDoc[i]['NOMBRE']+"</td>\
                                <td>"+separa(tipoDoc[i]['DOCUMENTOS'])+"</td>\
                                <td>"+separa(tipoDoc[i]['REVERSOS'])+"</td>\
                                <td>"+separa(tipoDoc[i]['TOTAL'])+"</td>\
                            </tr>";
                if(parseInt(tipoDoc[i]['IDTIPOIMAGEN']) != 0){
                    tipificadas += parseInt(tipoDoc[i]['TOTAL']) ;
                }
                
                contMorrisData++;
            }
        }
        cadena += "</tbody><tfoot><tr>\
                    <td>Total</td>\
                    <td>"+separa(totDocs)+"</td>\
                    <td>"+separa(totReversos)+"</td>\
                    <td>"+separa(tot)+"</td>\
                </tr></tfoot></table>";
        $("#tipificadas").html(number_format(tipificadas,0));


        $("#imagenes").html(number_format(totDocs,0));
        $("#datos").html(cadena);

        console.log(data);

        Morris.Donut({
        element: 'tipoDoc',
        data: data,
        resize:true,
        colors: ['#1d49fc','#3ACE33','#ffe821','#FF9800','#F44336','#9B1900','#8100E0']
        });

    });


    function folios(avance){
        window.location="./consulta/index.php?avance="+avance;
    }


    function tabla_ejecutiva(){
        window.location=urlHome+'/reportes/reportes.php?opcion=tabla_ejecutiva';
    }
    
</script>  
