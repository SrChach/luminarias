<style>
	/* _________________ Radio Buttons styles _____________*/
    .switch {
        position: relative;
        display: inline-block;
        width: 50px;
        height: 14px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 20px;
        width: 20px;
        left: -4px;
        bottom: -3px;
        background-color: white;
        border: 1px solid #007bff;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(35px);
        -ms-transform: translateX(35px);
        transform: translateX(35px);
    }




    .min-width{
        min-width: 100px;
    }
    .formatImgLoading{
        width: 23px;
        height: 20px;
        display: none;
    }
    .loaderTable{
        margin: auto;
        vertical-align: middle;
        padding: 50px 0;
        width: auto;
        height: auto;
        max-width: 150px;
        max-height: 250px;
        display: none;
    }

    .loadingStandar{
        font-size: large;
        margin-right: .4rem;
    }

    .loaderTableStandar{
        margin: auto;
        vertical-align: middle;
        padding: 50px 0;            
        font-size: 6rem;
        color: #007bff;
        /*color: #28A6DE;  Color estandar para el loader */
        display: none;
        top: 25%;
    }
</style>


<section class='container-fluid'>
    <div class="row">
        <div class="col-md-3 row">
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="delegacion">
                            <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="formatImgLoading" id='loadingDelg'>Delegación -->
                            JEFE DE FAMILIA
                        </label>
                    </div>
                    <div class="form-control">
	                    <label class="switch align-middle form-control">
	                        <input type="checkbox" data-comboPerCheck="1" id="checkPersona" value="true">
	                        <span class="slider round"></span>  
	                    </label>                                            	
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="delegacion">
                            <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="formatImgLoading" id='loadingDelg'>Delegación -->
                            <i class="fa fa-spinner fa-spin loadingStandar" id='loadingDelg'></i>Delegación
                        </label>
                    </div>
                    <select class="custom-select" id="delegacion" onchange="construcSubDelg();">
                        <option value="">Selecciona...</option>
                    </select>                        
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="subDelegacion">Subdelegación</label>
                    </div>
                    <select class="custom-select" id="subDelegacion">
                        <option value='' selected>Selecciona...</option>                            
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">
                            <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="formatImgLoading" id='loadingEst'>Estado -->
                            <i class="fa fa-spinner fa-spin loadingStandar" id='loadingEst'></i>Estado
                        </label>
                    </div>
                    <select class="custom-select" id="estados" onchange="construcMcp();">
                        <option value='' selected>Selecciona...</option>                            
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">Municipio</label>
                    </div>
                    <select class="custom-select" id="municipios" onchange="construcLoc();">
                        <option value='' selected>Selecciona...</option>                            
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">Localidad</label>
                    </div>
                    <select class="custom-select" id="localidades">
                        <option value='' selected>Selecciona...</option>                            
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">Folio CUIS: </label>
                    </div>                                                
                    <input type="text" class='form-control' id="folioCuis" onchange="disableElements('folioCuis', 'nombre');">                        
                </div>
            </div>
            <div class="col-md-12 col-sm-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text min-width" style="font-size: 12px;" for="">Nombre: </label>
                    </div>
                    <input type="text" class='form-control' id="nombre" onchange="disableElements('nombre', 'folioCuis');">
                </div>
            </div>                

            <div class="col-md-12">
                <button class='btn btn-primary btn-block' onclick="getTableListado();">Buscar</button>
            </div>
        </div>
        <div class="col-md-9">
            <div class="container-fluid">
                <label for="" class='form-text'>Numero de Registros: <span id="numRegistros"</span></label>                    
            </div>
            <div class="row justify-content-center">
                <!-- <img src="http://www.lerocroy.com/wp-content/uploads/2017/01/loading5.gif" alt="" class="loaderTable"> -->
                <i class="fa fa-spinner fa-spin loaderTableStandar"></i>
                <div class="col-md-12" id="resultados"></div>
            </div>
        </div>
    </div>
</section>


<script>
	$(function(){
		getOptions();
	});	
	var resultsGeneral;
	var URL = '../../pillar/clases/integraExpListado_controller.php';

	function getOptions(){
		
	    let METODO = 'POST';
	    let objectData = { option: 'inicioIntegraListado' }

	    $('#loadingDelg').show('slow');
	    $('#loadingEst').show('slow');

	    var request = supportRequest(URL, METODO, objectData);

	    request.done(function(res){
	    	// console.log(JSON.parse(res));                
	        resultsGeneral = JSON.parse(res);                
	        // console.log(resultsGeneral);
	        let newOpDeleg = "<option value='' selected>Selecciona...</option>";
	        newOpDeleg += constructOptions(resultsGeneral.delegaciones, 'ID_DEL', 'DELEGACION');
	        $('#delegacion').html(newOpDeleg);
	                        
	        let newOpEst = "<option value='' selected>Selecciona...</option>";
	        newOpEst += constructOptions(resultsGeneral.estados, 'CVEENT', 'NOMBRE');
	        $('#estados').html(newOpEst);

	        $('#loadingDelg').hide('slow');
	        $('#loadingEst').hide('slow');
	    });

	    request.fail(function(jqXHR, textStatus){
	        console.log('Error: ', textStatus);
	        $('#loadingDelg').hide('slow');
	        $('#loadingEst').hide('slow');
	    });
	}





	function construcSubDelg(){            
        let subDelgSelect = $('#delegacion option:selected').val();            
        let newOpSubDeleg = "<option value='' selected>Selecciona...</option>";

        var disableOptions = false;
        if(subDelgSelect != '' && subDelgSelect != null){
            newOpSubDeleg += constructOptions(resultsGeneral.subdelegaciones[subDelgSelect], 'ID_SUB', 'NOMBRE');
            disableOptions = true;
        }
        changeSelectDefault("estados", disableOptions);
        changeSelectDefault("municipios", disableOptions);
        changeSelectDefault("localidades", disableOptions);
        changeInputDefault("folioCuis", disableOptions);
        changeInputDefault("nombre", disableOptions);            
        $('#subDelegacion').html(newOpSubDeleg);
    }

    function construcMcp(){            
        let selectEst = $('#estados option:selected').val();        
        let newOpMcp = "<option value='' selected>Selecciona...</option>";
        
        var disableOptions = false;
        if(selectEst != '' && selectEst != null){
            newOpMcp += constructOptions(resultsGeneral.municipios[selectEst], 'CVE_MUNICIPIO', 'NOMBRE');
            $('#municipios').html(newOpMcp);
            disableOptions = true;
        }
        else{
            $('#municipios').html(newOpMcp);
            construcLoc();
            disableOptions = false;  
        }

        changeSelectDefault("delegacion", disableOptions);
        changeSelectDefault("subDelegacion", disableOptions);            
        changeInputDefault("folioCuis", disableOptions);
        changeInputDefault("nombre", disableOptions);                        
    }
    
    function construcLoc(){            
        let selectLoc = $('#municipios option:selected').val();            
        let newOpLoc = "<option value='' selected>Selecciona...</option>";
        if(selectLoc != '' && selectLoc != null)newOpLoc += constructOptions(resultsGeneral.localidades[selectLoc], 'CVE_LOCALIDAD', 'NOMBRE');
        $('#localidades').html(newOpLoc);
    }

    function changeSelectDefault(idSelect, option){
        document.getElementById(idSelect).options.selectedIndex = 0;
        $('#'+idSelect).attr('disabled', option);
    }
    function changeInputDefault(idInputText, option){
        $('#'+idInputText).attr('disabled', option);
        $('#'+idInputText).val('');
    }

    function disableElements(idInput, idInputSecond){            
        var disableOptions = false;
        
        if($('#'+idInput).val() != "" && $('#'+idInput).val() != null){
            disableOptions = true;
        }else{
            if($('#'+idInputSecond).val() != "" && $('#'+idInputSecond).val() != null){
                disableOptions = true;
            }
        }

        changeSelectDefault("delegacion", disableOptions);
        changeSelectDefault("subDelegacion", disableOptions);
        changeSelectDefault("estados", disableOptions);
        changeSelectDefault("municipios", disableOptions);
        changeSelectDefault("localidades", disableOptions);            
    }       

    function getTableListado(){
        let delegacion = $('#delegacion option:selected').val();
        let subDelegacion = $('#subDelegacion option:selected').val();
        let estado = $('#estados option:selected').val();
        let municipio = $('#municipios option:selected').val();
        let localidad = $('#localidades option:selected').val();
        let folioCuis = $('#folioCuis').val();
        let nombre = $('#nombre').val();
        let parentesco = $('#checkPersona').is(':checked');                        
        
        // let URL = '../../pillar/clases/controlador_pruebas.php';
        let METODO = 'POST';
        let objectData = { 
        	option: 'buscarListado', 
        	delegacion: delegacion, 
        	subdelegacion: subDelegacion, 
        	estado: estado, 
        	municipio: municipio, 
        	localidad: localidad, 
        	folio_cuis: folioCuis, 
        	nombre: nombre,
        	parentesco : parentesco 
        };

        let request = supportRequest(URL, METODO, objectData);                    
        
        $('#resultados').hide('slow');
        $('.loaderTableStandar').show('slow');
        $('.tooltip').hide();
        request.done(function(res){ 
            let response = JSON.parse(res);
        	// console.log(response);               
            $('#numRegistros').html(response.generales.CONTEO);
            $('.loaderTableStandar').hide('slow');
            $('#resultados').show('slow');

            createTableListado(response.detalle);


            // tablePadron('#tbCuis','X',11);              coment
        });

        request.fail(function(jqXHR, textStatus){
            $('.loaderTableStandar').hide('slow');
            $('#resultados').show('slow');
            console.log('Error: ', textStatus);
        });
    }



	function supportRequest(URL, METODO, objectData){
        return $.ajax({
            url: URL,
            method: METODO,
            data: objectData,
            dataType: "HTML"
        });
    }

    function constructOptions(objectData, value, textValue, optionSelected = 0){
        var options = "";

        if(objectData){
        	objectData.forEach(function(item){
	            let select = (item == optionSelected[value]) ? 'selected' : "";                
	            options += "<option value='"+item[value]+"' "+select+">"+item[textValue]+"</option>" 
	        });
        }

        return options;
    }


    function createTableListado(datas) {   
       
        var cadena = "<table class='table table-bordered table-condensed table-striped  table-hover'  id='tbListado'>\
                        <thead class='thead-light' style='font-size:12px;'>\
                            <tr>\
                                <th>#</th>\
                                <th>FOLIOCUIS</th>\
                                <th>NOMBRE</th>\
                                <th>APELLIDO PATERNO</th>\
                                <th>APELLIDO MATERNO</th>\
                                <th>DELEGACION</th>\
                                <th>SUBDELEGACION</th>\
                                <th>CLAVE MUNICIPIO</th>\
                                <th>NOMBRE MUNICIPIO</th>\
                                <th>CLAVE LOCALIDAD</th>\
                                <th>NOMBRE LOCALIDAD</th>\
                                <th>PARENTESCO</th>\
                                <th></th>\
                            </tr>\
                        </thead>\
                        <tbody>";
                    
        var option='';
        $.each(datas,function (i,item) {
                
                //if (item.IDPROGRAMA==1) {
            option="?view=cedula&indexs="+item.IDLISTADO+',0';
            // }else{
            //     option="#!";
            // } 

            cadena += "<tr style='font-size:11px;'>\
                        <th>"+(i+1)+"</th>\
                        <td><a href='#!'><span class='click' data-toggle='tooltip' data-html='true' data-placement='right' title='<a href=\"?view=modules&act=consulta_cuis&index=1&folio="+item.FOLIOCUIS+"\" target=\"_blank\">CONSULTA FOLIOCUIS</a> <br> <a href=\"?view=modules&act=consulta_expediente&index=3&foliocuis="+item.FOLIOCUIS+"\" target=\"_blank\">CONSULTA EXPEDIENTE</a>' >"+item.FOLIOCUIS+"</span></a></td>\
                        <td>"+item.NOMBRE+"</td>\
                        <td>"+item.APATERNO+"</td>\
                        <td>"+item.AMATERNO+"</td>\
                        <td>"+item.DELEGACION+"</td>\
                        <td>"+item.SUBDELEGACION+"</td>\
                        <td>"+item.CVEMUN+"</td>\
                        <td>"+item.NOMBREMUN+"</td>\
                        <td>"+item.CVELOC+"</td>\
                        <td>"+item.NOMBRELOC+"</td>\
                        <td>"+item.PARENTESCO+"</td>\
                        <td>\
                            <a href='"+option+"' target='_blank'>\
                                <i class='fa fa-share' style='font-size:25px;'></i>\
                            </a>\
                        </td>\
                    </tr>";
        });

        cadena+="</tbody>\
        </table>";
        $('#resultados').html(cadena);
        tablePadron('#tbListado','CUIS',13)
        // tablePadron('#tbListado','PAdron');
        // settingsTable();
        
        $('.click').tooltip({trigger: "click"}); 
        $('[data-toggle="tooltip"]').tooltip();
    }

</script>