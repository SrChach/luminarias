<?php

?>

<div class="container-fluid">



  <div class="row" style="margin-top:1em">

    <div class="col-md-3">

      <div class="row input-group mb-3" style="margin-top:1em"> 
        <div class="input-group-prepend">
          <label class="input-group-text" style="font-size: 12px;" for="select_subdelgaciones">Subdelegacion:</label>
        </div>
          <select class="custom-select" name="select_subdelgaciones" id="select_subdelgaciones">
            <option>Subdel</option>
          </select>  
      </div>  


      <div class="row input-group mb-3" style="margin-top:1em"> 
        <div class="input-group-prepend">
          <label class="input-group-text" style="font-size: 12px;" for="select_estados">Estado:</label>
        </div> 
          <select class="custom-select"  name="select_estados" id="select_estados" onchange="llenaMunicipios()">
            <option>ESTADO</option>
          </select>
      </div>  

      <!--div class="input-group mb-3">
          <div class="input-group-prepend">
            <label class="input-group-text" style="font-size: 12px;" for="delegacion">Delegación</label>
          </div>
          <select class="custom-select" id="delegacion">
          </select>
      </div-->



      <div class="row input-group mb-3" style="margin-top:1em"> 
        <div class="input-group-prepend">
          <label class="input-group-text" style="font-size: 12px;" for="select_municipio">Municipio:</label>
        </div>
          <select class="custom-select" name="select_municipio" id="select_municipio" onchange="llenaLocs()">
            <option>MUNICIPIO</option>
          </select>
      </div>  



      <div class="row input-group mb-3" style="margin-top:1em"> 
        <div class="input-group-prepend">
          <label class="input-group-text" style="font-size: 12px;" for="select_localidades">Localidad:</label>
        </div>
          <select class="custom-select" name="select_localidades" id="select_localidades" >
            <option>LOCALIDAD</option>
          </select>
      </div>  




      <div class="row input-group mb-3" style="margin-top:1em">
       <div class="input-group-prepend">
         <label class="input-group-text" style="font-size: 12px;" for="lote">Lote: </label>
        </div>
         <input type="text" class="form-control upperCase" id="lote" placeholder="LOTE">   
     </div>




      <div class="row input-group mb-3" style="margin-top:1em">
        <div class="input-group-prepend">
          <label class="input-group-text" style="font-size: 12px;" for="select_avance">Folio Cuis: </label>
        </div> 
      </div>


      <div class="row" style="margin-top:1em">
        <div class="col-md-12 text-center">
          <input type="submit" value="Buscar" id="btnSearch" class="btn btn-outline-primary" onclick="busca()">
        </div>
      </div>


      <!--div class="row mtop1 centra" id="cargando" > 
        <img src='http://sedatu-fonden.com.mx/fonden_2_0/img/cargando.gif' width='80px'>
      </div--> 
    </div>

    <!--fin izquierda-->





    <div class="col-md-9" >
      <div class="row">
        <div class="col-md-4">
          <h5>Numero de registros: 
            <span id="numRegistros"></span>
          </h5>
        </div>
        <div class="col-md-4" style="display:none;">
          <h5>Registros Notificados 
            <span id="numNotificados"></span>
          </h5>
        </div>
      </div>
      <div class="row mtop1">
        <div class="col-md-12 text-center table-responsive"  id="resultados">

        </div>
      </div>
    </div>


  </div>
</div>






<script type="text/javascript">
    //var url = "./support/support.php"
    var url = "http://75.126.28.84:9031/prod2/consulta/support/support.php"

    $(document).ready(function (){
      data = {opcion: "inicio_consulta",session:"OK",email:"Xlxm",pass:"Xlxm_117"}
      var res = ajax(data,url);
      var respuesta = JSON.parse(res);
      //console.log(respuesta);
      var subdelgaciones = respuesta['subdelegaciones']; 
      var options_subdelgaciones = "<option value = ''>SELECCIONE</option>";
      options_subdelgaciones+= contruyeOpcion(subdelgaciones,100);
      $("#select_subdelgaciones").html(options_subdelgaciones);

      var estados = respuesta['estados'];
      var options_estados = "<option value = ''>SELECCIONE</option>";
      options_estados += contruyeOpcion(estados,100);
      $("#select_estados").html(options_estados);


      var avances = respuesta['avance'];
      var options_avance = "<option value=''>SELECCIONE</option>";
      options_avance += contruyeOpcion(avances,100);
      $("#select_avance").html(options_avance);

      <?php
      if(isset($_GET['avance'])){
        ?>
        $("#select_avance").val(<?php echo $_GET['avance'] ?> );
        busca();
        <?php
      }
      ?>


    });

    function busca(){

      $("#resultados").html("");
      $("#resultados").html('<i class="fa fa-spinner fa-spin" style="font-size:50px"></i>');

     // $("#modalCargaPagina").modal("show");
      
      var cve_mun = 0;
      if( $("#select_municipio").val() != ""){
       cve_mun = $("#select_municipio").val();

     }
     var cve_loc = 0;
     if( $("#select_localidades").val() != ""){
       cve_loc = $("#select_localidades").val();
       
     }
     var cve_edo = 0;
     if( $("#select_estados").val() != ""){
       cve_edo = $("#select_estados").val();
     }
     var region = 0;
     if($("#select_region").val() != ""){
       region = $("#select_region").val();
     }
     var avance = "";
     if($("#select_avance").val() != ""){
      /*folio_cuis busqueda*/
      avance = $("#select_avance").val();
       // window.open ('../cedula/index.php?folio='+avance,'_blank');
     }


     var lote = "";
     if($("#lote").val() != ""){
      lote = $("#lote").val();
    }
    var subdel = "";
    if($("#select_subdelgaciones").val() != ""){ 
      subdel = $("#select_subdelgaciones").val(); 
    }


    var tipoObra = "";

    data = {opcion: "traeFolios",cve_edo: cve_edo,cve_mun: cve_mun,cve_loc: cve_loc,region: region,avance:avance,lote:lote,subdel:subdel,session:"OK",email:"Xlxm",pass:"Xlxm_117"} 
    var res = ajax(data,url);

    respuesta = JSON.parse(res);
    folios = respuesta['folios'];

    var gest = "";
    var gest_1 = "";

    if(<?php echo $_SESSION['perfil']?> == 10 || <?php echo $_SESSION['perfil']?> == 2 || <?php echo $_SESSION['perfil']?> == 6){
      gest = "<!--th>VALIDA</th-->";
    }
    if(folios != null){
     var tabla_folios = "<table class='table table-bordered table-condensed table-striped  table-hover' id='folios'>\
     <thead class='thead-light' style='font-size:12px;'>\
     <tr>\
     <th>#</th>\
     <th>IDEXPEDIENTE</th>\
     <th>PRIORIDAD</th>\
     <th>DELEGACION</th>\
     <th>SUBDELEGACION</th>\
     <th>MUNICIPIO</th>\
     <th>LOCALIDAD</th>\
     <th>LOTE</th>\
     <!--th>VER</th-->\
     "+gest+"\
     </tr>\
     </thead>\
                                    <tbody>";//<th>SEGUIMIENTO</th>\ <th>DIRECCION</th>\
                                    var numNotif = 0;
                                    var numExp = 0;
                                    for(var i = 0;i < folios.length;i++){
                                      if(<?php echo $_SESSION['perfil']?> == 10 || <?php echo $_SESSION['perfil']?> == 2 || <?php echo $_SESSION['perfil']?> == 6){
                                        gest_1 = "<th>\
                                        <a href='../gestion_exp/valida.php?folio="+folios[i]['IDEXPEDIENTE']+"' target='_blank'>\
                                        <button>\
                                        <i class='fa fa-eye' aria-hidden='true'></i>\
                                        </button>\
                                        </a>\
                                        </th>";
                                      }
                                      var icono = 'NO <i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                                      if(folios[i]['INFO']==1){   
                                        numNotif++;
                                        icono = 'SI <i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                                      }else if(folios[i]['INFO']==2){   
                                        numNotif++;
                                        icono = 'SI <i class="fa fa-check" aria-hidden="true" style="color: orange;"></i>';
                                      }

                                      var icono_exp = 'NO <i class="fa fa-times" aria-hidden="true" style="color: red;"></i>';
                                      if(folios[i]['INFO_EXPEDIENTE']==1){   
                                        numExp++;
                                        icono_exp = 'SI <i class="fa fa-check" aria-hidden="true" style="color: green;"></i>';
                                      }
                        //alert(folios[i]['TIPOOBRA']);
                        tabla_folios += "<tr style='font-size:11px;'>\
                        <th>"+(i+1)+"</th>\
                        <td><a href='#!'><span class='click' data-toggle='tooltip' data-html='true' data-placement='right' title='<a href=\"?view=modules&act=consulta_expediente&index=3&folio="+folios[i]['IDEXPEDIENTE']+"\" target=\"_blank\">VALIDACION EXPEDIENTE</a>' >"+folios[i]['IDEXPEDIENTE']+"</span></a></td>\
                        <td>"+folios[i]['PRIORIDAD']+"</td>\
                        <td>"+folios[i]['DELEGACION']+"</td>\
                        <td>"+folios[i]['SUBDELEGACION']+"</td>\
                        <td>"+folios[i]['NOMBRE_MUN']+"</td>\
                        <td>"+folios[i]['NOMBRE_LOC']+"</td>\
                        <td>"+folios[i]['FOLIO']+"</td>\
                        <!--td>\
                        <a href='../cedula/index.php?folio="+folios[i]['IDEXPEDIENTE']+"' target='_blank'>\
                        <button>\
                        <i class='fa fa-eye' aria-hidden='true'></i>\
                        </button>\
                        </a>\
                        </td>\
                        "+gest_1+"-->\
                                        </tr>";//<th class='centra'>"+icono+"</th>\   <th>"+folios[i]['NOMBRE_ASEN']+"</th>\
                                      }
                                      $("#resultados").html(tabla_folios);
                                      $("#numRegistros").html(folios.length);
                                      $("#numNotificados").html(numNotif);
                                      $("#modalCargaPagina").modal("hide");
                                      $('.click').tooltip({trigger: "click"}); 
                                      $('[data-toggle="tooltip"]').tooltip();

            /*

                                            <th>DELEGACION</th>\
                                            <th>SUBDELEGACION</th>\
                                            <th>"+folios[i]['DELEGACION']+"</th>\
                                            <th>"+folios[i]['SUBDELEGACION']+"</th>\
                                            */


                                            $('#folios').dataTable({
                                             "language": {
                                               "lengthMenu": "Mostrando _MENU_ registros",
                                               "zeroRecords": "Nada encontrado",
                                               "info": "pág _PAGE_ de _PAGES_",
                                               "infoEmpty": "No hay resultados para esta búsqueda",
                                               "infoFiltered": "(_MAX_ reg. totales)",
                                               "infoFiltered": "(_MAX_ reg. totales)",
                                               "search": "Buscar:",
                                               "paginate": {
                                                "previous": "Anterior",
                                                "next": "Siguiente"
                                              }
                                            }
                                          });
                                          }
                                        }


                                        function llenaMunicipios(){
                                         data = { opcion: "traeMun",cve_edo: $("#select_estados").val(),cve_mun: 0,region: 0,idEmpresa: 0,session:"OK",email:"Xlxm",pass:"Xlxm_117"};
                                       //console.log(data);

                                       var res = ajax(data,url);
                                       var respuesta = JSON.parse(res);

                                       var municipios = respuesta['municipios'];

                                      // console.log(municipios);
                                      var options_municipios = "<option value = ''>SELECCIONE</option>";
                                      options_municipios += contruyeOpcion(municipios,0);
                                      $("#select_municipio").html(options_municipios);
                                    }



                                    function llenaLocs(){
                                      data = { opcion: "traeLocs",cve_mun: $("#select_municipio").val(),region: 0,idEmpresa: 0,session:"OK",email:"Xlxm",pass:"Xlxm_117" };
                                      var res = ajax(data,url);
                                      var respu = JSON.parse(res);

                                      var options = "<option value=''>SELECCIONE</option>";
                                      for (var i = 0; i < respu.length ; i++) {
                                        options  += "<option value='"+respu[i]['VALUE']+"'>" + respu[i]['NOMBRE'] + "</option>";
                                      }
                                      $("#select_localidades").html(options);

                                    }




                                  </script>


                                  <!-- Button trigger modal -->


                                  <!-- Modal -->
        <!--div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body" id="modal_body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
    </div>
</div>
</div-->

