<style>
    table > thead{
        background-color: #2bb273;
        color: white;
        font-family: sans-serif;
    }
    .bg-grey{
        background-color: #e9ecef;
        border-radius: .3rem;
        padding : 15px 10px;
        overflow-x: scroll;
    }

    .label-width{
        min-width : 160px;
    }

    .vertical-center-grid{
        align-content: center;
        display: grid;
    }

    .custom-control-label::before{
        border: 1px solid darkgrey;
        background-color: white;
    }

    .container {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    
    .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
        border: 1px solid #2196F3;
    }

    .container:hover input ~ .checkmark {
        background-color: #ccc;
    }

    .container input:checked ~ .checkmark {
        background-color: #2196F3;
    }

    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    .container input:checked ~ .checkmark:after {
        display: block;
    }

    .container .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    #contentSaveUsers, #textAsign{
        display: none;
    }

</style>

<div class="container-fluid">
    <h1 class="text-center mb-3">Grupos de Trabajo</h1>

    <section class="tableWorkGroup mb-3">
        <div id="workGroupTable"></div>
        
    </section>

    <section class="">
        <div class="row">
            <div class="col-lg-6 mb-5">
                <div class="bg-grey">
                    <div class="input-group mb-3" style="margin-top:1em">
                        <div class="input-group-prepend">
                            <label for="" class="input-group-text label-width">Nombre del Grupo</label>
                        </div>
                        <input type="text" name="nameGroup" id="nameGroupWork" class="form-control" placeholder="Nombre del Grupo">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label for="" class="input-group-text label-width">Origen</label>
                        </div>
                        <select name="" id="Origin" class="form-control">
                            <option value="">Seleccione...</option>
                            <option value="1">Campo</option>
                            <option value="2">Masivo</option>
                            <option value="3">Todos</option>
                        </select>
                    </div>

                    <div id="typeImg"></div>

                    <div class="container-fluid">
                        <button class="btn btn-primary btn-large" onclick="addWorkGroup();">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-5">
                <div class="bg-grey h-100 pt-3">
                    <div class="container-fluid">
                        <h3 id="textAsign" class="text-center font-weight-bold" style="color: #2bb273;">Asignar Usuarios al Grupo <span id="nameGroupAsign"></span></h3>
                    </div>
                    <h2 class="text-center mb-2">Usuarios con Perfil Operativo</h2>
                    <div id="usersProfileOp"></div>

                    <div id="contentSaveUsers" class="my-3">
                        <div class="row">
                            <div class="col-lg-6" id="btnSaveUsers"></div>
                            <div class="col-lg-6">
                                <button class="btn btn-block btn-success" onclick="cancelAsignWorkGroup();">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    let URL = './controller/gruposTrabajo_controller.php'
    $(function(){
        let objectSend = {
            option : 'getInitInfo'
        }

        let request = petitionSupport(URL, objectSend);

        request.done(function(res){
            let response = JSON.parse(res);

            if(response != undefined && Object.keys(response).length > 0){

                constructWorkGroups(response.workGroups, response.listTypeImg);     
           
                let listTypeImg = '<table class="table table-striped" id="tableTypeImg">\
                                    <thead>\
                                        <tr>\
                                            <th scope="col">IdTipoImagen</th>\
                                            <th scope="col">TipoImagen</th>\
                                            <th scope="col"></th>\
                                        </tr>\
                                    </thead>\
                                    <tbody>';
                if(response.listTypeImg != undefined && Object.keys(response.listTypeImg).length > 0 ){
                    $.each(response.listTypeImg, function(key, value){
                        listTypeImg += '<tr class="col-auto">\
                                            <td>'+value[0].IDTIPOIMAGEN+'</td>\
                                            <td>'+value[0].TIPOIMAGEN+'</td>\
                                            <td>\
                                                <label class="container">\
                                                    <input type="checkbox" data-target="tipoImg" value="'+value[0].IDTIPOIMAGEN+'">\
                                                    <span class="checkmark"></span\
                                                </label>\
                                            </td>\
                                        </tr>';  
                    });
                }
                
                listTypeImg += '</tbody>\
                            </table>';
                
                $('#typeImg').html(listTypeImg);
                tablePadron('#tableTypeImg','Tipo Imagen', 3);
                
                
                constructUsersGroup(response.listOperativeUsers);
            }
        });

        request.fail(function(XHR, status){
            console.log(status);
        });

    });

    function constructWorkGroups(listWorkGroups, listTypeImg){
        let workGroups = '<table class="table table-striped" id="tableWorkGroup">\
                                    <thead>\
                                        <th>IdGrupo</th>\
                                        <th>Grupo</th>\
                                        <th>Origen</th>\
                                        <th>TipoImagen</th>\
                                        <th>Acción</th>\
                                        <th>Asignar</th>\
                                    </thead>\
                                    <tbody>';
        if(listWorkGroups != undefined && Object.keys(listWorkGroups).length > 0 ){
            $.each(listWorkGroups, function(key, item){
                let tiposImagen = item.TIPOSIMAGEN.split(',');
                let result = '';
                tiposImagen.forEach(function(itemTypeImg){
                    result += listTypeImg[itemTypeImg][0]['TIPOIMAGEN'] + ', ';
                });
                result = result.slice(0, -2);
                
                workGroups += '<tr class="col-auto">\
                                    <td>'+item.IDGRUPO+'</td>\
                                    <td>'+item.GRUPO+'</td>\
                                    <td>'+item.ORIGEN+'</td>\
                                    <td>'+result+'</td>\
                                    <td><button class="btn btn-danger btn-block" onclick="removeWorkGroup('+item.IDGRUPO+')">Eliminar</button></td>\
                                    <td><button class="btn btn-primary btn-block" onclick="asignWorkGroup(\''+item.GRUPO+'\','+item.IDGRUPO+')">Asignar</button></td>\
                                </tr>';
                                
            });
        }
        workGroups += '</tbody>\
                    </table>';
    
        $('#workGroupTable').html(workGroups);
        tablePadron('#tableWorkGroup','Grupos Trabajo', 6);
    }

    function constructUsersGroup(listOperativeUsers){
        /* -----------------------USRPO--------------------------- */
        listTypeImg = '<table class="table table-striped" id="tableUsProfOp">\
                                <thead>\
                                    <tr>\
                                        <th></th>\
                                        <th>Nombre Usuario</th>\
                                        <th>Grupo Actual</th>\
                                    </tr>\
                                </thead>\
                                <tbody>';

        if(listOperativeUsers != undefined && Object.keys(listOperativeUsers).length > 0)
        {
            $.each(listOperativeUsers, function(key, item){
                listTypeImg += '<tr class="col-auto">\
                                    <td>\
                                        <label class="container">\
                                            <input type="checkbox" value="'+item.IDUSUARIO+'" data-target="user">\
                                            <span class="checkmark" style="margin-left: 50% !important;"></span\
                                        </label>\
                                    </td>\
                                    <td>'+item.NOMBRE+'</td>\
                                    <td>'+(item.GRUPO == null ? '' : item.GRUPO)+'</td>\
                                </tr>';  
            });
        }

        listTypeImg += '</tbody>\
                    </table>';

        $('#usersProfileOp').html(listTypeImg);
        tablePadron('#tableUsProfOp','Perfil Operativo', 3);

    }

    function findWorkGroups(){
        let objectSend = {
            option : 'findWorkGroups'
        }

        let request = petitionSupport(URL, objectSend);

        request.done(function(res){
            let response = JSON.parse(res);

            if(response != undefined && Object.keys(response).length > 0){
                constructWorkGroups(response.workGroups, response.listTypeImg); 
            }
        });
    }
    
    function findUsers(){
        let objectSend = {
            option : 'findUsers'
        }

        let request = petitionSupport(URL, objectSend);

        request.done(function(res){
            let response = JSON.parse(res);

            if(response != undefined && Object.keys(response).length > 0){
                constructUsersGroup(response.listOperativeUsers); 
            }
        });
    }
    
    function addWorkGroup(){
        let nameGroup = $('#nameGroupWork').val();
        let origin = $('#Origin').val();
        let typeImg = '';

        $("input[data-target='tipoImg']:checked").each(function(){
            typeImg += $(this).val() + ',';
        });

        typeImg = typeImg.slice(0, -1);
        
        let ObjectSend = {
            option : 'addWorkGroup',
            nameGroup : nameGroup,
            origin : origin,
            typeImg : typeImg
        };
        let request = petitionSupport(URL, ObjectSend);

        request.done(function(res){
            if(res == true || res == 'true'){
                alert('Inserción Correcta');
                findWorkGroups();
                // $("input[data-target='tipoImg']:checked").prop('checked', false);
            }
        });
    }

    function removeWorkGroup(keyWorkGroup){
        let objectSend = {
            option : 'removeWorkGroup',
            keyWork : keyWorkGroup
        }

        let request = petitionSupport(URL, objectSend);

        request.done(function(res){
            
            if(res == true || res == 'true'){
                alert('Grupo Eliminado');
                findWorkGroups();
            }else{
                alert('Error');
            }
        });
    }
    
    function asignWorkGroup(group, keyWorkGroup){
        $('#nameGroupAsign').text(group);
        $('#btnSaveUsers').html('<button class="btn btn-block btn-primary" onclick="saveUsersGroup('+keyWorkGroup+');">Guardar Usuarios</button>');
        $('#textAsign').show();
        $('#contentSaveUsers').show();
        $('#btnSaveUsers').show();
    }
    
    function cancelAsignWorkGroup(){
        $('#nameGroupAsign').text('');
        $('#btnSaveUsers').html('');
        $('#textAsign').hide();
        $('#contentSaveUsers').hide();
    }

    function saveUsersGroup(keyWorkGroup){
        let users = [];
        $("input[data-target='user']:checked").each(function(i){
            users.push($(this).val());
        });

        let objectSend = {
            option : 'asignUsers',
            users : users,
            keyWorkGroup : keyWorkGroup
        }

        let request = petitionSupport(URL, objectSend);
        
        request.done(function(res){
            if(res == true || res == 'true'){
                alert('Usuarios Agregados al Grupo');
            }else{
                alert('Algunos Usuarios no se ha agregado al grupo');
            }
            findUsers();
        });

        request.fail(function(XHR, status){
            console.log(status);
        });
    }

    function petitionSupport(URL, ObjectData){
        return $.ajax({
            url : URL,
            method : 'POST',
            data : ObjectData
        });
    }
</script>