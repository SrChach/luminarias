<div class="container-fluid">
	<div class="row">
		<div class="col">
			<?php

				/**
				*
				*/
				class viewSitios
				{
					protected $route;
					protected $sitios;
					protected $files;
					protected $actS;
					function __construct()
					{
						$this -> sitios=['','LEVANTAMIENTO DE CUIS','EXPEDICIÓN DE CREDENCIALES', 'INTEGRACIÓN DE EXPEDIENTES','ADMINISTRACIÓN Y CAMBIOS AL PADRON',
						'COMITES COMUNITARIOS Y CASAS DE ENLACE','AVANCE PRESUPUESTAL','RECURSOS HUMANOS','CONTROL VEHICULAR', 'GENERACIÓN DE VIÁTICOS','PLANEACIÓN OPERATIVA', 'RESGUARDO DE DM´S', 'ALMACEN OFICINA','ICATVER', 'FOTOCOPIADO'];
						$this -> routes=['','Cuis','Credenciales','IntegracionExpedientes','ActualizacionesPadron','ComitesCasas','AvancePresupuestal','RH','ControlVeh','GeneraVia','PlanOperativa','DMS','AlmacenOfi','Icatver','Fotocopiado'];
						$this -> files=['','busqueda_cuis','credenciales','integraExpeListado','actpadron','comiteCasas','avancePre','rh','controlVeh','generaVia','planOper','dms','almaOfi','icatver','fotocopiado', 'PanelEjecutivo'];
						$this -> actS = array('1' => ['PanelEjecutivo','consulta_cuis','carencias','poverty', 'unirTablas-271118', 'combos-291118'],
											'2' => ['lotesImpresionCredenciales'],
											'3' => ['index','consulta_expediente', 'gruposTrabajo'],
											'4' => ['cambios_cuis', 'altaBaja']);
					}
					public function viewSites($index,$act,$idP='')
					{	$view='';
						if ($act=='') {
							$view=$this -> files[$index];
						}else{
							if ($this->request($index,$act)) {
								$view=$act;
							}else{
								return array('site' => './view/Error/requestDenied.php','error' => 400,'module'=>'');
							}
							//echo $this->request($index,$act);
						}
						if ($idP!='') {
							$view='credencial_Programa';
						}

						$datos= array('site' => './view/modules/'.$this -> routes[$index].'/'.$view.'.php',
										'module'=> $this -> sitios[$index]);

						return $datos;
					}
					public function request($index,$act)
					{
						$request=false;
						$GET=$this -> actS[$index];
						for ($i=0; $i < count($GET); $i++) {
							if ($act==$GET[$i]) {
								$request=true;
								return $request;
							}else{
								$request=false;
							}
						}
						//var_dump($GET);
						return $request;
					}
				}
				$controllerSites= new viewSitios;
				$act=(isset($_GET['act']) and $_GET['act']== $_GET['act']) ? $_GET['act']:'';
				$idP=(isset($_GET['idP']) and $_GET['idP']== $_GET['idP']) ? $_GET['idP']:'';
				$viewSites=$controllerSites-> viewSites($_GET['index'],$act,$idP);
				include_once $viewSites['site'];
				//var_dump($viewSites);
			?>
		</div>
	</div>
</div>
<?php if (!isset($_GET['idP'])): ?>
<script type="text/javascript">
	$('#titlePage-2').html(': <?php echo $viewSites['module'] ?>');
</script>
<?php endif ?>
