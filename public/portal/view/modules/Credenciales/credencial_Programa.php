<style type="text/css">

</style>

<div class="container-fluid">
	<div class="row" id="contentBody">

    <?php //include_once './view/modules/Credenciales/completes/filtros_Meta.php'; ?>

    <div class="col-md-12 pt-5" id="content-Delegacion" style="display: none;"><!--TABLAS Y AVENCES-->
        <div class="row">
            <div class="col-md-4 pr-3 border-right-1" id="tableAvances" style="font-size: 12px;">
                Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>
            </div>
            <div class="col-md-8 text-center boderSALL">
                <h3 id="titleGrafica">DELEGACIONES.</h3>
                <div class="col-md-12 text-left">
                    <button class="btn btn-outline-secondary" id="btnBack" data-toggle="tooltip" data-placement="right" title="">
                        <!--i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i-->
                        <i class="fa fa-refresh" style="color:#276092;" id="iconMore"></i>
                    </button>
                </div>
                <div id="graficaAvance" class="table-responsive" style="height: 400px; padding-bottom: 50px;font-family: 'NeoSansPro-Bold'" >
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
                </div>
            </div>
        </div>
    </div>

    <?php include_once './view/modules/Credenciales/completes/complete_avances.php'; ?>

        
	</div>
</div>
<script type="text/javascript">
    var idP = <?php echo $_GET['idP'] ?>;
    var _t = '<?php echo $csrf_token ?>';
</script>
<script type="text/javascript" src='./assets/js/credenciales.js'></script>

