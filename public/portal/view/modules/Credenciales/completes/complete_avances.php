    <div class="col-md-12" id="content-Subdelegacion" style="display: none;"><!--TABLAS Y AVENCES SUBDELEGACIÓN-->
        <div class="row">
            <div class="col-md-4 pr-3 border-right-1" id="tableAvancesSubDelegacion" style="font-size: 12px;">
                Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>
            </div>
            <div class="col-md-8 text-center boderSALL">
                <h3 id="titleGraficaSubdelegacion"></h3>
                <div class="col-md-12 text-left">
                    <button class="btn btn-outline-secondary" id="btnBackDelegacion" data-toggle="tooltip" data-placement="right" title="">
                        <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                    </button>
                </div>
                <div id="graficaAvanceSubDelegacion" class="table-responsive" style="height: 400px; padding-bottom: 50px;font-family: 'NeoSansPro-Bold'" >
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" id="content-Municipio" style="display: none;"><!--TABLAS Y AVENCES MUNICIPIO-->
        <div class="row">
            <div class="col-md-4 pr-3 border-right-1" id="tableAvancesMunicipio" style="font-size: 12px;">
                Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>
            </div>
            <div class="col-md-8 text-center boderSALL">
                <h3 id="titleGraficaMunicipio"></h3>
                <div class="col-md-12 text-left">
                    <button class="btn btn-outline-secondary" id="btnBackSubdelegacion" data-toggle="tooltip" data-placement="right" title="">
                        <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                    </button>
                </div>
                <div id="graficaAvanceMunicipio" class="table-responsive" style="height: 400px; padding-bottom: 50px;font-family: 'NeoSansPro-Bold'" >
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
                </div>
            </div>
        </div>
    </div>

        <div class="col-md-12" id="content-Localidad" style="display: none;"><!--TABLAS Y AVENCES LOCALIDAD-->
        <div class="row">
            <div class="col-md-4 pr-3 border-right-1" id="tableAvancesLocalidad" style="font-size: 12px;">
                Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>
            </div>
            <div class="col-md-8 text-center boderSALL">
                <h3 id="titleGraficaLocalidad"></h3>
                <div class="col-md-12 text-left">
                    <button class="btn btn-outline-secondary" id="btnBackMunicipio" data-toggle="tooltip" data-placement="right" title="">
                        <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                    </button>
                </div>
                <div id="graficaAvanceLocalidad" class="table-responsive" style="height: 400px; padding-bottom: 50px;font-family: 'NeoSansPro-Bold'" >
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
                </div>
            </div>
        </div>
    </div>




