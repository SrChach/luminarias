
    <div class="col-md-12" id="content-Subdelegacion" style="display: none;"><!--TABLAS Y AVENCES SUBDELEGACIÓN-->
        <div class="row boderSALL">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <button class="btn btn-outline-secondary" id="btnBackDelegacion" data-toggle="tooltip" data-placement="right" title="">
                            <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                        </button>
                    </div>
                    <div class="col-md-10">
                        <h3 id="titleGraficaSubdelegacion" class="text-center"></h3>  
                    </div>
                </div>
                <div id="graficaAvanceSubDelegacion" class="col-md-12 boxGraph">
                    
                </div>
            </div>
            <div class="col-md-12 text-center">
                <hr class="hrSeparator">
                <div class="" id="wait2">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>    
                </div>
            </div>
            <div class="col-md-12" style="font-size: 12px;">
                <div class="col-md-12" id="tableAvancesSubDelegacion">
                    
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" id="content-Municipio" style="display: none;"><!--TABLAS Y AVENCES MUNICIPIO-->
        <div class="row boderSALL">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <button class="btn btn-outline-secondary" id="btnBackSubdelegacion" data-toggle="tooltip" data-placement="right" title="">
                            <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                        </button>
                    </div>
                    <div class="col-md-10">
                        <h3 id="titleGraficaMunicipio" class="text-center"></h3>
                    </div>
                </div>
                <div id="graficaAvanceMunicipio" class="col-md-12 boxGraph">
                    
                </div>
            </div>
            <div class="col-md-12 text-center">
                <hr class="hrSeparator">
                <div class="" id="wait3">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>    
                </div>
            </div>
            <div class="col-md-12" style="font-size: 12px;">
                <div class="col-md-12" id="tableAvancesMunicipio">
                    
                </div>
            </div>
        </div>
        <!--div class="row">
            <div class="col-md-4 pr-3 pl-3 border-right-1" id="tableAvancesMunicipio" style="font-size: 12px;">
                Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>
            </div>
            <div class="col-md-8 text-center boderSALL">
                <h3 id="titleGraficaMunicipio"></h3>
                <div class="col-md-12 text-left">
                    <button class="btn btn-outline-secondary" id="btnBackSubdelegacion" data-toggle="tooltip" data-placement="right" title="">
                        <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                    </button>
                </div>
                <div id="graficaAvanceMunicipio" class="table-responsive boxGraph">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
                </div>
            </div>
        </div-->
    </div>

    <div class="col-md-12" id="content-Localidad" style="display: none;"><!--TABLAS Y AVENCES LOCALIDAD-->
        <div class="row boderSALL">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <button class="btn btn-outline-secondary" id="btnBackMunicipio" data-toggle="tooltip" data-placement="right" title="">
                            <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                        </button>
                    </div>
                    <div class="col-md-10">
                        <h3 id="titleGraficaLocalidad" class="text-center"></h3>
                    </div>
                </div>
                <div id="graficaAvanceLocalidad" class="col-md-12 boxGraph">
                    
                </div>
            </div>
            <div class="col-md-12 text-center">
                <hr class="hrSeparator">
                <div class="" id="wait4">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>    
                </div>
            </div>
            <div class="col-md-12" style="font-size: 12px;">
                <div class="col-md-12" id="tableAvancesLocalidad">
                    
                </div>
            </div>
        </div>
        <!--div class="row">
            <div class="col-md-4 pr-3 pl-3 border-right-1" id="tableAvancesLocalidad" style="font-size: 12px;">
                Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px;"></i>
            </div>
            <div class="col-md-8 text-center boderSALL">
                <h3 id="titleGraficaLocalidad"></h3>
                <div class="col-md-12 text-left">
                    <button class="btn btn-outline-secondary" id="btnBackMunicipio" data-toggle="tooltip" data-placement="right" title="">
                        <i class="fa fa-angle-double-left" style="color:#276092;" id="iconMore"></i>
                    </button>
                </div>
                <div id="graficaAvanceLocalidad" class="table-responsive boxGraph">
                    Cargando <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
                </div>
            </div>
        </div-->
    </div>




