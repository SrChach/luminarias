<style>
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .checkLabelStyle {
        width: fit-content;
    }
    .checkStyle + .checkLabelStyle {
        display: block;
        margin: 0.2em;
        cursor: pointer;
        padding: 0.2em;
    }

    .checkStyle {
        display: none;
    }

    .checkStyle + .checkLabelStyle:before {
        content: "\2714";
        border: solid #007bff;
        border-radius: 0.2em;
        display: inline-block;
        width: 1.3em;
        height: 1.3em;
        vertical-align: middle;
        color: transparent;
        transition: .2s;
        margin-right: .3em;
    }

    .checkStyle + .checkLabelStyle:active:before {
        transform: scale(0);
    }

    .checkStyle:checked + .checkLabelStyle:before {
        background-color: MediumSeaGreen;
        border-color: MediumSeaGreen;
        color: white;
        align-content: center;
        display: inline-grid;        
    }

    .checkStyle:disabled + .checkLabelStyle:before {
        transform: scale(1);
        border-color: #aaa;
    }

    .checkStyle:checked:disabled + .checkLabelStyle:before {
        transform: scale(1);
        background-color: #bfb;
        border-color: #bfb;
    }
    .bloqueo{
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;       
        overflow: hidden;
        outline: 0;        
        position: fixed;
        background-color: rgba(154, 161, 164, 0.2);
        z-index: 10;
        display : none;
    }   
    /* .loaderStandar > i{
        color: #2bb273;
    } */
    .loaderStandar{
        margin: auto;
        vertical-align: middle;        
        font-size: 6rem;
        /* color: #2bb273; */
        color: #28A6DE;
        position: absolute;
        top: 45%;
        width : 100%;
        text-align : center;       
    }
</style>

<div class="bloqueo text-center">
    <div class="loaderStandar text-center">        
        <h4 class="text-center bold" style="color: #2bb273;">Cargando ...</h4>
        <i class="fa fa-spinner fa-spin" style="color: #28A6DE;"></i>
    </div>
</div>

<div class="container-fluid">
    <section class="container my-4">
        <div class="row">
            <div class="col-md-2">
                <label for="" class="form-text text-center">Lote Actual</label>
                <label for="" class="form-text text-center" id="loteActual"></label>
            </div>
            <div class="col-md-2">
                <label for="" class="form-text text-center">Impresiones</label>
                <label for="" class="form-text text-center" id="totalImpresiones"></label>
            </div>
            <div class="col-md-2">
                <label for="" class="form-text text-center">Reimpresiones</label>
                <label for="" class="form-text text-center" id="totalReimpresiones"></label>
            </div>
            <div class="col-md-2">
                <label for="" class="form-text text-center">Total</label>
                <label for="" class="form-text text-center" id="totalLotes"></label>
            </div>
            <div class="col-md-2">
                <button class="btn btn-block btn-primary" onclick="closeLots()">Cerrar Lote</button>
            </div>
        </div>
    </section>
    <div class="row my-3">
        <div id="resultadosTablaLotes" class="col-lg-12"></div>
    </div>

    <!-- <input type="checkbox" id="firstDay" name="typeDayOrder" value="1" class="checkStyle" data-catDate="1" data-dateOrder="1">
    <label for="firstDay" class="checkLabelStyle">1</label> -->
</div>



<script>
    var URL = '../../public/portal/controller/lotesImpresion_controller.php';
    $(function(){
        getLotActive();
        getAllLots();        
    });

    function getLotActive(){
        let METODO = 'POST';
        let objectSend = {
            option: 'getLotActive'
        }
        let request = petitionSupport(URL, METODO, objectSend);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);
            if(Object.keys(response).length !== 0){
                // console.log(response);
                $('#loteActual').text(response.IDLOTEIMPRESION);
                $('#totalImpresiones').text(response.IMPRESIONES);
                $('#totalReimpresiones').text(response.REIMPRESIONES);
                $('#totalLotes').text(response.TOTAL);
                // createTableLotesImpresion(response);
            }
            $('.bloqueo').fadeOut('slow');
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }

    function getAllLots(){
        let METODO = 'POST';
        let objectSend = {
            option: 'getAllLots'
        }
        let request = petitionSupport(URL, METODO, objectSend);
        $('.bloqueo').show();
        request.done(function(res){
            let response = JSON.parse(res);
            if(Object.keys(response).length !== 0){
                createTableLotesImpresion(response);
            }
            $('.bloqueo').fadeOut('slow');
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }
    function closeLots(){
        let METODO = 'POST';
        let objectSend = {
            option : 'closeLot',
            cveLoteActual : $('#loteActual').text()
        }
        let request = petitionSupport(URL, METODO, objectSend);
        $('.bloqueo').show();
        request.done(function(res){
            // console.log(res);
            if(res == false || res == 'false'){
                alert('Error al cerrar el lote');
            }else{
                getLotActive();
                getAllLots();
            }
            $('.bloqueo').fadeOut('slow');
        });
        request.fail(function(XHR, textStatus){
            console.log(textStatus);
            $('.bloqueo').fadeOut('slow');
        });
    }

    function petitionSupport(URL, METODO, objectData){
        return $.ajax({
            url: URL,
            method : METODO,
            data : objectData
        });
    }
    function createTableLotesImpresion(objectInfo) {           
        load(); 
        var lote=$('#loteActual').text();
        var cadena = "<table class='table table-bordered table-condensed table-striped  table-hover'  id='tablaLotes'>\
                        <thead class='thead-light' style='font-size:12px;'>\
                            <tr>\
                                <th>#</th>\
                                <th>Lote</th>\
                                <th>Fecha Apertura</th>\
                                <th>Fecha Cierre</th>\
                                <th>Impresiones</th>\
                                <th>Reimpresiones</th>\
                                <th>Total</th>\
                                <th>Descargar Base</th>\
                            </tr>\
                        </thead>\
                        <tbody>";
                    
        var links="";
        var exists=null;

        $.each(objectInfo,function (i,item) {
                //if (item.IDPROGRAMA==1) {
            // option=hrefCedula();
            // }else{
            //     option="#!";
            // }             
            
            if (fileExist(item.IDLOTEIMPRESION)==true||fileExist(item.IDLOTEIMPRESION)=='true') {
                links="<i class='fa fa-cloud-download' aria-hidden='true' style='color: #007bff;font-size: xx-large;cursor: pointer;' onclick='printLote("+item.IDLOTEIMPRESION+");'></i>\
                        <a href='../../../../MEDIA/imagenes_impresion_credenciales/lote_"+item.IDLOTEIMPRESION+".zip' class='tooltipZip' id='Zip-loteFile"+item.IDLOTEIMPRESION+"' download><i class='fa fa-file-zip-o' style='color: #007bff;font-size: xx-large;'></i></a>\
                        <a  class='tooltipZipReload' id='Zip-loteReload"+item.IDLOTEIMPRESION+"' onclick='makeZip("+item.IDLOTEIMPRESION+")'><i class='fa fa-refresh' style='color: #007bff;font-size: xx-large;'></i></a>\
                ";
            }else{
                links="<i class='fa fa-cloud-download' aria-hidden='true' style='color: #007bff;font-size: xx-large;cursor: pointer;' onclick='printLote("+item.IDLOTEIMPRESION+");'></i>\
                        <a class='tooltipZipMake' id='Zip-loteMake"+item.IDLOTEIMPRESION+"' onclick='makeZip("+item.IDLOTEIMPRESION+")'><i class='fa fa-gear' style='color: #007bff;font-size: xx-large;'></i></a>";
            }

            // item.URL = "<a href='../../../soportecambios/"+item.URL+"' download><i class='fa fa-cloud-download' aria-hidden='true' style='color: #007bff;font-size: xx-large;cursor: pointer;'></i></a>";
            item.FECHACIERRE = (item.FECHACIERRE != null) ? item.FECHACIERRE : '';
            item.IMPRESIONES = (item.IMPRESIONES != null) ? item.IMPRESIONES : '';
            item.REIMPRESIONES = (item.REIMPRESIONES != null) ? item.REIMPRESIONES : '';
            item.TOTAL = (item.TOTAL != null) ? item.TOTAL : '';

            cadena += "<tr style='font-size:11px;' id='rowlote"+item.IDLOTEIMPRESION+"'><a>\
                        <th>"+(i+1)+"</th>\
                        <td>\
                            <a href='#!'>\
                                <span class='click' data-toggle='tooltip' data-html='true' data-placement='right' title='<a href=\""+'#'+"\" target=\"_blank\">CONSULTA FOLIOCUIS</a> <br>\
                                <a href=\""+'#'+"\" target=\"_blank\">CONSULTA LOTE IMPRESION</a>' >"+item.IDLOTEIMPRESION+"</span>\
                            </a>\
                        </td>\
                        <td>"+item.FECHAAPERTURA+"</td>\
                        <td>"+item.FECHACIERRE+"</td>\
                        <td>"+item.IMPRESIONES+"</td>\
                        <td>"+item.REIMPRESIONES+"</td>\
                        <td>"+item.TOTAL+"</td>\
                        <td id='opcionesLote"+item.IDLOTEIMPRESION+"'>\
                            "+links+"\
                        </td>\
                    </tr>";
        });

        cadena +="</tbody>\
                    </table>";
                    
        $('#resultadosTablaLotes').html(cadena);
        tablePadron('#tablaLotes','LOTES',8)
        settingsTable();
        $('.tooltipZip').tooltip({title:'DESCARGAR ZIP'});
        $('.tooltipZipReload').tooltip({title:'VOLVER A GENERAR'});
        $('.tooltipZipMake').tooltip({title:'GENERAR ZIP'});
        $('#Zip-loteFile'+lote).hide();
        $('#Zip-loteReload'+lote).hide();
        $('#Zip-loteMake'+lote).hide();
        load(false);
        
        // $('.click').tooltip({trigger: "click"}); 
        // $('[data-toggle="tooltip"]').tooltip();
    }

    function printLote(cveLote){
        let METODO = 'POST';
        let objectSend = {
            option : 'printLote',
            cveLote : cveLote
        }

        let request = petitionSupport(URL, METODO, objectSend);
        
        request.done(function(res){
            // if(res)
            //     alert('Archivo Generado, Reviese su correo');
            // let response = JSON.parse(res);

            // if(response != undefined && Object.keys(response).length > 0 ){                
            //     createFileCsv(response);         
            // }
        });

        request.fail(function(XHR, textStatus){
            console.log(textStatus);
        });
    }

    function makeZip(lote) {
        $('#Zip-loteFile'+lote).tooltip('hide');
        $('#Zip-loteReload'+lote).tooltip('hide');
        $('#Zip-loteMake'+lote).tooltip('hide');
        var urlController= './controller/CreateZipController.php';
        var user='<?php echo $_SESSION['IDUSUARIO'] ?>';
        var object={csrf_token:'Token',opcion:'makeZip',lote:lote,user:user};
        var res;
        var cadena="";
        $('#Zip-loteReload'+lote).find( 'i' ).addClass('fa-spin');
        $('#Zip-loteMake'+lote).find( 'i' ).addClass('fa-spin');
        notificar('AVISO! ESTO PUEDE TARDAR VARIOS MINUTOS','Se le notificará por correo electrónico cuando este listo el fichero. GRACIAS!','info','8000');
        $.post(urlController, object, function(data, textStatus, xhr) {
            /*optional stuff to do after success */
            res=$.parseJSON(data);
            console.log(res);
            if (res==true||res=='true') {
               console.log(res);
                $('#Zip-loteReload'+lote).find( 'i' ).removeClass('fa-spin');
                $('#Zip-loteMake'+lote).find( 'i' ).removeClass('fa-spin');
                $('#Zip-loteMake'+lote).hide();
                $('#Zip-loteFile'+lote).show('slow');
                $('#Zip-loteReload'+lote).show('slow');
                cadena="<i class='fa fa-cloud-download' aria-hidden='true' style='color: #007bff;font-size: xx-large;cursor: pointer;' onclick='printLote("+lote+");'></i>\
                        <a href='../../../../MEDIA/imagenes_impresion_credenciales/lote_"+lote+".zip' class='tooltipZip' id='Zip-loteFile"+lote+"' download><i class='fa fa-file-zip-o' style='color: #007bff;font-size: xx-large;'></i></a>\
                        <a  class='tooltipZipReload' id='Zip-loteReload"+lote+"' onclick='makeZip("+lote+")'><i class='fa fa-refresh' style='color: #007bff;font-size: xx-large;'></i></a>";
                $('#opcionesLote'+lote).html(cadena);
                $('.tooltipZip').tooltip({title:'DESCARGAR ZIP'});
                $('.tooltipZipReload').tooltip({title:'VOLVER A GENERAR'});
                $('.tooltipZipMake').tooltip({title:'GENERAR ZIP'});
            }else{
                cadena="<i class='fa fa-cloud-download' aria-hidden='true' style='color: #007bff;font-size: xx-large;cursor: pointer;' onclick='printLote("+lote+");'></i>\
                        <a href='#!'>"+res.DATOS+"</a>";
                $('#opcionesLote'+lote).html(cadena);
            }
            //$('#Zip-lote'+lote).show();
        });
    }

    function fileExist(lote) {
        var urlController= './controller/CreateZipController.php';
        var object={csrf_token:'Token',opcion:'fileExist',lote:lote};
        var res= $.parseJSON(ajaxCallback(object,urlController));
        return res;
        console.log(res);
        
    }


    // function createFileCsv(response) {
    //     //comprobamos compatibilidad
    //     if(window.Blob && (window.URL || window.webkitURL)){
    //         var contenido = "",
    //             d = new Date(),
    //             blob,
    //             reader,
    //             save,
    //             clicEvent;
            
        
    //         Object.keys(response[0]).forEach(function(key){
    //             contenido += key+',';
    //         });

    //         contenido = contenido.slice(0, -1) + "\n";
            
    //         response.forEach(function(row){
    //             Object.keys(row).forEach(function(itemValue){                       
    //                 contenido += row[itemValue] + ',';
    //             });
    //             contenido += contenido.split(',').pop() + "\n";
    //         });   

            
    //         //Creamos el blob
    //         blob =  new Blob(["\ufeff", contenido], {type: 'text/csv'});
            

    //         var reader = new FileReader();
    //         reader.onload = function (event) {
                                
    //             //Escuchamos su evento load y creamos un enlace en dom
    //             save = document.createElement('a');
    //             save.href = event.target.result;
    //             save.target = '_blank';

                
    //             save.download = "LoteImpresion_"+ d.getFullYear() + (d.getMonth()+1) + d.getDate() + d.getHours() + d.getMinutes() + d.getSeconds() + ".csv";

    //             try {
    //                 //creamos un evento click
    //                 clicEvent = new MouseEvent('click', {
    //                     'view': window,
    //                     'bubbles': true,
    //                     'cancelable': true
    //                 });
    //             } catch (e) {
    //                 //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
    //                 clicEvent = document.createEvent("MouseEvent");
    //                 clicEvent.initEvent('click', true, true);
    //             }
                
    //             save.dispatchEvent(clicEvent);
                
    //             // (window.URL || window.webkitURL).revokeObjectURL(save.href);
    //         }
            
    //         //Leemos como url
    //         reader.readAsDataURL(blob);
    //     }else {
    //         //Crear csv a la antiguita
    //         var csv = '';
    //         Object.keys(response[0]).forEach(function(key){
    //             csv += key+',';
    //         });

    //         csv = csv.slice(0, -1) + "\n";
            
    //         response.forEach(function(row){
    //             Object.keys(row).forEach(function(itemValue){                       
    //                 csv += row[itemValue] + ',';
    //             });
    //             csv += csv.split(',').pop() + "\n";
    //         });                           

    //         var hiddenElement = document.createElement('a');
    //         hiddenElement.href = 'data:text/csv;CHARSET=UTF-8,' + decodeURI(csv);
    //         hiddenElement.target = '_blank';
    //         hiddenElement.download = 'LotesImpresion.csv';
    //         hiddenElement.click();
    //     }
    // }
 
</script>