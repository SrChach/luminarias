<style type="text/css">
  .imgBlackWhite{
    -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    filter: grayscale(100%);
  }
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col">
			<div class="row" id='contentProgramas'></div>
		</div>
	</div>
</div>

<script type="text/javascript">
 var _t = '<?php echo $csrf_token ?>';
 var existe=null;

	$(function () {
		var url="./controller/CredencialesController.php"
    	objet={opcion:'Credenciales',action: 'CredentialedPrograms', csrf_token: _t};
    	ajaxCallback(objet,url,function (respuesta){
          try{
              res = JSON.parse(respuesta);
              if (res.CODIGO==true) {
                $.each(res.DATOS.programas, function(i, item) {
                  if (typeof res.DATOS.programasCredencial[item.IDPROGRAMA]!=='undefined') {
                      existe = res.DATOS.programasCredencial[item.IDPROGRAMA];
                  }
                  $('#contentProgramas').append(Render(item,existe));
                });
              }
              else{
                $('#titlePage-2').html(': ERROR !');
                showAlertInwindow('#contentProgramas','warning',res.DATOS);
              }
          }
          catch(err) {
              $('#titlePage-2').html(': ERROR !');
              showAlertInwindow('#contentProgramas','warning',err.message);
              console.log(err);
              $("#modalCargaPagina").modal('hide');
          }
    	});
  });

  	function Render(item,existe) {
      var style="";
      var styleImg="";
      var credenciales=0;
      var classe="";
      
      if (existe===null) {
        styleImg="imgBlackWhite";
        classe="onclick='return false'";
      }else{
        credenciales =  existe[0].CREDENCIALES;
      }
  		
  		if (item.IDPROGRAMA==2) {
  			style=" backCardsCredencialesM";
  		}else{
  			style=" backCardsCredenciales";
  		}
  		return "<div class='col-md-4 col-sm-6 col-xs-2' "+style+">\
        <a href='?view=modules&index=2&idP="+item.IDPROGRAMA+"' "+classe+">\
          <div class='card hover "+style+"'>\
            <li class='list-group-item'>\
              <img class='card-img-top img-fluid "+styleImg+"' src='./Storage/Evidencias/imgPrograms/PNG-CUADRADO/"+item.IMAGEN_CUADRADA+"' alt='Card image cap'>\
            </li>\
            <div class='card-body'>\
              <h5 class='card-title'>"+item.NOMBREPROGRAMA+"</h5>\
              <p><blockquote class='blockquote mb-0'>\
                <footer class='blockquote-footer'>CREDENCIALES: <cite title='Source Title'>"+separa(credenciales)+"</cite></footer>\
              </blockquote></p>\
            </div>\
          </div>\
        </a>\
      </div>";
  	}
</script>