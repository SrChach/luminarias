
<link rel="stylesheet" type="text/css" href="./assets/css/login.css">


<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-offset-4 col-md-4">
			<form class="form-group" action="javascript:startSession();">
				<?php if (!empty($imgFondoLogin) and empty($colorFondoLogin)): ?>
					<div class="col-md-12 boxS" style="background-image: url(<?php echo $imgFondoLogin ?>);background-repeat:non-repeat;background-size: cover; height: 100%" >
				<?php endif ?>
				<?php if (empty($imgFondoLogin) and !empty($colorFondoLogin)): ?>
					<div class="col-md-12 boxS" style="background-color: <?php echo $colorFondoLogin ?>;height: 100%" >
				<?php endif ?>
				
					
					<h3 align="center"  style="padding-top: 55%">INICIO DE SESIÓN</h3>
					<div class="col-md-12">
						<div class="aler alert-danger" id="access-denied"></div>
					</div>
					<hr>
					<div class="col-md-12" align="center" >
						<div class="col-md-offset-1 col-md-10">
							<div class="input-group">
								<label class="input-group-addon login-box-icon"><i class="fa fa-user" aria-hidden="true"></i></label>
                           		<input type="text" name="usuario" id="user" class="form-control" placeholder="Usuario: " required="true">
                        	</div>
						</div>
					</div>
					<div class="col-md-12" align="center">
					<br>
						<div class="col-md-offset-1 col-md-10">
							<div class="input-group">
								<label class="input-group-addon login-box-icon"><i class="fa fa-lock" aria-hidden="true"></i></label>
                            	<input type="password" name="psw" id="psw" class="form-control" placeholder="Contraseña:" required="true">
                        	</div>
                        </div>
					</div>
					<div class="col-md-12" align="center">
					<br>
						<div class="col-md-offset-3 col-md-6">
							<input type="submit"  class="form-control boxS" value="INGRESAR" required="true" style="background-color: #276092; color: white;">
						</div>
					</div>
					<div class="col-md-12" align="center">
					<br>
						<div class="col-md-offset-3 col-md-6" id="cargando" style="display: none;">
							<img src='<?php echo $gifLoad ?>' width='60px'>
						</div>
					</div>
					<hr>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>





<script type="text/javascript">
var data;
var res;
var url="./controller/MethodController.php";
	function startSession() {
		$('#cargando').show();
		data={opcion:'session',csrf_token:'<?php echo $csrf_token ?>',c:$('#user').val(),k:$('#psw').val()};
		ajaxCallback(data,url,function (respuesta){
            res = JSON.parse(respuesta);
            if (res.CODIGO==1 && res.DATOS==true) {
            	location.href = "?view=inicio";
            }else{
            	$('#access-denied').html(res.DATOS);
            }
            $('#cargando').hide();
    	});
		
	}
	
</script>