<?php
    if (isset($_SESSION['_TOKEN'])) {
        header('Location: ./?view=inicio');
    }
?>
<div class="row">
    <div class="col-md-12 text-center titleIndex fixed-top" style="background-color:#048037">
        <h2><?php echo $system; ?></h2>
    </div>
    <div class="col-md-12">
        <div class="offset-md-2 col-md-8">
            <div class="row">
                <div class="col-md-6 col-sm-3 col-xs-3">
                    <!--img src="./assets/img/logos-inferior.png" style="position: absolute;left: 0;bottom: 0; height:200px; width:200px;">
                    <img src="./assets/img/logo-censo.png" style="position: absolute;right: 0;bottom: 0; height:200px; width:200px;"-->
                </div>
                <div class="col-md-6 col-sm-9 col-xs-9" style="margin-top:30%">
                    <!--img src="./assets/img/logo-home.png" class="img-fluid"-->
                <div class="offset-md-1 col-md-10 login mb-5 mt-3">
                    <div class="col-md-12 text-center" style="font-family: Montserrat-Bold;">
                        <h3 style="padding-top: 5%; letter-spacing: 0px;" >INICIO DE SESIÓN</h3>
                        <!--img src="./assets/img/icon-user.png" class="img-fluid" width="30%"-->
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-danger" id="access-denied" style="display: none;">
                        </div>
                    </div>
                    <div class="offset-md-3 col-md-6" id="cargando" style="display: none;" align="center">
                        <img src='<?php echo $gifLoad ?>' width='60px'>
                    </div>
                    <!--div class="row" style="padding-top: 10%;">
                        <div class="offset-md-3 col-md-6" id="cargando" style="display: none;" align="center">
                            <img src='<?php echo $gifLoad ?>' width='60px'>
                        </div>
                    </div-->
                    <div class="col-md-12" style="padding-top: 0px;margin-bottom: 20px;">
                        <form action="javascript:startSession();">
                            <div class="row" style="padding-top: 10%;">
                                <div class="col" align="center">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                        <input class="form-control inputLogin" type="text" id='user' placeholder="Usuario :" required="required" autofocus="true" onkeypress="return check(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10%;">
                                <div class="col" align="center">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                        </span>
                                        <input class="form-control inputLogin" type="password" id='psw' placeholder="Contraseña:" required="required" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10%;">
                                <div class="col" align="center">
                                    <div class="input-group">
                                        <button type="submit" class="btn-session">INICIAR</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/div-->
<?php
    if (isset($_GET['c'])) {
        $completeUrl="?view=cedula&act=cedulaImage&certificado=".$_GET['c'];
    }else{
        $completeUrl="?view=inicio";
    }
?>
<script type="text/javascript">
var data;
var res;
var url="./controller/MethodController.php";
	function startSession() {
        $('#access-denied').hide('slow');
		$('#cargando').show();
		data={opcion:'session',csrf_token:'<?php echo $csrf_token ?>',c:$('#user').val(),k:$('#psw').val()};
		ajaxCallback(data,url,function (respuesta){
            res = JSON.parse(respuesta);
            if (res.CODIGO==true && res.DATOS==true) {
                if(res.CREDENTIAL==3 || res.CREDENTIAL==4 ){
                    location.href = '?view=reporte&act=teams';
                }else if(res.CREDENTIAL==11 || res.CREDENTIAL==12 || res.CREDENTIAL==13){
                    if(res.VERIFICADO==0){
                        location.href = '?view=users&act=verificar';
                    }else{
                        location.href = '?view=inicio';
                    }
                }
                else{
                    location.href = '<?php echo $completeUrl ?>';
                }
            }else{
                $('#access-denied').show('slow');
            	$('#access-denied').html(res.DATOS);
                setTimeout("$('#access-denied').hide('slow')",2500);
            }
            $('#cargando').hide();
    	});

	}

    function check(e) {
        var tecla = (document.all) ? e.keyCode : e.which;
        //Tecla permitidas (-,_,@,#,.,borrar,adelante/atras,space)
        if (tecla == 8 ||  tecla == 35 || tecla == 45 || tecla == 46 || tecla == 64 || tecla == 95 || tecla == 0 || tecla == 32) {
            return true;
        }
        // Patron de entrada, en este caso solo acepta numeros y letras
        var patron = /[A-Za-z0-9]/;
        var tecla_final = String.fromCharCode(tecla);
        if (patron.test(tecla_final)) {
            $('#access-denied').html('');
            $('#access-denied').hide('slow');
        }else{
            $('#access-denied').html('LO SIENTO! CARACTER NO VALIDO.');
            $('#access-denied').show('slow');
        }
        return patron.test(tecla_final);
    }

    $(function () {
         /*var myInput = document.getElementById('user');
            myInput.onpaste = function(e) {
            e.preventDefault();
            $('#access-denied').html('LO SIENTO! ACCIÓN NO PERMITIDA EN ESTE CAMPO.');
            $('#access-denied').show('slow');
            setTimeout("$('#access-denied').hide('slow')",2500);
        }*/
    });

</script>
