<style type="text/css">
    .centra{text-align: center;}.izquierda{text-align: left;}.centro{align:center;}

    .sombra_y_borde{
        border:solid; border-radius: 15px 15px 15px 15px;
        box-shadow: 6px 6px 3px 3px #505160;
        margin-top: 1em;
        margin-bottom: 1em;
      }
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
<div class="container-fluid">
            <div class="alert alert-success alert-dismissible fade show" role="alert" id='save' style="display:none;">
              <strong>Cambios Guardados</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <form method="POST" action="./controller/GestionController.php" id="formulario">
                    <input type="hidden" name="idExpediente" id="idExpediente">

                    <div class="row mtop2 ">
                        <div class="col-md-2 offset-md-5">
                            <button class="form-control btn btn-success" onclick="guardar_datos_val(0)">GUARDAR</button>
                        </div>
                    </div>
                    <div class="row mtop2">
                        <div class="col-md-2 offset-md-5" >
                            <input type='button' onclick="menosImagenes()" class='form-control btn-info' value="Anterior"></input>
                            <!--button class="form-control btn btn-success" >Mas</button-->
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" name="opcion" id="opcion" value="guardar_datos_val">
                        <input type="hidden" name="cuantos" id="cuantos">
                        <div class="col-md-12" id="resultados">

                            <!-- Resultados -->
                        </div>
                        <div class="col-md-3 centra" id=""></div>
                        <div class="col-md-6 centra" id="">
                            <h4>Observacion General</h4>
                            <textarea class="form-control" name="obs_gen" id = "obs_gen" rows="8" id="observacion_general"></textarea>
                        </div>
                    </div>
                    <div class="row mtop2">
                        <div class="col-md-2 offset-md-5 mx-auto" >
                            <input type='button' onclick="masImagenes()" class='form-control btn-info' value="Mas"></input>
                            <!--button class="form-control btn btn-success" >Mas</button-->
                        </div>
                    </div>

                    <!--div class="row mtop2">
                        <div class="col-md-2 col-md-offset-5 centra">
                            <button class="form-control btn btn-success" onclick="guardar()">GUARDAR</button>
                        </div>
                    </div-->
                </form>
            </div>
        </div>

        <div id="footer">
            <div class="container-fluid">
                <p class="text-muted">Copyright &copy; Todos los derechos reservados</p>
            </div>
        </div>





<script type="text/javascript">
    var urll = "./controller/ControladorGestion.php";
    var imagenes = [];
    var tiposDocumento = [];
    var tipoDocumentoEspecifico = [];
    var preguntas = [];
    var respuestas = [];
    //var urlimagenes = "http://173.192.101.210:9043/desarrollo_des/pillar/clases/imagen.php?idImagen=";
    var urlimagenes = "../../pillar/clases/imagen.php?idImagen=";


    $(document).ready(function (){

        var idObra_folio_cuis = 1;
        var tipoImagen = 0;
        var oculta = 0;

        var url_string = window.location.href;
        var url = new URL(url_string);

        var foliocuis = checaValorDefault( url.searchParams.get("foliocuis") );
        var lote = checaValorDefault( url.searchParams.get("lote") );
        var c_persona = checaValorDefault( url.searchParams.get("c_persona") );
        var idListado = checaValorDefault( url.searchParams.get("idListado") );


        data = {opcion: "imagenes",idObra_folio_cuis: idObra_folio_cuis,tipoImagen: tipoImagen,oculta:oculta,
                    foliocuis:foliocuis,lote:lote,c_persona:c_persona,idListado:idListado}

        var res = ajax(data,urll);
        var respuesta = JSON.parse(res);


        imagenes = respuesta['imagenes'];
        tiposDocumento = respuesta['tiposDocumento'];
        tipoDocumentoEspecifico = respuesta['tiposDocumentoEspecifico'];
        preguntas = respuesta['preguntas'];
        respuestas = respuesta['respuestas'];

        pintaImagenes();
    });

    function pintaImagenes(){
        indiceAux = 0;
        <?php
        if( !isset($_GET['indexG'] ) ){
            $_GET['indexG'] = 0;
        }
        ?>

        var idG = parent.location.hash;
        indiceGen = Number( idG.replace("#","") );
        var campos = "";
        console.log(imagenes);

        for (var i = indiceGen; i < imagenes.length; i++) {
            var idImagen = imagenes[i]['IDIMAGEN'];
            var idTipoImagen = imagenes[i]['IDTIPOIMAGEN'];
            var idTipoDocumentoEspecifico = imagenes[i]['IDTIPODOCUMENTO'];



            var preg = pintaPreguntas(idImagen,idTipoDocumentoEspecifico,preguntas,respuestas);

            var carga = "";

            var idVisita = imagenes[i]['IDVISITA'];
            if(idVisita != null){
                var origen = "Campo";
            }else{
                var origen = "Digitalizacion";
            }

            var url = imagenes[i]['URL'];
            var fechaCreacion = imagenes[i]['FECHACREACION'];
            var fechaValidacion = imagenes[i]['FECHAVALIDACION'];
            var nombreDocumento = imagenes[i]['NOMBRE'];
            var extrasGenerales = imagenes[i]['EXTRASGENERALES'];
            var extrasEspecificos = imagenes[i]['EXTRASESPECIFICOS'];
            var v_imagen = imagenes[i]['V_IMAGEN'];
            var docPAdre = imagenes[i]['DOCPADRE'];
            var ayuda = imagenes[i]['AYUDA'];
            var version = imagenes[i]['DIA'];
            var folio_cuis_imagen = imagenes[i]['FOLIO_CUIS'];
            var observacion = checaValorDefault(imagenes[i]['OBSERVACION']);

            var validacion = imagenes[i]['V_IMAGEN'];
            var puntosNoAprovados = imagenes[i]['NO_APROBADOS_EXTRAS'];


            // Select del tipo Documento General
            var options = "<option value = ''>SELECCIONE</option>";
            options += contruyeOpcion_separador(tiposDocumento,idTipoImagen);
            var selectTipoImagen = '<select name="tipoImagen_'+idImagen+'" id="tipoImagen_'+idImagen+'" class="form-control" onchange="cambio(this); traeDocsEsp(this.id);">'+options+'</select>'

            // ///////////////////////////////////

            // Select del tipo Documento Especifico

            var options = "";
            var selectTipoDocumentoEspecifico = "";

            if(idTipoImagen != 0){
                var options = "";
                var bloc = "";
                if(tipoDocumentoEspecifico[idTipoImagen] != null){
                    if(tipoDocumentoEspecifico[idTipoImagen].length>1){
                        options += "<option value = ''>SELECCIONE</option>";
                    }else{
                        //bloc = "disabled";
                        bloc = "";
                    }
                    if(idTipoDocumentoEspecifico!=0){

                    }
                    options += contruyeOpcion(tipoDocumentoEspecifico[idTipoImagen],idTipoDocumentoEspecifico);
                }
                //alert(options + " 2");
                var selectTipoDocumentoEspecifico = '<select '+bloc+' name="tipoDocumentoEspecifico_'+idImagen+'" id="tipoDocumentoEspecifico_'+idImagen+'" class="form-control bloqueo" \
                onchange="cambio(this);\
                $(\'#extras_\'+extraeID(this.id)).html(pintaPreguntas(extraeID(this.id)  ,this.value,preguntas,respuestas)); \
                pintaCamposChange(this.id)">\
                '+options+'</select>'
            }
            // ////////////////////////////////////
            var checked = "";
            if(docPAdre==1 || docPAdre == null){
                checked = "checked";
            }

            var iconoValidacion = "";
            if(validacion == 0){
              iconoValidacion = '<i class="fa fa-question-circle fa-3x" aria-hidden="true" style="color:orange"></i>';
          }else if(validacion == 1){
              iconoValidacion = '<i class="fa fa-check-square fa-3x" aria-hidden="true" style="color:green"></i>';
          }else if(validacion == 2){
              iconoValidacion = '<i class="fa fa-refresh fa-3x" aria-hidden="true" style="color:red"></i>';
          }

          var marco = "";
          if(idTipoImagen != 10000){
            marco = "<img class=''  src='"+urlimagenes+imagenes[i]['IDIMAGEN']+"'  style='max-width:100%; max-height:500px'>"
        }

        carga += '<div class="row" style="margin-top:0.5em">\
        <div class="col-md-4 offset-md-1" >\
        <div class="row sombra_y_borde" style="height:500px; overflow-y:auto;"  >\
        <div class="col-md-12 letra_10">\
        <div class="row">\
        <!--div class="col-md-1">\
        <h4><a href="#" data-toggle="tooltip" title="'+ayuda+'"><i class="fa fa-question-circle" aria-hidden="true"></i></a></h4>\
        </div-->\
        <div class="col-md-5">\
        '+selectTipoImagen+'\
        <input type="hidden" name="cambio_'+idImagen+'" id="cambio_'+idImagen+'" value = "0">\
        <input type="hidden" name="campos_'+idImagen+'" id="campos_'+idImagen+'" value = "'+campos+'">\
        </div>\
        <div class="col-md-5" id="div_select_especifico_'+idImagen+'">\
        '+selectTipoDocumentoEspecifico+'\
        </div>\
        <div class="col-md-2" id="div_select_especifico_'+idImagen+'">\
        Padre:<input type="checkbox" value="1" name="docpadre_'+idImagen+'" id="docpadre_'+idImagen+'" onchange="cambio(this)" '+checked+'>\
        </div>\
        </div>\
        <!--div class="row">\
        <div class="col-md-1">\
        <h4><a href="#" data-toggle="tooltip" title="'+ayuda+'"><i class="fa fa-question-circle" aria-hidden="true"></i></a></h4>\
        </div>\
        <div class="col-md-8">\
        '+selectTipoDocumentoEspecifico+'\
        </div>\
        </div-->\
        <div id="extras_'+idImagen+'">\
        '+preg+'\
        </div>\
        <div class="row centra mtop1">\
        <div class="col-md-1">\
        <a href="#" data-toggle="tooltip" title="'+puntosNoAprovados+'">'+iconoValidacion+'</a>\
        </div>\
        <div class="col-md-8">\
        <textarea name="observacion_'+idImagen+'"  id="observacion_'+idImagen+'" class="form-control letra_10" placeholder="OBSERVACION" onchange="cambio(this)">'+observacion+'\</textarea>\
        </div>\
        <div class="col-md-3">\
        <input type="text" class="form-control" name="version_'+idImagen+'"  id="version_'+idImagen+'" onchange="cambio(this)" value = '+version+'>\
        Version del Documento\
        </div>\
        </div>\
        <div class="row centra mbottom_1" >\
        <div class="col-md-12">Datos De la imagen</div>\
        </div>\
        <div class="row mbottom_1" >\
        <div class="col-md-6">\
        <div class="row mbottom_1" >\
        <!--div class="col-md-6">Fecha Creacion:</div>\
        <div class="col-md-6">'+fechaCreacion+'</div-->\
        <div class="col-md-6">Folio Cuis:</div>\
        <div class="col-md-6">'+folio_cuis_imagen+'</div>\
        </div>\
        <div class="row mbottom_1" >\
        <div class="col-md-6">Origen:</div>\
        <div class="col-md-6">'+origen+'</div>\
        </div>\
        </div>\
        <div class="col-md-6">\
        <div class="row mbottom_1" >\
        <h5><div class="col-md-6">Consecutivo:</div>\
        <div class="col-md-6">'+ Number(indiceGen + 1)  + '/'+imagenes.length+'</div>\
        <input type="button" value="Refresca"  \
        onclick="refresca_doc(\''+imagenes[i]['FOLIO']+'\',\''+imagenes[i]['URL']+'\','+idImagen+')">\
        </h5>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        <div class="col-md-5 offset-md-1 sombra_y_bordee">\
            <div class="row">\
                <div class="col-md-12 centra sombra_y_borde" style="height:500px" id="marco_'+idImagen+'">\
                '+marco+'\
                </div>\
            </div>\
        <!--div class="row">\
            <div class="col-md-6">\
                <button onclick="gira(\'zoom_'+i+'\',90)" type="button">gira <i class="fa fa-undo" aria-hidden="true"></i> </button>\
            </div>\
            <div class="col-md-6">\
                <input onclick="gira(\'zoom_'+i+'\',-90)" type="button">gira <i class="fa fa-repeat" aria-hidden="true"></i> </button>\
            </div>\
        </div>\
        </div-->\
        </div>';

            //<img id="zoom_'+i+'" class="centra zoom" src="'+urlimagenes+imagenes[i]['URL']+'" data-zoom-image="'+urlimagenes+imagenes[i]['URL']+'" style="max-width:100%; max-height:500px;" border="1" >\
            $("#resultados").append(carga);
            pintaCamposChange("cambio_" + idImagen,imagenes[i]);

            indiceGen++;
            indiceAux++;

            if(indiceAux == 10){ //Cuantas imagenes pinta a la vez
                break;
            }


        }
        //$("#save").fadeOut();
        topFunction();
    }

    function pintaPreguntas(idImagen,idTipoImagen,preguntas,respuestas){
        var preg = "";
        if (typeof (preguntas[idTipoImagen]) !== 'undefined') {
            for (var j = 0; j < preguntas[idTipoImagen].length; j++) {
                var res = "";
                if(respuestas != null){
                    if( typeof ( respuestas[idImagen] ) !== 'undefined' ){
                        if(typeof(preguntas[idTipoImagen][j]['TIPOPREGUNTA']) !== 'undefined'){
                            var idPregunta = preguntas[idTipoImagen][j]['IDPREGUNTA'];
                            if( typeof(respuestas[idImagen][idPregunta] ) !== 'undefined' ){
                                res = respuestas[idImagen][idPregunta][0]['RESPUESTA'];
                            }
                        }
                    }
                }
                var preg_a = pintaPregunta( idImagen, preguntas[idTipoImagen][j],res);
                preg += preg_a;
            }
        }else{

        }
        return preg;
    }


    function pintaPregunta(idImagen,pregunta,respuesta){
        //alert(pregunta['IDPREGUNTA'] +" D:_3" + idImagen);
        var input = "";
        if(pregunta['TIPOPREGUNTA'] == 1){
            input = construyeSelectExtraEspecifico(idImagen,pregunta,"SI/NO",respuesta);
        }else if(pregunta['TIPOPREGUNTA'] == 2){
            input = construyeSelectExtraEspecifico(idImagen,pregunta,"SiNoTiene",respuesta);
        }else if(pregunta['TIPOPREGUNTA'] == 3 || pregunta['TIPOPREGUNTA'] == 4){
            input = construyeInputExtraEspecifico(idImagen,pregunta,respuesta);
        }else if(pregunta['TIPOPREGUNTA'] == 5){
            input = construyefecha(idImagen,pregunta,respuesta);
        }else if(pregunta['TIPOPREGUNTA'] == 9){
            input = construyeSelectPersonalizado(idImagen,pregunta,respuesta);
        }
        return input;
    }

    function pintaCamposChange(id,respuestas = null){ /********************/
        var id = extraeID(id);

        var idTipoImagen = $("#tipoImagen_"+id).val();
        var idTipoDocumentoEspecifico = $("#tipoDocumentoEspecifico_"+id).val();

        var cadenaGenerales = "";
        var cadenaEspecificos = "";

        if(tipoDocumentoEspecifico[idTipoImagen]!=null){
            var tiposDoc = tipoDocumentoEspecifico[idTipoImagen];
            for( var i = 0; i < tiposDoc.length; i++){
                if(tiposDoc[i]['VALUE'] == idTipoDocumentoEspecifico){
                    cadenaGenerales = tiposDoc[i]['EXTRASGENERALES'];
                    cadenaEspecificos = tiposDoc[i]['EXTRASESPECIFICOS'];
                    break;
                }
            }

        }
        //var extrasEspecificos = pintaExtras_2(id,cadenaGenerales,cadenaEspecificos,respuestas);
        //$("#extras_"+id).html(extrasEspecificos);
    }




    function construyeSelectExtraEspecifico(idImagen,pregunta,options,opcionElegida){
        var resSiNo = [];resSiNo[0] = [];resSiNo[1] = [];

        resSiNo[0]['VALUE'] = 'SI';resSiNo[0]['NOMBRE'] = 'SI';resSiNo[1]['VALUE'] = 'NO';resSiNo[1]['NOMBRE'] = 'NO';

        var optionsSINO = "<option value=''>SELECCIONE</option>";
        optionsSINO += contruyeOpcion(resSiNo,opcionElegida);

        var resSiNoTiene = [];resSiNoTiene[0] = [];resSiNoTiene[1] = [];resSiNoTiene[2] = [];
        resSiNoTiene[0]['VALUE'] = 'SI';resSiNoTiene[0]['NOMBRE'] = 'SI';
        resSiNoTiene[1]['VALUE'] = 'NO';resSiNoTiene[1]['NOMBRE'] = 'NO';
        resSiNoTiene[2]['VALUE'] = 'NO TIENE';resSiNoTiene[2]['NOMBRE'] = 'NO TIENE';

        var optionsResSiNoTiene = "<option value=''>SELECCIONE</option>";
        optionsResSiNoTiene += contruyeOpcion(resSiNoTiene,opcionElegida);

        var optionsFinal = "";
        if(options == "SI/NO"){
            optionsFinal = optionsSINO;
        }else if(options == "tipoObra"){
            optionsFinal = optionsTipoObra;
        }else if(options == "SiNoTiene"){
            optionsFinal = optionsResSiNoTiene;
        }

        var select = '<div class="row mtop1">\
        <div class="col-md-6 ">'+pregunta['PREGUNTA']+'</div>\
        <div class="col-md-6">\
        <select class="form-control letra_10" name="val_'+idImagen+'_'+pregunta["IDPREGUNTA"]+'" \
        id="val_'+idImagen+'_'+pregunta["IDPREGUNTA"]+'" onchange="cambio(this)" required>\
        '+optionsFinal+'\
        </select>\
        </div>\
        </div>';
        return select;
    }

    function cambio(campo){
        var id = extraeID(campo.id);
        $("#cambio_"+id).val("1");
    }


    function construyeInputExtraEspecifico(idImagen,pregunta,respuesta){

        //console.log(pregunta);
        respuesta = checaValorDefault(respuesta);
        var block = "";
        if(pregunta['IDPREGUNTA'] == 10 || pregunta['IDPREGUNTA'] == 11 || pregunta['IDPREGUNTA'] == 12){
            block = "readonly";
        }
        jefes_folio = []; /*********************/
        folios = []; /*********************/

        /*if(typeof (jefes_folio[ folios[idImagen][0]['FOLIO_CUIS'] ])!=='undefined'){
            if(pregunta['IDPREGUNTA'] == 10 && esDocBen(idImagen) ){
                block = "readonly";
                if(respuesta == ""){
                    respuesta = jefes_folio[ folios[idImagen][0]['FOLIO_CUIS'] ][0]['NB_NOMBRE'];
                }
            }

            if(pregunta['IDPREGUNTA'] == 11 && esDocBen(idImagen)){
                block = "readonly";
                if(respuesta == ""){
                    respuesta = jefes_folio[ folios[idImagen][0]['FOLIO_CUIS'] ][0]['NB_PRIMER_AP'];
                }
            }
            if(pregunta['IDPREGUNTA'] == 12 && esDocBen(idImagen)){
                block = "readonly";
                if(respuesta == ""){
                    respuesta = jefes_folio[ folios[idImagen][0]['FOLIO_CUIS'] ][0]['NB_SEGUNDO_AP'];
                }
            }
        } */
    }

    function construyefecha(idImagen,pregunta,respuesta){

        var res_fecha = respuesta.split('_');
        var respuesta_a = res_fecha[0];
        var respuesta_m = checaValorDefault(res_fecha[1]);
        var respuesta_s = checaValorDefault(res_fecha[2]);

        var select = '<div class="row mtop1">\
        <div class="col-md-3">'+pregunta['PREGUNTA']+'</div>\
        <div class="col-md-3">\
        <input type="text" class="form-control letra_10" name="fechaanio_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" \
        id="fechaanio_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" onchange="cambio(this);" placeholder="AAAA" \
        onkeyup="javascript:this.value=this.value.toUpperCase(); soloNumeros(this.id);" value="'+respuesta_a+'" maxlength="4" required>\
        </div>\
        <div class="col-md-3">\
        <input type="text" class="form-control letra_10" name="fechames_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" \
        id="fechames_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" onchange="cambio(this);" placeholder="MM" \
        onkeyup="javascript:this.value=this.value.toUpperCase(); soloNumeros(this.id);" value="'+respuesta_m+'" maxlength="2" required>\
        </div>\
        <div class="col-md-3">\
        <input type="text" class="form-control letra_10" name="fechadia_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" \
        id="fechaadia_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" onchange="cambio(this);" placeholder="DD" \
        onkeyup="javascript:this.value=this.value.toUpperCase(); soloNumeros(this.id);" value="'+respuesta_s+'" maxlength="2" required>\
        </div>\
        </div>';
        return select;
    }

    function construyeSelectPersonalizado(idImagen,pregunta,opcionElegida){
        var cadena_opciones = pregunta['VALORESCORRECTOS'];
        var rutinaExtra = "";
        var opciones = cadena_opciones.split("/");

        //alert(opciones[0]);

        //alert(opciones.length);

        //alert(opciones.toSource());

        options = [];
        for (var i = 0; i < opciones.length; i++) {
            //console.log(opciones[i].split("_")[0]);
            options[i] = [];
            options[i]['VALUE'] = opciones[i].split("_")[0];
            options[i]['NOMBRE'] = opciones[i].split("_")[0];
        }

        if(pregunta['IDPREGUNTA'] == 52){
            options[opciones.length] = [];
            options[opciones.length]['VALUE'] = 0;
            options[opciones.length]['NOMBRE'] = "EXTRANJERO";
        }

        //var folio_cuis = folios[idImagen][0]['FOLIO_CUIS']; *****
        var folios = [];
        var integrantes_folio = [];
        var folio_cuis = 0;

        if(pregunta['IDPREGUNTA'] == 35){
            var options = [];
            var inte = [];
            if(typeof (integrantes_folio[folio_cuis])!=='undefined'){
                inte = integrantes_folio[folio_cuis];
            }
            for (var i = 0; i < inte.length; i++) {
                options[i] = [];
                options[i]['VALUE'] = inte[i]['C_PERSONA'];
                options[i]['NOMBRE'] = inte[i]['NB_NOMBRE'] + " " + inte[i]['NB_PRIMER_AP'] + " " + inte[i]['NB_SEGUNDO_AP'] ;
            }
            options[inte.length] = [];
            options[inte.length]['VALUE'] = 0;
            options[inte.length]['NOMBRE'] = "OTRO";
            rutinaExtra = " cargaNombreIntegrante(this.value,"+idImagen+"); ";
        }


        var optionsFinal = "<option value=''>SELECCIONE</option>";
        if(pregunta['IDPREGUNTA'] == 52){

        }
        optionsFinal += contruyeOpcion(options,opcionElegida);


        var select = '<div class="row mtop1">\
        <div class="col-md-6 ">'+pregunta['PREGUNTA']+'</div>\
        <div class="col-md-6">\
        <select class="form-control letra_10" name="val_'+idImagen+'_'+pregunta["IDPREGUNTA"]+'" \
        id="val_'+idImagen+'_'+pregunta["IDPREGUNTA"]+'" onchange="cambio(this); '+rutinaExtra+'" required>\
        '+optionsFinal+'\
        </select>\
        </div>\
        </div>';


        return select;
    }

    function construyeInputExtraEspecifico(idImagen,pregunta,respuesta){

        //console.log(pregunta);
        respuesta = checaValorDefault(respuesta);
        var block = "";
        if(pregunta['IDPREGUNTA'] == 10 || pregunta['IDPREGUNTA'] == 11 || pregunta['IDPREGUNTA'] == 12){
            block = "readonly";
        }

        /*if(typeof (jefes_folio[ folios[idImagen][0]['FOLIO_CUIS'] ])!=='undefined'){
            if(pregunta['IDPREGUNTA'] == 10 && esDocBen(idImagen) ){
                block = "readonly";
                if(respuesta == ""){
                    respuesta = jefes_folio[ folios[idImagen][0]['FOLIO_CUIS'] ][0]['NB_NOMBRE'];
                }
            }

            if(pregunta['IDPREGUNTA'] == 11 && esDocBen(idImagen)){
                block = "readonly";
                if(respuesta == ""){
                    respuesta = jefes_folio[ folios[idImagen][0]['FOLIO_CUIS'] ][0]['NB_PRIMER_AP'];
                }
            }
            if(pregunta['IDPREGUNTA'] == 12 && esDocBen(idImagen)){
                block = "readonly";
                if(respuesta == ""){
                    respuesta = jefes_folio[ folios[idImagen][0]['FOLIO_CUIS'] ][0]['NB_SEGUNDO_AP'];
                }
            }
        }  */


        if(pregunta['IDPREGUNTA'] == 1){

            //respuesta = folios[idImagen][0]['FOLIO_CUIS']; // SE PRECARGA DE LAS RESPUESTAS EXTRAORDINARI


            rutinaExtra = "";
            if(pregunta['OPCIONES'] == "NUMERICO"){
                rutinaExtra = "soloNumeros(this.id);";
            }


            var largoCampo = "";
            if(pregunta['VALORESCORRECTOS'] != null && pregunta['VALORESCORRECTOS'] != "null"){
                largoCampo = 'maxlength="'+pregunta['VALORESCORRECTOS']+'"';
            }

            var select = '<div class="row mtop1">\
            <div class="col-md-4">'+pregunta['PREGUNTA']+'</div>\
            <div class="col-md-6">\
            <input type="text" class="form-control letra_10" name="val_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" \
            id="val_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" onchange="cambio(this);" \
            onchange="traeDatosCUIS(this.val)" \
            onkeyup="javascript:this.value=this.value.toUpperCase();'+rutinaExtra+'" \
            value="'+respuesta+'" '+largoCampo+' required>\
            </div>\
            <div class="col-md-1">\
            <input type="button" class="btn" value="Datos" id="datosCuis_'+idImagen+'" \
            onclick="traeDatosCUIS($(\'#val_'+idImagen+'_'+pregunta['IDPREGUNTA']+'\').val(),extraeID(this.id));"/>\
            </div>\
            </div>';
        }





        ////
        else if(pregunta['TIPOPREGUNTA'] != 4 && pregunta['TIPOPREGUNTA'] != 5){

            rutinaExtra = "";
            rutinaExtra_onchange = "";
            rutinaValidaOcr=""

            if (pregunta['IDPREGUNTA']==41 || pregunta['IDPREGUNTA']==39 ) {
                rutinaValidaOcr = "validaOcr(this.id);";
            }


            if(pregunta['OPCIONES'] == "NUMERICO"){
                rutinaExtra = "soloNumeros(this.id);";
            }else if(pregunta['OPCIONES'] == "CURP"){
                rutinaExtra_onchange = "curpValida(this.id);";
            }


            var largoCampo = "";
            if(pregunta['VALORESCORRECTOS'] != null && pregunta['VALORESCORRECTOS'] != "null"){
                largoCampo = 'maxlength="'+pregunta['VALORESCORRECTOS']+'"';
            }


            var select = '<div class="row mtop1">\
            <div class="col-md-4">'+pregunta['PREGUNTA']+'</div>\
            <div class="col-md-8">\
            <input type="text" class="form-control letra_10" name="val_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" \
            id="val_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" \
            onchange="cambio(this);'+rutinaExtra_onchange+rutinaValidaOcr+'" \
            onkeyup="javascript:this.value=this.value.toUpperCase();'+rutinaExtra+'" \
            value="'+respuesta+'" '+largoCampo+' '+block+'>\
            </div>\
            </div>';
        }
        ////
        else if(pregunta['TIPOPREGUNTA'] == 4 ){
            var select = '<div class="row mtop1">\
            <div class="col-md-3">NOMBRE:</div>\
            <div class="col-md-9">\
            <input class="form-control letra_10" name="nombreBen_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" id="nombreBen_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" type="text" placeholder="NOMBRE" value = "'+respuesta+'" onchange="cambio(this)" onkeyup="javascript:this.value=this.value.toUpperCase();">\
            </div>\
            </div>\
            <div class="row">\
            <div class="col-md-3">PATERNO:</div>\
            <div class="col-md-9">\
            <input class="form-control letra_10" name="apPaterno_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" id="apPaterno_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" type="text" placeholder="PATERNO" value = "'+respuesta+'" onchange="cambio(this);" onkeyup="javascript:this.value=this.value.toUpperCase();">\
            </div>\
            </div>\
            <div class="row">\
            <div class="col-md-3">MATERNO:</div>\
            <div class="col-md-9">\
            <input class="form-control letra_10" name="apMaterno_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" id="apMaterno_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" type="text" placeholder="MATERNO" value = "'+respuesta+'" onchange="cambio(this)" onkeyup="javascript:this.value=this.value.toUpperCase();">\
            </div>\
            </div>';
        }

        else if(pregunta['TIPOPREGUNTA'] == 5 ){

            var select = '<div class="row mtop1">\
            <div class="col-md-3">NOMBRE:</div>\
            <div class="col-md-9">\
            <input class="form-control letra_10" name="anio_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" id="nombreBen_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" type="text" placeholder="NOMBRE" value = "'+respuesta+'" onchange="cambio(this)" onkeyup="javascript:this.value=this.value.toUpperCase();">\
            </div>\
            </div>\
            <div class="row">\
            <div class="col-md-3">PATERNO:</div>\
            <div class="col-md-9">\
            <input class="form-control letra_10" name="mes_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" id="apPaterno_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" type="text" placeholder="PATERNO" value = "'+respuesta+'" onchange="cambio(this);" onkeyup="javascript:this.value=this.value.toUpperCase();">\
            </div>\
            </div>\
            <div class="row">\
            <div class="col-md-3">MATERNO:</div>\
            <div class="col-md-9">\
            <input class="form-control letra_10" name="dia_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" id="apMaterno_'+idImagen+'_'+pregunta['IDPREGUNTA']+'" type="text" placeholder="MATERNO" value = "'+respuesta+'" onchange="cambio(this)" onkeyup="javascript:this.value=this.value.toUpperCase();">\
            </div>\
            </div>';
        }


        return select;
    }

    function masImagenes(){
        parent.location.hash = indiceGen;
        guardar_datos_val();
    }

    function guardar_datos_val(pinta = 1){
        $('.bloqueo').each(function(i, obj) {
            $("#"+obj.id).removeAttr("disabled");
        });

            var form = $("#formulario");
            var url = form.attr('action');
            data = {opcion: 'guardar_datos_val',form: form.serialize()}
            //alert(form.toSource());

            $.ajax({
                   type: "POST",
                   url: url,
                   data: form.serialize(), // serializes the form's elements.
                   success: function(data)
                   {
                        $("#save").fadeIn();
                        setTimeout(function(){
                            $("#save").fadeOut();
                        },2000);
                       //alert(data); // show response from the php script.
                       if(pinta == 1 ){
                        $("#resultados").html('');
                        pintaImagenes();
                       }
                   }
                 });

            //e.preventDefault(); // avoid to execute the actual submit of the form.


    }

    function traeDocsEsp(id){
        var idImagen = extraeID(id);
        var idTipoImagen = $("#tipoImagen_"+idImagen).val();
        var options = "";
        var bloc = "";

        if(tipoDocumentoEspecifico[idTipoImagen] !=null){
            if(tipoDocumentoEspecifico[idTipoImagen].length>1){
                options += "<option value = ''>SELECCIONE</option>";
            }else{
                bloc = "";
            }
            options += contruyeOpcion(tipoDocumentoEspecifico[idTipoImagen],0);
        }
        var selectTipoDocumentoEspecifico = '<select '+bloc+' name="tipoDocumentoEspecifico_'+idImagen+'" id="tipoDocumentoEspecifico_'+idImagen+'" class="form-control bloqueo" onchange="cambio(this); $(\'#extras_\'+extraeID(this.id)).html(pintaPreguntas(extraeID(this.id),this.value,preguntas,respuestas));  pintaCamposChange(this.id)">'+options+'</select>';

        $("#div_select_especifico_"+idImagen).html(selectTipoDocumentoEspecifico);
        if(tipoDocumentoEspecifico[idTipoImagen].length>1){

        }else{
            var idImag = extraeID(id)
            var preg = pintaPreguntas(idImag,tipoDocumentoEspecifico[idTipoImagen][0]['VALUE'],preguntas,respuestas);
            $("#extras_" + idImag ).html(preg);
        }
    }

</script>
