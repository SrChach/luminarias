
<?php

?>

<style type="text/css">
b{
	color: #585858;
}

h5{
	font-family: italic;
	color: #6E6E6E;
	font-size: 12pt;
}

p{
	font-family: italic;
	color: #6E6E6E;
	font-size: 12pt;
}

h3{
	color: #6E6E6E;
	font-size: 12pt;
}

a{
	padding-left: 10px;
}
.demo{
	border: 2px solid grey;

-webkit-border-radius: 6px 10px 2px 8px; /* recuerda la primera frase */
-moz-border-radius: 4px; /* si quieres todas las esquinas iguales */
border-radius: 10px;
}	
	

 </style>


    

<div class="demo  container d-flex h-100 shadow-lg p-3 mb-5 bg-white rounded">
	<div class="row justify-content-center align-self-center">

		<div class="col-lg-12" id="8">
			<nav class="nav  bg-secondary" >
			<a>I. CONTROL DE LLENADO</a>	
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p505">FOLIO PROGRAMA/PROYECTO</b><br></h6><h5><span class="data" id="505"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p501">Tipo de proceso</b><br></h6><h5><span class="data" id="501"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p502">Punto de recolección</b><br></h6><h5><span class="data" id="502"><br><br></span><hr></h5>
		</div>	


		<div class="col-lg-12" id="8">
			<nav class="nav bg-secondary">
 		 	<a>II. IDENTIFICACIÓN GEOGRÁFICA</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p301">Elija la entidad federativa</b><br></h6><h5><span class="data" id="301"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p302">Elija el municipio o delegación</b><br></h6><h5><span class="data" id="302"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p303">Elija la localidad</b><br></h6><h5><span class="data" id="303"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p304">Escriba la Clave de AGEB</b><br></h6><h5><span class="data" id="304"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p305">Escriba la Clave de Manzana</b><br></h6><h5><span class="data" id="305"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="8">
			<nav class="nav  bg-secondary">
 		 	<a>III. DOMICILIO GEOGRÁFICO</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p306">Encuestador: ¿La vivienda se encuentra referida a una carretera o a un camino?</b><br></h6><h5><span class="data" id="306"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="9">
			<nav class="nav bg-secondary">
 		 	<a>III. A. IDENTIFICACIÓN Y REGISTRO DE LOS COMPONENTES DE CARRETERA</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p307">Tipo de administración de la carretera a la que está referida la vivienda</b><br></h6><h5><span class="data" id="307"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p308">Derecho de tránsito de la carretera a la que está referida la vivienda</b><br></h6><h5><span class="data" id="308"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p309">Identificar y anotar el código de la carretera a la que está referida la vivienda</b><br></h6><h5><span class="data" id="309"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p310">Identificar y anotar el tramo (origen) del domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="310"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p311">Identificar y anotar el tramo (destino) del domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="311"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p312">Identificar y anotar el cadenamiento (kilómetro) del domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="312"><br><br></span><hr></h5>	
			<h6 class="questionBlock goLeft"><b id="p313">Identificar y anotar el cadenamiento (metros) del domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="313"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="10">		
			<nav class="nav bg-secondary">
 		 	<a>III. B. IDENTIFICACIÓN Y REGISTRO DE LOS COMPONENTES DEL CAMINO</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p314">Término genérico con el que se identifica el camino al cual está referida la vivienda</b><br></h6><h5><span class="data" id="314"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p315">Identificar y anotar el tramo (origen) del domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="315"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p316">Identificar y anotar el tramo (destino) del domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="316"> <br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p317">Identificar y seleccionar el margen sobre el que se ubica el domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="317"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p318">Identificar y anotar el cadenamiento (kilómetro) del domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="318"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p319">Identificar y anotar el cadenamiento (metros) del domicilio geográfico de la vivienda</b><br></h6><h5><span class="data" id="319"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="11">
			<nav class="nav bg-secondary">
 		 	<a>III. C. IDENTIFICACIÓN Y REGISTRO DE LOS COMPONENTES DE VIALIDAD</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p320">Identificar el tipo de vialidad a la que está referida la vivienda</b><br></h6><h5><span class="data" id="320"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p321">Nombre de la vialidad a la que está referida la vivienda</b><br></h6><h5><span class="data" id="321"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="12">
			<nav class="nav bg-secondary">
 		 	<a>III. IDENTIFICACIÓN VIALIDAD</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p322">Número exterior del domicilio geográfico</b><br></h6><h5><span class="data" id="322"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p324">Número exterior anterior</b><br></h6><h5><span class="data" id="324"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p503">Número interior del domicilio geográfico</b><br></h6><h5><span class="data" id="503"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p504">Letra interior del domicilio geográfico</b><br></h6><h5><span class="data" id="504"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p332">Código postal</b><br></h6><h5><span class="data" id="332"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p333">Identificar y registrar el tipo de asentamiento en que se encuentra la vivienda:</b><br></h6><h5><span class="data" id="333"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p334">Identificar y registrar el nombre del asentamiento</b><br></h6><h5><span class="data" id="334"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p325">Tipo de la entre vialidad 1 del domicilio geográfico</b><br></h6><h5><span class="data" id="325"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p326">Nombre de la entre vialidad 1 del domicilio geográfico</b><br></h6><h5><span class="data" id="326"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p327">Tipo de la entre vialidad 2 del domicilio geográfico</b><br></h6><h5><span class="data" id="327"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p328">Nombre de la entre vialidad 2 del domicilio geográfico</b><br></h6><h5><span class="data" id="328"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p329">Tipo de la vialidad posterior del domicilio geográfico</b><br></h6><h5><span class="data" id="329"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p330">Nombre de la vialidad posterior del domicilio geográfico</b><br></h6><h5><span class="data" id="330"> <br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p331">Identificar y registrar la descripción de ubicación (referencia) del domicilio geográfico</b><br></h6><h5><span class="data" id="331"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="13">
			<nav class="nav bg-secondary">
 		 	<a>IV. INFORMANTE ADECUADO</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p336">1. Identifique si existe un informante adecuado que cumpla con las siguientes características:</b><br></h6><h5><span class="data" id="336"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p337">2a. Documento oficial que presenta (informante) para acreditar su identidad</b><br></h6><h5><span class="data" id="337"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p338">2b. Ingrese el folio del documento.</b><br></h6><h5><span class="data" id="338"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p339">3a. Documento oficial que presenta (informante) para acreditar su edad</b><br></h6><h5><span class="data" id="339"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p340">3b. Ingrese el folio del documento.</b><br></h6><h5><span class="data" id="340"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="13">
			<nav class="nav bg-secondary">
 		 	<a>V. TIPO DE VIVIENDA</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p341">4. ¿Su vivienda es...?</b><br></h6><h5><span class="data" id="341"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p342">5. ¿Cuántas personas habitan su vivienda?</b><br></h6><h5><span class="data" id="342"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="13">
	    	<nav class="nav bg-secondary">
 		 	<a>VI. NÚMERO DE HOGARES</a>
	    	</nav><br>
			<h6 class="questionBlock goLeft"><b id="p343">6. ¿Cuántos hogares hay en su vivienda?, incluyendo el suyo</b><br></h6><h5><span class="data" id="343"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="13">
		    <nav class="nav bg-secondary">
 		 	<a >VII. IDENTIFICACIÓN DEL HOGAR</a>
	    	</nav><br>
			<h6 class="questionBlock goLeft"><b id="p344">7. ¿Cuántas personas forman parte de este hogar, contando a los niños pequeños, adultos mayores y personas con discapacidad?</b><br></h6><h5><span class="data" id="344"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p345">8. Las personas que forman parte de este hogar, ¿Comparten gastos?</b><br></h6><h5><span class="data" id="345"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p346">9. Las personas que forman parte de este hogar, ¿Habitan regularmente la misma vivienda en la que duermen, preparan y comparten sus alimentos?</b><br></h6><h5><span class="data" id="346"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p347">10a. ¿Tiene teléfono propio o alguno en el que le puedan dejar recado?</b><br></h6><h5><span class="data" id="347"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p544">10b. ¿Cuantos numeros de teléfono desea registrar?</b><br></h6><h5><span class="data" id="544"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p582">10c. Ingrese los números de teléfono</b><br></h6><h5><span class="data" id="582"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p348">10c. ¿Cuál es el número?</b><br></h6><h5><span class="data" id="348"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p349">10d. ¿Qué tipo de teléfono es?</b><br></h6><h5><span class="data" id="349"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="44" >
		   <nav class="nav bg-secondary">
		   <a>VIII. DATOS DE TODOS LOS INTEGRANTES DEL HOGAR</a>
		   </nav>
		   <div id="aux"></div>	
		</div>
		

		<div class="col-lg-12" id="23">
		    <nav class="nav bg-secondary">
 		 	<a>VIII. DATOS DE TODOS LOS INTEGRANTES DEL HOGAR</a>
		    </nav><br>
			<h6 class="questionBlock goLeft"><b id="p485">¿Quién es el informante del hogar?</b><br></h6><h5><span class="data" id="485"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p542">Firma de informante</b><br></h6><h5><span class="data" id="542"><img src= "" alt="" style="width:300px;height:auto;"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p543">Tome la huella dígital del informante</b><br></h6><h5><span class="data" id="543"><img src="http://75.126.73.172:8888/Sync/imageData/" alt="" style="width:300px;height:auto;"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p583">Fotografía del jefe del hogar</b><br></h6><h5><span class="data" id="583"><img src="http://75.126.73.172:8888/Sync/imageData/" alt="" style="width:300px;height:auto;"><br><br></span><hr></h5>
			<!-- <h6 class="questionBlock goLeft"><b id="p362">21. ¿Vive el padre de %nombre% en este hogar?</b><br></h6><h5><span class="data" id="362"><br><br></span><hr></h5> -->
			<!-- <h6 class="questionBlock goLeft"><b id="p363">21b. ¿Quién es el padre de %nombre%?</b><br></h6><h5><span class="data" id="363"><br><br></span><hr></h5> -->
			<!-- <h6 class="questionBlock goLeft"><b id="p364">22. ¿Vive la madre de %nombre% en este hogar?</b><br></h6><h5><span class="data" id="364"><br><br></span><hr></h5> -->
			<!-- <h6 class="questionBlock goLeft"><b id="p365">22b. ¿Quién es la madre de %nombre%?</b><br></h6><h5><span class="data" id="365"><br><br></span><hr></h5> -->
		</div>


		<div class="col-lg-12" id="44" >
		    <nav class="nav bg-secondary">
	    	<a>IX-XV. DATOS GENERALES DE LOS INTEGRANTES DEL HOGAR</a>
		    </nav>
	    	<div id="aux2"></div>	
		</div>


		<div class="col-lg-12" id="35">
	    	  <nav class="nav bg-secondary">
 		 	<a>XVIII. SALUD EN EL HOGAR</a>
	    	</nav><br>
			<h6 class="questionBlock goLeft"><b id="p396">51a. ¿A algún integrante del hogar le ha sido diagnosticada Artritis por un médico?</b><br></h6><h5><span class="data" ><ul><li id="396"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p397">51b. ¿A algún integrante del hogar le ha sido diagnosticado Cáncer por un médico?</b><br></h6><h5><span class="data"><ul><li id="397"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p398">51c. ¿A algún integrante del hogar le ha sido diagnosticada Cirrosis por un médico?</b><br></h6><h5><span class="data"><ul><li id="398"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p399">51d. ¿A algún integrante del hogar le ha sido diagnosticada Deficiencia renal por un médico?</b><br></h6><h5><span class="data"><ul><li id="399"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p400">51e. ¿A algún integrante del hogar le ha sido diagnosticada Diabetes por un médico?</b><br></h6><h5><span class="data"><ul><li id="400"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p401">51f. ¿A algún integrante del hogar le ha sido diagnosticadas Enfermedades del corazón por un médico?</b><br></h6><h5><span class="data"><ul><li id="401"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p402">51g. ¿A algún integrante del hogar le ha sido diagnosticada Enfisema pulmonar por un médico?</b><br></h6><h5><span class="data"><ul id="402"></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p403">51h. ¿A algún integrante del hogar le ha sido diagnosticada VIH por un médico?</b><br></h6><h5><span class="data"><ul><li id="403"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p404">51i. ¿A algún integrante del hogar le ha sido diagnosticada Deficiencia nutricional (anemia/desnutrición) por un médico?</b><br></h6><h5><span class="data"><ul><li id="404"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p405">51j. ¿A algún integrante del hogar le ha sido diagnosticada Hipertensión por un médico?</b><br></h6><h5><span class="data"><ul><li id="405"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p406">51k. ¿A algún integrante del hogar le ha sido diagnosticada Obesidad por un médico?</b><br></h6><h5><span class="data"><ul><li id="406"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p407">52a.1.  ¿Algún integrante del hogar tiene limitación para caminar, moverse, subir o bajar escaleras?</b><br></h6><h5><span class="data"><ul><li id="407"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p408">52a.2. ¿Qué grado de limitación tiene para caminar, moverse, subir o bajar escaleras?</b><br></h6><h5><span class="data"><ul><li id="408"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p409">52a.3. ¿Cuál es el origen de la limitación que tiene para caminar, moverse, subir o bajar escaleras?</b><br></h6><h5><span class="data"><ul><li id="409"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p561">52a.4. ¿Qué apoyo requiere para moverse?</b><br></h6><h5><span class="data"><ul><li id="561"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p562">52a.5. ¿Actualmente tiene el apoyo que requiere para moverse y cuál es su estado?</b><br></h6><h5><span class="data"><ul><li id="562"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p410">52b.1.  ¿Algún integrante del hogar tiene limitación para ver, o sólo ve sombras aún usando lentes?</b><br></h6><h5><span class="data"><ul><li id="410"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p411">52b.2. ¿Qué grado de limitación tiene para ver, o sólo ve sombras aún usando lentes?</b><br></h6><h5><span class="data"><ul><li id="411"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p412">52b.3. ¿Cuál es el origen de la limitación que tiene para ver, o sólo ve sombras aún usando lentes?</b><br></h6><h5><span class="data"><ul><li id="412"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p563">52b.4. ¿Qué requiere para ver mejor?</b><br></h6><h5><span class="data"><ul><li id="563"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p413">52c.1.  ¿Algún integrante del hogar tiene limitación para hablar, comunicarse o conversar?</b><br></h6><h5><span class="data"><ul><li id="413"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p414">52c.2. ¿Qué grado de limitación tiene para hablar, comunicarse o conversar?</b><br></h6><h5><span class="data"><ul><li id="414"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p415">52c.3. ¿Cuál es el origen de la limitación que tiene para hablar, comunicarse o conversar?</b><br></h6><h5><span class="data"><ul><li id="415"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p416">52d.1.  ¿Algún integrante del hogar tiene limitación para oír, aún usando aparato auditivo?</b><br></h6><h5><span class="data"><ul><li id="416"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p500">52d.2. ¿Qué grado de limitación tiene para oír, aún usando aparato auditivo?</b><br></h6><h5><span class="data"><ul><li id="500"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p417">52d.3. ¿Cuál es el origen de la limitación que tiene para oír, aún usando aparato auditivo?</b><br></h6><h5><span class="data"><ul><li id="417"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p564">52d.4. ¿Qué apoyo requiere para oír mejor?</b><br></h6><h5><span class="data"><ul><li id="564"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p418">52e.1.  ¿Algún integrante del hogar tiene limitación para vestirse, bañarse o comer, desplazarse u otras de cuidado personal?</b><br></h6><h5><span class="data"><ul><li id="418"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p419">52e.2. ¿Qué grado de limitación tiene para vestirse, bañarse o comer, desplazarse u otras de cuidado personal?</b><br></h6><h5><span class="data"><ul><li id="419"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p420">52e.3. ¿Cuál es el origen de la limitación que tiene para vestirse, bañarse o comer, desplazarse u otras de cuidado personal?</b><br></h6><h5><span class="data"><ul><li id="420"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p565">52e.4. ¿Qué apoyo de cuidado personal requiere?</b><br></h6><h5><span class="data"><ul><li id="565"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p421">52f.1.  ¿Algún integrante del hogar tiene limitación para poner atención, aprender cosas sencillas o concentrarse?</b><br></h6><h5><span class="data"><ul><li id="421"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p422">52f.2. ¿Qué grado de limitación tiene para poner atención, aprender cosas sencillas o concentrarse?</b><br></h6><h5><span class="data"><ul><li id="422"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p423">52f.3. ¿Cuál es el origen de la limitación que tiene para poner atención, aprender cosas sencillas o concentrarse?</b><br></h6><h5><span class="data"><ul><li id="423"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p424">53. Cuando tienen problemas de salud ¿en dónde se atienden los integrantes del hogar?</b><br></h6><h5><span class="data"><ul><li id="424"></li></ul><br></span><hr></h5>
		</div>



		<div class="col-lg-12" id="36">
	     	<nav class="nav bg-secondary">
 		 	<a>XIX. TRABAJO NO REMUNERADO</a>
	    	</nav><br>
			<h6 class="questionBlock goLeft"><b id="p425">54a. ¿Quién realiza regularmente las siguientes actividades? Cuidar sin pago y de manera exclusiva a niños, enfermos, adultos mayores o discapacitados.</b><br></h6><h5><span class="data" id="425"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p426">54a.2. Seleccione los integrantes que realizan regularmente el cuidado sin pago y de manera exclusiva a niños, enfermos, adultos mayores o discapacitados.</b><br></h6><h5><span class="data" id="426"><ul><li></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p427">54b. ¿Quién realiza regularmente trabajo comunitario o voluntario?</b><br></h6><h5><span class="data" id="427"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p428">54b.2. Seleccione los integrantes que realizan regularmente trabajo comunitario o voluntario.</b><br></h6><h5><span class="data"><ul><li id="428"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p429">54c. ¿Quién realiza regularmente reparaciones a la vivienda, aparatos domésticos o vehículos?</b><br></h6><h5><span class="data" id="429"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p430">54c.2. Seleccione los integrantes que realizan regularmente reparaciones a la vivienda, aparatos domésticos o vehículos.</b><br></h6><h5><span class="data" id="430"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p431">54d. ¿Quién realiza regularmente el quehacer de su hogar?</b><br></h6><h5><span class="data" id="431"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p432">54d.2. Seleccione los integrantes que realizan regularmente el quehacer de su hogar.</b><br></h6><h5><span class="data"><ul><li id="432"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p433">54e. ¿Quién realiza regularmente el acarreo de agua o leña?</b><br></h6><h5><span class="data" id="433"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p434">54e.2. Seleccione los integrantes que realizan regularmente el acarreo de agua o leña.</b><br></h6><h5><span class="data"><ul><li id="434"></li></ul><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="37">
		    <nav class="nav bg-secondary">
 		 	<a>XX. OTROS INGRESOS DEL HOGAR</a>
		    </nav><br>
			<h6 class="questionBlock goLeft"><b id="p435">55. En este hogar vive algún integrante que sea …</b><br></h6><h5><span class="data"><ul><li id="435"></li></ul><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p508">55b. ¿Cuánto gana mensualmente?</b><br></h6><h5><span class="data"><ul><li id="508"></li></ul><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="38">
		    <nav class="nav bg-secondary">
 		 	<a>XXI. REMESAS</a>
		    </nav><br>
			<h6 class="questionBlock goLeft"><b id="p436">56. ¿Alguien en el hogar recibe dinero proveniente de otros países?</b><br></h6><h5><span class="data" id="436"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="39">
		    <nav class="nav bg-secondary">
 		 	<a>XXII. GASTO Y CONSUMO</a>
		    </nav><br>
			<h6 class="questionBlock goLeft"><b id="p437">57a. Regularmente en un mes ¿cuánto gasta su hogar en la compra de alimentos, bebidas? (cereales, carnes, pescados y mariscos, leche, huevo aceite, verduras, legumbres, frutas, azúcar, chocolate)</b><br></h6><h5><span class="data" id="437"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p438">57b. Regularmente en un mes ¿cuánto gasta su hogar en la compra o reparación de vestido o calzado?</b><br></h6><h5><span class="data" id="438"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p439">57c. Regularmente en un mes ¿cuánto gasta su hogar en la compra de artículos y servicios de educación (inscripción, colegiatura, útiles escolares, etc.)?</b><br></h6><h5><span class="data" id="439"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="40">
		    <nav class="nav bg-secondary">
 		 	<a>XXIII. SEGURIDAD ALIMENTARIA</a>
		    </nav><br>
			<h6 class="questionBlock goLeft"><b id="p440">58. ¿Cuántas comidas al día acostumbran hacer los miembros de este hogar? (desayuno, almuerzo, comida y cena)</b><br></h6><h5><span class="data" id="440"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p441">59. ¿Con qué frecuencia consume por semana...?</b><br></h6><h5><span class="data"><ul><li id="441"></li><li></li><li></li><li></li><li></li><li></li><li></li></ul><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p442">60. En los últimos tres meses, por falta de dinero o recursos ¿alguna vez usted o algún adulto (integrante de 18 años a más) …</b><br></h6><h5><span class="data"><ul><li id="442"></li><li></li><li></li><li></li><li></li><li></li></ul><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p443">61. En los últimos tres meses, por falta de dinero o recursos ¿alguna vez algún menor de 18 años en su hogar…</b><br></h6><h5><span class="data"><ul><li id="443"></li><li></li><li></li><li></li><li></li><li></li></ul><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p444">62a. ¿Acostumbran desayunar los integrantes menores de 12 años de este hogar?</b><br></h6><h5><span class="data" id="444"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p445">62b. ¿En dónde?</b><br></h6><h5><span class="data" id="445"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p446">62c. ¿Por qué?</b><br></h6><h5><span class="data" id="446"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="41">
		    <nav class="nav bg-secondary">
 		 	<a>XXIV. CARACTERÍSTICAS DE LA VIVIENDA</a>
		    </nav><br>
			<h6 class="questionBlock goLeft"><b id="p447">63. ¿Cuántos cuartos tiene en total esta vivienda contando la cocina? (No cuente ni pasillos ni baños)</b><br></h6><h5><span class="data" id="447"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p448">64. ¿Cuántos cuartos usan para dormir?</b><br></h6><h5><span class="data" id="448"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p449">65. En el cuarto donde cocinan ¿también duermen?</b><br></h6><h5><span class="data" id="449"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p450">66. ¿De qué material es la mayor parte del piso de su vivienda?</b><br></h6><h5><span class="data" id="450"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p537">67. ¿El cemento o piso firme de su vivienda fue otorgado por algún programa gubernamental (Federal, Estatal o Municipal)?</b><br></h6><h5><span class="data" id="537"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p451">68. ¿La mayor parte del piso de la vivienda presenta hundimientos o agrietamientos mayores a 1 cm de grosor?</b><br></h6><h5><span class="data" id="451"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p452">69. Alguno de los cuartos donde duermen o cocinan ¿tiene piso de tierra?</b><br></h6><h5><span class="data" id="452"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p453">70. ¿De qué material es la mayor parte del techo de su vivienda?</b><br></h6><h5><span class="data" id="453"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p454">71. ¿Al menos un techo de los cuartos presenta flexión y/o fracturas, o está en riesgo de caerse?</b><br></h6><h5><span class="data" id="454"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p455">72. ¿De qué material es la mayor parte de las paredes o muros de su vivienda?</b><br></h6><h5><span class="data" id="455"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p456">73. ¿Algún muro de la vivienda presenta grietas o fisuras mayores a 1 cm. de grosor, o está en riesgo de caerse?</b><br></h6><h5><span class="data" id="456"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p457">74. ¿Qué tipo de baño o escusado tiene su vivienda?</b><br></h6><h5><span class="data" id="457"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p538">75. ¿El baño o excusado de su vivienda fue otorgado por algún programa gubernamental (Federal, Estatal o Municipal)?</b><br></h6><h5><span class="data" id="538"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p458">76. ¿El baño o escusado es para uso exclusivo de los habitantes de su vivienda?</b><br></h6><h5><span class="data" id="458"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p459">77. En esta vivienda tienen …</b><br></h6><h5><span class="data" id="459"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p551">¿La llave pública, hidrante o red de agua a que distancia le queda?</b><br></h6><h5><span class="data" id="551"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p552">¿Por qué no ha contratado el servicio?</b><br></h6><h5><span class="data" id="552"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p553">¿Cuánto cuesta el contrato?</b><br></h6><h5><span class="data" id="553"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p554">En tanto llega la red de agua, ¿Qué facilitaría el suministro, la captación o almacenamiento del agua?</b><br></h6><h5><span class="data" id="554"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p460">78. ¿Qué tratamiento le dan al agua para beberla?</b><br></h6><h5><span class="data"><ul><li id="460"></li></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p461">79. ¿Qué tipo de drenaje o desagüe de aguas sucias tiene su vivienda?</b><br></h6><h5><span class="data" id="461"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p555">¿Hay red pública de drenaje cerca de su casa?</b><br></h6><h5><span class="data" id="555"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p556">¿A qué distancia de su casa está?</b><br></h6><h5><span class="data" id="556"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p557">¿Por qué no ha hecho el contrato de conexión al drenaje?</b><br></h6><h5><span class="data" id="557"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p558">¿Cuánto cuesta el contrato?</b><br></h6><h5><span class="data" id="558"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p462">80. En su vivienda ¿Qué hacen con la basura?</b><br></h6><h5><span class="data" id="462"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p463">81. ¿Cuál es el combustible que más usan para cocinar?</b><br></h6><h5><span class="data" id="463"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p464">82. ¿Qué aparato usa para cocinar?</b><br></h6><h5><span class="data" id="464"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p495">82b. ¿El fogón está dentro de la vivienda?</b><br></h6><h5><span class="data" id="495"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p465">83. ¿En su hogar tiene y sirve?</b><br></h6><h5><span class="data"><ul><li id="465"></li><li></li></ul><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p539">83b. ¿La computadora fue otorgada por algún programa gubernamental (Federal, Estatal o Municipal)</b><br></h6><h5><span class="data"><ul><li id="539"></li><li></li></ul><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p466">84. En su vivienda ¿La luz eléctrica la obtienen...</b><br></h6><h5><span class="data" id="466"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p467">85. ¿La vivienda que habita es…</b><br></h6><h5><span class="data" id="467"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p468">86a. Indique el integrante del hogar que tiene a su nombre las escrituras…</b><br></h6><h5><span class="data" id="468"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p469">86b. Seleccione el integrante del hogar que tiene a su nombre las escrituras</b><br></h6><h5><span class="data"><ul><li id="469"></li></ul></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p470">87a. La vivienda tiene dos o más niveles</b><br></h6><h5><span class="data" id="470"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p471">87b.1. La vivienda tiene espacio disponible para construcción o uso</b><br></h6><h5><span class="data" id="471"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p472">87b.2. ¿De qué medida?</b><br></h6><h5><span class="data" id="472"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p473">87c.1. La vivienda tiene Local anexo</b><br></h6><h5><span class="data" id="473"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p474">87c.2. ¿De qué medida?</b><br></h6><h5><span class="data" id="474"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p566">Clave INEGI</b><br></h6><h5><span class="data" id="566"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p567">¿La vivienda necesita energía eléctrica?</b><br></h6><h5><span class="data" id="567"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p568">Sin contar baños y pasillos, ¿Cuántos cuartos tiene la vivienda?</b><br></h6><h5><span class="data" id="568"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p569">Condiciones de cada cuarto</b><br></h6><h5><span class="data" ><ul id="569"><h5><b>Cuarto: 1</b></h5><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><br><h5><b>Cuarto: 2</b></h5><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><br></ul><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p570">Croquis del Terreno</b><br></h6><h5><span class="data" id="570"><img src="http://75.126.73.172:8888/Sync/imageData/" alt="" style="width:300px;height:auto;"></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p571">Indicar la figura del terreno</b><br></h6><h5><span class="data" id="571"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p572">Medidas del terreno</b><br></h6><h5><span class="data" id="572"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p573">Medidas del terreno</b><br></h6><h5><span class="data" id="573"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p574">Área del terreno</b><br></h6><h5><span class="data" id="574"><br><br></span><hr></h5>		
			<h6 class="questionBlock goLeft"><b id="p575">Terreno disponible</b><br></h6><h5><span class="data"><ul id="575"><h5><b></b></h5><li></li><li></li><li></li><li></li><br></ul><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p576">Características del suelo</b><br></h6><h5><span class="data" id="576"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p577">El predio de esta vivienda se encuentra</b><br></h6><h5><span class="data" id="577"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p578">¿El camino más cercano a la vivienda se encuentra a más de 200 metros de distancia?</b><br></h6><h5><span class="data" id="578"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p579">Fotografía de la vivienda</b><br></h6><h5><span class="data" id="579"><img src="http://75.126.73.172:8888/Sync/imageData/" alt ="" style="width:300px;height:auto;"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p580">Fotografía del predio</b><br></h6><h5><span class="data" id="580"><img src="http://75.126.73.172:8888/Sync/imageData/" alt ="" style="width:300px;height:auto;"><br><br></span><hr></h5>                                                              
			<h6 class="questionBlock goLeft"><b id="p581">Observaciones de la vivienda</b><br></h6><h5><span class="data" id="581"> <br><br></span><hr></h5>
		</div>   


		<div class="col-lg-12" id="42">
			<nav class="nav bg-secondary">
 		 	<a>XXV. POSESIÓN DE BIENES PRODUCTIVOS</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p475">88. ¿Alguna persona del hogar posee o utilizó en los últimos 12 meses tierras para la agricultura o aprovechamiento forestal?</b><br></h6><h5><span class="data" id="475"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p476">89a. ¿Las tierras pertenecen a algún integrante del hogar (propias)?</b><br></h6><h5><span class="data" id="476"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p477">89b. Seleccione al integrante a quien le pertenecen las tierras</b><br></h6><h5><span class="data" id="477"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p478">90. ¿Qué productos cultiva?</b><br></h6><h5><span class="data" id="478"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p479">91. Para cultivar utiliza ¿</b><br></h6><h5><span class="data" id="479"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p480">92. ¿En el hogar se emplea la hidroponía o la agricultura de traspatio (huertos) para el cultivo de productos?</b><br></h6><h5><span class="data" id="480"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p481">93. ¿Tienen en este hogar para trabajo y/o consumo ...</b><br></h6><h5><span class="data"><ul><li id="481"></li></ul><br></span><hr></h5>
		</div>

		
		<div class="col-lg-12" id="43">
			<nav class="nav bg-secondary">
 		 	<a>XXVI. PROYECTOS PRODUCTIVOS</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p482">94. ¿A algún integrante de este hogar le gustaría realizar un proyecto productivo o de servicio?</b><br></h6><h5><span class="data" id="482"><br><br></span><hr></h5>
		</div>


		<div class="col-lg-12" id="44" >
			<nav class="nav bg-secondary">
 		 	<a>XXVIII. RESULTADO DE LA ENCUESTA</a>
			</nav><br>
			<h6 class="questionBlock goLeft"><b id="p484">95. ¿La información de la encuesta fue obtenida a través de un traductor?</b><br></h6><h5><span class="data" id="484"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p559">Pregunta para el encuestador ¿En su percepción, la familia es susceptible de recibir los apoyos del Programa? </b><br></h6><h5><span class="data" id="559"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p560">Proporcione la ubicación de referencia</b><br></h6><h5><span class="data" id="560"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p511">Visita del encuestador</b><br></h6><h5><span class="data" id="511"><br><br></span><hr></h5>
			<h6 class="questionBlock goLeft"><b id="p490">Observaciones de la encuesta:</b><br></h6><h5><span class="data" id="490"></span><hr></h5>
			<h6 class="privacidad"><b id="">96. ¿Me dieron a conocer y acepto los términos, condiciones y la política de privacidad?</b><br></h6><h5><span class="data" id="">SI<br><br></span><hr></h5>
		</div>
			
	</div>
</div>

	
<script>	

var fcuis = "<?php echo $_GET['folio_cuis'] ?>";
var url = "../../pillar/clases/controlador_pruebas.php";
var data = {opcion:"respuestas_cuis",folio_cuis:fcuis};
res = ajax(data,url);
var respuesta = JSON.parse(res);
var respuestas_cuis  = respuesta['respuestas'].respuestas; 
var itera =  respuesta['respuestas'].itera.ITERA;
var count=0;
var contenidoS_22='';
var contenidoS_14='';
$.each(respuesta.respuestas.respuestas_iteraciones,function (i,item) {
	num_inte=(i+1);
	contenidoS_22+= '<h3 class="text-light shadow p-3 mb-5 bg-white rounded border-bottom-0"><b>INTEGRANTE DEL HOGAR  '+num_inte+'</b></h3>';
	contenidoS_14+= '<h3 class="text-light shadow p-3 mb-5 bg-white rounded border-bottom-0"><b>INTEGRANTE DEL HOGAR  '+num_inte+'</b></h3>';
	$.each(item[i],function (j,value) {
		$.each(value,function (k,objet) {
			
			if (objet.ID_S==22) {
				contenidoS_22 += '<div><label style=" color: #585858" for="" data-iteracion="'+i+'">'+objet.PREGRUNTA+'</label><p>'+objet.RESPUESTA+'<hr></p></div>';	
			}
			if (objet.ID_S==14) {
				contenidoS_14 += '<div><label style=" color: #585858" for="" data-iteracion="'+i+'">'+objet.PREGRUNTA+'</label><p>'+objet.RESPUESTA+'<hr></p></div>';	
			}
			
		})
	})
});
//------------envio de contenido
$('#aux').append(contenidoS_22);
$('#aux2').append(contenidoS_14);


var nombresObj = [];
for (var i = 0; i < respuestas_cuis.length; i++) {
	
		var respuesta_a_pregunta_especifica = respuestas_cuis[i];
		var pregunta_id = respuesta_a_pregunta_especifica['PREGUNTA_ID'];
		var texto_respuesta =  respuesta_a_pregunta_especifica['TEXTO'];
		var iteracion_respuesta =  respuesta_a_pregunta_especifica['ITERACION'];
			
		if(pregunta_id=="350"){
				if(!nombresObj[iteracion_respuesta]){					
					nombresObj[iteracion_respuesta] = {nombre : texto_respuesta };														
				}
		}
		if(pregunta_id=="351"){
				if(nombresObj[iteracion_respuesta]){					
					nombresObj[iteracion_respuesta].primerApellido = texto_respuesta;										
				}
		}
		if(pregunta_id=="352"){
				if(nombresObj[iteracion_respuesta]){					
					nombresObj[iteracion_respuesta].segundoApellido = texto_respuesta;					
				}
		}
		
//------------mostrar imagenes
	if (texto_respuesta!= null) {
		var aux=texto_respuesta.split(".");
		if (aux[(aux.length-1)]=='png') {
				$("#"+pregunta_id).html('<img src="http://75.126.73.172:8888/Sync/imageData/'+texto_respuesta+'" alt="" width="280px" height="290px" />');	
			}else{ 

				
				if (iteracion_respuesta==0) {
				$("#"+pregunta_id).html(texto_respuesta);
				}
 	 		}
	}

}
//-------------Concatenar nombre y sustituir por %nombre%
Object.keys(nombresObj).forEach(function(item){
	$('label[data-iteracion="'+item+'"]').each(function(i, itemEncontrado){
	let nuevoValor = $(itemEncontrado).text();
	nuevoValor = nuevoValor.replace('%nombre%', (nombresObj[item].nombre + ' ' + nombresObj[item].primerApellido + ' ' + nombresObj[item].segundoApellido));
	$(itemEncontrado).text(nuevoValor);		
	})
});


/*for (var i = 0; i < Things.length; i++) {
	Things[i]

	if(   $("#"+539).html()=='<br><br>' ||  $("#"+539).html()=='' ){
		$("<span.data"+"#"+539).fadeOut();
	 	$("#"+"p"+539).fadeOut();	
		alert('ok');
	 } 
	
}*/


</script>
